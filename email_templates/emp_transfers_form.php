<?php
    // print_r();
    // exit;
?>
<table width="650px" style="line-height:22px">
	<tr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <tr>
                    <td width="70%">
                        <img src="<?php echo $transfer_details['LOGO_URL']?>" width="151" height="35" />
                    </td>
                    <td width="30%" align="right">
                    	Report Generated At<br />
                        <?php echo $transfer_details['CREATED_DATE_TIME']?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <tr bgcolor="#0070C0">
                    <td valign="top" colspan="2" style="color:#FFF">EMPLOYEE DETAILS</td>
                </tr>                
                <tr>
                	<td width="10%" valign="top">Employee Name</td>
                    <td width="20%" valign="top"><?php echo $transfer_details['EMPLOYEE_NAME']?></td>
                    <td width="10%" valign="top">Employee Number</td>
                    <td width="20%" valign="top"><?php echo $transfer_details['EMPLOYEE_CODE']?></td>
                    <td width="10%" valign="top">D.O.J</td>
                    <td width="20%" valign="top"><?php echo $transfer_details['JOINING_DATE']?></td>
                </tr>
                <tr>
                	<td valign="top">Designation</td>
                    <td valign="top"><?php echo $transfer_details['EMPLOYEE_DESIGNATION']?></td>
                </tr>
                <tr>
                	<td valign="top">Present Work in Branch/Department Name</td>
                    <td valign="top"><?php echo $transfer_details['CURRENT_BRANCH']?></td>
                </tr>
                <tr>
                	<td valign="top">Transfer to new Branch/Department Name</td>
                    <td valign="top"><?php echo $transfer_details['TRNSFER_BRANCH']?></td>
                </tr>
                <tr>
                	<td valign="top">First time posting Branch/Department Name</td>
                    <td valign="top"><?php echo $transfer_details['FIRST_BRANCH']?></td>
                </tr>
                <tr>
                	<td valign="top">Date of transfer in the Branch/Department Name</td>
                    <td valign="top"><?php echo $transfer_details['FIRST_BRANCH_DATE']?></td>
                </tr>
                <tr>
                	<td valign="top">Processed transfer in first time</td>
                    <td valign="top"><?php echo $transfer_details['FIRST_BRANCH_PROCESSED']?></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <thead>
                    <tr bgcolor="#0070C0">
                        <th>Posting</th>
                        <th>From Branch/Dept Name</th>
                        <th>To Branch/Dept Name</th>
                        <th>Date of Posting</th>
                        <th>Period</th>
                    </tr>   

                </thead>
                <tbody>
                    <?php 
                    $i =1;
                    foreach ($transfer_details['POSTINGS'] as $key => $posting) {
                    ?>
                    <tr>
                        <td><?php echo $i?></td>
                        <td><?php echo $posting['company_from']?></td>
                        <td><?php echo  $posting['company_to']?></td>
                        <td><?php echo  $posting['processed_date']?></td>
                        <td><?php echo  10?></td>
                    </tr>to
                    <?php
                    $i++;
                    }
                        ?>
                </tbody>

            </table>
        </td>
    </tr>
  
</table>