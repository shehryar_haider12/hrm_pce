<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_Management extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID, COMPANY_ADMIN_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["resumeFolder"]			= RESUME_FOLDER;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->arrData["resumeFolderDownload"]	= str_replace('./', '', RESUME_FOLDER);
		$this->arrData["docFolder"]				= LM_ACTIVITY_DOCS_FOLDER;
		$this->arrData["docFolderShow"]			= str_replace('./', '', LM_ACTIVITY_DOCS_FOLDER);
		$this->arrData["empDocFolder"]			= EMP_DOCS_FOLDER;
		$this->arrData["empDocFolderShow"]		= str_replace('./', '', EMP_DOCS_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		$this->arrData['strHierarchy'] = $this->employee->getHierarchyWithMultipleAuthorities($this->userEmpNum);
		
		$this->arrData['skipParams'] = array(
												'list_employees'
											);
		
		if(!in_array($this->currentAction, $this->arrData['skipParams'])) {
			
			$this->employeeID = (int)$this->uri->segment(3);
			
			if(!$this->employeeID) {
				$this->employeeID = $this->userEmpNum;
			}
						
			$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID), false);
			
			if($this->employeeID == $this->userEmpNum) {
				$this->arrData['ownUser'] = true;
			}
			
			if($this->userRoleID == COMPANY_ADMIN_ROLE_ID && $this->arrData['arrEmployee']['emp_location_id'] != $this->userLocationID) {
				redirect($this->baseURL . '/message/access_denied');
			}
		
			$arrEmpSupervisors = getEmpSupervisors($this->employeeID);
			
			if($this->currentAction == 'resignation') {
				if(
					(($this->userRoleID == EMPLOYEE_ROLE_ID && !$this->arrData['ownUser']) || 
					(!$this->arrData['ownUser'] && !in_array($this->userEmpNum, $arrEmpSupervisors))) &&
					(!isAdmin($this->userRoleID) && count(array_diff($arrEmpSupervisors, explode(',', $this->arrData['strHierarchy']))) == count($arrEmpSupervisors))
					) {
						$arrAuthorities = $this->employee->getResignationStepAuthorities();
						if(count($arrAuthorities)) {
							if(!in_array($this->userEmpNum, $arrAuthorities)) {
								redirect($this->baseURL . '/message/access_denied');
								exit;
							}
						} else {
							redirect($this->baseURL . '/message/access_denied');
							exit;
						}
					}
		
			} else {
				if(
					(($this->userRoleID == EMPLOYEE_ROLE_ID && !$this->arrData['ownUser']) || 
					(!$this->arrData['ownUser'] && !in_array($this->userEmpNum, $arrEmpSupervisors))) &&
					(!isAdmin($this->userRoleID) && count(array_diff($arrEmpSupervisors, explode(',', $this->arrData['strHierarchy']))) == count($arrEmpSupervisors))
					) {
						redirect($this->baseURL . '/message/access_denied');
						exit;
					} else if(!isAdmin($this->userRoleID) && !$this->arrData['ownUser']) {
										
						$this->arrData['canWrite'] = NO;
						$this->arrData['canDelete'] = NO;
						
					}
			}
		}		
		
		if(!count($this->arrData['arrEmployee'])) {
			$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
			$this->arrData['empJobCategoryID'] = $this->arrData['arrEmployee']['emp_job_category_id'];
		}
		
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'employee_management/index', $this->arrData);
		$this->template->render();
		
	}
	
	public function add_employee() {
		$this->load->model('model_user_management', 'user_management', true);
		if(!isAdmin($this->userRoleID)) {
				# SET LOG
				redirect($this->baseURL . '/message/access_denied');
				exit;
		}
		#################################### FORM VALIDATION START ####################################
		$this->form_validation->set_rules('empTitle', 'Title', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('empReligion', 'Religion', '');
		$this->form_validation->set_rules('empFirstName', 'First Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('empLastName', 'Last Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('empEmail', 'Email', 'trim|valid_email|xss_clean');
		$this->form_validation->set_rules('empDOB', 'Date of Birth', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empGender', 'Gender', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('empMaritalStatus', 'Marital Status', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('empNICNo', 'NIC/EmiratesID Number', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('empReligion', 'Religion', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empAddress', 'Local Address', 'trim|xss_clean');
		$this->form_validation->set_rules('empPermAddress', 'Permanent Address', 'trim|xss_clean');
		$this->form_validation->set_rules('empHomePhone', 'Home Phone Number', 'trim|xss_clean|callback_strValidate[empHomePhone]');
		$this->form_validation->set_rules('empCellPhone', 'Cell Phone Number', 'trim|xss_clean|callback_strValidate[empCellPhone]');
		$this->form_validation->set_rules('empRoleID', 'Role', 'trim|required|numeric|xss_clean');
		
		
		$this->form_validation->set_rules('empCompany', 'Company', 'trim|numeric|xss_clean');
		//empID changed from numeric to apha_dash_space
		$this->form_validation->set_rules('empID', 'Employee Id', 'trim|xss_clean');
		$this->form_validation->set_rules('empDepartment', 'Department', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empDesignation', 'Designation', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empWorkEmail', 'Work EMail', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('empJobLocation', 'Job Location', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empEmploymentStatus', 'Employment Status', 'trim																																	|xss_clean');
		$this->form_validation->set_rules('empJoiningDate', 'Joining Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empProbationEndDate', 'Probation End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empBasicSalary', 'Basic Salary', 'trim|required');
		
		if($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_CONFIRMED) {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|required|xss_clean');
		} else {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		}
		
		$this->form_validation->set_rules('empAnnualLeaves', 'Annual Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empSickLeaves', 'Sick Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empNICNo', 'Cnic No', 'required|trim');
		$this->form_validation->set_rules('empReligion', 'Select Religion', 'trim|xss_clean');
		$this->form_validation->set_rules('Country', 'Select Country', 'trim|xss_clean');
		$this->form_validation->set_rules('Nationality', 'Select Nationality', 'trim|xss_clean');
		#################################### FORM VALIDATION END ####################################
		//value changed to false
		if ($this->form_validation->run() == true) {
			
			if($this->employee->notExistingEmployee('emp_nic_num', $this->input->post("empNICNo"))) {
				
				$boolFileUp = true;
				
				#	PROFILE PICTURE
				$uploadPicConfig['upload_path'] 	= $this->arrData["pictureFolder"];
				$uploadPicConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp';
				$uploadPicConfig['max_size']		= '1024';
				$uploadPicConfig['max_filename']	= '100';
				$uploadPicConfig['encrypt_name']	= true;
	
				$this->load->library('upload');
				$this->upload->initialize($uploadPicConfig);
				
				$picFileName = $this->input->post("picFileName");
				
				if(!$this->upload->do_upload('empPicture')) {
					if(!empty($_FILES['empPicture']['name'])) {
						$error = array('error' => $this->upload->display_errors());	
						$this->arrData['error_message'] = $error['error'];	
						$boolFileUp = false;
					}
					
				} else {		
				
					$dataUpload = $this->upload->data();
					$picFileName = basename($dataUpload['file_name']);
					$arrImg = getimagesize($this->arrData["pictureFolder"] . basename($dataUpload['file_name']));
					$imgWidth = (int)$arrImg[0];
					$imgHeight = (int)$arrImg[1];
					
					$aspectRatio = round(($imgHeight / $imgWidth), 2);
						
					if($imgWidth > $imgHeight) {
						$newRatio = $imgWidth / $imgHeight;
						$newWidth = $imgHeight * $newRatio;
						$newHeight = $newWidth / $newRatio;
						
						if($newWidth > 300) {
							$newWidth = 300;
							$newHeight = $newWidth / $newRatio;
						}
						
					} else {
						$newRatio = 1.14;
						$newHeight = $imgWidth * $newRatio;
						$newWidth = $newHeight / $newRatio;
						
						if($newHeight > 300) {
							$newHeight = 300;
							$newWidth = $newHeight / $newRatio;
						}
					}
					$arrFileName = explode('.', basename($dataUpload['file_name']));
					$picFileName = $arrFileName[0] . '.' . $arrFileName[1];
					$resizeConfig['image_library'] 	= 'gd2';
					$resizeConfig['source_image']	= $this->arrData["pictureFolder"] . basename($dataUpload['file_name']);
					$resizeConfig['create_thumb'] 	= TRUE;
					$resizeConfig['maintain_ratio'] = TRUE;
					$resizeConfig['width']	 		= $newWidth;
					$resizeConfig['height']			= $newHeight;
					$this->load->library('image_lib', $resizeConfig);						
					$this->image_lib->resize();
					//unlink($this->arrData["pictureFolder"] . basename($dataUpload['file_name']));
				}
				
				if($boolFileUp) {
					
					if($picFileName != $this->input->post("picFileName")) {
						unlink($this->arrData["pictureFolder"] . $this->input->post("picFileName")); # DELETE PREVIOUS PROFILE PICTURE
					}
						
					$statusID = STATUS_ACTIVE; 
					
					$strFullName = str_replace('  ', ' ', $this->input->post("empFirstName") . ' ' . $this->input->post("empSecondName") . ' ' . $this->input->post("empLastName"));
					
					$arrValues = array(					
										'emp_title' => $this->input->post("empTitle"),
										'emp_first_name' => $this->input->post("empFirstName"),
										'emp_second_name' => $this->input->post("empSecondName"),
										'emp_last_name' => $this->input->post("empLastName"),
										'emp_full_name' => $strFullName,
										'emp_family_name' => $this->input->post("empFamilyName"),
										'emp_rpt_name' => $this->input->post("empRPTName"),
										'emp_passport_name' => $this->input->post("empPassportName"),
										//Removing validation By Abdul Hai
										'emp_passport_num' => $this->input->post("empPassportNo"),
										'emp_passport_expiry' => !empty($this->input->post("empPassportExpiry")) ? $this->input->post("empPassportExpiry") : '0000-00-00',
										'emp_email' => $this->input->post("empEmail"),
										'emp_dob' => $this->input->post("empDOB"),
										'emp_gender' => $this->input->post("empGender"),
										'emp_blood_group' => $this->input->post("empBloodGroup"),
										'emp_father_name' => $this->input->post("empFatherName"),
										'emp_mother_name' => $this->input->post("empMotherName"),
										'emp_spouse_name' => $this->input->post("empSpouseName"),
										'emp_religion_id' => $this->input->post("empReligion"),
										'emp_marital_status' => $this->input->post("empMaritalStatus"),
										'emp_nic_num' => $this->input->post("empNICNo"),
										'emp_permanent_address' => $this->input->post("empPermAddress"),
										'emp_country_id' => $this->input->post("Country"),
										'emp_nationality_id' => $this->input->post("Nationality"),
										'emp_home_telephone' => $this->input->post("empHomePhone"),
										'emp_mobile' => $this->input->post("empCellPhone"),
										'emp_code' => $this->input->post("empCode"),
										'emp_ip_num' => $this->input->post("empIP"),
										'emp_work_email' => $this->input->post("empWorkEmail"),
										'emp_code' => $this->input->post("empID"),
										'emp_company_id' => $this->input->post("empCompany"),
										'emp_job_category_id' => $this->input->post("empDepartment"),
										'emp_designation' => $this->input->post("empDesignation"),
										'emp_location_id' => $this->input->post("empJobLocation"),
										'emp_ot_eligibility' => $this->input->post("empOT"),
										'emp_ip_num' => $this->input->post("empIPNum"),
										'emp_work_email' => $this->input->post("empWorkEmail"),
										'emp_authority_id' => (int)$this->input->post("empSupervisor"),
										'emp_currency_id' => !empty($this->input->post("empCurrency")) ? $this->input->post("empCurrency") : 0,
										'emp_employment_type' => $this->input->post("empEmploymentType"),
										'emp_employment_status' => $this->input->post("empEmploymentStatus"),
										'emp_joining_date' => $this->input->post("empJoiningDate"),
										'emp_probation_end_date' => $this->input->post("empProbationEndDate"),
										'emp_confirmation_date' => $this->input->post("empConfirmationDate"),
										'emp_basic_salary' => $this->input->post("empBasicSalary"),
										// 'emp_total_salary' => $this->input->post("empTotalSalary"),
										// 'emp_visa_company_id' => $this->input->post("empVisaCompany"),
										// 'emp_visa_issue_date' => $this->input->post("empVisaIssueDate"),
										// 'emp_visa_expiry_date' => $this->input->post("empVisaExpiryDate"),
										'emp_nic_issue_date' => $this->input->post("empNICIssueDate"),
										'emp_nic_expiry_date' => $this->input->post("empNICExpiryDate"),
										'emp_labour_card_issue_date' => $this->input->post("empLabourIssueDate"),
										'emp_labour_card_expiry_date' => $this->input->post("empLabourExpiryDate"),
										// 'emp_sponsor_id' => $this->input->post("empSponsor"),
										'emp_annual_leaves' => $this->input->post("empAnnualLeaves"),
										'emp_sick_leaves' => $this->input->post("empSickLeaves"),									
										'emp_age' => $this->input->post("empAge"),									
										'emp_place_of_birth' => $this->input->post("empPlaceOfBirth"),									
										'emp_languages' => $this->input->post("empLanguages"),									
										'emp_expenses' => $this->input->post("empExpenses"),									
										'emp_willing_location' => $this->input->post("empWillingLocation"),									
										'emp_children_married' => $this->input->post("empMarriedChildren"),									
										'emp_local_address' => $this->input->post("empAddress"),
										'emp_permanent_address' => $this->input->post("empPermenantAddress"),									
										// 'emp_union_council' => $this->input->post("empUnionCouncil"),									
										// 'emp_domicile' => $this->input->post("empDomicile"),									
										'emp_father_alive' => $this->input->post("empFatherAlive"),									
										'emp_father_occupation' => $this->input->post("empFatherOccupation"),									
										'emp_father_designation' => $this->input->post("empFatherDesignation"),									
										'emp_father_organization' => $this->input->post("empFatherOrganization"),			
										'emp_region_id' => $this->input->post("empRegionType"),						
										// 'emp_quran' => $this->input->post("empQuran"),									
										// 'emp_total_brother' => $this->input->post("empTotalBrother"),									
										// 'emp_total_chachu' => $this->input->post("empTotalChachu"),									
										// 'emp_total_mamu' => $this->input->post("empTotalMamu"),									
										// 'emp_sect' => $this->input->post("empSect"),									
										// 'emp_smoke' => $this->input->post("empSmoke"),									
										// 'emp_disease' => $this->input->post("empDisease"),									
										// 'emp_legal_case' => $this->input->post("empLegalCase"),									
										// 'emp_party_member' => $this->input->post("empPartyMember"),									
										// 'emp_academic_qualification' => $this->input->post("empAcademicQualification"),									
										// 'emp_technical_qualification' => $this->input->post("empTechnicalQualification"),									
										// 'emp_educational_certificate' => $this->input->post("empEducationalCertificate"),									
										// 'emp_experience_certificate' => $this->input->post("empExperienceCertificate"),									
										'emp_status' => $statusID,
										'created_by' => $this->userEmpNum,
										'created_date' => date($this->arrData["dateTimeFormat"])		
										);
										
					if(trim($picFileName) != '') {
						$arrValues['emp_photo_name'] = $picFileName;
					}
					
					$empID = $this->employee->saveValues(TABLE_EMPLOYEE, $arrValues);
					
					$arrSupervisors = $this->input->post("empSupervisor");
					
					if(count($arrSupervisors)) {
						foreach($arrSupervisors as $strKey => $strValue) {
							$arrValues = array(					
										'emp_id' => $empID,
										'supervisor_emp_id' => (int)$strValue,
										'created_by' => $this->userEmpNum,
										'created_date' => date($this->arrData["dateTimeFormat"])		
										);
										
							$this->employee->saveValues(TABLE_EMPLOYEE_SUPERVISORS, $arrValues);
						}
					}
					
					# NEW USER
					
					$strEmail = explode('@', $this->input->post("empWorkEmail"));
					$userName = trim($strEmail[0]);
					
					if(strlen($userName) <= 0) {
						$userName = str_replace(' ', '', strtolower(substr($this->input->post("empFirstName"), 0, 1) . $this->input->post("empLastName") . $empID));
					}
					
					$passPhrase = randomString(8);
					
					$arrValues = array(
							'user_role_id' => $this->input->post("empRoleID"),
							'employee_id' => $empID,
							'user_name' => $userName,
							'plain_password' => $passPhrase,
							'password' => md5($passPhrase),
							'user_status' => $statusID,
							'created_by' => $this->userEmpNum,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);								
					
					$this->user_management->saveValues(TABLE_USER, $arrValues);
								
					# SET LOG
					debugLog("Added a New Employee: [EmpID/UserName: ".$empID."/".$userName."]");
					
					# SHOOT EMAIL
					$strEmail = $this->input->post("empWorkEmail");
					if(!empty($strEmail)) {
						
						$arrValues = array(
											'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
											'[EMPLOYEE_NAME]' 			=> $strFullName,
											'[EMPLOYEE_DASHBOARD_LINK]' => $this->baseURL,
											'[EMPLOYEE_USERNAME]' 		=> $userName,
											'[EMPLOYEE_PASSWORD]' 		=> $passPhrase,
											'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'retrieve_login_details_emp.html');
						
						$this->sendEmail(
											$arrTo = array(
															$strEmail
															), 										# RECEIVER DETAILS
											'Account Details' . EMAIL_SUBJECT_SUFFIX,				# SUBJECT
											$emailHTML												# EMAIL HTML MESSAGE
										);
					}
					
					$this->session->set_flashdata('success_message', 'Employee Added Successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/list_employees');
					
					exit;
				}
			} else {
				//if emp id error uncomment next line and comment 2nd next line
				//redirect($this->baseURL . '/' . $this->currentController . '/list_employees');
				 $this->arrData['validation_error_message'] = '<p>Identification Number is already registered</p>';
			}	
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['Religions'] = $this->configuration->getReligions();
		$this->arrData['Genders'] = $this->config->item('genders');
		$this->arrData['Salutations'] = $this->config->item('salutations');
		$this->arrData['maritalStatuses'] = $this->config->item('marital_status');
		$this->arrData['bloodGroups'] = $this->config->item('blood_groups');
		$this->arrData['employmentTypes'] = $this->config->item('employment_types');
		$this->arrData['userRoles'] = $this->user_management->getUserRoles();		
		
		$this->arrData['empWorkShifts'] = $this->employee->populateWorkShifts();
		$this->arrData['empSupervisors'] = $this->employee->getSupervisors();
		$this->arrData['empRegions'] = $this->employee->getRegions();
		$this->arrData['empEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['empDepartments'] = $this->employee->populateDepartments();
		$this->arrData['empCompanies'] = $this->configuration->getCompanies();
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['empSponsors'] = $this->configuration->getSponsors();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'employee_management/add_employee', $this->arrData);
		$this->template->render();
	}
	
	public function save_employee($employeeID = 0) {
		$this->load->model('model_user_management', 'user_management', true);
		// print_r($this->arrData['arrEmployee'] );exit;
		
		$this->arrData['ownUser'] = false;
		$this->arrData['record'] = array();
		
		$employeeID = (int)$employeeID;
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		$arrWhere = array();
		
		if($employeeID) {			
			$arrWhere = array(
							'e.emp_id' => $employeeID
							);
			
			if($this->userEmpNum == $employeeID) {
				$this->arrData['ownUser'] = true;
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				
				# SET LOG
				
				redirect($this->baseURL . '/message/access_denied');
				exit;
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
			
		if($this->input->post('changePW') == YES) {
			
			$changePassWord = true;
			$this->form_validation->set_rules('newPassWord', 'New Password', 'trim|required|min_length[8]|matches[confirmPassWord]|xss_clean');
			$this->form_validation->set_rules('confirmPassWord', 'Confirm Password', 'trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('currentPassWord', 'Current Password', 'trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('oldPassWord', 'Old Password', 'trim|required|min_length[8]|md5|callback_checkOldPassword[currentPassWord]|xss_clean');
			
		} else {
			$this->form_validation->set_rules('empTitle', 'Title', 'trim|required|xss_clean');
			$this->form_validation->set_rules('empFirstName', 'First Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
			$this->form_validation->set_rules('empLastName', 'Last Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
			$this->form_validation->set_rules('empEmail', 'Email', 'trim|valid_email|xss_clean');
			$this->form_validation->set_rules('empDOB', 'Date of Birth', 'trim|required|xss_clean');
			$this->form_validation->set_rules('empGender', 'Gender', 'trim|required|xss_clean');
			$this->form_validation->set_rules('empMaritalStatus', 'Marital Status', 'trim|xss_clean');
			$this->form_validation->set_rules('empNICNo', 'NIC/EmiratesID Number', 'trim|xss_clean');
			$this->form_validation->set_rules('empPassportNo', 'Passport Number', 'trim|xss_clean');
			$this->form_validation->set_rules('empAge', 'Age', 'trim|numeric|xss_clean');
			$this->form_validation->set_rules('empplaceofbirth', 'Place of birth', 'trim|xss_clean');
			$this->form_validation->set_rules('empAddress', 'Local Address', 'trim|xss_clean');
			$this->form_validation->set_rules('empPermAddress', 'Permanent Address', 'trim|xss_clean');
			$this->form_validation->set_rules('empHomePhone', 'Home Phone Number', 'trim|xss_clean|callback_strValidate[empHomePhone]');
			$this->form_validation->set_rules('empCellPhone', 'Cell Phone Number', 'trim|xss_clean|callback_strValidate[empCellPhone]');
			$this->form_validation->set_rules('Country', 'Select Country', 'trim|xss_clean');
			$this->form_validation->set_rules('Nationality', 'Select Nationality', 'trim|xss_clean');
			$this->form_validation->set_rules('empHomePhone', 'Select empHomePhone', 'trim|xss_clean');
			$this->form_validation->set_rules('empCellPhone', 'Select empCellPhone', 'trim|xss_clean');
			$this->form_validation->set_rules('empMarriedChildren', 'Select empMarriedChildren', 'trim|xss_clean');
			$this->form_validation->set_rules('empReligion', 'Select Religion', 'trim|xss_clean');
			$this->form_validation->set_rules('Country', 'Select Country', 'trim|xss_clean');
			$this->form_validation->set_rules('Nationality', 'Select Nationality', 'trim|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			if($changePassWord) {
				
				$arrValues = array(
									'plain_password' => $this->input->post("newPassWord"),
									'password' => md5($this->input->post("newPassWord")),
									'modified_by' => $this->userEmpNum,
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
											
				$this->user_management->saveValues(TABLE_USER, $arrValues, array('user_id' => $this->userID));
				
				# FOR ENTRY UPDATE IN DISCUSSION FORM
				
				$dataJSON = array();
				$arrUserName = explode('@', $this->arrData['arrEmployee']['emp_work_email']);
				$userName = trim($arrUserName[0]);
				
				if(!empty($userName)) {
					
					$dataJSON['data'] = json_encode(array(
															'username' => $userName,
															'password' => $this->input->post("newPassWord"),
															'email' => $this->arrData['arrEmployee']['emp_work_email']
														)
													);
					$curlRequest = curl_init();
					curl_setopt_array($curlRequest, array(
															CURLOPT_RETURNTRANSFER => 1,
															CURLOPT_URL => DISCUSSION_FORUM_API_USER_EDIT,
															CURLOPT_HTTPHEADER => array(
																						'IS_FROM_HRMS: 1'
																						),
															CURLOPT_POST => sizeof($dataJSON),
															CURLOPT_POSTFIELDS => $dataJSON,
															CURLOPT_CONNECTTIMEOUT => 30
														)
									);
					$curlResponse = curl_exec($curlRequest);
					curl_close($curlRequest);
					
					/*
					$arrResponse = json_decode($curlResponse);
					if((int)$arrResponse['error']) {
						...
					}
					*/
				}
						
				# SET LOG
				debugLog("Employee Changed Password: [EmpID: ".$employeeID."]");
					
				$this->session->set_flashdata('success_message', 'Password Updated Successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
				exit;

				// $this->session->set_flashdata('success_message', 'Your Password is modified Successfully');
				// redirect($this->baseURL . '/' . $this->currentController);
				
			} else if((int)$this->arrData['canWrite']) {
				
				if($this->employee->notExistingEmployee('emp_nic_num', $this->input->post("empNICNo"), $employeeID)) {
					
					$boolFileUp = true;
					
					#	PROFILE PICTURE
					$uploadPicConfig['upload_path'] 	= $this->arrData["pictureFolder"];
					$uploadPicConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp';
					$uploadPicConfig['max_size']		= '1024';
					$uploadPicConfig['max_filename']	= '100';
					$uploadPicConfig['encrypt_name']	= true;
		
					$this->load->library('upload');
					$this->upload->initialize($uploadPicConfig);
					
					$picFileName = $this->input->post("picFileName");
					$delPhoto 	 = (int)$this->input->post("delPhoto");
					
					if(!$delPhoto) {
						if(!$this->upload->do_upload('empPicture')) {
							if(!empty($_FILES['empPicture']['name'])) {
								$error = array('error' => $this->upload->display_errors());	
								$this->arrData['error_message'] = $error['error'];	
								$boolFileUp = false;
							}
							
						} else {		
						
							$dataUpload 	= $this->upload->data();
							$picFileName 	= basename($dataUpload['file_name']);
							$arrImg 		= getimagesize($this->arrData["pictureFolder"] . basename($dataUpload['file_name']));
							$imgWidth 		= (int)$arrImg[0];
							$imgHeight 		= (int)$arrImg[1];
							
							$aspectRatio 	= round(($imgHeight / $imgWidth), 2);
																	
							if($imgWidth > $imgHeight) {
								$newRatio 	= $imgWidth / $imgHeight;
								$newWidth 	= $imgHeight * $newRatio;
								$newHeight 	= $newWidth / $newRatio;
								
								if($newWidth > 300) {
									$newWidth 	= 300;
									$newHeight 	= $newWidth / $newRatio;
								}
								
							} else {
								$newRatio = 1.14;
								$newHeight	= $imgWidth * $newRatio;
								$newWidth 	= $newHeight / $newRatio;
								
								if($newHeight > 300) {
									$newHeight 	= 300;
									$newWidth 	= $newHeight / $newRatio;
								}
							}								
										
							$arrFileName = explode('.', basename($dataUpload['file_name']));
							$picFileName = $arrFileName[0] . '.' . $arrFileName[1];
						
							$resizeConfig['image_library'] 	= 'gd2';
							$resizeConfig['source_image']	= $this->arrData["pictureFolder"] . basename($dataUpload['file_name']);
							$resizeConfig['create_thumb'] 	= TRUE;
							$resizeConfig['maintain_ratio'] = TRUE;
							$resizeConfig['width']			= $newWidth;
							$resizeConfig['height']			= $newHeight;
							$this->load->library('image_lib', $resizeConfig);						
							$this->image_lib->resize();
				//			unlink($this->arrData["pictureFolder"] . basename($dataUpload['file_name']));
						}
					} else {
						$picFileName = '';
					}
					
					if($boolFileUp) {
						
						if($picFileName != $this->input->post("picFileName")) {
							unlink($this->arrData["pictureFolder"] . $this->input->post("picFileName")); # DELETE PREVIOUS PROFILE PICTURE
						}
							
						$statusID = (int)$this->input->post("empStatus"); 
						if($statusID == STATUS_INACTIVE) {
							$statusID = STATUS_INACTIVE_VIEW;
						}
						
						$strFullName = str_replace('  ', ' ', $this->input->post("empFirstName") . ' ' . $this->input->post("empSecondName") . ' ' . $this->input->post("empLastName"));
						$arrValues = array(
										'emp_title' => $this->input->post("empTitle"),
										'emp_first_name' => $this->input->post("empFirstName"),
										'emp_second_name' => $this->input->post("empSecondName"),
										'emp_last_name' => $this->input->post("empLastName"),
										'emp_full_name' => $strFullName,
										'emp_family_name' => $this->input->post("empFamilyName"),
										'emp_rpt_name' => $this->input->post("empRPTName"),
										'emp_passport_name' => $this->input->post("empPassportName"),
										'emp_passport_num' => $this->input->post("empPassportNo"),
										'emp_passport_expiry' => $this->input->post("empPassportExpiry"),
										'emp_email' => $this->input->post("empEmail"),
										'emp_dob' => $this->input->post("empDOB"),
										'emp_gender' => $this->input->post("empGender"),
										'emp_blood_group' => $this->input->post("empBloodGroup"),
										'emp_father_name' => $this->input->post("empFatherName"),
										'emp_father_alive' => $this->input->post("empFatheralive"),
										'emp_mother_name' => $this->input->post("empMotherName"),
										'emp_spouse_name' => $this->input->post("empSpouseName"),
										'emp_religion_id' => $this->input->post("empReligion"),
										'emp_age' => $this->input->post("empAge"),
										'emp_place_of_birth' => $this->input->post("empplaceofbirth"),
										'emp_marital_status' => $this->input->post("empMaritalStatus"),
										'emp_nic_num' => $this->input->post("empNICNo"),
										'emp_local_address' => $this->input->post("empAddress"),
										'emp_permanent_address' => $this->input->post("empPermAddress"),
										'emp_country_id' => $this->input->post("Country"),
										'emp_nationality_id' => $this->input->post("Nationality"),
										'emp_home_telephone' => $this->input->post("empHomePhone"),
										'emp_mobile' => $this->input->post("empCellPhone"),
										'emp_region_id' => $this->input->post("empRegionType"),
										'emp_children_married' => $this->input->post("empMarriedChildren")
									);
						if($employeeID) {
							$arrValues['modified_by'] = $this->userEmpNum;
							$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
						} else {
							$arrValues['created_by'] = $this->userEmpNum;
							$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
						}
						
						if($statusID == STATUS_DELETED) {
							$arrValues['deleted_by'] = $this->userEmpNum;
							$arrValues['deleted_date'] = date($this->arrData["dateTimeFormat"]);
						}
						
						if(isAdmin($this->userRoleID)) {
							$arrValues['emp_status'] = $statusID;
						}
											
						if(trim($picFileName) != '' || $delPhoto) {
							$arrValues['emp_photo_name'] = $picFileName;
						}
						
						$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
						
						if(isAdmin($this->userRoleID)) { #	IF ONLY SUPER ADMIN THEN CHANGE USER TABLE VALUES
							$this->user_management->saveValues(	TABLE_USER, 
																array('user_status' => $statusID), 		# WHAT TO UPDATE
																array('employee_id' => $employeeID)		# WHERE
																);
						}	
						# SET LOG
						debugLog("Employee Updated: [EmpID: ".$employeeID."]");
						$this->session->set_flashdata('success_message', 'Information saved successfully');
						redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
						exit;
					}
				} else {
					$this->arrData['validation_error_message'] = '<p>Identification Number is already registered</p>';
				}	
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		// print_r($employeeID);exit;
		if($employeeID) {
			$this->arrData['record'] = $this->arrData['arrEmployee'];
		}
		// print_r($this->arrData['record']);
		// exit;
		
		# CODE FOR PAGE CONTENT
		$this->arrData['employeeID'] = $employeeID;
		$this->arrData['empRegions'] = $this->employee->getRegions();
	//	print_r($this->arrData['empRegions']);exit;
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['Religions'] = $this->configuration->getReligions();
		$this->arrData['Genders'] = $this->config->item('genders');
		$this->arrData['Salutations'] = $this->config->item('salutations');
		$this->arrData['maritalStatuses'] = $this->config->item('marital_status');
		$this->arrData['bloodGroups'] = $this->config->item('blood_groups');
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/save_employee', $this->arrData);
		$this->template->render();
	}
	
	public function list_employees($pageNum = 1) {
				
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		$notApplied = false;
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE, array('emp_id' => (int)$this->input->post("record_id")), 'emp_status', STATUS_INACTIVE_VIEW)) {
					echo NO; exit;
				} else {
					$this->employee->saveValues(TABLE_USER, array('user_status' => STATUS_INACTIVE_VIEW), array('employee_id' => (int)$this->input->post("record_id")));
					
					# SET LOG
					debugLog("Employee set as Deleted/InActive: [EmpID: ".(int)$this->input->post("record_id")."]");
					
					echo YES; exit;
				}								
			}
		}
		##########################
		
		$arrWhere = array();
		$arrWhere['e.emp_id > '] = 1; # WEB ADMINISTRATOR
		if ($this->input->post()) {
			
			if($this->input->post("empCode")) {
				$arrWhere['e.emp_code'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$this->arrData['empIP'] = $this->input->post("empIP");
			}
			
			if($this->input->post("empDesignation")) {
				$arrWhere['e.emp_grade_id'] = $this->input->post("empDesignation");
				$this->arrData['empDesignation'] = $this->input->post("empDesignation");
			}
			
			if($this->input->post("empMaritalStatus")) {
				$arrWhere['e.emp_marital_status'] = $this->input->post("empMaritalStatus");
				$this->arrData['empMaritalStatus'] = $this->input->post("empMaritalStatus");
			}
			
			if($this->input->post("empGender")) {
				$arrWhere['e.emp_gender'] = $this->input->post("empGender");
				$this->arrData['empGender'] = $this->input->post("empGender");
			}
			
			if($this->input->post("empWorkShift")) {
				$arrWhere['e.emp_work_shift_id'] = $this->input->post("empWorkShift");
				$this->arrData['empWorkShift'] = $this->input->post("empWorkShift");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['e.emp_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empAgeFrom")) {
				$arrWhere['e.emp_dob <= '] = date('Y-m-d', strtotime('-' . (int)$this->input->post("empAgeFrom") . ' years'));
				$this->arrData['empAgeFrom'] = $this->input->post("empAgeFrom");
			}
			
			if($this->input->post("empAgeTo")) {
				$arrWhere['e.emp_dob >= '] = date('Y-m-d', strtotime('-' . (int)$this->input->post("empAgeTo") . ' years'));
				$this->arrData['empAgeTo'] = $this->input->post("empAgeTo");
			}
			
			if($this->input->post("empDepartment")) {
				$arrWhere['e.emp_job_category_id'] = $this->input->post("empDepartment");
				$this->arrData['empDepartment'] = $this->input->post("empDepartment");
			}
			
			if($this->input->post("eduDegree")) {
				$arrWhere['edu.edu_level_id'] = $this->input->post("eduDegree");
				$this->arrData['eduDegree'] = $this->input->post("eduDegree");
			}
			
			if($this->input->post("eduMajor")) {
				$arrWhere['edu.edu_major_id'] = $this->input->post("eduMajor");
				$this->arrData['eduMajor'] = $this->input->post("eduMajor");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("empStatus")) {
				if($this->input->post("empStatus") == 'none') {
					$arrWhere['e.emp_employment_status = '] = STATUS_INACTIVE_VIEW;
					$this->arrData['empStatus'] = $this->input->post("empStatus");
				} else if($this->input->post("empStatus") == 'active') {
					$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
					$arrWhere['e.emp_employment_status > '] = STATUS_INACTIVE_VIEW;
					$this->arrData['empStatus'] = $this->input->post("empStatus");
				} else if($this->input->post("empStatus") == 'resigned') {
					$arrWhere['e.emp_employment_status > '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
					$this->arrData['empStatus'] = $this->input->post("empStatus");
				} else {
					$arrWhere['e.emp_employment_status'] = $this->input->post("empStatus");
					$this->arrData['empStatus'] = $this->input->post("empStatus");
				}
			}
			
			if($this->input->post("empEmail")) {
				$arrWhere['e.emp_email'] = $this->input->post("empEmail");
				$this->arrData['empEmail'] = $this->input->post("empEmail");
			}
			
			if($this->input->post("empWorkEmail")) {
				$arrWhere['e.emp_work_email'] = $this->input->post("empWorkEmail");
				$this->arrData['empWorkEmail'] = $this->input->post("empWorkEmail");
			}
			
			if($this->input->post("empFatherNICNo")) {
				$arrWhere['e.emp_father_nic_num'] = $this->input->post("empFatherNICNo");
				$this->arrData['empFatherNICNo'] = $this->input->post("empFatherNICNo");
			}
			
			if($this->input->post("empName")) {
				$arrWhere['e.emp_name'] = $this->input->post("empName");
				$this->arrData['empName'] = $this->input->post("empName");
			}
			
			if($this->input->post("empJoiningDate")) {
				$arrWhere['e.emp_joining_date'] = $this->input->post("empJoiningDate");
				$this->arrData['empJoiningDate'] = $this->input->post("empJoiningDate");
			}
			
			if($this->input->post("empDocument") && $this->input->post("empDocument") != '') {
				$arrWhere['ed.doc_id'] = $this->input->post("empDocument");
				$this->arrData['empDocument'] = $this->input->post("empDocument");
			}
			
			if($this->input->post("empCountry") && $this->input->post("empCountry") != '') {
				$arrWhere['e.emp_location_id'] = $this->input->post("empCountry");
				$this->arrData['empCountry'] = $this->input->post("empCountry");
			}
			
			if($this->input->post("empEmploymentType") && $this->input->post("empEmploymentType") != '') {
				$arrWhere['e.emp_employment_type'] = $this->input->post("empEmploymentType");
				$this->arrData['empEmploymentType'] = $this->input->post("empEmploymentType");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
		
		if(!isAdmin($this->userRoleID) && !isset($arrWhere['es.supervisor_emp_id in '])) {			
			$arrWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
		}
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$arrWhere['e.emp_location_id'] = $this->userLocationID;
			$this->arrData['empCountry'] = $this->userLocationID;
		}
		
		/*if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}*/
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && isAdmin($this->userRoleID)) {
			
			$selColumns = $this->input->post('empColumns');
			$arrRecords = $this->employee->getEmployees($arrWhere);
			$strCSV = "";
			
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				
				$arrRecord = array();
				if(!$ind) {
					foreach($arrRecords[$ind] as $cName => $cVal) {
						if(in_array($cName, $selColumns)) {
							$strCSV .=  $cName . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrRecords[$ind] as $cName => $cVal) {
					if(in_array($cName, $selColumns)) {
						$arrRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrRecord)) , "\\x00-\\x20")  . "\n"; 
				
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=employee_data.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$this->arrData['totalRecordsCount'] = $this->employee->getTotalEmployees($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->employee->getEmployees($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListEmployees');
		
		# CODE FOR PAGE CONTENT
		$this->arrData['Genders'] = $this->config->item('genders');		
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => STATUS_ACTIVE));
		$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => STATUS_ACTIVE));
		if(!isAdmin($this->userRoleID)) {
			$empJobType = $this->configuration->getValues(TABLE_GRADES, 'job_type', array('grade_id' => $this->arrData['arrEmployee']['emp_grade_id']));
			$empJobType = $empJobType[0]['job_type'];
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE, 'job_type' => $empJobType));
		} else {
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE));
		}
		$this->arrData['empMarkets'] = $this->configuration->getValues(TABLE_LOCATION, '*', array('location_type_id in ' => '(1,2)','location_status' => STATUS_ACTIVE ));
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$this->arrData['arrWorkShifts'] = $this->configuration->getWorkShifts();
		$this->arrData['maritalStatuses'] = $this->config->item('marital_status');
		
		if(!isAdmin($this->userRoleID)) {		
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_code >' => ZERO));
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] = $finalResultSupervisors;
		
		if(isAdmin($this->userRoleID)) {
			foreach($this->arrData['arrRecords'][0] as $cName => $cVal) {
				$this->arrData['arrTblColumns'][] =  $cName;
			}
		}
		
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['employmentTypes'] = $this->config->item('employment_types');
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'employee_management/list_employees', $this->arrData);
		$this->template->render();
	}
	
	public function employment_details($employeeID = 0) {
		
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->arrData['record'] = array();
		
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$arrWhere = array(
						'emp_id' => $employeeID
						);
		
		if(!isAdmin($this->userRoleID)) {
			
			# SET LOG
			
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		#################################### FORM VALIDATION START ####################################	
		
		$this->form_validation->set_rules('empCompany', 'Company', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empID', 'Employee Id', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empDepartment', 'Department', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empDesignation', 'Designation', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empWorkEmail', 'Work EMail', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('empJobLocation', 'Job Location', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empEmploymentStatus', 'Employment Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empJoiningDate', 'Joining Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empProbationEndDate', 'Probation End Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empBasicSalary', 'Basic Salary', 'trim|required|xss_clean');
		
		if($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_CONFIRMED) {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		} else {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		}
		
		$this->form_validation->set_rules('empAnnualLeaves', 'Annual Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empSickLeaves', 'Sick Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empFlexiLeaves', 'Flexi Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empEduLeaves', 'Educational/Training Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empMaternityLeaves', 'Maternity/Paternity Leaves', 'trim|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ######################################
		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post() && (int)$this->arrData['canWrite'])
			{
				$arrValues = array(
								'emp_code' => $this->input->post("empID"),
								'emp_company_id' => $this->input->post("empCompany"),
								'emp_job_category_id' => $this->input->post("empDepartment"),
								'emp_designation' => $this->input->post("empDesignation"),
								'emp_location_id' => $this->input->post("empJobLocation"),
								'emp_ot_eligibility' => $this->input->post("empOT"),
								'emp_ip_num' => $this->input->post("empIPNum"),
								'emp_work_email' => $this->input->post("empWorkEmail"),
								'emp_basic_salary' => (int)$this->input->post("empBasicSalary"),
								// 'emp_total_salary' => $this->input->post("empTotalSalary"),
								'emp_currency_id' => $this->input->post("empCurrency"),
								'emp_employment_type' => $this->input->post("empEmploymentType"),
								'emp_employment_status' => $this->input->post("empEmploymentStatus"),
								'emp_joining_date' => $this->input->post("empJoiningDate"),
								'emp_probation_end_date' => $this->input->post("empProbationEndDate"),
								'emp_confirmation_date' => $this->input->post("empConfirmationDate"),
								//'emp_visa_company_id' => $this->input->post("empVisaCompany"),
								// 'emp_visa_issue_date' => $this->input->post("empVisaIssueDate"),
								// 'emp_visa_expiry_date' => $this->input->post("empVisaExpiryDate"),
								'emp_nic_issue_date' => $this->input->post("empNICIssueDate"),
								'emp_nic_expiry_date' => $this->input->post("empNICExpiryDate"),
								// 'emp_labour_card_issue_date' => $this->input->post("empLabourIssueDate"),
								// 'emp_labour_card_expiry_date' => $this->input->post("empLabourExpiryDate"),
								'emp_expenses' => $this->input->post("empExpenses"),
								// 'emp_sponsor_id' => $this->input->post("empSponsor"),
								'emp_annual_leaves' => $this->input->post("empAnnualLeaves"),
								'emp_sick_leaves' => $this->input->post("empSickLeaves"),
								'emp_flexi_leaves' => $this->input->post("empFlexiLeaves"),
								'emp_edu_leaves' => $this->input->post("empEduLeaves"),
								'emp_maternity_leaves' => $this->input->post("empMaternityLeaves"),
								'emp_father_occupation' => $this->input->post("empFatherOcupation"),
								'emp_father_organization' => $this->input->post("empFatherOrganization"),
								'emp_father_designation' => $this->input->post("empFatherDesignation"),
								'emp_languages' => $this->input->post("empPreferredLanguages"),
								// 'emp_maternity_leaves' => $this->input->post("empPreferredLanguages"),
								);
										
				if($employeeID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				} else {
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				}
								
				if($employeeID) {
					$result = $this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
					
					if((int)$this->input->post("empEmploymentStatus") < STATUS_EMPLOYEE_SEPARATED) {
						$userStatusID = STATUS_ACTIVE;
					} else {
						$userStatusID = STATUS_INACTIVE_VIEW;
					}
					
					$this->employee->saveValues(	TABLE_USER, 
													array('user_status' => $userStatusID), 	# WHAT TO UPDATE
													array('employee_id' => $employeeID)		# WHERE
												);
				}
				
				$arrSupervisors = $this->input->post("empSupervisor");
					
				if(count($arrSupervisors)) {
					
					$this->master->deleteValue(TABLE_EMPLOYEE_SUPERVISORS, null, array('emp_id' => $employeeID));
					
					foreach($arrSupervisors as $strKey => $strValue) {
						$arrValues = array(					
									'emp_id' => $employeeID,
									'supervisor_emp_id' => (int)$strValue,
									'created_by' => $this->userEmpNum,
									'created_date' => date($this->arrData["dateTimeFormat"])		
									);
									
						$this->employee->saveValues(TABLE_EMPLOYEE_SUPERVISORS, $arrValues);
					}
				}
								
				if($result)
				{
					# SHOOT EMAIL
					
					$strEmail = $this->input->post("empWorkEmail");
					if(!empty($strEmail)) {
						
						$arrUserDetails = $this->configuration->getValues(TABLE_USER, ' user_name, plain_password ', array('employee_id' => $employeeID));
						$arrUserDetails = $arrUserDetails[0];
						
						/*$arrUserName = explode('@', $strEmail);
						$userName = trim($arrUserName[0]);
						
						$userName = trim($arrUserDetails['user_name']);
						
						if($userName != $arrUserDetails['user_name']) {
							$userResult = $this->configuration->saveValues(TABLE_USER, array('user_name' => $userName), array('employee_id' => $employeeID));
						}*/
				
						$arrValues = array(
											'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
											'[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
											'[EMPLOYEE_DASHBOARD_LINK]' => $this->baseURL,
											'[EMPLOYEE_USERNAME]' 		=> $arrUserDetails['user_name'],
											'[EMPLOYEE_PASSWORD]' 		=> $arrUserDetails['plain_password'],
											'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'details_update_notification_and_login_details.html');
						
						$this->sendEmail(
											$arrTo = array(
															$strEmail
															), 															# RECEIVER DETAILS
											'Employment Details Update Notification' . EMAIL_SUBJECT_SUFFIX,			# SUBJECT
											$emailHTML																	# EMAIL HTML MESSAGE
										);
					}
					
					# SET LOG
					debugLog("Employment Details Updated: [EmpID/Code: ".$employeeID."/".$this->input->post("empID")."]");
					
					$this->session->set_flashdata('success_message', 'Information saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
					exit;
				} else {
					$this->arrData['error_message'] = 'Data not saved, try again';
				}
				
				
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$this->arrData['record'] = $this->arrData['arrEmployee'];
		}
		// print_r($this->arrData['record']);
		// exit;
		# CODE FOR PAGE CONTENT
		//$this->arrData['empGrades'] = $this->employee->populateGrades();
		$this->arrData['empWorkShifts'] = $this->employee->populateWorkShifts();
		$this->arrData['empSupervisors'] = $this->employee->getSupervisors(array('e.emp_id != ' => $employeeID));
		// print_r($this->arrData['empSupervisors']);exit;
		$this->arrData['empEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['empDepartments'] = $this->employee->populateDepartments();
		$this->arrData['empCompanies'] = $this->configuration->getCompanies();
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['empSponsors'] = $this->configuration->getSponsors();
		$this->arrData['employmentTypes'] = $this->config->item('employment_types');
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/employment_detail', $this->arrData);
		$this->template->render();
	}
	
	public function employment_status($employeeID = 0) {
		
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->arrData['record'] = array();
		
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$arrWhere = array(
						'emp_id' => $employeeID
						);
		
		if(!isAdmin($this->userRoleID)) {
			
			# SET LOG
			
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		#################################### FORM VALIDATION START ####################################	
		
		//$this->form_validation->set_rules('empID', 'Employee Id', 'trim|numeric');
		//$this->form_validation->set_rules('empIPNum', 'IP Number', 'trim|numeric');
		//$this->form_validation->set_rules('empSupervisor', 'Supervisor', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empEmploymentStatus', 'Employment Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empJoiningDate', 'Joining Date', 'trim|xss_clean');
		
		if($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_CONFIRMED) {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		} else {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		}
		
		if(($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_SEPARATED)||($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_TERMINATED))
		{
			$this->form_validation->set_rules('empEndDate', 'End Date', 'trim|xss_clean');
			$this->form_validation->set_rules('empReason', 'Reason', 'trim|required|xss_clean');
			$this->form_validation->set_rules('empRemarks', 'Remarks', 'trim|xss_clean');
		}
		
		#################################### FORM VALIDATION END ######################################
		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post())
			{
				$arrValues = array(
								//'emp_code' => $this->input->post("empID"),
								//'emp_ip_num' => $this->input->post("empIPNum"),
								//'emp_authority_id' => $this->input->post("empSupervisor"),
								'emp_employment_status' => $this->input->post("empEmploymentStatus"),
								'emp_joining_date' => $this->input->post("empJoiningDate"),
								'emp_confirmation_date' => $this->input->post("empConfirmationDate")	
								);
				
				if(($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_SEPARATED)||($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_TERMINATED))
				{					
					$arrValues['emp_end_date'] = $this->input->post("empEndDate");
					$arrValues['emp_end_reason'] = $this->input->post("empReason");
					$arrValues['emp_end_remarks'] = $this->input->post("empRemarks");
					$arrValues['emp_status'] = STATUS_INACTIVE_VIEW;
					$arrUserValues['user_status'] = STATUS_INACTIVE_VIEW;
					$arrUserValues['modified_by'] = $this->userEmpNum;
					$arrUserValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
					
				}
						
				if($employeeID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				} else {
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				}
								
				if($employeeID) {
					$result = $this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
				}
				
				if(($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_SEPARATED)||($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_TERMINATED))
				{
					$userResult = $this->employee->saveValues(TABLE_USER, $arrUserValues, array('employee_id' => $employeeID));
				}
				
				if($result)
				{
					$this->session->set_flashdata('success_message', 'Information saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
					exit;
				} else {
					$this->arrData['error_message'] = 'Data not saved, try again';
				}
				
				
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$this->arrData['record'] = $this->arrData['arrEmployee'];
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['empEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/employment_status', $this->arrData);
		$this->template->render();
	}
	
	public function emp_education_history($eduEmployeeID = 0, $eduID = 0) {
		
		$eduEmployeeID = (int)$eduEmployeeID;
		if(!$eduEmployeeID) {
			$eduEmployeeID = $this->employeeID;
		}
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_EDUCATION, array('edu_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('eduEmployeeID', 'Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('eduLevel', 'Qualification Level', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('eduInstitute', 'Educational Institute', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('eduMajors', 'Majors', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('eduGPA', 'GPA', 'trim|xss_clean');
		$this->form_validation->set_rules('eduTotalMarks', 'Total Marks', 'trim|xss_clean');
		$this->form_validation->set_rules('eduObtMarks', 'Obtained Marks', 'trim|xss_clean');
		$this->form_validation->set_rules('eduobt_eng_math_Marks', 'Obtained Marks In English / Math', 'trim|xss_clean');
		$this->form_validation->set_rules('edu_compul_Sub', 'Compulsory Subjects', 'trim|xss_clean');
		$this->form_validation->set_rules('eduEnded', 'Passing Year', 'trim|required|min_length[4]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$eduID = (int)$eduID;
		$arrWhere['e.emp_id'] = $eduEmployeeID;
		$this->arrData['record'] = array();
		
		if($eduID) {			
			$arrWhere['e.edu_id'] = $eduID;
		}
		
		if ($this->form_validation->run() == true) {
			
			$arrValues = array(
								'emp_id' 				=> $this->input->post("eduEmployeeID"),
								'edu_level_id' 			=> $this->input->post("eduLevel"),
								'edu_institute' 		=> $this->input->post("eduInstitute"),
								'edu_major_id' 			=> $this->input->post("eduMajors"),
								'eduTotalMarks' 		=> ($this->input->post("eduTotalMarks") != FALSE) ? $this->input->post("eduTotalMarks") : NULL,
								'eduObtMarks' 			=> ($this->input->post("eduObtMarks") != FALSE) ? $this->input->post("eduObtMarks") : NULL,
								'eduobt_eng_math_Marks' => ($this->input->post("eduobt_eng_math_Marks") != FALSE) ? $this->input->post("eduobt_eng_math_Marks") : NULL,
								'edu_compul_Sub' 		=> ($this->input->post("edu_compul_Sub") != FALSE) ? $this->input->post("edu_compul_Sub") : NULL,
								'edu_gpa' 				=> ($this->input->post("eduGPA") != FALSE) ? $this->input->post("eduGPA") : NULL,
								'edu_ended' 			=> $this->input->post("eduEnded")
							);
								
			if($eduID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
				
			if($eduID) {
				$this->employee->saveValues(TABLE_EMPLOYEE_EDUCATION, $arrValues, $arrWhere);
			} else {
				$this->employee->saveValues(TABLE_EMPLOYEE_EDUCATION, $arrValues);
			}					
			
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);			
			$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, array('emp_id' => $eduEmployeeID));
			
			# SET LOG
			debugLog("Education History Updated: [EmpID: ".$eduEmployeeID."]");
			
			$this->session->set_flashdata('success_message', 'Education history saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $eduEmployeeID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EDUCATION RECORD
		if($eduID) {
			$this->arrData['record'] = $this->employee->getEducationalHistory($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['eduEmployeeID'] = $eduEmployeeID;
		$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => STATUS_ACTIVE));
		$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => STATUS_ACTIVE));
		$this->arrData['arrRecords'] = $this->employee->getEducationalHistory(array('e.emp_id' => $eduEmployeeID));
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/education_history', $this->arrData);
		$this->template->render();
	}
	
	public function emp_work_history($workEmployeeID = 0, $workID = 0) {
		
		$workEmployeeID = (int)$workEmployeeID;
		if(!$workEmployeeID) {
			$workEmployeeID = $this->employeeID;
		}
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_WORK_EXPERIENCE, array('work_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('workEmployeeID', 'Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('workCompany', 'Company Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('workJobTitle', 'Job Title', 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('workFrom', 'Starting Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('workTo', 'Work Ending Date', 'trim|xss_clean');
		$this->form_validation->set_rules('workDescription', 'Work Description', 'trim|max_length[1000]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$workID = (int)$workID;
		$arrWhere['emp_id'] = $workEmployeeID;
		$this->arrData['record'] = array();
		
		if($workID) {			
			$arrWhere['work_id'] = $workID;
		}
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'emp_id' => $this->input->post("workEmployeeID"),
								'work_company' => $this->input->post("workCompany"),
								'work_job_title' => $this->input->post("workJobTitle"),
								'work_from' => $this->input->post("workFrom"),
								'work_to' => $this->input->post("workTo"),
								'work_description' => $this->input->post("workDescription")
								);	
								
			if($workID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}							
								
			if($workID) {
				$this->employee->saveValues(TABLE_EMPLOYEE_WORK_EXPERIENCE, $arrValues, $arrWhere);
			} else {
				$this->employee->saveValues(TABLE_EMPLOYEE_WORK_EXPERIENCE, $arrValues);
			}
				
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);			
			$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, array('emp_id' => $workEmployeeID));
				
			# SET LOG
			debugLog("Work History Updated: [EmpID: ".$workEmployeeID."]");
			
			$this->session->set_flashdata('success_message', 'Work history saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $workEmployeeID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT WORK RECORD
		if($workID) {
			$this->arrData['record'] = $this->employee->getWorkHistory($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['workEmployeeID'] = $workEmployeeID;
		$this->arrData['arrRecords'] = $this->employee->getWorkHistory(array('emp_id' => $workEmployeeID));
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/work_history', $this->arrData);
		$this->template->render();
	}
	
	public function dependents($employeeID = 0,$dependentID = 0) {
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$dependentID = (int)$dependentID;
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_DEPENDENTS, array('dependent_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}		
		
		if(!$employeeID) {
			$employeeID = $this->userEmpNum;
		}
		
		$arrWhere['emp_id'] = $employeeID;
		
		if($dependentID) {			
			$arrWhere['dependent_id'] = $dependentID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('employeeID', 'Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('dependentName', 'Dependent Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('dependentRelationship', 'Dependent Relationship', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dependentDOB', 'Dependent DOB', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'emp_id' => $this->input->post("employeeID"),
								'dependent_name' => $this->input->post("dependentName"),
								'dependent_relationship' => $this->input->post("dependentRelationship"),
								'dependent_dob' => $this->input->post("dependentDOB")
								);	
								
			if($dependentID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
						
			if($dependentID) {
				$result = $this->employee->saveValues(TABLE_EMPLOYEE_DEPENDENTS, $arrValues, $arrWhere);
			} else {
				$result = $this->employee->saveValues(TABLE_EMPLOYEE_DEPENDENTS, $arrValues);
			}
			
			if($result)
			{
				$this->session->set_flashdata('success_message', 'Dependent saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
				exit;
			} else {
				$this->arrData['error_message'] = 'Data not saved, try again';
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR CURRENT WORK RECORD
		if($dependentID) {
			$this->arrData['record'] = $this->employee->getDependents($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrRecords'] = $this->employee->getDependents(array('emp_id' => $employeeID));
		$this->arrData['arrRelationships'] = $this->config->item('dependent_relationships');
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/dependents', $this->arrData);
		$this->template->render();
	}
	
	public function languages($employeeID = 0,$languageID = 0) {
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$languageID = (int)$languageID;
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_LANGUAGE, array('emp_lang_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}
		
		if(!$employeeID) {
			$employeeID = $this->userEmpNum;
		}
		
		$arrWhere['emp_id'] = $employeeID;
		
		if($languageID) {			
			$arrWhere['emp_lang_id'] = $languageID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('employeeID', 'Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('language', 'Language', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{
			$table = TABLE_EMPLOYEE_LANGUAGE;
			$column = 'language_id';
			$Where = array(
						'emp_id' => $this->input->post("employeeID"),
						'language_id' => $this->input->post("language")
						);
			$checkResult = $this->employee->checkValues($table, $Where, $column, $languageID);
			if($checkResult == YES)
			{
				$arrValues = array(
									'emp_id' => $this->input->post("employeeID"),
									'language_id' => $this->input->post("language")
									);	
									
				if($languageID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				} else {
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				}
							
				if($languageID) {
					$result = $this->employee->saveValues(TABLE_EMPLOYEE_LANGUAGE, $arrValues, $arrWhere);
				} else {
					$result = $this->employee->saveValues(TABLE_EMPLOYEE_LANGUAGE, $arrValues);
				}
				
				if($result)
				{
					$this->session->set_flashdata('success_message', 'Language saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
					exit;
				} else {
					$this->arrData['error_message'] = 'Data not saved, try again';
				}
			}
			else
			{
				$this->arrData['error_message'] = 'The selected Language already exists in Employee Language List.';
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT WORK RECORD
		if($languageID) {
			$this->arrData['record'] = $this->employee->getLanguages($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrRecords'] = $this->employee->getLanguages(array('emp_id' => $employeeID));
		$this->arrData['arrLanguages'] = $this->employee->populateLanguages();
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/languages', $this->arrData);
		$this->template->render();
	}
	
	public function emergency_contacts($employeeID = 0,$contactID = 0) {
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$contactID = (int)$contactID;
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_EMERGENCY_CONTACTS, array('emc_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}		
		
		if(!$employeeID) {
			$employeeID = $this->userEmpNum;
		}
		
		$arrWhere['emp_id'] = $employeeID;
		
		if($contactID) {			
			$arrWhere['emc_id'] = $contactID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('employeeID', 'Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('emergencyContactName', 'Contact Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('emergencyContactRelationship', 'Contact Relationship', 'trim|required|xss_clean');
		$this->form_validation->set_rules('emergencyContactAge', 'Age', 'trim|required|max_length[3]|xss_clean');
		$this->form_validation->set_rules('emergencyContactcompany', 'Company Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('emergencyContactDesignation', 'Designation', 'trim|required|xss_clean');
		$this->form_validation->set_rules('emergencyContactNumber', 'Contact Number', 'trim|required|xss_clean|min_length[7]|callback_strValidate[empEmergencyContact]');
		
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'emp_id' => $this->input->post("employeeID"),
								'emc_name' => $this->input->post("emergencyContactName"),
								'emc_relationship' => $this->input->post("emergencyContactRelationship"),
								'emc_age' => $this->input->post("emergencyContactAge"),
								'emc_company' => $this->input->post("emergencyContactcompany"),
								'emc_designation' => $this->input->post("emergencyContactDesignation"),
								'emc_contact_number' => $this->input->post("emergencyContactNumber")
								);	
								
			if($contactID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
						
			if($contactID) {
				$result = $this->employee->saveValues(TABLE_EMPLOYEE_EMERGENCY_CONTACTS, $arrValues, $arrWhere);
			} else {
				$result = $this->employee->saveValues(TABLE_EMPLOYEE_EMERGENCY_CONTACTS, $arrValues);
			}
			
			if($result)
			{
				$this->session->set_flashdata('success_message', 'Emergency Contact saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
				exit;
			} else {
				$this->arrData['error_message'] = 'Data not saved, try again';
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR CURRENT WORK RECORD
		if($contactID) {
			$this->arrData['record'] = $this->employee->getEmergencyContacts($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrRecords'] = $this->employee->getEmergencyContacts(array('emp_id' => $employeeID));
		$this->arrData['arrRelationships'] = $this->config->item('dependent_relationships');
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/emergency_contacts', $this->arrData);
		$this->template->render();
	}
	
	public function document_list($employeeID = 0) {
		
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$arrWhere['ed.emp_id'] = $employeeID;		
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {
				$arrRecord = $this->employee->getEmpDocumentList(array('ed.doc_id' => (int)$this->input->post("record_id"), 'ed.emp_id' => $employeeID));
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_DOCUMENTS, array('doc_id' => (int)$this->input->post("record_id"), 'emp_id' => $employeeID))) {
					echo NO; exit;
				} else {
					unlink($this->arrData["empDocFolder"] . $arrRecord[0]['doc_file']);
					echo YES; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('docType', 'Document Type', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('docTitle', 'Title', 'trim|required|xss_clean');
		if (empty($_FILES['docFile']['name'])) {
			$this->form_validation->set_rules('docFile', 'Document', 'required');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		# CODE FOR POPULATING PAGE CONTENT
		if($this->form_validation->run() == true && $this->arrData['canWrite']) {
			
			#	DOCUMENT UPLOADING
			$uploadConfig['upload_path'] 	= $this->arrData["empDocFolder"];
			$uploadConfig['allowed_types'] 	= '*'; //'jpg|jpeg|png|bmp|doc|docs|pdf|xls|xlsx';
			$uploadConfig['max_size']		= '12240';
			$uploadConfig['max_filename']	= '100';
			$uploadConfig['encrypt_name']	= true;

			$this->load->library('upload');
			$this->upload->initialize($uploadConfig);
			
			$docFileName = '';
			
			if(!$this->upload->do_upload('docFile')) {
				if(!empty($_FILES['docFile']['name'])) {
					$error = array('error' => $this->upload->display_errors());	
					$this->arrData['error_message'] = $error['error'];
				}					
			} else {				
				$dataUpload = $this->upload->data();
				$docFileName = basename($dataUpload['file_name']);
										
				$arrValues = array(
									'emp_id' => $employeeID,
									'doc_type_id' => $this->input->post("docType"),
									'doc_title' => $this->input->post("docTitle"),
									'doc_file' => $docFileName,
									'created_by' => $this->userEmpNum,
									'created_date' => date($this->arrData["dateTimeFormat"])
								);
			
				$resultID = $this->employee->saveValues(TABLE_EMPLOYEE_DOCUMENTS, $arrValues);
				
				if($resultID) {					
					$this->session->set_flashdata('success_message', 'Document Uploaded Successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
					exit;
				}
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		$this->arrData['arrRecords'] = $this->employee->getEmpDocumentList(array('ed.emp_id' => $employeeID));
		$this->arrData['arrDocTypes'] = $this->employee->getValues(TABLE_EMPLOYEE_DOCUMENT_TYPES);
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/document_checklist', $this->arrData);
		$this->template->render();
	}
	
	public function documents() {
		
		$arrWhere = array();
		
		if(!isAdmin($this->userRoleID)) {
			$arrEmpDetail = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
			$arrWhereDocs = array('ed.emp_id' => (int)$this->input->post("empID"), 'e.emp_location_id' => $arrEmpDetail['emp_location_id']);
			$arrEmpWhere = array('e.emp_location_id' => $arrEmpDetail['emp_location_id'], 'e.emp_status' => 1);
		} else {
			$arrWhereDocs = array('ed.emp_id' => (int)$this->input->post("empID"));
			$arrEmpWhere = array();
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('empID', 'Employee', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		# CODE FOR POPULATING PAGE CONTENT
		if($this->form_validation->run() == true) {
			$this->arrData['arrRecords'] = $this->employee->getEmpDocumentList($arrWhereDocs);
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		$_POST['sort_field'] = 'e.emp_full_name';
		$_POST['sort_order'] = 'ASC';
		
		$arrEmployees = $this->employee->getEmployees($arrEmpWhere);
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,');
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		unset($_POST['sort_field']);
		unset($_POST['sort_order']);
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'employee_management/documents', $this->arrData);
		$this->template->render();
	}
	
	public function benefits($empID = 0, $benefitID = 0) {
		
		$empID = (int)$empID;
		if(!$empID) {
			$empID = $this->employeeID;
		}
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_BENEFITS, array('benefit_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {				
					echo YES; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('empID', 'Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('benefitTitle', 'Benefit', 'trim|required|xss_clean');
		$this->form_validation->set_rules('benefitDetail', 'Entitlement', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$benefitID = (int)$benefitID;
		$arrWhere['emp_id'] = $empID;
		$this->arrData['record'] = array();
		
		if($benefitID) {			
			$arrWhere['benefit_id'] = $benefitID;
		}
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'emp_id' => $this->input->post("empID"),
								'benefit_title' => $this->input->post("benefitTitle"),
								'benefit_detail' => $this->input->post("benefitDetail")
								);	
								
			if($benefitID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				$this->employee->saveValues(TABLE_EMPLOYEE_BENEFITS, $arrValues, $arrWhere);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$this->employee->saveValues(TABLE_EMPLOYEE_BENEFITS, $arrValues);
			}
				
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);			
			$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, array('emp_id' => $workEmployeeID));
			
			$this->session->set_flashdata('success_message', 'Benefit saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $empID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT WORK RECORD
		if($benefitID) {
			$this->arrData['record'] = $this->employee->getBenefits($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['empBenefits'] = $this->config->item('employee_benefits');
		$this->arrData['empID'] = $empID;
		$this->arrData['arrRecords'] = $this->employee->getBenefits(array('emp_id' => $empID));
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/benefits', $this->arrData);
		$this->template->render();
	}
	
	public function salary_details($empID = 0) {
		
		$this->load->model('model_payroll_management', 'payroll', true);
		$this->load->model('model_attendance_management');
		
		$empID = (int)$empID;
		if(!$empID) {
			$empID = $this->employeeID;
		}
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('payrollMonth', 'Month', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollYear', 'Year', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$this->arrData['record'] = array();
		
		if ($this->form_validation->run() == true) {
			
			$arrWhere['ep.emp_id'] = $empID;
			$arrWhere['ep.payroll_month'] = $this->input->post("payrollMonth");
			$arrWhere['ep.payroll_year'] = $this->input->post("payrollYear");
			
			$this->arrData['record'] = $this->payroll->getPayrolls($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
			$this->arrData['arrLocations'] = $this->configuration->getLocations();
			
			if($this->input->post("txtExport") == 1) {
				
				$arrCompany = $this->configuration->getCompanies(array('company_id' => $this->arrData['record']['payroll_company_id']));
				$arrJobCategory = $this->configuration->getJobCategories(array('job_category_id' => $this->arrData['arrEmployee']['emp_job_category_id']));
				$arrLocation = $this->configuration->getLocations(array('location_id' => $arrCompany[0]['company_currency_id']));
				$arrBank = $this->configuration->getBanks(array('bank_id' => $this->arrData['arrEmployee']['emp_salary_bank_id']));
				$basic = (($this->arrData['record']['payroll_earning_basic'] / 165) * 100);
				// print_r($basic);exit;
				$totalEarning = (int)$basic + 
								(int)$this->arrData['record']['payroll_earning_housing'] +
								(int)$this->arrData['record']['payroll_earning_transport'] +
								(int)$this->arrData['record']['payroll_earning_utility'] +
								(int)$this->arrData['record']['payroll_earning_travel'] +
								(int)$this->arrData['record']['payroll_earning_health'] +
								(int)$this->arrData['record']['payroll_earning_fuel'] +
								(int)$this->arrData['record']['payroll_earning_mobile'] +
								(int)$this->arrData['record']['payroll_earning_medical_relief'] +
								(int)$this->arrData['record']['payroll_earning_bonus'] +
								(int)$this->arrData['record']['payroll_earning_annual_leave_encashment'] +
								(int)$this->arrData['record']['payroll_earning_claims'] +
								(int)$this->arrData['record']['payroll_earning_commission'] +
								(int)$this->arrData['record']['payroll_earning_annual_ticket'] +
								(int)$this->arrData['record']['payroll_earning_gratuity'] +
								(int)$this->arrData['record']['payroll_earning_survey_expense'] +
								(int)$this->arrData['record']['payroll_earning_settlement'] +
								(int)$this->arrData['record']['payroll_earning_misc'] +
								(int)$this->arrData['record']['payroll_earning_food_allowance'];
								
					$totalDeduction = 
								(int)$this->arrData['record']['payroll_deduction_tax'] + 
								(int)$this->arrData['record']['payroll_deduction_pf'] + 
								(int)$this->arrData['record']['payroll_deduction_loan'] + 
								(int)$this->arrData['record']['payroll_deduction_eobi'] + 
								(int)$this->arrData['record']['payroll_deduction_telephone'] + 
								(int)$this->arrData['record']['payroll_deduction_misc'] +
								//NEW FIELDS
								(int)$this->arrData['record']['payroll_deduction_mobile'] +
								(int)$this->arrData['record']['payroll_deduction_jeans'] +
								(int)$this->arrData['record']['payroll_deduction_smoking'] +
								(int)$this->arrData['record']['payroll_deduction_wrong_time'] +
								(int)$this->arrData['record']['payroll_deduction_cashier_checking'] +
								(int)$this->arrData['record']['payroll_deduction_guard_attention'] +
								(int)$this->arrData['record']['payroll_deduction_dvr_off'] +
								(int)$this->arrData['record']['payroll_deduction_laptop_in_branch'] +
								(int)$this->arrData['record']['payroll_deduction_no_response'] +
								(int)$this->arrData['record']['payroll_deduction_branch_incharge'] +
								(int)$this->arrData['record']['payroll_deduction_CCFLA'] +
								(int)$this->arrData['record']['payroll_deduction_dealing_without'] +
								(int)$this->arrData['record']['payroll_deduction_misbehave'] +
								(int)$this->arrData['record']['payroll_deduction_open_close'] +
								(int)$this->arrData['record']['payroll_deduction_pending_work'] +
								(int)$this->arrData['record']['payroll_deduction_late_coming'] +
								(int)$this->arrData['record']['cctc_approval_deduction'] +
								(int)$this->arrData['record']['compliance_approval_deduction'] +
								(int)$this->arrData['record']['payroll_deductsion_MMB_loan'];
								  
				// $objFormatter = new NumberFormatter("en", NumberFormatter::SPELLOUT);
				
				$strEarningsHTML = '';
				
				if((int)$this->arrData['record']['net_salary']) {
					$strEarningsHTML .= '<tr>
                	<td>Basic</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($basic, 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_housing']) {
					$strEarningsHTML .= '<tr>
                	<td>Housing Allowance</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_housing'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_transport']) {
					$strEarningsHTML .= '<tr>
                	<td>Transport Allowance</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_transport'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_utility']) {
					$strEarningsHTML .= '<tr>
                	<td>Utility Allowance</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_utility'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_travel']) {
					$strEarningsHTML .= '<tr>
                	<td>Travel Expenses</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_travel'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_survey_expense']) {
					$strEarningsHTML .= '<tr>
                	<td>Travel/Survey Expense</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_survey_expense'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_commission']) {
					$strEarningsHTML .= '<tr>
                	<td>Commission</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_commission'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_health']) {
					$strEarningsHTML .= '<tr>
                	<td>Health Allowance</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_health'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_fuel']) {
					$strEarningsHTML .= '<tr>
                	<td>Fuel</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_fuel'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_mobile']) {
					$strEarningsHTML .= '<tr>
                	<td>Mobile/Telephone</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_mobile'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_medical_relief']) {
					$strEarningsHTML .= '<tr>
                	<td>Medical Relief</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_medical_relief'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_bonus']) {
					$strEarningsHTML .= '<tr>
                	<td>Bonus</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_bonus'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_annual_ticket']) {
					$strEarningsHTML .= '<tr>
                	<td>Annual Ticket</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_annual_ticket'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_claims']) {
					$strEarningsHTML .= '<tr>
                	<td>Claims</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_claims'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_annual_leave_encashment']) {
					$strEarningsHTML .= '<tr>
                	<td>Annual Leave Encashment</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_annual_leave_encashment'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_gratuity']) {
					$strEarningsHTML .= '<tr>
                	<td>Gratuity</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_gratuity'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_settlement']) {
					$strEarningsHTML .= '<tr>
                	<td>Final Settlement</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_settlement'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_earning_misc']) {
					$strEarningsHTML .= '<tr>
                	<td>Others</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_misc'], 2) . '</td>
                </tr>';
				}
				
				$strDeductionsHTML = '';
				
				if((int)$this->arrData['record']['payroll_deduction_tax']) {
					$strDeductionsHTML .= '<tr>
                	<td>Income Tax</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_tax'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_deduction_pf']) {
					$strDeductionsHTML .= '<tr>
                	<td>Provident Fund</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_pf'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_deduction_loan']) {
					$strDeductionsHTML .= '<tr>
                	<td>Salary Advance/Loan Recovery</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_loan'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_deduction_eobi']) {
					$strDeductionsHTML .= '<tr>
                	<td>EOBI</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_eobi'], 2) . '</td>
                </tr>';
				}
				
				if((int)$this->arrData['record']['payroll_deduction_telephone']) {
					$strDeductionsHTML .= '<tr>
                	<td>Telephone Expense</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_telephone'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_misc']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Misc</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_misc'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_mobile']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Mobile</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_mobile'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_jeans']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Jeans</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_jeans'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_smoking']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Smoking</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_smoking'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_wrong_time']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Wrong Time</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_wrong_time'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_cashier_checking']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Cashier Checking</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_cashier_checking'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_guard_attention']) {
					$strDeductionsHTML .= '<tr>
                	<td>Guard Deduction</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_guard_attention'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_dvr_off']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Dvr Off</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_dvr_off'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_laptop_in_branch']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Laptop in Branch</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_laptop_in_branch'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_no_response']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction No Response</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_no_response'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_branch_incharge']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction By Incharge</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_branch_incharge'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_CCFLA']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction DDFLA</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_CCFLA'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_dealing_without']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Dealing Without</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_dealing_without'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_misbehave']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction MisBehave</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_misbehave'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_open_close']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Open Close</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_open_close'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_pending_work']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Pending Work</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_pending_work'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deduction_late_coming']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction Late Comming</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_late_coming'], 2) . '</td>
                </tr>';
				}
				if((int)$this->arrData['record']['payroll_deductsion_MMB_loan']) {
					$strDeductionsHTML .= '<tr>
                	<td>Deduction MMB Loan</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deductsion_MMB_loan'], 2) . '</td>
                </tr>';
				}
				
				$strDeductionsHTML .= '<tr>
                	<td>Others</td>
                	<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_misc'], 2) . '</td>
                </tr>';
				// print_r('adsfasdf');exit;
				$present = $this->model_attendance_management->totalPresent($empID,$leave_month,$this->arrData['record']['payroll_year']);
				$Absent = $this->model_attendance_management->totalAbsent($empID,$leave_month,$this->arrData['record']['payroll_year']);
				$Leave = $this->model_attendance_management->totalLeaves($empID,$leave_month,$this->arrData['record']['payroll_year']);
				$arrValues = array(
					'[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
					'[OFFICE_ADDRESS]' 			=> $arrCompany[0]['company_name'] . '<br />&nbsp;&nbsp;' . $arrCompany[0]['company_address'],
					'[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),
					'[MONTH]' 					=> date('F', mktime(0, 0, 0, (int)$this->arrData['record']['payroll_month'], 10)),
					'[YEAR]' 						=> $this->arrData['record']['payroll_year'],
					'[EMPLOYEE_CODE]' 			=> $this->arrData['arrEmployee']['emp_code'],
					'[EMPLOYEE_DESIGNATION]' 		=> $this->arrData['arrEmployee']['emp_designation'],
					'[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
					'[EMPLOYEE_JOINING_DATE]' 	=> date(SHOW_DATE_TIME_FORMAT, strtotime($this->arrData['arrEmployee']['emp_joining_date'])),
					'[EMPLOYEE_DEPARTMENT]' 		=> $arrJobCategory[0]['job_category_name'],
					'[EMPLOYEE_CURRENCY]' 		=> $arrLocation[0]['location_currency_code'],
					'[EMPLOYEE_BANK]' 			=> $arrBank[0]['bank_name'],
					'[EMPLOYEE_BRANCH]' 			=> $this->arrData['arrEmployee']['emp_salary_bank_branch'],
					'[EMPLOYEE_ACCOUNT_NUMBER]' 	=> $this->arrData['arrEmployee']['emp_salary_bank_account_number'],
					'[BASIC_SALARY]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($basic, 2),
					'[HOUSING]' 					=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_housing'], 2),
					'[TRANSPORT]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_transport'], 2),
					'[GROSS_EARNING]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalEarning, 2),
					'[EARNINGS]' 					=> $strEarningsHTML,
					'[PRESENT]' 					=> $present[0]['present'],
					'[ABSENT]' 					=> $Absent,
					'[LEAVE]' 					=> $Leave,
					'[TOTAL_EARNINGS]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalEarning, 2),
					'[LOAN]' 						=> (int)$this->arrData['record']['payroll_deduction_loan'],
					'[TAXDEDUCTIONS]' 			=> (int)$this->arrData['record']['payroll_deduction_tax'],
					'[FINEDEDUCTIONS]' 			=> (int)$this->arrData['record']['payroll_deduction_misc'],
					'[TOTAL_DEDUCTIONS]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalDeduction, 2),
					'[NET_SALARY]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format((int)($totalEarning - $totalDeduction), 2),
				  //   '[AMOUNT_IN_WORDS]' 			=> strtoupper($objFormatter->format((int)($totalEarning - $totalDeduction)))
					'[AMOUNT_IN_WORDS]' 			=> strtoupper($this->convertNumberToWord($totalEarning - $totalDeduction)),
				  );
					
					$strHTML = getHTML($arrValues, 'payslip.html');
				  
				  require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

				  $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
				  // set document information
				  $pdf->SetCreator(PDF_CREATOR);
				  $pdf->SetAuthor(PDF_AUTHOR);
				  $pdf->setCellHeightRatio(2);
				  $pdf->setPrintHeader(false);
				  $pdf->setPrintFooter(false);
				  $pdf->SetFontSize(8);				  
				  // set margins
				  $pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);				  
				  // set auto page breaks
				  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
				  // set image scale factor
				  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
				  // set some language-dependent strings (optional)
				  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
					  require_once(dirname(__FILE__).'/lang/eng.php');
					  $pdf->setLanguageArray($l);
				  }
				  
				  // add a page
				  $pdf->AddPage();			  
				  // output the HTML content
				  $pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
				  $pdf->lastPage();
				  $pdfFileName = 'Salary_Slip_' . str_replace(' ', '_', $this->arrData['arrEmployee']['emp_full_name']) . '_' . date('F', mktime(0, 0, 0, (int)$this->input->post("payrollMonth"), 10)) . '_' . $this->input->post("payrollYear") . '.pdf';
				  $pdf->Output($pdfFileName, 'D');
				  
				  header('Content-Type: text/doc');
				  header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
				  header('Cache-Control: max-age=0');
				  readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
				  
				  exit;
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		$this->arrData['arrMonths'] = $this->config->item('months');
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/salary', $this->arrData);
		$this->template->render();
	}
	
	public function strValidate($strValue, $strField) {
		
		if($strField == 'empHomePhone' || $strField == 'empCellPhone' || $strField == 'empEmergencyContact') { 
			
			if($strField == 'empHomePhone') {
				$strMsg = 'Home Phone Number field must contain a valid phone number';
			} else if($strField == 'empCellPhone') {
				$strMsg = 'Cell Phone Number field must contain a valid phone number';
			 }else if($strField == 'empEmergencyContact') {
				 $strMsg = 'Emergency Contact Number field must contain a valid phone number';
			 }
			$this->form_validation->set_message('strValidate', $strMsg);
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'nic') {
			$this->form_validation->set_message('strValidate', 'NIC Number field must only contain numbers.');
			return (preg_match("/^[0-9]*$/u", $strValue)) ? true : false;
		}
		return false;
		
 	}
	
	public function checkOldPassword($strCurrentPassword, $strOldPassword) {
		if($strCurrentPassword == $this->input->post($strOldPassword)) {
			return true;
		}
		$this->form_validation->set_message('checkOldPassword', 'Incorrect Old Password');
		return false;
	}
	
	public function checkedCheckbox() {
    	
		if ($this->input->post('chequeUndertaking')) {
			return true;
 		}		
 		$this->form_validation->set_message('checkedCheckbox', 'Please accept the clearance check undertaking');
 		return false;
 	}

	 public function transfer_history($employeeID = 0,$TransferID = 0) {
		$employeeID = (int)$employeeID;
		$this->load->model('model_transfer_management');

		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$contactID = (int)$TransferID;
		
		if(!count($this->arrData['arrEmployee'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_EMPLOYEE_EMERGENCY_CONTACTS, array('emc_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}		
		
		// if(!$employeeID) {
		// 	$employeeID = $this->userEmpNum;
		// }
		
		$arrWhere['emp_id'] = $employeeID;
		
		if($contactID) {			
			$arrWhere['emc_id'] = $contactID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('transferFrom', 'Transfer From', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('transferTo', 'Transfer To', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('transferReason', 'Reason', 'trim|required|xss_clean');
		
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			#	SUPPORTING DOCUMENT (IF ANY)
			$uploadPicConfig['upload_path'] 	= $this->arrData["docFolder"];
			$uploadPicConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp|doc|docs|pdf';
			$uploadPicConfig['max_size']		= '2048';
			$uploadPicConfig['max_filename']	= '100';
			$uploadPicConfig['encrypt_name']	= true;

			$this->load->library('upload');
			$this->upload->initialize($uploadPicConfig);
			
			$docFileName = '';
			
			if(!$this->upload->do_upload('TransferDoc')) {
				if(!empty($_FILES['TransferDoc']['name'])) {
					$this->arrData['error_message'] = $error['error'];
					$error = array('error' => $this->upload->display_errors());	
					
						//echo $error['error'] ;
				}					
			} else {				
				$dataUpload = $this->upload->data();
				$docFileName = basename($dataUpload['file_name']);
				//echo $docFileName;
			}
			// print_r($employeeID);exit;		
			$arrValues = array(
								'emp_id' => $employeeID,
								'transfer_from' => $this->input->post("transferFrom"),
								'transfer_to' => $this->input->post("transferTo"),
								'transfer_reason' => $this->input->post("transferReason"),
								'processed_date' => $this->input->post("transferDate"),
								'transfer_status' => 1,
								'created_by' => $this->userEmpNum,
							);
			
			if(!empty($docFileName)) {
				$arrValues['transfer_doc'] = $docFileName;
				
			}
			$arrValues['created_date'] 	= date($this->arrData["dateTimeFormat"]);
			// print_r($arrValues);
			// exit;
			$this->model_transfer_management->saveValues(TABLE_TRANSFERS, $arrValues);
			$this->session->set_flashdata('success_message', 'Application submitted successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/transfer_history'.$employeeID);
		
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR CURRENT WORK RECORD
		if($contactID) {
			$this->arrData['record'] = $this->employee->getEmergencyContacts($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		// $this->arrData['arrRecords'] = $this->employee->getEmergencyContacts(array('emp_id' => $employeeID));
		$this->arrData['listbranch'] = $this->employee->getEmergencyTransfer(array('emp_id' => $employeeID));
		// print_r($this->arrData['listbranch']);exit;
		$this->arrData['arrRecords'] 		= $this->model_transfer_management->getAllTransfersByEmployee($employeeID);
		// print_r($this->arrData['arrRecords']);
		// print_r($employeeID);
		// exit;
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/transfer_history', $this->arrData);
		$this->template->render();
	}

	function convertNumberToWord($num = false)
	{
		$num = str_replace(array(',', ' '), '' , trim($num));
		if(! $num) {
			return false;
		}
		$num = (int) $num;
		$words = array();
		$list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
			'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
		);
		$list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
		$list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
			'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
			'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
		);
		$num_length = strlen($num);
		$levels = (int) (($num_length + 2) / 3);
		$max_length = $levels * 3;
		$num = substr('00' . $num, -$max_length);
		$num_levels = str_split($num, 3);
		for ($i = 0; $i < count($num_levels); $i++) {
			$levels--;
			$hundreds = (int) ($num_levels[$i] / 100);
			$hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
			$tens = (int) ($num_levels[$i] % 100);
			$singles = '';
			if ( $tens < 20 ) {
				$tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
			} else {
				$tens = (int)($tens / 10);
				$tens = ' ' . $list2[$tens] . ' ';
				$singles = (int) ($num_levels[$i] % 10);
				$singles = ' ' . $list1[$singles] . ' ';
			}
			$words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
		} //end for loop
		$commas = count($words);
		if ($commas > 1) {
			$commas = $commas - 1;
		}
		return implode(' ', $words);
	}
}

/* End of file employee_management.php */
/* Location: ./application/controllers/employee_management.php */
