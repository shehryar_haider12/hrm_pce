<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compliance_Management extends Master_Controller {
	private $arrData 			= array();
	public $arrRoleIDs			= array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter 			= '-';
	private $employeeID 		= 0;
	private $employeeCode 		= 0;
	private $companyID 			= 0;
	
	public $dateFrom 			= 0;
	public $dateTo 				= 0;	
	public $maxLateAllowed 		= 5;
	public $halfOnLates 		= 7;
	public $fullOnLates 		= 8;
	public $fullHalfDayHours 	= 4.45;
	public $halfHalfDayHours 	= 4.15;
	public $fullHours 			= 8.45;
	public $halfdayHours 		= 8.15;
	public $secLate 			= 0;	// 0 MINUTES
	public $secHalfday 			= 1800;	// 30 MINUTES
	public $lateMinSeconds		= 900;	// 15 MINUTES
	public $lateMaxTime			= 0.25;
	
	public $startRamadan;
	public $endRamadan;	
	public $dateTimeChange;
	function __construct() {
		parent::__construct();
		$this->dateFrom 					= date('Y-m-d', mktime(0, 0, 0, date("m"), 01, date("Y")));
		$this->dateTo 						= date('Y-m-d', mktime(0, 0, 0, date("m"), date("t"), date("Y")));
		$this->load->model('model_compliance_management', 'employee', true);
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID, COMPANY_ADMIN_ROLE_ID,REGIONAL_MANAGER);
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["super_visor_roleid"] 	= SUPERVISOR_ROLE_ID;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["docFolder"]				= ATTENDANCE_TRANSFER_DOCS_FOLDER;
		$this->arrData["docFolderShow"]			= str_replace('./', '', ATTENDANCE_TRANSFER_DOCS_FOLDER);
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		$this->arrData['strHierarchy'] = $this->employee->getHierarchyWithMultipleAuthorities($this->userEmpNum);
		
		$this->arrData['skipParams'] = array(
												// 'compliance_deduction_details',
												// 'add_compliance_deduction',
												'list_compliance',
												'request_compliance',
											);	
		if(!in_array($this->currentAction, $this->arrData['skipParams'])) {
			$this->employeeID = (int)$this->input->post("empID");
			if(!(int)$this->employeeID) {
				$this->employeeID = (int)$this->uri->segment(3);
			}
			
			if(!(int)$this->employeeID) {
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
				$this->employeeID = $this->arrData['arrEmployee']['emp_id'];
				$this->employeeCode = $this->arrData['arrEmployee']['emp_code'];
				$this->companyID = $this->arrData['arrEmployee']['emp_company_id'];
			} else {
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID), false);
				$this->employeeID = $this->arrData['arrEmployee']['emp_id'];
				$this->employeeCode = $this->arrData['arrEmployee']['emp_code'];
				$this->companyID = $this->arrData['arrEmployee']['emp_company_id'];
			}
			
			// print
			if($this->employeeID == $this->userEmpNum) {
				$this->arrData['ownUser'] = true;
			}
			
			if($this->userRoleID == COMPANY_ADMIN_ROLE_ID && $this->arrData['arrEmployee']['emp_location_id'] != $this->userLocationID) {
				redirect($this->baseURL . '/message/access_denied');
			}
			$arrEmpSupervisors = getEmpSupervisors($this->employeeID);
			// print_r($this->arrData['ownUser']);
			// exit;
			if((($this->userRoleID == 2 && !$this->arrData['ownUser']) || (!$this->arrData['ownUser'] && !in_array($this->userEmpNum, $arrEmpSupervisors))) && (!isAdmin($this->userRoleID) && count(array_diff($arrEmpSupervisors, explode(',', $this->arrData['strHierarchy']))) == count($arrEmpSupervisors)))
			{
				// print_r("sdfsadf");
				// exit;
				redirect($this->baseURL . '/message/access_denied');
				exit;
			}
			$this->arrData['employeeCode'] = $this->employeeCode;
		}
	}
	
	public function index() {
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');		
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'transfer_management/index', $this->arrData);
		$this->template->render();
	}

	public function list_compliance($pageNum = 1, $leaveID = 0) {
		// print_r("uytrew");exit;
		$this->load->model('model_compliance_management');
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		$query_and_chr = false; 
		if($this->input->post("loanYear") && $this->input->post("loanMonth")) {
			$strDateFrom 						= 	$this->input->post("loanYear") . '-' . $this->input->post("loanMonth") . '-';
			$arrWhere['custom_string'] 			= 	"created_by like '%" . $strDateFrom . "%' ";
			$query_and_chr = true;
		}
		if($this->input->post("emp_id")) {
			if($query_and_chr == false){
				$emp_loan_id 	= $this->input->post("emp_id");
				$arrWhere['emp_id'] 	= " cd1.emp_id = ".$emp_loan_id;
				$query_and_chr = true;
			}else{
				$emp_loan_id 	= $this->input->post("emp_id");
				$arrWhere['emp_id'] 	= " AND cd1.emp_id = ".$emp_loan_id;
				$query_and_chr = true;
			}
		}		
		if($this->input->post("deductionStatus")) {
			if($query_and_chr == false){
				$deductionStatus 				= $this->input->post("deductionStatus");
				if($deductionStatus == -1){
					$deductionStatus = 0;
					$arrWhere['deductionStatus'] = " deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}else{
					$arrWhere['deductionStatus'] = " deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}
			}else{
				$deductionStatus 				= $this->input->post("deductionStatus");
				if($deductionStatus == -1){
					$deductionStatus = 0;
					$arrWhere['deductionStatus'] = "AND deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}else{
					$arrWhere['deductionStatus'] = "AND deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}
			}
		}

		if($this->input->post("deductionYear") && $this->input->post("deductiononth")) {
			if($query_and_chr == false){
				$strDateFrom 					= 	$this->input->post("deductionYear") . '-' . $this->input->post("deductiononth") . '-';
				$arrWhere['strDateFrom'] = " cd1.created_date like '%".$strDateFrom."%' ";
				$query_and_chr = true;
			}else{
				$arrWhere['strDateFrom'] = "AND cd1.created_date like '%".$strDateFrom."%' ";
				$query_and_chr = true;
			}
		}

		$offSet 								= ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] 			= $this->model_compliance_management->get_compliance_deduction($arrWhere, $this->limitRecords, $offSet);
		$this->arrData['arrSupervisors'] 		= $this->model_compliance_management->getSupervisors($this->userEmpNum);
		$this->arrData['all_employees']			= $this->model_compliance_management->getAllEmployeeBranch();
		$this->arrData['employee_region_id']	= $this->model_compliance_management->getEmployeeRegion($this->userEmpNum);
		$this->arrData['employee_company_id']	= $this->model_compliance_management->getEmployeeCompany($this->userEmpNum);
		$supers 	= $this->arrData['arrSupervisors'];
		$super_key 	= 'supervisor_emp_id';
		$super_id =array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
		}, $supers);
		
		$this->arrData['arrSupervisorsIds'] 	= $super_id;
		$numPages 							= ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] 		= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmTranferStatus');
		// print_r($this->arrData['arrRecords']);exit;
		$this->arrData['frmActionURL'] 		= $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		$this->template->write_view('content', 'compliance_management/list_compliance', $this->arrData);
		$this->template->render();
	}

	public function request_compliance($DeductionID = 0,$delete = 0,$pageNum = 1) {
		$this->load->model('model_compliance_management');
		$this->load->model('model_employee_management');

		if((int)$delete == 1 && (int)$DeductionID) {
			$this->model_compliance_management->deleteDeductionStatus((int)$DeductionID, $this->userEmpNum, date($this->arrData["dateTimeFormat"]));
			$this->session->set_flashdata('success_message', 'Application updated successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_compliance');
			exit;
		}
		if((int)$delete != 2 && (int)$DeductionID) {
			$this->model_compliance_management->updateDeduction((int)$DeductionID, $this->userEmpNum, date($this->arrData["dateTimeFormat"]));
			$this->session->set_flashdata('success_message', 'Application updated successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_compliance');
			exit;
		}
		#################################### FORM VALIDATION START ####################################		

		$this->form_validation->set_rules('empID', 'Emplyee Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('deduction_title', 'Deduction Title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('deduction_price', 'Deduction Price', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################

		// print_r($this->arrData["docFolder"]);exit;
		$upload_path = './docs/complaince/';
		if ($this->form_validation->run() == true) {
			#	SUPPORTING DOCUMENT (IF ANY)
			$uploadPicConfig['upload_path'] 	= $upload_path;
			$uploadPicConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp|doc|docs|pdf';
			$uploadPicConfig['max_size']		= '2048';
			$uploadPicConfig['max_filename']	= '100';
			$uploadPicConfig['encrypt_name']	= true;

			$this->load->library('upload');
			$this->upload->initialize($uploadPicConfig);
			
			$docFileName = '';
			// print_r($_FILES['deductionDoc']['name']);exit;
			if(!$this->upload->do_upload('deductionDoc')) {
				if(!empty($_FILES['deductionDoc']['name'])) {
					// echo "adfafd";exit;
					$this->arrData['error_message'] = $error['error'];
					$error = array('error' => $this->upload->display_errors());	
					// print_r($error);exit;
				}					
			} else {				
				$dataUpload = $this->upload->data();
				$docFileName = basename($dataUpload['file_name']);
			}
			// print_r($docFileName);exit;
			$arrValues = array(
								'emp_id' 			=> $this->input->post("empID"),
								'title' 			=> $this->input->post("deduction_title"),
								'price' 			=> $this->input->post("deduction_price"),
								'reason' 			=> $this->input->post("deductionReason"),
								'deduction_status' 	=> 0,
								'created_by' 		=> $this->userEmpNum
							);
			if(!empty($docFileName)) {
				$arrValues['attachment'] = $docFileName;
			}


			if((int)$delete == 2 && (int)$DeductionID) {
				$this->model_compliance_management->saveValues(TABLE_COMPLIANCE_DEDUCTION, $arrValues, array('l.ID' => (int)$DeductionID, 'l.deduction_status' => 0));
				$this->session->set_flashdata('success_message', 'Application updated successfully');
				
			} else {
				// $empSupervisors = $this->model_compliance_management->getSupervisors($this->userEmpNum);
				$arrValues['created_date'] 	= date($this->arrData["dateTimeFormat"]);
				$Transfer_id =  $this->model_compliance_management->saveValues(TABLE_COMPLIANCE_DEDUCTION, $arrValues);
				$this->session->set_flashdata('success_message', 'Application submitted successfully');
			}
			redirect($this->baseURL . '/' . $this->currentController . '/list_compliance');
			exit;
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		// $this->arrData['deduction']		= $this->model_compliance_management->getAllDeduction();
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		}
		$this->arrData['arrEmployees'] = $this->employee->getEmployees();

		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		$this->arrData["arrEmployees"] = $finalResult;
		if($delete == 2 && (int)$DeductionID)
		{
			$this->arrData['emploanRequest']	= $this->model_compliance_management->getEmployeeloanRequest($DeductionID);
		}
		$this->template->write_view('content', 'compliance_management/request_compliance', $this->arrData);
		$this->template->render();
	}
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */