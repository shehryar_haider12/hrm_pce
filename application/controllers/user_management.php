<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Management extends Master_Controller {
		
	private $arrData = array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter = '-';
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_user_management', 'model_user_management', true);
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID);
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
	}
	
	public function index()
	{
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'user_management/index', $this->arrData);
		$this->template->render();
	}
			
	public function save_user($isemployeeFlag = 0, $editID = 0)
	{			
		// print_r('asdfadsfdf');exit;
		$tblName = TABLE_USER;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$editID = (int)$editID;
		if($isemployeeFlag=YES && $editID) {			
			$arrWhere = array(
				'employee_id' => $editID
				);
				
			$this->arrData['freezeEmployeeId'] = YES;
		}
		if($isemployeeFlag=0 && $editID) {			
			$arrWhere = array(
				'candidate_id' => $editID
				);
				
			$this->arrData['freezeEmployeeId'] = YES;
		}
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('userRole', 'User Role', 'trim|callback_numcheck[userRole]');
		$this->form_validation->set_rules('employeeId', 'Employee Id', 'trim|numeric|required|min_length[1]|max_length[5]xss_clean');
		$this->form_validation->set_rules('loginName', 'User Name', 'trim|required|min_length[5]|max_length[20]|xss_clean|callback_validate_english[both]');
		$this->form_validation->set_rules('loginPassword', 'Password', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('loginConfirmPassword', 'Confirm Password', 'trim|required|min_length[5]|max_length[20]|matches[loginPassword]');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			if($this->input->post())
			{				
				if($editID)
				{
					$status = $this->input->post("status");
					if($status == STATUS_INACTIVE) {
						$status = STATUS_INACTIVE_VIEW;
					}
					
					$arrValues = array(
						'user_role_id' => $this->input->post("userRole"),
						'user_name' => $this->input->post("loginName"),
						'plain_password' => $this->input->post("loginPassword"),
						'password' => md5($this->input->post("loginPassword")),
						'user_status' => (int)$status,
						'modified_by' => $this->userID,
						'modified_date' => date($this->arrData["dateTimeFormat"])
						);
				}
				else 
				{
					$status = $this->input->post("status");
					if($status == STATUS_INACTIVE) {
						$status = STATUS_INACTIVE_VIEW;
					}
					
					$arrValues = array(
						'user_role_id' => $this->input->post("userRole"),
						'employee_id' => $this->input->post("employeeId"),
						'user_name' => $this->input->post("loginName"),
						'plain_password' => $this->input->post("loginPassword"),
						'password' => md5($this->input->post("loginPassword")),
						'user_status' => (int)$status,
						'created_by' => $this->userID,
						'created_date' => date($this->arrData["dateTimeFormat"])
						);
				}	
				
				if(!$this->model_user_management->saveValues($tblName, $arrValues, $arrWhere)) 
				{
					$this->arrData['error_message'] = 'Data not saved, try again';
				} else {
					
					# SET LOG
					debugLog("User Updated: [".$arrValues['user_name']."]");
					
					$this->session->set_flashdata('success_message', 'User saved successfully');
					redirect(base_url() . $this->currentController . '/list_user');
					exit;
				}
				
			}
		} else {			
			$this->arrData['validation_error_message'] = validation_errors();
		}
	//	print_r('dsfadfadf');exit;
		# CODE FOR DISPLAYING DATA OF SELECTED USER
		if($editID) {
			$this->arrData['record'] = $this->model_user_management->getUsers($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
	//	print_r('asdfadsfdf');exit;		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/save_user', $this->arrData);
		$this->template->render();
	}
	
	public function list_user($pageNum = 1)
	{						
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				$tblName = TABLE_USER;
				
				$arrDeleteWhere = array(
					'employee_id' => $this->input->post("record_id")
					);
					
				$arrDeleteValues = array(
					'user_status' => STATUS_DELETED,
					'deleted_by' => $this->userID,
					'deleted_date' => date($this->arrData["dateTimeFormat"])
					);
				
				if(!$this->model_user_management->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("employeeId")) {
				$arrWhere['u.employee_id'] = $this->input->post("employeeId");
				$this->arrData['employeeId'] = $this->input->post("employeeId");
			}
			// if($this->input->post("empIP")) {
			// 	$arrWhere['u.emp_ip_num like '] = '%'.$this->input->post("empIP");
			// 	$this->arrData['empIP'] = $this->input->post("empIP");
			// }
			// if($this->input->post("candidateId")) {
			// 	$arrWhere['u.emp_code'] = $this->input->post("candidateId");
			// 	$this->arrData['candidateId'] = $this->input->post("candidateId");
			// }
			if($this->input->post("nameContains")) {
				$arrWhere['user_name like '] = $this->input->post("nameContains").'%';
				$this->arrData['nameContains'] = $this->input->post("nameContains");
			}
			if($this->input->post("userRole")) {				
				$arrWhere['u.user_role_id'] = $this->input->post("userRole");
				$this->arrData['userRole'] = $this->input->post("userRole");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = 0;
				}
				$arrWhere['u.user_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
						
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_user_management->getTotalUsers($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_user_management->getUsers($arrWhere, $this->limitRecords, $offSet);
		// print_r($this->arrData['arrRecords']);exit;
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListUsers');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		$arrEmployees = $this->employee->getEmployees();
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/list_user', $this->arrData);
		$this->template->render();
	}
	
	public function save_user_role($userRoleID = 0)
	{
		$tblName = TABLE_USER_ROLE;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$userRoleID = (int)$userRoleID;
		if($userRoleID) {			
			$arrWhere = array(
				'user_role_id' => $userRoleID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('userRole', 'User Role Name', 'trim|required|min_length[5]|max_length[25]|xss_clean|callback_validate_english[english]');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			if($this->input->post())
			{				
				if($userRoleID) 
				{
					$status = $this->input->post("status");
					if($status == STATUS_INACTIVE) {
						$status = STATUS_INACTIVE_VIEW;
					}
					
					$arrValues = array(
						'user_role_name' => ucwords($this->input->post("userRole")),
						'is_admin' => (int)$this->input->post("isAdmin"),
						'user_role_status' => (int)$status,
						'modified_by' => $this->userID,
						'modified_date' => date($this->arrData["dateTimeFormat"])
						);
				}
				else
				{
					$status = $this->input->post("status");
					if($status == STATUS_INACTIVE) {
						$status = STATUS_INACTIVE_VIEW;
					}
					
					$arrValues = array(
						'user_role_name' => ucwords($this->input->post("userRole")),
						'is_admin' => (int)$this->input->post("isAdmin"),
						'user_role_status' => (int)$status,
						'created_by' => $this->userID,
						'created_date' => date($this->arrData["dateTimeFormat"])
						);
				}
				
				if(!$this->model_user_management->saveValues($tblName, $arrValues, $arrWhere)) {
					$this->arrData['error_message'] = 'Data not saved, try again';
				} else {
					
					# SET LOG
					debugLog("User Role Saved: [".$arrValues['user_role_name']."]");
					
					$this->session->set_flashdata('success_message', 'User Role saved successfully');
					redirect(base_url() . $this->currentController . '/list_user_role');
					exit;
				}
				
			}
			
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED USER ROLE
		if($userRoleID) {
			$this->arrData['record'] = $this->model_user_management->getUserRoles($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/save_user_role', $this->arrData);
		$this->template->render();
	}
	
	public function list_user_role($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				$tblName = TABLE_USER_ROLE;
					
				$arrDeleteWhere = array(
					'user_role_id' => $this->input->post("record_id")
					);
					
				$arrDeleteValues = array(
					'user_role_status' => STATUS_DELETED,
					'deleted_by' => $this->userID,
					'deleted_date' => date($this->arrData["dateTimeFormat"])
					);
				
				if(!$this->model_user_management->deleteValue($tblName, $arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}
				
			}
		}		
		
		$arrWhere = array();
		
		if ($this->input->post()) 
		{			
			if($this->input->post("isAdmin")) {
				$arrWhere['ur.is_admin'] = $this->input->post("isAdmin");
				$this->arrData['isAdmin'] = $this->input->post("isAdmin");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['ur.user_role_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_user_management->getTotalUserRoles($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_user_management->getUserRoles($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListUserRoles');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/list_user_role', $this->arrData);
		$this->template->render();
	}
	
	public function save_user_permission($roleID = 0,$moduleID = 0,$subModuleID = 0)
	{
		$tblName = TABLE_USER_PERMISSIONS;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$roleID = (int)$roleID;
		$moduleID = (int)$moduleID;
		$subModuleID = (int)$subModuleID;
		if($roleID && $moduleID && $subModuleID) {			
			$arrWhere = array(
				'user_role_id' => $roleID,
				'module_id' => $moduleID,
				'sub_module_id' => $subModuleID
				);
				
			$this->arrData['editFlag'] = YES;
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('userRole', 'User Role', 'trim|callback_numcheck[userRole]');
		$this->form_validation->set_rules('module', 'Module', 'trim|callback_numcheck[module]');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			if($this->input->post())
			{
				$data = array();
				
				$data["subModules"] = $this->input->post('sub_modules');
				$count = 0;
				if($data["subModules"])
				{
					foreach ($data["subModules"] as $key => $value) 
					{
						// print_r($data["subModules"]);exit;
						$data["status"] = NO;
						$data["canRead"] = NO;
						$data["canWrite"] = NO;
						$data["canDelete"] = NO;
						
						$data["userRoleId"] = $this->input->post('userRole');
						$data["moduleId"] = $this->input->post('module');
						$data["subModuleId"] = $key;
						
						if($this->input->post('status'))
						{
							$statusArray = $this->input->post('status');
							if(in_array($key,array_keys($statusArray)))
							{								
								$data["status"] = $statusArray[$key];								
							}
						}
						
						if($this->input->post('can_read'))
						{
							$canReadArray = $this->input->post('can_read');
							if(in_array($key,array_keys($canReadArray)))
							{
								$data["canRead"] = YES;
							}
						}
						
						if($this->input->post('can_write'))
						{
							$canWriteArray = $this->input->post('can_write');
							if(in_array($key,array_keys($canWriteArray)))
							{
								$data["canWrite"] = YES;
							}
						}
						
						if($this->input->post('can_delete'))
						{
							$canDeleteArray = $this->input->post('can_delete');
							if(in_array($key,array_keys($canDeleteArray)))
							{
								$data["canDelete"] = YES;
							}
						}
						if($count == 0){
							$arrValues = array(
								'user_role_id' 	=> $data["userRoleId"],
								'module_id' 	=> 1,
								'sub_module_id' => $data["moduleId"],
								'status' 		=> 1,
								'can_read' 		=> 1,
								'can_write' 	=> 1,
								'can_delete' 	=> 1,
								);
							$result = $this->model_user_management->saveValues($tblName, $arrValues, $arrWhere);

						}
						
						if($roleID && $moduleID && $subModuleID) 
						{
							$arrValues = array(
								'status' => $data["status"],
								'can_read' => $data["canRead"],
								'can_write' => $data["canWrite"],
								'can_delete' => $data["canDelete"],
								);
						}
						else {
							$arrValues = array(
								'user_role_id' => $data["userRoleId"],
								'module_id' => $data["moduleId"],
								'sub_module_id' => $data["subModuleId"],
								'status' => $data["status"],
								'can_read' => $data["canRead"],
								'can_write' => $data["canWrite"],
								'can_delete' => $data["canDelete"],
								);
						}
						
						$result = $this->model_user_management->saveValues($tblName, $arrValues, $arrWhere);
						
						if(!$result) {
							$this->arrData['error_message'] = 'Data not saved, try again';
						}
						$count++;
					}
					if($result) 
					{
						# SET LOG
						debugLog("User Permission Updated Against User Role: [".$data["userRoleId"]."], Module Id: [".$data["moduleId"]."]");
						
						$this->session->set_flashdata('success_message', 'User Role Permissions saved successfully !!');
						redirect(base_url() . $this->currentController . '/list_user_permission');
						exit;
					}
					
				}
				
			}
			
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED USER PERMISSION WHICH INCLUDES ROLE ID, MODULE ID, SUB MODULE ID
		if($roleID && $moduleID && $subModuleID) {
			$this->arrData['roleID'] = $roleID;
			$this->arrData['moduleID'] = $moduleID;
			$this->arrData['subModuleID'] = $subModuleID;
		}
				
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
		$this->arrData["modules"] = $this->model_user_management->populateModules();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/save_user_permission', $this->arrData);
		$this->template->render();		 
	}
	
	public function copy_user_permission()
	{
		$tblName = TABLE_USER_PERMISSIONS;
		
		$arrWhere = array();
		$this->arrData['record'] = array();		
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('userRoleFrom', 'User Role From', 'trim|required|xss_clean');		
		$this->form_validation->set_rules('moduleFrom', 'Module From', 'trim|required|xss_clean');
		$this->form_validation->set_rules('userRoleTo', 'User Role To', 'trim|required|xss_clean');		
		$this->form_validation->set_rules('moduleTo', 'Module To', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			if($this->input->post())
			{
				$data = array();
				
				$data["subModules"] = $this->input->post('sub_modules');
				
				if($data["subModules"])
				{
					foreach ($data["subModules"] as $key => $value) 
					{
						$data["status"] = NO;
						$data["canRead"] = NO;
						$data["canWrite"] = NO;
						$data["canDelete"] = NO;
						
						$data["userRoleId"] = $this->input->post('userRoleTo');
						$data["moduleId"] = $this->input->post('moduleTo');
						$data["subModuleId"] = $key;
						
						if($this->input->post('status'))
						{
							$statusArray = $this->input->post('status');
							if(in_array($key,array_keys($statusArray)))
							{								
								$data["status"] = $statusArray[$key];								
							}
						}
						
						if($this->input->post('can_read'))
						{
							$canReadArray = $this->input->post('can_read');
							if(in_array($key,array_keys($canReadArray)))
							{
								$data["canRead"] = YES;
							}
						}
						
						if($this->input->post('can_write'))
						{
							$canWriteArray = $this->input->post('can_write');
							if(in_array($key,array_keys($canWriteArray)))
							{
								$data["canWrite"] = YES;
							}
						}
						
						if($this->input->post('can_delete'))
						{
							$canDeleteArray = $this->input->post('can_delete');
							if(in_array($key,array_keys($canDeleteArray)))
							{
								$data["canDelete"] = YES;
							}
						}
						
						
						$arrValues = array(
							'user_role_id' => $data["userRoleId"],
							'module_id' => $data["moduleId"],
							'sub_module_id' => $data["subModuleId"],
							'status' => $data["status"],
							'can_read' => $data["canRead"],
							'can_write' => $data["canWrite"],
							'can_delete' => $data["canDelete"],
							);
							
						$arrWhere = array(
							'user_role_id' => $data["userRoleId"],
							'module_id' => $data["moduleId"],
							'sub_module_id' => $key
							);
						
						$recordsExists = $this->model_user_management->checkValues($tblName, $arrWhere);
						
						if($recordsExists) {
							$this->model_user_management->deleteValue($tblName, null, $arrWhere);
						}
						
						$result = $this->model_user_management->saveValues($tblName, $arrValues);
						
						if(!$result) {
							$this->arrData['error_message'] = 'Data not saved, try again';
						}
						
					}
					if($result) 
					{
						# SET LOG
						debugLog("User Permission Updated Against User Role: [".$data["userRoleId"]."], Module Id: [".$data["moduleId"]."]");
						
						$this->session->set_flashdata('success_message', 'User Role Permissions copied successfully !!');
						redirect(base_url() . $this->currentController . '/list_user_permission');
						exit;
					}
					
				}
				
			}
			
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}		
						
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
		$this->arrData["modules"] = $this->model_user_management->populateModules();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/copy_user_permission', $this->arrData);
		$this->template->render();		 
	}
	
	public function list_user_permission($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				$tblName = TABLE_USER_PERMISSIONS;
				
				$arrDeleteValues = array(
					'user_permission_id' => $this->input->post("record_id")
					);
				
				if(!$this->model_user_management->deleteValue($tblName, $arrDeleteValues)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}				
			}
		}
				
		$arrWhere = array();
		
		if ($this->input->post()) {			
			if($this->input->post("userRole")) {				
				$arrWhere['up.user_role_id'] = $this->input->post("userRole");
				$this->arrData['userRole'] = $this->input->post("userRole");
			}
			if($this->input->post("module")) {
				$arrWhere['up.module_id'] = $this->input->post("module");
				$this->arrData['module'] = $this->input->post("module");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['up.status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
			if($this->input->post("canReadCheck")) {
				$canReadCheck = $this->input->post("canReadCheck");
				if($canReadCheck == STATUS_INACTIVE) {
					$canReadCheck = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['up.can_read'] = $canReadCheck;
				$this->arrData['canReadCheck'] = $this->input->post("canReadCheck");
			}
			if($this->input->post("canWriteCheck")) {
				$canWriteCheck = $this->input->post("canWriteCheck");
				if($canWriteCheck == STATUS_INACTIVE) {
					$canWriteCheck = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['up.can_write'] = $canWriteCheck;
				$this->arrData['canWriteCheck'] = $this->input->post("canWriteCheck");
			}
			if($this->input->post("canDeleteCheck")) {
				$canDeleteCheck = $this->input->post("canDeleteCheck");
				if($canDeleteCheck == STATUS_INACTIVE) {
					$canDeleteCheck = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['up.can_delete'] = $canDeleteCheck;
				$this->arrData['canDeleteCheck'] = $this->input->post("canDeleteCheck");
			}
			
		}
		
		# CODE FOR PAGING
		$totalCount = $this->model_user_management->getTotalUserPermissions($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_user_management->getUserPermissions($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListUserPermission');
		
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
		$this->arrData["modules"] = $this->model_user_management->populateModules();
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/list_user_permission', $this->arrData);
		$this->template->render();		
	}
	
	public function user_reporting($pageNum = 1)
	{
		$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID), false);
		$this->arrData['strHierarchy'] = $this->employee->getHierarchy($this->userEmpNum);
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		$arrWhere = array();
		$arrWhere['e.emp_id > '] = '2';
		if ($this->input->post()) {
			
			if($this->input->post("empCode")) {
				$arrWhere['e.emp_code'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$this->arrData['empIP'] = $this->input->post("empIP");
			}
			
			if($this->input->post("empDesignation")) {
				$arrWhere['e.emp_grade_id'] = $this->input->post("empDesignation");
				$this->arrData['empDesignation'] = $this->input->post("empDesignation");
			}
			
			if($this->input->post("empDepartment")) {
				$arrWhere['e.emp_job_category_id'] = $this->input->post("empDepartment");
				$this->arrData['empDepartment'] = $this->input->post("empDepartment");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchy($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("empSBTEmail")) {
				$arrWhere['e.emp_work_email'] = $this->input->post("empSBTEmail");
				$this->arrData['empSBTEmail'] = $this->input->post("empSBTEmail");
			}
			
			if($this->input->post("dateFrom")) {
				$arrWhere['u.last_login_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			
			if($this->input->post("dateTo")) {
				$arrWhere['u.last_login_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
		}
		
		# CODE FOR PAGING
		$this->arrData['totalRecordsCount'] = $this->employee->getTotalEmployees($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_user_management->getUserLoginReport($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmUserReporting');
		
		# CODE FOR PAGE CONTENT
		$this->arrData['empDesignations'] = $this->configuration->getValues(TABLE_GRADES, '*', array('grade_status' => 1));
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => 1, 'order_by' => 'job_category_name'));
		
		if(!isAdmin($this->userRoleID)) {		
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/user_reporting', $this->arrData);
		$this->template->render();
	}
	
	public function user_logs($pageNum = 1) {
		
		$this->load->model('model_system_configuration', 'system', true);
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
			
		if($this->input->post("empID")) {
			$arrWhere['l.emp_id'] = $this->input->post("empID");
			$this->arrData['empID'] = $this->input->post("empID");
		}
		
		if($this->input->post("moduleID")) {
			$arrWhere['l.module_id'] = $this->input->post("moduleID");
			$this->arrData['moduleID'] = $this->input->post("moduleID");
		}
		
		if($this->input->post("dateFrom")) {
			$arrWhere['l.created_date >='] = $this->input->post("dateFrom") . ' 00:00:00';
			$this->arrData['dateFrom'] = $this->input->post("dateFrom");
		}
		
		if($this->input->post("dateTo")) {
			$arrWhere['l.created_date <='] = $this->input->post("dateTo") . ' 23:59:59';
			$this->arrData['dateTo'] = $this->input->post("dateTo");
		}			
		
		if($this->input->post("activity")) {
			$arrWhere['l.log_activity_txt'] = $this->input->post("activity");
			$this->arrData['activity'] = $this->input->post("activity");
		}
		
		$this->arrData['totalRecordsCount'] = $this->model_user_management->getTotalLogs($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		
		if((int)$this->input->post("txtExport")) {
			
			$arrActivities = $this->model_user_management->getLogs($arrWhere);
			$strCSV =  "Employee,Code,Module,When,What" . "\n";
			
			for($ind = 0; $ind < count($arrActivities); $ind++) {
				
				$strCSV .=  $arrActivities[$ind]['emp_full_name'] . ","; 
				$strCSV .=  $arrActivities[$ind]['emp_code'] . '/' . $arrActivities[$ind]['emp_ip_num'] . ","; 
				$strCSV .=  $arrActivities[$ind]['display_name'] . ","; 
				$strCSV .=  date('j-F-Y g:i A', strtotime($arrActivities[$ind]['created_date'])) . ","; 
				$strCSV .=  $arrActivities[$ind]['log_activity_txt'] . "\n";
				
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=user_log_data.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$this->arrData['arrActivities'] = $this->model_user_management->getLogs($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmLogs');
		
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		$arrEmployees = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE), false);
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		
		$this->arrData['arrModules'] = $this->model_user_management->populateModules();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/user_logs', $this->arrData);
		$this->template->render();
	}
	
	public function special_permissions($pageNum = 1) {
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->employee->deleteValue(TABLE_USER_SPECIAL_PERMISSIONS, array('emp_permission_id' => (int)$this->input->post("record_id")), 'emp_status', 0)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}								
			}
		}
		##########################
		
		if($this->input->post("empID")) {
			$arrWhere['usp.emp_id'] = $this->input->post("empID");
			$this->arrData['empID'] = $this->input->post("empID");
		}
		
		if($this->input->post("moduleName")) {
			$arrWhere['usp.module_name'] = $this->input->post("moduleName");
			$this->arrData['moduleName'] = $this->input->post("moduleName");
		}	
		
		if($this->input->post("userRole")) {
			$arrWhere['usp.assigned_role_id'] = $this->input->post("userRole");
			$this->arrData['userRole'] = $this->input->post("userRole");
		}	
		
		$this->arrData['totalRecordsCount'] = $this->model_user_management->getTotalSpecialPermissions($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrPermissions'] = $this->model_user_management->getSpecialPermissions($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmSpecialPermissions');
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;

		$arrEmployees = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE), false);
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		$this->arrData['arrModules'] = $this->model_user_management->populateModules();		
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/special_permissions', $this->arrData);
		$this->template->render();
	}
	
	public function save_special_permission($recordID = 0) {
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('empID', 'Employee', 'trim|numeric');
		$this->form_validation->set_rules('empDepartment', 'Job Category', 'trim|numeric');
		$this->form_validation->set_rules('userRoleID', 'user Role', 'trim|numeric');
		$this->form_validation->set_rules('moduleName', 'Module', 'trim|required|xss_clean');
		$this->form_validation->set_rules('screenName', 'Sub Module', 'trim|required|xss_clean');
		$this->form_validation->set_rules('userRole', 'User Role', 'trim|required|numeric');
		
		#################################### FORM VALIDATION END ####################################
		
		if($this->input->post()) {
			
			$moduleID = getValue($this->modulesAllowed, 'module_name', $this->input->post("moduleName"), 'module_id');
			$subModuleID = $this->configuration->getValues(TABLE_MODULE, 'module_id', array('module_name' => $this->input->post("screenName"), 'module_parent_id' => $moduleID));
			$arrValues = array(
								'emp_id' => (int)$this->input->post("empID"),
								'job_category_id' => (int)$this->input->post("empDepartment"),
								'user_role_id' => (int)$this->input->post("userRoleID"),
								'module_name' => $this->input->post("moduleName"),
								'module_id' => $moduleID,
								'sub_module_name' => $this->input->post("screenName"),
								'sub_module_id' => $subModuleID[0]["module_id"],
								'assigned_role_id' => $this->input->post("userRole")
							);
							
			if((int)$recordID) {
				$arrWhere = array('emp_permission_id' => $recordID);
				$arrValues['modified_date']	= date($this->arrData["dateTimeFormat"]);	
				$arrValues['modified_by']	= $this->userEmpNum;
			} else {
				$arrValues['created_date'] 	= date($this->arrData["dateTimeFormat"]);
				$arrValues['created_by'] 	= $this->userEmpNum;
			}
				
			$resultID = $this->model_user_management->saveValues(TABLE_USER_SPECIAL_PERMISSIONS, $arrValues, $arrWhere);
			if($resultID) {
				debugLog("User Special Permission Updated Against Emp: [".$this->input->post("empID")."], Module: [".$this->input->post("moduleName")."], Screen: [".$this->input->post("screenName")."], Role: [".$this->input->post("userRole")."]");
				
				$this->session->set_flashdata('success_message', 'User Special Permission Saved Successfully');
				redirect(base_url() . $this->currentController . '/special_permissions');
				exit;
			}
		}
		
		if((int)$recordID) {
			$arrWhere = array('usp.emp_permission_id' => $recordID);
			$this->arrData['arrRecord'] = $this->model_user_management->getSpecialPermissions($arrWhere);
			$this->arrData['arrRecord'] = $this->arrData['arrRecord'][0];
		}
		
		$arrEmployees = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE), false);
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$this->arrData['arrModules'] = $this->model_user_management->populateModules();		
		$this->arrData["userRoles"] = $this->model_user_management->populateUserRoles();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'user_management/save_special_permission', $this->arrData);
		$this->template->render();
	}
	
	public function numcheck($in, $value) {
		if (intval($in) <= 0) {
			if($value == 'userRole')
				$this->form_validation->set_message('numcheck', 'Select User Role.');
			else if ($value == 'department')
				$this->form_validation->set_message('numcheck', 'Select Department.');
			else if ($value == 'market')
				$this->form_validation->set_message('numcheck', 'Select Market.');
			else if ($value == 'module')
				$this->form_validation->set_message('numcheck', 'Select Module.');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function validate_english($string,$value) 
	{
		if(preg_match("/((select|delete).+from|update.+set|(alter|truncate|drop).+table|<[a-z])/i", $email)==false)	//condition for sql injection
		{	
			if($value == 'english') 
			{ 
				$this->form_validation->set_message('validate_english', 'User Role Name field must only contain letters.');
				return (preg_match("/^[a-zA-Z,.\/\s]*$/u", $string))?true:false;				
			}
			else if($value == 'both') 
			{
				$this->form_validation->set_message('validate_english', 'User Name field must only contain letters and numbers.');
				return (preg_match("/^[a-zA-Z0-9.]*$/u", $string))?true:false;
			}
		} else return false;
	}
	
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */
