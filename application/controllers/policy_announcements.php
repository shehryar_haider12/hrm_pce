<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policy_Announcements extends Master_Controller {
		
	private $arrData = array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter = '-';
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_policy_announcement', 'policy', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID);
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["imageAnnPath"]			= $this->imagePath . '/announcements';
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["annFolder"]				= ANNOUNCEMENT_FILES_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
	}
	
	public function index() {
		
		$arrPolicies = array('ann_status' => 1, 'ann_is_policy' => 1);
		$arrAnnouncements = array('ann_status' => 1, 'ann_is_policy' => 0);
		
		if(!isAdmin($this->userRoleID)) {
			$arrPolicies['ann_company_id'] = $this->userCompanyID;
			$arrAnnouncements['ann_company_id'] = $this->userCompanyID;
		}
		
		$this->arrData['arrPolicies'] = $this->policy->getAnnouncements($arrPolicies);
		$this->arrData['arrAnnouncements'] = $this->policy->getAnnouncements($arrAnnouncements, 10);
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu');
		$this->template->write_view('content', 'policy_announcements/index', $this->arrData);
		$this->template->render();
	}
	
	public function announcements($annID = 0) {
								
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == 1) {				
				if(!$this->policy->deleteValue(TABLE_ANNOUNCEMENTS, null, array('ann_id' => (int)$this->input->post("record_id")))) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################	
			
		$this->form_validation->set_rules('annTitle', 'Announcement Title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('annHTML', 'Announcement HTML', 'trim|xss_clean');
		$this->form_validation->set_rules('annStatus', 'Announcement Status', 'trim|required');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
						
			$arrWhere = array();
			if((int)$annID) {
				$arrWhere['ann_id'] = $annID;
			}
			
			$this->load->library('upload');
			
			$uploadDocConfig['upload_path'] 	= $this->arrData["annFolder"];
			$uploadDocConfig['allowed_types'] 	= 'pdf';
			$uploadDocConfig['max_size']		= '12048';
			$uploadDocConfig['max_filename']	= '100';
			$uploadDocConfig['encrypt_name']	= true;
			
			$this->upload->initialize($uploadDocConfig);
			
			$pdfFileName = $this->input->post("pdfFileName");
			
			if(!$this->upload->do_upload('annFile')) {
				
				if(!empty($_FILES['annFile']['name'])) {
					$error = array('error' => $this->upload->display_errors());	
					$this->arrData['error_message'] = $error['error'];
					$pdfFileName = '';
				}
							
			} else {		
						
				$dataUpload = $this->upload->data();
				unlink($this->arrData["annFolder"] . $this->input->post("pdfFileName"));
				$pdfFileName = basename($dataUpload['file_name']);
			}
							
			$statusID = (int)$this->input->post("annStatus");
			if($statusID == -1) {
				$statusID = 0;
			}
			
			$arrValues = array(
								'ann_title' => $this->input->post("annTitle"),
								'ann_is_new' => (int)$this->input->post("annIsNew"),
								'ann_is_policy' => (int)$this->input->post("annIsPolicy"),
								'ann_file' => $pdfFileName,
								'ann_html' => $this->input->post("annHTML"),
								'ann_company_id' => (int)$this->input->post("annCompany"),
								'ann_sort_order' => (int)$this->input->post("annSortOrder"),
								'ann_status' => $statusID
								);
								
			if((int)$annID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				$this->policy->saveValues(TABLE_ANNOUNCEMENTS, $arrValues, $arrWhere);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$this->policy->saveValues(TABLE_ANNOUNCEMENTS, $arrValues);
			}
				
			$this->session->set_flashdata('success_message', 'Announcement Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);			
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		if((int)$annID) {
			$this->arrData['record'] = $this->policy->getAnnouncements(array('ann_id' => $annID));
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		$this->arrData['arrAnnouncements'] = $this->policy->getAnnouncements();
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		$this->template->write_view('content', 'policy_announcements/announcements', $this->arrData);
		$this->template->render();
	}
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */