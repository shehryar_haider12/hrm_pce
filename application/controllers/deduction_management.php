<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class deduction_Management extends Master_Controller {
	private $arrData 			= array();
	public $arrRoleIDs			= array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter 			= '-';
	private $employeeID 		= 0;
	private $employeeCode 		= 0;
	private $companyID 			= 0;
	
	public $dateFrom 			= 0;
	public $dateTo 				= 0;	
	public $maxLateAllowed 		= 5;
	public $halfOnLates 		= 7;
	public $fullOnLates 		= 8;
	public $fullHalfDayHours 	= 4.45;
	public $halfHalfDayHours 	= 4.15;
	public $fullHours 			= 8.45;
	public $halfdayHours 		= 8.15;
	public $secLate 			= 0;	// 0 MINUTES
	public $secHalfday 			= 1800;	// 30 MINUTES
	public $lateMinSeconds		= 900;	// 15 MINUTES
	public $lateMaxTime			= 0.25;
	
	public $startRamadan;
	public $endRamadan;	
	public $dateTimeChange;
	function __construct() {
		parent::__construct();
		$this->dateFrom 					= date('Y-m-d', mktime(0, 0, 0, date("m"), 01, date("Y")));
		$this->dateTo 						= date('Y-m-d', mktime(0, 0, 0, date("m"), date("t"), date("Y")));
		$this->load->model('model_deduction_management', 'employee', true);
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID, COMPANY_ADMIN_ROLE_ID,REGIONAL_MANAGER);
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["super_visor_roleid"] 	= SUPERVISOR_ROLE_ID;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["docFolder"]				= ATTENDANCE_TRANSFER_DOCS_FOLDER;
		$this->arrData["docFolderShow"]			= str_replace('./', '', ATTENDANCE_TRANSFER_DOCS_FOLDER);
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		$this->arrData['strHierarchy'] = $this->employee->getHierarchyWithMultipleAuthorities($this->userEmpNum);
		
		$this->arrData['skipParams'] = array(
												'deduction_details',
												'deduction_request',
												'list_deduction',
												'deduction_CCTV_request',
											);	
		if(!in_array($this->currentAction, $this->arrData['skipParams'])) {
			
			$this->employeeID = (int)$this->input->post("empID");
			if(!(int)$this->employeeID) {
				$this->employeeID = (int)$this->uri->segment(3);
			}
			
			if(!(int)$this->employeeID) {
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
				$this->employeeID = $this->arrData['arrEmployee']['emp_id'];
				$this->employeeCode = $this->arrData['arrEmployee']['emp_code'];
				$this->companyID = $this->arrData['arrEmployee']['emp_company_id'];
			} else {
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID), false);
				$this->employeeID = $this->arrData['arrEmployee']['emp_id'];
				$this->employeeCode = $this->arrData['arrEmployee']['emp_code'];
				$this->companyID = $this->arrData['arrEmployee']['emp_company_id'];
			}
			
			// print
			if($this->employeeID == $this->userEmpNum) {
				$this->arrData['ownUser'] = true;
			}
			
			if($this->userRoleID == COMPANY_ADMIN_ROLE_ID && $this->arrData['arrEmployee']['emp_location_id'] != $this->userLocationID) {
				redirect($this->baseURL . '/message/access_denied');
			}
			
			$arrEmpSupervisors = getEmpSupervisors($this->employeeID);
			
			// print_r($this->arrData['ownUser']);
			// exit;
			
			if((($this->userRoleID == 2 && !$this->arrData['ownUser']) || (!$this->arrData['ownUser'] && !in_array($this->userEmpNum, $arrEmpSupervisors))) && (!isAdmin($this->userRoleID) && count(array_diff($arrEmpSupervisors, explode(',', $this->arrData['strHierarchy']))) == count($arrEmpSupervisors)))
			{
				// print_r("sdfsadf");
				// exit;
				redirect($this->baseURL . '/message/access_denied');
				exit;
			}
			
			$this->arrData['employeeCode'] = $this->employeeCode;
		}
	}
	
	public function index() {
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');		
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'transfer_management/index', $this->arrData);
		$this->template->render();
	}

	public function deduction_details($pageNum = 1, $leaveID = 0) {
		$this->load->model('model_deduction_management');
		$offSet 								= ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] 			= $this->model_deduction_management->getLoan($arrWhere, $this->limitRecords, $offSet);
		$this->arrData['arrSupervisors'] 		= $this->model_deduction_management->getSupervisors($this->userEmpNum);
		$this->arrData['all_employees']			= $this->model_deduction_management->getAllEmployeeBranch();
		$this->arrData['employee_region_id']	= $this->model_deduction_management->getEmployeeRegion($this->userEmpNum);
		$this->arrData['employee_company_id']	= $this->model_deduction_management->getEmployeeCompany($this->userEmpNum);
		$supers 	= $this->arrData['arrSupervisors'];
		$super_key 	= 'supervisor_emp_id';
		$super_id =array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
		}, $supers);
		
		$this->arrData['arrSupervisorsIds'] 	= $super_id;
		$numPages 							= ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] 		= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmTranferStatus');
		// print_r($this->arrData['arrRecords']);exit;
		$this->arrData['frmActionURL'] 		= $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		$this->template->write_view('content', 'deduction_management/deduction_details', $this->arrData);
		$this->template->render();
	}

	public function list_deduction($pageNum = 1, $leaveID = 0) {

		$this->load->model('model_deduction_management');
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}

		$query_and_chr = false; 
		if($this->input->post("loanYear") && $this->input->post("loanMonth")) {
			$strDateFrom 						= 	$this->input->post("loanYear") . '-' . $this->input->post("loanMonth") . '-';
			$arrWhere['custom_string'] 			= 	"loan_date like '%" . $strDateFrom . "%' ";
			$query_and_chr = true;
		}
		if($this->input->post("emp_id")) {
			if($query_and_chr == false){
				$emp_loan_id 	= $this->input->post("emp_id");
				$arrWhere['emp_id'] 	= " cd1.emp_id = ".$emp_loan_id;
				$query_and_chr = true;
			}else{
				$emp_loan_id 	= $this->input->post("emp_id");
				$arrWhere['emp_id'] 	= " AND cd1.emp_id = ".$emp_loan_id;
				$query_and_chr = true;
			}
		}		
		if($this->input->post("deductionStatus")) {
			if($query_and_chr == false){
				$deductionStatus 				= $this->input->post("deductionStatus");
				if($deductionStatus == -1){
					$deductionStatus = 0;
					$arrWhere['deductionStatus'] = " deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}else{
					$arrWhere['deductionStatus'] = " deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}
			}else{
				$deductionStatus 				= $this->input->post("deductionStatus");
				if($deductionStatus == -1){
					$deductionStatus = 0;
					$arrWhere['deductionStatus'] = "AND deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}else{
					$arrWhere['deductionStatus'] = "AND deduction_status = ".$deductionStatus;
					$query_and_chr = true;
				}
			}
		}

		if($this->input->post("deductionYear") && $this->input->post("deductiononth")) {
			if($query_and_chr == false){
				$strDateFrom 					= 	$this->input->post("deductionYear") . '-' . $this->input->post("deductiononth") . '-';
				$arrWhere['strDateFrom'] = " cd1.created_date like '%".$strDateFrom."%' ";
				$query_and_chr = true;
			}else{
				$arrWhere['strDateFrom'] = "AND cd1.created_date like '%".$strDateFrom."%' ";
				$query_and_chr = true;
			}
		}

		$offSet 								= ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] 			= $this->model_deduction_management->getCCTVdeduction($arrWhere, $this->limitRecords, $offSet);
		$this->arrData['arrSupervisors'] 		= $this->model_deduction_management->getSupervisors($this->userEmpNum);
		$this->arrData['all_employees']			= $this->model_deduction_management->getAllEmployeeBranch();
		$this->arrData['employee_region_id']	= $this->model_deduction_management->getEmployeeRegion($this->userEmpNum);
		$this->arrData['employee_company_id']	= $this->model_deduction_management->getEmployeeCompany($this->userEmpNum);
		$supers 	= $this->arrData['arrSupervisors'];
		$super_key 	= 'supervisor_emp_id';
		$super_id =array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
		}, $supers);
		
		$this->arrData['arrSupervisorsIds'] 	= $super_id;
		$numPages 							= ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] 		= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmTranferStatus');
		// print_r($this->arrData['arrRecords']);exit;
		$this->arrData['frmActionURL'] 		= $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		$this->template->write_view('content', 'deduction_management/list_deduction', $this->arrData);
		$this->template->render();
	}

	public function view_employee_loan_details($EmpID = 0) {
		$this->load->model('model_transfer_management');
		
			$arrTransferDetail 		= $this->model_transfer_management->getAllTransfersByEmployee($EmpID);
			$transfers_count 		= count($arrTransferDetail);
			$transferarrValues['transfer_details'] = array(
				'LOGO_URL' 				=> "https://amanahmall.com.pk/wp-content/uploads/2018/12/Pakistan-Currency-Exchange.png",
				'CREATED_DATE_TIME' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),							  
				'EMPLOYEE_CODE' 			=> $arrTransferDetail[0]['emp_code'],
				'EMPLOYEE_NAME' 			=> $arrTransferDetail[0]['emp_full_name'],
				'JOINING_DATE'			=> $arrTransferDetail[0]['emp_joining_date'],
				'EMPLOYEE_DESIGNATION' 	=> $arrTransferDetail[0]['emp_designation'],
				'CURRENT_BRANCH'			=> $arrTransferDetail[$transfers_count - 1]['company_to'],
				'TRNSFER_BRANCH'			=> $arrTransferDetail[0]['company_from'],
				'FIRST_BRANCH'			=> $arrTransferDetail[0]['company_from'],
				'FIRST_BRANCH_DATE'		=> $arrTransferDetail[0]['processed_date'],
				'FIRST_BRANCH_PROCESSED'	=> $arrTransferDetail[0]['processed_name'],
				'POSTINGS'				=> $arrTransferDetail,
			);
				
			
			$this->template->write_view('content', 'transfer_management/emp_transfers_form', $transferarrValues);
			$this->template->render();
	}
	
	public function view_loan_employees($EmpID = 0,$pageNum = 1) {
		$this->load->model('model_deduction_management');
		$this->load->model('model_system_configuration');

		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		$arrWhere = array();
		if($this->input->post("emp_id")) {
			$arrWhere['emp_id'] 	= $this->input->post("emp_id");
		}		
		if($this->input->post("branch_from")) {
			$arrWhere['company_from'] 	= $this->input->post("branch_from");
			// print_r($arrWhere['company_from']);
			// exit;
		}		

		if($this->input->post("branch_to")) {
			$arrWhere['company_to'] 	= $this->input->post("branch_to");
		}		
		if($this->input->post("transferStatus")) {
			$arrWhere['transfer_details'] 		= $this->input->post("transferStatus");
			if($arrWhere['transfer_details'] == -1) {
				$arrWhere['transfer_details'] = 0;
			}
		}		
		if($this->input->post("tansferYear") && $this->input->post("tansferMonth")) {

			$strDateFrom 					= 	$this->input->post("tansferYear") . '-' . $this->input->post("tansferMonth") . '-';
			$arrWhere['custom_string'] 		= 	"created_date like '%" . $strDateFrom . "%' ";
			
		}
		$offSet 							= ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] 		= $this->model_transfer_management->getTransfersEmployees($arrWhere, $this->limitRecords, $offSet);
		$this->arrData['arrSupervisors'] 	= $this->model_transfer_management->getSupervisors($this->userEmpNum);
		$supers 	= $this->arrData['arrSupervisors'];
		$super_key 	= 'supervisor_emp_id';

		$super_id =array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
		  }, $supers);

		$this->arrData['arrSupervisorsIds'] 	= $super_id;
		$numPages 							= ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] 		= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmTranferStatus');
		$this->arrData['companies']			= $this->model_transfer_management->getCompanies();
		$this->arrData['regions']			= $this->model_system_configuration->getRegions();
		$this->arrData['all_employees']		= $this->model_transfer_management->getAllEmployeeBranch();
		// print_r($this->arrData['all_employees']);
		// exit;
		$this->arrData['frmActionURL'] 		= $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		$this->template->write_view('content', 'transfer_management/view_transfer_employees', $this->arrData);
		$this->template->render();
	}

	public function deduction_request($DeductionID = 0,$delete = 0,$pageNum = 1) {
		// print_r("oiuytresdfghm,mnbv");exit;

		$this->load->model('model_deduction_management');

		if((int)$delete == 0 && (int)$DeductionID) {
			$response  = $this->model_deduction_management->updateDeduction((int)$DeductionID, $this->userEmpNum, date($this->arrData["dateTimeFormat"]));
			$this->session->set_flashdata('success_message', 'Application updated successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/deduction_details');
			exit;
		}
		if((int)$delete == 1 && (int)$DeductionID) {
			$response  = $this->model_deduction_management->deleteDeductionStatus((int)$DeductionID, $this->userEmpNum, date($this->arrData["dateTimeFormat"]));
			$this->session->set_flashdata('success_message', 'Application updated successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/deduction_details');
			exit;
		}
		// print_r("asdasd");exit;
		#################################### FORM VALIDATION START ####################################		

		$this->form_validation->set_rules('deductiontitle', 'Deduction Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('deductionammount', 'Deduction Amount', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
								
			$arrValues = array(
								'name' 			=> $this->input->post("deductiontitle"),
								'price' 		=> $this->input->post("deductionammount"),
								'created_by' 	=> $this->userEmpNum,
								'created_date' 	=> date($this->arrData["dateTimeFormat"]),
							);
			if((int)$DeductionID) {
				$this->model_deduction_management->saveValues(TABLE_DEDUCTION, $arrValues, array('l.id' => (int)$DeductionID));
				$this->session->set_flashdata('success_message', 'Application updated successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/deduction_details');
				exit;
			}else{
				$this->model_deduction_management->saveValues(TABLE_DEDUCTION, $arrValues);
				$this->session->set_flashdata('success_message', 'Application submitted successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/deduction_details');
				exit;
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		$this->arrData['totalRecordsCount']	= $this->model_deduction_management->getTotalLeavesForApproval($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_deduction_management->getTransfersForApproval($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmLeaveRequests');
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
				
		$arrWhereSupervisor = array('e.emp_status' => STATUS_ACTIVE); //, 'e.emp_code >' => ZERO
		$this->arrData['arrSupervisors'] 	= $this->employee->getSupervisors($arrWhereSupervisor);
		$finalResult 						= array();
		$this->arrData['companies']			= $this->model_deduction_management->getCompanies();
		$emp_id_updated = !empty($this->arrData['arrEmployee']['emp_id']) ? $this->arrData['arrEmployee']['emp_id'] : $this->userEmpNum; 
		// $this->arrData['employee_branch']	= $this->model_deduction_management->getEmployeeBranch($emp_id_updated);
		$this->arrData['arrSupervisors'] 	= $finalResult;
		if($delete == 2 && (int)$DeductionID)
		{
			$this->arrData['emploanRequest']	= $this->model_deduction_management->getEmployeeloanRequest($DeductionID);
		}
		$this->template->write_view('content', 'deduction_management/deduction_request', $this->arrData);
		$this->template->render();
	}

	public function deduction_CCTV_request($DeductionID = 0,$delete = 0,$pageNum = 1) {
		// print_r($this->userRoleID);exit;

		$this->load->model('model_deduction_management');
		$this->load->model('model_employee_management');

		if((int)$delete == 1 && (int)$DeductionID) {
			$this->model_deduction_management->deleteDeductionStatus((int)$DeductionID, $this->userEmpNum, date($this->arrData["dateTimeFormat"]));
			$this->session->set_flashdata('success_message', 'Application updated successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_deduction');
			exit;
		}

		if((int)$delete != 2 && (int)$DeductionID) {
			$this->model_deduction_management->updateDeduction((int)$DeductionID, $this->userEmpNum, date($this->arrData["dateTimeFormat"]));
			$this->session->set_flashdata('success_message', 'Application updated successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_deduction');
			exit;
		}
		
		#################################### FORM VALIDATION START ####################################		

		$this->form_validation->set_rules('empID', 'Emplyee Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('deduction[]', 'Deduction', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		if ($this->form_validation->run() == true) {
			// print_r("iuytre");exit;
			if((int)$delete == 2 && (int)$DeductionID) {
				foreach ($this->input->post("deduction") as $deduction){
					$arrValues = array(
						'emp_id' 		=> $this->input->post("empID"),
						'deduction_id' 	=> $deduction,
						'created_by' 	=> $this->userEmpNum,
						'created_date' 	=> date($this->arrData["dateTimeFormat"]),
					);
					$this->model_deduction_management->saveValues(TABLE_CCTV_DEDUCTION, $arrValues, array('l.id' => (int)$DeductionID));
				} 
				$this->session->set_flashdata('success_message', 'Application updated successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/deduction_CCTV_request');
				exit;
			}else{
				foreach ($this->input->post("deduction") as $deduction){
					$arrValues = array(
						'emp_id' 		=> $this->input->post("empID"),
						'deduction_id' 	=> $deduction,
						'created_by' 	=> $this->userEmpNum,
						'created_date' 	=> date($this->arrData["dateTimeFormat"]),
					);
					$this->model_deduction_management->saveValues(TABLE_CCTV_DEDUCTION, $arrValues);
				} 
				$this->session->set_flashdata('success_message', 'Application submitted successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/deduction_CCTV_request');
				exit;
			}
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		$this->arrData['arrRecords'] = $this->model_deduction_management->getTransfersForApproval($arrWhere, $this->limitRecords, $offSet);
		$this->arrData['deduction']	= $this->model_deduction_management->getAllDeduction();
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		}
		$this->arrData['arrEmployees'] = $this->employee->getEmployees();

		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		$this->arrData["arrEmployees"] = $finalResult;
		// print_r($this->arrData['employees']);exit;
		if($delete == 2 && (int)$DeductionID)
		{
			$this->arrData['emploanRequest']	= $this->model_deduction_management->getEmployeeloanRequest($DeductionID);
		}
		$this->template->write_view('content', 'deduction_management/deduction_CCTV_request', $this->arrData);
		$this->template->render();
	}

	public function loan_approval_view($LoanID = 0) {
		$this->load->model('model_loan_management');
		
		$this->arrData['arrRecords']	= $this->model_loan_management->getLoaneApprovalViews($LoanID);
		// print_r($this->arrData['arrRecord']);
		// exit;
		$this->template->write_view('content', 'loan_management/transfer_approval_view', $this->arrData);
		$this->template->render();
	}
	
	public function deduction_employees($pageNum = 1,$transferID = 0) {
		
		// print_r($delete);
		// exit;
		$this->load->model('model_transfer_management');
		
		// if((int)$delete == 2 && (int)$transferID) {
		// 	// print_r("dsfasdfasdf");
		// 	// exit;

		// 	$this->template->write_view('content', 'transfer_management/transfer_requests', $this->arrData);
		// 	$this->template->render();

		// }
		
		if($this->input->post('TransferFrom'))
		{
			#################################### FORM VALIDATION START ####################################		
					
			$this->form_validation->set_rules('TransferFrom', 'Leave Type', 'trim|numeric|required|xss_clean');
			$this->form_validation->set_rules('TransferTo', 'Leave Category', 'trim|numeric|required|xss_clean');
			$this->form_validation->set_rules('TransferReason', 'Reason', 'trim|required|xss_clean');
			
			#################################### FORM VALIDATION END ####################################
			
		
		if ($this->form_validation->run() == true) {
			
			#	SUPPORTING DOCUMENT (IF ANY)
			$uploadPicConfig['upload_path'] 	= $this->arrData["docFolder"];
			$uploadPicConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp|doc|docs|pdf';
			$uploadPicConfig['max_size']		= '2048';
			$uploadPicConfig['max_filename']	= '100';
			$uploadPicConfig['encrypt_name']	= true;

			$this->load->library('upload');
			$this->upload->initialize($uploadPicConfig);
			
			$docFileName = '';
			
			if(!$this->upload->do_upload('TransferDoc')) {
				if(!empty($_FILES['TransferDoc']['name'])) {
					$this->arrData['error_message'] = $error['error'];
					$error = array('error' => $this->upload->display_errors());	
					
						//echo $error['error'] ;
				}					
			} else {				
				$dataUpload = $this->upload->data();
				$docFileName = basename($dataUpload['file_name']);
				//echo $docFileName;
			}
										
			$arrValues = array(
								'emp_id' => $this->input->post("emp_id"),
								'transfer_from' => $this->input->post("TransferFrom"),
								'transfer_to' => $this->input->post("TransferTo"),
								'transfer_reason' => $this->input->post("TransferReason"),
								'transfer_status' => 0,
								'created_by' => $this->userEmpNum
							);
			
			if(!empty($docFileName)) {
				$arrValues['transfer_doc'] = $docFileName;
				
			}
			
			
			
			if((int)$transferID) {
					
				$this->model_transfer_management->saveValues(TABLE_TRANSFERS, $arrValues, array('l.transfer_id' => (int)$transferID, 'l.emp_id' => $this->userEmpNum, 'l.transfer_status' => 0));
				
				
				//var_dump($arrValues);
			
				
				$this->session->set_flashdata('success_message', 'Application updated successfully');
				
			} else {
				
				$arrValues['created_date'] 	= date($this->arrData["dateTimeFormat"]);
						
				// print_r($arrValues);
				// exit;
				$this->model_transfer_management->saveValues(TABLE_TRANSFERS, $arrValues);
			
				#	SHOOT EMAIL
				
				$arrTo = array();
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
				$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => $this->arrData['arrEmployee']['emp_id']));
				foreach($arrSupervisors as $arrSupervisor) {
					$arrTo[] = $arrSupervisor['emp_work_email'];
				}
				
				if(count($arrTo)) {
					
					$arrValues = array(
										'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
										'[EMPLOYEE_NAME]' 			=> getSupervisorName($this->arrData['arrEmployee']['emp_id']),
										'[APPLICANT_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
										'[DASHBOARD_LINK]' 			=> $this->baseURL . '/' . $this->currentController . '/leave_requests',
										'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'leave_request_notification.html');
					
					$this->sendEmail(
										$arrTo, 																	# RECEIVER DETAILS
										'Leave Request Notification' . EMAIL_SUBJECT_SUFFIX,						# SUBJECT
										$emailHTML																	# EMAIL HTML MESSAGE
									);
				}
				
				$this->session->set_flashdata('success_message', 'Application submitted successfully');
				
			}
				redirect($this->baseURL . '/' . $this->currentController . '/transfer_requests');
			
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		}
		
		$this->arrData['totalRecordsCount']	= $this->model_transfer_management->getTotalLeavesForApproval($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_transfer_management->getTransfersForApproval($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmLeaveRequests');
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
				
		$this->arrData['arrCategories'] 	= $this->configuration->getValues(TABLE_TRANSFERS, '*');
		$arrWhereSupervisor 				= array('e.emp_status' => STATUS_ACTIVE); //, 'e.emp_code >' => ZERO
		$this->arrData['arrSupervisors'] 	= $this->employee->getSupervisors($arrWhereSupervisor);
		$arrJobCategories 					= $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' 		=> STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$finalResult 						= array();

		$this->arrData['companies']			= $this->model_transfer_management->getCompanies();
		$emp_id_updated = !empty($this->arrData['arrEmployee']['emp_id']) ? $this->arrData['arrEmployee']['emp_id'] : $this->userEmpNum; 
		// $this->arrData['employee_branch']	= $this->model_transfer_management->getEmployeeBranch($emp_id_updated);
		$this->arrData['all_employees']		= $this->model_transfer_management->getAllEmployeeBranch();
		// print_r($this->arrData['all_employees']);
		// exit;
		$this->arrData['arrSupervisors'] 	= $finalResult;
		$this->arrData['arrTypes'] 			= $this->config->item('leave_types');
		$this->arrData['arrTypes'] 			= $this->config->item('leave_types');
		
		// print_r($this->arrData);
		// exit;
		$this->template->write_view('content', 'transfer_management/transfer_employees', $this->arrData);
		$this->template->render();
	}

	public function view_branches($pageNum = 1, $transferID = 0) {
		$this->load->model('model_transfer_management');
		$this->load->model('model_system_configuration');
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		$arrWhere = array();
		
		$arrWhere['emp_id'] 				= $this->userEmpNum;
		if($this->input->post("region_id")) {
			$arrWhere['region_id'] 	= $this->input->post("region_id");
		}		

		if($this->input->post("location_id")) {
			$arrWhere['company_country_id'] 	= $this->input->post("location_id");
		}		
		
		$offSet 							= ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] 		 = $this->model_system_configuration->getBranches($arrWhere, $this->limitRecords, $offSet);
		// print_r($this->arrData['arrRecords']);
		// exit;
		
		$this->arrData['arrSupervisors'] 	= $this->model_transfer_management->getSupervisors($this->userEmpNum);
		$supers 	= $this->arrData['arrSupervisors'];
		$super_key 	= 'supervisor_emp_id';

		$super_id =array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
		  }, $supers);

		  $this->arrData['arrSupervisorsIds'] 	= $super_id;
		  // print_r($this->userEmpNum);
		// print_r($super_id);
		// exit;
		// print_r($this->userEmpNum);
		// exit;
		// $this->arrData['upperRoles'] 		= $this->model_transfer_management->getTransfers($arrWhere, $this->limitRecords, $offSet);
		// print_r($this->arrRoleIDs);
		// exit;
		$numPages 							= ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] 		= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmTranferStatus');
		$this->arrData['companies']			= $this->model_transfer_management->getCompanies();
		$this->arrData['regions']			= $this->model_system_configuration->getRegions();
		$this->arrData['countries']			= $this->model_system_configuration->getLocations();
		
		$this->arrData['frmActionURL'] 		= $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		// print_r($arrData);
		// exit;
		// $this->arrData['arrCategories'] 	= $this->configuration->getValues(TABLE_ATTENDANCE_LEAVE_CATEGORIES, '*');		
		// $this->arrData['arrSupervisors'] 	= $this->employee->getSupervisors();
		// print_r($this->arrData);
		// exit;
		// $this->arrData['arrTypes'] 			= $this->config->item('leave_types');

		// print_r($this->arrData['arrRecords']);
		// exit;

		$this->template->write_view('content', 'transfer_management/view_branches', $this->arrData);
		$this->template->render();
	}
	
	public function att_detail() {
		//New Controller CRUD
		// $this->load->model('model_transfer_management', 'attendance', true);
		// $data["fetch_timing"] = $this->main_model->fetch_timing();
		// // $this->load->view('attendance_detail', $data);
		// $this->load->view("attendance_detail", $data);
		// //Ends Here
		//NEW CONTROLLER FUNCTION STARTS
		$this->load->model('model_transfer_management');
		$this->load->model('model_employee_management');
		//$this->load->model('model_transfer_management', 'attendance', true);
		$data["fetch_timing"] = $this->model_transfer_management->fetch_timing();
		$this->load->view("transfer_management/attendance_detail", $data);
		//ENDS HERE

		$this->arrRoleIDs[] = ACCOUNT_MANAGER_ROLE_ID;
		$this->arrRoleIDs[] = ACCOUNT_EMPLOYEE_ROLE_ID;
		
		$this->arrData['displayClocking'] = false;
		
		$arrWhere['USERID'] = (int)$this->employeeCode;
		$this->arrData['empCode'] = (int)$this->employeeCode;
		$this->arrData['empID'] = (int)$this->employeeID;
		
		/*if($this->input->post("dateFrom")) {
			$arrWhere['DATE >= '] = $this->input->post("dateFrom");
		} else {
			$arrWhere['DATE >= '] = $this->dateFrom;
		}
		$this->arrData['dateFrom'] = $arrWhere['DATE >= '];
		
		if($this->input->post("dateTo")) {
			$arrWhere['DATE <= '] = $this->input->post("dateTo");
		} else {
			$arrWhere['DATE <= '] = $this->dateTo;
		}
		$this->arrData['dateTo'] = $arrWhere['DATE <= '];*/
		
		if((int)$this->input->post("selMonth") && (int)$this->input->post("selYear")) {
			$arrWhere['DATE >= '] = $this->input->post("selYear") . '-' . $this->input->post("selMonth") . '-01';
			$arrWhere['DATE <= '] = $this->input->post("selYear") . '-' . $this->input->post("selMonth") . '-' . date('t', strtotime($arrWhere['DATE >= ']));
		} else {
			$arrWhere['DATE >= '] = $this->dateFrom;
			$arrWhere['DATE <= '] = $this->dateTo;
		}
		
		$this->arrData['selMonth'] = date('m', strtotime($arrWhere['DATE >= ']));
		$this->arrData['selYear'] = date('Y', strtotime($arrWhere['DATE >= ']));
		
		$this->arrData['dateFrom'] = $arrWhere['DATE >= '];
		$this->arrData['dateTo'] = $arrWhere['DATE <= '];
		
		$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		//$arrEmpWhere['e.emp_code > '] = ZERO;
		
		//if(!in_array($this->userRoleID, $this->arrData["forcedAccessRoles"])) {
		$arrJobCategoryWhere = array(
									'job_category_status' => STATUS_ACTIVE,
									'order_by' => 'job_category_name'
									);
									
		if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
		} else if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) { 
			$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
		}		
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$arrEmpWhere['e.emp_location_id'] = $this->userLocationID;
		}
		
		$_POST['sort_field'] = 'e.emp_code';
		$_POST['sort_order'] = 'ASC';
		
		$arrEmployees = $this->model_employee_management->getEmployees(array('e.emp_id' => $this->userEmpNum));
		$arrEmployees = array_merge($arrEmployees, $this->model_employee_management->getEmployees($arrEmpWhere));
		$arrJobCategories = $this->model_employee_management->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrJobCategoryWhere);
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		unset($_POST['sort_field']);
		unset($_POST['sort_order']);
		
		if($this->arrData['empID']) {
			
			$objAllDates = new DatePeriod(
				 new DateTime($this->arrData['dateFrom']),
				 new DateInterval('P1D'),
				 new DateTime(date('Y-m-d', strtotime($this->arrData['dateTo'] . ' +1 day')))
			);
			
			$this->arrData['arrAllDates'] = array();
			foreach ($objAllDates as $strKey => $strValue) {
				$this->arrData['arrAllDates'][] = $strValue->format('Y-m-d');
			}
			
			$arrCompany = $this->configuration->getCompanies(array('company_id' => $this->arrData['arrEmployee']['emp_company_id']));
			$locationID = $arrCompany[0]['company_country_id'];
			$this->arrData['arrHolidays'] = $this->configuration->getValues(TABLE_PUBLIC_HOLIDAYS, "*", array('holiday_country_id' => $locationID, 'holiday_date >= ' => $arrWhere['DATE >= '], 'holiday_date <= ' => $arrWhere['DATE <= ']));
			
			$arrWhere['DATE >= '] = $arrWhere['DATE >= '] . ' 00:00:00';
			$arrWhere['DATE <= '] = $arrWhere['DATE <= '] . ' 23:59:59';
			
			// if($this->companyID == 1) {
			// 	$this->arrData['arrRecords'] = $this->model_transfer_management->getAttendanceRecordsPK($arrWhere);
			// } else {
			// 	$this->arrData['arrRecords'] = $this->model_transfer_management->getAttendanceRecordsDxb($arrWhere);
			// }
			
			$this->arrData['totalRecordsCount'] = count($this->arrData['arrRecords']);
			
			if($this->input->post("txtExport") == 1) {
				
				$arrJobCategory = $this->configuration->getJobCategories(array('job_category_id' => $this->arrData['arrEmployee']['emp_job_category_id']));
				
				$strHTML = '';
				
				for($ind = 0; $ind < count($this->arrData['arrAllDates']); $ind++) {
					
					$dateIndex = array_search($this->arrData['arrAllDates'][$ind], array_column($this->arrData['arrRecords'], 'DATE'));
					
					if($dateIndex !== false) {
						$jnd = $dateIndex;
					} else {
						$jnd = -1;
					}
					
					$rowBGColor = '';
					$publicHoliday = '';
					$holidayIndex = array_search($this->arrData['arrAllDates'][$ind], array_column($this->arrData['arrHolidays'], 'holiday_date'));
					
					if(date('N', strtotime($this->arrData['arrAllDates'][$ind])) == $this->arrData['arrEmployee']['company_weekly_off_1'] || date('N', strtotime($this->arrData['arrAllDates'][$ind])) == $this->arrData['arrEmployee']['company_weekly_off_2'] || $holidayIndex !== false) {
						$rowBGColor = ' bgcolor="#8FB05C" style="color:#FFF"';
						if($holidayIndex !== false) {
							$publicHoliday = $this->arrData['arrHolidays'][$holidayIndex]['holiday_name'];
						}
					}
					
					// TO HIDE TODAY'S OUT TIME TO AVOID CONFUSION
		
					if($this->arrData['arrAllDates'][$ind] == date('Y-m-d')) {
						$this->arrData['arrRecords'][$jnd]['OUT'] = '';
					}
					
				  
				  $strDate = readableDate($this->arrData['arrAllDates'][$ind], 'jS M, Y');
				  $strDay = date('l', strtotime($this->arrData['arrAllDates'][$ind]));
				  $strIN = ($this->arrData['arrRecords'][$jnd]['IN'] != '') ? date('g:i A', strtotime($this->arrData['arrRecords'][$jnd]['IN'])) : '-';
				  $strOUT = ($this->arrData['arrRecords'][$jnd]['OUT'] != '') ? date('g:i A', strtotime($this->arrData['arrRecords'][$jnd]['OUT'])) : '-';
				  if($publicHoliday != '') {
					  $strHrs = $publicHoliday;
				  } else {
					  if($this->arrData['arrRecords'][$jnd]['IN'] != '' && $this->arrData['arrRecords'][$jnd]['OUT'] != '') { 
						  $strTime1 = new DateTime($this->arrData['arrRecords'][$jnd]['IN']);
						  $strTime2 = new DateTime($this->arrData['arrRecords'][$jnd]['OUT']);
						  $intInterval = $strTime1->diff($strTime2);
						  if($intInterval->format('%h') > 0 || $intInterval->format('%i') > 0) {
							  $strHrs = $intInterval->format('%h')." hr ".$intInterval->format('%i')." min";
						  }
					  } else {
						  $strHrs = '-';
					  }
				  }
				  
				  $strNotes = getAttendanceNotes($this->arrData['empID'], $this->arrData['arrAllDates'][$ind]);				  
				  if(trim($strNotes) == '') {
				  	$strNotes = '-';
				  }
				  
				  $strLeave = getIfLeaveApplied($this->arrData['empID'], $this->arrData['arrAllDates'][$ind]);
				  if(trim($strLeave) == '') {
				  	$strLeave = '-';
				  }
				  
				  $strHTML .= '<tr ' . $rowBGColor . ' height="30px">
					<td class="listContentCol">' . $strDate . '</td>
					<td class="listContentCol">' . $strDay . '</td>
					<td class="listContentCol">' . $strIN . '</td>
					<td class="listContentCol">' . $strOUT . '</td>
					<td class="listContentCol">' . $strHrs . '</td>
					<td class="listContentCol">' . $strLeave . '</td>
					<td class="listContentCol">' . $strNotes . '</td>
				  </tr>';
				}
				
				$arrValues = array(
									  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
									  '[OFFICE_ADDRESS]' 			=> $arrCompany[0]['company_name'] . '<br />&nbsp;&nbsp;' . $arrCompany[0]['company_address'],
									  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),
									  '[MONTH]' 					=> strtoupper(date('F', mktime(0, 0, 0, (int)$this->input->post("selMonth"), 10))),
									  '[YEAR]' 						=> $this->input->post("selYear"),
									  '[EMPLOYEE_CODE]' 			=> $this->arrData['arrEmployee']['emp_code'],
									  '[EMPLOYEE_DESIGNATION]' 		=> $this->arrData['arrEmployee']['emp_designation'],
									  '[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
									  '[EMPLOYEE_JOINING_DATE]' 	=> date(SHOW_DATE_TIME_FORMAT, strtotime($this->arrData['arrEmployee']['emp_joining_date'])),
									  '[EMPLOYEE_DEPARTMENT]' 		=> $arrJobCategory[0]['job_category_name'],
									  '[EMPLOYEE_SUPERVISOR]' 		=> getSupervisorName($this->arrData['arrEmployee']['emp_id']),
									  '[ATTENDANCE_DETAIL]'			=> $strHTML
									  );
									  
				  $strHTML = getHTML($arrValues, 'attendance.html');
				  
				  require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

				  $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
				  // set document information
				  $pdf->SetCreator(PDF_CREATOR);
				  $pdf->SetAuthor(PDF_AUTHOR);
				  $pdf->setCellHeightRatio(1);
				  $pdf->setPrintHeader(false);
				  $pdf->setPrintFooter(false);
				  $pdf->SetFontSize(8);				  
				  // set margins
				  $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);				  
				  // set auto page breaks
				  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
				  // set image scale factor
				  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
				  // set some language-dependent strings (optional)
				  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
					  require_once(dirname(__FILE__).'/lang/eng.php');
					  $pdf->setLanguageArray($l);
				  }
				  
				  // add a page
				  $pdf->AddPage();			  
				  // output the HTML content
				  $pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
				  $pdf->lastPage();
				  $pdfFileName = 'Attendance_' . str_replace(' ', '_', $this->arrData['arrEmployee']['emp_full_name']) . '_' . $this->input->post("selMonth") . '_' . $this->input->post("selYear") . '.pdf';
				  $pdf->Output($pdfFileName, 'D');
				  
				  /*header('Content-Type: text/doc');
				  header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
				  header('Cache-Control: max-age=0');
				  readfile('./' . PDF_FILES_FOLDER . $pdfFileName);*/
				  
				  exit;
			}
		}
		
		$this->arrData['arrEmploymentStatuses'] = $this->model_employee_management->populateEmploymentStatus();
		// $this->arrData['csvinfo'] = 'sdfsdfsdfdaf';
		$this->template->write_view('content', 'transfer_management/attendance_detail', $this->arrData);
		$this->template->render();
	}
	
	public function clocking_detail($empID = 0, $strDate = '') {
		
		$arrWhere = array();
		
		if(trim($strDate) == '') {
			$strDate = date(strtotime('now'), 'Y-m-d');
		}
		
		$arrWhere['DATE >= '] = $strDate . ' 00:00:00';
		$arrWhere['DATE <= '] = $strDate . ' 23:59:59';
		$arrWhere['USERID'] = $this->arrData['arrEmployee']['emp_code'];
		
		if($this->companyID == 1) {
			$this->arrData['arrRecords'] = $this->model_transfer_management->getClockingRecordsPK($arrWhere);
		} else {
			$this->arrData['arrRecords'] = $this->model_transfer_management->getClockingRecordsDxb($arrWhere);
		}
		$this->arrData['strDate'] = $strDate;
		
		$this->template->write_view('content', 'transfer_management/clocking_detail', $this->arrData);
		$this->template->render();
	}

	public function employment_details($employeeID = 0) {
		
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->arrData['record'] = array();
		
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$arrWhere = array(
						'emp_id' => $employeeID
						);
		
		if(!isAdmin($this->userRoleID)) {
			
			# SET LOG
			
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		#################################### FORM VALIDATION START ####################################	
		
		$this->form_validation->set_rules('empCompany', 'Company', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empID', 'Employee Id', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empDepartment', 'Department', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empDesignation', 'Designation', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empWorkEmail', 'Work EMail', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('empJobLocation', 'Job Location', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empEmploymentStatus', 'Employment Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empJoiningDate', 'Joining Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empProbationEndDate', 'Probation End Date', 'trim|required|xss_clean');
		
		if($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_CONFIRMED) {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|required|xss_clean');
		} else {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		}
		
		$this->form_validation->set_rules('empAnnualLeaves', 'Annual Leaves', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empSickLeaves', 'Sick Leaves', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empFlexiLeaves', 'Flexi Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empEduLeaves', 'Educational/Training Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empMaternityLeaves', 'Maternity/Paternity Leaves', 'trim|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ######################################
		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post() && (int)$this->arrData['canWrite'])
			{
				$arrValues = array(
								'emp_code' => $this->input->post("empID"),
								'emp_company_id' => $this->input->post("empCompany"),
								'emp_job_category_id' => $this->input->post("empDepartment"),
								'emp_designation' => $this->input->post("empDesignation"),
								'emp_location_id' => $this->input->post("empJobLocation"),
								'emp_ot_eligibility' => $this->input->post("empOT"),
								'emp_ip_num' => $this->input->post("empIPNum"),
								'emp_work_email' => $this->input->post("empWorkEmail"),
								//'emp_authority_id' => (int)$this->input->post("empSupervisor"),
								'emp_currency_id' => $this->input->post("empCurrency"),
								'emp_employment_type' => $this->input->post("empEmploymentType"),
								'emp_employment_status' => $this->input->post("empEmploymentStatus"),
								'emp_joining_date' => $this->input->post("empJoiningDate"),
								'emp_probation_end_date' => $this->input->post("empProbationEndDate"),
								'emp_confirmation_date' => $this->input->post("empConfirmationDate"),
								'emp_visa_company_id' => $this->input->post("empVisaCompany"),
								'emp_visa_issue_date' => $this->input->post("empVisaIssueDate"),
								'emp_visa_expiry_date' => $this->input->post("empVisaExpiryDate"),
								'emp_nic_issue_date' => $this->input->post("empNICIssueDate"),
								'emp_nic_expiry_date' => $this->input->post("empNICExpiryDate"),
								'emp_labour_card_issue_date' => $this->input->post("empLabourIssueDate"),
								'emp_labour_card_expiry_date' => $this->input->post("empLabourExpiryDate"),
								'emp_sponsor_id' => $this->input->post("empSponsor"),
								'emp_annual_leaves' => $this->input->post("empAnnualLeaves"),
								'emp_sick_leaves' => $this->input->post("empSickLeaves"),
								'emp_flexi_leaves' => $this->input->post("empFlexiLeaves"),
								'emp_edu_leaves' => $this->input->post("empEduLeaves"),
								'emp_maternity_leaves' => $this->input->post("empMaternityLeaves")
								);
										
				if($employeeID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				} else {
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				}
								
				if($employeeID) {
					$result = $this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
					
					if((int)$this->input->post("empEmploymentStatus") < STATUS_EMPLOYEE_SEPARATED) {
						$userStatusID = STATUS_ACTIVE;
					} else {
						$userStatusID = STATUS_INACTIVE_VIEW;
					}
					
					$this->employee->saveValues(	TABLE_USER, 
													array('user_status' => $userStatusID), 	# WHAT TO UPDATE
													array('employee_id' => $employeeID)		# WHERE
												);
				}
				
				$arrSupervisors = $this->input->post("empSupervisor");
					
				if(count($arrSupervisors)) {
					
					$this->master->deleteValue(TABLE_EMPLOYEE_SUPERVISORS, null, array('emp_id' => $employeeID));
					
					foreach($arrSupervisors as $strKey => $strValue) {
						$arrValues = array(					
									'emp_id' => $employeeID,
									'supervisor_emp_id' => (int)$strValue,
									'created_by' => $this->userEmpNum,
									'created_date' => date($this->arrData["dateTimeFormat"])		
									);
									
						$this->employee->saveValues(TABLE_EMPLOYEE_SUPERVISORS, $arrValues);
					}
				}
								
				if($result)
				{
					# SHOOT EMAIL
					
					$strEmail = $this->input->post("empWorkEmail");
					if(!empty($strEmail)) {
						
						$arrUserDetails = $this->configuration->getValues(TABLE_USER, ' user_name, plain_password ', array('employee_id' => $employeeID));
						$arrUserDetails = $arrUserDetails[0];
						
						/*$arrUserName = explode('@', $strEmail);
						$userName = trim($arrUserName[0]);
						
						$userName = trim($arrUserDetails['user_name']);
						
						if($userName != $arrUserDetails['user_name']) {
							$userResult = $this->configuration->saveValues(TABLE_USER, array('user_name' => $userName), array('employee_id' => $employeeID));
						}*/
				
						$arrValues = array(
											'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
											'[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
											'[EMPLOYEE_DASHBOARD_LINK]' => $this->baseURL,
											'[EMPLOYEE_USERNAME]' 		=> $arrUserDetails['user_name'],
											'[EMPLOYEE_PASSWORD]' 		=> $arrUserDetails['plain_password'],
											'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'details_update_notification_and_login_details.html');
						
						$this->sendEmail(
											$arrTo = array(
															$strEmail
															), 															# RECEIVER DETAILS
											'Employment Details Update Notification' . EMAIL_SUBJECT_SUFFIX,			# SUBJECT
											$emailHTML																	# EMAIL HTML MESSAGE
										);
					}
					
					# SET LOG
					debugLog("Employment Details Updated: [EmpID/Code: ".$employeeID."/".$this->input->post("empID")."]");
					
					$this->session->set_flashdata('success_message', 'Information saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
					exit;
				} else {
					$this->arrData['error_message'] = 'Data not saved, try again';
				}
				
				
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$this->arrData['record'] = $this->arrData['arrEmployee'];
		}
		
		# CODE FOR PAGE CONTENT
		//$this->arrData['empGrades'] = $this->employee->populateGrades();
		$this->arrData['empWorkShifts'] = $this->employee->populateWorkShifts();
		$this->arrData['empSupervisors'] = $this->employee->getSupervisors(array('e.emp_id != ' => $employeeID));
		$this->arrData['empEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['empDepartments'] = $this->employee->populateDepartments();
		$this->arrData['empCompanies'] = $this->configuration->getCompanies();
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['empSponsors'] = $this->configuration->getSponsors();
		$this->arrData['employmentTypes'] = $this->config->item('employment_types');
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/employment_detail', $this->arrData);
		$this->template->render();
	}

	public function importcsv()
	{
		$this->load->library('csvimport');
		$this->load->model('model_transfer_management');
		// print_r($_FILES["csv_file"]["tmp_name"]);
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
		// print_r($file_data);
		// exit;
		foreach($file_data as $row)
		{
			$data[] = array(
				'employee_id'	=>	$row["Employee_ID"],
        		'att_date'		=>	$row["Date"],
        		'att_in'		=>	$row["In"],
        		'att_out'		=>	$row["Out"],
			);
		}
		// print_r($data);
		// exit;
		$this->model_transfer_management->insert($data);
	}
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */