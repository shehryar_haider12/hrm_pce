<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
//	$config['base_url']	= '';
$config['base_url']	= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 'https://' : 'http://' . $_SERVER['HTTP_HOST'];
$config['base_url'] = $config['base_url']."/HRM";

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
$config['index_page'] = 'index.php';

/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of 'AUTO' works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol']	= 'AUTO';

/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language']	= 'english';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the 'hooks' feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = FALSE;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'MY_';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify with a regular expression which characters are permitted
| within your URLs.  When someone tries to submit a URL with disallowed
| characters they will get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| By default CodeIgniter enables access to the $_GET array.  If for some
| reason you would like to disable it, set 'allow_get_array' to FALSE.
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The other items let you set the query string 'words' that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger']	= 'c';
$config['function_trigger']		= 'm';
$config['directory_trigger']	= 'd'; // experimental not currently in use

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 0;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| application/logs/ folder. Use a full server path with trailing slash.
|
*/
$config['log_path'] = '';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Session class you
| MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = '_@sbT-hRM_';

/*
|--------------------------------------------------------------------------
| Session Variables
|--------------------------------------------------------------------------
|
| 'sess_cookie_name'		= the name you want for the cookie
| 'sess_expiration'			= the number of SECONDS you want the session to last.
|   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
| 'sess_expire_on_close'	= Whether to cause the session to expire automatically
|   when the browser window is closed
| 'sess_encrypt_cookie'		= Whether to encrypt the cookie
| 'sess_use_database'		= Whether to save the session data to a database
| 'sess_table_name'			= The name of the session database table
| 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
| 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
| 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
|
*/
$config['sess_cookie_name']		= 'ci_session';
$config['sess_expiration']		= 7200;
$config['sess_expire_on_close']	= FALSE;
$config['sess_encrypt_cookie']	= FALSE;
$config['sess_use_database']	= FALSE;
$config['sess_table_name']		= 'ci_sessions';
$config['sess_match_ip']		= FALSE;
$config['sess_match_useragent']	= TRUE;
$config['sess_time_to_update']	= 300;

/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
|
| 'cookie_prefix' = Set a prefix if you need to avoid collisions
| 'cookie_domain' = Set to .your-domain.com for site-wide cookies
| 'cookie_path'   =  Typically will be a forward slash
| 'cookie_secure' =  Cookies will only be set if a secure HTTPS connection exists.
|
*/
$config['cookie_prefix']	= "";
$config['cookie_domain']	= "";
$config['cookie_path']		= "/";
$config['cookie_secure']	= FALSE;

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = FALSE;

/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not 'echo' any values with compression enabled.
|
*/
$config['compress_output'] = FALSE;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = FALSE;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';

/*	Define Standard Constants	*/
$config['css_path'] 				= 	$config['base_url']."/css/";
$config['style_css_path'] 			= 	$config['base_url']."/css/style.css";
$config['image_path'] 				= 	$config['base_url']."/images";
$config['script_path'] 				= 	$config['base_url']."/js";
$config['docs_path'] 				= 	$config['base_url']."/docs";
$config['hr_admin_emp_id']			= 	0;
$config['attendance_db_pk']			= 	array(
												'UID'				=> 'att_user',
												'PWD'				=> '',
												'Database'			=> 'Attendance',
												'ConnectionPooling' => 0,
												'CharacterSet'		=> 'UTF-8',
												'ReturnDatesAsStrings' => 1
											);
$config['localhost']			= 	array(
												'UID'				=> 'root',
												'PWD'				=> '',
												'Database'			=> 'Attendance',
												'ConnectionPooling' => 0,
												'CharacterSet'		=> 'UTF-8',
												'ReturnDatesAsStrings' => 1
											);
$config['attendance_db_dxb']		= 	array(
												'UID'				=> 'att_user',
												'PWD'				=> '',
												'Database'			=> 'COSEC',
												'ConnectionPooling' => 0,
												'CharacterSet'		=> 'UTF-8',
												'ReturnDatesAsStrings' => 1
											);
$config['employment_types']			=	array(
										'N/A',
										'Limited',
										'Unlimited'
									);
$config['employee_benefits']		=	array(
										'Annual Leaves',
										'Extra Hours',
										'Gratuity',
										'Medical Insurance',
										'Provident Fund',
										'Public Holidays',
										'Ticket Adult',
										'Ticket Child',
										'Ticket Infant',
										'Weekly Off'
									);
$config['blood_groups']				=	array(
										'A+',
										'A-',
										'B+',
										'B-',
										'O+',
										'O-',
										'AB+',
										'AB-'
									);
$config['salutations']				=	array(
										'Mr.',
										'Ms.',
										'Mrs.',
										'Miss',
										'Dr.',
										'Eng.',
										'Prof.',
										'Lady',
										'Sir'
									);
$config['education_levels']			=	array(
										'Doctorate (PhD)',
										'MPhil',
										'MBBS/D-Pharm/BDS',
										'Masters in Business Administration',
										'Masters in Engineering',
										'Masters (MA, Mcom, MSc or equivalent)',
										'Bachelors in Business Administration',
										'Bachelors in Engineering',
										'Bachelors (BA, Bcom, BSc or equivalent)',
										'Certification',
										'Associate Degree',
										'Diploma',
										'Intermediate/A-Level',
										'Matriculation/O-Level',
										'Non-Matriculation'
									);
$config['genders']					=	array(
										'Male',
										'Female'
									);
$config['marital_status']			=	array(
										'Single',
										'Engaged',
										'Married',
										'Divorced',
										'Widowed',
										'Separated'
									);
$config['dependent_relationships']	=	array(
										'Mother',
										'Father',
										'Grand Mother',
										'Grand Father',
										'Brother',
										'Sister',
										'Spouse',
										'Child',
										'Uncle',
										'Aunt',
										'Other Relative'
									);
$config['interview_ratings']		=	array(
										'Poor' 			=> '2',
										'Fair/Average' 	=> '4',
										'Good' 			=> '6',
										'Excellent' 	=> '8',
										'Outstanding' 	=> '10'
									);
$config['office_ips']				=	array(
										# PAKISTAN
										'43.225.98.254',
										'127.0.0.1'
									);
$config['allowed_roles']			= array(2, 10);
$config['allowed_employees']		= array(1);
$config['forced_access_roles']		= array(HR_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID);
$config['complain_priorities']	=	array(
										'3' => 'Normal',
										'2' => 'Important',
										'1' => 'Urgent'
									);

$config['complain_statuses']	=	array(
										'1' => 'Open',
										'2' => 'In Progress',
										'3' => 'Complete',
										'4' => 'Incomplete'
									);
									
$config['complain_types']	=	array(
										'1' => 'Suggestion',
										'2' => 'Complain',
										'3' => 'Request'
									);
									
$config['leave_types']			=	array(
										'1' => 'Full Day'
									//	'2' => 'Half Day'
									);
									
$config['vendor_priorities']	=	array(
										'1' => 'Primary',
										'2' => 'Secondary',
										'3' => 'Tertiary',
										'4' => 'Other'
									);
									
$config['days']					=	array(
										'1' => 'Monday',
										'2' => 'Tuesday',
										'3' => 'Wednesday',
										'4' => 'Thursday',
										'5' => 'Friday',
										'6' => 'Saturday',
										'7' => 'Sunday'
									);
									
$config['months']				=	array(
										'1' => 'January',
										'2' => 'February',
										'3' => 'March',
										'4' => 'April',
										'5' => 'May',
										'6' => 'June',
										'7' => 'July',
										'8' => 'August',
										'9' => 'September',
										'10' => 'October',
										'11' => 'November',
										'12' => 'December'
									);
									
$config['resignation_steps']	=	array(
										'1' => 'Manager/Supervisor',
										'2' => 'HR Department',
										'3' => 'Manager/Supervisor',
										'4' => 'IT-Web Department',
										'5' => 'Network Administrator',
										'6' => 'Admin Department',
										'7' => 'HR Department',
										'8' => 'Finance Department'
									);
									
$config['resignation_reasons']	=	array(
										'Another Position',
										'Attendance',
										'Favouritism',
										'Internal Politics',
										'Lay Off',
										'Personal Reasons',
										'Position Eliminated',
										'Relocation',
										'Reorganization',
										'Retirement',
										'Return to School/Studies',
										'Violation of Company Policy',
										'Work Stress and Pressure',
										'Other'
									);
									
$config['talent_contest_status']	=	array(
										'1' => 'In Progress',
										'2' => 'Publish'
									);
									
$config['talent_contest_visibility_status']	=	array(
												'1' => 'Active',
												'2' => 'InActive'
											);								

$config['talent_interview_status']	=	array(
										'1' => 'Approved',
										'2' => 'Rejected'
									);
									
$config['poll_status']	=	array(
										'1' => 'In Progress',
										'2' => 'Submit for Approval & Publish',
										'3' => 'Approve & Publish',
										'4' => 'DisApprove',
										'5' => 'Close'
									);
									
$config['poll_visibility_status']	=	array(
												'1' => 'Active',
												'2' => 'InActive'
											);
											
$config['poll_results_display']			=	array(
												'1' => 'Show Results to Voters',
												'0' => 'Hide Results'
											);
											
$config['performance_status']		=	array(
												'0' => 'Entering Tasks',
												'1' => 'Submit Tasks for Review',
												'2' => 'Submit Review to HR',
											);
											
$config['assessment_status']		=	array(
												'0' => 'Supervisor Entering Details',
												'1' => 'Employee Entering Remarks',
												'2' => 'Submitted to HR',
											);
											
$config['qc_reports_frequency']			=	array(
												'1' => 'Daily',
												'2' => 'Monthly'
											);
											
$config['arrival_source_web_marketing']	=	array(
												'Online Advertisement',
												'Facebook',
												'Google Search'
											);
											
$config['task_statuses']				=	array(
												'1' => 'Open',
												'2' => 'In Progress',
												'3' => 'Completed',
												'4' => 'InCompleted'
											);
											
$config['payment_modes']				=	array(
												'Bank',
												'Bank-IT',
												'Cash',
												'Cheque',
												'WPS'
											);
											
$config['kpi_assessment']				=	array(
												'1' => 'Excellent',
												'2' => 'Needs Improvement',
												'3' => 'Meets Expectations',
												'4' => 'Exceeds Expectations',
												'5' => 'Unsatisfactory'
											);
/* End of file config.php */
/* Location: ./application/config/config.php */


function __autoload($class)
{
    if (strpos($class, 'CI_') !== 0)
    {
        @include_once(APPPATH . 'libraries/' . $class . EXT);
    }
} 
