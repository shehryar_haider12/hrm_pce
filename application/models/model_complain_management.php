<?php
class Model_Complain_Management extends Model_Master {
	
	function __construct() {
		 parent::__construct();	
	}
	
	function getIssues($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' ei.*,e1.emp_id AS reported_by_id, e1.emp_full_name AS reported_by,e1.emp_code,e1.emp_ip_num,e1.emp_photo_name,e2.emp_id AS reported_to_id, e2.emp_full_name AS reported_to ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = ei.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e2 ', 'e2.emp_id = ei.complain_report_to_id', 'left');
				
		if((int)($arrWhere['ei.emp_id'])) {
			$this->db->where('(ei.emp_id = '.$arrWhere['ei.emp_id'].' OR ei.complain_report_to_id IN ('.$arrWhere['ei.emp_id'].'))');		
			unset($arrWhere['ei.emp_id']);
		}
		if((int)($arrWhere['ei.emp_id_custom'])) {
			$this->db->where('ei.emp_id', $arrWhere['ei.emp_id_custom']);		
			unset($arrWhere['ei.emp_id_custom']);
		}
		
		if((int)($arrWhere['for_cron']) == 1) {
			$this->db->where('ei.emp_id = ei.complain_last_replied_by AND ei.complain_status <= '.IN_PROGRESS.'');
			unset($arrWhere['for_cron']);
		} else if((int)($arrWhere['for_cron']) == 2) {
			$this->db->where('ei.emp_id != ei.complain_last_replied_by AND ei.complain_status <= '.IN_PROGRESS.'');
			unset($arrWhere['for_cron']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if(!isset($_POST['sort_field']) || trim($_POST['sort_field']) == '') {
			$this->db->order_by('ei.complain_status', 'ASC');
			$this->db->order_by('ei.complain_id', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('ei.' . $sortColumn, $sortOrder);
			}
		}		
		// $this->db->order_by('ei.created_date', 'DESC');
		
		$results = $this->db->get(TABLE_EMPLOYEE_COMPLAINS . ' ei ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getTotalIssues($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = ei.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e2 ', 'e2.emp_id = ei.complain_report_to_id', 'left');
		
		if((int)($arrWhere['ei.emp_id'])) {
			$this->db->where('(ei.emp_id = '.$arrWhere['ei.emp_id'].' OR ei.complain_report_to_id IN ('.$arrWhere['ei.emp_id'].'))');		
			unset($arrWhere['ei.emp_id']);
		}
		if((int)($arrWhere['ei.emp_id_custom'])) {
			$this->db->where('ei.emp_id', $arrWhere['ei.emp_id_custom']);		
			unset($arrWhere['ei.emp_id_custom']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$results = $this->db->get(TABLE_EMPLOYEE_COMPLAINS . ' ei ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getIssueDetails($employeeID = 0,$issueID = 0)
	{
		$this->db->select(' ech.*,e1.emp_full_name,e1.emp_photo_name,e1.emp_gender', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = ech.emp_id', 'left');
				
		if((int)$issueID) {
			$this->db->where('(ech.complain_id = '.$issueID.')');		
		}
		
		if((int)($employeeID) && (!isAdmin($this->userRoleID))) {
			$this->db->where('(ec.emp_id = '.$employeeID.' OR ec.complain_report_to_id = '.$employeeID.')');
		}
		
		$this->db->order_by('ech.complain_history_id', 'ASC');
		
		$this->db->join(TABLE_EMPLOYEE_COMPLAINS . ' ec ', 'ec.complain_id = ech.complain_id', 'left');
		$results = $this->db->get(TABLE_EMPLOYEE_COMPLAINS_HISTORY . ' ech ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
		
	}
	
	function getIssuesUnChecked($arrWhere = array())
	{		
		$this->db->select(' count(`is_checked`) as total_complains ');
		
		if(isset($arrWhere['emp_id'])) {
			$this->db->where('(complain_last_replied_by != ' . (int)$arrWhere['emp_id'] . ')');
			$this->db->where('(emp_id = ' . (int)$arrWhere['emp_id'] . ' OR complain_report_to_id = '.(int)$arrWhere['emp_id'].')');
			unset($arrWhere['emp_id']);
		}	
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);	
		}
		
		$this->db->where('complain_status <= '.IN_PROGRESS.'');
		
		$results = $this->db->get(TABLE_EMPLOYEE_COMPLAINS);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getTasksUnChecked($arrWhere = array())
	{		
		$this->db->select(' count(`is_checked`) as total_tasks ');
		
		if(isset($arrWhere['emp_id'])) {
			$this->db->where('(task_last_replied_by != ' . (int)$arrWhere['emp_id'] . ')');
			$this->db->where('(emp_id = ' . (int)$arrWhere['emp_id'] . ' OR task_report_to_id = '.(int)$arrWhere['emp_id'].')');
			unset($arrWhere['emp_id']);
		}	
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);	
		}
		
		$this->db->where('task_status <= '.IN_PROGRESS.'');
		
		$results = $this->db->get(TABLE_EMPLOYEE_TASKS);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getImmediateSupervisor($arrWhere = array())
	{
		$this->db->select(' e.emp_id, e.emp_code, e.emp_full_name, e.emp_work_email, e.emp_authority_id, e1.emp_full_name AS emp_authority_name, e1.emp_work_email AS emp_authority_work_email ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = e.emp_authority_id AND e1.emp_status = 1', 'left');
		$this->db->where('e.emp_status', ''.STATUS_ACTIVE.'');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);	
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getReportToEmployees($arrWhere = array())
	{
		$this->db->select(' e.emp_id, e.emp_code, e.emp_job_category_id, jc.job_category_name, e.emp_designation as emp_job_title_id, e.emp_full_name ');		
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
		
		$this->db->where('(e.emp_job_category_id in ('.REQUEST_TO_DEPARTMENT_CATEGORY_IDS.') AND u.user_role_id > 1 AND e.emp_status = '.STATUS_ACTIVE.' AND e.emp_employment_status < '.STATUS_EMPLOYEE_SEPARATED.')');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('e.emp_full_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getLastIssueDetails($arrWhere = array())
	{
		$this->db->select(' * ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('ech.complain_history_id', 'DESC');
		
		$this->db->limit(1);
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_COMPLAINS_HISTORY . ' ech ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getTotalTasks($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = et.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e2 ', 'e2.emp_id = et.task_report_to_id', 'left');
		
		if((int)($arrWhere['et.emp_id'])) {
			$this->db->where('(et.emp_id = '.$arrWhere['et.emp_id'].' OR et.task_report_to_id IN ('.$arrWhere['et.emp_id'].'))');		
			unset($arrWhere['et.emp_id']);
		}
		if((int)($arrWhere['et.emp_id_custom'])) {
			$this->db->where('et.emp_id', $arrWhere['et.emp_id_custom']);		
			unset($arrWhere['et.emp_id_custom']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$results = $this->db->get(TABLE_EMPLOYEE_TASKS . ' et ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getTasks($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' et.*,e1.emp_id AS reported_by_id, e1.emp_full_name AS reported_by,e1.emp_code,e1.emp_ip_num,e1.emp_photo_name,e1.emp_work_email AS reported_by_email,e2.emp_id AS reported_to_id, e2.emp_full_name AS reported_to, e2.emp_work_email AS reported_to_email ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = et.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e2 ', 'e2.emp_id = et.task_report_to_id', 'left');
				
		if((int)($arrWhere['et.emp_id'])) {
			$this->db->where('(et.emp_id = '.$arrWhere['et.emp_id'].' OR et.task_report_to_id IN ('.$arrWhere['et.emp_id'].'))');		
			unset($arrWhere['et.emp_id']);
		}
		if((int)($arrWhere['et.emp_id_custom'])) {
			$this->db->where('et.emp_id', $arrWhere['et.emp_id_custom']);		
			unset($arrWhere['et.emp_id_custom']);
		}
		
		if((int)($arrWhere['for_cron']) == 1) {
			$this->db->where('et.emp_id = et.task_last_replied_by AND et.task_status <= '.IN_PROGRESS.'');
			unset($arrWhere['for_cron']);
		} else if((int)($arrWhere['for_cron']) == 2) {
			$this->db->where('et.emp_id != et.task_last_replied_by AND et.task_status <= '.IN_PROGRESS.'');
			unset($arrWhere['for_cron']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if(!isset($_POST['sort_field']) || trim($_POST['sort_field']) == '') {
			$this->db->order_by('et.task_status', 'ASC');
			$this->db->order_by('et.task_id', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('et.' . $sortColumn, $sortOrder);
			}
		}		
		
		$results = $this->db->get(TABLE_EMPLOYEE_TASKS . ' et ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getTaskDetails($employeeID = 0,$taskID = 0)
	{
		$this->db->select(' eth.*,e1.emp_full_name,e1.emp_photo_name,e1.emp_gender', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = eth.emp_id', 'left');
				
		if((int)$taskID) {
			$this->db->where('(eth.task_id = '.$taskID.')');		
		}
		
		if((int)($employeeID) && (!isAdmin($this->userRoleID))) {
			$this->db->where('(et.emp_id = '.$employeeID.' OR et.task_report_to_id = '.$employeeID.')');
		}
		
		$this->db->order_by('eth.task_history_id', 'ASC');
		
		$this->db->join(TABLE_EMPLOYEE_TASKS . ' et ', 'et.task_id = eth.task_id', 'left');
		$results = $this->db->get(TABLE_EMPLOYEE_TASKS_HISTORY . ' eth ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getLastTaskDetails($arrWhere = array())
	{
		$this->db->select(' * ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('eth.task_history_id', 'DESC');
		
		$this->db->limit(1);
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_TASKS_HISTORY . ' eth ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
}