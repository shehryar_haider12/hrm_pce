<?php
class Model_deduction_Management extends Model_Master {
	
	public $strHierarchy;
		
	function __construct() {
		parent::__construct();
	}
	function fetch_timing(){
		$query = $this->db->get("hrm_attendancce");
		return $query;
	}
	function insert($data)
	{
		$this->db->insert_batch('hrm_attendancce', $data);
	}
	
	function getAttendanceRecordsPK($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true, $filterStr = false) {
				
		$db = mysqli_connect(localhost, $this->config->item('localhost'));
		$strQuery = "SELECT U.Badgenumber as USERID, min(C.CHECKTIME) as 'IN', max(C.CHECKTIME) as 'OUT', cast(C.CHECKTIME as date) as 'DATE' FROM " . TABLE_ATTENDANCE_PK . " C left join USERINFO U ON U.USERID = C.USERID WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " U.Badgenumber = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " U.Badgenumber in (" . $arrWhere['USERID in '] . ")";
		}
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " AND C.CHECKTIME <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " AND C.CHECKTIME >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " GROUP BY U.Badgenumber, cast(C.CHECKTIME as date) ORDER BY U.Badgenumber, 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getAttendanceRecordsDxb($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true, $filterStr = false) {
				
		$db = mysqli_connect(ATTENDANCE_DB_HOST_DXB, $this->config->item('attendance_db_dxb'));
		$strQuery = "SELECT UserID AS USERID, max(Punch1) as 'IN', max(OutPunch) as 'OUT', cast(PDate as date) as 'DATE' FROM " . TABLE_ATTENDANCE_DXB . " WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " USERID = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " USERID in (" . $arrWhere['USERID in '] . ")";
		}
		
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " and PDate <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " and PDate >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " GROUP BY USERID, cast(PDate as date) ORDER BY USERID, 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getClockingRecordsPK($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$db = mysqli_connect(localhost, $this->config->item('localhost'));
		$strQuery = "SELECT U.Badgenumber as USERID, C.CHECKTIME as 'DATE' FROM " . TABLE_ATTENDANCE_PK . " C left join USERINFO U ON U.USERID = C.USERID WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " U.Badgenumber = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " U.Badgenumber in (" . $arrWhere['USERID in '] . ")";
		}
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " AND C.CHECKTIME <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " AND C.CHECKTIME >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " ORDER BY 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getClockingRecordsDxb($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$db = mysqli_connect(ATTENDANCE_DB_HOST_DXB, $this->config->item('attendance_db_dxb'));
		$strQuery = "SELECT UserID AS USERID, Edatetime as 'DATE' FROM " . TABLE_ATTENDANCE_CLOCKING_DXB . " WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " USERID = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " USERID in (" . $arrWhere['USERID in '] . ")";
		}
		
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " and Edatetime <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " and Edatetime >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " ORDER BY 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getSupervisors($emp_id){
		$objResult = $this->db->query('Select supervisor_emp_id from hrm_employee_supervisors where emp_id = '.$emp_id);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}

	function getLoan($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		if($arrWhere) {
			// print_r("SELECT el1.loan_id, el1.emp_id, e1.emp_full_name, el1.loan_date, el1.loan_Reason, el1.loan_amount, el1.loan_type, el1.loan_comments, el1.loan_attachment, el1.procesed_by, el1.procesed_date, el1.loan_status FROM hrmbackup.hrm_employee_loan el1 INNER JOIN hrmbackup.hrm_employee e1 ON el1.emp_id = e1.emp_id WHERE ".$arrWhere['custom_string'] . " " .$arrWhere['LoanStatus'] . " " .$arrWhere['emp_id']);exit;
			// $objResult = $this->db->query("SELECT el1.loan_id, el1.emp_id, e1.emp_full_name, el1.loan_date, el1.loan_Reason, el1.loan_amount, el1.loan_type, el1.loan_comments, el1.loan_attachment, el1.procesed_by, el1.procesed_date, el1.loan_status FROM hrmbackup.hrm_employee_loan el1 INNER JOIN hrmbackup.hrm_employee e1 ON el1.emp_id = e1.emp_id WHERE ".$arrWhere['custom_string'] . " " .$arrWhere['LoanStatus'] . " " .$arrWhere['emp_id'] . " order by cd1.created_date DESC " );
		}else{
			$objResult = $this->db->query("SELECT ed1.id, ed1.`name`, ed1.price, e1.emp_full_name, ed1.created_date FROM hrmbackup.hrm_employee_deduction ed1 INNER JOIN hrmbackup.hrm_employee e1 ON ed1.created_by = e1.emp_id order by ed1.created_date DESC ");
		}
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			return $arrResult;
	}

	function getCCTVdeduction($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		if($arrWhere) {
			// print_r("SELECT el1.loan_id, el1.emp_id, e1.emp_full_name, el1.loan_date, el1.loan_Reason, el1.loan_amount, el1.loan_type, el1.loan_comments, el1.loan_attachment, el1.procesed_by, el1.procesed_date, el1.loan_status FROM hrmbackup.hrm_employee_loan el1 INNER JOIN hrmbackup.hrm_employee e1 ON el1.emp_id = e1.emp_id WHERE ".$arrWhere['custom_string'] . " " .$arrWhere['LoanStatus'] . " " .$arrWhere['emp_id']);exit;
			$objResult = $this->db->query("SELECT cd1.id, cd1.emp_id, e1.emp_full_name AS emp_name, ed1.`name`, e2.emp_full_name AS operator_name, cd1.deduction_status, cd1.created_date, cd1.processed_by, cd1.processed_date FROM hrmbackup.hrm_cctc_deduction cd1 
			INNER JOIN hrmbackup.hrm_employee e1 ON cd1.emp_id = e1.emp_id 
			INNER JOIN hrmbackup.hrm_employee e2 ON cd1.created_by = e2.emp_id
			INNER JOIN hrmbackup.hrm_employee_deduction ed1 ON cd1.deduction_id = ed1.id WHERE ".$arrWhere['strDateFrom'] . " " .$arrWhere['deductionStatus'] . " " .$arrWhere['emp_id'] . " order by cd1.id DESC ");
		}else{
			$objResult = $this->db->query("SELECT cd1.id, cd1.emp_id, e1.emp_full_name AS emp_name, ed1.`name`, e2.emp_full_name AS operator_name, cd1.deduction_status, cd1.created_date, cd1.processed_by, cd1.processed_date FROM hrmbackup.hrm_cctc_deduction cd1 
			INNER JOIN hrmbackup.hrm_employee e1 ON cd1.emp_id = e1.emp_id 
			INNER JOIN hrmbackup.hrm_employee e2 ON cd1.created_by = e2.emp_id
			INNER JOIN hrmbackup.hrm_employee_deduction ed1 ON cd1.deduction_id = ed1.id order by cd1.id DESC " );
		}
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			return $arrResult;
	}
	
	function getAllDeduction() {
			$objResult = $this->db->query("SELECT id, `name` FROM hrmbackup.hrm_employee_deduction ");
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			return $arrResult;
	}
	
	function getTotalTransfers($arrWhere = array()) {
		
		$this->db->select(' count(*) as total_count ');
		
		foreach($arrWhere as $key => $value) {
			// print_r($key);
			// exit;
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
			// print_r($arrWhere);
			// exit;
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		// print_r($arrWhere);
		// exit;
		$objResult = $this->db->get(TABLE_TRANSFERS . ' l ');
		// $objResult = $this->db->get(TABLE_TRANSFERS);
		// $objResult = $this->db->query('select * from '.TABLE_TRANSFERS);
		$arrResult = $objResult->result_array();
		// print_r($arrResult);
		// exit;
		// $objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
		// return $objResult->num_rows;
	}
	
	function getTransfersForApproval($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		// $this->db->distinct();
		$objResult = $this->db->query('SELECT t1.transfer_id, t1.emp_id,e1.emp_full_name, t1.transfer_reason, t1.transfer_status, t1.transfer_comments, t1.processed_by, c1.company_name AS company_from, c2.company_name AS company_to
		FROM hrm_employee_transfers t1
		INNER JOIN hrm_companies c1 ON t1.transfer_from = c1.company_id
		INNER JOIN hrm_companies c2 ON t1.transfer_to = c2.company_id
		INNER JOIN hrm_employee e1 ON t1.emp_id = e1.emp_id ');
		
		// foreach($arrWhere as $key => $value) {
		// 	if(strpos($key, ' in ')) {
		// 		$this->db->where($key, $arrWhere[$key], false);
		// 		unset($arrWhere[$key]);
		// 	}
			
		// 	if(strpos($key, 'custom_string') !== false) {
		// 		$this->db->where($arrWhere[$key], NULL, false);
		// 		unset($arrWhere[$key]);
		// 	}
		// }
		
		// if(count($arrWhere)) {
		// 	$this->db->where($arrWhere);			
		// }
		
		// if((int)$rowsLimit > 0) {
		// 	$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		// }		
		
		// $this->db->order_by('l.transfer_id', 'DESC');
		
		// $objResult = $this->db->get(TABLE_TRANSFERS . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTransfereApprovalViews($transfer_id) {
		$objResult = $this->db->query('SELECT t1.td_id, t1.transfer_id, e1.emp_full_name, e1.emp_work_email, e1.emp_designation, t1.approval_status, t1.attachment, t1.comments FROM hrmbackup.transfer_details t1 INNER JOIN hrmbackup.hrm_employee e1 ON t1.supervisor_id = e1.emp_id WHERE t1.transfer_id ='. $transfer_id);
		
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($arrResult);exit;
		return $arrResult;
	}

	function getTotalLeavesForApproval($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' count(*) as total_count ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getLeavesSummary($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db_attend = $this->load->database('attendance', TRUE);
		
		if(count($arrWhere)) {
			$this->db_attend->where($arrWhere);			
		}		
		
		$this->db_attend->order_by('emp_code', 'ASC');
		
		$objResult = $this->db_attend->get(TABLE_ATTENDANCE_SUMMARY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getLeaveDetails($leaveID) {
		
		$this->db->select(' l.*, lc.leave_category, e.emp_full_name, e.emp_designation, e.emp_annual_leaves, e.emp_sick_leaves, e.emp_flexi_leaves, e.emp_edu_leaves, e.emp_maternity_leaves, e1.emp_full_name as processed_by_name ');
		$this->db->join(TABLE_ATTENDANCE_LEAVE_CATEGORIES . ' lc ', 'lc.leave_category_id = l.leave_category', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = l.processed_by', 'left');
		
		$this->db->where(array('leave_id' => (int)$leaveID));
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}

	function getCompanies(){
		$objResult = $this->db->query('SELECT * from '.TABLE_COMPANIES);

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}

	function getEmployeeBranch($emp_id){
		$objResult = $this->db->query("SELECT * from hrm_employee hrm_companies where hrm_employee.emp_company_id = hrm_companies.company_id and hrm_employee.emp_id = ".$emp_id);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function getAllEmployeeBranch(){
		// $objResult = $this->db->query("SELECT * from hrm_employee, hrm_companies where hrm_employee.emp_company_id = hrm_companies.company_id");
		$objResult = $this->db->query("SELECT * from hrm_employee");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($arrResult);
		// exit;
		
		return $arrResult;
	}

	function updateDeduction($deduction_id, $emp_id, $date){
		// $deductions = $this->db->query("SELECT transfer_id,emp_id,transfer_to from hrm_employee_transfers WHERE transfer_id = ".$transfer_id);
		// $trarrResult = $transfers->result_array();
		// $transfers->free_result();
		// return $trarrResult;
		// print_r("UPDATE ". TABLE_CCTV_DEDUCTION ." SET deduction_status = '1', processed_by = '".$emp_id."', processed_date = '".$date."' where id = ".$deduction_id);exit;
		$deductions = $this->db->query("UPDATE ". TABLE_CCTV_DEDUCTION ." SET deduction_status = '1', processed_by = '".$emp_id."', processed_date = '".$date."' where id = ".$deduction_id);
		// $trarrResult = $deductions->result_array();
		// $deductions->free_result();
		return $deductions;
		
	}

	function deleteDeductionStatus($deduction_id, $emp_id, $date){
		$objResult = $this->db->query("UPDATE ".TABLE_CCTV_DEDUCTION." SET deduction_status = '2', processed_by = '".$emp_id."', processed_date = '".$date."'  where id = ".$deduction_id);
		return $objResult;
	}

	function getEmployeeloanRequest($DeductionID){
		$objResult = $this->db->query("SELECT * FROM hrmbackup.hrm_employee_deduction WHERE id = ".$DeductionID);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult[0];
	}
	
	function getEMPpayrollMonth($emp_id, $month, $year){
		// print_r("SELECT * FROM hrmbackup.hrm_cctc_deduction WHERE emp_id = ".$emp_id." AND YEAR(`created_date`)=".$year." AND MONTH(`created_date`) = ".$month ." AND `deduction_status` = 1");exit;
		$objResult = $this->db->query("SELECT sum(price) AS price FROM hrmbackup.hrm_cctc_deduction cd1 INNER JOIN hrm_employee_deduction ed1 ON cd1.deduction_id = ed1.id WHERE cd1.emp_id = ".$emp_id." AND YEAR(cd1.created_date)=".$year." AND MONTH(cd1.created_date) = ".$month." AND cd1.deduction_status = 1");
		$arrResult = $objResult->result_array();
		// $objResult->free_result();
		return $arrResult[0]['price'];
	}

	function getPayrollSummary($emp_id, $month, $year){
		// print_r("SELECT * FROM hrmbackup.hrm_cctc_deduction cd1 INNER JOIN hrm_employee_deduction ed1 ON cd1.deduction_id = ed1.id WHERE cd1.emp_id = ".$emp_id." AND YEAR(cd1.created_date)=".$year." AND MONTH(cd1.created_date) = ".$month." AND cd1.deduction_status = 1");exit;
		$objResult = $this->db->query("SELECT cd1.id, e1.emp_full_name, e1.emp_code, e1.emp_joining_date, e1.emp_designation, ed1.name, ed1.price ,cd1.created_date, e2.emp_full_name AS processed_name FROM hrmbackup.hrm_cctc_deduction cd1 
		INNER JOIN hrm_employee_deduction ed1 ON cd1.deduction_id = ed1.id
		INNER JOIN hrm_employee e1 ON cd1.emp_id = e1.emp_id
		INNER JOIN hrm_employee e2 ON cd1.processed_by = e2.emp_id
		WHERE cd1.emp_id = ". $emp_id ." AND YEAR(cd1.created_date)= ". $year ." AND MONTH(cd1.created_date) = ".$month." AND cd1.deduction_status = 1");
		$arrResult = $objResult->result_array();
		// print_r($arrResult);exit;
		return $arrResult;
	}

	function getBranches(){
		$objResult = $this->db->query("SELECT * FROM hrm_companies");
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}

	function getBrancheEmployee($branch_id){
		
		$objResult = $this->db->query("SELECT * FROM hrm_employee WHERE emp_company_id = ".$branch_id);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return count($arrResult);
	}
	function getTransfersEmployees($arrWhere = array(), $rowsLimit = '', $rowsOffset = ''){
		
		$objResult = $this->db->query("SELECT  t1.transfer_id,t1.emp_id,c3.company_name AS current_branch,e2.emp_full_name AS processed_name,e3.emp_full_name AS created_name,e1.emp_full_name,e1.emp_code, t1.transfer_reason, t1.transfer_status, t1.transfer_comments, t1.processed_by,t1.created_by,t1.processed_date,t1.created_date,re1.region_id,re1.region_name, c1.company_name AS company_from, c2.company_name AS company_to
		FROM hrm_employee_transfers t1
		INNER JOIN hrm_companies c1 ON t1.transfer_from = c1.company_id
		INNER JOIN hrm_companies c2 ON t1.transfer_to = c2.company_id
		INNER JOIN hrm_regions re1 ON c1.company_region_id = re1.region_id
		INNER JOIN hrm_employee e2 ON t1.processed_by = e2.emp_id AND t1.processed_by != 0
		INNER JOIN hrm_companies c3 ON e2.emp_company_id = c3.company_id
		INNER JOIN hrm_employee e3 ON t1.created_by = e3.emp_id AND t1.created_by != 0
		-- LEFT JOIN hrm_employee e4 ON e4.emp_company_id = c1.company_id
		INNER JOIN hrm_employee e1 ON t1.emp_id = e1.emp_id WHERE t1.transfer_status = 1  GROUP BY t1.emp_id 
		ORDER BY t1.created_date DESC");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($arrResult);
		// exit;
		return $arrResult;
	}
	
	function getAllTransfersByEmployee($emp_id){
		$objResult = $this->db->query("SELECT  t1.transfer_id,t1.emp_id,c3.company_name AS current_branch,e2.emp_full_name AS processed_name,e3.emp_full_name AS created_name,e1.emp_full_name,e1.emp_joining_date,e1.emp_code,e1.emp_designation, t1.transfer_reason, t1.transfer_status, t1.transfer_comments, t1.processed_by,t1.created_by,t1.processed_date,t1.created_date, c1.company_name AS company_from, c2.company_name AS company_to
		FROM hrm_employee_transfers t1
		INNER JOIN hrm_companies c1 ON t1.transfer_from = c1.company_id
		INNER JOIN hrm_companies c2 ON t1.transfer_to = c2.company_id
		LEFT JOIN hrm_employee e2 ON t1.processed_by = e2.emp_id
		LEFT JOIN hrm_employee e3 ON t1.created_by = e3.emp_id AND t1.created_by != 0
		INNER JOIN hrm_companies c3 ON e3.emp_company_id = c3.company_id
		INNER JOIN hrm_employee e1 ON t1.emp_id = e1.emp_id WHERE t1.transfer_status = 1 AND t1.emp_id = ".$emp_id." 
		ORDER BY t1.created_date ASC");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($arrResult);
		// exit;
		return $arrResult;
	}

	function getEmployeeRegion($emp_id){
		$objResult = $this->db->query("SELECT emp_id,emp_company_id,r1.region_id,r1.region_name From hrm_employee 
		INNER JOIN hrm_companies c1 ON hrm_employee.emp_company_id = c1.company_id
		INNER JOIN hrm_regions r1 ON c1.company_region_id = r1.region_id 
		WHERE emp_id = ".$emp_id."
		");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult[0];
	}
	
	function getEmployeeCompany($emp_id){
		$objResult = $this->db->query("SELECT emp_id,emp_company_id,c1.company_name From hrm_employee 
		INNER JOIN hrm_companies c1 ON hrm_employee.emp_company_id = c1.company_id
		WHERE emp_id = ".$emp_id."
		");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult[0];
	}
}
?>