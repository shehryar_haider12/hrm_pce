<?php
class Model_Master extends CI_Model {
	
	function __construct() {
		 parent::__construct();	
	}
	
	function saveValues($tblName, $arrData, $arrWhere = array()) {
		
		if(count($arrWhere)) {
				
			foreach($arrWhere as $key => $value) {
				if(strpos($key, ' in ')) {
					$this->db->where($key, $arrWhere[$key], false);
					unset($arrWhere[$key]);
				}
			}
			
			foreach($arrWhere as $strKey => $strVal) {
				if(strpos($strKey, '.')) {
					$arrWhere[substr($strKey, (strpos($strKey, '.') + 1))] = $strVal;
					unset($arrWhere[$strKey]);
				}
			}
			
			$this->db->update($tblName, $arrData, $arrWhere);
			return ($this->db->affected_rows() > 0);
		} else {
			$this->db->insert($tblName, $arrData);
			return $this->db->insert_id();
		}
		
	}
	
	function deleteValue($tblName, $arrDeleteValues = array(), $arrDeleteWhere = array())
	{
		if(count($arrDeleteWhere)) {
			$this->db->where($arrDeleteWhere);
			$this->db->delete($tblName);
			return ($this->db->affected_rows() > 0);
		}
	}
	
	function getValues($tblName, $colSelect = '*', $arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select($colSelect);
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['order_by'] != '') {
			$this->db->order_by($arrWhere['order_by'], 'ASC');
			unset($arrWhere['order_by']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get($tblName);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function checkValues($tableName, $Where = array(), $column = 0, $strID = 0) {
		if(count($Where)) {
			$this->db->select(' count(*) as total ');
			$this->db->where($Where);
			
			if($strID) {
				$this->db->where($column.' not in ', '(' . $strID . ')', false);
			}
			
			$objResult = $this->db->get($tableName);
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if($arrResult[0]['total'] > 0){
				return $arrResult = NO;
			} else {
				return $arrResult = YES;
			}
			
		}
	}
}
?>