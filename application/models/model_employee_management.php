<?php
class Model_Employee_Management extends Model_Master {
	
	public $strHierarchy;
	private $tblNoDeletion	= array(
									TABLE_EMPLOYEE
									);
	
	function __construct() {
		 parent::__construct();	
	}

	function deleteValue($tblName, $arrDeleteValues = array(), $colStatus = '', $valStatus = 0) {
		
		if(count($arrDeleteValues)) {
			
			$this->db->where($arrDeleteValues);
			if(!in_array($tblName, $this->tblNoDeletion)) {
				$this->db->delete($tblName);
			} else {
				$arrValues = array(
									'deleted_by' => $this->userEmpNum,
									'deleted_date' => date(DATE_TIME_FORMAT)
									);
									
				if($tblName == TABLE_EMPLOYEE) {
					$arrValues['emp_status'] = STATUS_INACTIVE_VIEW;
				}
									
				if(!empty($colStatus) && strlen($colStatus) > 7 && is_numeric($valStatus)) {
					$arrValues[$colStatus] = $valStatus;
				}
				
				$this->db->update($tblName, $arrValues, $arrDeleteValues);
			}
			return ($this->db->affected_rows() > 0);
			
		}
	}
	
	function getEmployees($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->distinct();
		$this->db->select(' e.*, u.user_role_id, ur.user_role_name, u.employee_id as user_exists, l.location_name, jc.job_category_name ');
		
		/*
		(select max(created_date) from '.TABLE_EMPLOYEE_EDUCATION.' ec where ec.emp_id = e.emp_id) as education_created, 
		(select max(modified_date) from '.TABLE_EMPLOYEE_EDUCATION.' em where em.emp_id = e.emp_id) as education_modified, 
		(select max(created_date) from '.TABLE_EMPLOYEE_WORK_EXPERIENCE.' wc where wc.emp_id = e.emp_id) as work_created, 
		(select max(modified_date) from '.TABLE_EMPLOYEE_WORK_EXPERIENCE.' wm where wm.emp_id = e.emp_id) as work_modified  ');
		*/
		
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'ur.user_role_id = u.user_role_id', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = e.emp_location_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if((int)$arrWhere['emp_role_id']) {
			$this->db->where('u.user_role_id', $arrWhere['emp_role_id']);
			unset($arrWhere['emp_role_id']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_full_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if($arrWhere['edu.edu_level_id'] || $arrWhere['edu.edu_major_id']) {			
			$this->db->join(TABLE_EMPLOYEE_EDUCATION . ' edu ', 'edu.emp_id = e.emp_id', 'left');
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if($doSort) {
			if(!isset($_POST['sort_field']) || $_POST['sort_field'] == '') {
				$this->db->order_by('e.emp_id', 'DESC');
			} else if($this->currentController == 'employee_management') {
				$sortColumn = $_POST['sort_field'];
				$sortOrder = $_POST['sort_order'];
				
				if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
					$this->db->order_by($sortColumn, $sortOrder);
				}
				$this->db->order_by('e.emp_id', 'DESC');
			} else {
				$this->db->order_by('e.emp_id', 'DESC');
			}
		}
		
		/*if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', STATUS_ACTIVE);
		}*/
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}

	function getEmployeeName() {
		$objResult = $this->db->query("SELECT emp_id, emp_full_name FROM ". TABLE_EMPLOYEE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($objResult);exit;
		
		return $arrResult;
	}
	
	function getTotalEmployees($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' count(e.emp_id) as total_records ');
				
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = e.emp_location_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if((int)$arrWhere['emp_role_id']) {
			$this->db->where('u.user_role_id', $arrWhere['emp_role_id']);
			unset($arrWhere['emp_role_id']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_full_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if($arrWhere['edu.edu_level_id'] || $arrWhere['edu.edu_major_id']) {			
			$this->db->join(TABLE_EMPLOYEE_EDUCATION . ' edu ', 'edu.emp_id = e.emp_id', 'left');
		}
		
		if($arrWhere['em.location_id']) {			
			$this->db->join(TABLE_EMPLOYEE_MARKETS . ' em ', 'em.emp_id = e.emp_id', 'left');
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if(!isset($_POST['sort_field']) || $_POST['sort_field'] == '') {
			$this->db->order_by('e.emp_id', 'ASC');
		} else if($this->currentController == 'employee_management') {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		$arrResult = (int)$arrResult[0]['total_records'];
		
		return $arrResult;
	}
	
	function getEmployeesSorted($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('e.emp_full_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getEmployeeDetail($arrWhere = array(), $boolSystem = true) {
		
		$this->db->distinct();
		$this->db->select(' e.*, c.company_name, c.company_weekly_off_1, c.company_weekly_off_2, u.user_status, u.password, jc.job_category_name ');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
			
		if(!isAdmin($this->userRoleID) && $boolSystem) {
			$this->db->where('es.supervisor_emp_id', $this->userEmpNum);
		}			
			
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		if(count($arrResult) == 1) {
			$arrResult = $arrResult[0];
		}
		
		return $arrResult;
	}

	function getEmployeesalary($emp_id) {
		// print_r('SELECT emp_basic_salary FROM '. TABLE_EMPLOYEE .' WHERE emp_id = '. $emp_id);exit;
		$objResult = $this->db->query('SELECT emp_basic_salary FROM '. TABLE_EMPLOYEE .' WHERE emp_id = '. $emp_id);			
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getInterviewerEmployees($arrRoleIDs = array()) {
		
		$this->db->select(' e.emp_id, e.emp_code, e.emp_full_name ');
		$this->db->where('e.emp_status', STATUS_ACTIVE);
		
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where('u.user_role_id in ', '('.implode(',', $arrRoleIDs).')', false);
		
		$this->db->or_where('e.emp_id in ', '(select distinct interviewer_id from '.TABLE_CANDIDATE_INTERVIEW_INTERVIEWER.')', false);
		$this->db->order_by('e.emp_full_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getSupervisors($arrWhere = array()) {
		
		// print_r($arrWhere);
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->select(' e.emp_id, e.emp_code, e.emp_job_category_id, e.emp_full_name ');
		$this->db->where('e.emp_status', STATUS_ACTIVE);
		
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where('u.user_role_id in ', '('.INTERVIEWERS_ROLE_IDS.')', false);
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		//$this->db->or_where('e.emp_id in ', '(select distinct interviewer_id from '.TABLE_CANDIDATE_INTERVIEW_INTERVIEWER.')', false);
		$this->db->order_by('e.emp_full_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getManagers($arrWhere = array()) {
		
		$this->db->select(' e.emp_id, e.emp_code, e.emp_job_category_id, e.emp_full_name ');
		$this->db->where('e.emp_status', STATUS_ACTIVE);
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where('u.user_role_id in ', '('.MANAGER_ROLE_IDS.')', false);
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		//$this->db->or_where('e.emp_id in ', '(select distinct interviewer_id from '.TABLE_CANDIDATE_INTERVIEW_INTERVIEWER.')', false);
		$this->db->order_by('e.emp_full_name', 'ASC');
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function getRegions() {
		// print_r('SELECT * FROM hrm_employee e1 INNER JOIN hrm_regions r1 ON e1.emp_region_id = r1.region_id');exit;
		$objResult = $this->db->query('SELECT * FROM hrm_regions where region_status = 1');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function getEmpSupervisors($arrWhere = array()) {
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->select(' supervisor_emp_id ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_SUPERVISORS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getEmpSupervisorsDetails($arrWhere = array()) {
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->select(' e.* ');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.supervisor_emp_id = e.emp_id', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function notExistingEmployee($strField = 'emp_id', $strValue = 0, $strID = 0)
	{	
		$this->db->select(' count(*) as count ');
		$this->db->where($strField, $strValue);
		
		if($strID) {
			$this->db->where('emp_id not in ', '(' . $strID . ')', false);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		if($arrResult[0]['count'] >= 1) {
			return false;
		}
		return true;
	}
	
	function getEducationalHistory($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' e.*, l.edu_level_name, m.edu_major_name ');
		$this->db->join(TABLE_EDUCATION_LEVELS . ' l ', 'l.edu_level_id = e.edu_level_id', 'left');
		$this->db->join(TABLE_EDUCATION_MAJORS . ' m ', 'm.edu_major_id = e.edu_major_id', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('e.edu_level_id', 'ASC');
		$objResult = $this->db->get(TABLE_EMPLOYEE_EDUCATION . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getWorkHistory($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('work_to', 'DESC');
		$objResult = $this->db->get(TABLE_EMPLOYEE_WORK_EXPERIENCE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getDependents($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_DEPENDENTS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getEmergencyContacts($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_EMERGENCY_CONTACTS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getLanguages($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' el.*, l.language_name ');
		$this->db->join(TABLE_LANGUAGE . ' l ', 'l.language_id = el.language_id', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_LANGUAGE . ' el ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getMarkets($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' em.*, l.location_name ');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = em.location_id', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_MARKETS . ' em ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getActivities($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' ea.*, e.emp_full_name, e.emp_ip_num ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ea.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('ea.created_date', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_LM_ACTIVITIES . ' ea ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getActivityDocs($activityID) {
				
		$this->db->where('activity_id', $activityID);
		
		$objResult = $this->db->get(TABLE_LM_ACTIVITIES_DOCS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalResignations($arrWhere = array()) {
		
		$this->db->select(' count(er.resignation_id) as total_records ');				
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = er.resignation_emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_RESIGNATION . ' er ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		$arrResult = (int)$arrResult[0]['total_records'];
		
		return $arrResult;
	}
	
	function populateLanguages()
	{
		$this->db->select(' language_id,language_name ');
		$this->db->where('language_status', STATUS_ACTIVE);
		$results = $this->db->get(TABLE_LANGUAGE);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateMarkets()
	{
		$this->db->select(' location_id,location_name ');
		$this->db->where('location_type_id in ', '(1,2)', false);
		$this->db->where('location_status', STATUS_ACTIVE);
		$results = $this->db->get(TABLE_LOCATION);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateGrades()
	{
		$this->db->select(' grade_id,grade_code,grade_title,job_type ');
		$this->db->where('grade_status', STATUS_ACTIVE);
		$this->db->order_by('job_type, grade_code', 'ASC');
		$results = $this->db->get(TABLE_GRADES);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function populateJobTitles()
	{
		$this->db->select(' job_title_id,job_title_name ');
		$this->db->where('job_title_status', STATUS_ACTIVE);
		$this->db->order_by('job_title_name', 'ASC');
		$results = $this->db->get(TABLE_JOB_TITLE);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function populateWorkShifts()
	{
		$this->db->select(' shift_id,shift_name ');
		$this->db->where('shift_status', STATUS_ACTIVE);
		$results = $this->db->get(TABLE_WORK_SHIFT);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function populateEmploymentStatus()
	{
		$this->db->select(' employment_status_id,employment_status_name');
		$this->db->where('status', STATUS_ACTIVE);
		$results = $this->db->get(TABLE_EMPLOYMENT_STATUS);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function populateDepartments()
	{
		$this->db->select(' job_category_id,job_category_name ');
		$this->db->where('job_category_status', STATUS_ACTIVE);
		$this->db->order_by('job_category_name', 'ASC');
		$results = $this->db->get(TABLE_JOB_CATEGORY);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getHierarchy($empID, $strHierarchy = '') {
		
		$this->db->select(' emp_id ');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');		
		$this->db->where('e.emp_authority_id', $empID);
		$this->db->where('u.user_role_id in ', '('.INTERVIEWERS_ROLE_IDS.')', false);
		
		$results = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		if(empty($strHierarchy)) {
			$this->strHierarchy = $empID;
		} else {
			$this->strHierarchy .= ',' . $empID;
		}		
		
		for($ind = 0; $ind < count($arrResult); $ind++) {
			$this->getHierarchy($arrResult[$ind]['emp_id'], $this->strHierarchy);
		}
		
		return $this->strHierarchy;
	}
	
	function getHierarchyWithMultipleAuthorities($empID, $strHierarchy = '') {
		
		$this->db->select(' e.emp_id ');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'e.emp_id = es.emp_id', 'left');
		$this->db->where('es.supervisor_emp_id', $empID);
		$this->db->where('u.user_role_id in ', '('.INTERVIEWERS_ROLE_IDS.')', false);
		
		$results = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		if(empty($strHierarchy)) {
			$this->strHierarchy = $empID;
		} else {
			$this->strHierarchy .= ',' . $empID;
		}		
		
		for($ind = 0; $ind < count($arrResult); $ind++) {
			$this->getHierarchyWithMultipleAuthorities($arrResult[$ind]['emp_id'], $this->strHierarchy);
		}
		
		return $this->strHierarchy;
	}
	
	function getResignations($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->select(' er.*, e.emp_full_name, e.emp_authority_id, e.emp_gender, e.emp_photo_name ');				
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = er.resignation_emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('er.created_date', 'DESC');
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_RESIGNATION . ' er ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getResignation($arrWhere = array(), $rowsLimit = 0, $rowsOffset = 0) {
		
		$this->db->join(TABLE_EMPLOYEE_RESIGNATION_CLEARANCE . ' erc ', 'er.resignation_id = erc.resignation_id', 'left');
		$this->db->order_by('er.resignation_id', 'DESC');
		
		foreach($arrWhere as $key => $value) {
			$newKey = 'er.' . $key;
			$this->db->where($newKey, $arrWhere[$key]);
			unset($arrWhere[$key]);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->limit(1, 0);
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_RESIGNATION . ' er ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		if(count($arrResult) == 1) {
			return $arrResult[0];
		}
		
		return $arrResult;
	}
	
	function getResignationStepAuthorities($arrWhere = array()) {
		
		$this->db->select('authorized_emp_id');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_RESIGNATION_AUTHORITIES);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		$arrIDs = array();
		
		if(count($arrResult)) {
			foreach($arrResult as $arrRecord) {
				$arrIDs[] = $arrRecord['authorized_emp_id'];
			}
		}
		
		return $arrIDs;
	}
	
	function getDocumentList() {
		
		$this->db->order_by('sort_order', 'ASC');
		$results = $this->db->get(TABLE_EMPLOYEE_DOCUMENT_LIST);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getEmpDocumentList($arrWhere = array()) {
		
		$this->db->select(' ed.*, dt.doc_type ');				
		
		$this->db->join(TABLE_EMPLOYEE_DOCUMENT_TYPES . ' dt ', 'dt.doc_type_id = ed.doc_type_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ed.emp_id = e.emp_id', 'left');
		$this->db->where($arrWhere);
		$this->db->order_by('doc_title', 'ASC');
		$results = $this->db->get(TABLE_EMPLOYEE_DOCUMENTS . ' ed ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getBenefits($arrWhere = array()) {
		
		$this->db->where($arrWhere);
		$this->db->order_by('benefit_title', 'ASC');
		$results = $this->db->get(TABLE_EMPLOYEE_BENEFITS);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getEmployeeCallsStats($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {	
		
		$this->db->select(' ec.*, e.emp_code, e.emp_full_name ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', ' ec.called_emp_id = e.emp_id ', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->order_by('ec.call_id', 'DESC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
			
		$objResult = $this->db->get(TABLE_EMPLOYEE_CALLS . ' ec ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getEmployeeCallsStatsTotal($arrWhere = array()) {	
		
		$this->db->select(' count(*) as total_records ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
			
		$objResult = $this->db->get(TABLE_EMPLOYEE_CALLS . ' ec ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_records'];
	}
	
	function getEmergencyTransfer($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		// $emp_id = $arrWhere['emp_id'];
		// print_r($emp_id);exit;
		// if(count($arrWhere)) {
		// 	$this->db->where($arrWhere);			
		// }
		// if((int)$rowsLimit > 0) {
		// 	$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		// }
		// $objResult = $this->db->get(TABLE_EMPLOYEE_EMERGENCY_CONTACTS);
		// $arrResult = $objResult->result_array();
		// $objResult->free_result();
		// return $arrResult;

		// $objResult = $this->db->query("SELECT  t1.transfer_id,t1.emp_id,c3.company_name AS current_branch,e2.emp_full_name AS processed_name,e3.emp_full_name AS created_name,e1.emp_full_name,e1.emp_code, t1.transfer_reason, t1.transfer_status, t1.transfer_comments, t1.processed_by,t1.created_by,t1.processed_date,t1.created_date,re1.region_id,re1.region_name, c1.company_name AS company_from, c2.company_name AS company_to
		// FROM hrm_employee_transfers t1
		// INNER JOIN hrm_companies c1 ON t1.transfer_from = c1.company_id
		// INNER JOIN hrm_companies c2 ON t1.transfer_to = c2.company_id
		// INNER JOIN hrm_regions re1 ON c1.company_region_id = re1.region_id
		// INNER JOIN hrm_employee e2 ON t1.processed_by = e2.emp_id AND t1.processed_by != 0
		// INNER JOIN hrm_companies c3 ON e2.emp_company_id = c3.company_id
		// INNER JOIN hrm_employee e3 ON t1.created_by = e3.emp_id AND t1.created_by != 0
		// -- LEFT JOIN hrm_employee e4 ON e4.emp_company_id = c1.company_id
		// INNER JOIN hrm_employee e1 ON t1.emp_id = e1.emp_id WHERE t1.transfer_status = 1  GROUP BY t1.emp_id 
		// ORDER BY t1.created_date DESC");

		$objResult = $this->db->query("SELECT company_id, company_name FROM hrm_companies");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($arrResult);
		// exit;
		return $arrResult;
	}
	function getEmergencyTransferDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		// $emp_id = $arrWhere['emp_id'];
		// print_r($emp_id);exit;
		// if(count($arrWhere)) {
		// 	$this->db->where($arrWhere);			
		// }
		// if((int)$rowsLimit > 0) {
		// 	$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		// }
		// $objResult = $this->db->get(TABLE_EMPLOYEE_EMERGENCY_CONTACTS);
		// $arrResult = $objResult->result_array();
		// $objResult->free_result();
		// return $arrResult;

		$objResult = $this->db->query("SELECT  t1.transfer_id,t1.emp_id,c3.company_name AS current_branch,e2.emp_full_name AS processed_name,e3.emp_full_name AS created_name,e1.emp_full_name,e1.emp_code, t1.transfer_reason, t1.transfer_status, t1.transfer_comments, t1.processed_by,t1.created_by,t1.processed_date,t1.created_date,re1.region_id,re1.region_name, c1.company_name AS company_from, c2.company_name AS company_to
		FROM hrm_employee_transfers t1
		INNER JOIN hrm_companies c1 ON t1.transfer_from = c1.company_id
		INNER JOIN hrm_companies c2 ON t1.transfer_to = c2.company_id
		INNER JOIN hrm_regions re1 ON c1.company_region_id = re1.region_id
		INNER JOIN hrm_employee e2 ON t1.processed_by = e2.emp_id AND t1.processed_by != 0
		INNER JOIN hrm_companies c3 ON e2.emp_company_id = c3.company_id
		INNER JOIN hrm_employee e3 ON t1.created_by = e3.emp_id AND t1.created_by != 0
		-- LEFT JOIN hrm_employee e4 ON e4.emp_company_id = c1.company_id
		INNER JOIN hrm_employee e1 ON t1.emp_id = e1.emp_id WHERE t1.transfer_status = 1  GROUP BY t1.emp_id 
		ORDER BY t1.created_date DESC");

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		// print_r($arrResult);
		// exit;
		return $arrResult;
	}

	function saveTransfer($emp_id, $date){
		// $transfers = $this->db->query("SELECT transfer_id,emp_id,transfer_to from hrm_employee_transfers WHERE transfer_id = ".$transfer_id);
		// $transfers = $this->db->query("INSERT INTO MyGuests (firstname, lastname, email)");
		// $trarrResult = $transfers->result_array();
		// $transfers->free_result();
		// return $trarrResult;
		$this->db->query("UPDATE hrm_employee_transfers SET transfer_status = '1', transfer_comments = 'approved', processed_by = '".$emp_id."', processed_date = '".$date."' where transfer_id = ".$transfer_id);

		$update_emp_company = "UPDATE hrm_employee SET emp_company_id = ".$trarrResult[0]['transfer_to']."  where emp_id =". $trarrResult[0]['emp_id'];
		$this->db->query($update_emp_company);
		// return $update_emp_company;
	}
}
?>