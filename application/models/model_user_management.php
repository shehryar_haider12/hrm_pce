<?php
class Model_User_Management extends Model_Master {
		
	function __construct() {
		parent::__construct();	
	}
	
	function isValidUser($userName, $passPhrase) {
		$arrValues = array(
							'u.user_name' => $userName, 
							'u.password' => $passPhrase,
							'u.user_status' => 1 
							);
		
		$this->db->select(' u.user_id, u.user_role_id, emp.emp_id, emp.emp_full_name AS employee_name, emp.emp_job_category_id, emp.emp_company_id, emp.emp_location_id ');
		$this->db->join(TABLE_EMPLOYEE . ' emp ', 'u.employee_id = emp.emp_id', 'left');
		$chkRecord = $this->db->get_where(TABLE_USER . ' u ', $arrValues);
		
		$arrResult = $chkRecord->result_array();
		$chkRecord->free_result();
		if(count($arrResult) > 0) {
			return $arrResult[0];
		}
		return false;
	}
	
	function populateUserRoles()
	{
		$this->db->select(' user_role_id,user_role_name ');
		$this->db->where('user_role_status', '1');
		$this->db->order_by('user_role_name', 'ASC');
		$results = $this->db->get(TABLE_USER_ROLE);	
		$arrResult = $results->result_array();
		$results->free_result();
				
		return $arrResult;
	}
	
	function populateDepartments()
	{
		$this->db->select(' department_id,department_name ');
		$this->db->where('department_status', '1');
		$this->db->order_by('department_name', 'ASC');
		$results = $this->db->get(TABLE_DEPARTMENT);	
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateMarkets()
	{
		$this->db->select(' market_id,market_name ');
		$this->db->where('market_status', '1');
		$results = $this->db->get($this->tblOhrmMarket);	
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateModules()
	{
		$this->db->select(' module_id,display_name,module_name ');
		$this->db->where('module_status', '1');
		$this->db->where('module_skip_display', '0');
		$values = array('0', '1');
		$this->db->where_in('module_parent_id', $values);
		$this->db->order_by('display_name', 'ASC');
		$results = $this->db->get(TABLE_MODULE);	
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function deleteValue($tblName, $arrDeleteValues, $arrDeleteWhere = array())
	{		
		if(count($arrDeleteWhere)) {
			$this->db->where($arrDeleteWhere);
			$this->db->update($tblName, $arrDeleteValues);
			return ($this->db->affected_rows() > 0);
		}
	}
	
	function checkExistingEmployee($employeeID)
	{
		$this->db->select(' count(*) as count ');
		$this->db->where('employee_id',$employeeID);
		$objResult = $this->db->get(TABLE_USER);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		if($arrResult[0]['count'] >= 1)
		return false;
		else return true;
	}
	
	function getUsers($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') 
	{
		$this->db->select(' u.*,ur.user_role_name ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		if(isset($_POST['sort_field'])) {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('u.' . $sortColumn, $sortOrder);
			}
		}
		$this->db->order_by('u.user_id', 'DESC');
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'u.user_role_id = ur.user_role_id', 'left');
		$objResult = $this->db->get(TABLE_USER . ' u ');
		if ($arrWhere["user_name like "]){
			$objResult = $this->db->query('SELECT * FROM '.'hrmbackup.'.TABLE_USER .',hrmbackup.'.TABLE_USER_ROLE.' where hrm_user.user_role_id = hrm_user_role.user_role_id and user_name LIKE "'.$arrWhere["user_name like "].'"');
		}
		// $this->db->order_by('u.emp_id', 'DESC');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function getTotalUsers($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if(isset($_POST['sort_field'])) {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('u.' . $sortColumn, $sortOrder);
			}
		}
				
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'u.user_role_id = ur.user_role_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' emp ', 'u.employee_id = emp.emp_id', 'left');
		$objResult = $this->db->get(TABLE_USER . ' u ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getUserRoles($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{
		$this->db->select(' ur.user_role_id,ur.user_role_name,is_admin,ur.user_role_status ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if(isset($_POST['sort_field'])) {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('ur.' . $sortColumn, $sortOrder);
			}
		} else {
			$this->db->order_by('ur.user_role_name', 'ASC');
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_USER_ROLE . ' ur ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalUserRoles($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if(isset($_POST['sort_field'])) {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('ur.' . $sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_USER_ROLE . ' ur ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getUserPermissions($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
				
		$this->db->select(' up.*,ur.user_role_name,m.display_name as module_name,m1.display_name as sub_module_name ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if(isset($_POST['sort_field'])) {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('up.' . $sortColumn, $sortOrder);
			}
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'up.user_role_id = ur.user_role_id', 'left');
		$this->db->join(TABLE_MODULE . ' m ', 'up.module_id = m.module_id', 'left');
		$this->db->join(TABLE_MODULE . ' m1 ', 'up.sub_module_id = m1.module_id', 'left');
		$objResult = $this->db->get(TABLE_USER_PERMISSIONS . ' up ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalUserPermissions($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if(isset($_POST['sort_field'])) {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('up.' . $sortColumn, $sortOrder);
			}
		}
		
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'up.user_role_id = ur.user_role_id', 'left');
		$this->db->join(TABLE_MODULE . ' m ', 'up.module_id = m.module_id', 'left');
		$this->db->join(TABLE_MODULE . ' m1 ', 'up.sub_module_id = m1.module_id', 'left');
		$objResult = $this->db->get(TABLE_USER_PERMISSIONS . ' up ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
		
	function getUserLoginReport($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->select(' e.*, g.grade_title, u.user_role_id, u.employee_id as user_exists, u.last_login_date, u.last_logout_date,
						(select timestampdiff(second, u.last_login_date, u.last_logout_date )/60) as total_time_spent,
						');
				
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_GRADES . ' g ', 'e.emp_grade_id = g.grade_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if((int)$arrWhere['emp_role_id']) {
			$this->db->where('u.user_role_id', $arrWhere['emp_role_id']);
			unset($arrWhere['emp_role_id']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if($doSort) {
			if(!isset($_POST['sort_field'])) {
				$this->db->order_by('u.last_login_date', 'DESC');
			} else {
				$sortColumn = $_POST['sort_field'];
				$sortOrder = $_POST['sort_order'];
				
				if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
					$this->db->order_by($sortColumn, $sortOrder);
				}
				$this->db->order_by('e.emp_full_name', 'ASC');
			}
		}
		
		if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', '1');
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function populateModuleParticularScreen($role_id,$module_id,$sub_module_id)
	{		
		$this->db->select(' m.module_id,m.display_name,up.can_read,up.can_write,up.can_delete,up.status ');
		$this->db->where('up.user_role_id', $role_id);
		$this->db->where('up.module_id', $module_id);
		if($sub_module_id > 0)
			{$this->db->where('up.sub_module_id', $sub_module_id);}
		$this->db->join(TABLE_MODULE . ' m ', 'up.sub_module_id = m.module_id', 'left');
		$results = $this->db->get(TABLE_USER_PERMISSIONS . ' up ');
		
		$arrResult = $results->result_array();
		
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateRoleModuleScreens($role_id,$module_id)
	{		
		$this->db->select(' m.module_id,m.display_name, m.module_parent_id, m1.module_name AS module_parent_name,up.can_read,up.can_write,up.can_delete,up.status ');			
		$this->db->join(TABLE_MODULE . ' m ', 'up.sub_module_id = m.module_id', 'left');
		$this->db->join(TABLE_MODULE . ' m1 ', 'up.module_id = m1.module_id', 'left');
		
		$this->db->where('up.user_role_id', $role_id);
		if($module_id > 0) {
			$this->db->where('up.module_id', $module_id);
		}
		
		$this->db->order_by('m.module_parent_id', 'ASC');
		$this->db->order_by('m.module_id', 'ASC');
		
		$results = $this->db->get(TABLE_USER_PERMISSIONS . ' up ');
		
		$arrResult = $results->result_array();
		
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateModuleScreens($role_id,$module_id)
	{
		$query = "SELECT m.module_id, m.display_name 
					FROM hrm_module m 
					WHERE m.module_id NOT IN (SELECT sub_module_id FROM hrm_user_permissions WHERE user_role_id =".$role_id." AND module_id=".$module_id.") 
					AND m.module_parent_id=".$module_id;
		
		$results = $this->db->query($query);
				
		$arrResult = $results->result_array();
		
		$results->free_result();
		
		return $arrResult;
	}
	
	function isParentModule($module_id)
	{
		$this->db->select(' module_id ');
		$this->db->where('module_parent_id', $module_id);
		$results = $this->db->get(TABLE_MODULE);
		$arrResult = $results->result_array();
		$results->free_result();
		if($arrResult)
			return true;
		else
			return false;
	}
	
	function getLogs($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' l.*, e.emp_full_name, e.emp_code, e.emp_ip_num, m.display_name ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'l.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_MODULE . ' m ', 'l.module_id = m.module_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['l.log_activity_txt'] != '') {
			$this->db->where('(l.log_activity_txt like \'%' . $arrWhere['l.log_activity_txt'] . '%\')'); 
			unset($arrWhere['l.log_activity_txt']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('l.created_date', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_USER_LOGS . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalLogs($arrWhere = array()) {
		
		$this->db->select(' count(*) as total_count ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_USER_LOGS . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getTotalSpecialPermissions($arrWhere = array()) {
		
		$this->db->select(' count(*) as total_records ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_USER_SPECIAL_PERMISSIONS . ' usp ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_records'];
	}
	
	function getSpecialPermissions($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		$this->db->select(' usp.*, e.emp_full_name, e.emp_code, e.emp_ip_num, m.display_name, m.module_name, ur.user_role_name, ur1.user_role_name as actual_user_role, jc.job_category_name');
		//..	$this->db->select(' usp.*, e.emp_first_name, e.emp_last_name, e.emp_code, e.emp_ip_num, m.display_name, m.module_name, ur.user_role_name ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'usp.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_MODULE . ' m ', 'usp.module_name = m.module_name', 'left');
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'usp.assigned_role_id = ur.user_role_id', 'left');
		$this->db->join(TABLE_USER_ROLE . ' ur1 ', 'usp.user_role_id = ur1.user_role_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'usp.job_category_id = jc.job_category_id', 'left');
		
		/*
		if($arrWhere['usp.user_role_id']) {		
			$this->db->join(TABLE_USER_ROLE . ' ur1 ', 'usp.user_role_id = ur1.user_role_id', 'left');
		}
		if($arrWhere['usp.job_category_id']) {	
			$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'usp.job_category_id = jc.job_category_id', 'left');
		}
		*/
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_USER_SPECIAL_PERMISSIONS . ' usp ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
}
?>