<?php
class Model_System_Configuration extends Model_Master {
		
	function __construct() {
		 parent::__construct();	
	}
	
	function checkValues($tableName, $Where = array(), $column = 0, $strID = 0, $strID2 = 0) {
		if(count($Where)) {
			$this->db->select(' count(*) as total ');
			$this->db->where($Where);
			
			if($strID) {
				$this->db->where($column.' not in ', '(' . $strID . ')', false);
			}
			
			if($strID2) {
				$this->db->where('module_parent_id in ', '(' . $strID2 . ')', false);
			}
			
			$objResult = $this->db->get($tableName);
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if($arrResult[0]['total'] > 0){
				return $arrResult = 0;
			} else {
				return $arrResult = 1;
			}
			
		}
	}
		
	



//add code by zeeshan qasim


function getCompaniesDocumentList($arrWhere = array()) {
		
		$this->db->select(' ed.*, dt.doc_type, e.company_id ');				
		
		$this->db->join(TABLE_COMPANIES_DOCUMENT_TYPES . ' dt ', 'dt.doc_type_id = ed.doc_type_id', 'left');
		$this->db->join(TABLE_COMPANIES . ' e ', 'ed.company_id = e.company_id', 'left');
		
		$this->db->where($arrWhere);
		$this->db->order_by('e.company_name', 'ASC');
		$results = $this->db->get(TABLE_COMPANIES_DOCUMENTS . ' ed ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}



function deleteValue($tblName, $arrDeleteValues = array(), $colStatus = '', $valStatus = 0) {
		
		if(count($arrDeleteValues)) {
			
			$this->db->where($arrDeleteValues);
			if(!in_array($tblName, $this->tblNoDeletion)) {
				$this->db->delete($tblName);
			} else {
				$arrValues = array(
									'deleted_by' => $this->userEmpNum,
									'deleted_date' => date(DATE_TIME_FORMAT)
									);
									
				if($tblName == TABLE_EMPLOYEE) {
					$arrValues['emp_status'] = STATUS_INACTIVE_VIEW;
				}
									
				if(!empty($colStatus) && strlen($colStatus) > 7 && is_numeric($valStatus)) {
					$arrValues[$colStatus] = $valStatus;
				}
				
				$this->db->update($tblName, $arrValues, $arrDeleteValues);
			}
			return ($this->db->affected_rows() > 0);
			
		}
	}


function getCompaniesDocumentListReal($arrWhere = array()) {
		
		$this->db->select(' ed.*, dt.doc_type ');				
		
		$this->db->join(TABLE_COMPANIES_DOCUMENT_TYPES . ' dt ', 'dt.doc_type_id = ed.doc_type_id', 'left');
		//$this->db->join(TABLE_COMPANIES . ' e ', 'ed.company_id = e.company_id', 'left');
		
		$this->db->where($arrWhere);
		$this->db->order_by('company_name', 'ASC');
		$results = $this->db->get(TABLE_COMPANIES_DOCUMENTS . ' ed ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}


function getTotalCompaniesDocuments($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_COMPANIES_DOCUMENTS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}


//end add code by zeeshan qasim



function getModules($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') 
	{
		$this->db->select(' m.*,m1.display_name as parent_module_name,l.layout_display_name as layout_name ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_MODULE . ' m1 ', 'm.module_parent_id = m1.module_id', 'left');
		$this->db->join(TABLE_LAYOUT . ' l ', 'm.layout = l.layout_id', 'left');
		$this->db->order_by('m.display_name', 'ASC');
		$objResult = $this->db->get(TABLE_MODULE . ' m ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalModules($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_MODULE . ' m1 ', 'm.module_parent_id = m1.module_id', 'left');
		$this->db->join(TABLE_LAYOUT . ' l ', 'm.layout = l.layout_id', 'left');
		$objResult = $this->db->get(TABLE_MODULE . ' m ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getDesignations($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{
		$this->db->select(' * ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('designation_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_DESIGNATIONS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getTotalDesignations($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_DESIGNATIONS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getLocationsSystem($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{
		$this->db->select(' l.*,l1.location_name as region_name,l2.location_name as country_name,(SELECT location_id FROM hrm_location WHERE l2.location_parent_id = location_id) AS country_region_id,(SELECT location_name FROM hrm_location WHERE l2.location_parent_id = location_id) AS country_region_name ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_LOCATION . ' l1 ', 'l.location_parent_id = l1.location_id and l1.location_type_id=1', 'left');
		$this->db->join(TABLE_LOCATION . ' l2 ', 'l.location_parent_id = l2.location_id and l2.location_type_id=2', 'left');
		$this->db->order_by('l.location_type_id, country_name', 'ASC');
		$objResult = $this->db->get(TABLE_LOCATION .' l ');				
		$arrResult = $objResult->result_array();
					
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getTotalLocations($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_LOCATION . ' l1 ', 'l.location_parent_id = l1.location_id and l1.location_type_id=1', 'left');
		$this->db->join(TABLE_LOCATION . ' l2 ', 'l.location_parent_id = l2.location_id and l2.location_type_id=2', 'left');
		$objResult = $this->db->get(TABLE_LOCATION .' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getLocations($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$this->db->order_by('location_name', 'ASC');
		$objResult = $this->db->get(TABLE_LOCATION);
		$arrResult = $objResult->result_array();
		
		$objResult->free_result();
		
		return $arrResult;
	}
	function getRegions($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$this->db->order_by('region_name', 'ASC');
		$objResult = $this->db->get(TABLE_REGIONS);
		$arrResult = $objResult->result_array();
		
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getLocationParent($arrWhere = array()) {
		
		$this->db->select(' location_parent_id ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}	
		
		$objResult = $this->db->get(TABLE_LOCATION);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}
	
	function getJobTitles($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' jt.*, jc.job_category_name ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = jt.job_category_id', 'left');
		$this->db->order_by('jt.job_title_name', 'ASC');
		$objResult = $this->db->get(TABLE_JOB_TITLE . ' jt ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalJobTitles($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = jt.job_category_id', 'left');
		$objResult = $this->db->get(TABLE_JOB_TITLE . ' jt ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getJobCategories($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') 
	{
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		$this->db->order_by('job_category_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_JOB_CATEGORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalJobCategories($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_JOB_CATEGORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getWorkShifts($arrWhere = array('shift_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_WORK_SHIFT);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalWorkShifts($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_WORK_SHIFT);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getEduLevels($arrWhere = array('edu_level_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		$this->db->order_by('edu_level_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_EDUCATION_LEVELS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalEduLevels($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EDUCATION_LEVELS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getLanguages($arrWhere, $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_LANGUAGE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalLanguages($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_LANGUAGE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getVacancyTitles($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' jv.vacancy_id, jv.vacancy_name ');
		$this->db->join(TABLE_JOB_TITLE . ' jt ', 'jt.job_title_id = jv.job_title_code', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$objResult = $this->db->get(TABLE_VACANCY . ' jv ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getEduMajors($arrWhere = array('edu_major_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		$this->db->order_by('edu_major_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_EDUCATION_MAJORS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalEduMajors($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EDUCATION_MAJORS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getGrades($arrWhere = array('grade_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('job_type', 'ASC');
		
		$objResult = $this->db->get(TABLE_GRADES);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalGrades($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_GRADES);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getReligions($arrWhere = array('religion_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' * ');		
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('religion_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_RELIGIONS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalReligions($arrWhere = array()) {
		
		$this->db->select(' count(*) as total ');	
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_RELIGIONS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCompanies($arrWhere = array('company_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' c.*, e.emp_full_name as company_head_name, l.location_name as company_country_name,r.region_name as company_region_name, (select count(*) from ' . TABLE_EMPLOYEE . ' where emp_visa_company_id = c.company_id) as company_visas_granted, (select count(*) from ' . TABLE_EMPLOYEE . ' where emp_company_id = c.company_id) as total_employee');
		$this->db->join('hrm_regions' . ' r ', 'r.region_id = c.company_region_id', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = c.company_country_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = c.company_head_id', 'left');	
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('company_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	function getBranches($arrWhere = array('company_status' => 1), $rowsLimit = '', $rowsOffset = '') {
		
		// $this->db->select(' c.*, e.emp_full_name as company_head_name, l.location_name as company_country_name,r.region_name as company_region_name, (select count(*) from ' . TABLE_EMPLOYEE . ' where emp_visa_company_id = c.company_id) as company_visas_granted, (select count(*) from ' . TABLE_EMPLOYEE . ' where emp_company_id = c.company_id) as total_employee');
		// $this->db->join('hrm_regions' . ' r ', 'r.region_id = c.company_region_id', 'left');
		// $this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = c.company_country_id', 'left');
		// $this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = c.company_head_id', 'left');	
		// print_r($arrWhere);
		// exit;
		if($arrWhere['region_id'] && $arrWhere['company_country_id']){
			$objResult = $this->db->query("SELECT c1.company_id,c1.company_name,c1.company_address,c1.company_country_id,c1.company_region_id,cn1.location_id,cn1.location_name, r1.region_name, r1.region_id
			FROM hrmbackup.hrm_companies c1
			LEFT JOIN hrmbackup.hrm_location cn1 ON c1.company_country_id = cn1.location_id
			LEFT JOIN hrmbackup.hrm_regions r1 ON c1.company_region_id = r1.region_id WHERE c1.company_region_id = ".$arrWhere['region_id']." AND c1.company_country_id = ".$arrWhere['company_country_id']."
			");
			
		}elseif ($arrWhere['region_id']) {
			$objResult = $this->db->query("SELECT c1.company_id,c1.company_name,c1.company_address,c1.company_country_id,c1.company_region_id,cn1.location_id,cn1.location_name, r1.region_name, r1.region_id
			FROM hrmbackup.hrm_companies c1
			LEFT JOIN hrmbackup.hrm_location cn1 ON c1.company_country_id = cn1.location_id
			LEFT JOIN hrmbackup.hrm_regions r1 ON c1.company_region_id = r1.region_id WHERE c1.company_region_id = ".$arrWhere['region_id']."
			");
			
		}elseif ($arrWhere['company_country_id']) {
			$objResult = $this->db->query("SELECT c1.company_id,c1.company_name,c1.company_address,c1.company_country_id,c1.company_region_id,cn1.location_id,cn1.location_name, r1.region_name, r1.region_id
			FROM hrmbackup.hrm_companies c1
			LEFT JOIN hrmbackup.hrm_location cn1 ON c1.company_country_id = cn1.location_id
			LEFT JOIN hrmbackup.hrm_regions r1 ON c1.company_region_id = r1.region_id WHERE c1.company_country_id = ".$arrWhere['company_country_id']."
			");	
		}else {
			$objResult = $this->db->query("SELECT c1.company_id,c1.company_name,c1.company_address,c1.company_country_id,c1.company_region_id,cn1.location_id,cn1.location_name, r1.region_name, r1.region_id
			FROM hrmbackup.hrm_companies c1
			LEFT JOIN hrmbackup.hrm_location cn1 ON c1.company_country_id = cn1.location_id
			LEFT JOIN hrmbackup.hrm_regions r1 ON c1.company_region_id = r1.region_id
			");
		}

		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;

		// if(count($arrWhere)) {
		// 	$this->db->where($arrWhere);			
		// }		
		// if((int)$rowsLimit > 0) {
		// 	$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		// }
		
		// $this->db->order_by('company_name', 'ASC');
		
		// $objResult = $this->db->get(TABLE_COMPANIES . ' c ');
		// $arrResult = $objResult->result_array();
		// $objResult->free_result();
		
		// return $arrResult;
	}
	
	function getTotalCompanies($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_COMPANIES);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getSponsors($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('sponsor_type', 'ASC');
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_SPONSOR);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalSponsors($arrWhere = array())
	{
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_SPONSOR);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;	
	}
	
	function getBanks($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('bank_name', 'ASC');
		
		$objResult = $this->db->get(TABLE_BANKS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
		
	function getSettings($arrWhere = array()) {
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		
		$objResult = $this->db->get(TABLE_CONFIGURATION);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function populateParentModules()
	{
		$this->db->select(' module_id,display_name ');
		$this->db->where('module_status', '1');
		$this->db->where('module_skip_display', '0');
		$values = array('0', '1');
		$this->db->where_in('module_parent_id', $values);
		$this->db->order_by('display_name', 'ASC');
		$results = $this->db->get(TABLE_MODULE);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function populateLayouts()
	{
		$this->db->select(' layout_id,layout_display_name ');
		$this->db->where('layout_status','1');
		$this->db->order_by('layout_display_name', 'ASC');
		$results = $this->db->get(TABLE_LAYOUT);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
}
?>