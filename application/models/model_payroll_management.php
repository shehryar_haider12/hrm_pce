<?php
class Model_Payroll_Management extends Model_Master {
	
	public $strHierarchy;
	private $tblNoDeletion	= array(
									TABLE_EMPLOYEE_PAYROLL
									);
	
	function __construct() {
		 parent::__construct();	
	}
	
	function getPayrolls($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		// return $arrWhere;
		$this->db->distinct();
		$this->db->select(' ep.*, e.*, c.company_currency_id ');				
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_COMPANIES. ' c ', 'c.company_id = ep.payroll_company_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('e.created_date', 'DESC');
		
		if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', STATUS_ACTIVE);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_PAYROLL . ' ep ');

		// if(count($arrWhere)) {
			// print_r('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE ' .$arrWhere['empID'] .' ' .$arrWhere['selRegion'] .' ' .$arrWhere['empCompany'] .' ' .$arrWhere['empSupervisor'] .' ' .$arrWhere['selMonth'] .' ' .$arrWhere['selYear']);exit;
		// 	$objResult = $this->db->query('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON e.emp_id = es.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE ' .$arrWhere['empID'] .' ' .$arrWhere['selRegion'] .' ' .$arrWhere['empCompany'] .' ' .$arrWhere['empSupervisor'] .' ' .$arrWhere['selMonth'] .' ' .$arrWhere['selYear'] );
		// }else{
		// 	$objResult = $this->db->query('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id');
		// }
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function getTotalPayrolls($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' ep.payroll_id ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_COMPANIES. ' c ', 'c.company_id = ep.payroll_company_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_PAYROLL . ' ep ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		// if(count($arrWhere)) {
			// print_r("SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE " .$arrWhere['empID'] ." " .$arrWhere['selRegion'] ." " .$arrWhere['empCompany'] ." " .$arrWhere['empSupervisor'] ." " .$arrWhere['selMonth'] ." " .$arrWhere['selYear']);exit;
		// 	$objResult = $this->db->query("SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON e.emp_id = es.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE " .$arrWhere['empID'] ." " .$arrWhere['selRegion'] ." " .$arrWhere['empCompany'] ." " .$arrWhere['empSupervisor'] ." " .$arrWhere['selMonth'] ." " .$arrWhere['selYear'] );
		// }else{
		// 	$objResult = $this->db->query('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id');
		// }
		// $arrResult = $objResult->result_array();
		// $objResult->free_result();

		// $arrResult = count($arrResult);
		return $arrResult;
	}
	
	function getEmployeesPayroll($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->distinct();
		$this->db->select(' e.*, u.user_role_id, ur.user_role_name, u.employee_id as user_exists, l.location_name, jc.job_category_name, c.company_currency_id ');
		
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'ur.user_role_id = u.user_role_id', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = e.emp_location_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_PAYROLL . ' p ', 'p.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_COMPANIES. ' c ', 'c.company_id = p.payroll_company_id', 'left');
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		
		
		if((int)$arrWhere['p.payroll_company_id']) {
			$this->db->where(" (p.payroll_company_id = '"  . (int)$arrWhere['p.payroll_company_id'] . "' OR e.emp_company_id = '" . (int)$arrWhere['p.payroll_company_id'] . "') ", null, false);
			unset($arrWhere['p.payroll_company_id']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_full_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if($arrWhere['edu.edu_level_id'] || $arrWhere['edu.edu_major_id']) {			
			$this->db->join(TABLE_EMPLOYEE_EDUCATION . ' edu ', 'edu.emp_id = e.emp_id', 'left');
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if($doSort) {
			if(!isset($_POST['sort_field']) || $_POST['sort_field'] == '') {
				$this->db->order_by('e.emp_full_name', 'ASC');
			} else if($this->currentController == 'employee_management') {
				$sortColumn = $_POST['sort_field'];
				$sortOrder = $_POST['sort_order'];
				
				if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
					$this->db->order_by($sortColumn, $sortOrder);
				}
				$this->db->order_by('e.emp_full_name', 'ASC');
			} else {
				$this->db->order_by('e.emp_full_name', 'ASC');
			}
		}
		
		/*if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', STATUS_ACTIVE);
		}*/
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}

	
}
?>