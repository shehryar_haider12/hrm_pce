<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Master_Controller extends CI_Controller
{
	public $userID = 0;
	public $userRoleID = 0;
	public $userRoleIDOriginal = 0;
	public $userWebAdmin = 0;
	public $allEmployees = array();
	public $userEmpNum = 0;
	public $userEmpName;
	public $baseURL;
	public $imagePath;
	public $modulesAllowed;
	public $screensAllowed;
	public $modulesAllowedForMenu;
	public $screensAllowedForMenu;
	public $currentController = '';
	public $currentAction = '';
	public $salaryYearStarted = 2015;
	public $HRMYearStarted = 2018;
	public $homeModule = 'home';
	public $homeModuleID = '1';
	public $currModuleID = 0;
	
	public $layout = '';
	public $screen_name = '';
	public $controller_id = '';
	
	function __construct() {
		parent::__construct();
		
		$this->load->library(array('session', 'form_validation'));	
		$this->load->helper(array('url', 'common', 'html', 'form'));
		
		###############	SYSTEM RELATED SETTINGS ############
		
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Dubai");
		$this->load->model('model_master', 'master', true);
		$this->load->model('model_system_configuration', 'configuration', true);
		$this->setupConfig($this->configuration->getSettings()); # SETUP ALL SYSTEM WIDE VARIABLES REQUIRED IN CODE (TABLE hrm_configuration)
		$this->setupEmail(); # SETUP EMAIL ATTRIBUTES IF SMTP DETAILS ARE AVAILABLE
		$arrMaintenance = explode(',', DOWN_FOR_MAINTENANCE);
		
		####################################################
		
		$this->currentController = $this->router->fetch_class();
		$this->currentAction = $this->router->method;
		
		$this->load->model('Model_Privilege', 'privilege', true);
		$this->baseURL = $this->config->item('base_url');
		$this->imagePath = $this->config->item('image_path');
		$this->docsPath = $this->config->item('docs_path');
		
		$this->userID = (int)$this->session->userdata('user_id');
		$this->userName = $this->session->userdata('user_name');
		$this->userRoleID = (int)$this->session->userdata('user_role_id');
		$this->userCompanyID = $this->session->userdata('emp_company_id');
		$this->userRoleIDOriginal = $this->userRoleID;
		$this->userEmpNum = (int)$this->session->userdata('employee_id');
		$this->userWelcomeName = $this->session->userdata('display_name');
		$this->userCategoryID = $this->session->userdata('emp_job_category_id');
		$this->userLocationID = $this->session->userdata('location_id');
		
		$this->checkLogin();
		$this->createMenu();			
		$this->checkSpecialPermission();
		$this->checkUser();
		$this->checkAccess();
		
		# UPDATE SBT DISCUSSION FORUM COOKIE
		$this->cookieSetUnset(false, $this->userEmpNum, $this->userName);
		
		# LOGOFF CODE PLACED OUTSIDE checkLogin() TO GET CURRENT MODULE ID
			
		// print_r($this->input->get('logoff'));
		// exit;
		if($this->input->post('logoff')) {
			
			if((int)$this->userEmpNum) {
				debugLog("Log-Out");
			}
			
			$this->load->model('model_user_management', 'user', true);
			$arrValues = array(
								'last_logout_date' => date(DATE_TIME_FORMAT)
								);
			$this->user->saveValues(TABLE_USER, $arrValues, array('user_id' => $this->session->userdata('user_id')));
			// print_r("asdfasdffasddsff");
			// exit;
			$this->session->sess_destroy();
			
			redirect(base_url() . 'welcome');
			// exit;
		}
		
		if(in_array($this->currentController, $arrMaintenance) && $this->userRoleID != WEB_ADMIN_ROLE_ID) {
			redirect(base_url() . 'message/down_for_maintenance');
			exit;
		}
		
		//	==============	Setting the Dynamic Layout against current screen	==============	//
		if($this->currentAction != '' && $this->currentAction != 'index') {
			$this->screen_name = $this->currentAction;
			
			$moduleInformation = $this->configuration->getModules(array("m.module_name" => $this->currentController));
			$this->controller_id = $moduleInformation[0]["module_id"];
		} else {
			$this->screen_name = $this->currentController;
		}
		
		$this->layout = $this->privilege->getscreenlayout($this->screen_name, $this->controller_id);
		
		if($this->layout == '') {
			$this->layout = $this->template->config[$this->template->config['active_template']]['template'];
		} else {
			$this->layout = 'layouts/' . $this->layout;
			$this->template->add_region('menubar');
			$this->template->write_view('menubar', 'templates/menubar');
			//..	$this->template->add_region('searchbox');			$this->template->write_view('searchbox', 'templates/searchbox');
		}
		
		$this->template->set_master_template($this->layout);
		$this->template->write_view('font', 'templates/font');
		$this->template->write_view('header', 'templates/header');
		$this->template->write_view('footer', 'templates/footer');
		
		debugLog("Entered in " . current_url());
	}	
	
	function checkLogin() {

		// print_r($this->currentController);exit;
		if(!$this->userID && $this->currentController != 'welcome') {
			// print_r("adsfasdf");
			// exit;
			$this->session->set_userdata('came_from', current_url());

			redirect(base_url() . 'welcome');
			exit;
		} else if($this->userID && $this->currentController == 'welcome') {
			if($this->session->userdata('came_from') != '') {
				$strRedirectURL = $this->session->userdata('came_from');
				$this->session->unset_userdata('came_from');
				redirect($strRedirectURL);
			} else {
				redirect(base_url() . 'home');
			}
			exit;
		}
		
	}		
	
	function createMenu() {
		$modulePreference = array();
		$this->modulesAllowedForMenu = $this->privilege->getTopPrivileges($this->userRoleIDOriginal);
		$modulesSpeciallyAllowed = $this->privilege->getSpecialPrivileges($this->userEmpNum, $this->userCategoryID, $this->userRoleIDOriginal);
		// print_r($this->userRoleIDOriginal);
		// exit;
		
		if($this->userRoleID > 1) { # IF NOT CANDIDATE
			for($ind = 0; $ind < count($modulesSpeciallyAllowed); $ind++) {
				
				$notExists = true;
				for($jnd = 0; $jnd < count($this->modulesAllowedForMenu); $jnd++) {
					if($this->modulesAllowedForMenu[$jnd]['module_id'] == $modulesSpeciallyAllowed[$ind]['module_id']) {
						if(!(int)$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['emp_id'] && !(int)$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['job_category_id'] && !(int)$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['job_category_id']) {
							$this->modulesAllowedForMenu[$jnd]['assigned_role_id'] = $modulesSpeciallyAllowed[$ind]['assigned_role_id'];
						}
						if(!(int)$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['emp_id'] && !(int)$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['job_category_id']) {
							$this->modulesAllowedForMenu[$jnd]['assigned_role_id'] = $modulesSpeciallyAllowed[$ind]['assigned_role_id'];
						}
						if(!(int)$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['emp_id']) {
							$this->modulesAllowedForMenu[$jnd]['assigned_role_id'] = $modulesSpeciallyAllowed[$ind]['assigned_role_id'];
						}
						if((int)$modulesSpeciallyAllowed[$ind]['emp_id']) {
							$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['emp_id'] = true;
						}
						if((int)$modulesSpeciallyAllowed[$ind]['job_category_id']) {
							$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['job_category_id'] = true;
						}
						if((int)$modulesSpeciallyAllowed[$ind]['user_role_id']) {
							$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['user_role_id'] = true;
						}
						$notExists = false;
					}
				}
				if($notExists) {
					
					if((int)$modulesSpeciallyAllowed[$ind]['emp_id']) {
						$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['emp_id'] = true;
					}
					if((int)$modulesSpeciallyAllowed[$ind]['job_category_id']) {
						$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['job_category_id'] = true;
					}
					if((int)$modulesSpeciallyAllowed[$ind]['user_role_id']) {
						$modulePreference[$modulesSpeciallyAllowed[$ind]['module_id']]['user_role_id'] = true;
					}
					
					$arrIndex = count($this->modulesAllowedForMenu);					
					$this->modulesAllowedForMenu[$arrIndex]['module_id'] = $modulesSpeciallyAllowed[$ind]['module_id'];
					$this->modulesAllowedForMenu[$arrIndex]['module_name'] = $modulesSpeciallyAllowed[$ind]['module_name'];
					$this->modulesAllowedForMenu[$arrIndex]['display_name'] = $modulesSpeciallyAllowed[$ind]['display_name'];
					$this->modulesAllowedForMenu[$arrIndex]['assigned_role_id'] = $modulesSpeciallyAllowed[$ind]['assigned_role_id'];
				}
			}
			$this->modulesAllowedForMenu[]['module_name'] = $this->homeModule;
			
			for($ind = 0; $ind < count($this->modulesAllowedForMenu); $ind++) {
				
				if((int)$this->modulesAllowedForMenu[$ind]['module_id']) {
					
					$assignedRoleID = ((int)$this->modulesAllowedForMenu[$ind]['assigned_role_id'] > 0) ? $this->modulesAllowedForMenu[$ind]['assigned_role_id'] : $this->userRoleIDOriginal;
					$screensSpeciallyAllowed = $this->privilege->getSpecialPrivileges($this->userEmpNum, $this->userCategoryID, $assignedRoleID, false, $this->modulesAllowedForMenu[$ind]['module_id']);
					
					if(count($screensSpeciallyAllowed)) {
						$this->screensAllowedForMenu = $this->privilege->getPrivilege($this->userRoleIDOriginal, $this->modulesAllowedForMenu[$ind]['module_id'], true);
					} else {
						$this->screensAllowedForMenu = $this->privilege->getPrivilege($assignedRoleID, $this->modulesAllowedForMenu[$ind]['module_id'], true);
					}
					
					for($jnd = 0; $jnd < count($screensSpeciallyAllowed); $jnd++) {
					
						$notExists = true;
						for($knd = 0; $knd < count($this->screensAllowedForMenu); $knd++) {
							if($this->screensAllowedForMenu[$knd]['module_id'] == $screensSpeciallyAllowed[$jnd]['module_id']) {
								$notExists = false;
							}
						}
						if($notExists) {
							$arrIndex = count($this->screensAllowedForMenu);
							$this->screensAllowedForMenu[$arrIndex]['module_id'] = $screensSpeciallyAllowed[$jnd]['module_id'];
							$this->screensAllowedForMenu[$arrIndex]['module_name'] = $screensSpeciallyAllowed[$jnd]['module_name'];
							$this->screensAllowedForMenu[$arrIndex]['display_name'] = $screensSpeciallyAllowed[$jnd]['display_name'];
						}
					}
					
					$this->modulesAllowedForMenu[$ind]['sub_menu'] = $this->screensAllowedForMenu;
				}
			}
		}
		else {			
			for($ind = 0; $ind < count($this->modulesAllowedForMenu); $ind++) {
				
				if((int)$this->modulesAllowedForMenu[$ind]['module_id']) {
					$this->screensAllowedForMenu = $this->privilege->getPrivilege($this->userRoleIDOriginal, $this->modulesAllowedForMenu[$ind]['module_id'], true);
					$this->modulesAllowedForMenu[$ind]['sub_menu'] = $this->screensAllowedForMenu;
				}
			}
		}
	}
	
	function checkUser() {
		
		if($this->userRoleID == WEB_ADMIN_ROLE_ID || (int)$this->session->userdata('user_is_webadmin')) { 
		
			$this->session->set_userdata('user_is_webadmin', 1);
			
			$this->load->model('model_employee_management', 'employee', true);
			$this->load->model('model_user_management', 'user', true);
			
			//$_POST['sort_field'] = 'e.emp_first_name';
			//$_POST['sort_order'] = 'ASC';
			
			$this->allEmployees = $this->employee->getEmployees();
			
			//unset($_POST['sort_field']);
			//unset($_POST['sort_order']);
			
			if($this->input->post('changeUser')) {
				$empID = $this->input->post('changeUser');
				$redirectURL = $this->input->post('redirectURL');
				
				
				$arrEmp = $this->employee->getEmployees(array('e.emp_id' => $empID));
				$arrUser = $this->user->getUsers(array('employee_id' => $empID));
				
				$arrEmp = $arrEmp[0];
				$arrUser = $arrUser[0];
				
				$this->session->set_userdata('user_name', $arrUser['user_name']);
				$this->session->set_userdata('user_id', $arrUser['user_id']);
				$this->session->set_userdata('employee_id', $arrEmp['emp_id']);
				$this->session->set_userdata('user_role_id', $arrUser['user_role_id']);
				$this->session->set_userdata('display_name', $arrEmp['emp_full_name']);
				$this->session->set_userdata('emp_job_category_id', $arrEmp['emp_job_category_id']);
				$this->session->set_userdata('emp_company_id', $arrEmp['emp_company_id']);
				
				$this->userID = $arrUser['user_id'];
				$this->userName = $arrUser['user_name'];
				$this->userRoleID = (int)$arrUser['user_role_id'];
				$this->userCompanyID = $arrEmp['emp_company_id'];
				$this->userRoleIDOriginal = $this->userRoleID;
				$this->userEmpNum = (int)$arrEmp['emp_id'];
				$this->userWelcomeName = $arrEmp['emp_full_name'];
				$this->userCategoryID = $arrEmp['emp_job_category_id'];
				
				redirect(current_url());
			}
		}
				
	}
	
	function checkAccess() {
				
		if($this->userID)
		{
			if($this->currentController == 'ajax') {
				return;
			}
			
			$boolAllowed = false;
			$boolExists = $this->privilege->moduleScreenExists($this->currentController);	
			
			if(!$boolExists) {
				redirect(base_url() . 'message/not_found');
				exit;
			}
			// transfer_management
			$this->modulesAllowed = $this->modulesAllowedForMenu;
			// print_r($this->modulesAllowed);
			// exit;
			$this->modulesAllowed[]['module_name'] = $this->homeModule;
			for($ind = 0; $ind < count($this->modulesAllowed); $ind++) {
			
				// if($this->modulesAllowed[$ind]['module_name'] == 'transfer_requests'){

				// }
				// transfer_management
				if($this->modulesAllowed[$ind]['module_name'] == $this->currentController || $this->currentController == 'transfer_management') {						
					$boolAllowed = true;
					$this->currModuleID = $this->modulesAllowed[$ind]['module_id'];
				}
				// print_r($boolAllowed);
				// exit;
				
			}
			if(!$boolAllowed) {
				redirect(base_url() . 'message/access_denied');
				exit;
			}
			
			$boolAllowed = false;
			$boolExists = $this->privilege->moduleScreenExists($this->currentAction);		
			
			if(!$boolExists && $this->currentAction != 'index') {
				redirect(base_url() . 'message/not_found');
				exit;
			}
			
			$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $this->currModuleID);
			$this->screensAllowed[]['module_name'] = 'index';
			
			for($ind = 0; $ind < count($this->screensAllowed); $ind++) {
				
				if($this->screensAllowed[$ind]['module_name'] == $this->currentAction) {						
					$boolAllowed = true;				
				}
				
			}
			// print_r($boolAllowed);
			// exit;
			
			if(!$boolAllowed) {
				redirect(base_url() . 'message/access_denied');
				exit;
			}
		}
	}
	
	function checkSpecialPermission()
	{
		if($this->userRoleID > 1) { # IF NOT CANDIDATE
			$this->load->model('model_user_management', 'user', true);
			
			$arrWhereR = array(
								'usp.user_role_id' => $this->userRoleID,
								'usp.module_name' => $this->currentController
							);
			$this->arrPermissionsRole = $this->user->getSpecialPermissions($arrWhereR);
			
			$arrWhereD = array(
								'usp.job_category_id' => $this->userCategoryID,
								'usp.module_name' => $this->currentController
							);
			$this->arrPermissionsDepartment = $this->user->getSpecialPermissions($arrWhereD);
			
			$arrWhereE = array(
								'usp.emp_id' => $this->userEmpNum,
								'usp.module_name' => $this->currentController
							);
			$this->arrPermissionsEmployee = $this->user->getSpecialPermissions($arrWhereE);
			
			if(count($this->arrPermissionsEmployee) > 0) {
				foreach($this->arrPermissionsEmployee as $keyEmployee => $valEmployee) {
					if($this->userEmpNum == $valEmployee["emp_id"]) {
						if($valEmployee['sub_module_name'] == $this->currentAction) {
							$this->userRoleID = $valEmployee['assigned_role_id'];
							$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $valEmployee['sub_module_id']);
						} else if($valEmployee['sub_module_name'] == '' && $valEmployee['module_name'] == $this->currentController) {
							$this->userRoleID = $valEmployee['assigned_role_id'];
							$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $valEmployee['module_id']);
						}
					}
				}
			}
			else if(count($this->arrPermissionsDepartment) > 0) {
				foreach($this->arrPermissionsDepartment as $keyDepartment => $valDepartment) {
					if($this->userCategoryID == $valDepartment["job_category_id"]) {
						if($valDepartment['sub_module_name'] == $this->currentAction) {
							$this->userRoleID = $valDepartment['assigned_role_id'];
							$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $valDepartment['sub_module_id']);
						} else if($valDepartment['sub_module_name'] == '' && $valDepartment['module_name'] == $this->currentController) {
							$this->userRoleID = $valDepartment['assigned_role_id'];
							$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $valDepartment['module_id']);
						}
					}
				}
			}
			else if(count($this->arrPermissionsRole) > 0) {
				foreach($this->arrPermissionsRole as $keyRole => $valRole) {
					if($this->userRoleID == $valRole["user_role_id"]) {
						if($valRole['sub_module_name'] == $this->currentAction) {
							$this->userRoleID = $valRole['assigned_role_id'];
							$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $valRole['sub_module_id']);
						} else if($valRole['sub_module_name'] == '' && $valRole['module_name'] == $this->currentController) {
							$this->userRoleID = $valRole['assigned_role_id'];
							$this->screensAllowed = $this->privilege->getPrivilege($this->userRoleID, $valRole['module_id']);
						}
					}
				}
			}
			return $this->screensAllowed;
		}
	}
	
	function cookieSetUnset($cUnset = false, $userID = 0, $userName = '') {		
		if($cUnset) {	# UNSET		
			setcookie("hrm_user_id", "", time() - $this->session->sess_expiration, "/");
			setcookie("hrm_user_name", "", time() - $this->session->sess_expiration, "/");			
		} else if((int)$userID && $userName) {	# ADD OR UPDATE		
			setcookie("hrm_user_id", $userID, time() + $this->session->sess_expiration, "/");
			setcookie("hrm_user_name", $userName, time() + $this->session->sess_expiration, "/");			
		}
	}
	
	private function setupConfig($arrConfig) {
		for($ind = 0; $ind < count($arrConfig); $ind++) {
			if(!defined($arrConfig[$ind]['config_key'])) {
				define($arrConfig[$ind]['config_key'], $arrConfig[$ind]['config_value']);
			}
		}
	}
	
	private function setupEmail() {
		
		if(SMTP_HOST != '') {
					
			$this->config->set_item('protocol', 'smtp');
			$this->config->set_item('smtp_host', SMTP_HOST);
			$this->config->set_item('smtp_user', SMTP_USER);
			$this->config->set_item('smtp_pass', SMTP_PASSWORD);
			$this->config->set_item('smtp_port', SMTP_PORT);
			
		} else {
			$this->config->set_item('protocol', 'sendmail');
		}
	}
	
	public function sendEmail($arrTo = array(), $strSubject, $strMessage, $fromEmail = SYSTEM_EMAIL_ADDRESS, $fromName = SYSTEM_SENDER_NAME, $arrAttachment = array(), $boolSend = true, $ReplyTo = array()) {
		
		if((int)SYSTEM_SEND_EMAILS && $boolSend) {
			
			require_once(APPPATH . 'libraries/PHPMailerAutoload.php');
			$objMail = new PHPMailer();
			
			if(SMTP_HOST != '') { 			
				$objMail->isSMTP();
				//$objMail->SMTPDebug = 2;
				$objMail->SMTPSecure = 'tls';
				$objMail->SMTPAuth = true;
				$objMail->Host = SMTP_HOST;
				$objMail->Port = SMTP_PORT;
				$objMail->Username = SMTP_USER;
				$objMail->Password = SMTP_PASSWORD;
			} else {
				$objMail->isSendmail();
			}
			
			if(count($ReplyTo)) {
				$objMail->addReplyTo($ReplyTo[0], $ReplyTo[1]);
			}
			
			$objMail->setFrom($fromEmail, $fromName);
			
			for($ind = 0; $ind < count($arrTo); $ind++) {
				$objMail->addAddress($arrTo[$ind]);
			}
			
			$objMail->Subject = $strSubject;
			
			$objMail->msgHTML($strMessage);
			
			if(count($arrAttachment)) {
				for($ind = 0; $ind < count($arrAttachment); $ind++) {
					$objMail->AddAttachment($arrAttachment[$ind]);
				}
			}
			
			if (!$objMail->send()) {
				debugLog("Mailer Error: " . $objMail->ErrorInfo);
				//$this->sendEmail($arrTo, $strSubject, $strMessage, $fromEmail, $fromName, $arrAttachment, $boolSend, $ReplyTo);
				//echo "Mailer Error: " . $objMail->ErrorInfo; die();
				//return false;
			}
		}
		return true;
		
	}
	
	public function search($array, $key, $value) {
		$results = array();
	
		if (is_array($array)) {
			for($i=0; $i < count($array); $i++) {
				if (isset($array[$i][$key]) && $array[$i][$key] == $value) {
					$results[] = $array[$i];
				}
			}
		}
	
		return $results;
	}
}
