<?php
$userRoleFrom	=	(isset($_POST['userRoleFrom']))	?	$_POST['userRoleFrom']	:	'';
$moduleFrom		=	(isset($_POST['moduleFrom']))	?	$_POST['moduleFrom'] 	:	'';
$userRoleTo		=	(isset($_POST['userRoleTo']))	?	$_POST['userRoleTo']	:	'';
$moduleTo		=	(isset($_POST['moduleTo']))		?	$_POST['moduleTo'] 		:	'';
?>

<form name="frmCopyUserPermission" id="frmCopyUserPermission" method="post">
<div class="listPageMain">
	<div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
			<tr>
	        	<td class="formHeaderRow" colspan="2">Copy Privileges From</td>
			</tr>
			<tr>
				<td class="formLabelContainer">User Role:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<select name="userRoleFrom" id="userRoleFrom" class="dropDown" onChange="populateRoleModuleScreens(this.value,$('#moduleFrom').val(),$('#userRoleFrom option:selected').text(),'1');">
						<option value="">Select User Role</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>" ><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="formLabelContainer">Module:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<select name="moduleFrom" id="moduleFrom" class="dropDown" onChange="populateRoleModuleScreens($('#userRoleFrom').val(),this.value,$('#userRoleFrom option:selected').text(),'1');checkToPopulate();">
						<option value="">Select Module</option>
					<?php
					if (count($modules)) {
						foreach($modules as $arrModule) {
					?>
						<option value="<?php echo $arrModule['module_id']; ?>"><?php echo $arrModule['display_name']; ?></option>
					<?php
						}
					}
					?>
					</select>
				</td>
			</tr>
		</table>
	</div>
    <div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
			<tr>
	        	<td class="formHeaderRow" colspan="2">Copy Privileges To</td>
			</tr>
			<tr>
				<td class="formLabelContainer">User Role:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<select name="userRoleTo" id="userRoleTo" class="dropDown" onChange="populateRoleModuleScreens(this.value,$('#moduleTo').val(),$('#userRoleTo option:selected').text(),'2');">
						<option value="">Select User Role</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>" ><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="formLabelContainer">Module:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<select name="moduleTo" id="moduleTo" class="dropDown" onchange="checkToPopulate();populateRoleModuleScreens($('#userRoleTo').val(),this.value,$('#userRoleTo option:selected').text(),'2');">
						<option value="">Select Module</option>
					<?php
					if (count($modules)) {
						foreach($modules as $arrModule) {
					?>
						<option value="<?php echo $arrModule['module_id']; ?>"><?php echo $arrModule['display_name']; ?></option>
					<?php
						}
					}
					?>
					</select>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="formMainWide formAlternateRow" id="screens" style="display:none">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		</table>
	</div>
	
	<div class="listPageMain" id="copyTo" style="display:none" align="center"><img src="<?php echo $this->imagePath; ?>/arrow-down.png"></div>
	<div class="formMainWide yellow" id="screensTo" style="display:none">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		</table>
	</div>
	
	<table border="0" cellspacing="0" cellpadding="0" style="width:100%;display:none;" id="copyPermission">
		<tr>
			<td class="formLabelContainer"></td>
			<td class="formButtonContainerWide" align="left">
				<input class="smallButton" id="deletButton" name="copyUserPermission" type="submit" value="Save">
			</td>
		</tr>
	</table>
    
</div>
</form>

<script>
	$('#userRoleFrom').val('<?php echo $userRoleFrom; ?>');
	$('#moduleFrom').val('<?php echo $moduleFrom; ?>');
	$('#userRoleTo').val('<?php echo $userRoleTo; ?>');
	$('#moduleTo').val('<?php echo $moduleTo; ?>');
	
	<?php if((isset($_POST['userRoleFrom'])) && (isset($_POST['moduleFrom']))) { ?>
		populateRoleModuleScreens(<?php echo $_POST['userRoleFrom']; ?>,<?php echo $_POST['moduleFrom']; ?>,$('#userRoleFrom option:selected').text(),'1');
		$('#moduleTo').val(<?php echo $_POST['moduleFrom']; ?>);
	<?php } ?>
	
	<?php if($canWrite == 0) { ?>
		$("#frmCopyUserPermission :input").attr("disabled", true);
	<?php } ?>
	
	function changeValues(moduleID,canRead,canWrite,canDelete)
	{
		var status = $('#status'+moduleID).val();
		if(status == 0) {			
			$('#can_read'+moduleID).attr('checked', false);
			$('#can_write'+moduleID).attr('checked', false);
			$('#can_delete'+moduleID).attr('checked', false);
		} else if(status == 1) {
			if(canRead == 0){
				var canReadProp = false;
			}else if(canRead == 1){
				var canReadProp = true;
			}
			$('#can_read'+moduleID).prop('checked', canReadProp);
			
			if(canWrite == 0){
				var canWriteProp = false;
			}else if(canWrite == 1){
				var canWriteProp = true;
			}
			$('#can_write'+moduleID).prop('checked', canWriteProp);
			
			if(canDelete == 0){
				var canDeleteProp = false;
			}else if(canDelete == 1){
				var canDeleteProp = true;
			}
			$('#can_delete'+moduleID).prop('checked', canDeleteProp);
		}
		
	}
	
	function checkToPopulate()
	{
		if($('#moduleFrom').val() != "") {
			$('#moduleTo').val($('#moduleFrom').val());
			populateRoleModuleScreens($('#userRoleTo').val(),$('#moduleTo').val(),$('#userRoleTo option:selected').text(),'2');
		}
	}
	
</script>