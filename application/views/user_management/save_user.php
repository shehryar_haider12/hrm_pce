<?php
$userRole 		=	(isset($_POST['userRole']))			?	$_POST['userRole']		:	$record['user_role_id'];
$employeeId 	=	(isset($_POST['employeeId'])) 		? 	$_POST['employeeId'] 	:	$record['employee_id'];
$loginName 		=	(isset($_POST['loginName']))		?	$_POST['loginName']		:	$record['user_name'];
$loginPassword 	= 	(isset($_POST['loginPassword'])) 	?	$_POST['loginPassword'] :	$record['plain_password'];
if ($record['user_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['user_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['user_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		=	(isset($_POST['status'])) 			?	$_POST['status']		:	$recordStatus;
?>

<form name="frmAddUser" id="frmAddUser" method="post">
<div class="listPageMain">
	<div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
			<tr>
            	<?php if($record['user_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update User</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New User</td>
                <?php } ?>
			</tr>
			<tr>
				<td class="formLabelContainer">User Role:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
				<select id="userRole" name="userRole" class="dropDown">
					<option value="">Select One</option>
					<?php
					if (count($userRoles)) {
						foreach($userRoles as $arrUserRole) {
					?>
						<option value="<?php echo $arrUserRole['user_role_id']; ?>"><?php echo $arrUserRole['user_role_name']; ?></option>
					<?php
						}
					}
					?>
				</select>
				</td>
			</tr>
			<tr class="formAlternateRow">
				<td class="formLabelContainer">Employee Id:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<input class="textBox" type="text" id="employeeId" name="employeeId" value="<?php echo $employeeId; ?>" size="5" maxlength="5" <?php if($freezeEmployeeId == 1) { echo 'readonly'; } ?> />
				</td>
			</tr>
			<tr>
				<td class="formLabelContainer">User Name:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<input class="textBox" type="text" id="loginName" name="loginName" value="<?php echo $loginName; ?>" size="40" maxlength="40" />
				</td>
			</tr>
			<tr class="formAlternateRow">
				<td class="formLabelContainer">Password:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<input class="textBox" type="password" id="loginPassword" name="loginPassword" value="<?php echo $loginPassword; ?>" size="20" maxlength="20" />
				</td>
			</tr>
			<tr>
				<td class="formLabelContainer">Confirm Password:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<input class="textBox" type="password" id="loginConfirmPassword" name="loginConfirmPassword" value="<?php echo $loginPassword; ?>" size="20" maxlength="20" />
				</td>
			</tr>
			<tr class="formAlternateRow">
				<td class="formLabelContainer">Status:</td>
				<td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
				</td>
			</tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addUser" type="submit" value="Save">
                    <?php if($record['employeeId'] && strpos($_SERVER["REQUEST_URI"],$record['employeeId']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_user' ?>';">
                    <?php } ?>
                </td>
            </tr>
		</table>
	</div>
</div>
</form>

<script>
	$('#userRole').val('<?php echo $userRole; ?>');
	$('#status').val('<?php echo $status; ?>');
	<?php if($canWrite == 0) { ?>
		$("#frmAddUser :input").attr("disabled", true);
	<?php } ?>
	<?php if(($this->userID != $employeeId)) { ?>
		$('#rowPassword').hide();
		$('#rowConfirmPassword').hide();
	<?php } ?>
	<?php if(($this->userID == $employeeId)||($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) { ?>
		$('#rowPassword').show();
		$('#rowConfirmPassword').show();
	<?php } ?>
</script>