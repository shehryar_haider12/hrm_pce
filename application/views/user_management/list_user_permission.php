<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$userRole = ($userRole != '') ? $userRole : $this->input->post('userRole');
$module = ($module != '') ? $module : $this->input->post('module');
$status = ($status != '') ? $status : $this->input->post('status');
$canReadCheck = ($canReadCheck != '') ? $canReadCheck : $this->input->post('canReadCheck');
$canWriteCheck = ($canWriteCheck != '') ? $canWriteCheck : $this->input->post('canWriteCheck');
$canDeleteCheck = ($canDeleteCheck != '') ? $canDeleteCheck : $this->input->post('canDeleteCheck');
$txtSortField = (isset($_POST['sort_field'])) ? $_POST['sort_field'] : '';
$txtSortOrder = (isset($_POST['sort_order'])) ? $_POST['sort_order'] : '';
?>

<div class="centerButtonContainer">
    <input class="addButton" type="button" value="Copy User Permission" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/copy_user_permission/' ?>';" />
</div>

<form name="frmListUserPermission" id="frmListUserPermission" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
		<div class="searchHeader">Search Criteria</div>
		
		<div class="searchcontentmain">
			<div class="searchCol">
				<div class="labelContainer">User Role:</div>
				<div class="textBoxContainer">
					<select class="dropDown" id="userRole" name="userRole">
						<option value="">All</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>"><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
				</div>
				
				<div class="labelContainer">Module:</div>
				<div class="textBoxContainer">
					<select class="dropDown" id="module" name="module">
						<option value="">All</option>
						<?php
						if (count($modules)) {
							foreach($modules as $arrModule) {
						?>
							<option value="<?php echo $arrModule['module_id']; ?>"><?php echo $arrModule['display_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
				</div>
			</div>
			
			<div class="searchCol">
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<select class="dropDown" name="status" id="status">
						<option value="">All</option>
						<option value="1" >Allowed</option>
						<option value="-1">DisAllowed</option>
					</select>
				</div>
				
				<div class="labelContainer">Can Read:</div>
				<div class="textBoxContainer">
					<select class="dropDown" id="canReadCheck" name="canReadCheck">
						<option value="">All</option>
						<option value="1" >Yes</option>
						<option value="-1">No</option>
					</select>
				</div>
			</div>
			
			<div class="searchCol">
				<div class="labelContainer">Can Write:</div>
				<div class="textBoxContainer">
					<select class="dropDown" id="canWriteCheck" name="canWriteCheck">
						<option value="">All</option>
						<option value="1" >Yes</option>
						<option value="-1">No</option>
					</select>
				</div>
				
				<div class="labelContainer">Can Delete:</div>
				<div class="textBoxContainer">
					<select class="dropDown" id="canDeleteCheck" name="canDeleteCheck">
						<option value="">All</option>
						<option value="1" >Yes</option>
						<option value="-1">No</option>
					</select>
				</div>
			</div>
			
			<div class="buttonContainer">
            	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      			<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
				<input type="submit" class="searchButton" name="btnSearchUserPermission" id="btnSearchUserPermission" value="Search">
			</div>
		</div>
	</div>
	
	<script>
		$('#userRole').val('<?php echo $userRole; ?>');
		$('#module').val('<?php echo $module; ?>');
		$('#status').val('<?php echo $status; ?>');
		$('#canReadCheck').val('<?php echo $canReadCheck; ?>');
		$('#canWriteCheck').val('<?php echo $canWriteCheck; ?>');
		$('#canDeleteCheck').val('<?php echo $canDeleteCheck; ?>');
	</script>
</form>

	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
	<?php
	if($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
	</div>
	
	<div class="listContentMain">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
			<tr class="listHeader">
				<td class="listHeaderCol">User Role</td>
				<td class="listHeaderColClickable" onclick="setSort('module_id', 'frmListUserPermission')">Module</td>
				<td class="listHeaderColClickable" onclick="setSort('sub_module_id', 'frmListUserPermission')">Sub Module</td>
				<td class="listHeaderColClickable" onclick="setSort('status', 'frmListUserPermission')">Status</td>
				<td class="listHeaderCol">Can Read</td>
				<td class="listHeaderCol">Can Write</td>
				<td class="listHeaderCol">Can Delete</td>
				<?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['status'] != STATUS_DELETED))) { ?>
				<td class="listHeaderColLast">Actions</td>
				<?php } ?>
			</tr>
			
			<?php
			for($ind = 0; $ind < count($arrRecords); $ind++)
			{
				($arrRecords[$ind]['status'] == 0) ? $classListingData = "listContentAlternate" : $classListingData = "listContent";
			?>
			<tr class="<?php echo $classListingData; ?>">
				<td class="listContentCol"><?php echo $arrRecords[$ind]['user_role_name']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['module_name']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['sub_module_name']; ?></td>
				<td class="listContentCol"><?php if ($arrRecords[$ind]['status'] == 1) echo "Allowed"; else echo "DisAllowed"; ?></td>
				<td class="listContentCol"><?php if($arrRecords[$ind]['can_read'] == 1) echo "Yes"; else echo "No"; ?></td>
				<td class="listContentCol"><?php if($arrRecords[$ind]['can_write'] == 1) echo "Yes"; else echo "No"; ?></td>
				<td class="listContentCol"><?php if($arrRecords[$ind]['can_delete'] == 1) echo "Yes"; else echo "No"; ?></td>
				<?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['status'] != 2))) { ?>
				<td class="listContentColLast">
					<div class="colButtonContainer">
					<?php if($canWrite == 1) { ?>
						<input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_user_permission/' . $arrRecords[$ind]['user_role_id'] . '/' . $arrRecords[$ind]['module_id'] . '/' . $arrRecords[$ind]['sub_module_id']; ?>';" />
					<?php } ?>
					<?php if($canDelete == 1) { ?>
						<input class="smallButton" id="deletButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['user_permission_id']; ?>');" />
					<?php } ?>
					</div>
				</td>
				<?php } ?>
			</tr>
			<?php
			}
			if(!$ind)
			{
			?>
			<tr class="listContentAlternate">
				<td colspan="8" align="center" class="listContentCol">No Record Found</td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
</div>
