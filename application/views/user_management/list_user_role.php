<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$status = ($status != '') ? $status : $this->input->post('status');
$isAdmin = ($isAdmin != '') ? $isAdmin : $this->input->post('isAdmin');
$txtSortField = (isset($_POST['sort_field'])) ? $_POST['sort_field'] : 'user_role_id';
$txtSortOrder = (isset($_POST['sort_order'])) ? $_POST['sort_order'] : 'DESC';
?>

<form name="frmListUserRoles" id="frmListUserRoles" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
		<div class="searchHeader">Search Criteria</div>
		
		<div class="searchcontentmain">
			<div class="searchCol">
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'All', 'dropDown'); ?>
				</div>
				
				<div class="labelContainer">Is Admin:</div>
				<div class="textBoxContainer">
					<div class="formCheckBoxContainer">
					<input value="1" type="checkbox" id="isAdmin" name="isAdmin" <?php echo ($isAdmin == 1)? 'checked="checked"' : ''; ?>>
					</div>
				</div>
			</div>
			<div class="formButtonContainerWide">
            	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      			<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
				<input type="submit" class="searchButton" name="btnSearchUserRole" id="btnSearchUserRole" value="Search">
			</div>

		</div>
	</div>
	
	<script>
		$('#status').val('<?php echo $status; ?>');
	</script>
</form>

	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
	<?php
	if($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
	</div>

	<div class="listContentMain">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
			<tr class="listHeader">
				<td class="listHeaderCol">User Role Name</td>
				<td class="listHeaderColClickable" onclick="setSort('is_admin', 'frmListUserRoles')">Is Admin</td>
				<td class="listHeaderColClickable" onclick="setSort('user_role_status', 'frmListUserRoles')">Status</td>
				<?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['user_role_status'] != STATUS_DELETED))) { ?>
				<td class="listHeaderColLast" style="width:387px">Actions</td>
				<?php } ?>
			</tr>
			
			<?php
			for($ind = 0; $ind < count($arrRecords); $ind++)
			{
				($arrRecords[$ind]['user_role_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate";
			?>
			<tr class="<?php echo $classListingData; ?>">
				<td class="listContentCol"><?php echo $arrRecords[$ind]['user_role_name']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['is_admin']; ?></td>
				<td class="listContentCol"><?php if ($arrRecords[$ind]['user_role_status'] == STATUS_ACTIVE) echo "Active"; else if ($arrRecords[$ind]['user_role_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; else if ($arrRecords[$ind]['user_role_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
				<?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['user_role_status'] != STATUS_DELETED))) { ?>
				<td class="listContentColLast">
					<div class="colButtonContainer">
					<?php if($canWrite == 1) { ?>
						<input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_user_role/' . $arrRecords[$ind]['user_role_id']; ?>';" />
					<?php } ?>
					<?php if(($canDelete == 1) && ($arrRecords[$ind]['user_role_status'] != STATUS_DELETED)) { ?>
						<input class="smallButton" id="deletButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['user_role_id']; ?>');" />
					<?php } ?>
					</div>
				</td>
				<?php } ?>
			</tr>
			<?php
			}
			if(!$ind)
			{
			?>
			<tr class="listContentAlternate">
				<td colspan="5" align="center" class="listContentCol">No Record Found</td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
</div>