<?php
$path 			= '/'.$this->currentController.'/'.$this->currentAction;
$employeeId 	= ($employeeId != '') 			? $employeeId 			: $this->input->post('employeeId');
$empIP			= ($empIP != '') 				? $empIP 				: $this->input->post('empIP');
$candidateId 	= ($candidateId != '') 			? $candidateId 			: $this->input->post('candidateId');
$nameContains 	= ($nameContains != '') 		? $nameContains 		: $this->input->post('nameContains');
$userRole 		= ($userRole != '') 			? $userRole 			: $this->input->post('userRole');
$status 		= ($status != '') 				? $status 				: $this->input->post('status');
$txtSortField 	= (isset($_POST['sort_field'])) ? $_POST['sort_field'] 	: '';
$txtSortOrder 	= (isset($_POST['sort_order'])) ? $_POST['sort_order'] 	: '';
?>

<form name="frmListUsers" id="frmListUsers" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
		<div class="searchHeader">Search Criteria</div>
		
		<div class="searchcontentmain">
			<div class="searchCol">
				<div class="labelContainer">Employee:</div>
				<div class="textBoxContainer">					
                    <select id="employeeId" name="employeeId" class="dropDown">
                        <option value="">All</option>
                        <?php
                        if (count($arrEmployees)) {
                            foreach($arrEmployees as $key => $arrEmployee) {
                        ?>
                            <optgroup label="<?php echo $key; ?>">
                                <?php for($i=0;$i<count($arrEmployee);$i++) { ?>					
                                    <option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_full_name']; ?></option>
                                <?php } ?>
                            </optgroup>
                        <?php	}
                        }
                        ?>
                    </select>
				</div>
                
                <!-- <div class="labelContainer">IP Extension:</div>
                <div class="textBoxContainer">
                  <input type="text" id="empIP" name="empIP" maxlength="4" class="textBox" value="<?php echo $empIP; ?>">
                </div> -->
			</div>
            			
			<div class="searchCol">
            	<!-- <div class="labelContainer">Candidate Id:</div>
				<div class="textBoxContainer">
					<input class="textBox" type="text" id="candidateId" name="candidateId" size="8" value="<?php echo $candidateId; ?>" />
				</div> -->
           		
            	<div class="labelContainer">Name Contains:</div>
                <div class="textBoxContainer">
                    <input type="text" id="nameContains" name="nameContains" class="textBox" value="<?php echo $nameContains; ?>" />
                </div>                
			</div>
            
            <div class="searchCol">
            	<div class="labelContainer">User Role:</div>
				<div class="textBoxContainer">
					<select class="dropDown" id="userRole" name="userRole">
						<option value="">All</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>"><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
				</div>
                
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'All', 'dropDown'); ?>
				</div>
            </div>
			
			<div class="formButtonContainerWide">
            	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      			<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
				<input type="submit" class="searchButton" name="btnSearchUser" id="btnSearchUser" value="Search">
			</div>
		</div>
	</div>
	
	<script>
		$('#employeeId').val('<?php echo $employeeId; ?>');
		$('#userRole').val('<?php echo $userRole; ?>');
		$('#status').val('<?php echo $status; ?>');
	</script>
</form>

	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
	<?php
	if($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
	</div>

	<div class="listContentMain">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
			<tr class="listHeader">
				<td class="listHeaderColClickable" onclick="setSort('user_role_id', 'frmListUsers')">User Role</td>
				<td class="listHeaderCol">User Name</td>
				<td class="listHeaderColClickable" onclick="setSort('user_status', 'frmListUsers')">Status</td>
				<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
				<td class="listHeaderCol" style="width:200px">Password</td>
				<?php } ?>
				
				<?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['user_status'] != STATUS_DELETED))) { ?>
				<td class="listHeaderColLast">Actions</td>
				<?php } ?>
			</tr>
			
			<?php
			for($ind = 0; $ind < count($arrRecords); $ind++)
			{
				($arrRecords[$ind]['user_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
			?>
			<tr class="<?php echo $classListingData; ?>">
				<td class="listContentCol"><?php echo $arrRecords[$ind]['user_role_name']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['user_name']; ?></td>
				<td class="listContentCol"><?php if ($arrRecords[$ind]['user_status'] == STATUS_ACTIVE) echo "Active"; elseif ($arrRecords[$ind]['user_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; elseif ($arrRecords[$ind]['user_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
				<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['plain_password']; ?></td>
				<?php } ?>
				<?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['user_status'] != STATUS_DELETED))) { ?>
				<td class="listContentColLast">
					<div class="colButtonContainer">
					<?php 
					if($arrRecords[$ind]['employee_id'] == "") 
					{
						$editID = $arrRecords[$ind]['candidate_id'];
						$isemployeeFlag = 0;
					}
					else
					{
						$editID = $arrRecords[$ind]['employee_id'];
						$isemployeeFlag = 1;
					}
					
					if($canWrite == 1) { ?>
						<input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_user/' . $isemployeeFlag. '/' . $editID; ?>';" />
					<?php } ?>
					<?php if(($canDelete == 1) && ($arrRecords[$ind]['user_status'] != STATUS_DELETED)) { ?>
						<input class="smallButton test" id="deletButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['employee_id']; ?>');" />
					<?php } ?>
					</div>
				<?php } ?>
				</td>
			</tr>
			<?php
			}
			if(!$ind)
			{
			?>
			<tr class="listContentAlternate">
				<td colspan="6" align="center" class="listContentCol">No Record Found</td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
</div>

