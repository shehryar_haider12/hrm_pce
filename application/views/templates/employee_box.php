<?php
$allowedSubModulesList = $this->privilege->getPrivilege($this->userRoleID, 5, true);
// .. print("==========>><PRE>"); 
// print_r($allowedSubModulesList);
// exit;

if(count($arrEmployee)) {
	if(!file_exists($pictureFolder . $arrEmployee['emp_photo_name']) || empty($arrEmployee['emp_photo_name'])) {
		if(empty($arrEmployee['emp_gender'])) {
			$arrEmployee['emp_gender'] = 'male';
		}
		$arrEmployee['emp_photo_name'] = 'no_image_' . strtolower($arrEmployee['emp_gender']) . '.jpg';
	}
?>
    <div class="employeeDetailLeftCol">
      <div class="employeePicMain">
		<div class="employeeNameContainer">
		<?php echo $arrEmployee['emp_full_name']; ?>
		</div>
        <div class="employeePicContainer">
            <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrEmployee['emp_photo_name']; ?>" width="185">
        </div>
      </div>
      <div class="employeeDetailLinksMain">
        <ul>
          <li id="save_employee"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/save_employee/' . $arrEmployee['emp_id']; ?>">Employee Profile</a></li>
          <li id="employment_details"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/employment_details/' . $arrEmployee['emp_id']; ?>">Employment Details</a></li>
          <li id="emp_education_history"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/emp_education_history/' . $arrEmployee['emp_id']; ?>">Educational History</a></li>
          <li id="emp_work_history"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/emp_work_history/' . $arrEmployee['emp_id']; ?>">Professional Experience</a></li>
          <li id="dependents"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/dependents/' . $arrEmployee['emp_id']; ?>">Dependents</a></li>
          <li id="emergency_contacts"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/emergency_contacts/' . $arrEmployee['emp_id']; ?>">Emergency Contacts</a></li>
          <li id="benefits"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/benefits/' . $arrEmployee['emp_id']; ?>">Benefits</a></li>
          <li id="document_list"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/document_list/' . $arrEmployee['emp_id']; ?>">Documents</a></li>
          <li id="salary_details"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/salary_details/' . $arrEmployee['emp_id']; ?>">Salary Details</a></li>
          <li id="transfer_history"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/transfer_history/' . $arrEmployee['emp_id']; ?>">Tranfer History</a></li>
          <?php if(isAdmin($this->userRoleID)) { ?>
          <li id="employment_status"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/employment_status/' . $arrEmployee['emp_id']; ?>">Employee's Exit</a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <script>$('#<?php echo $this->currentAction; ?>').addClass('selected');</script>
<?php
}
?>
