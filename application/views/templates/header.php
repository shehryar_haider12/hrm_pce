<style>
    .selectize-control.dropDown.single {
        width: 250px !important;
    }
</style>
<?php $userRoleID = $this->session->userdata('user_role_id');

$headerTitle = "PCE Employee Dashboard - HRMS";
$url = $_SERVER['REQUEST_URI'];
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- <div class="headerTitle">
    <a href="<?php echo base_url();?>home">
        <img src="<?php echo $this->imagePath; ?>/logo.png" alt=" HRM" title="ValuStrat HRM">
    </a>
</div>

<?php if($this->currentController != 'message') {
		if($this->userID != 0) { ?>
	<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || (int)$this->session->userdata('user_is_webadmin')) { ?>
<div class="logoutMain" style="padding-top:22px">
	<div style="float:right">    	
    	<form name="frmChangeUser" id="frmChangeUser" method="post">
            <div class="logoutText">Use As:&nbsp;
            <select name="changeUser" id="changeUser" onchange="this.form.submit()" class="dropDown" style="float:right">
                <option value="">Select Employee</option>
                <?php for($ind = 0; $ind < count($this->allEmployees); $ind++) { ?>
                <option value="<?php echo $this->allEmployees[$ind]['emp_id']; ?>"><?php echo $this->allEmployees[$ind]['emp_full_name']; ?> - <?php echo $this->allEmployees[$ind]['user_role_name']; ?></option>
                <?php } ?>
            </select></div>
            <script>$('#changeUser').val('<?php echo (int)$this->userEmpNum; ?>');</script>
        </form>
    </div>
    <?php } else { ?>
    <div class="logoutMain">
    <?php } ?>
	<div class="logoutText">
		Welcome - <?php echo $this->userWelcomeName; ?>
	</div> -->
<!-- 	
	<?php echo form_open(); ?>
	<div class="logoutButton">
		<input class="smallButton" name="logoffBtn" type="submit" value="Logout">
		<input type="hidden" name="logoff" value="1">
	</div>
	<?php echo form_close(); ?>
</div> -->
<div>
    <div id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed main-content-narrow">
		<nav class="cl" style="background-image: linear-gradient(45deg, #780206, #061161)" id="sidebar" aria-label="Main Navigation">
                <!-- Side Header (mini Sidebar mode) -->
                <div class="smini-visible-block">
                    <div class="content-header">
                        <!-- Logo -->
                        <a class="font-w600 text-white tracking-wide" href="index.html">
                            D<span class="opacity-75">x</span>
                        </a>
                        <!-- END Logo -->
                    </div>
                </div>
                <!-- END Side Header (mini Sidebar mode) -->

                <!-- Side Header (normal Sidebar mode) -->
                <div class="smini-hidden">
                    <div class="content-header justify-content-lg-center">
                        <!-- Logo -->
                        <a class="font-w600 text-white tracking-wide" href="index.html">    
						<!-- Dash<span class="opacity-75">mix</span>
                        <span class="font-w400">Modern</span> -->
						<img src="https://amanahmall.com.pk/wp-content/uploads/2018/12/Pakistan-Currency-Exchange.png" alt="Pakistan Currency Exchange Logo" width="150" heigh="auto">
                        </a>
						
                        <!-- END Logo -->
                        <!-- Options -->
                        <div class="d-lg-none">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <a class="text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                                <i class="fa fa-times-circle"></i>
                            </a>
                            <!-- END Close Sidebar -->
                        </div>
                        <!-- END Options -->
                    </div>
                </div>
                <!-- END Side Header (normal Sidebar mode) -->

                <!-- Sidebar Scrolling -->
                <div class="js-sidebar-scroll">
                    <!-- Side Actions -->
                    <div class="content-side content-side-full bg-black-10 text-center">
                        <div class="smini-hide">
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-user-circle"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-pencil-alt"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-file-alt"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-envelope"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-cog"></i>
                            </button>
                        </div>
                    </div>
                    <!-- END Side Actions -->
                    <!-- UserRole Management Start -->
                    <div style="display: flex">
        <form name="frmChangeUser" id="frmChangeUser" method="post">
            <div class="useAs">Use As:&nbsp;
            <select name="changeUser" id="changeUser" onchange="this.form.submit()" class="dropDown" style="float:right">
                <option value="">Select Employee</option>
                <?php for($ind = 0; $ind < count($this->allEmployees); $ind++) { ?>
                <option value="<?php echo $this->allEmployees[$ind]['emp_id']; ?>"><?php echo $this->allEmployees[$ind]['emp_full_name']; ?> - <?php echo $this->allEmployees[$ind]['user_role_name']; ?></option>
                <!-- USER CHECKING FOR DEV PROCESS -->
                <!-- <//?php echo $arrEmpSupervisors; ?> -->
                <?php } ?>
                <!-- <//?php echo $userRoleID;?> -->  
                <?php echo print_r($arrEmpSupervisors); ?>
            </select></div>
            <script>$('#changeUser').val('<?php echo (int)$this->userEmpNum; ?>');</script>
        </form>
        <br><br>
        <?php echo form_open(); ?>
            <div class="logoutButton" style="    position: relative;right: 104px;top: 110px;">
            <input class="logout" name="logoffBtn" type="submit" value="Logout">
            <input type="hidden" name="logoff" value="1">
            </div>
        <?php echo form_close(); ?>
    </div>
    <div class="content-side">
        <ul class="nav-main">
            <li class="nav-main-item">
                <a class="nav-main-link active" href="/HRM/home">
                    <span class="nav-main-link-name">Log As </span><span class="nav-main-link-name"><?php echo $this->session->userdata('display_name');  
                    ?> </span>
                    <!-- <span class="nav-main-link-badge badge badge-pill badge-primary">3</span> -->
                </a>
            </li>
        </ul>
    </div>
               
             
<!-- UserRole Management Ends -->
                    <!-- Side Navigation -->
                    <div class="content-side">
                        <ul class="nav-main">
                            <hr>
                            <li class="nav-main-item">
                                <a class="nav-main-link active" href="/HRM/home">
                                    <i class="nav-main-link-icon fa fa-chart-bar"></i>
                                    <span class="nav-main-link-name">Dashboard</span>
                                    <!-- <span class="nav-main-link-badge badge badge-pill badge-primary">3</span> -->
                                </a>
                            </li>
                            <li class="nav-main-heading">Manage</li>
                            <?php if($userRoleID == 10){
                            echo '
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/system_configuration">
                                    <i class="nav-main-link-icon fa fa-cogs"></i>
                                    <span class="nav-main-link-name">System Configuration</span>
                                </a>
                                <ul class="nav-main-submenu">
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/system_settings">
                                            <i class="nav-main-link-icon fa fa-wrench"></i>
                                            <span class="nav-main-link-name">System Settings</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/list_module">
                                            <i class="nav-main-link-icon fa fa-sliders"></i>
                                            <span class="nav-main-link-name">Modules</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/list_companies">
                                            <i class="nav-main-link-icon fa fa-building"></i>
                                            <span class="nav-main-link-name">Companies</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/docList_Real">
                                            <i class="nav-main-link-icon fa fa-star"></i>
                                            <span class="nav-main-link-name">Documents</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/list_job_category">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Departments</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/list_religions">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Religions</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/list_edu_level">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Education Levels</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/system_configuration/list_edu_major">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Education Majors</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/user_management">
                                    <i class="nav-main-link-icon fa fa-user"></i>
                                    <span class="nav-main-link-name">User Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/save_user">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add Users</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/list_user">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage Users</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/save_user_role">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add User Role</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/list_user_role">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage User Roles</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/save_user_permission">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add User Permission</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/list_user_permission">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage User Permission</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/special_permissions">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Special Permissions</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/user_management/user_logs">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">User Logs</span>
                                        </a>
                                    </li>';}?>
                                </ul>
                            </li>
                            <?php
                                if($userRoleID != CCTV_OPERATOR && $userRoleID != COMPLAINCE_MANAGER && $userRoleID != COMPLAINCE_OPERATOR){
                            ?>
							<li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                    <i class="nav-main-link-icon fa fa-user"></i>
                                    <span class="nav-main-link-name">Employee Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                <?php if($userRoleID == 10){
                                echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/add_employee">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add Employee</span>
                                        </a>
                                    </li>';}?>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/save_employee">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Profile</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/employment_details">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Employment Details</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/emp_education_history">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Educational History</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/emp_work_history">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Work Experience</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/dependents">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Dependents</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/emergency_contacts">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Emergency Contacts</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/document_list">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Documents List</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/salary_details">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Salary</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/benefits">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Benefits</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/transfer_history">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Transfer History</span>
                                        </a>
                                    </li>
                                    <?php if($userRoleID == 10){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/documents">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Documents</span>
                                        </a>
                                    </li>';}?>
									<?php if($userRoleID == 10 || $userRoleID == 11){
									echo'<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/employee_management/list_employees">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">List Employees</span>
                                        </a>
                                    </li>';}?>
								</ul>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                    <i class="nav-main-link-icon fa fa-file-alt"></i>
                                    <span class="nav-main-link-name">Policies & Announcements</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/policy_announcements">
                                            <i class="nav-main-link-icon fa fa-plus"></i>
                                            <span class="nav-main-link-name">Policies & Announcements</span>
                                        </a>
                                    </li>
                                    <?php if($userRoleID == 10){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/policy_announcements/announcements">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">Add Edit Policies Announcement</span>
                                        </a>
                                    </li>';}?>
                                </ul>
                            </li>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/complain_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Request Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/complain_management/submit_complain">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Request Submission</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/complain_management/list_complains">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">List Requests</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/attendance_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Attendance & Leave</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/attendance_management/att_detail">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Attendance Details</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/attendance_management/leave_status">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Leaves</span>
                                        </a>
                                    </li>
                                    <?php if($userRoleID == 10 || $userRoleID == 11){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/attendance_management/leave_requests">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Leave Request</span>
                                        </a>
                                    </li>';}?>
                                </ul>
                            </li>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/transfer_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Transfer Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/transfer_management/transfer_details">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Transfer Details</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/transfer_management/transfer_requests">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Transfer Request</span>
                                        </a>
                                    </li>
                                    <?php if($userRoleID == 10 || $userRoleID == 11){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/transfer_management/view_branches">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">View Total Employees By Branches</span>
                                        </a>
                                    </li>';}?>
                                    <?php if($userRoleID == SUPERVISOR_ROLE_ID || $userRoleID == 10){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/transfer_management/transfer_employees">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Transfer Employees</span>
                                        </a>
                                    </li>';}
                                    ?>
                                    <?php if(in_array($userRoleID, array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID, COMPANY_ADMIN_ROLE_ID)) ||$userRoleID == SUPERVISOR_ROLE_ID){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/transfer_management/view_transfer_employees">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">View Transfer Employees</span>
                                        </a>
                                    </li>';}
                                    // print_r($this->arrRoleIDs);
                                    // exit;
                                    ?>
                                </ul>
                            </li>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/transfer_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Loan Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/loan_management/loan_details">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Loan Details</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/loan_management/loan_request">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Loan Request</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <?php }?>
                            <?php if($userRoleID == CCTV_OPERATOR || $userRoleID == HR_ADMIN_ROLE_ID){ ?>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/deduction_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">CCTV deduction Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                <?php if($userRoleID != HR_ADMIN_ROLE_ID){ ?>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/deduction_management/deduction_details">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">deduction Details</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/deduction_management/deduction_request">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add deduction</span>
                                        </a>
                                    </li>
                                    <hr>
                                <?php } ?>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/deduction_management/list_deduction">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">List Deduction CCTV</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/deduction_management/deduction_CCTV_request">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">deduction CCTV Request</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php } ?>
                            
                            <?php if($userRoleID == COMPLAINCE_OPERATOR || $userRoleID == COMPLAINCE_MANAGER){ ?>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/compliance_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Complaince Management</span>
                                </a>
                                <ul>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/compliance_management/list_compliance">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">List Deduction</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/compliance_management/request_compliance">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Compliance Deduction Request</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php } ?>
							<!-- <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/task_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Performance Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/task_management/my_tasks">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Self Appraisal</span>
                                        </a>
                                    </li>
                                    <?php if($userRoleID == 10 || $userRoleID == 11){
                                    echo '<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/task_management/my_team_tasks">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Team\'s Self Appraisal</span>
                                        </a>
                                    </li>';}?>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/task_management/my_kpis">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">KPIs</span>
                                        </a>
                                    </li>
                                    <?php if($userRoleID == 10 || $userRoleID == 11){
									echo' <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/task_management/my_team_kpis">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Team\'s KPIs</span>
                                        </a>
                                    </li>';}
                                    ?>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/task_management/probation_assessment">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Probationary Assessment</span>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->
                            <?php if($userRoleID == 10){
							echo '<li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/payroll_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Payroll Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/payroll_management/list_accounts">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">List Accounts</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/payroll_management/save_payroll">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Submit Monthly Salary</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/payroll_management/list_payroll">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Payroll Details</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/payroll_management/manage_payroll">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage Payroll</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>';}?>
                            <?php
                                if($userRoleID != CCTV_OPERATOR && $userRoleID != COMPLAINCE_MANAGER && $userRoleID != COMPLAINCE_OPERATOR){
                            ?>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/HRM/forum">
                                    <i class="nav-main-link-icon fa fa-commenting"></i>
                                    <span class="nav-main-link-name">Forum</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/forum/birthday_calendar">
                                            <i class="nav-main-link-icon fa fa-globe"></i>
                                            <span class="nav-main-link-name">Birthday Calender</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/HRM/forum/list_events">
                                            <i class="nav-main-link-icon fa fa-directions"></i>
                                            <span class="nav-main-link-name">Events</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php }?>
                            <!-- <li class="nav-main-heading"></li> -->
                            <!-- <li class="nav-main-item">
                                <a class="nav-main-link" href="">
                                    <i class="nav-main-link-icon fa fa-arrow-left"></i>
                                    <span class="nav-main-link-name">Go Back</span>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- END Sidebar Scrolling -->
            </nav>

	</div>
</div>
<?php }
} 
?>