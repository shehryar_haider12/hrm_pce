<?php
$css_path = $this->config->item("css_path");

$style_css_path = $this->config->item("style_css_path");
$script_path = $this->config->item("script_path");
?><head>
	<link href="<?php echo $css_path; ?>multi-select.css" media="screen" rel="stylesheet" type="text/css">
    <script src="<?php echo $script_path; ?>/jquery.multi-select.js" type="text/javascript"></script>
  
</head>


<div class="employeeDetailLeftCol">    
    <div class="employeeDetailLinksMain">
        <ul>
        	<?php if($showOptions == false) { ?>
           		<li id="manage_poll"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/manage_poll'; ?>">Poll Management</a></li>
            <?php } else if($showOptions == true) { ?>
            	<li id="manage_poll"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/manage_poll/' . $pollID; ?>">Poll Management</a></li>
                <li id="poll_questions"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/poll_questions/' . $pollID; ?>">Poll Questions</a></li>
                <?php if($showResultOptions == true) { ?>
                <li id="poll_result"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/poll_result/' . $pollID; ?>">Poll Result</a></li>
                <li id="poll_result_graph"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/poll_result_graph/' . $pollID; ?>">Poll Result Graphically</a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>
<script>$('#<?php echo $this->currentAction; ?>').addClass('selected');</script>
