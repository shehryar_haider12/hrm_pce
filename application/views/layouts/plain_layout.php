<?php
$css_path = $this->config->item("css_path");
$style_css_path = $this->config->item("style_css_path");
$script_path = $this->config->item("script_path");
?>
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>HAi</title>

	<!--	====================	ACCORDIAN	====================	-->
	<script type="text/javascript" src="<?php echo $script_path; ?>/accordian/prototype.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/accordian/effects.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/accordian/accordion.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/accordian/accordiancommon.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $css_path; ?>accordian.css" />

    <link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>jquery-ui.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $style_css_path; ?>">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>reset.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>tipsy.css">
    <script>var base_url = '<?php echo $this->baseURL; ?>';</script>
<!--
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo $script_path; ?>/jquery.tipsy.js"></script>
-->
	<script type="text/javascript" src="<?php echo $script_path; ?>/common.js"></script>
	<?php echo $font; ?>

	<!--	Right Click Not Allowed		-->
	<!--
	<SCRIPT language=JavaScript>
	var message = "This operation is not allowed"; 
	function rtclickcheck(keyp){ if (navigator.appName == "Netscape" && keyp.which == 3){ 	alert(message); return false; } 
	if (navigator.appVersion.indexOf("MSIE") != -1 && event.button == 2) { 	alert(message); 	return false; } } 
	document.onmousedown = rtclickcheck;
	</SCRIPT>
	-->
  </head>
  <body>
	<div class="mainBody">
  		<div class="mainDiv">
    		
			<!-- Header Start -->
			<div class="headerMain">
				<?php echo $header; ?>
			</div>
			<!-- Header End -->
       		
			<!-- Breadcrumbs Start -->
			<!--
			<div class="Breadcrumbs">
				< ?php echo $breadcrumbs; ?>
			</div>
			-->
			<!--	Breadcrumbs End -->
			
			<!--	Message Box Start [It is required to move in elements later] -->
			<?php 
			if(trim($this->session->flashdata('success_message')) != '')
				$success_message = trim($this->session->flashdata('success_message'));
			if ($success_message != '' || $error_message != '' || $validation_error_message != '') { ?>
				<div class="messageBox">
					<?php if($success_message != '') {?>
					<div class="successMessage">
						<?php echo $success_message; ?>
					</div>
					<?php } ?>

					<?php if($error_message != '') {?>
					<div class="errorMessage">
						<?php echo $error_message; ?>
					</div>
					<?php } ?>
					
					<?php if($validation_error_message != '') { ?>
					<div class="errorMessage">
						<?php echo $validation_error_message; ?>
					</div>
					<?php } ?>
				</div>
			<?php } ?>
			<!-- Message Box End  -->
			
			<!--	Mid Section Start  -->
			<div class="midMain">
    			<?php echo $content; ?>
			</div>
			<!-- Mid Section End -->
   		
			
			<!-- Footer Start -->
			<div class="footerMain">
				<?php echo $footer; ?>
			</div>
			<!-- Footer End -->
		</div>
	</div>
	
	<!--	====================	ACCORDIAN	====================	-->
	<script type="text/javascript" >
		//
		// You can hide the accordions on page load like this, it maintains accessibility
		//
		// Special thanks go out to Will Shaver @ http://primedigit.com/
		//
		var verticalAccordions = $$('.accordion_toggle');
		verticalAccordions.each(function(accordion) {
			$(accordion.next(0)).setStyle({
			  height: '0px'
			});
		});

	</script>

  </body>
</html>