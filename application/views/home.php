
<div class="midSection">
    <div class="dashboardTitle">
    </div>

	<!-- <div class="notificationMain">
    	<div class="notificationBox">
        	<h1>Notice Board</h1>
            <div class="notificationContent">
                <div class="boldBlueHeading" style="font-family:arial; line-height:22px">
                
                	<?php
					if($performanceMessage != '') {
					?>
                    <div class="errorMessage center blink">
                    <?php
						echo strtoupper($performanceMessage);
					?>
                    </div>
                    <?php
					}
					
                    for($ind = 0; $ind < count($arrAnnouncements); $ind++) {
					?>
						<a class="boldBlueHeading" href="<?php echo $this->baseURL;?>/policy_announcements"><?php echo $arrAnnouncements[$ind]['ann_title']; ?></a>
                        <?php if($arrAnnouncements[$ind]['ann_is_new']) { ?>
                        <span class="fileIcon"><img src="<?php echo $this->imagePath . '/new_notification.gif'; ?>"></span>
                        <?php } ?>
                        <div class="clear"></div>
                    <?php
					}
					?>
                </div>
            </div>
        </div>
    </div> -->
    
	<!-- <div class="dashboardBoxesMain">
		<?php
		$tipsyID = '';
		foreach($this->modulesAllowed as $modules => $moduleValue)
		{
			if(!in_array($moduleValue['module_name'], $skipModule))
			{
				$tipsyID = '';
				$complain = false;
				$performance = false;
				$task = false;
				
				if(strpos(strtolower($moduleValue['module_name']), 'complain') !== false) {
					$arrWhere = array(
						'emp_id' => $this->userEmpNum
					);
						
					$issueUnChecked = getIssuesUnChecked($arrWhere);
					if((int)$issueUnChecked){
						$tipsyID = ' tipsyID';
						$complain = true;
					}
				}
				
				if(strpos(strtolower($moduleValue['module_name']), 'task') !== false) 
				{
					$reviewUnChecked = getReviewsUnChecked($this->userEmpNum);
					
					if((int)$reviewUnChecked)
					{
						$tipsyID = ' tipsyID';
						$performance = true;
					}
				}
				
				if(strpos(strtolower($moduleValue['module_name']), 'attendance') !== false) 
				{
					$arrWhere = array(
						'e.emp_status' => 1,
						'l.leave_status' => 0,
						'es.supervisor_emp_id' => $this->userEmpNum
					);
											
					$leavesUnChecked = getLeavesUnchecked($arrWhere);
					
					if((int)$leavesUnChecked)
					{
						$tipsyID = ' tipsyID';
						$leaves = true;
					}
				}
		?>
			<div class="boxMain<?php echo $tipsyID; ?>" original-title="<?php if($complain && $task) { ?><?php echo $issueUnChecked; ?> issue(s) awaiting response, <?php echo $tasksUnChecked; ?> task(s) awaiting response<?php } else if($complain) { ?><?php echo $issueUnChecked; ?> issue(s) awaiting response<?php } else if($task) { ?><?php echo $tasksUnChecked; ?> task(s) awaiting response<?php } else if($performance) { ?><?php echo $reviewUnChecked; ?> review(s) not submitted<?php } else if($leaves) { ?><?php echo $leavesUnChecked; ?> leave request(s) awaiting response<?php } ?>">
				<!-- <div class="boxImageMain">
					<a href="<?php echo $baseURL . '/' . $moduleValue['module_name']; ?>">
						<img src="<?php echo $imagePath . '/modules/' . $moduleValue['module_name']; ?>.png" alt="">
					</a>
				</div>
				<div class="boxTitle">
					<a href="<?php echo $baseURL . '/' . $moduleValue['module_name']; ?>">
						<?php echo $moduleValue['display_name']; ?>
					</a>
				</div> 
			</div>
		<?php
		echo $arrEmployee;
			}
		}
		?>
	</div> -->
</div>
<script>	
$(".tipsyID").each(function() {
	$( this ).tipsy({gravity: "s", title: "original-title", trigger: "manual"});
	$( this ).tipsy("show");
});

function blink(selector){
$(selector).fadeOut('slow', function(){
    $(this).fadeIn('slow', function(){
        blink(this);
    });
});
}

blink('.blink');
</script>
<script src="assets/js/dashmix.core.min.js"></script>

        <!--
            Dashmix JS

            Custom functionality including Blocks/Layout API as well as other vital and optional helpers
            webpack is putting everything together at assets/_js/main/app.js
        -->
        <script src="assets/js/dashmix.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="assets/js/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

        <!-- Page JS Helpers (jQuery Sparkline plugin) -->
        <script>jQuery(function () {
                        Dashmix.helpers('sparkline');
                    });</script>