
<?php


$leaveCategory 		= (isset($_POST['leaveCategory'])) 						? $_POST['leaveCategory'] 					: '';
$leaveStatus 		= (isset($_POST['leaveStatus'])) 						? $_POST['leaveStatus'] 					: '';
$leaveMonth 		= (isset($_POST['leaveMonth'])) 						? $_POST['leaveMonth'] 						: '';
$leaveYear 			= (isset($_POST['leaveYear'])) 							? $_POST['leaveYear'] 						: '';
$empSickLeaves			=	(isset($_POST['empSickLeaves']))		?	$_POST['empSickLeaves']			:	$record['emp_sick_leaves'];
if($leaveStatus == 0 && $leaveStatus != '') {
	$leaveStatus = -1;
}
?>
<form name="frmTranferStatus" id="frmTranferStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
	<div class="searchCol">
        <div class="labelContainer">Region:</div>
        <div class="textBoxContainer">
      	<select name="region_id" id="branch" class="dropDown" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($regions)) {
				  for($ind = 0; $ind < count($regions); $ind++) {
            if($regions[$ind]['region_id'] != $employee_branch[0]['region_id'])
            {
			  ?>
				  <option value="<?php echo $regions[$ind]['region_id']; ?>"
          <?php
            if(isset($empTransferRequest[0]['region_id']))
            {
              echo $empTransferRequest[0]['region_id'] == $regions[$ind]['region_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $regions[$ind]['region_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Country:</div>
        <div class="textBoxContainer">
      	<select name="location_id" id="branch_to" class="dropDown" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($countries)) {
				  for($ind = 0; $ind < count($countries); $ind++) {
           
			  ?>
				  <option value="<?php echo $countries[$ind]['location_id']; ?>"
         
          ><?php echo $countries[$ind]['location_name']; ?></option>
			  <?php
            }
				}
			  ?>
        </select>
        </div>
      </div>
	  <!-- <div class="searchCol">
        <div class="labelContainer">Transfer Status:</div>
        <div class="textBoxContainer">
      	<select name="transferStatus" id="transferStatus" class="dropDown" style="width:175px">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>             -->
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>
    </div>
  </div>
  <script>
  	// $('#leaveCategory').val('<?php echo $leaveCategory; ?>');
  	// $('#leaveStatus').val('<?php echo $leaveStatus; ?>');
  	// $('#leaveMonth').val('<?php echo $leaveMonth; ?>');
  	// $('#leaveYear').val('<?php echo $leaveYear; ?>');
  </script>
</form>

    <?php
	// print_r($arrRecords);
	// exit;
	?>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php 
		// echo "Total Records Count: " . count($arrRecords);
		 ?>
		
   </div>	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>
		
	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
<tr class="listHeader">
    <td class="listHeaderCol">ID</td>
    <td class="listHeaderCol">Branch Name</td>
    <td class="listHeaderCol" style="max-width:140px;min-width:140px">Branch Address</td>
    <td class="listHeaderCol" style="max-width:150px;min-width:150px">Branch Country</td>
    <td class="listHeaderCol" style="max-width:150px;min-width:150px">Branch Region</td>
    <td class="listHeaderCol" style="max-width:150px;min-width:150px">Total Employees</td>
</tr>
  <?php
//   print_r($arrSupervisors);
//   exit;
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol"><?php echo $arrRecords[$ind]['company_id']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['company_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['company_address']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['location_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['region_name']; ?></td>
    <td class="listContentCol"><?php 
		$total_employees = $this->model_transfer_management->getBrancheEmployee($arrRecords[$ind]['company_id']);
		echo $total_employees; 
	
	?></td>
  </tr>
  <?php
	
}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="12" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="265px">Edit <span style="font-style:italic;color:#666">(Can only edit pending requests)</span></td>
        </tr>
    </table>
</div>