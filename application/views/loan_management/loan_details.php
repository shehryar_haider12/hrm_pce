<script>
// $(document).ready(function () {
// 	$('.select-state').selectize({
// 		sortField: 'text'
// 	});
// });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;

	}
	.item{
		width: 100%;
	} 
/* .selectize-input.items.has-options.full.has-items{
	height: 35px !important;
}
.item{
	width: 100%;
} */
</style>
<?php


$leaveCategory 	= (isset($_POST['leaveCategory'])) 		? $_POST['leaveCategory'] 	: '';
$leaveStatus 	= (isset($_POST['leaveStatus'])) 		? $_POST['leaveStatus'] 	: '';
$leaveMonth 	= (isset($_POST['leaveMonth'])) 		? $_POST['leaveMonth'] 		: '';
$leaveYear 		= (isset($_POST['leaveYear'])) 			? $_POST['leaveYear'] 		: '';
$empSickLeaves	=	(isset($_POST['empSickLeaves']))	?	$_POST['empSickLeaves']	: $record['emp_sick_leaves'];
if($leaveStatus == 0 && $leaveStatus != '') {
	$leaveStatus = -1;
}
?>
<form name="frmLoanStatus" id="frmLoanStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Leaves Of:</div>
        <div class="textBoxContainer">
        	<select id="select-state" name="loanMonth" class="dropDown select-state" style="width:85px">
            	<option value="">Month</option>
            	<option value="01">Jan</option>
            	<option value="02">Feb</option>
            	<option value="03">Mar</option>
            	<option value="04">Apr</option>
            	<option value="05">May</option>
            	<option value="06">Jun</option>
            	<option value="07">Jul</option>
            	<option value="08">Aug</option>
            	<option value="09">Sep</option>
            	<option value="10">Oct</option>
            	<option value="11">Nov</option>
            	<option value="12">Dec</option>
          	</select>&nbsp;&nbsp;&nbsp;&nbsp;
        	<select id="select-state" name="loanYear" class="dropDown select-state" style="width:85px; margin-left:5px">
            	<option value="">Year</option>
            	<?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
            	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
				<?php } ?>
          	</select>
        </div>
      </div>
	  <!-- <div class="searchCol">
        <div class="labelContainer">Branch From:</div>
        <div class="textBoxContainer">
      	<select name="branch_from" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($emploanRequest[0]['company_to_id']))
            {
              echo $emploanRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Branch To:</div>
        <div class="textBoxContainer">
      	<select name="branch_to" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($emploanRequest[0]['company_to_id']))
            {
              echo $emploanRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div> -->
	  <div class="searchCol">
        <div class="labelContainer">Select Employee:</div>
        <div class="textBoxContainer">
      	<select name="emp_id" class="dropDown select-state" id="select-state"  style="width:175px">
            <option value="">All</option>
            <?php
				if (count($all_employees)) {
					for($ind = 0; $ind < count($all_employees); $ind++) {?>
					<option value="<?php echo $all_employees[$ind]['emp_id']; ?>" 
					<?php
						if(isset($emploanRequest[0]['emp_id']))
						{
							echo $emploanRequest[0]['emp_id'] == $all_employees[$ind]['emp_id'] ? 'selected' : null;
						}
					?>
					><?php echo $all_employees[$ind]['emp_code'].' --- '.$all_employees[$ind]['emp_full_name']; ?></option>
					<?php
					}
			  	}
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">loan Status:</div>
        <div class="textBoxContainer">
      	<select name="loanStatus" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>            
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>
    </div>
  </div>
  <script>
  	// $('#leaveCategory').val('<?php echo $leaveCategory; ?>');
  	// $('#leaveStatus').val('<?php echo $leaveStatus; ?>');
  	// $('#leaveMonth').val('<?php echo $leaveMonth; ?>');
  	// $('#leaveYear').val('<?php echo $leaveYear; ?>');
  </script>
</form>

<div class="centerButtonContainer">
	<input class="addButton" type="button" value="Request Loan" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/loan_request' ?>';" />
</div>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php 
		// echo "Total Records Count: " . count($arrRecords);
		 ?>
		
		<b>Legends:</b>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F9F084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#D9FFA0;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Approved</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Rejected</span>
    </div>	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>
		
<div class="listContentMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		<tr class="listHeader">
			<td class="listHeaderCol">ID</td>
			<td class="listHeaderCol">Full Name</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Loan Type</td>
			<td class="listHeaderCol" >Loan Amount</td>
			<td class="listHeaderCol" style="max-width:150px;min-width:150px">Reason</td>
			<td class="listHeaderCol" >Attachement</td>
			<td class="listHeaderCol" style="max-width:150px;min-width:150px">loan_comments</td>
			<td class="listHeaderCol" style="max-width:150px;min-width:150px">Processed by</td>
			<td class="listHeaderCol" style="max-width:150px;min-width:150px">Processed Date</td>
			<td class="listHeaderCol" >status</td>
			<td class="listHeaderColLast" align="center">Action/Status</td>
		</tr>
		<?php
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				// print_r($arrRecords[$ind]['emp_id']);exit;
				if(in_array($this->userRoleID, $this->arrRoleIDs) || $this->userRoleID == $super_visor_roleid  || $arrRecords[$ind]['emp_id'] == $this->userEmpNum){

				// if(($employee_region_id['region_id'] == $arrRecords[$ind]['region_id'] && in_array($this->userRoleID,array(REGIONAL_MANAGER,SUPERVISOR_ROLE_ID))) || in_array($this->userRoleID,array(HR_ADMIN_ROLE_ID,SUPER_ADMIN_ROLE_ID,WEB_ADMIN_ROLE_ID,HR_MANAGER_ROLE_ID,WEB_ADMIN_EMP_ID,WEB_ADMINISTRATOR)))
				// {
					if($arrRecords[$ind]['loan_status'] == 0) {
						$cssBtn = 'smallButtonUpdate';
						$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
						$strStatus = 'Pending';
					} elseif($arrRecords[$ind]['loan_status'] == 1) {
						$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
						$strStatus = 'Approved';
					} elseif($arrRecords[$ind]['loan_status'] == 2) {
						$trCSSClass = 'listContentAlternate" style="background-color:#F38374"';
						$strStatus = 'Rejected';
					}  else {
						$trCSSClass = 'listContentAlternate';
						$strStatus = '';
					}

					if($arrRecords[$ind]['loan_type'] == 1){
						$loantype = "Personal Loan";
					}else if($arrRecords[$ind]['loan_type'] == 2){
						$loantype = "Educational Loan";
					}else if($arrRecords[$ind]['loan_type'] == 3){
						$loantype = "Home Loan";
					}else if($arrRecords[$ind]['loan_type'] == 4){
						$loantype = "Emergency Loan";
					}

					?>
					<tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
						<td class="listContentCol"><?php echo $arrRecords[$ind]['loan_id']; ?></td>
						<td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
						<td class="listContentCol"><?php echo $loantype ?></td>
						<td class="listContentCol"><?php echo $arrRecords[$ind]['loan_amount']; ?></td>
						<td class="listContentCol paddingTopBottom"><?php echo $arrRecords[$ind]['loan_Reason']; ?></td>
						<td class="listContentCol paddingTopBottom"><?php echo $arrRecords[$ind]['loan_attachment']; ?></td>
						<td class="listContentCol paddingTopBottom"><?php echo $arrRecords[$ind]['loan_comments']; ?></td>
						<td class="listContentCol"><?php echo $arrRecords[$ind]['procesed_by']; ?></td>
						<td class="listContentCol"><?php echo $arrRecords[$ind]['procesed_date']; ?></td>
						<td class="listContentCol"><?php echo $strStatus; ?></td>
						<td class="listContentColLast" align="center">
						
								<?php
									$arrSupervisors 	= $this->model_loan_management->getSupervisors($arrRecords[$ind]['emp_id']);
									$supers 	= $arrSupervisors;
									$super_key 	= 'supervisor_emp_id';
							
									$super_id = array_map(function($supers) use ($super_key) {
										return is_object($supers) ? $supers->$super_key : $supers[$super_key];
									}, $supers);

									if(($arrRecords[$ind]['loan_status'] == 0 && in_array($this->userRoleID, $this->arrRoleIDs)) || (in_array($this->userEmpNum,$super_id) && $this->userEmpNum != $arrRecords[$ind]['emp_id'])) {	 
								?>

								&nbsp;&nbsp;
								<img title="Approve" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/leave_approve.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/loan_request/' . $arrRecords[$ind]['loan_id']; ?>';">
								&nbsp;&nbsp;
								<img title="Delete" style="margin:-7px 0;cursor:pointer" width="20" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/loan_request/' . $arrRecords[$ind]['loan_id'].'/1'; ?>';">
								<?php } elseif($arrRecords[$ind]['loan_status'] == 0 && $arrRecords[$ind]['emp_id'] == $this->userEmpNum) {
								?>
								<br />
								&nbsp;&nbsp;
								<img title="Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/loan_request/' . $arrRecords[$ind]['loan_id'].'/2' ?>';">
								<?php
									}
							?>
						</td>
					</tr>
				<?php
				} 
			// } 
		}
		if(!$ind) {
		?>
		<tr class="listContentAlternate">
			<td colspan="11" align="center" class="listContentCol">No Record Found</td>
		</tr>
		<?php
		}
		?>
	</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="265px">Edit <span style="font-style:italic;color:#666">(Can only edit pending requests)</span></td>
        </tr>
    </table>
</div>
