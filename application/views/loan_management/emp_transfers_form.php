<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    @import url(http://fonts.googleapis.com/css?family=Lato:400,700);
    nav#sidebar{
        display: none;
    }
body
{
    font-family: 'Lato', 'sans-serif';
    }
.profile 
{
    min-height: 355px;
    display: inline-block;
}
.well.profile {
    width: 1000px;
}
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }
.dropdown-menu 
{
    background-color: #34495e;    
    box-shadow: none;
    -webkit-box-shadow: none;
    width: 250px;
    margin-left: -125px;
    left: 50%;
    }
.dropdown-menu .divider 
{
    background:none;    
    }
.dropdown-menu>li>a
{
    color:#f5f5f5;
    }
.dropup .dropdown-menu 
{
    margin-bottom:10px;
    }
.dropup .dropdown-menu:before 
{
    content: "";
    border-top: 10px solid #34495e;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    bottom: -10px;
    left: 50%;
    margin-left: -10px;
    z-index: 10;
    }
    .emp_info{
        width: 45% !important;
        font-size: 17px;
        padding: 14px;
    }
    .emp_info_value{
        width: 45% !important;
        font-size: 17px;
        font-weight: bold;
        padding: 14px;
        border-bottom: 1px solid #cccaca;
    }
</style>
<?php
    // print_r();
    // exit;
?>
<div class="col-md-12">
    <!-- <td width="650px">
        <table border="1" width="100%" style="line-height:22px">
        <tr>
            <td width="70%">
                <img src="<?php echo $transfer_details['LOGO_URL']?>" width="151" height="35" />
            </td>
            <td width="30%" align="right">
                Report Generated At<br />
                <?php echo $transfer_details['CREATED_DATE_TIME']?>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
            </td>
        </tr>
    </table>
    </td> -->
<div class="well profile">
    <div class="col-sm-12">
    <tr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <tr>
                    <td width="70%">
                        <img src="<?php echo $transfer_details['LOGO_URL']?>" width="151" height="150px" />
                    </td>
                    <td width="30%" align="right">
                    	Report Generated At<br />
                        <?php echo $transfer_details['CREATED_DATE_TIME']?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
        <div class="col-xs-12 col-sm-8">
            <h2><?php echo $transfer_details['EMPLOYEE_NAME']?></h2>
            <p><strong>Employee Number : </strong> <?php echo $transfer_details['EMPLOYEE_CODE']?> </p>
            <p><strong>D.O.J : </strong> <?php echo $transfer_details['JOINING_DATE']?> </p>
            <p><strong>Designation: </strong>  <?php echo $transfer_details['EMPLOYEE_DESIGNATION']?></p>
            
        </div>             
        
                
    

<table style="line-height:22px;width: 100% !important;">

    <tr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <tr bgcolor="#0070C0">
                    <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">EMPLOYEE DETAILS</td>
                </tr>                
                <!-- <tr>
                	<td width="10%" valign="top">Employee Name</td>
                    <td valign="top"><?php echo $transfer_details['EMPLOYEE_NAME']?></td>
                    <td width="10%" valign="top">Employee Number</td>
                    <td valign="top"><?php echo $transfer_details['EMPLOYEE_CODE']?></td>
                    <td width="10%" valign="top">D.O.J</td>
                    <td valign="top"><?php echo $transfer_details['JOINING_DATE']?></td>
                </tr>
                <tr>
                	<td valign="top">Designation</td>
                    <td valign="top"><?php echo $transfer_details['EMPLOYEE_DESIGNATION']?></td>
                </tr> -->
                <tr>
                	<td valign="top" class="emp_info">Present Work in Branch/Department Name</td>
                    <td valign="top" class="emp_info_value"><?php echo $transfer_details['CURRENT_BRANCH']?></td>
                </tr>
                <tr>
                	<td valign="top" class="emp_info">Transfer to new Branch/Department Name</td>
                    <td valign="top" class="emp_info_value"><?php echo $transfer_details['TRNSFER_BRANCH']?></td>
                </tr>
                <tr>
                	<td valign="top" class="emp_info">First time posting Branch/Department Name</td>
                    <td valign="top" class="emp_info_value"><?php echo $transfer_details['FIRST_BRANCH']?></td>
                </tr>
                <tr>
                	<td valign="top" class="emp_info">Date of transfer in the Branch/Department Name</td>
                    <td valign="top" class="emp_info_value"><?php echo $transfer_details['FIRST_BRANCH_DATE']?></td>
                </tr>
                <tr>
                	<td valign="top" class="emp_info">Processed transfer in first time</td>
                    <td valign="top" class="emp_info_value"><?php echo $transfer_details['FIRST_BRANCH_PROCESSED']?></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <tr bgcolor="#0070C0" style="    border-bottom: 1px solid white;">
            <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">TRANSFER SUMMARY</td>
        </tr> 
        <hr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <thead>
                    <tr bgcolor="#0070C0" >
                        <th style="padding: 11px;color: white;font-weight: bold;">Posting</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">From Branch/Dept Name</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">To Branch/Dept Name</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">Date of Posting</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">Processed By</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">Period</th>
                    </tr>   

                </thead>
                <tbody>
                    <?php 
                    $i =1;
                    // print_r($transfer_details['POSTINGS']);
                    // exit;
                    foreach ($transfer_details['POSTINGS'] as $key => $posting) {
                    ?>
                    <tr>
                        <td style="padding: 11px;"><?php echo $i?></td>
                        <td style="padding: 11px;"><?php echo $posting['company_from']?></td>
                        <td style="padding: 11px;"><?php echo  $posting['company_to']?></td>
                        <td style="padding: 11px;"><?php echo  $posting['processed_date']?></td>
                        <td style="padding: 11px;"><?php echo $posting['processed_by'] > 0 ? $posting['processed_name'] : 'Manual'; ?></td>

                        <td style="padding: 11px;"><?php 
                            $now = time(); // or your date as well
                            $your_date = strtotime($posting['processed_date']);
                            $datediff = $now - $your_date;
                            
                            echo round($datediff / (60 * 60 * 24)).' Days';
                        ?></td>
                    </tr>
                    <?php
                    $i++;
                    }
                        ?>
                </tbody>

            </table>
        </td>
    </tr>
  
</table>
</div>                 
</div></div>