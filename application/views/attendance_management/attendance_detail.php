<?php
$empID 		= isset($_POST['empID']) 		? $_POST['empID'] 		: $empID;
$selMonth 	= isset($_POST['selMonth']) 	? $_POST['selMonth'] 	: $selMonth;
$selYear 	= isset($_POST['selYear']) 		? $_POST['selYear'] 	: $selYear;
$employee_id = array();
$attendance_date = array();
$attendance_in = array();
$attendance_out = array();
if ($fetch_timing->num_rows() > 0) {
	foreach ($fetch_timing->result() as $row) {
		array_push($employee_id, $row->employee_id);
		array_push($att_date, $row->att_date);
		array_push($att_in, $row->att_in);
		array_push($att_out, $row->att_out);
	}
} else {
	echo "no data found";
}	
$data1 = [];
foreach ($fetch_timing->result() as $key => $row) {
	$data1[$key]['employee_id'] = $row->employee_id;
	$data1[$key]['attendance_date'] = $row->att_date;
	$data1[$key]['attendance_in'] = $row->att_in;
	$data1[$key]['attendance_out'] = $row->att_out;
}

$emp_key = array_search($empID, $employee_id);
$worktime_in;
$worktime_out;
?>
<script>
$(document).ready(function () {
      $('.select-state').selectize({
          sortField: 'text'
      });
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;

	}
	.item{
		width: 100%;
	}
</style>
<script>
	$(function() {
		$(".datePicker").datepicker({
			changeMonth: true,
			changeYear: true
		});
		$(".datePicker").datepicker("option", "dateFormat", "<?php echo $dateFormat; ?>");
		$(".datePicker").datepicker("option", "minDate", '<?php echo date('Y-m-d', strtotime('6 months ago')); ?>');
		$("#dateFrom").datepicker("setDate", "<?php echo $dateFrom; ?>");
		$("#dateTo").datepicker("setDate", "<?php echo $dateTo; ?>");
	});

	function saveNotes(txtArea, empID, strDate) {
		var strNotes = $('#' + txtArea).val();
		$.ajax({
			url: base_url + "/ajax/saveAttNotes/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_notes=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function(msg) {
				alert('Note Saved!');
			},
			error: function(msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}
</script>
<?php
	if(count($arrAllDates) > 0)
	{
?>
<form name="frmAttDetail" id="frmAttDetail" method="post" action="<?php echo $frmActionURL; ?>">
	<div class="searchBoxMain">
		<div class="searchHeader">Search Criteria</div>
		<div class="searchcontentmain">
			<?php
			if (count($arrEmployees) > 0) {
			?>
				<div class="searchCol">
					<div class="labelContainer">Employee:</div>
					<div class="textBoxContainer">
						<select name="empID" id="empID" class="dropDown select-state">
							<option value="">Select Employee</option>
							<?php
							if (count($arrEmployees)) {
								foreach ($arrEmployees as $key => $arrEmp) {
							?>
									<optgroup label="<?php echo $key; ?>">
										<?php for ($i = 0; $i < count($arrEmp); $i++) { ?>
											<option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
										<?php } ?>
									</optgroup>
							<?php	}
							}
							?>
						</select>
					</div>
				</div>
			<?php
			}
			?>
			<div class="searchCol">
				<div class="labelContainer">Month:</div>
				<div class="textBoxContainer">
					<select id="selMonth" name="selMonth" class="dropDown select-state">
						<option value="">Month</option>
						<option value="01">Jan</option>
						<option value="02">Feb</option>
						<option value="03">Mar</option>
						<option value="04">Apr</option>
						<option value="05">May</option>
						<option value="06">Jun</option>
						<option value="07">Jul</option>
						<option value="08">Aug</option>
						<option value="09">Sep</option>
						<option value="10">Oct</option>
						<option value="11">Nov</option>
						<option value="12">Dec</option>
					</select>
				</div>
			</div>
			<div class="searchCol">
				<div class="labelContainer">Year:</div>
				<div class="textBoxContainer">
					<select id="selYear" name="selYear" class="dropDown select-state">
						<option value="">Year</option>
						<?php for ($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
							<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="buttonContainer">
				<input type="hidden" name="txtExport" id="txtExport" value="0" />
				<input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val('0')">
				<input class="searchButton" name="btnExport" id="btnExport" type="submit" value="Export PDF" onclick="$('#txtExport').val('1')">
				
			</div>
		</div>
	</div>
	<script>
		$('#empID').val('<?php echo $empID; ?>');
		$('#selMonth').val('<?php echo $selMonth; ?>');
		$('#selYear').val('<?php echo $selYear; ?>');		
	</script>
</form>
<form method="post" id="import_csv" enctype="multipart/form-data" action="">
		<input type="file" id="myFile" name="csv_file" id="csv_file" accept=".csv" />
		<button type="submit" name="import_csv" class="searchButton" id="import_csv_button">Import CSV</button>
	</form>
	<div id="imported_csv_data"></div>
<script>


$('#import_csv').on('submit', function(event){
	event.preventDefault();
	// console.log('asdsfasdfdf');
	$.ajax({
		url:"<?php echo base_url(); ?>attendance_management/importcsv",
		method:"POST",
		data:new FormData(this),
		contentType:false,
		cache:false,
		processData:false,
		beforeSend:function(){
			$('#import_csv_button').html('Importing...');
		},
		success:function(res)
		{
			console.log(res);
			$('#import_csv')[0].reset();
			$('#import_csv_button').attr('disabled', false);
			$('#import_csv_button').html('Import Done');
			// load_data();
			// location.reload();
			// window.location.href = "http://google.com";
		}
	})
});

</script>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<b>Legends:</b>&nbsp;&nbsp;&nbsp;<span style="background-color:#8FB05C;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Off Day</span>
	</div>
	<?php
	if ($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php }	?>
</div>
<div class="listContentMain">
	
	<table cellspacing="0" cellpadding="0" class="listTableMain">
		<thead>
			<tr class="listHeader">
				<td class="listHeaderCol">Date</td>
				<!-- cast(checktime) -->
				<td class="listHeaderCol">Day</td>
				<!-- cast(checktime) -->
				<td class="listHeaderCol">In</td>
				<!-- min(checktime) -->
				<td class="listHeaderCol">Out</td>
				<!-- max(checktime) -->
				<td class="listHeaderCol">Working Hrs</td>
				<td class="listHeaderCol">Leave Applied</td>
				<td class="listHeaderCol">Notes</td>
				
			</tr>
		</thead>
		<tbody>
		<?php
		$Total_hours = [];
		
		for ($ind = 0; $ind < count($arrAllDates); $ind++) {
			$showClocking = true;
			$dateIndex = array_search($arrAllDates[$ind], array_column($arrRecords, 'DATE'));
			if ($dateIndex !== false) {
				$jnd = $dateIndex;
			} else {
				$jnd = -1;
				$showClocking = false;
			}
			$rowBGColor = '';
			$publicHoliday = '';

			$TW_date = readableDate($arrAllDates[$ind], 'd/m/Y');
			$TW_month = readableDate($arrAllDates[$ind], 'm');
			$TW_year = readableDate($arrAllDates[$ind], 'Y');
			$myTime = strtotime($TW_date); 
			$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $TW_month, $TW_year);
			$workDays = 0;
			while($daysInMonth > 0)
			{
				$day = date("D", $myTime);
				if($day != "Sun")
					$workDays++;

				$daysInMonth--;
				$myTime += 32400; 
			}
			$workHours = $workDays * 9; 

			$holidayIndex = array_search($arrAllDates[$ind], array_column($arrHolidays, 'holiday_date'));
			// if (date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_1'] || date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_2'] || $holidayIndex !== false) {
			if (date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_2'] || $holidayIndex !== false) {
			
				$rowBGColor = ' bgcolor="#8FB05C" style="color:#FFF"';
				$showClocking = false;
				if ($holidayIndex !== false) {
					$publicHoliday = $arrHolidays[$holidayIndex]['holiday_name'];
					$showClocking = false;
				}
			}

			if ($arrAllDates[$ind] == date('Y-m-d')) {
				$arrRecords[$jnd]['OUT'] = '';
			}
			?>
			<tr <?php echo $rowBGColor; ?> height="30px">
				<td class="listContentCol"><?php echo readableDate($arrAllDates[$ind], 'jS M, Y'); ?></td>
				<td class="listContentCol"><?php echo date('l', strtotime($arrAllDates[$ind])); ?></td>
				<td class="listContentCol">
					<?php 

						$dateupdate = readableDate($arrAllDates[$ind], 'Y-m-d');
						// print_r($data1);exit;
						foreach ($data1 as $key => $value) {
					
							if ($empID == $value['employee_id'] && $dateupdate == $value['attendance_date']){
								echo $value['attendance_in'];
								break;
							}
						} 
					?>
				</td>
				<td class="listContentCol">
					<?php 
					foreach ($data1 as $key => $value) {

						if ($empID == $value['employee_id'] && $dateupdate == $value['attendance_date']){
							echo $value['attendance_out'];
							break;
						}
					} 
					?>
				</td>
				<td class="listContentCol">
					<?php
					if ($publicHoliday != '') {
						echo $publicHoliday;
					 } else {
						foreach ($data1 as $key => $value) { 
							if ($empID == $value['employee_id']  && $dateupdate == $value['attendance_date']){
								$worktime_in = $value['attendance_in'];
								$worktime_out = $value['attendance_out'];
								echo number_format(((((strtotime($worktime_out) - strtotime($worktime_in)) / 60) /60) ), 2);
								$Total_hours[] = ((((strtotime($worktime_out) - strtotime($worktime_in)) / 60) /60) );
								break;
							}
						}
						
					}
					if ($arrRecords[$jnd]['IN'] != '' && $arrRecords[$jnd]['OUT'] != '') {
						$strTime1 = new DateTime($arrRecords[$jnd]['IN']);
						$strTime2 = new DateTime($arrRecords[$jnd]['OUT']);
						$intInterval = $strTime1->diff($strTime2);
						if ($intInterval->format('%h') > 0 || $intInterval->format('%i') > 0) {
							echo $intInterval->format('%h') . " hr " . $intInterval->format('%i') . " min";
						}
					}
					?>
				</td>
				<td class="listContentCol" width="100px">
					<?php
					$strLeave = getIfLeaveApplied($empID, $arrAllDates[$ind]);
					echo ($strLeave != '') ? $strLeave : '-';
					?>
				</td>
				<td class="listContentCol" width="300px">
					<?php
					$strNotes = getAttendanceNotes($empID, $arrAllDates[$ind]);

					if ($canWrite == YES) {
					?>
						<input type="text" name="txtNote<?php echo $arrAllDates[$ind]; ?>" id="txtNote<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $strNotes; ?>">&nbsp;
						<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveNotes('txtNote<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>')">
					<?php
					} else {
						echo ($strNotes != '') ? $strNotes : '-';
					}
					?>
				</td>
				<?php if (false) { ?>
					<td class="listContentCol" align="center"><?php if ($showClocking) { ?><img src="<?php echo $this->imagePath; ?>/clocking-icon.png" alt="Clocking" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/clocking_detail/' . $empID . '/' . $arrAllDates[$ind]; ?>', 650, 500)" style="cursor:pointer" /><?php } ?></td>
				<?php }
				
				?>
				</tr>
			<?php
		}
			?>
			<?php
			if (!$ind) {
			?>
				<tr class="listContentAlternate">
					<td colspan="7" align="center" class="listContentCol">No Record Found</td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listHeader">
		<td class="listHeaderCol">Total Working Hours</td>
		<td class="listHeaderCol">Total Hours Should Be</td>
	</tr>
	<tr>
	<td class="listContentCol"><?php echo number_format(array_sum($Total_hours), 2); ?></td>
		<td class="listContentCol"><?php echo $workHours; ?></td>
	</tr>
		
</table>
<?php } ?>
