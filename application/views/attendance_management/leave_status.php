<?php


$leaveCategory 		= (isset($_POST['leaveCategory'])) 						? $_POST['leaveCategory'] 					: '';
$leaveStatus 		= (isset($_POST['leaveStatus'])) 						? $_POST['leaveStatus'] 					: '';
$leaveMonth 		= (isset($_POST['leaveMonth'])) 						? $_POST['leaveMonth'] 						: '';
$leaveYear 			= (isset($_POST['leaveYear'])) 							? $_POST['leaveYear'] 						: '';
$empSickLeaves			=	(isset($_POST['empSickLeaves']))		?	$_POST['empSickLeaves']			:	$record['emp_sick_leaves'];
if($leaveStatus == 0 && $leaveStatus != '') {
	$leaveStatus = -1;
}
?>
<form name="frmLeaveStatus" id="frmLeaveStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Leaves Of:</div>
        <div class="textBoxContainer">
        	<select id="leaveMonth" name="leaveMonth" class="dropDown" style="width:85px">
            	<option value="">Month</option>
            	<option value="01">Jan</option>
            	<option value="02">Feb</option>
            	<option value="03">Mar</option>
            	<option value="04">Apr</option>
            	<option value="05">May</option>
            	<option value="06">Jun</option>
            	<option value="07">Jul</option>
            	<option value="08">Aug</option>
            	<option value="09">Sep</option>
            	<option value="10">Oct</option>
            	<option value="11">Nov</option>
            	<option value="12">Dec</option>
          	</select>&nbsp;&nbsp;&nbsp;&nbsp;
        	<select id="leaveYear" name="leaveYear" class="dropDown" style="width:85px; margin-left:5px">
            	<option value="">Year</option>
            	<?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
            	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
				<?php } ?>
          	</select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Leave Category:</div>
        <div class="textBoxContainer">
      	<select name="leaveCategory" id="leaveCategory" class="dropDown" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($arrCategories)) {
				  for($ind = 0; $ind < count($arrCategories); $ind++) {
			  ?>
				  <option value="<?php echo $arrCategories[$ind]['leave_category_id']; ?>"><?php echo $arrCategories[$ind]['leave_category']; ?></option>
			  <?php
				  }
			  }
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Leave Status:</div>
        <div class="textBoxContainer">
      	<select name="leaveStatus" id="leaveStatus" class="dropDown" style="width:175px">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>            
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>
    </div>
  </div>
  <script>
  	$('#leaveCategory').val('<?php echo $leaveCategory; ?>');
  	$('#leaveStatus').val('<?php echo $leaveStatus; ?>');
  	$('#leaveMonth').val('<?php echo $leaveMonth; ?>');
  	$('#leaveYear').val('<?php echo $leaveYear; ?>');
  </script>
</form>

<div class="centerButtonContainer">
	<input class="addButton" type="button" value="Request Leave" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/leave_requisition' ?>';" />
</div>
    <?php
	// print_r($arrRecords);
	// exit;
	?>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<!-- Leave visibile for indvidual -->
		<?php echo round($arrRecords[$ind]['emp_annual_leaves']); ?>
		<?php echo round($empAnnualLeaves); ?>
		<?php echo round($empSickLeaves); ?>
		
		<b>Legends:</b>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F9F084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#D9FFA0;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Approved</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Rejected</span>
    </div>	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Type</td>
    <td class="listHeaderCol">Category</td>
    <td class="listHeaderCol">Applied Date</td>
    <td class="listHeaderCol">From - To</td>
    <td class="listHeaderCol">Return Date</td>
    <td class="listHeaderCol">Total Days</td>
    <td class="listHeaderCol" style="max-width:200px;min-width:200px">Reason</td>
    <td class="listHeaderCol">Document</td>
    <td class="listHeaderCol">Processed By</td>
    <td class="listHeaderCol" style="max-width:200px;min-width:200px">Comments</td>
    <td class="listHeaderCol">Leave Form</td>
    <td class="listHeaderColLast" align="center">Action/Status</td>
  </tr>
  <?php
//   print_r(count($arrRecords));
//   exit;
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		
		if($arrRecords[$ind]['leave_status'] == 0) {
			$cssBtn = 'smallButtonUpdate';
			$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
			$strStatus = 'Pending';
		} elseif($arrRecords[$ind]['leave_status'] == 1) {
			$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
			$strStatus = 'Approved';
		} elseif($arrRecords[$ind]['leave_status'] == 2) {
			$trCSSClass = 'listContentAlternate" style="background-color:#F38374"';
			$strStatus = 'Rejected';
		}  else {
			$trCSSClass = 'listContentAlternate';
			$strStatus = '';
		}
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol"><?php echo $arrTypes[$arrRecords[$ind]['leave_type']]; ?></td>
    <td class="listContentCol"><?php echo getValue($arrCategories, 'leave_category_id', $arrRecords[$ind]['leave_category'], 'leave_category'); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['created_date'], $showDateFormat); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['leave_from'], $showDateFormat) . ' -<br />' . readableDate($arrRecords[$ind]['leave_to'], $showDateFormat); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['leave_return'], $showDateFormat); ?></td>
    <td class="listContentCol"><?php echo (int)$arrRecords[$ind]['leave_days']; ?></td>
    <td class="listContentCol paddingTopBottom"><?php echo $arrRecords[$ind]['leave_reason']; ?></td>	
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['leave_doc'] != '') ? '<a href="' . $this->baseURL . '/' . $docFolderShow . $arrRecords[$ind]['leave_doc'] . '" style="color:#f00" target="_blank">View Document</a>' : ' - '; ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['processed_date'] != '') ? getValue($arrSupervisors, 'emp_id', $arrRecords[$ind]['processed_by'], array('emp_full_name')) . '<br />' . readableDate($arrRecords[$ind]['processed_date'], $showDateFormat) : ' - '; ?></td>
    <td class="listContentCol"><?php echo nl2br($arrRecords[$ind]['leave_comments']); ?></td>
      <td class="listContentCol" align="center">
      <?php if((int)$arrRecords[$ind]['leave_status']) { ?>
      	<img title="Leave Form" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/display.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/leave_status/1/' . $arrRecords[$ind]['leave_id']; ?>';" />
      <?php } ?>
    </td>
    <td class="listContentColLast" align="center">
     <?php if($arrRecords[$ind]['leave_status'] == 0) { ?>
      	&nbsp;&nbsp;
        <img title="Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/leave_requisition/' . $arrRecords[$ind]['leave_id']; ?>';">
        <!--<img title="Delete" style="margin:-7px 0;cursor:pointer" width="20" src="<?php //echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php //echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php //echo $arrRecords[$ind]['leave_id']; ?>');">-->
      <?php } else {
		  		echo $strStatus;
	  		}
	  ?>
      </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="12" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="265px">Edit <span style="font-style:italic;color:#666">(Can only edit pending requests)</span></td>
        </tr>
    </table>
</div>