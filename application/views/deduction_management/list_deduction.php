<script>
// $(document).ready(function () {
// 	$('.select-state').selectize({
// 		sortField: 'text'
// 	});
// });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;

	}
	.item{
		width: 100%;
	} 
/* .selectize-input.items.has-options.full.has-items{
	height: 35px !important;
}
.item{
	width: 100%;
} */
</style>
<?php


$leaveCategory 	= (isset($_POST['leaveCategory'])) 		? $_POST['leaveCategory'] 	: '';
$leaveStatus 	= (isset($_POST['leaveStatus'])) 		? $_POST['leaveStatus'] 	: '';
$leaveMonth 	= (isset($_POST['leaveMonth'])) 		? $_POST['leaveMonth'] 		: '';
$leaveYear 		= (isset($_POST['leaveYear'])) 			? $_POST['leaveYear'] 		: '';
$empSickLeaves	=	(isset($_POST['empSickLeaves']))	?	$_POST['empSickLeaves']	: $record['emp_sick_leaves'];
if($leaveStatus == 0 && $leaveStatus != '') {
	$leaveStatus = -1;
}
?>
<form name="frmDeductionStatus" id="frmDeductionStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Leaves Of:</div>
        <div class="textBoxContainer">
        	<select id="select-state" name="deductiononth" class="dropDown select-state" style="width:85px">
            	<option value="">Month</option>
            	<option value="01">Jan</option>
            	<option value="02">Feb</option>
            	<option value="03">Mar</option>
            	<option value="04">Apr</option>
            	<option value="05">May</option>
            	<option value="06">Jun</option>
            	<option value="07">Jul</option>
            	<option value="08">Aug</option>
            	<option value="09">Sep</option>
            	<option value="10">Oct</option>
            	<option value="11">Nov</option>
            	<option value="12">Dec</option>
          	</select>&nbsp;&nbsp;&nbsp;&nbsp;
        	<select id="select-state" name="deductionYear" class="dropDown select-state" style="width:85px; margin-left:5px">
            	<option value="">Year</option>
            	<?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
            	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
				<?php } ?>
          	</select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Select Employee:</div>
        <div class="textBoxContainer">
      	<select name="emp_id" class="dropDown select-state" id="select-state"  style="width:175px">
            <option value="">All</option>
            <?php
				if (count($all_employees)) {
					for($ind = 0; $ind < count($all_employees); $ind++) {?>
					<option value="<?php echo $all_employees[$ind]['emp_id']; ?>" 
					<?php
						if(isset($emploanRequest[0]['emp_id']))
						{
							echo $emploanRequest[0]['emp_id'] == $all_employees[$ind]['emp_id'] ? 'selected' : null;
						}
					?>
					><?php echo $all_employees[$ind]['emp_code'].' --- '.$all_employees[$ind]['emp_full_name']; ?></option>
					<?php
					}
			  	}
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Deduction Status:</div>
        <div class="textBoxContainer">
      	<select name="deductionStatus" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>            
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>

    </div>
  </div>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php 
		// echo "Total Records Count: " . count($arrRecords);
		 ?>
		
		<b>Legends:</b>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F9F084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#D9FFA0;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Approved</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Rejected</span>
    </div>	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>
		
<div class="listContentMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		<tr class="listHeader">
			<td class="listHeaderCol">ID</td>
			<td class="listHeaderCol">Emplyee Name</td>
			<td class="listHeaderCol">Deduction</td>
			<td class="listHeaderCol">Created by</td>
			<td class="listHeaderCol">Created Date</td>
			<td class="listHeaderColLast" align="center">Action/Status</td>
		</tr>
		<?php
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				if(in_array($this->userRoleID,array(HR_ADMIN_ROLE_ID,CCTV_OPERATOR)))
				{

				
				if($arrRecords[$ind]['deduction_status'] == 0) {
					$cssBtn = 'smallButtonUpdate';
					$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
					$strStatus = 'Pending';
				} elseif($arrRecords[$ind]['deduction_status'] == 1) {
					$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
					$strStatus = 'Approved';
				} elseif($arrRecords[$ind]['deduction_status'] == 2) {
					$trCSSClass = 'listContentAlternate" style="background-color:#F38374"';
					$strStatus = 'Rejected';
				}  else {
					$trCSSClass = 'listContentAlternate';
					$strStatus = '';
				}
				?>
				<tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
					<td class="listContentCol"><?php echo $arrRecords[$ind]['id']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['emp_name']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['name']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['operator_name']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['created_date']; ?></td>
					<td class="listContentColLast" align="center">
					<?php
						if(($arrRecords[$ind]['deduction_status'] == 0 && in_array($this->userRoleID,array(HR_ADMIN_ROLE_ID))) ) {
							?>
							&nbsp;&nbsp;
							<img title="Approve" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/leave_approve.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/deduction_CCTV_request/' . $arrRecords[$ind]['id']; ?>';">
							&nbsp;&nbsp;
							<img title="Delete" style="margin:-7px 0;cursor:pointer" width="20" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/deduction_CCTV_request/' . $arrRecords[$ind]['id'].'/1'; ?>';">
						<?php } elseif($arrRecords[$ind]['deduction_status'] == 0 && $arrRecords[$ind]['emp_id'] == $this->userEmpNum) {
							?>
							<br />
							&nbsp;&nbsp;
							<img title="Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/deduction_CCTV_request/' . $arrRecords[$ind]['id'].'/2' ?>';">
							<?php
						}elseif($arrRecords[$ind]['deduction_status'] == 1){
							echo $strStatus;
						}elseif($arrRecords[$ind]['deduction_status'] == 2){
							echo $strStatus;
						}else{
							echo $strStatus;

						}
					?>
					</td>
				</tr>
				<?php
			}
			}
		if(!$ind) {
		?>
		<tr class="listContentAlternate">
			<td colspan="11" align="center" class="listContentCol">No Record Found</td>
		</tr>
		<?php
		}
		?>
	</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="265px">Edit <span style="font-style:italic;color:#666">(Can only edit pending requests)</span></td>
        </tr>
    </table>
</div>
