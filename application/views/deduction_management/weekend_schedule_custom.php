<div class="popupNotificationMain" style="font-size:13px">
  <div class="popupNotificationBox">
    <div class="weekendPopupHeading"> Saturday Schedule for <?php echo date('F Y'); ?> </div>
    <div style="float:left;width:100%">
      <div style="border:5px solid #99DAEF;float:none;margin:0 auto;width:60%">
        <table border="0" align="center" cellspacing="0" cellpadding="0" class="dottedBorder" style="width:100%">
          <tr>
            <td class="weekendPopupCol">Name</td>
            <td colspan="2" class="weekendPopupColLast"><?php echo $arrEmployee['emp_full_name']; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Employee Id</td>
            <td colspan="2" class="weekendPopupColLast"><?php echo $arrEmployee['emp_code']; ?></td>
          </tr>
          <tr>
            <td class="weekendPopupCol">Date of Joining</td>
            <td colspan="2" class="weekendPopupColLast"><?php echo readableDate($arrEmployee['emp_joining_date'], 'jS F, Y'); ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Local Manager</td>
            <td colspan="2" class="weekendPopupColLast"><?php echo $arrSupervisor['emp_full_name']; ?></td>
          </tr>
          <tr style="display:none">
            <td class="weekendPopupCol">Target (Ship Ok)</td>
            <td colspan="2" class="weekendPopupColLast">&nbsp;</td>
          </tr>
          <tr class="weekendPopupRowAlternate" style="display:none">
            <td class="weekendPopupCol">Achieve (Ship Ok)</td>
            <td colspan="2" class="weekendPopupColLast">&nbsp;</td>
          </tr>
          <tr>
            <td class="weekendPopupColLast">&nbsp;</td>
            <td colspan="2" class="weekendPopupColLast">&nbsp;</td>
          </tr>
          <?php
				$jnd = -1;
				
				for($ind = 0; $ind < count($arrRecords); $ind++) {
											
					if((int)$arrRecords[$ind]['weekend_status']) {
						$cssClass = 'weekendPopupBtnBlue';
						$strTxt = 'ON&nbsp;';
					} else {
						$cssClass = 'weekendPopupBtnRed';
						$strTxt = 'OFF';
					}
			?>
          <tr>
            <td style="width:40%" class="weekendPopupColLast"><?php echo readableDate($arrRecords[$ind]['weekend_date'], 'jS F, Y'); ?></td>
            <td style="width:50%" class="weekendPopupColLast">&nbsp;</td>
            <td style="width:10%" class="weekendPopupColLast"><span class="<?php echo $cssClass; ?>"><?php echo $strTxt; ?></span></td>
          </tr>
          <?php
				}
			?>
          <tr>
            <td class="weekendPopupColLast">&nbsp;</td>
            <td colspan="2" class="weekendPopupColLast">&nbsp;</td>
          </tr>
          <tr>
            <td class="weekendPopupColLast" colspan="3" style="color:#F00">* Above information takes some time to be updated from Accounts Department</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div style="text-align:center">
    <input  class="smallButton"  name="close" type="button" value="Close" onclick="window.close()">
  </div>
</div>
