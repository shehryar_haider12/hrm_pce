<?php
$religionName	 	= (isset($_POST['religionName']))		?	$_POST['religionName']		:	$record['religion_name'];
if ($record['religion_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['religion_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['religion_status'] == STATUS_DELETED) {$recordStatus = 2;}
$selStatus 			= (isset($_POST['selStatus'])) 			?	$_POST['selStatus']			:	$recordStatus;
?>

<form name="frmAddReligion" id="frmAddReligion" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['religion_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Religion</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add Religion</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Religion Name:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="religionName" name="religionName" class="textBox" value="<?php echo $religionName; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('selStatus',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addGrade" type="submit" value="Save">
                    <?php if($record['religion_id'] && strpos($_SERVER["REQUEST_URI"],$record['religion_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_religions' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#selStatus').val('<?php echo $selStatus; ?>');
</script>