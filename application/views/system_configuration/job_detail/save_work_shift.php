<?php
$workShift 		= (isset($_POST['workShift']))		?	$_POST['workShift']			:	$record['shift_name'];
$workShiftFrom 	= (isset($_POST['workShiftFrom'])) 	? 	$_POST['workShiftFrom']		:	$record['shift_hours_from'];
$workShiftTo 	= (isset($_POST['workShiftTo']))	?	$_POST['workShiftTo']		:	$record['shift_hours_to'];
$workShiftHours = (isset($_POST['workShiftHours'])) ?	$_POST['workShiftHours']	:	$record['shift_duration'];
if ($record['shift_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['shift_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['shift_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		= (isset($_POST['status'])) 		?	$_POST['status']			:	$recordStatus;
?>

<form name="frmAddWorkShift" id="frmAddWorkShift" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['shift_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Work Shift</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Work Shift</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Work Shift:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="workShift" name="workShift" maxlength="100" class="textBox" value="<?php echo $workShift; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Work Shift From:</td>
                <td class="formTextBoxContainer">
                	<select id="workShiftFrom" name="workShiftFrom" class="dropDown">
                        <option value="">From</option>
                        <?php
                        if (count($shift_timings)) {
                            foreach($shift_timings as $arrShift) {
                        ?>
                            <option value="<?php echo $arrShift; ?>"><?php echo $arrShift; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
            	<td class="formLabelContainer">Work Shift To:</td>
                <td class="formTextBoxContainer">
                	<select id="workShiftTo" name="workShiftTo" class="dropDown">
                        <option value="">To</option>
                        <?php
                        if (count($shift_timings)) {
                            foreach($shift_timings as $arrShift) {
                        ?>
                            <option value="<?php echo $arrShift; ?>"><?php echo $arrShift; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Work Shift Hours:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="workShiftHours" name="workShiftHours" maxlength="2" class="textBox" value="<?php echo $workShiftHours; ?>">
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addWorkShift" type="submit" value="Save">
                    <?php if($record['shift_id'] && strpos($_SERVER["REQUEST_URI"],$record['shift_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_work_shift' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#workShiftFrom').val('<?php echo $workShiftFrom; ?>');
	$('#workShiftTo').val('<?php echo $workShiftTo; ?>');
	$('#status').val('<?php echo $status; ?>');
</script>