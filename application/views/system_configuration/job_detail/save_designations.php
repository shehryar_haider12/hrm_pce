<?php
$designationName	=	(isset($_POST['designationName']))	?	$_POST['designationName']	:	$record['designation_name'];
$jobType	=	(isset($_POST['jobType']))	?	$_POST['jobType']	:	$record['job_type'];
if ($record['designation_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['designation_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['designation_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		= 	(isset($_POST['status'])) 			?	$_POST['status']			:	$recordStatus;
?>

<form name="frmAddDesignation" id="frmAddDesignation" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['designation_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Designation</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Designation</td>
                <?php } ?>
			</tr>
            <tr>
                <td class="formLabelContainer">Designation Name:</td>
                <td class="formTextBoxContainer">
                    <input class="textBox" type="text" id="designationName" name="designationName" value="<?php echo $designationName; ?>" maxlength="25" />
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Designation Type:</td>
                <td class="formTextBoxContainer">
                    <select id="jobType" name="jobType" class="dropDown">
                        <option value="">Select Designation Type</option>
                        <option value="1">Sales</option>
                        <option value="2">Non-Sales</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addDesignation" type="submit" value="Save">
                    <?php if($record['designation_id'] && strpos($_SERVER["REQUEST_URI"],$record['designation_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_designation' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>    
</div>
</form>

<script>
	$('#status').val('<?php echo $status; ?>');
	$('#jobType').val('<?php echo $jobType; ?>');
</script>