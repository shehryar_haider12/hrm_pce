<?php
$jobCategory	=	(isset($_POST['jobCategory']))	?	$_POST['jobCategory']	:	$record['job_category_name'];
if ($record['job_category_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['job_category_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['job_category_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status			=	(isset($_POST['status']))		?	$_POST['status']		:	$recordStatus;
?>

<form name="frmAddJobCategory" id="frmAddJobCategory" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['job_category_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Job Category</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Job Category</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Job Category:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="jobCategory" name="jobCategory" maxlength="100" class="textBox" value="<?php echo $jobCategory; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addJobCategory" type="submit" value="Save">
                    <?php if($record['job_category_id'] && strpos($_SERVER["REQUEST_URI"],$record['job_category_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_job_category' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#status').val('<?php echo $status; ?>');
</script>