<?php
$jobCategory	=	(isset($_POST['jobCategory']))		?	$_POST['jobCategory']		:	$record['job_category_id'];
$jobTitle 		= 	(isset($_POST['jobTitle'])) 		? 	$_POST['jobTitle'] 			:	$record['job_title_name'];
$jobDescription = 	(isset($_POST['jobDescription'])) 	? 	$_POST['jobDescription']	:	$record['job_description'];
if ($record['job_title_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['job_title_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['job_title_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		= (isset($_POST['status'])) 			? 	$_POST['status']			: 	$recordStatus;
?>

<form name="frmAddJobTitle" id="frmAddJobTitle" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['job_title_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Job Title</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Job Title</td>
                <?php } ?>
			</tr>
            <tr>
                <td class="formLabelContainer">Job Category:</td>
                <td class="formTextBoxContainer">
                    <select id="jobCategory" name="jobCategory" class="dropDown">
                    	<option value="">Select Job Category</option>
						<?php
                        if (count($jobCategories)) {
                            foreach($jobCategories as $arrJobCategory) {
                        ?>
                            <option value="<?php echo $arrJobCategory['job_category_id']; ?>"><?php echo $arrJobCategory['job_category_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Job Title:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="jobTitle" name="jobTitle" maxlength="100" class="textBox" value="<?php echo $jobTitle; ?>">
                </td>
            </tr>
            <tr>
            	<td class="formLabelContainer">Job Description:</td>
                <td class="formTextBoxContainer">
                	<textarea rows="4" cols="30" name="jobDescription" maxlength="500" id="jobDescription"><?php echo $jobDescription; ?></textarea>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addJobTitle" type="submit" value="Save">
                    <?php if($record['job_title_id'] && strpos($_SERVER["REQUEST_URI"],$record['job_title_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_job_title' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#jobCategory').val('<?php echo $jobCategory; ?>');
	$('#status').val('<?php echo $status; ?>');
</script>