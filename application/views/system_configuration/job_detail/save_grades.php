<?php
$jobType	 		= (isset($_POST['jobType']))			?	$_POST['jobType']			:	$record['job_type'];
$gradeCode 			= (isset($_POST['gradeCode']))			?	$_POST['gradeCode']			:	$record['grade_code'];
$gradeTitle 		= (isset($_POST['gradeTitle']))			?	$_POST['gradeTitle']		:	$record['grade_title'];
$designation 		= (isset($_POST['designation']))		?	$_POST['designation']		:	$record['designation_id'];
$salaryRangeStart 	= (isset($_POST['salaryRangeStart']))	?	$_POST['salaryRangeStart']	:	$record['salary_range_start'];
$salaryRangeEnd 	= (isset($_POST['salaryRangeEnd']))		?	$_POST['salaryRangeEnd']	:	$record['salary_range_end'];
$gradeCriteria 		= (isset($_POST['gradeCriteria']))		?	$_POST['gradeCriteria']		:	$record['grade_criteria'];
$gradeBenefits 		= (isset($_POST['gradeBenefits']))		?	$_POST['gradeBenefits']		:	$record['grade_benefits'];
if ($record['grade_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['grade_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['grade_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 			= (isset($_POST['status'])) 			?	$_POST['status']			:	$recordStatus;
?>

<form name="frmAddGrade" id="frmAddGrade" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['edu_level_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Band & Grade</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add Band & Grade</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Job Type:</td>
                <td class="formTextBoxContainer">
                	<select id="jobType" name="jobType" class="dropDown">
                        <option value="">Select Job Type</option>
                        <option value="1">Sales</option>
                        <option value="2">Non-Sales</option>
                    </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Grade Code:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="gradeCode" name="gradeCode" maxlength="3" class="textBox" value="<?php echo $gradeCode; ?>">
                </td>
            </tr>
            <tr>
            	<td class="formLabelContainer">Grade Title:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="gradeTitle" name="gradeTitle" maxlength="100" class="textBox" value="<?php echo $gradeTitle; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Designation:</td>
                <td class="formTextBoxContainer">
                	<select id="designation" name="designation" class="dropDown">
                    	<option value="">Select Designation</option>
						<?php
                        if (count($Designations)) {
                            foreach($Designations as $arrDesignation) {
								if($arrDesignation['job_type'] == 1) $designationType = "Sales";
								else if($arrDesignation['job_type'] == 2) $designationType = "Non-Sales";
                        ?>
                            <option value="<?php echo $arrDesignation['designation_id']; ?>"><?php echo $designationType .' - '. $arrDesignation['designation_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
             <tr>
            	<td class="formLabelContainer">Salary Range Start:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="salaryRangeStart" name="salaryRangeStart" maxlength="8" class="textBox" value="<?php echo $salaryRangeStart; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Salary Range End:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="salaryRangeEnd" name="salaryRangeEnd" maxlength="8" class="textBox" value="<?php echo $salaryRangeEnd; ?>">
                </td>
            </tr>
            <tr>
            	<td class="formLabelContainer">Grade Criteria:</td>
                <td class="formTextBoxContainer">
                	<textarea rows="5" cols="30" name="gradeCriteria" id="gradeCriteria" class="textArea"><?php echo $gradeCriteria; ?></textarea>
                </td>
            </tr>
             <tr class="formAlternateRow">
            	<td class="formLabelContainer">Grade Benefits:</td>
                <td class="formTextBoxContainer">
                	<textarea rows="5" cols="30" name="gradeBenefits" id="gradeBenefits" class="textArea"><?php echo $gradeBenefits; ?></textarea>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addGrade" type="submit" value="Save">
                    <?php if($record['grade_id'] && strpos($_SERVER["REQUEST_URI"],$record['grade_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_grades' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#jobType').val('<?php echo $jobType; ?>');
	$('#status').val('<?php echo $status; ?>');
	$('#designation').val('<?php echo $designation; ?>');
</script>