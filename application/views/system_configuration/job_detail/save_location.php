<?php

$locationType = (isset($_POST['locationType']))	?	$_POST['locationType']	:	$record['location_type_id'];

if($record['region_name'] == '')
{
	if($record['country_region_name'] != '')
	{ $region = $record['country_region_name']; }
} else {
	$region = $record['region_name'];
}

if($region == '')
{ $region = $record['location_name']; }

$regionName = (isset($_POST['Region']))	?	$_POST['Region']	:	$region;

if ($record['location_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['location_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['location_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status = (isset($_POST['status']))	?	$_POST['status']	:	$recordStatus;
?>

<form name="frmAddLocation" id="frmAddLocation" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['location_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Location</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Location</td>
                <?php } ?>
			</tr>
             <tr id="trLocationType">
                <td class="formLabelContainer">Location Type:</td>
                <td class="formTextBoxContainer">
                    <select id="locationType" name="locationType" class="dropDown" onchange="displayFields(this.value)">
                        <option value="">Select Location Type</option>
                        <option value="1">Is Region</option>
                        <option value="2">Is Country</option>
                        <option value="3">Is City</option>
                    </select>
                </td>
            </tr>
            <tr id="trRegion" class="formAlternateRow" style="display:none">
                <td class="formLabelContainer">Region:</td>
                <td id="tdRegion" class="formTextBoxContainer">
                    <input class="textBox" type="text" id="Region" name="Region" value="<?php echo $regionName; ?>" maxlength="25" />
                </td>
            </tr>
            <tr id="trCountry" style="display:none">
                <td class="formLabelContainer">Country:</td>
                <td id="tdCountry" class="formTextBoxContainer">
                    <input class="textBox" type="text" id="Country" name="Country" value="<?php echo $countryName; ?>" maxlength="25" />
                </td>
            </tr>
            <tr id="trCity" class="formAlternateRow" style="display:none">
                <td class="formLabelContainer">City:</td>
                <td id="tdCity" class="formTextBoxContainer">
                   <input class="textBox" type="text" id="City" name="City" value="<?php echo $cityName; ?>" maxlength="25" />
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addLocation" type="submit" value="Save">
                    <?php if($record['location_id'] && strpos($_SERVER["REQUEST_URI"],$record['location_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_location' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>

</form>

<script>

	function displayFields(field)
	{
		if(field == '1')
		{
			$('#trCountry').hide();
			$('#trCity').hide();
			$('#trRegion').show();
			$('#Region').remove();
			$('#tdRegion').html('<input type="text" name="Region" id="Region" class="textBox" />');
		}
		else if(field == '2')
		{
			$('#trCity').hide();
			$('#trRegion').show();
			$('#trCountry').show();
			populateRegion('tdRegion', '1', '0', field, '0');
		}
		else if(field == '3')
		{
			$('#trRegion').show();
			$('#trCountry').hide();
			$('#trCity').hide();
			populateRegion('tdRegion', '1', '1', field, '0');
			$('#tdCountry').show();
			
		}
		else
		{
			$('#trRegion').hide();
			$('#trCountry').hide();
			$('#trCity').hide();
		}
		
	}

	$('#locationType').val('<?php echo $locationType; ?>');
	$('#status').val('<?php echo $status; ?>');
</script>

<?php if($locationType) { ?>
	<script>displayFields($('#locationType').val())</script>
<?php } ?>

<?php if($record['location_type_id'] == 1) { ?>
	<script>
		$('#trLocationType').show();
		$('#trRegion').show();
    </script>
<?php } else if($record['location_type_id'] == 2) { ?>
	<script>
		$('#trLocationType').show();
		$('#trRegion').show();
		$('#trCountry').show();
		populateRegion('tdRegion', '1', '0', '2', '<?php echo $record['location_parent_id'];  ?>');
		$('#Country').val('<?php echo $record['location_name'];  ?>');
    </script>
<?php } else if($record['location_type_id'] == 3) { ?>
	<script>
		$('#trLocationType').hide();
		$('#trRegion').hide();
		$('#trCountry').show();
		$('#trCity').show();
		populateCountry('tdCountry', '<?php echo $record['country_region_id']; ?>', '<?php echo $record['location_parent_id'];  ?>')
		$('#City').val('<?php echo $record['location_name'];  ?>');
    </script>
<?php } ?>