<?php
$educationMajor = (isset($_POST['educationMajor']))		?	$_POST['educationMajor']	:	$record['edu_major_name'];
if ($record['edu_major_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['edu_major_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['edu_major_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		= (isset($_POST['status'])) 			?	$_POST['status']			:	$recordStatus;
?>

<form name="frmAddEduLevel" id="frmAddEduLevel" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['edu_level_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Education Major</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add Education Major</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Education Major:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="educationMajor" name="educationMajor" maxlength="100" class="textBox" value="<?php echo $educationMajor; ?>">
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addEduMajor" type="submit" value="Save">
                    <?php if($record['edu_major_id'] && strpos($_SERVER["REQUEST_URI"],$record['edu_major_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_edu_major' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#status').val('<?php echo $status; ?>');
</script>