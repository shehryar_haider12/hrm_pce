<?php
$displayModuleName		= (isset($_POST['displayModuleName']))	?	$_POST['displayModuleName'] 	:	$record['display_name'];
$moduleName 			= (isset($_POST['moduleName'])) 	   	?	$_POST['moduleName'] 			:	$record['module_name'];
$moduleParent 			= (isset($_POST['moduleParent'])) 		?	$_POST['moduleParent']			:	$record['module_parent_id'];
$layout 				= (isset($_POST['layout'])) 			? 	$_POST['layout']				:	$record['layout'];
$moduleSortOrder 		= (isset($_POST['moduleSortOrder'])) 	? 	$_POST['moduleSortOrder']		:	$record['module_sort_order'];
$moduleSkipListing 		= (isset($_POST['moduleSkipListing'])) 	? 	$_POST['moduleSkipListing']		:	$record['module_skip_display'];
if ($record['module_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['module_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['module_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 				= (isset($_POST['status'])) 			?	$_POST['status']				:	$recordStatus;
?>

<form name="frmAddModule" id="frmAddModule" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['module_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Module</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Module</td>
                <?php } ?>
			</tr>
            <tr>
                <td class="formLabelContainer">Module Display Name:</td>
                <td class="formTextBoxContainer">
                    <input class="textBox" type="text" id="displayModuleName" name="displayModuleName" value="<?php echo $displayModuleName; ?>" maxlength="100" />
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Module Name:</td>
                <td class="formTextBoxContainer">
                    <input class="textBox" type="text" id="moduleName" name="moduleName" value="<?php echo $moduleName; ?>" maxlength="100" />
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Parent Module:</td>
                <td class="formTextBoxContainer">
                    <select id="moduleParent" name="moduleParent" class="dropDown">
                        <option value="">Select Parent Module</option>
                        <?php
                        if (count($parentModules)) {
                            foreach($parentModules as $arrParentModule) {
                        ?>
                            <option value="<?php echo $arrParentModule['module_id']; ?>"><?php echo $arrParentModule['display_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Layout:</td>
                <td class="formTextBoxContainer">
                    <select id="layout" name="layout" class="dropDown">
                        <option value="">Select Layout</option>
                        <?php
                        if (count($layouts)) {
                            foreach($layouts as $arrLayout) {
                        ?>
                            <option value="<?php echo $arrLayout['layout_id']; ?>"><?php echo $arrLayout['layout_display_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Module Sort Order:</td>
                <td class="formTextBoxContainer">
                    <input class="textBox" type="text" id="moduleSortOrder" name="moduleSortOrder" value="<?php echo $moduleSortOrder; ?>" maxlength="2" />
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Module Skip Listing:</td>
                <td class="formTextBoxContainer">
                    <input type="checkbox" id="moduleSkipListing" name="moduleSkipListing" value="1" <?php echo ($moduleSkipListing == 1)? 'checked="checked"' : ''; ?> />
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addModule" type="submit" value="Save">
                    <?php if($record['module_id'] && strpos($_SERVER["REQUEST_URI"],$record['module_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_module' ?>';">
                    <?php } ?>
                </td>
        	</tr>
		</table>
	</div>      
</div>
</form>

<script>
	$('#moduleParent').val('<?php echo $moduleParent; ?>');
	$('#layout').val('<?php echo $layout; ?>');
	$('#status').val('<?php echo $status; ?>');
</script>