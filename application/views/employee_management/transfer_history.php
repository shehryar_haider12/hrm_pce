<?php
$TransferType 			= (isset($_POST['TransferType'])) 		? $_POST['TransferType'] 		  : $record['Transfer_type'];
$TransferCategory 	= (isset($_POST['TransferCategory'])) ? $_POST['TransferCategory'] 	: $record['Transfer_category'];
$TransferReason 		= (isset($_POST['TransferReason'])) 	? $_POST['TransferReason'] 		: $record['Transfer_reason'];
$TransferDate 		= (isset($_POST['TransferDate'])) 	? $_POST['TransferDate'] 		      : $record['processed_date'];
$dateFrom 			    = (isset($_POST['dateFrom'])) 				? $_POST['dateFrom'] 					: $record['Transfer_from'];
$dateTo 			      = (isset($_POST['dateTo'])) 					? $_POST['dateTo'] 						: $record['Transfer_to'];
$returnDate 		    = (isset($_POST['returnDate'])) 			? $_POST['returnDate'] 				: $record['Transfer_return'];
$TransferDays 			= (isset($_POST['TransferDays'])) 		? $_POST['TransferDays'] 			: $record['Transfer_days'];
$TransferDoc 		    = ($record['Transfer_doc'] != '') 	  ? $record['Transfer_doc'] 	  : 	'';
$empAnnualTransfers = $chuttiyan
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "minDate", "-30" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
	$( "#returnDate" ).datepicker( "setDate", "<?php echo $returnDate; ?>" );
	
});
</script>
<div class="row">
<div class="col-md-2">
<?php
$allowedSubModulesList = $this->privilege->getPrivilege($this->userRoleID, 5, true);
// .. print("==========>><PRE>"); 
// print_r($arrRecords);
// exit;

if(count($arrEmployee)) {
	if(!file_exists($pictureFolder . $arrEmployee['emp_photo_name']) || empty($arrEmployee['emp_photo_name'])) {
		if(empty($arrEmployee['emp_gender'])) {
			$arrEmployee['emp_gender'] = 'male';
		}
		$arrEmployee['emp_photo_name'] = 'no_image_' . strtolower($arrEmployee['emp_gender']) . '.jpg';
	}
?>
    <div class="employeeDetailLeftCol">
      <div class="employeePicMain">
		<div class="employeeNameContainer">
		<?php echo $arrEmployee['emp_full_name']; ?>
		</div>
        <div class="employeePicContainer">
            <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrEmployee['emp_photo_name']; ?>" width="185">
        </div>
      </div>
      <div class="employeeDetailLinksMain">
        <ul>
          <li id="save_employee"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/save_employee/' . $arrEmployee['emp_id']; ?>">Employee Profile</a></li>
          <li id="employment_details"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/employment_details/' . $arrEmployee['emp_id']; ?>">Employment Details</a></li>
          <li id="emp_education_history"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/emp_education_history/' . $arrEmployee['emp_id']; ?>">Educational History</a></li>
          <li id="emp_work_history"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/emp_work_history/' . $arrEmployee['emp_id']; ?>">Professional Experience</a></li>
          <li id="dependents"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/dependents/' . $arrEmployee['emp_id']; ?>">Dependents</a></li>
          <li id="emergency_contacts"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/emergency_contacts/' . $arrEmployee['emp_id']; ?>">Emergency Contacts</a></li>
          <li id="benefits"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/benefits/' . $arrEmployee['emp_id']; ?>">Benefits</a></li>
          <li id="document_list"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/document_list/' . $arrEmployee['emp_id']; ?>">Documents</a></li>
          <li id="salary_details"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/salary_details/' . $arrEmployee['emp_id']; ?>">Salary Details</a></li>
          <li id="transfer_history"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/transfer_history/' . $arrEmployee['emp_id']; ?>">Tranfer History</a></li>
          <?php if(isAdmin($this->userRoleID)) { ?>
          <li id="employment_status"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/employment_status/' . $arrEmployee['emp_id']; ?>">Employee's Exit</a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <script>$('#<?php echo $this->currentAction; ?>').addClass('selected');</script>
<?php
}
?>
</div>
<div class="col-md-9">
<?php if($canWrite == YES) { ?>
<form name="frmEmergencyContacts" id="frmEmergencyContacts" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Transfer History</td>
    </tr>
    
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Transfer From:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
        <select id="transferFrom" name="transferFrom" class="dropDown">
            <option value="">Select Branch</option>
            <?php
            if (count($listbranch)) {
                foreach($listbranch as $Transferfrom) {
            ?>
                <option value="<?php echo $Transferfrom['company_id']; ?>"><?php echo $Transferfrom['company_name']; ?></option>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Transfer To:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
        <select id="transferTo" name="transferTo" class="dropDown">
            <option value="">Select Branch</option>
            <?php
            if (count($listbranch)) {
                foreach($listbranch as $Transferto) {
            ?>
                <option value="<?php echo $Transferto['company_id']; ?>"><?php echo $Transferto['company_name']; ?></option>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Reason:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="transferReason" id="transferReason" class="textBox" value="<?php echo $TransferReason; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Transfer Date:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="date" name="transferDate" id="transferDate" class="textBox" value="<?php echo $TransferDate; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Add Supporting Document (If Any):</td>
      <td class="formTextBoxContainer">
      	<input type="file" name="TransferDoc" id="TransferDoc">
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"><input type="hidden" name="employeeID" id="employeeID" value="<?php echo $arrEmployee['emp_id']; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } 
// print_r($arrRecords);
// exit;
?>

<script>
	$('#emergencyContactRelationship').val('<?php echo $emergencyContactRelationship; ?>');
</script>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Posting</td>
    	<td class="listHeaderCol">From Branch/Dept Name</td>
    	<td class="listHeaderCol">To Branch/Dept Name</td>
    	<td class="listHeaderCol">Date of Posting</td>
    	<td class="listHeaderCol">Reason</td>
    	<td class="listHeaderCol">Period</td>
    	<td class="listHeaderCol">Processed  By</td>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</td>
		<?php } ?>
    </tr>
    <?php
    $i = 1;
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $i; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['company_from']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['company_to']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['processed_date']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['transfer_reason']; ?></td>
    	<td class="listContentCol"><?php 
      if(count($arrRecords) - 1 == $ind){
        $now = time(); // or your date as well
      }else{
        $now = strtotime($arrRecords[$ind + 1]['processed_date']); // or your date as well
      }

      $your_date = strtotime($arrRecords[$ind]['processed_date']);
      $datediff = $now - $your_date;
      
      echo round($datediff / (60 * 60 * 24)).' Days';
      // echo $ind;
      ?></td>
      <td class="listContentCol"><?php echo $arrRecords[$ind]['processed_by'] > 0 ? $arrRecords[$ind]['processed_name'] : 'Manual'; ?></td>
        <?php if(($canWrite == YES) || ($canDelete == YES)) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == YES) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $arrEmployee['emp_id'] . '/' . $arrRecords[$ind]['emc_id']; ?>';" />
            <?php } if($canDelete == YES) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['emc_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
    $i++;
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<?php if($canWrite == NO) { ?>
<script>$("#frmEmergencyContacts :input").attr("disabled", true);</script>
<?php } ?>
</div>
</div>