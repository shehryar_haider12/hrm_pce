<?php
$benefitTitle 		= (isset($_POST['benefitTitle'])) 			? $_POST['benefitTitle'] 		: $record['benefit_title'];
$benefitDetail 		= (isset($_POST['benefitDetail'])) 			? $_POST['benefitDetail'] 		: $record['benefit_detail'];
?>
<?php if($canWrite == YES) { ?>
<form name="frmBenefits" id="frmBenefits" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Benefit</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Benefit:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select id="benefitTitle" name="benefitTitle" class="dropDown">
            <option value="">Select Benefit</option>
            <?php
            if (count($empBenefits)) {
                foreach($empBenefits as $empBenefit) {
                    $selected = '';
                    if($benefitTitle == $empBenefit) {
                        $selected = 'selected="selected"';
                    }
            ?>
                <option value="<?php echo $empBenefit; ?>" <?php echo $selected; ?>><?php echo $empBenefit; ?></option>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Entitlement:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><textarea rows="6" name="benefitDetail" id="benefitDetail" style="width:400px" class="textArea"><?php echo $benefitDetail; ?></textarea></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"><input type="hidden" name="empID" id="empID" value="<?php echo $empID; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Benefit</td>
    	<td class="listHeaderCol">Detail</td>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</td>
		<?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol" width="30%"><?php echo $arrRecords[$ind]['benefit_title']; ?></td>
    	<td class="listContentCol" width="60%"><?php echo nl2br($arrRecords[$ind]['benefit_detail']); ?></td>
        <?php if($canWrite == YES) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == YES) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $arrRecords[$ind]['emp_id'] . '/' . $arrRecords[$ind]['benefit_id']; ?>';" />
            <?php } if($canDelete == YES) { ?>
            <input type="button" class="smallButton" id="deletButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrRecords[$ind]['emp_id']; ?>', '<?php echo $arrRecords[$ind]['benefit_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="3" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>