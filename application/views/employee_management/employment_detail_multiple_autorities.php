<?php
$empCompany				=	(isset($_POST['empCompany']))			?	$_POST['empCompany']			:	$record['emp_company_id'];
$empID					=	(isset($_POST['empID']))				?	$_POST['empID']					:	$record['emp_code'];
$empIPNum				=	(isset($_POST['empIPNum']))				?	$_POST['empIPNum']				:	$record['emp_ip_num'];
$empDepartment			=	(isset($_POST['empDepartment']))		?	$_POST['empDepartment']			:	$record['emp_job_category_id'];
$empWorkEmail			=	(isset($_POST['empWorkEmail']))			?	$_POST['empWorkEmail']			:	$record['emp_work_email'];
$empJobLocation			=	(isset($_POST['empJobLocation']))		?	$_POST['empJobLocation']		:	$record['emp_location_id'];
$empOT					=	(isset($_POST['empOT']))				?	$_POST['empOT']					:	$record['emp_ot_eligibility'];
$empDesignation			=	(isset($_POST['empDesignation']))		?	$_POST['empDesignation']		:	$record['emp_designation'];
$empSupervisor			=	(isset($_POST['empSupervisor']))		?	$_POST['empSupervisor']			:	$record['emp_authority_id'];
$empCurrency			=	(isset($_POST['empCurrency']))			?	$_POST['empCurrency']			:	$record['emp_currency_id'];
$empSponsor				=	(isset($_POST['empSponsor']))			?	$_POST['empSponsor']			:	$record['emp_sponsor_id'];
$empEmploymentType		=	(isset($_POST['empEmploymentType']))	?	$_POST['empEmploymentType']		:	$record['emp_employment_type'];
$empEmploymentStatus	=	(isset($_POST['empEmploymentStatus']))	?	$_POST['empEmploymentStatus']	:	$record['emp_employment_status'];
$empJoiningDate			=	(isset($_POST['empJoiningDate']))		?	$_POST['empJoiningDate']		:	$record['emp_joining_date'];
$empProbationEndDate	=	(isset($_POST['empProbationEndDate']))	?	$_POST['empProbationEndDate']	:	$record['emp_probation_end_date'];
$empConfirmationDate	=	(isset($_POST['empConfirmationDate']))	?	$_POST['empConfirmationDate']	:	$record['emp_confirmation_date'];
$empVisaCompany			=	(isset($_POST['empVisaCompany']))		?	$_POST['empVisaCompany']		:	$record['emp_visa_company_id'];
$empVisaIssueDate		=	(isset($_POST['empVisaIssueDate']))		?	$_POST['empVisaIssueDate']		:	$record['emp_visa_issue_date'];
$empVisaExpiryDate		=	(isset($_POST['empVisaExpiryDate']))	?	$_POST['empVisaExpiryDate']		:	$record['emp_visa_expiry_date'];
$empAnnualLeaves		=	(isset($_POST['empAnnualLeaves']))		?	$_POST['empAnnualLeaves']		:	$record['emp_annual_leaves'];
$empSickLeaves			=	(isset($_POST['empSickLeaves']))		?	$_POST['empSickLeaves']			:	$record['emp_sick_leaves'];
?>

<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	<?php if($empJoiningDate != '0000-00-00') { ?>
		$( "#empJoiningDate" ).datepicker( "setDate", "<?php echo $empJoiningDate; ?>" );
	<?php } ?>
	<?php if($empProbationEndDate != '0000-00-00') { ?>
		$( "#empProbationEndDate" ).datepicker( "setDate", "<?php echo $empProbationEndDate; ?>" );
	<?php } ?>
	<?php if($empConfirmationDate != '0000-00-00') { ?>
		$( "#empConfirmationDate" ).datepicker( "setDate", "<?php echo $empConfirmationDate; ?>" );
	<?php } ?>
	<?php if($empVisaIssueDate != '0000-00-00') { ?>
		$( "#empVisaIssueDate" ).datepicker( "setDate", "<?php echo $empVisaIssueDate; ?>" );
	<?php } ?>
	<?php if($empVisaExpiryDate != '0000-00-00') { ?>
		$( "#empVisaExpiryDate" ).datepicker( "setDate", "<?php echo $empVisaExpiryDate; ?>" );
	<?php } ?>
});
</script>

<form name="frmAddEmploymentDetails" id="frmAddEmploymentDetails" enctype="multipart/form-data" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Employment Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Company:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empCompany" name="empCompany" class="dropDown">
                  <option value="">Select Company</option>
                  <?php
                  if (count($empCompanies)) {
                      foreach($empCompanies as $arrCompany) {
                          $selected = '';
                          if($empCompany == $arrCompany['company_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrCompany['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompany['company_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Employee ID:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empID" id="empID" class="textBox" value="<?php echo $empID; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">IP Extension:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empIPNum" maxlength="4" id="empIPNum" class="textBox" value="<?php echo $empIPNum; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Work Email:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empWorkEmail" maxlength="100" id="empWorkEmail" class="textBox" value="<?php echo $empWorkEmail; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Department:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empDepartment" name="empDepartment" class="dropDown">
                  <option value="">Select Department</option>
                  <?php
                  if (count($empDepartments)) {
                      foreach($empDepartments as $departmentVal) {
                          $selected = '';
                          if($empDepartment == $departmentVal['job_category_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $departmentVal['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $departmentVal['job_category_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Designation:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empDesignation" id="empDesignation" class="textBox" value="<?php echo $empDesignation; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Job Location:</td>
            <td class="formTextBoxContainer">
        		<select id="empJobLocation" name="empJobLocation" class="dropDown">
                  <option value="">Select Location</option>
                  <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empJobLocation == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                  ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                  <?php
                        }
                    }
                  ?>
              </select> 
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Supervisor:</td>
            <td class="formTextBoxContainer">
        		<select id="empSupervisor" name="empSupervisor[]" class="dropDown" multiple="multiple" style="height:100px">
                  <option value="">Select Supervisor</option>
                  <?php
                  if (count($empSupervisors)) {
                      foreach($empSupervisors as $supervisorVal) {
                          $selected = '';
                          if(in_array($supervisorVal['emp_id'], $empSupervisor)) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $supervisorVal['emp_id']; ?>" <?php echo $selected; ?>><?php echo $supervisorVal['emp_full_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
         <tr>
            <td class="formLabelContainer">OT Eligibility:</td>
            <td class="formTextBoxContainer">
        		<select id="empOT" name="empOT" class="dropDown">
                  <option value="">Select OT Eligibility</option>
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
              </select>  
            </td>
      	</tr>
         <tr class="formAlternateRow">
            <td class="formLabelContainer">Currency:</td>
            <td class="formTextBoxContainer">
        		<select id="empCurrency" name="empCurrency" class="dropDown">
                  <option value="">Select Currency</option>
                  <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empCurrency == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                  ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name'] . ' - ' . $dataCountry['location_currency']; ?></option>
                  <?php
                        }
                    }
                  ?>
              </select>
            </td>
      	</tr>
        <tr>
            <td class="formHeaderRow" colspan="2">Contract Details</td>
        </tr>
        <tr>
            <td class="formLabelContainer">Contract Type:</td>
            <td class="formTextBoxContainer">
        		 <select id="empEmploymentType" name="empEmploymentType" class="dropDown" tabindex="46">
                  <option value="">Select Contract Type</option>
                  <?php
                  if (count($employmentTypes)) {
                      foreach($employmentTypes as $employmentType) {
                          $selected = '';
                          if($employmentType == $empEmploymentType) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $employmentType; ?>" <?php echo $selected; ?>><?php echo $employmentType; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Employment Status:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empEmploymentStatus" name="empEmploymentStatus" class="dropDown">
                  <option value="">Select Employment Status</option>
                  <?php
                  if (count($empEmploymentStatuses)) {
                      foreach($empEmploymentStatuses as $employmentStatusVal) {
                          $selected = '';
                          if($empEmploymentStatus == $employmentStatusVal['employment_status_id']) {
                              $selected = 'selected="selected"';
                          }
						  
						  if($employmentStatusVal['employment_status_id'] >= 6) {
								  $selected .= ' disabled="disabled"';
							  }
                  ?>
                      <option value="<?php echo $employmentStatusVal['employment_status_id']; ?>" <?php echo $selected; ?>><?php echo $employmentStatusVal['employment_status_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
              <script>$('#lblEmpEmploymentStatus').html($('#empEmploymentStatus').find(":selected").text());</script>
            </td>
      	</tr>
        <tr id="joiningDate">
            <td class="formLabelContainer">Joining Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empJoiningDate" maxlength="30" id="empJoiningDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr class="formAlternateRow" id="probationDate">
            <td class="formLabelContainer">Probation End Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empProbationEndDate" maxlength="30" id="empProbationEndDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr id="confirmationDate">
            <td class="formLabelContainer">Confirmation Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empConfirmationDate" maxlength="30" id="empConfirmationDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Remaining Annual Leaves:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
            	<?php
                if($canWrite != YES) {
					$empAnnualLeaves = round($empAnnualLeaves);
				}
				?>
        		 <input type="text" name="empAnnualLeaves" maxlength="4" id="empAnnualLeaves" class="textBox" value="<?php echo $empAnnualLeaves; ?>"> <span style="font-size:12px; font-style:italic; color:#999; margin-left:10px">(Employees on probation may not avail the  annual leaves. However, they can consume once confirmed.)</span>
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Remaining Sick Leaves:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empSickLeaves" maxlength="2" id="empSickLeaves" class="textBox" value="<?php echo $empSickLeaves; ?>">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Visa Company:</td>
            <td class="formTextBoxContainer">
        		 <select id="empVisaCompany" name="empVisaCompany" class="dropDown">
                  <option value="">Select Visa Company</option>
                  <?php
                  if (count($empCompanies)) {
                      foreach($empCompanies as $arrCompany) {
                          $selected = '';
                          if($empVisaCompany == $arrCompany['company_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrCompany['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompany['company_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Visa Issue Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empVisaIssueDate" maxlength="30" id="empVisaIssueDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Visa Expiry Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empVisaExpiryDate" maxlength="30" id="empVisaExpiryDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Sponsor:</td>
            <td class="formTextBoxContainer">
        		 <select id="empSponsor" name="empSponsor" class="dropDown">
                  <option value="">Select Sponsor</option>
                  <?php
                  if (count($empSponsors)) {
                      foreach($empSponsors as $arrSponsor) {
                          $selected = '';
                          if($empSponsor == $arrSponsor['sponsor_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrSponsor['sponsor_id']; ?>" <?php echo $selected; ?>><?php echo $arrSponsor['sponsor_type']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
        <?php if((isAdmin($this->userRoleID)) || ($canWrite == YES)) { ?>
		<tr class="formAlternateRow">      
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
    		</td>
        </tr>
        <?php } else { ?>
        	<script>$("#frmAddEmploymentDetails :input").attr("disabled", true).css({"background-color": "#ddd"});</script>
        <?php } ?>
  </table>
  </div>
</form>

<script>
	$("#empOT").val("<?php echo $empOT; ?>");
	
	function showResponse(value)
	{
		if(value == 1){
			$('#confirmationDateStar').hide();
		}else if((value == 6)||(value == 7)){
			$('#confirmationDateStar').hide();
		} else {
			$('#confirmationDateStar').hide();
		}
	}
	
	showResponse(<?php echo (int)$empEmploymentStatus; ?>);
	
</script>