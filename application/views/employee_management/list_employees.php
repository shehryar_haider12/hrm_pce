<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: '';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: '';
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", "0" );
	$( "#empJoiningDate" ).datepicker( "setDate", "<?php echo $empJoiningDate; ?>" );
	
});
</script>
<form name="frmListEmployees" id="frmListEmployees" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee Code:</div>
        <div class="textBoxContainer">
			<input type="text" id="empCode" name="empCode" class="textBox" value="<?php echo $empCode; ?>">
        </div>
        <div class="labelContainer">IP Extension:</div>
        <div class="textBoxContainer">
          <input type="text" id="empIP" name="empIP" class="textBox" value="<?php echo $empIP; ?>">
        </div>              
        <div class="labelContainer">Supervisor:</div>
        <div class="textBoxContainer">
        	<select name="empSupervisor" id="empSupervisor" class="dropDown">
            	<option value="">All</option>
                <?php
                if (count($arrSupervisors)) {
                    foreach($arrSupervisors as $key => $arrSupervisor) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($i=0;$i<count($arrSupervisor);$i++) { ?>					
                            <option value="<?php echo $arrSupervisor[$i]['emp_id']; ?>"><?php echo $arrSupervisor[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
                <?php	}
                }
                ?>
            </select>
        </div>
        <div class="labelContainer">Employee Email:</div>
        <div class="textBoxContainer">
          <input type="text" id="empEmail" name="empEmail" class="textBox" value="<?php echo $empEmail; ?>" />
        </div>             
        <div class="labelContainer">Company:</div>
        <div class="textBoxContainer">
        	<select id="empCompany" name="empCompany" class="dropDown">
                <option value="">All</option>
                <?php
                  for($ind = 0; $ind < count($arrCompanies); $ind++) {
                      $selected = '';
                      if($arrCompanies[$ind]['company_id'] == $empCompany) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $arrCompanies[$ind]['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompanies[$ind]['company_name']; ?></option>
                <?php
                  }
                ?>
              </select>
        </div>
      </div>
      <div class="searchCol">
		<div class="labelContainer">Education Level:</div>
        <div class="textBoxContainer">
          <select id="eduDegree" name="eduDegree" class="dropDown">
            <option value="">All</option>
            <?php
			  for($ind = 0; $ind < count($eduLevels); $ind++) {
				  $selected = '';
				  if($eduLevels[$ind]['edu_level_id'] == $eduLevel) {
					  $selected = 'selected="selected"';
				  }
            ?>
             <option value="<?php echo $eduLevels[$ind]['edu_level_id']; ?>" <?php echo $selected; ?>><?php echo $eduLevels[$ind]['edu_level_name']; ?></option>
            <?php
              }
            ?>
          </select>
        </div>
        <div class="labelContainer">Education Majors:</div>
        <div class="textBoxContainer">
          <select id="eduMajor" name="eduMajor" class="dropDown">
            <option value="">All</option>
            <?php
			for($ind = 0; $ind < count($eduMajors); $ind++) {
				$selected = '';
				if($eduMajors[$ind]['edu_major_id'] == $eduMajor) {
					$selected = 'selected="selected"';
				}
            ?>
            <option value="<?php echo $eduMajors[$ind]['edu_major_id']; ?>" <?php echo $selected; ?>><?php echo $eduMajors[$ind]['edu_major_name']; ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="labelContainer">Employment Status:</div>
        <div class="textBoxContainer">
        	<select name="empStatus" id="empStatus" class="dropDown">
            	<option value="">All</option>
            	<option value="active">Active</option>
            	<option value="none">None</option>
				<?php
                if (count($arrEmploymentStatuses)) {
                    foreach($arrEmploymentStatuses as $arrEmploymentStatus) {
                ?>
                <option value="<?php echo $arrEmploymentStatus['employment_status_id']; ?>"><?php echo $arrEmploymentStatus['employment_status_name']; ?></option>
                <?php
                    }
                }
                ?>
            	<option value="resigned">Resigned + Terminated</option>
            </select>
        </div>
        <div class="labelContainer">Employee Work Email:</div>
        <div class="textBoxContainer">
          <input type="text" id="empWorkEmail" name="empWorkEmail" class="textBox" value="<?php echo $empWorkEmail; ?>" />
        </div>
        <?php if(isAdmin($this->userRoleID)) { ?>
        <div class="labelContainer">Department:</div>
        <div class="textBoxContainer">
          <select id="empDepartment" name="empDepartment" class="dropDown">
            <option value="">All</option>
            <?php
			  for($ind = 0; $ind < count($empDepartments); $ind++) {
				  $selected = '';
				  if($empDepartments[$ind]['job_category_id'] == $empDepartment) {
					  $selected = 'selected="selected"';
				  }
            ?>
             <option value="<?php echo $empDepartments[$ind]['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $empDepartments[$ind]['job_category_name']; ?></option>
            <?php
              }
            ?>
          </select>
        </div>
        <?php } ?>
      </div>        
      <div class="searchCol">
        <div class="labelContainer">Marital Status:</div>
        <div class="textBoxContainer">
          <select id="empMaritalStatus" name="empMaritalStatus" class="dropDown">
            <option value="">All</option>
           	 <?php
			 if (count($maritalStatuses)) {
				foreach($maritalStatuses as $maritalVal) {
			 ?>
			  <option value="<?php echo $maritalVal; ?>"><?php echo $maritalVal; ?></option>
			  <?php
				  }
			  }
			  ?>
          </select>
        </div>
		<div class="labelContainer">Gender:</div>
        <div class="textBoxContainer">
          <select id="empGender" name="empGender" class="dropDown">
            <option value="">All</option>
            <?php
			if (count($Genders)) {
				foreach($Genders as $strGender) {
			?>
            <option value="<?php echo $strGender; ?>"><?php echo $strGender; ?></option>
            <?php
				}
			}
			?>
          </select>
        </div>
        <div class="labelContainer">Name Contains:</div>
        <div class="textBoxContainer">
        	<input type="text" id="empName" name="empName" class="textBox" value="<?php echo $empName; ?>" />
        </div>
        <div class="labelContainer">Location:</div>
        <div class="textBoxContainer">
          <select id="empCountry" name="empCountry" class="dropDown">
              <option value="">All</option>
              <?php 
                if (count($Countries)) {
                    foreach($Countries as $dataCountry) {
                        $strSelected = '';
                        if($empCountry == $dataCountry['location_id']) {
                            $strSelected = 'selected="selected"';
                        }
              ?>
                    <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
              <?php
                    }
                }
              ?>
          </select>
        </div>
        <div class="labelContainer">Contract Type:</div>
        <div class="textBoxContainer">
          <select id="empEmploymentType" name="empEmploymentType" class="dropDown" tabindex="46">
              <option value="">All</option>
              <?php
              if (count($employmentTypes)) {
                  foreach($employmentTypes as $employmentType) {
                      $selected = '';
                      if($employmentType == $empEmploymentType) {
                          $selected = 'selected="selected"';
                      }
              ?>
                  <option value="<?php echo $employmentType; ?>" <?php echo $selected; ?>><?php echo $employmentType; ?></option>
              <?php
                  }
              }
              ?>
          </select>
        </div>
      </div>
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val(0);">
        <?php if(isAdmin($this->userRoleID)) { ?>
        <input class="searchButton" name="btnExport" id="btnExport" value="Export Records" type="button" onclick="$('#txtExport').val(0); $('#tblColumns').toggle('fast');">
        <?php } ?>
      </div>
    </div> 
	<?php if(isAdmin($this->userRoleID)) { ?>
    <div class="clear"></div>
    <div id="tblColumns" style="display:none; background-color:#FFF;">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td class="weekendPopupColLast" colspan="7">
                  <div class="weekendPopupHeading">Select Columns</div>
              </td>
            </tr>
            <tr class="listContent">
            <?php
            for($ind = 0; $ind < count($arrTblColumns); $ind++) {
                if($ind % 7 == 0 && $ind < count($arrTblColumns)) {
            ?>
            </tr>
            <tr class="listContent">
            <?php
                }
            ?>
                <td align="left" style="width:200px">
                    <input type="checkbox" name="empColumns[]" id="<?php echo $arrTblColumns[$ind]; ?>" value="<?php echo $arrTblColumns[$ind]; ?>" />
                    <label for="<?php echo $arrTblColumns[$ind]; ?>"><?php echo $arrTblColumns[$ind]; ?></label>
                </td>
            <?php } ?>
            </tr>
            <tr>
                <td colspan="7" align="right">
                    <input type="hidden" name="txtExport" id="txtExport" value="0" />
                    <input class="searchButton" type="button" name="chkAll" id="chkAll" value="Check All" onclick="chkAllBoxes()" />
                    <input class="searchButton" type="submit" value="Submit" onclick="$('#txtExport').val(1);" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">
        function chkAllBoxes() {
            if($('#chkAll').val() == 'Check All') {
                $("#tblColumns input:checkbox").prop("checked", true);
                $('#chkAll').val('Uncheck All');
            } else {
                $("#tblColumns input:checkbox").prop("checked", false);
                $('#chkAll').val('Check All');
            }
        }
        </script>
    </div>
    <?php } ?>
  </div> 
  <script>
  	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#empIP').val('<?php echo $empIP; ?>');
  	$('#empDesignation').val('<?php echo $empDesignation; ?>');
  	$('#empMaritalStatus').val('<?php echo $empMaritalStatus; ?>');
  	$('#empGender').val('<?php echo $empGender; ?>');
  	$('#empWorkShift').val('<?php echo $empWorkShift; ?>');
  	$('#eduDegree').val('<?php echo $eduDegree; ?>');
  	$('#empMarket').val('<?php echo $empMarket; ?>');
	$('#empDepartment').val('<?php echo $empDepartment; ?>');
  	$('#eduMajor').val('<?php echo $eduMajor; ?>');
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  	$('#empCompany').val('<?php echo $empCompany; ?>');
  	$('#empStatus').val('<?php echo $empStatus; ?>');
	$('#empEmail').val('<?php echo $empEmail; ?>');
	$('#empWorkEmail').val('<?php echo $empWorkEmail; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Legends:</b>&nbsp;&nbsp;&nbsp;<span style="background-color:#B9D1B8;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Recently Modified</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#F38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Terminated/Resigned</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#EDEDED;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Inactive</span>
		&nbsp;&nbsp;&nbsp;<span style="text-decoration:underline">abc</span><span class="mandatoryStar"> Sortable Columns</span>
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol"></td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_first_name', 'frmListEmployees')">Employee Name</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_code', 'frmListEmployees')">Code</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_ip_num', 'frmListEmployees')">IP No.</td>
    <td class="listHeaderCol">Email</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_designation', 'frmListEmployees')">Designation</td>
    <td class="listHeaderColClickable" onclick="setSort('l.location_name', 'frmListEmployees')">Location</td>
    <td class="listHeaderColLast">Action</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		
		$cssWrkBtn = 'normal';
		$cssEduBtn = 'normal';
		
		if($arrRecords[$ind]['emp_employment_status'] > STATUS_EMPLOYEE_ONNOTICEPERIOD) {
			$trCSSClass = 'listContentAlternate" style="background-color:#F38374;color:#fff"';
		} else if($arrRecords[$ind]['emp_status'] <= STATUS_INACTIVE_VIEW) {
			$trCSSClass = 'listContentAlternate" style="background-color:#EDEDED"';
		} else if(strtotime($arrRecords[$ind]['modified_date']) >= strtotime('-3 days')) {
			$cssBtn = 'smallButtonUpdate';
			$trCSSClass = 'listContentAlternate" style="background-color:#B9D1B8"';
		} else {
			$trCSSClass = 'listContentAlternate';
		}
		
		if(strtotime($arrRecords[$ind]['work_created']) >= strtotime('-3 days') || strtotime($arrRecords[$ind]['work_modified']) >= strtotime('-3 days')) {
			$cssWrkBtn = 'update';
		}		
		if(strtotime($arrRecords[$ind]['education_created']) >= strtotime('-3 days') || strtotime($arrRecords[$ind]['education_modified']) >= strtotime('-3 days')) {
			$cssEduBtn = 'update';
		}
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol" style="background-color:#FFF; padding:0px; width:60px">
    <?php
    if(!file_exists($pictureFolder . $arrRecords[$ind]['emp_photo_name']) || empty($arrRecords[$ind]['emp_photo_name'])) {
		$arrRecords[$ind]['emp_photo_name'] = 'no_image_' . strtolower($arrRecords[$ind]['emp_gender']) . '.jpg';
	}
	?>
    	<img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrRecords[$ind]['emp_photo_name']; ?>" width="60" height="60" />
    </td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_code']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_ip_num']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_work_email']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_designation']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['location_name']; ?></td>
    <td class="listContentColLast">
      <div class="empColButtonContainer">
        <img title="Work Experience" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/work_'.$cssWrkBtn.'.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/emp_work_history/' . $arrRecords[$ind]['emp_id']; ?>';" >
        <img title="Education" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/education_'.$cssEduBtn.'.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/emp_education_history/' . $arrRecords[$ind]['emp_id']; ?>';" >
        <?php if($canWrite == YES || $this->userRoleID == COMPANY_ADMIN_ROLE_ID) { ?>
      	<img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/save_employee/' . $arrRecords[$ind]['emp_id']; ?>';">
      	<?php } if($canDelete == YES) { ?>
      	<img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecords[$ind]['emp_id']; ?>');">
      	<?php } ?>
	  </div>
	  </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
        	<td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/work_normal.png';?>" /></td>
            <td width="110px">Work Experience</td>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/education_normal.png';?>" /></td>
            <td width="75px">Education</td>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="70px">View / Edit</td>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" /></td>
            <td width="65px">Delete</td>
        </tr>
    </table>
</div>

