<?php
$eduLevel 		          = (isset($_POST['eduLevel'])) 			? $_POST['eduLevel'] 			: $record['edu_level_id'];
$eduInstitute 	        = (isset($_POST['eduInstitute'])) 		? $_POST['eduInstitute'] 		: $record['edu_institute'];
$eduMajor 		          = (isset($_POST['eduMajors'])) 			? $_POST['eduMajors'] 			: $record['edu_major_id'];
$eduTotalMarks	        = (isset($_POST['eduTotalMarks'])) 			? $_POST['eduTotalMarks'] 				: $record['eduTotalMarks'];
$eduObtMarks	 	        = (isset($_POST['eduObtMarks'])) 			? $_POST['eduObtMarks'] 				: $record['eduObtMarks'];
$eduobt_eng_math_Marks	= (isset($_POST['eduobt_eng_math_Marks'])) 			? $_POST['eduobt_eng_math_Marks'] 				: $record['eduobt_eng_math_Marks'];
$edu_compul_Sub	 		    = (isset($_POST['edu_compul_Sub'])) 			? $_POST['edu_compul_Sub'] 				: $record['edu_compul_Sub'];
$eduGPA	 		            = (isset($_POST['eduGPA'])) 			? $_POST['eduGPA'] 				: $record['edu_gpa'];
$eduEnded 		          = (isset($_POST['eduEnded'])) 			? $_POST['eduEnded'] 			: $record['edu_ended'];
// print_r($eduObtMarks);exit;
?>
<?php
    // print_r($eduObtMarks);
    // exit;
    ?>
<script>
$(function() {
	$( ".datePicker" ).datepicker();
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#eduFrom" ).datepicker( "setDate", "<?php echo $eduFrom; ?>" );
	$( "#eduTo" ).datepicker( "setDate", "<?php echo $eduTo; ?>" );
});
</script>
<?php if($canWrite == YES) { ?>
<form name="frmEduHistory" id="frmEduHistory" method="post">
  <div class="employeeFormMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  	<tr>
        <td class="formHeaderRow" colspan="2">Add/Edit Educational History</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Qualification Level:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          <select id="eduLevel" name="eduLevel" class="dropDown">
          	  <option value="">Select Degree</option>
              <?php
                  for($ind = 0; $ind < count($eduLevels); $ind++) {
					  $selected = '';
					  if($eduLevels[$ind]['edu_level_id'] == $eduLevel) {
						  $selected = 'selected="selected"';
					  }
              ?>
                  <option value="<?php echo $eduLevels[$ind]['edu_level_id']; ?>" <?php echo $selected; ?>><?php echo $eduLevels[$ind]['edu_level_name']; ?></option>
              <?php
                  }
              ?>
          </select>      
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Educational Institute:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="eduInstitute" maxlength="100" id="eduInstitute" class="textBox" value="<?php echo $eduInstitute; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Majors:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">     
          <select id="eduMajors" name="eduMajors" class="dropDown">
          	  <option value="">Select Major</option>
              <?php
                  for($ind = 0; $ind < count($eduMajors); $ind++) {
					  $selected = '';
					  if($eduMajors[$ind]['edu_major_id'] == $eduMajor) {
						  $selected = 'selected="selected"';
					  }
              ?>
                  <option value="<?php echo $eduMajors[$ind]['edu_major_id']; ?>" <?php echo $selected; ?>><?php echo $eduMajors[$ind]['edu_major_name']; ?></option>
              <?php
                  }
              ?>
          </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Passing Year:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">      	
      	<select name="eduEnded" id="eduEnded" class="dropDown" onchange="chkEduEnded('', this.value)">
            <option value="">Select Year</option>
            <option value="In Progress">In Progress</option>
            <?php for($jnd = date('Y'); $jnd > 1969; $jnd--) { ?>
            <option value="<?php echo $jnd; ?>"><?php echo $jnd; ?></option>
            <?php } ?>
        </select>
        <script>$('#eduEnded').val('<?php echo $eduEnded; ?>')</script>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Total Marks:</td>
      <td class="formTextBoxContainer"><input type="text" name="eduTotalMarks" maxlength="12" id="eduTotalMarks" class="textBox" value="<?php echo $eduTotalMarks; ?>"></td>
    </tr>
    <tr>
    
      <td class="formLabelContainer">Obtained Marks:</td>
      <td class="formTextBoxContainer"><input type="text" name="eduObtMarks" maxlength="12" id="eduObtMarks" class="textBox" value="<?php echo $eduObtMarks; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Obtained Marks In English / Math:</td>
      <td class="formTextBoxContainer"><input type="text" name="eduobt_eng_math_Marks" maxlength="12" id="eduobt_eng_math_Marks" class="textBox" value="<?php echo $eduobt_eng_math_Marks; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Compulsory Subjects (Major Subjects):</td>
      <td class="formTextBoxContainer"><input type="text" name="edu_compul_Sub" id="edu_compul_Sub" class="textBox" value="<?php echo $edu_compul_Sub; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">GPA/Grade:</td>
      <td class="formTextBoxContainer"><input type="text" name="eduGPA" maxlength="12" id="eduGPA" class="textBox" value="<?php echo $eduGPA; ?>" <?php echo ($eduEnded == 'In Progress') ? 'disabled="disabled"' : ''; ?>></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"><input type="hidden" name="eduEmployeeID" id="eduEmployeeID" value="<?php echo $eduEmployeeID; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">      
      </td>
    </tr>
    <tr>
    	<td colspan="2" height="25px">&nbsp;</td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Degree</th>
    	<td class="listHeaderCol">Institute</th>
    	<td class="listHeaderCol">Majors</th>
    	<td class="listHeaderCol">Ended At</th>
    	<td class="listHeaderCol">Total Marks</th>
    	<td class="listHeaderCol">Obtained Marks</th>
    	<td class="listHeaderCol">Compulsory Subjects</th>
    	<td class="listHeaderCol">GPA/Grade</th>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</th>
        <?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_level_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_institute']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_major_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_ended']; ?></td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['eduTotalMarks'])) ? $arrRecords[$ind]['eduTotalMarks'] : ' - '; ?></td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['eduObtMarks'])) ? $arrRecords[$ind]['eduObtMarks'] : ' - '; ?></td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['edu_compul_Sub'])) ? $arrRecords[$ind]['edu_compul_Sub'] : ' - '; ?></td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['edu_gpa'])) ? $arrRecords[$ind]['edu_gpa'] : ' - '; ?></td>
        <?php if($canWrite == YES) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == YES) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $eduEmployeeID . '/' . $arrRecords[$ind]['edu_id']; ?>';" />
            <?php } if($canDelete == YES) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['edu_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="6" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<br />
<?php if($canWrite == NO) { ?>
<script>$("#frmEduHistory :input").attr("disabled", true);</script>
<?php } ?>