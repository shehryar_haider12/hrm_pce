<?php
$empCompany				=	(isset($_POST['empCompany']))			?	$_POST['empCompany']			:	$record['emp_company_id'];
$empID					=	(isset($_POST['empID']))				?	$_POST['empID']					:	$record['emp_code'];
$empIPNum				=	(isset($_POST['empIPNum']))				?	$_POST['empIPNum']				:	$record['emp_ip_num'];
$empDepartment			=	(isset($_POST['empDepartment']))		?	$_POST['empDepartment']			:	$record['emp_job_category_id'];
$empWorkEmail			=	(isset($_POST['empWorkEmail']))			?	$_POST['empWorkEmail']			:	$record['emp_work_email'];
$empJobLocation			=	(isset($_POST['empJobLocation']))		?	$_POST['empJobLocation']		:	$record['emp_location_id'];
$empOT					=	(isset($_POST['empOT']))				?	$_POST['empOT']					:	$record['emp_ot_eligibility'];
$empDesignation			=	(isset($_POST['empDesignation']))		?	$_POST['empDesignation']		:	$record['emp_designation'];
$empSupervisor			=	(isset($_POST['empSupervisor']))		?	$_POST['empSupervisor']			:	getEmpSupervisors($record['emp_id']);
$empCurrency			=	(isset($_POST['empCurrency']))			?	$_POST['empCurrency']			:	$record['emp_currency_id'];
$empSponsor				=	(isset($_POST['empSponsor']))			?	$_POST['empSponsor']			:	$record['emp_sponsor_id'];
$empEmploymentType		=	(isset($_POST['empEmploymentType']))	?	$_POST['empEmploymentType']		:	$record['emp_employment_type'];
$empEmploymentStatus	=	(isset($_POST['empEmploymentStatus']))	?	$_POST['empEmploymentStatus']	:	$record['emp_employment_status'];
$empJoiningDate			=	(isset($_POST['empJoiningDate']))		?	$_POST['empJoiningDate']		:	$record['emp_joining_date'];
$empProbationEndDate	=	(isset($_POST['empProbationEndDate']))	?	$_POST['empProbationEndDate']	:	$record['emp_probation_end_date'];
$empConfirmationDate	=	(isset($_POST['empConfirmationDate']))	?	$_POST['empConfirmationDate']	:	$record['emp_confirmation_date'];
$empBasicSalary			=	(isset($_POST['empBasicSalary']))		?	$_POST['empBasicSalary']		:	$record['emp_basic_salary'];
$empTotalSalary			=	(isset($_POST['empTotalSalary']))		?	$_POST['empTotalSalary']		:	$record['emp_total_salary'];
$empVisaIssueDate		=	(isset($_POST['empVisaIssueDate']))		?	$_POST['empVisaIssueDate']		:	$record['emp_visa_issue_date'];
$empVisaExpiryDate		=	(isset($_POST['empVisaExpiryDate']))	?	$_POST['empVisaExpiryDate']		:	$record['emp_visa_expiry_date'];

$empNICIssueDate		=	(isset($_POST['empNICIssueDate']))		?	$_POST['empNICIssueDate']		:	$record['emp_nic_issue_date'];
$empNICExpiryDate		=	(isset($_POST['empNICExpiryDate']))		?	$_POST['empNICExpiryDate']		:	$record['emp_nic_expiry_date'];
$empLabourIssueDate		=	(isset($_POST['empLabourIssueDate']))	?	$_POST['empLabourIssueDate']	:	$record['emp_labour_card_issue_date'];
$empLabourExpiryDate	=	(isset($_POST['empLabourExpiryDate']))	?	$_POST['empLabourExpiryDate']	:	$record['emp_labour_card_expiry_date'];

$empAnnualLeaves		=	(isset($_POST['empAnnualLeaves']))		?	$_POST['empAnnualLeaves']		:	$record['emp_annual_leaves'];
$empSickLeaves			=	(isset($_POST['empSickLeaves']))		?	$_POST['empSickLeaves']			:	$record['emp_sick_leaves'];
$empFlexiLeaves			=	(isset($_POST['empFlexiLeaves']))		?	$_POST['empFlexiLeaves']		:	$record['emp_flexi_leaves'];
$empEduLeaves			=	(isset($_POST['empEduLeaves']))			?	$_POST['empEduLeaves']			:	$record['emp_edu_leaves'];
$empMaternityLeaves		=	(isset($_POST['empMaternityLeaves']))	?	$_POST['empMaternityLeaves']	:	$record['emp_maternity_leaves'];
$empFatherOcupation		=	(isset($_POST['empFatherOcupation']))	?	$_POST['empFatherOcupation']	:	$record['emp_father_occupation'];
$empFatherOrganization		=	(isset($_POST['empFatherOrganization']))	?	$_POST['empFatherOrganization']	:	$record['emp_father_organization'];
$empFatherDesignation		=	(isset($_POST['empFatherDesignation']))	?	$_POST['empFatherDesignation']	:	$record['emp_father_designation'];
$empPreferredLanguages		=	(isset($_POST['empPreferredLanguages']))	?	$_POST['empPreferredLanguages']	:	$record['emp_languages'];
$empExpenses		=	(isset($_POST['empExpenses']))	?	$_POST['empExpenses']	:	$record['emp_expenses'];
$empWillingLocation		=	(isset($_POST['empWillingLocation']))	?	$_POST['empWillingLocation']	:	$record['emp_willing_location'];
?>

<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	<?php if($empJoiningDate != '0000-00-00') { ?>
		$( "#empJoiningDate" ).datepicker( "setDate", "<?php echo $empJoiningDate; ?>" );
	<?php } ?>
	<?php if($empProbationEndDate != '0000-00-00') { ?>
		$( "#empProbationEndDate" ).datepicker( "setDate", "<?php echo $empProbationEndDate; ?>" );
	<?php } ?>
	<?php if($empConfirmationDate != '0000-00-00') { ?>
		$( "#empConfirmationDate" ).datepicker( "setDate", "<?php echo $empConfirmationDate; ?>" );
	<?php } ?>
	<?php if($empVisaIssueDate != '0000-00-00') { ?>
		$( "#empVisaIssueDate" ).datepicker( "setDate", "<?php echo $empVisaIssueDate; ?>" );
	<?php } ?>
	<?php if($empVisaExpiryDate != '0000-00-00') { ?>
		$( "#empVisaExpiryDate" ).datepicker( "setDate", "<?php echo $empVisaExpiryDate; ?>" );
	<?php } ?>
	<?php if($empNICIssueDate != '0000-00-00') { ?>
		$( "#empNICIssueDate" ).datepicker( "setDate", "<?php echo $empNICIssueDate; ?>" );
	<?php } ?>
	<?php if($empNICExpiryDate != '0000-00-00') { ?>
		$( "#empNICExpiryDate" ).datepicker( "setDate", "<?php echo $empNICExpiryDate; ?>" );
	<?php } ?>
	<?php if($empLabourIssueDate != '0000-00-00') { ?>
		$( "#empLabourIssueDate" ).datepicker( "setDate", "<?php echo $empLabourIssueDate; ?>" );
	<?php } ?>
	<?php if($empLabourExpiryDate != '0000-00-00') { ?>
		$( "#empLabourExpiryDate" ).datepicker( "setDate", "<?php echo $empLabourExpiryDate; ?>" );
	<?php } ?>
});
</script>

<form name="frmAddEmploymentDetails" id="frmAddEmploymentDetails" enctype="multipart/form-data" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Employment Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Branch Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empCompany" name="empCompany" class="dropDown">
                  <option value="">Select Branch</option>
                  <?php
                  if (count($empCompanies)) {
                      foreach($empCompanies as $arrCompany) {
                          $selected = '';
                          if($empCompany == $arrCompany['company_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrCompany['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompany['company_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Employee ID:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empID" id="empID" class="textBox" value="<?php echo $empID; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">IP Extension:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empIPNum" maxlength="4" id="empIPNum" class="textBox" value="<?php echo $empIPNum; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Work Email:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empWorkEmail" maxlength="100" id="empWorkEmail" class="textBox" value="<?php echo $empWorkEmail; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Department:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empDepartment" name="empDepartment" class="dropDown">
                  <option value="">Select Department</option>
                  <?php
                  if (count($empDepartments)) {
                      foreach($empDepartments as $departmentVal) {
                          $selected = '';
                          if($empDepartment == $departmentVal['job_category_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $departmentVal['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $departmentVal['job_category_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Designation:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empDesignation" id="empDesignation" class="textBox" value="<?php echo $empDesignation; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Job Location:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empJobLocation" name="empJobLocation" class="dropDown">
                  <option value="">Select Location</option>
                  <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empJobLocation == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                  ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                  <?php
                        }
                    }
                  ?>
              </select> 
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Supervisor:</td>
            <td class="formTextBoxContainer">
        		<select id="empSupervisor" name="empSupervisor[]" class="dropDown" multiple="multiple" style="height:100px">
                  <option value="">Select Supervisor</option>
                  <?php
                  if (count($empSupervisors)) {
                      foreach($empSupervisors as $supervisorVal) {
                          $selected = '';
                          if(in_array($supervisorVal['emp_id'], $empSupervisor)) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $supervisorVal['emp_id']; ?>" <?php echo $selected; ?>><?php echo $supervisorVal['emp_full_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> <br />&nbsp;&nbsp;&nbsp;
              <span style="font-size:12px"><?php echo getSupervisorName($record['emp_id']); ?></span>
            </td>
      	</tr>
         <tr>
            <td class="formLabelContainer">OT Eligibility:</td>
            <td class="formTextBoxContainer">
        		<select id="empOT" name="empOT" class="dropDown">
                  <option value="">Select OT Eligibility</option>
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
              </select>  
            </td>
      	</tr>
         <tr class="formAlternateRow">
            <td class="formLabelContainer">Currency:</td>
            <td class="formTextBoxContainer">
        		<select id="empCurrency" name="empCurrency" class="dropDown">
                  <option value="">Select Currency</option>
                  <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empCurrency == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                  ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name'] . ' - ' . $dataCountry['location_currency']; ?></option>
                  <?php
                        }
                    }
                  ?>
              </select>
            </td>
      	</tr>
        <tr>
            <td class="formHeaderRow" colspan="2">Contract Details</td>
        </tr>
        <tr>
            <td class="formLabelContainer">Contract Type:</td>
            <td class="formTextBoxContainer">
        		 <select id="empEmploymentType" name="empEmploymentType" class="dropDown" tabindex="46">
                  <option value="">Select Contract Type</option>
                  <?php
                  if (count($employmentTypes)) {
                      foreach($employmentTypes as $employmentType) {
                          $selected = '';
                          if($employmentType == $empEmploymentType) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $employmentType; ?>" <?php echo $selected; ?>><?php echo $employmentType; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Employment Type:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empEmploymentStatus" name="empEmploymentStatus" class="dropDown">
                  <option value="">Select Employment Type</option>
                  <?php
                  if (count($empEmploymentStatuses)) {
                      foreach($empEmploymentStatuses as $employmentStatusVal) {
                          $selected = '';
                          if($empEmploymentStatus == $employmentStatusVal['employment_status_id']) {
                              $selected = 'selected="selected"';
                          }
						  
						  if($employmentStatusVal['employment_status_id'] >= 6) {
								  $selected .= ' disabled="disabled"';
							  }
                  ?>
                      <option value="<?php echo $employmentStatusVal['employment_status_id']; ?>" <?php echo $selected; ?>><?php echo $employmentStatusVal['employment_status_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
              <script>$('#lblEmpEmploymentStatus').html($('#empEmploymentStatus').find(":selected").text());</script>
            </td>
      	</tr>
        <tr id="joiningDate">
            <td class="formLabelContainer">Joining Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empJoiningDate" maxlength="30" id="empJoiningDate" value="<?php echo $empJoiningDate; ?>" class="textBox datePicker">
            </td>
      	</tr>
        <tr class="formAlternateRow" id="probationDate">
            <td class="formLabelContainer">Probation End Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empProbationEndDate" maxlength="30" id="empProbationEndDate" value="<?php echo $empProbationEndDate; ?>" class="textBox datePicker">
            </td>
      	</tr>
        <tr id="confirmationDate">
            <td class="formLabelContainer">Confirmation Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empConfirmationDate" maxlength="30" id="empConfirmationDate" value="<?php echo $empConfirmationDate; ?>" class="textBox datePicker">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Remaining Annual Leaves:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
            	<?php
                if($canWrite != YES) {
					$empAnnualLeaves = round($empAnnualLeaves);
				}
				?>
        		 <input type="text" name="empAnnualLeaves" maxlength="4" id="empAnnualLeaves" class="textBox" value="<?php echo $empAnnualLeaves; ?>"> <span style="font-size:12px; font-style:italic; color:#999; margin-left:10px">(Employees on probation may not avail the  annual leaves. However, they can consume once confirmed.)</span>
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Remaining Sick Leaves:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empSickLeaves" maxlength="2" id="empSickLeaves" class="textBox" value="<?php echo $empSickLeaves; ?>">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Remaining Flexi Leaves:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empFlexiLeaves" maxlength="2" id="empFlexiLeaves" class="textBox" value="<?php echo $empFlexiLeaves; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Remaining Educational/Training Leaves:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empEduLeaves" maxlength="2" id="empEduLeaves" class="textBox" value="<?php echo $empEduLeaves; ?>">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Remaining Maternity/Paternity Leaves:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empMaternityLeaves" maxlength="2" id="empMaternityLeaves" class="textBox" value="<?php echo $empMaternityLeaves; ?>">
            </td>
      	</tr>
        <!-- <tr>
            <td class="formLabelContainer">Branch Name:</td>
            <td class="formTextBoxContainer">
        		 <select id="empVisaCompany" name="empVisaCompany" class="dropDown">
                  
              </select>
            </td>
      	</tr> -->
        <!-- <tr class="formAlternateRow">
            <td class="formLabelContainer">Transfer Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empVisaIssueDate" maxlength="30" id="empVisaIssueDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Next Transfer Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empVisaExpiryDate" maxlength="30" id="empVisaExpiryDate" class="textBox datePicker">
            </td>
      	</tr>-->
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Identification Document Issue Date (EID/CNIC):</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empNICIssueDate" maxlength="30" id="empNICIssueDate" class="textBox datePicker" value="<?php echo $empNICIssueDate; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Identification Document Expiry Date (EID/CNIC):</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empNICExpiryDate" maxlength="30" id="empNICExpiryDate" class="textBox datePicker" value="<?php echo $empNICExpiryDate; ?>">
            </td>
      	</tr>
        <!-- <tr class="formAlternateRow">
            <td class="formLabelContainer">Labour Card Issue Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empLabourIssueDate" maxlength="30" id="empLabourIssueDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Labour Card Expiry Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="date" name="empLabourExpiryDate" maxlength="30" id="empLabourExpiryDate" class="textBox datePicker">
            </td>
      	</tr>-->
        <tr>
            <td class="formLabelContainer">Father Occupation Name:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empFatherOcupation" maxlength="30" id="empFatherOcupation" class="textBox" value="<?php echo $empFatherOcupation; ?>">
            </td>
      	</tr> 
        <tr>
            <td class="formLabelContainer">Father Serving Organization Name:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empFatherOrganization" maxlength="30" id="empFatherOrganization" class="textBox" value="<?php echo $empFatherOrganization; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Father Desgination:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empFatherDesignation" maxlength="30" id="empFatherDesignation" class="textBox" value="<?php echo $empFatherDesignation; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Preferred Languages:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empPreferredLanguages" maxlength="30" id="empPreferredLanguages" class="textBox" value="<?php echo $empPreferredLanguages; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">Monthly Expense:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empExpenses" maxlength="30" id="empExpenses" class="textBox" value="<?php echo $empExpenses; ?>">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Willing to Relocate AnyWhere in Pakistan:</td>
            <td class="formTextBoxContainer">
        		 <select id="empWillingLocation" name="empWillingLocation" class="dropDown">
                  <option value="Yes" <?php echo $empWillingLocation == 'Yes' ? 'Selected' : null ; ?>>Yes</option>
                  <option value="No" <?php echo $empWillingLocation == 'No' ? 'Selected' : null ; ?>>No</option>
              </select>
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Sponsor:</td>
            <td class="formTextBoxContainer">
        		 <select id="empSponsor" name="empSponsor" class="dropDown">
                  <option value="">Select Sponsor</option>
                  <?php
                  if (count($empSponsors)) {
                      foreach($empSponsors as $arrSponsor) {
                          $selected = '';
                          if($empSponsor == $arrSponsor['sponsor_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrSponsor['sponsor_id']; ?>" <?php echo $selected; ?>><?php echo $arrSponsor['sponsor_type']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
        <tr>
            <td class="formHeaderRow" colspan="2">Payroll Details</td>
        </tr>
        <tr class="formAlternateRow">
          <td class="formLabelContainer">Basic Salary :<span class="mandatoryStar">*</span></td>
          <td class="formTextBoxContainer">
            <input type="text" name="empBasicSalary" id="empBasicSalary" class="textBox datePicker" tabindex="39" value="<?php echo $empBasicSalary ?>">
          </td>
        </tr>
        <!-- <tr class="formAlternateRow">
          <td class="formLabelContainer">Total Salary :<span class="mandatoryStar"></span></td>
          <td class="formTextBoxContainer">
            <input type="text" name="empTotalSalary" maxlength="30" id="empTotalSalary" class="textBox datePicker" tabindex="39" value="<?php echo $empTotalSalary ?>">
          </td>
        </tr> -->
        <?php if($canWrite == YES) { ?>
		<tr>      
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">      
    		</td>
        </tr>
        <?php } else { ?>
        	<script>$("#frmAddEmploymentDetails :input").attr("disabled", true).css({"background-color": "#ddd"});</script>
        <?php } ?>
  </table>
  </div>
</form>

<script>
	$("#empOT").val("<?php echo $empOT; ?>");
	$('#empSupervisor').select2();
	function showResponse(value)
	{
		if(value == 1){
			$('#confirmationDateStar').hide();
		}else if((value == 6)||(value == 7)){
			$('#confirmationDateStar').hide();
		} else {
			$('#confirmationDateStar').hide();
		}
	}
	
	showResponse(<?php echo (int)$empEmploymentStatus; ?>);
	
</script>