<?php
$empTitle 				  = (isset($_POST['empTitle'])) 			        ? $_POST['empTitle'] 			        : $record['emp_title'];
$empFirstName 			= (isset($_POST['empFirstName'])) 		      ? $_POST['empFirstName'] 		      : $record['emp_first_name'];
$empSecondName 			= (isset($_POST['empSecondName'])) 		      ? $_POST['empSecondName'] 		    : $record['emp_second_name'];
$empLastName 			  = (isset($_POST['empLastName'])) 		        ? $_POST['empLastName'] 		      : $record['emp_last_name'];
$empFamilyName 			= (isset($_POST['empFamilyName'])) 		      ? $_POST['empFamilyName'] 		    : $record['emp_family_name'];
$empRPTName 			  = (isset($_POST['empRPTName'])) 		        ? $_POST['empRPTName'] 			      : $record['emp_rpt_name'];
$empPassportName 		= (isset($_POST['empPassportName'])) 	      ? $_POST['empPassportName'] 	    : $record['emp_passport_name'];
$empPassportNo 			= (isset($_POST['empPassportNo'])) 		      ? $_POST['empPassportNo'] 		    : $record['emp_passport_num'];
$empPassportExpiry 	= (isset($_POST['empPassportExpiry'])) 	    ? $_POST['empPassportExpiry'] 	  : $record['emp_passport_expiry'];
$empFatherName 			= (isset($_POST['empFatherName'])) 		      ? $_POST['empFatherName'] 		    : $record['emp_father_name'];
$empFatheralive 		= (isset($_POST['empFatheralive'])) 		    ? $_POST['empFatheralive'] 		    : $record['emp_father_alive'];
$empMotherName 			= (isset($_POST['empMotherName'])) 		      ? $_POST['empMotherName'] 		    : $record['emp_mother_name'];
$empSpouseName 			= (isset($_POST['empSpouseName'])) 		      ? $_POST['empSpouseName'] 		    : $record['emp_spouse_name'];
$empEmail 				  = (isset($_POST['empEmail'])) 			        ? $_POST['empEmail'] 			        : $record['emp_email'];
$empPassord 			  = (set_value('passWord') != '') 		        ? set_value('passWord') 		      : $record['password'];
$empNICNo 				  = (isset($_POST['empNICNo'])) 			        ? $_POST['empNICNo'] 			        : $record['emp_nic_num'];
$empGender 				  = (isset($_POST['empGender'])) 			        ? $_POST['empGender'] 			      : $record['emp_gender'];
$empBloodGroup			= (isset($_POST['empBloodGroup'])) 		      ? $_POST['empBloodGroup'] 		    : $record['emp_blood_group'];
$empMaritalStatus 	= (isset($_POST['empMaritalStatus'])) 	    ? $_POST['empMaritalStatus'] 	    : $record['emp_marital_status'];
$empReligion 			  = (isset($_POST['empReligion'])) 		        ? $_POST['empReligion'] 		      : $record['emp_religion_id'];
$empDOB					    = (isset($_POST['empDOB'])) 			          ? $_POST['empDOB'] 				        : $record['emp_dob'];
$empAge					    = (isset($_POST['empAge'])) 			          ? $_POST['empAge'] 				        : $record['emp_age'];
$empplaceofbirth		= (isset($_POST['empplaceofbirth'])) 			  ? $_POST['empplaceofbirth'] 			: $record['emp_place_of_birth'];
$empAddress				  = (isset($_POST['empAddress'])) 		        ? $_POST['empAddress'] 			      : $record['emp_local_address'];
$empPermAddress			= (isset($_POST['empPermAddress'])) 	      ? $_POST['empPermAddress'] 		    : $record['emp_permanent_address'];
$empCountryID 			= (isset($_POST['Country'])) 			          ? $_POST['Country'] 			        : $record['emp_country_id'];
$empNationalityID 	= (isset($_POST['Nationality'])) 		        ? $_POST['Nationality'] 		      : $record['emp_nationality_id'];
$empHomePhone			  = (isset($_POST['empHomePhone'])) 		      ? $_POST['empHomePhone'] 		      : $record['emp_home_telephone'];
$empCellPhone			  = (isset($_POST['empCellPhone'])) 		      ? $_POST['empCellPhone'] 		      : $record['emp_mobile'];
$empStatus 				  = (isset($_POST['empStatus'])) 			        ? $_POST['empStatus'] 			      : $record['user_status'];
$empRegionType 			= (isset($_POST['empRegionType'])) 			    ? $_POST['empRegionType'] 			  : $record['emp_region_id'];
$empPicture 			  = ($record['emp_photo_name'] != '') 	      ? $record['emp_photo_name'] 	    : '';
$empMarriedChildren = ($record['emp_children_married'] != '') 	? $record['emp_children_married'] : '';

// print_r($record);
// exit;
if($empStatus == 0 && $empStatus != '') {
	$empStatus = -1;
}
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#empDOB" ).datepicker( "setDate", "<?php echo $empDOB; ?>" );
	
	$( "#empPassportExpiry" ).datepicker( "setDate", "<?php echo $empPassportExpiry; ?>" );
	$( "#empPassportExpiry" ).datepicker( "option", "minDate", "0" );
	
});
</script>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
<tr>
<td>
<form name="frmAddEmployee" id="frmAddEmployee" enctype="multipart/form-data" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Personal Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Title:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="empTitle" name="empTitle" class="dropDown">
                  <option value="">Select Title</option>
                  <?php
                  if (count($Salutations)) {
                      foreach($Salutations as $salutationVal) {
                          $selected = '';
                          if($empTitle == $salutationVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $salutationVal; ?>" <?php echo $selected; ?>><?php echo $salutationVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">First Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" name="empFirstName" maxlength="50" id="empFirstName" class="textBox" value="<?php echo $empFirstName; ?>">
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Second Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empSecondName" maxlength="50" id="empSecondName" class="textBox" value="<?php echo $empSecondName; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Last Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empLastName" maxlength="50" id="empLastName" class="textBox" value="<?php echo $empLastName; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Family Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empFamilyName" maxlength="50" id="empFamilyName" class="textBox" value="<?php echo $empFamilyName; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">RPT Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empRPTName" maxlength="50" id="empRPTName" class="textBox" value="<?php echo $empRPTName; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">NIC Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empPassportName" maxlength="50" id="empPassportName" class="textBox" value="<?php echo $empPassportName; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">NIC Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empPassportNo" maxlength="50" id="empPassportNo" class="textBox" value="<?php echo $empPassportNo; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Father Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empFatherName" maxlength="50" id="empFatherName" class="textBox" value="<?php echo $empFatherName; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Father Alive:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empFatheralive" maxlength="50" id="empFatheralive" class="textBox" value="<?php echo $empFatheralive; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Mother Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empMotherName" maxlength="50" id="empMotherName" class="textBox" value="<?php echo $empMotherName; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Spouse Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empSpouseName" maxlength="50" id="empSpouseName" class="textBox" value="<?php echo $empSpouseName; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Personal Email:</td>
            <td class="formTextBoxContainer" id="tdEmail">
        		<input type="text" name="empEmail" id="empEmail" class="textBox" value="<?php echo $empEmail; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Date of Birth:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="date" name="empDOB" maxlength="30" id="empDOB" class="textBox datePicker" value="<?php echo $empDOB; ?>" >
            </td>
      	</tr>
		<tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Age<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empAge" maxlength="30" id="empAge" class="textBox datePicker" value="<?php echo $empAge; ?>">
            </td>
      	</tr>
		<tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Place of birth<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empplaceofbirth" maxlength="30" id="empplaceofbirth" class="textBox datePicker" value="<?php echo $empplaceofbirth; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Gender:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empGender" name="empGender" class="dropDown">
                  <option value="">Select Gender</option>
                  <?php
                  if (count($Genders)) {
                      foreach($Genders as $genderVal) {
                          $selected = '';
                          if($empGender == $genderVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $genderVal; ?>" <?php echo $selected; ?>><?php echo $genderVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Marital Status:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empMaritalStatus" name="empMaritalStatus" class="dropDown">
                  <option value="">Select Marital Status</option>
                  <?php
                  if (count($maritalStatuses)) {
                      foreach($maritalStatuses as $maritalVal) {
                          $selected = '';
                          if($empMaritalStatus == $maritalVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $maritalVal; ?>" <?php echo $selected; ?>><?php echo $maritalVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Blood Group:</td>
            <td class="formTextBoxContainer">
            	<select id="empBloodGroup" name="empBloodGroup" class="dropDown">
                  <option value="">Select Blood Group</option>
                  <?php
                  if (count($bloodGroups)) {
                      foreach($bloodGroups as $bloodGroup) {
                          $selected = '';
                          if($empBloodGroup == $bloodGroup) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $bloodGroup; ?>" <?php echo $selected; ?>><?php echo $bloodGroup; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Religion:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
            	<select id="empReligion" name="empReligion" class="dropDown">
                  <option value="">Select Religion</option>
                  <?php
                  if (count($Religions)) {
                      foreach($Religions as $arrReligion) {
                          $selected = '';
                          if($empReligion == $arrReligion['religion_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrReligion['religion_id']; ?>" <?php echo $selected; ?>><?php echo $arrReligion['religion_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Identification Number:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer" id="tdNIC">
        		<input type="text" name="empNICNo" maxlength="20" id="empNICNo" class="textBox" value="<?php echo $empNICNo; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
        <td class="formLabelContainer">Profile Picture:</td>
        <td class="formTextBoxContainer">
        <input type="file" name="empPicture" id="empPicture" />&nbsp;
    <?php
    // print_r($empPicture);exit;
            if(!empty($empPicture) && file_exists($pictureFolder . $empPicture)) {
    ?>
              <input type="hidden" name="picFileName" id="picFileName" value="<?php echo $empPicture; ?>" />
            <?php
    }
    ?>
        </td>
    </tr>
    <tr>
    <td class="formLabelContainer">Region</td>
            <td class="formTextBoxContainer">
            <select id="empRegionType" name="empRegionType" class="dropDown" tabindex="46">
                  <option value="">Select Region Type</option>
                  <?php
                  if (count($empRegions)) {
                      foreach($empRegions as $empRegion ) {
                          $selected = '';
                          if($empRegion['region_id'] == $empRegionType) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $empRegion['region_id']; ?>" <?php echo $selected; ?>><?php echo $empRegion['region_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
    </tr>
    <tr>
        <td class="formHeaderRow" colspan="2">Contact Details</td>
    </tr>
		<tr>
            <td class="formLabelContainer">Local Address:</td>
            <td class="formTextBoxContainer">
        		<textarea rows="5" cols="30" name="empAddress" id="empAddress" class="textArea"><?php echo $empAddress; ?></textarea>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Permanent Address:</td>
            <td class="formTextBoxContainer">
        		<textarea rows="5" cols="30" name="empPermAddress" id="Address" class="textArea"><?php echo $empPermAddress; ?></textarea>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Country:</td>
            <td class="formTextBoxContainer" id="tdlocation">
        		<select id="Country" name="Country" class="dropDown">
                  	<option value="">Select Country</option>
                    <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empCountryID == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Nationality:</td>
            <td class="formTextBoxContainer" id="tdlocation">
        		<select id="Nationality" name="Nationality" class="dropDown">
                  	<option value="">Select Nationality</option>
                    <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empNationalityID == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Home Phone Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empHomePhone" id="empHomePhone" class="textBox" value="<?php echo $empHomePhone; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Cell Phone Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empCellPhone" id="empCellPhone" class="textBox" value="<?php echo $empCellPhone; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Children if Married:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empMarriedChildren" id="empMarriedChildren" class="textBox" value="<?php echo $empMarriedChildren; ?>">
            </td>
      	</tr>
        <?php if(isAdmin($this->userRoleID)) { ?>
		<tr>
            <td class="formLabelContainer">Status:</td>
            <td class="formTextBoxContainer">
            	<select class="dropDown" name="empStatus" id="empStatus">
					<option value="<?php echo STATUS_ACTIVE; ?>">Active</option>
					<option value="<?php echo STATUS_INACTIVE; ?>">InActive</option>
                </select>
      		</td>
    	</tr>
		<tr>
        <?php } else { ?>
		<tr class="formAlternateRow">        
        <?php } ?>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      		<?php if($canWrite == YES) { ?>
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">      
            <?php } ?>
    		</td>
        </tr>
  </table>
  </div>
</form>
</td>
<?php if($ownUser) { ?>
<td valign="top">
<form name="frmPassword" id="frmPassword" enctype="multipart/form-data" method="post">
  <div class="employeeFormMain" style="width:98%; margin-left:10px">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Update Password
            	<input type="hidden" name="changePW" id="changePW" value="1" />
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">New Password:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="password" name="newPassWord" maxlength="16" id="newPassWord" class="textBox" value="">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Confirm Password:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="password" name="confirmPassWord" maxlength="16" id="confirmPassWord" class="textBox" value="">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Old Password:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="password" name="oldPassWord" maxlength="16" id="oldPassWord" class="textBox" value="">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      			<input type="submit" class="smallButton" name="btnSave" value="Update">&nbsp;
        		<input type="button" class="smallButton" id="deletButton" value="Cancel" onclick="history.go(-1)">      
    		</td>
        </tr>
        <input type="hidden" name="currentPassWord" id="currentPassWord" value="<?php echo $empPassord; ?>" />
  </table>
  </div>
</form>
</td>
<?php } ?>
</tr>
</table>
<script>
	$("#empStatus").val('<?php echo $empStatus; ?>');
	<?php if($canWrite == NO) { ?>
	$("#frmAddEmployee :input").attr("disabled", true).css({"background-color": "#ddd"});
	<?php } ?>
	<?php if(!isAdmin($this->userRoleID)) { ?>
		$("#empNICNo").attr("readonly", true).css({"background-color": "#ddd"});
	<?php } ?>
</script>