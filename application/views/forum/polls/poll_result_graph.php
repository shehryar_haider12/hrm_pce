<div class="listPageMain">
    <div class="notificationWideBox">
    
    <div class="pollingHeaderImage" style="background:url('<?php echo $imagePath.'/modules/forum/poll_headers/'.$arrRecord['poll_header'].'.jpg'; ?>');">
    	<span class="" style="width:260px;height:230px;float:right;padding-top:15px;padding-right:10px;">
			<div class="articleHeading paddingAll center bold"><?php echo $arrRecord['poll_topic']; ?></div>
			<br>
            <div class="articleText center">Initiated By: <br><?php echo $arrRecord['emp_full_name']; ?></div>
            <br>
			<div class="articleText center">Started On:	<?php echo readableDate($arrRecord['poll_start_date'], 'M j, Y'); ?></div>
			<div class="articleText center">Last Date:	<?php echo readableDate($arrRecord['poll_end_date'], 'M j, Y'); ?></div>
		</span>
    </div>
    <div class="clear" />
    
    <?php if(count($pollResult) > 0) { 
		for($ind = 0; $ind < count($pollResult); $ind++)
		{			
	?>
    	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
          google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Choices', 'Votes', { role: 'style' }],
              <?php for($jnd = 0; $jnd < count($pollResult[$ind]['choices']); $jnd++) {
				?>
              ['<?php echo ucwords($pollResult[$ind]['choices'][$jnd]['poll_answer']); ?>',  <?php echo (int)$pollResult[$ind]['choices'][$jnd]['poll_count']; ?>,  ''],
              <?php
			  } ?>
            ]);
        
            var options = {
              title: '<?php echo "Question ".($ind+1).": ".$pollResult[$ind]['poll_question']; ?>',
              legend: { position: 'none' },
              isStacked: false,
              bar: {groupWidth: "30%"},
              vAxis: {title: 'Votes',  titleTextStyle: {color: 'red'}},
              hAxis: {title: 'Choices',  titleTextStyle: {color: 'red'}}
            };
				
			var chart = new google.visualization.ColumnChart(document.getElementById('chart_div<?php echo $ind; ?>'));           
            chart.draw(data, options);
          }
        </script>
        
        <div id="chart_div<?php echo $ind; ?>" style="float:left; width: 100%; height: 400px;"></div>
		
	<?php
		}
	} ?>
	</div>
</div>