<?php
$pollTopic				=	(isset($_POST['pollTopic']))				?	$_POST['pollTopic']					:	($arrRecord['poll_topic'] ? $arrRecord['poll_topic'] : '');
$pollHeader				=	(isset($_POST['pollHeader']))				?	$_POST['pollHeader']				:	($arrRecord['poll_header'] ? $arrRecord['poll_header'] : '');
$dateFrom				=	(isset($_POST['dateFrom']))					?	$_POST['dateFrom']					:	($arrRecord['poll_start_date'] ? $arrRecord['poll_start_date'] : '');
$dateTo					=	(isset($_POST['dateTo']))					?	$_POST['dateTo']					:	($arrRecord['poll_end_date'] ? $arrRecord['poll_end_date'] : '');
$pollStatus				=	(isset($_POST['pollStatus']))				?	$_POST['pollStatus']				:	($arrRecord['poll_status'] ? $arrRecord['poll_status'] : '');
$pollVisibilityStatus	=	(isset($_POST['pollVisibilityStatus']))		?	$_POST['pollVisibilityStatus']		:	($arrRecord['poll_visibility_status'] ? $arrRecord['poll_visibility_status'] : '');
$pollResultsDisplay		=	(isset($_POST['pollResultsDisplay']))		?	$_POST['pollResultsDisplay']		:	$arrRecord['poll_results_display'];
$today					=	date('Y-m-d');
?>

<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	<?php if($arrRecord['poll_start_date'] > $today) { ?>
		$( ".datePicker" ).datepicker( "option", "minDate", '0' );
	<?php } else if(!empty($arrRecord['poll_start_date'])) { ?>
		$( ".datePicker" ).datepicker( "option", "minDate", '<?php echo $arrRecord['poll_start_date']; ?>' );
	<?php } else { ?>
		$( ".datePicker" ).datepicker( "option", "minDate", '0' );
	<?php } ?>
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});
</script>

<form name="frmInputPoll" id="frmInputPoll" method="post" action="<?php echo $frmActionURL; ?>">
<div class="employeeFormMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain" id="tblQuestion">
        <?php if((!($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) && ($arrRecord['poll_status'] >= 2)) { ?>
        <tr class="yellow">
            <td colspan="2" class="articleText">
                At this Status, you can not modify. If you require any modification, kindly request to IT Web Department person using <a href="<?php echo $baseURL.'/complain_management/submit_complain';  ?>"> Complain Management System </a>.
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td class="formHeaderRow" colspan="2">Create/Edit Poll</td>
        </tr>
        <tr>
            <td class="formLabelContainer">Poll Topic:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <textarea rows="2" cols="30" name="pollTopic" id="pollTopic" class="textArea" style="width:360px"><?php echo $pollTopic; ?></textarea>
            </td>                
        </tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Poll Header:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
            	<div>
                	<?php
						$directory = $pollHeadersFolder;
						if (glob($directory. "*.jpg") != false)
						{
						 $fileCount = count(glob($directory . "*.jpg"));						 
						 
						 $files = glob($directory. "*.jpg");
						 
						 for($i = 0; $i < $fileCount; $i++)
						 {
							 $explodedArray = explode('/',$files[$i]);
							 $fileName = $explodedArray[5];
							 $explodedFileName = explode('.',$fileName);
							 $explodedFileName = $explodedFileName[0];
							 
							 if ($explodedFileName == $pollHeader)
							 { $selectedHeader = "checked=checked"; }
							 else if(($i == 0) && empty($pollHeader)) 
							 { $selectedHeader = "checked=checked"; } 
							 else $selectedHeader = "";
						?>
                        	<?php if($i != 0 && $i % 2 == 0) {  ?>
                            	<div class="clear" />
                            <?php } ?>
                                <span>
                                    <input type="radio" name="pollHeader" value="<?php echo $explodedFileName; ?>" style="vertical-align:top" <?php echo $selectedHeader; ?> />
                                    <img class="paddingRightFive" src="<?php echo $imagePath.'/modules/forum/poll_headers/'.$fileName; ?>" height="58px" width="300px" />
                                </span>                            						
						<?php
						 }
						}
						else
						{
						 echo 0;
						}
					?>              
                </div>                
            </td>                
        </tr>
        <tr>
            <td class="formLabelContainer">Poll For Department:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <?php if((!($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) && ($arrRecord['poll_status'] >= 2)) 
                    { $disabled = 'disabled="disabled"'; } ?>
                <select id='pollDepartment' name="pollDepartment[]" <?php echo $disabled; ?> multiple='multiple'>
                    <?php $selectedAll = "";
							if(isset($_POST['pollDepartment'])) 
							{ 
								for($i = 0; $i < count($_POST['pollDepartment']); $i++)
                                {
                                    if($_POST['pollDepartment'][$i] == 0)
                                    {$selectedAll = "selected";}
                                }
							} 
							else if($arrRecordDepartments)
                            {
                                for($i = 0; $i < count($arrRecordDepartments); $i++)
                                {
                                    if($arrRecordDepartments[$i]['poll_department_id'] == 0)
                                    {$selectedAll = "selected";}
                                }
                             } 
					?>
                    <?php if(isAdmin($this->userRoleID)) { ?>
                    	<option value="0" <?php echo $selectedAll; ?>>All</option>
                    <?php } ?>
					<?php
                    if (count($empDepartments)) {
                        foreach($empDepartments as $empDepartment) {
                            $selected = "";
                            if(isset($_POST['pollDepartment']))
                            {
                                for($i = 0; $i < count($_POST['pollDepartment']); $i++)
                                {
                                    if($_POST['pollDepartment'][$i] == $empDepartment['job_category_id'])
                                    {$selected = "selected";}
                                }
                             }
                            else if($arrRecordDepartments)
                            {
                                for($i = 0; $i < count($arrRecordDepartments); $i++)
                                {
                                    if($arrRecordDepartments[$i]['poll_department_id'] == $empDepartment['job_category_id'])
                                    {$selected = "selected";}
                                }
                             }
                    ?>
                        <option value="<?php echo $empDepartment['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $empDepartment['job_category_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <script>$('#pollDepartment').multiSelect();</script>
            
        </tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Poll For Designation:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="pollDesignation" name="pollDesignation[]" <?php echo $disabled; ?> multiple='multiple'>
                	<?php 	$selectedAll = "";
							if(isset($_POST['pollDesignation'])) 
							{
								for($i = 0; $i < count($_POST['pollDesignation']); $i++)
                                {
                                    if($_POST['pollDesignation'][$i] == 0)
                                    {$selectedAll = "selected";}
                                }
							} 
							else if($arrRecordRoles)
                            {
                                for($i = 0; $i < count($arrRecordRoles); $i++)
                                {
                                    if($arrRecordRoles[$i]['poll_role_id'] == 0)
                                    {$selectedAll = "selected";}
                                }
                             } 
					?>
                    <?php if(isAdmin($this->userRoleID)) { ?>
                    	<option value="0" <?php echo $selectedAll; ?>>All</option>
                    <?php } ?>
					<?php
                    if (count($userRoles)) {
                        foreach($userRoles as $userRole) {
                            $selected = "";
                            if(isset($_POST['pollDesignation']))
                            {
                                for($i = 0; $i < count($_POST['pollDesignation']); $i++)
                                {
                                    if($_POST['pollDesignation'][$i] == $userRole['user_role_id'])
                                    {$selected = "selected";}
                                }
                             }
                             else if($arrRecordRoles)
                            {
                                for($i = 0; $i < count($arrRecordRoles); $i++)
                                {
                                    if($arrRecordRoles[$i]['poll_role_id'] == $userRole['user_role_id'])
                                    {$selected = "selected";}
                                }
                             }
                    ?>
                        <option value="<?php echo $userRole['user_role_id']; ?>" <?php echo $selected; ?>><?php echo $userRole['user_role_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <script>$('#pollDesignation').multiSelect();</script>
        </tr>
        <tr>
            <td class="formLabelContainer">Poll Start Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" class="textBox datePicker" name="dateFrom" id="dateFrom" value="<?php echo $dateFrom; ?>" />
            </td>                
        </tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Poll End Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" class="textBox datePicker" name="dateTo" id="dateTo" value="<?php echo $dateTo; ?>" />
            </td>
        </tr>
        <tr>
            <td class="formLabelContainer">Poll Status:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="pollStatus" name="pollStatus" class="dropDown">
                    <option value="">Select Poll Status</option>
                    <?php
                    if (count($arrPollStatus)) {
						for($i=1; $i <= count($arrPollStatus); $i++)
						{
							if($i > 2)
							{ 
								if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)
								{ $statusVisibility = ''; } else { $statusVisibility = 'disabled="disabled"'; }
							}
                    ?>
                       			<option value="<?php echo $i; ?>" <?php echo $statusVisibility; ?>><?php echo $arrPollStatus[$i]; ?></option>
                    <?php
						}
                    }
                    ?>
                    
                 </select>
            </td>
        </tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Results Display:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
            	<?php if(empty($pollResultsDisplay)) { $checked = ""; } else if(($pollResultsDisplay == 1) || ($pollResultsDisplay == 0)) { $checked = "checked=checked"; } else { $checked = ""; } ?>
                <?php
				if (count($arrPollResultsDisplay)) {
					foreach($arrPollResultsDisplay as $key => $value) {
						if($pollResultsDisplay == '') { $checked = ""; }
						else if($pollResultsDisplay == $key) { $checked = "checked=checked"; }
						else { $checked = ""; }
				?>
                	<input type="radio" name="pollResultsDisplay" value="<?php echo $key; ?>" <?php echo $checked; ?> /> <span class="formLabelContainer"><?php echo $value; ?></span> <br />
                <?php
                    }
                }
                ?>
            </td>            
        </tr>        
        <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
        <tr>
            <td class="formLabelContainer">Poll Visibility Status :<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="pollVisibilityStatus" name="pollVisibilityStatus" class="dropDown">
                    <option value="">Select Poll Visibility Status</option>
                    <?php
                    if (count($arrPollVisibilityStatus)) {
                        foreach($arrPollVisibilityStatus as $key => $value) {
                    ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php
                        }
                    }
                    ?>
                 </select>
            </td>
        </tr>
        <?php } ?>        
        <tr>
            <td class="formLabelContainer"></td>
			<td class="formTextBoxContainer">
            	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">
				<input class="smallButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/polls'; ?>';">
            </td>
        </tr>
    </table>        
</div>
</form>

<script>
	$('#pollVisibilityStatus').val('<?php echo $pollVisibilityStatus; ?>');
	$('#pollStatus').val('<?php echo $pollStatus; ?>');
</script>

<?php if((!($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) && ($arrRecord['poll_status'] >= 2)) { ?>
	<script>$("#frmInputPoll :input").attr("disabled", true); $('#btnSave').hide();</script>
<?php } ?>