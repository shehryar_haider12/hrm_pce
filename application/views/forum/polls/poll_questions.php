<?php
$pollQuestion = (isset($_POST['pollQuestion'])) ? $_POST['pollQuestion'] : ($arrQuestionRecord['poll_question'] ? $arrQuestionRecord['poll_question'] : '');
?>
<?php if($arrRecord['poll_status'] < 2 || $this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
<form name="frmInputPollQuestion" id="frmInputPollQuestion" method="post" action="<?php echo $frmActionURL; ?>">
<div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Create/Edit Poll Question</td>
        </tr>
        <tr>
        	<td class="formLabelContainer">Question Type:<span class="mandatoryStar"> *</span></td>
        	<td class="formTextBoxContainer">
            	<a onclick="radioFunction('radio')">
					<span id="radio" class="radio"></span>
                </a>
                <a onclick="radioFunction('check')">
                	<span id="check" class="check"></span>
                </a>
                </span>
                <input type="hidden" id="pollChoiceType" name="pollChoiceType" value="" />
            </td>
        </tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Question:<span class="mandatoryStar"> *</span></td>
			<td class="formTextBoxContainer">
                <textarea rows="3" cols="30" name="pollQuestion" id="pollQuestion" class="textArea"><?php echo $pollQuestion; ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="formLabelContainer">Choices:<span class="mandatoryStar"> *</span></td>
        	<td class="formTextBoxContainer">
            	<table id="tblChoice" border="0" cellpadding="0" cellspacing="0">
                	<?php if($this->input->post('pollChoice')) 
					{
                    	for($ind = 0; $ind < count($this->input->post('pollChoice')); $ind++) { ?>
                            
                            <tr id="trChoice<?php echo $ind + 1; ?>">
                            	<?php if($ind <= 2) { ?>
                                	<td colspan="2"><input type="text" name="pollChoice[]" class="textBox" value="<?php echo set_value('pollChoice[]'); ?>" /></td>
                                <?php } else { ?>
                                	<td><input type="text" name="pollChoice[]" class="textBox" value="<?php echo set_value('pollChoice[]'); ?>" /></td>
                                    <td><img  class="anchor paddingLeftFive" src="<?php echo $imagePath.'/remove.png'; ?>" onclick="removeChoice('trChoice<?php echo $ind + 1; ?>'); return false;" /></td>
                                <?php } ?>
                            </tr>      
                    <?php 
						}
					} else if($arrQuestionRecord['choices']) 
					{
						for($ind = 0; $ind < count($arrQuestionRecord['choices']); $ind++) { ?>
					
                    	<tr id="trChoice<?php echo $ind + 1; ?>">
							<?php if($ind <= 2) { ?>
                                <td colspan="2"><input type="text" name="pollChoice[]" class="textBox" value="<?php echo $arrQuestionRecord['choices'][$ind]['poll_answer']; ?>" /></td>
                            <?php } else { ?>
                                <td><input type="text" name="pollChoice[]" class="textBox" value="<?php echo $arrQuestionRecord['choices'][$ind]['poll_answer']; ?>" /></td>
                                <td><img  class="anchor paddingLeftFive" src="<?php echo $imagePath.'/remove.png'; ?>" onclick="removeChoice('trChoice<?php echo $ind + 1; ?>'); return false;" /></td>
                            <?php } ?>
                        </tr> 
					
					<?php
						}
					} else { ?>
                	<tr id="trChoice1">
                    	<td colspan="2"><input type="text" name="pollChoice[]" class="textBox"/></td>
                    </tr>
                    <tr id="trChoice2">
                    	<td colspan="2"><input type="text" name="pollChoice[]" class="textBox"/></td>
                    </tr>
                    <tr id="trChoice3">
                    	<td colspan="2"><input type="text" name="pollChoice[]" class="textBox"/></td>
                    </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
        <tr>
			<td class="formLabelContainer"></td>
			<td class="formTextBoxContainer">
                <input type="hidden" name="tblChoices" id="tblChoices" value="<?php echo $tblChoices; ?>" />
                <?php if($arrQuestionRecord['poll_question']) { ?>
                <input class="smallButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' .$pollID; ?>';">
                <?php } ?>
                <input type="button" class="smallButton" value="Add Choice" onclick="addChoice('tblChoice'); return false;">
                <input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">
            </td>
        </tr>
	</table>
</div>
</form>
<?php } ?>

<?php if(count($pollQuestionAnswers) > 0) { ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
    <?php if((!($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) && ($arrRecord['poll_status'] >= 2)) { ?>
    <tr class="yellow">
        <td colspan="3" class="articleText">
            At this Status, you can not add/modify Question. If you require any modification, kindly request to IT Web Department person using <a href="<?php echo $baseURL.'/complain_management/submit_complain';  ?>"> Complain Management System </a>.
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td class="formHeaderRow" colspan="3">Poll Questions & Choices</td>
    </tr>
    <tr class="listHeader" align="center">
    	<td class="listHeaderCol" width="440px">Questions</td>
        <td class="listHeaderCol" width="460px">Choices</td>
        <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID || $arrRecord['poll_status'] < 2) { ?>
			<?php if(($canWrite == 1) || ($canDelete == 1))  { ?>
            <td class="listHeaderColLast" width="60px">Action</td>
            <?php } ?>
        <?php } ?>
    </tr>
    <?php for($i=0; $i < count($pollQuestionAnswers); $i++) { 
		$rowClass = ($i % 2 == 0) ? "" : "formAlternateRow" ;
	?>
    <tr class="<?php echo $rowClass; ?>">
    	<td class="listContentCol subHeadingLabel"><?php echo $pollQuestionAnswers[$i]['poll_question']; ?></td>
        <td class="listContentCol subHeadingLabel">
        	<table  border="0" cellspacing="0" cellpadding="0">
			<?php for($j=0; $j < count($pollQuestionAnswers[$i]['choices']); $j++) 
			{ 
				if($pollQuestionAnswers[$i]['poll_answer_type'] == 1) { $imageType = "radio-button.png"; }
				else if($pollQuestionAnswers[$i]['poll_answer_type'] == 2) { $imageType = "check-button.png"; }
			?>
                <tr>
                	<td><img src="<?php echo $imagePath.'/'.$imageType; ?>" /></td>
                    <td valign="top" class="paddingLeftFive"><?php echo $pollQuestionAnswers[$i]['choices'][$j]['poll_answer']; ?></td>
                </tr>
            <?php } ?>
            </table>
        </td>
        <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID || $arrRecord['poll_status'] < 2) { ?>
            <td class="listContentColLast" align="center">                
                <img title="View/Edit" class="anchor" width="25px" height="25px" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/poll_questions/' . $pollID . '/' .$pollQuestionAnswers[$i]['poll_question_id']; ?>';" />
				<?php if($canDelete == 1) { ?>                
				<img title="Delete" class="anchor" width="25px" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/poll_questions/'.$pollID; ?>', '<?php echo $pollQuestionAnswers[$i]['poll_question_id']; ?>');" />
                <?php } ?>
            </td>
        <?php } ?>
    </tr>
    <?php } ?>
</table>
<?php } ?>

<script>

<?php if(($this->input->post('pollChoiceType') == '1') || ($arrQuestionRecord['poll_answer_type'] == 1)) { ?>
		$("#radio").attr("class", "radio");
		$("#check").attr("class", "checkDisabled");
		$("#pollChoiceType").val('1');
<?php	} else if(($this->input->post('pollChoiceType') == '2')  || ($arrQuestionRecord['poll_answer_type'] == 2)) { ?>
		$("#check").attr("class", "check");
		$("#radio").attr("class", "radioDisabled");
		$("#pollChoiceType").val('2');
<?php } ?> 

function radioFunction(value)
{
	if(value == 'radio')
	{
		$("#radio").attr("class", "radio");
		$("#check").attr("class", "checkDisabled");
		$("#pollChoiceType").val('1');
	} 
	else if(value == 'check')
	{
		$("#check").attr("class", "check");
		$("#radio").attr("class", "radioDisabled");
		$("#pollChoiceType").val('2');
	}
}

function addChoice(elmID) {
	var totalInt = $('#tblChoices').val();
	var strHTML = '<tr id="trChoice'+(parseInt(totalInt) + 1)+'"><td><input type="text" name="pollChoice[]" class="textBox" /></td>';
	$('#' + elmID).append(strHTML + '<td><img  class="anchor paddingLeftFive" src="<?php echo $imagePath.'/remove.png'; ?>" onclick="removeChoice(\'trChoice'+(parseInt(totalInt) + 1)+'\'); return false;" /></td></tr>');
	$('#tblChoices').val(parseInt(totalInt) + 1);	
}

function removeChoice(elmID) {
	var totalInt = $('#tblChoices').val();
	totalInt = parseInt(totalInt) - 1;
	$('#' + elmID).remove();
	$('#tblChoices').val(parseInt(totalInt));	
}

$(".notification").each(function() {
	$( this ).tipsy({gravity: "n", title: "original-title", trigger: "manual", className: "tipsy-resignation"});
	$( this ).tipsy("show");
});
</script>