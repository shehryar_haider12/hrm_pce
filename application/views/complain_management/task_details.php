<?php
$taskStatus			=	(isset($_POST['taskStatus']))		?	$_POST['taskStatus']		:	'';
$taskResponse		=	(isset($_POST['taskResponse'])) 	? 	$_POST['taskResponse'] 	:	'';
$taskTransferTo		=	(isset($_POST['taskTransferTo']))	?	$_POST['taskTransferTo']	:	'';

$showForm = true;
$webAdminResponse = NO;
?>
<div class="midMain">
	<div style="float:right; padding-bottom:5px">
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">
    </div>
    <div style="clear:both"></div>
    <div class="interviewHistoryMain">
        <div class="intervieweeMain">
            <div class="interviewee">
            	<?php echo nl2br($arrParentRecord['task_description']); ?>                
                <?php if(!empty($arrParentRecord['task_supporting_doc'])) { ?>   
                	<br /><br />             
					<?php if (strpos($arrParentRecord['task_supporting_doc'], ',') !== false) { 
						$explodedDocs = explode(",",$arrParentRecord['task_supporting_doc']);
						for($i=0; $i < count($explodedDocs); $i++) {
					?>
                    	<a class="normalLink" href="<?php echo base_url().$docFolderTasks.$explodedDocs[$i]; ?>" target="_blank">View Supporting Document <?php echo $i+1; ?></a>
                        <br />
                    <?php } 
					} else { ?>
                    	<a class="normalLink" href="<?php echo base_url().$docFolderTasks.$arrParentRecord['task_supporting_doc']; ?>" target="_blank">View Supporting Document</a>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="intervieweeIcon">
                <div class="intervieweeIconImage">
                	<?php
					$parentEmpId = $arrParentRecord['emp_id'];
					$lastRepliedBy = $arrParentRecord['task_last_replied_by'];
					$parentReportById = $arrParentRecord['reported_by_id'];
					$empName = '';
					
					if(!file_exists($pictureFolder . $arrParentRecord['emp_photo_name']) || empty($arrParentRecord['emp_photo_name']))
					{
						if(empty($arrParentRecord['emp_gender'])) {
							$arrParentRecord['emp_gender'] = 'male';
						}
						$arrParentRecord['emp_photo_name'] = 'no_image_' . strtolower($arrParentRecord['emp_gender']) . '.jpg';
					}
					
					$empName = $arrParentRecord['reported_by'];
										
					if(($this->userEmpNum != $arrParentRecord['reported_by_id']) && ($this->userEmpNum != $arrParentRecord['reported_to_id']) && ($this->userEmpNum != $lastRepliedBy) && (in_array($this->userRoleID, $forcedAccessRoles)))
					{
						$showForm = true;	
						$webAdminResponse = YES;					
					} 
					else if(($this->userEmpNum == $arrParentRecord['reported_by_id']) || ($this->userEmpNum == $arrParentRecord['reported_to_id']))
					{
						if(($this->userEmpNum == $lastRepliedBy)){
							$showForm = false;
						} else if($arrParentRecord['task_status'] <= IN_PROGRESS) {
							$showForm = true;							
						} else {
							$showForm = false;
						}
						
					} else {
						$showForm = false;
					}
					
					?>
                    <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrParentRecord['emp_photo_name']; ?>" alt="" width="60" />
                </div>
                <div class="intervieweeIconName"><b><?php echo $empName; ?></b>            
                <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrParentRecord['created_date'])); ?></div>
            </div>
        </div>
    
    <?php if(count($arrRecords)) { ?>
    
		<?php 
        
        for($ind = 0; $ind < count($arrRecords); $ind++) 
        {
            $empName = '';
			
			if($arrRecords[$ind]['emp_id'] == $parentEmpId)
            {
				$rowCss = 'interviewee';
                
                $empName = $arrRecords[$ind]['emp_full_name'];
				
				if(!file_exists($pictureFolder . $arrRecords[$ind]['emp_photo_name']) || empty($arrRecords[$ind]['emp_photo_name']))
				{
					if(empty($arrRecords[$ind]['emp_gender'])) {
						$arrRecords[$ind]['emp_gender'] = 'male';
					}

					$arrRecords[$ind]['emp_photo_name'] = 'no_image_' . strtolower($arrRecords[$ind]['emp_gender']) . '.jpg';
				}
                
            } else 
            {
				$rowCss = 'reporter';
                
                $empName = $arrRecords[$ind]['emp_full_name'];
				
				if(!file_exists($pictureFolder . $arrRecords[$ind]['emp_photo_name']) || empty($arrRecords[$ind]['emp_photo_name']))
				{
					if(empty($arrRecords[$ind]['emp_gender'])) {
						$arrRecords[$ind]['emp_gender'] = 'male';
					}
					$arrRecords[$ind]['emp_photo_name'] = 'no_image_' . strtolower($arrRecords[$ind]['emp_gender']) . '.jpg';
				}
				
            }
                  
        ?>
        
        	<div class="<?php echo $rowCss.'Main'; ?>">
            	<div class="<?php echo $rowCss; ?>">
					<?php echo nl2br($arrRecords[$ind]['task_history_description']); ?>
                   	<?php if(!empty($arrRecords[$ind]['task_supporting_doc'])) { ?>
                    	<br /><br />
						<?php if (strpos($arrRecords[$ind]['task_supporting_doc'], ',') !== false) { 
                            $explodedNestedDocs = explode(",",$arrRecords[$ind]['task_supporting_doc']);
                            for($i=0; $i < count($explodedNestedDocs); $i++) {
                        ?>
                            <a class="normalLink" href="<?php echo base_url().$docFolderTasks.$explodedNestedDocs[$i]; ?>" target="_blank">View Supporting Document <?php echo $i+1; ?></a>
                            <br />
                        <?php } unset($explodedNestedDocs);
                        } else { ?>
                            <a class="normalLink" href="<?php echo base_url().$docFolderTasks.$arrRecords[$ind]['task_supporting_doc']; ?>" target="_blank">View Supporting Document</a>
                        <?php } ?>
                    <?php } ?>
                </div>
                 <div class="<?php echo $rowCss.'Icon'; ?>">
                	<div class="<?php echo $rowCss.'IconImage'; ?>">
                    	<img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrRecords[$ind]['emp_photo_name']; ?>" alt="" width="60" />
                    </div>
                    <div class="<?php echo $rowCss.'IconName'; ?>"><b><?php echo $empName; ?></b>
                    <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrRecords[$ind]['created_date'])); ?></div>
                 </div>
            </div>
    
    	<?php } ?>
    
    </div>
    <?php } ?>
    
</div>

<div style="clear:both">&nbsp;</div>
<?php if($showForm) { ?>
<form name="frmTaskResponse" id="frmTaskResponse" method="post" enctype="multipart/form-data">
  <div class="listPageMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Submit Response</td>
    </tr>
    <?php if($parentEmpId == $this->userEmpNum) { ?>
     <tr>
      <td class="formLabelContainer">Task Status:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select id="taskStatus" name="taskStatus" class="dropDown">
                <option value="">Select Status</option>
            <?php
            if (count($taskStatuses)) {
                foreach($taskStatuses as $key => $value) {
					if($key > OPEN) {
            ?>
                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php
					}
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <?php } ?>    
    <tr id="rowTaskResponse">
    	<td class="formLabelContainer">Response:<span class="mandatoryStar"> *</span></td>
        <td class="formTextBoxContainer">
        	<textarea name="taskResponse" id="taskResponse" class="textArea" style="width:400px; height:125px"><?php echo $taskResponse; ?></textarea>
        </td>
        <input type="hidden" id="parentReportById" name="parentReportById" value="<?php echo $parentReportById; ?>" />
        <?php if($webAdminResponse == YES) { ?>
        	<input type="hidden" id="webAdminResponse" name="webAdminResponse" value="<?php echo $webAdminResponse; ?>" />
        <?php } ?>
    </tr>
    <tr>
    	<td class="formLabelContainer">Supportive Document (if any):</td>
        <td class="formTextBoxContainer">
        	<input type="file" name="supportingDoc[]" id="supportingDoc" multiple="multiple" />
            <br />
            [Add multiple files by using Ctrl function of keyboard]
        </td>
    </tr>
    <?php if($parentEmpId != $this->userEmpNum) { ?>
    <tr>
    	<td class="formLabelContainer">Do you want this Task to transfer to another person?</td>
        <td class="formTextBoxContainer">
        	<input type="checkbox" id="isTransfer" name="isTransfer" value="1" onclick="isTransferFunction()" />
        </td>
    </tr>
    <tr style="display:none" id="rowTaskTransferTo">
      <td class="formLabelContainer">Task Transfer To:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select id="taskTransferTo" name="taskTransferTo" class="dropDown">
            <option value="">Select One</option>
            <?php
            if (count($arrReportToEmployees)) {
                foreach($arrReportToEmployees as $key => $arrReportToEmployee) {											
            ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($i=0;$i<count($arrReportToEmployee);$i++) { ?>
                            <?php if($arrReportToEmployee[$i]['emp_id'] != $this->userEmpNum && $arrReportToEmployee[$i]['emp_id'] != $arrParentRecord['emp_id'] && $arrReportToEmployee[$i]['emp_id'] != $arrParentRecord['task_report_to_id']) { ?>
                                <option value="<?php echo $arrReportToEmployee[$i]['emp_id']; ?>"><?php echo $arrReportToEmployee[$i]['emp_full_name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </optgroup>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <?php } ?>
    <tr>
      <td class="formLabelContainer">
      </td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" id="deletButton" name="btnSave" id="btnSave" value="Submit">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } ?>

<script>
	function isTransferFunction()
	{
		//$('#rowTaskResponse').toggle();
		$('#rowTaskTransferTo').toggle();
	}
	<?php if((isset($_POST['isTransfer']))) { ?>
		isTransferFunction();
		$('#isTransfer').attr('checked',true);
	<?php }?> 
</script>