<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: '';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: '';

$path = '/'.$this->currentController.'/'.$this->currentAction;
$complainType	 = ($complainType != '') ? $complainType : $this->input->post('complainType');
$complainPriority = ($complainPriority != '') ? $complainPriority : $this->input->post('complainPriority');
$complainReportTo = ($complainReportTo != '') ? $complainReportTo : $this->input->post('complainReportTo');
$status = ($status != '') ? $status : $this->input->post('status');

?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "setDate", "<?php echo $dateReported; ?>" );
});
</script>
<form name="frmListIssues" id="frmListIssues" method="post" action="<?php echo $frmActionURL; ?>">
<div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    
    <div class="searchcontentmain">		
         <div class="searchCol">
            <div class="labelContainer">Ticket Type:</div>
            <div class="textBoxContainer">
                <select id="complainType" name="complainType" class="dropDown">
				<option value="">All</option>
				 <?php
				if (count($complainTypes)) {
					foreach($complainTypes as $key => $value) {
				?>
					<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				<?php
					}
				}
				?>
                </select>
            </div>         
            <div class="labelContainer">Priority:</div>
            <div class="textBoxContainer">
                <select id="complainPriority" name="complainPriority" class="dropDown">
                    <option value="">All</option>
                     <?php
                    if (count($complainPriorities)) {
                        foreach($complainPriorities as $key => $value) {
                    ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="labelContainer">Status:</div>
            <div class="textBoxContainer">
                <select id="status" name="status" class="dropDown">
				<option value="">All</option>
				 <?php
				if (count($complainStatuses)) {
					foreach($complainStatuses as $key => $value) {
				?>
					<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				<?php
					}
				}
				?>
                </select>
            </div>
        </div>
        
        <div class="searchCol"> 
            <?php if(isAdmin($this->userRoleID)) { ?>
            <div class="labelContainer">Reported To:</div>
            <div class="textBoxContainer">
                <select id="complainReportTo" name="complainReportTo" class="dropDown">
                    <option value="">All</option>
                    <?php
					if (count($arrReportToEmployees)) {
						foreach($arrReportToEmployees as $key => $arrReportToEmployee) {
					?>
						<optgroup label="<?php echo $key; ?>">
							<?php for($i=0;$i<count($arrReportToEmployee);$i++) { ?>					
								<option value="<?php echo $arrReportToEmployee[$i]['emp_id']; ?>"><?php echo $arrReportToEmployee[$i]['emp_full_name']; ?></option>
							<?php } ?>
						</optgroup>
					<?php	}
					}
					?>
                </select>
            </div>
            <div class="labelContainer">Reported By:</div>
            <div class="textBoxContainer">
                <select id="complainReportedBy" name="complainReportedBy" class="dropDown">
                    <option value="">All</option>
                    <?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmployee) {
					?>
						<optgroup label="<?php echo $key; ?>">
							<?php for($i=0;$i<count($arrEmployee);$i++) { ?>					
								<option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_full_name']; ?></option>
							<?php } ?>
						</optgroup>
					<?php	}
					}
					?>
                </select>
            </div>
            <?php } ?> 
            <div class="labelContainer">Reported Date:</div>
            <div class="textBoxContainer">
                <input type="text" class="textBox datePicker" name="dateReported" id="dateReported" />
            </div> 
        </div>
        
        <div class="formButtonContainerWide">
            <input type="submit" class="searchButton" name="btnSearchComplains" id="btnSearchComplains" value="Search">
        </div>
    </div>
</div>

<script>
  	$('#complainType').val('<?php echo $complainType; ?>');
  	$('#complainPriority').val('<?php echo $complainPriority; ?>');
  	$('#complainReportTo').val('<?php echo $complainReportTo; ?>');
  	$('#complainReportedBy').val('<?php echo $complainReportedBy; ?>');
	$('#status').val('<?php echo $status; ?>');
  </script>

<div class="centerElementsContainer">
    <div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Legends:</b>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#f9f084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Suggestions</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#FFF;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Complains</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#E5E5E5;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Request</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#CCE5A6;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Complete </span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#f38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Incomplete </span>
        &nbsp;&nbsp;&nbsp;<img src="<?php echo $this->imagePath . '/urgent.png' ?>" /><span class="mandatoryStar"> Urgent </span>
        &nbsp;&nbsp;&nbsp;<img src="<?php echo $this->imagePath . '/important.png' ?>" /><span class="mandatoryStar"> Important </span>
        &nbsp;&nbsp;&nbsp;<img src="<?php echo $this->imagePath . '/normal.png' ?>" /><span class="mandatoryStar"> Normal </span>
        </div>
    <?php
    if($pageLinks) {
    ?>
        <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
    <?php 	}	?>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<?php if(isAdmin($this->userRoleID)) { ?>
        	<td class="listHeaderCol" width="40px">ID</td>
        <?php } ?>
        <td class="listHeaderCol" width="150px">Type</td>
        <td class="listHeaderCol" width="155px">Reported By</td>
        <td class="listHeaderColClickable" onclick="setSort('created_date', 'frmListIssues')" width="80px">Date</td>
        <td class="listHeaderCol" width="155px">Reported To</td>
        <td class="listHeaderCol" width="310px">Message</td>
        <td class="listHeaderColClickable" onclick="setSort('complain_status', 'frmListIssues')" width="65px">Status</td>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast" width="215px">Action</td>
		<?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		$bgColor = '';
		
		if($arrRecords[$ind]['complain_type_id'] == TYPE_SUGGESTION) {
			$issueType = 'Suggestion';
			$bgColor = 'style="background-color:#f9f084"';
		} else if($arrRecords[$ind]['complain_type_id'] == TYPE_COMPLAIN) {
			$issueType = 'Complain';
		} else if($arrRecords[$ind]['complain_type_id'] == TYPE_REQUEST) {
			$issueType = 'Request';
			$bgColor = 'style="background-color:#E5E5E5"';
		}
		
		if($arrRecords[$ind]['complain_status'] == RESOLVED) {
			$bgColor = 'style="background-color:#CCE5A6"';
		} else if($arrRecords[$ind]['complain_status'] == UNRESOLVED) {
			$bgColor = 'style="background-color:#f38374"';
		}
		
		if(!$arrRecords[$ind]['complain_status']) {
			$bgColor = 'style="background-color:#B9D1B8"';
		}
		
		$arrWhere = array(
			'complain_id' => $arrRecords[$ind]['complain_id'],
			'emp_id' => $this->userEmpNum
		);
		
		$issueUnChecked = getIssuesUnChecked($arrWhere);
		
		$tipsyID = '';
		if((int)$issueUnChecked){
			$tipsyID = 'class="notification" id="'.$arrRecords[$ind]['complain_id'].'notification"';
		}
		
		if(!$ind) {
			$empID = $arrRecords[$ind]['emp_id'];
		}
		
		$isAnonymous = $arrRecords[$ind]['is_anonymous'];
		
		if(($isAnonymous)){
			$reportedBy = 'Anonymous';
		} else {
			$reportedBy = $arrRecords[$ind]['reported_by'];
		}
		
		$reportedTo = $arrRecords[$ind]['reported_to'];				
		?>
    <tr class="listContent" <?php echo $bgColor; ?>>
    	<?php if(isAdmin($this->userRoleID)) { ?>
    		<td class="listContentCol"><?php echo $arrRecords[$ind]['complain_id']; ?></td>
        <?php } ?>
        <td class="listContentCol">
			<?php if($arrRecords[$ind]['complain_priority'] == URGENT_PRIORITY) { ?>
            	<img src="<?php echo $this->imagePath . '/urgent.png' ?>" align="top" />
            <?php } else if($arrRecords[$ind]['complain_priority'] == IMPORTANT_PRIORITY) { ?>
            	<img src="<?php echo $this->imagePath . '/important.png' ?>" align="top" />
            <?php } else if($arrRecords[$ind]['complain_priority'] == NORMAL_PRIORITY) { ?>
            	<img src="<?php echo $this->imagePath . '/normal.png' ?>" align="top" />
            <?php } ?>
			<?php echo $issueType; ?><span <?php echo $tipsyID; ?> original-title="awaiting"></span></td>
        <td class="listContentCol">
			<?php echo $reportedBy; ?>
			<?php if($reportedBy == 'Anonymous') { ?>
				<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { echo "<br>".$arrRecords[$ind]['reported_by']; } ?>
            <?php } ?>
            <?php if(($reportedBy != 'Anonymous') || ($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) 
			{ 
				echo "<br>IP: <b>".$arrRecords[$ind]['emp_ip_num'] . '</b>';
				echo "<br>Code: <b>".$arrRecords[$ind]['emp_code'] . '</b>';
			}
			?>
        </td>
        <td class="listContentCol">
			<?php echo readableDate($arrRecords[$ind]['created_date'], 'M j, Y'); ?>
        </td>
        <td class="listContentCol">
			<?php echo $reportedTo; ?>
            <?php if($reportedTo == 'Anonymous') { ?>
				<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { echo "<br>".$arrRecords[$ind]['reported_to']; } ?>
            <?php } ?>
        </td>
        <td class="listContentCol" style="padding: 10px 0;"><?php echo substr($arrRecords[$ind]['complain_text'], 0, 200); echo (strlen($arrRecords[$ind]['complain_text']) > 199) ? ' ....' : ''; ?></td>
        <?php if($arrRecords[$ind]['complain_status'] == OPEN) 
				$issueStatus = 'Open'; 
			else if($arrRecords[$ind]['complain_status'] == IN_PROGRESS) 
				$issueStatus = 'In Progress';
			else if($arrRecords[$ind]['complain_status'] == RESOLVED) 
				$issueStatus = 'Complete';
			else if($arrRecords[$ind]['complain_status'] == UNRESOLVED) 
				$issueStatus = 'Incomplete'; 
		?>
    	<td class="listContentCol"><?php echo $issueStatus; ?></td>
        <?php if(($canWrite == YES) || ($canDelete == YES)) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == YES) {?>
            <?php if(($arrRecords[$ind]['complain_status'] == OPEN) || ($arrRecords[$ind]['complain_status'] == IN_PROGRESS)) 
					{ $text = "View / Submit Response"; } 
					else if(($arrRecords[$ind]['complain_status'] == RESOLVED) || ($arrRecords[$ind]['complain_status'] == UNRESOLVED))
					{ $text = "View Details"; } ?>
        	<input type="button" class="smallButton" value="<?php echo $text; ?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/complain_details/' . $arrRecords[$ind]['complain_id']; ?>';" />
            <?php } if($canDelete == YES) { ?>
            <input type="button" class="smallButton" id="deletButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['complain_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
</form>

<script>	
$(".notification").each(function() {
	$( this ).tipsy({gravity: "w", title: "original-title", trigger: "manual"});
	$( this ).tipsy("show");
});
</script>