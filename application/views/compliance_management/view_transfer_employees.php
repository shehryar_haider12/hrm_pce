<script>
$(document).ready(function () {
      $('.select-state').selectize({
          sortField: 'text'
      });
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;

	}
	.item{
		width: 100%;
	} 
/* .selectize-input.items.has-options.full.has-items{
	height: 35px !important;
}
.item{
	width: 100%;
} */
</style>
<?php

	// print_r($arrRecords);
	// exit;
$leaveCategory 		= (isset($_POST['leaveCategory'])) 						? $_POST['leaveCategory'] 					: '';
$leaveStatus 		= (isset($_POST['leaveStatus'])) 						? $_POST['leaveStatus'] 					: '';
$leaveMonth 		= (isset($_POST['leaveMonth'])) 						? $_POST['leaveMonth'] 						: '';
$leaveYear 			= (isset($_POST['leaveYear'])) 							? $_POST['leaveYear'] 						: '';
$empSickLeaves			=	(isset($_POST['empSickLeaves']))		?	$_POST['empSickLeaves']			:	$record['emp_sick_leaves'];
if($leaveStatus == 0 && $leaveStatus != '') {
	$leaveStatus = -1;
}
?>
<form name="frmTranferStatus" id="frmTranferStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
	<hr>
    <div class="searchcontentmain">
      <!-- <div class="searchCol">
        <div class="labelContainer">Leaves Of:</div>
        <div class="textBoxContainer">
        	<select id="select-state" name="tansferMonth" class="dropDown select-state" style="width:85px">
            	<option value="">Month</option>
            	<option value="01">Jan</option>
            	<option value="02">Feb</option>
            	<option value="03">Mar</option>
            	<option value="04">Apr</option>
            	<option value="05">May</option>
            	<option value="06">Jun</option>
            	<option value="07">Jul</option>
            	<option value="08">Aug</option>
            	<option value="09">Sep</option>
            	<option value="10">Oct</option>
            	<option value="11">Nov</option>
            	<option value="12">Dec</option>
          	</select>&nbsp;&nbsp;&nbsp;&nbsp;
        	<select id="select-state" name="tansferYear" class="dropDown select-state" style="width:85px; margin-left:5px">
            	<option value="">Year</option>
            	<?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
            	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
				<?php } ?>
          	</select>
        </div>
      </div> -->
	  <div class="searchCol">
        <div class="labelContainer">Select Region:</div>
        <div class="textBoxContainer">
      	<select name="region" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($regions)) {
				  for($ind = 0; $ind < count($regions); $ind++) {
          ?>
					<option value="<?php echo $regions[$ind]['region_id']; ?>">
			<?php echo $regions[$ind]['region_name']; ?></option>
			 <?php
				  }
			}
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Current Branch:</div>
        <div class="textBoxContainer">
      	<select name="branch_from" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($empTransferRequest[0]['company_to_id']))
            {
              echo $empTransferRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div>
	  <!-- <div class="searchCol">
        <div class="labelContainer">Branch To:</div>
        <div class="textBoxContainer">
      	<select name="branch_to" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($empTransferRequest[0]['company_to_id']))
            {
              echo $empTransferRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div> -->
	  <div class="searchCol">
        <div class="labelContainer">Select Employee:</div>
        <div class="textBoxContainer">
      	<select name="emp_id" class="dropDown select-state" id="select-state"  style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($all_employees)) {
				  for($ind = 0; $ind < count($all_employees); $ind++) {
            if($all_employees[$ind]['emp_id'] != $employee_branch[0]['emp_id'])
            {
			  ?>
				  <option value="<?php echo $all_employees[$ind]['emp_id']; ?>" 
          <?php
            if(isset($empTransferRequest[0]['emp_id']))
            {
              echo $empTransferRequest[0]['emp_id'] == $all_employees[$ind]['emp_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $all_employees[$ind]['emp_code'].' --- '.$all_employees[$ind]['emp_full_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div>
	  <!-- <div class="searchCol">
        <div class="labelContainer">Transfer Status:</div>
        <div class="textBoxContainer">
      	<select name="transferStatus" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>             -->
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>
    </div>
  </div>
  <script>
  	// $('#leaveCategory').val('<?php echo $leaveCategory; ?>');
  	// $('#leaveStatus').val('<?php echo $leaveStatus; ?>');
  	// $('#leaveMonth').val('<?php echo $leaveMonth; ?>');
  	// $('#leaveYear').val('<?php echo $leaveYear; ?>');
  </script>
</form>

<!-- <div class="centerButtonContainer">
	<input class="addButton" type="button" value="Request Transfer" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/transfer_requests' ?>';" />
</div> -->
    <?php
	// print_r($arrRecords);
	// exit;
	?>
<!-- <div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php 
		// echo "Total Records Count: " . count($arrRecords);
		 ?>
		
		<b>Legends:</b>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F9F084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#D9FFA0;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Approved</span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Rejected</span>
    </div>	
</div> -->
<?php
if($pageLinks) {
?>
<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
<?php 	}	?>
		
	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">ID</td>
    <td class="listHeaderCol">Employee Code</td>
    <td class="listHeaderCol">Full Name</td>
    <td class="listHeaderCol" style="max-width:140px;min-width:140px">Current Branch</td>
    <td class="listHeaderCol" style="max-width:140px;min-width:140px">Last Transfer From</td>
    <td class="listHeaderCol" style="max-width:150px;min-width:150px">Processed By</td>
    <td class="listHeaderCol" style="max-width:150px;min-width:150px">Last Transfer Date</td>
    <td class="listHeaderCol" style="max-width:150px;min-width:150px">Region</td>
    <!-- <td class="listHeaderCol" style="max-width:150px;min-width:150px">Last Transfer Date</td> -->
    <td class="listHeaderColLast" align="center">Action/Status</td>
  </tr>
  <?php
//   print_r($arrRecords);
//   exit;
    for($ind = 0; $ind < count($arrRecords); $ind++) {
			
		$arrSupervisors 	= $this->model_transfer_management->getSupervisors($arrRecords[$ind]['emp_id']);
		$supers 	= $arrSupervisors;
		$super_key 	= 'supervisor_emp_id';

		$super_id =array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
			}, $supers);
		
		if((in_array($this->userRoleID, $this->arrRoleIDs)) || ( $arrRecords[$ind]['emp_id'] != $this->userEmpNum && $this->userEmpNum != $arrRecords[$ind]['emp_id'])) {	 
	
		// if((in_array($this->userRoleID, $this->arrRoleIDs) || $this->userRoleID == $super_visor_roleid)  || ($arrRecords[$ind]['emp_id'] == $this->userEmpNum){
		
		$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
		
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_id']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_code']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['company_to']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['company_from']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['processed_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['processed_date']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['region_name']; ?></td>
  	<td class="listContentColLast" align="center">
    	&nbsp;&nbsp;
		<img title="Detials" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/education_normal.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/view_employee_transfer_details/' . $arrRecords[$ind]['emp_id']; ?>';">
        &nbsp;&nbsp;
		
    </td>
  </tr>
  <?php
	} 
	} 

	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="12" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/education_normal.png';?>" /></td>
            <td width="265px">Details <span style="font-style:italic;color:#666">(Can only view transfers detials)</span></td>
        </tr>
    </table>
</div>
