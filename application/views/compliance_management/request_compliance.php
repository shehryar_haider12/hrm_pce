<script>
$(document).ready(function () {
      $('.select-state').selectize({
          sortField: 'text'
      });
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;
	}
      .selectize-control.dropDown.single {
            width: 192px !important;
      }
	.item{
		width: 100%;
	} 
      .formTextBoxContainer div{
		height: auto !important;
	}
	.selectize-input{
		border: none;
	}
</style>

<?php
$empID 	= (isset($_POST['empID'])) 	? $_POST['empID']    	: $emploanRequest['emp_id'];
$deduction_title 	= (isset($_POST['deduction_title'])) 	? $_POST['deduction_title']  	: $emploanRequest['title'];
$deduction_price 	= (isset($_POST['deduction_price'])) 	? $_POST['deduction_price']  	: $emploanRequest['price'];
$deductionReason 	= (isset($_POST['deductionReason'])) 	? $_POST['deductionReason']  	: $emploanRequest['reason'];
?>

<?php if($canWrite == 1) { ?>
<form name="frmdeductioncctv" id="frmdeductioncctv" method="post" enctype="multipart/form-data">
	<div class="listPageMain">
	<div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
			<tr>
				<td class="formHeaderRow" colspan="2">deduction Application</td>
			</tr>
		<tr>
			<td class="formLabelContainer">Employee:</td>
			<td class="formTextBoxContainer" colspan="7">
				<select name="empID" id="empID" class="dropDown select-state">
					<option value="">Select Employee</option>
					<?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmp) {
							?>
						<optgroup label="<?php echo $key; ?>">
							<?php for($i = 0; $i < count($arrEmp); $i++) { ?>
								<option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_code'];?> - <?php echo $arrEmp[$i]['emp_full_name'];?></option>
							<?php } ?>
						</optgroup>
					<?php	}
					}
					?>
				</select>
			</td>
		</tr>
		<tr class="formAlternateRow">
			<td class="formLabelContainer">Title:<span class="mandatoryStar"> *</span></td>
			<td class="formTextBoxContainer">
			<?php if(isset($deduction_title)){ ?>
				<input id="deduction_title" name="deduction_title"  value="<?php echo $deduction_title ?>" />
			<?php }else{ ?>
				<input id="deduction_title" name="deduction_title"   />
			<?php } ?>
			</td>
		</tr>
		<tr class="formAlternateRow">
			<td class="formLabelContainer">Price:<span class="mandatoryStar"> *</span></td>
			<td class="formTextBoxContainer">
			<?php if(isset($deduction_title)){ ?>
				<input id="deduction_price" name="deduction_price"  value="<?php echo $deduction_price ?>" />
			<?php }else{ ?>
				<input id="deduction_price" name="deduction_price"   />
			<?php } ?>
			</td>
		</tr>
		<tr>
			<td class="formLabelContainer">Reason:<span class="mandatoryStar"> *</span></td>
			<td class="formTextBoxContainer">
			<?php
				if(isset($deductionReason))
				{ ?>
					<input name="deductionReason"style="height:150px;border: 1px solid #80808094 !important;" value="<?php echo $deductionReason; ?>">
					<?php 
				}else{
					?>
						<input name="deductionReason"style="height:150px;border: 1px solid #80808094 !important;">
				<?php }?>
			</td>
			</tr>
			<tr class="formAlternateRow">
			<td class="formLabelContainer">Add Supporting Document (If Any):</td>
			<td class="formTextBoxContainer">
				<input type="file" name="deductionDoc" id="deductionDoc">
			</td>
		</tr>
		<tr>
			<td class="formLabelContainer"></td>
			<td class="formTextBoxContainer">
				<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Submit">&nbsp;
				<input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
			</td>
		</tr>
	</table>
	</div>
  </div>
</form>
<br  />
<?php } ?>