<?php
$path 				= '/'.$this->currentController.'/'.$this->currentAction;
$currentMonth		= (int)date('m');
$currentYear		= date('Y');
$month 				= ($month != '') 	? $month 	: ($this->input->post('month') ? $this->input->post('month') : ($this->input->post('selectedMonth')));
$year 				= ($year != '') 	? $year 	: ($this->input->post('year') ? $this->input->post('year') : (($this->input->post('selectedYear')) ? $this->input->post('selectedYear') : (int)date('Y')));

$empID 				= ($empID != '') 	? $empID 	: ($this->input->post('empID') ? $this->input->post('empID') : (($this->input->post('selectedEmp')) ? $this->input->post('selectedEmp') : null));

$txt0				= (isset($_POST['txt0'])) 			? $_POST['txt0'] 			: json_decode($arrTasks[0]['review_kpis']);
$jobKPIScore		= (isset($_POST['jobKPIScore'])) 	? $_POST['jobKPIScore'] 	: $arrTasks[0]['review_kpis_score'];
$vsKPIScore			= (isset($_POST['vsKPIScore'])) 	? $_POST['vsKPIScore'] 		: $arrTasks[0]['review_vs_kpis_score'];
$reviewRemarks		= (isset($_POST['reviewRemarks'])) 	? $_POST['reviewRemarks'] 	: $arrTasks[0]['review_remarks'];

$arrRadVS 			= array();

if(count($arrTasks)) {
	
	$arrAssessment 	= json_decode($arrTasks[0]['review_vs_kpis']);
	$arrRadVS[]		= $arrAssessment[0];
	$arrRadVS[]		= $arrAssessment[1];
	$arrRadVS[]		= $arrAssessment[2];
	$arrRadVS[]		= $arrAssessment[3];
	$arrRadVS[]		= $arrAssessment[4];
	$arrRadVS[]		= $arrAssessment[5];
	$arrRadVS[]		= $arrAssessment[6];
	$arrRadVS[]		= $arrAssessment[7];
	$arrRadVS[]		= $arrAssessment[8];
	$arrRadVS[]		= $arrAssessment[9];
	
} else if(count($_POST['txt0'])) {
	
	$arrRadVS[]			= $_POST['rad_vs_0'];
	$arrRadVS[]			= $_POST['rad_vs_1'];
	$arrRadVS[]			= $_POST['rad_vs_2'];
	$arrRadVS[]			= $_POST['rad_vs_3'];
	$arrRadVS[]			= $_POST['rad_vs_4'];
	$arrRadVS[]			= $_POST['rad_vs_5'];
	$arrRadVS[]			= $_POST['rad_vs_6'];
	$arrRadVS[]			= $_POST['rad_vs_7'];
	$arrRadVS[]			= $_POST['rad_vs_8'];
	$arrRadVS[]			= $_POST['rad_vs_9'];
	
}

$showStatusTable = false;
?>

<!-- SEARCH PANEL - START -->

<div class="listPageMain">
<form name="frmSearchTasks" id="frmSearchTasks" method="post" action="<?php echo $frmActionURL; ?>">
	<div class="searchBoxMain">
		<div class="searchHeader">KPI Assessment <?php if(isset($_POST['month']) && ($validation_error == false)) { ?> - <?php echo date('F', mktime(0, 0, 0, $month, 10)) . ' ' .$year; ?><?php } ?></div>
		<div class="searchcontentmain">
			<div class="searchCol">
				<div class="labelContainer">Employee:</div>
				<div class="textBoxContainer">
                <select name="empID" id="empID" class="dropDown">
                  <option value="">Select Employee</option>
                  <?php
                    if (count($arrEmployees)) {
                        foreach($arrEmployees as $key => $arrEmp) {
                    ?>
                        <optgroup label="<?php echo $key; ?>">
                            <?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
                                <option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php	}
                    }
                    ?>
              </select>
              	</div>
				<div class="labelContainer">Month:</div>
				<div class="textBoxContainer">
                <select class="dropDown" id="month" name="month">
					<option value="">Month</option>
					<option value="6">June</option>
					<option value="12">December</option>
                      <option value="1">Jan</option>
                     <option value="2">Feb</option>
                   
                </select>
              	</div>
				<div class="labelContainer">Year:</div>
                <div class="textBoxContainer">
                <select id="year" name="year" class="dropDown">
                    <option value="">Year</option>
                    <?php for($ind = $this->HRMYearStarted; $ind <= (date('Y') + 1); $ind++) { ?>
                    	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
            
            <div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
				<?php if(count($arrTasks)) { ?>
                <input type="button" class="searchButton" name="btnExport" id="btnExport" value="Export to PDF" onClick="window.location.href = '<?php echo base_url() . $this->currentController . '/my_kpis'; ?>/<?php echo $arrTasks[0]['review_id']; ?>'">
                <?php } ?>
            </div>
		</div>
<script>
	$('#empID').val('<?php echo $empID; ?>');
	$('#month').val('<?php echo $month; ?>');
	$('#year').val('<?php echo $year; ?>');
</script> 
    </div>
</form>
</div>

<!-- SEARCH PANEL - END -->

<!-- GUIDELINE PANEL - START -->
    
<div class="listPageMain">
    <div class="searchBoxMain">
        <div class="yellow" style="padding-left:20px"><br />
            <b>INSTRUCTIONS FOR COMPLETING THIS FORM:</b><br /><br />
            <ul style="list-style:circle; padding-left:30px">
				<li>Unsatisfactory (U)</li>
				<li>Needs Improvement (NI)</li>
				<li>Meets Expectations (ME)</li>
				<li>Exceeds Expectations (EE)</li>
				<li>Excellent (E)</li>
			</ul><br />
        </div>
    </div>
</div>

<div class="listContentMain" style="height:auto">
<form name="frmAddTasks" id="frmAddTasks" method="post" action="<?php echo $frmActionURL; ?>">
	<input type="hidden" id="selectedEmp" name="selectedEmp" value="<?php echo $empID; ?>" />
	<input type="hidden" id="selectedMonth" name="selectedMonth" value="<?php echo $month; ?>" />
    <input type="hidden" id="selectedYear" name="selectedYear" value="<?php echo $year; ?>" />
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px 170px;">
              
		<?php if(!(int)$_POST['empID'] || !(int)$_POST['month'] || !(int)$_POST['year']) { ?>
        <tr class="listContentAlternate">
        	<td align="center" class="listContentCol articleText green">
            	To start adding the KPIs, please select specific Employee, Month &amp; Year from above panel and Click Search Button.
            </td>
        </tr>
		<?php } else { ?>
        
        <!-- POST RECORDS START -->
        <tr>
        	<td>
                <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
                    <tr class="listHeader">
                        <td class="listHeaderCol center" colspan="6">
                            KPI PERFORMANCE REVIEW FORM
                        </td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol" width="25%">Employee</td>
                        <td class="listContentCol" width="25%"><?php echo $arrEmployee['emp_full_name']; ?></td>
                        <td class="listContentCol" width="25%">Line Manager</td>
                        <td class="listContentCol" width="25%"><?php echo getSupervisorName($arrEmployee['emp_id']); ?></td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol">Designation</td>
                        <td class="listContentCol"><?php echo $arrEmployee['emp_designation']; ?></td>
                        <td class="listContentCol">Department</td>
                        <td class="listContentCol"><?php echo $arrEmployee['job_category_name']; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<table border="1" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px" id="table0">
                    <tr class="listHeader">
                        <td width="95%" colspan="6" align="center">Job Specific KPIs (60%)</td>
                        <td align="right">
                            <input type="button" class="smallButton" value="Add Row" onclick="addKPIRow()" />
                        </td>
                    </tr>
                    <tr class="listHeader">
                        <td align="center" width="5%">No.</td>
                        <td align="center" width="70%">Key Performance Indicators</td>
                        <td align="center" width="5%">U</td>
                        <td align="center" width="5%">NI</td>
                        <td align="center" width="5%">ME</td>
                        <td align="center" width="5%">EE</td>
                        <td align="center" width="5%">E</td>
                    </tr>
                    <tr>
                        <td colspan="7">&nbsp;</td>
                    </tr>
                <?php
                $totalRecords = count($txt0);
				if(!$totalRecords) {
					$totalRecords = 4;
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                	<tr class="listContent">
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="70%"><textarea rows="2" style="width: 99%" name="txt0[]" class="textArea"><?php echo $txt0[$ind][0]; ?></textarea></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_5" value="5"></td>
                    </tr>
                    <script>$('input:radio[name="rad<?php echo $ind; ?>"][value="<?php echo $txt0[$ind][1]; ?>"]').attr('checked',true);</script>
                <?php
				}
				?>
                	<input type="hidden" name="hid0" id="hid0" value="<?php echo ($ind + 1); ?>" />
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="1" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr class="listHeader">
                        <td colspan="7" align="center">PCE KPIs (40%)</td>
                    </tr>
                    <tr class="listHeader">
                        <td align="center" width="5%">No.</td>
                        <td align="center" width="70%">Key Performance Indicators</td>
                        <td align="center" width="5%">U</td>
                        <td align="center" width="5%">NI</td>
                        <td align="center" width="5%">ME</td>
                        <td align="center" width="5%">EE</td>
                        <td align="center" width="5%">E</td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">1</td>
                        <td align="center" width="70%">Client interaction and satisfaction</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">2</td>
                        <td align="center" width="70%">Team interaction and teamwork</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">3</td>
                        <td align="center" width="70%">Initiative for process improvement</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">4</td>
                        <td align="center" width="70%">On time delivery of work</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">5</td>
                        <td align="center" width="70%">Accuracy of work delivered</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">6</td>
                        <td align="center" width="70%">Punctuality</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">7</td>
                        <td align="center" width="70%">Dependability</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">8</td>
                        <td align="center" width="70%">Interaction with manager</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">9</td>
                        <td align="center" width="70%">General Conduct / Presentable / Professionalism</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">10</td>
                        <td align="center" width="70%">Adaptability to Change</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_5" value="5"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <script>
		<?php for($ind = 0; $ind < count($arrRadVS); $ind++) { ?>
			$('input:radio[name="rad_vs_<?php echo $ind; ?>"][value="<?php echo $arrRadVS[$ind]; ?>"]').attr('checked',true);
		<?php } ?>
		</script>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr>
                        <td width="10%"><b>Job KPIs</b></td>
                        <td width="10%" align="center">60%</td>
                        <td width="80%" align="left"><input type="text" name="jobKPIScore" id="jobKPIScore" value="<?php echo $jobKPIScore; ?>"></td>
                    </tr>
                    <tr>
                        <td><b>PCE KPIs</b></td>
                        <td align="center">40%</td>
                        <td align="left"><input type="text" name="vsKPIScore" id="vsKPIScore" value="<?php echo $vsKPIScore; ?>"></td>
                    </tr>
                    <tr>
                        <td><b>Total</b></td>
                        <td align="center">100%</td>
                        <td align="left"><input type="text" name="totalKPIScore" id="totalKPIScore" value="<?php echo ( ((60/100)*$jobKPIScore) + ((40/100)*$vsKPIScore)) ; ?>"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <!-- POST RECORDS END -->

	<?php if($arrTasks[0]['review_status_id'] > STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) { ?>
        <tr>
            <td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                	<tr>
                    	<td>
        <div class="intervieweeMain">
            <div class="interviewee">
                <?php echo nl2br("<b>Employee's Comments: </b><br />".$arrTasks[0]['review_emp_remarks']); ?>
            </div>
            <div class="intervieweeIcon">
                <div class="intervieweeIconImage">
                <?php
                if(!file_exists($pictureFolder . $arrEmployee['emp_photo_name']) || empty($arrEmployee['emp_photo_name']))
                    {
                        if(empty($arrEmployee['emp_gender'])) {
                            $arrEmployee['emp_gender'] = 'male';
                        }
                        $arrEmployee['emp_photo_name'] = 'no_image_' . strtolower($arrEmployee['emp_gender']) . '.jpg';
                    }
                ?>
                <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrEmployee['emp_photo_name']; ?>" alt="" width="60" />
                </div>
                <div class="intervieweeIconName">
                    <b><?php echo $arrEmployee['emp_full_name']; ?></b>
                    <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrTasks[0]['review_emp_remarks_date'])); ?>
                </div>
            </div>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
    <?php } ?>
    
	<?php if((int)$arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS || (int)$arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) { 
	?>
    <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr class="listHeader">
                        <td align="center">Manager Comments/Recommendations</td>
                    </tr>
                    <tr>
                       <td>&nbsp;</td>
                    </tr>
                    <tr>
                       <td><textarea name="reviewRemarks" id="reviewRemarks" rows="10" style="width:99%"><?php echo $reviewRemarks; ?></textarea></td>
                    </tr>
                </table>
            </td>
        </tr>
    <?php
	}
	else if($arrTasks[0]['review_status_id'] == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS || $arrTasks[0]['review_status_id'] > STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) { ?>
        <tr>
            <td>
            <table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
            <tr>
            <td>
        <div class="intervieweeMain">
            <div class="interviewee">
                <?php echo nl2br("<b>Supervisor's Comments: </b><br />".$arrTasks[0]['review_remarks']); ?>
            </div>
            <div class="intervieweeIcon">
                <div class="intervieweeIconImage">
                <?php
                if(!file_exists($pictureFolder . $arrReviewerDetails['emp_photo_name']) || empty($arrReviewerDetails['emp_photo_name']))
                    {
                        if(empty($arrReviewerDetails['emp_gender'])) {
                            $arrReviewerDetails['emp_gender'] = 'male';
                        }
                        $arrReviewerDetails['emp_photo_name'] = 'no_image_' . strtolower($arrReviewerDetails['emp_gender']) . '.jpg';
                    }
                ?>
                <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrReviewerDetails['emp_photo_name']; ?>" alt="" width="60" />
                </div>
                <div class="intervieweeIconName">
                    <b><?php echo $arrReviewerDetails['emp_full_name']; ?></b>
                    <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrTasks[0]['review_remarks_date'])); ?>
                </div>
            </div>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
    <?php } ?>
    
    
    
	<?php if(((int)$arrTasks[0]['review_status_id'] < STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS || (int)$arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) && isset($_POST['empID']) && isset($_POST['month']) && isset($_POST['year'])) { ?>
        <tr class="headerRow">
            <td>
                <select class="dropDown" id="selStatus" name="selStatus" onchange="setButtonName(this.value);" style="display:none">
                    <option value="">Select Status</option>
                    <?php for($ind = 0; $ind < 4; $ind++) { ?>
                        <option value="<?php echo $ind; ?>"><?php echo $performanceStatus[$ind]; ?></option>
                    <?php } ?>
                </select>&nbsp;
                <input type="submit" class="smallButton" name="btnAddTasks" id="btnAddTasks" value="Save" onclick="return confirmation('Save')">
                <input type="submit" class="smallButton" id="deletButton" name="btnSubmitTasks" id="btnSubmitTasks" value="Submit" onclick="return confirmation('Submit')">
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
    	<script>$('#selStatus').val('<?php echo $selStatus; ?>'); </script>
        
    <?php } else { ?>
    	<script>
			$("#frmAddTasks :input").attr("disabled", true).css({"background-color": "#ddd"});
			<?php if((int)$arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) { ?>
			$('#reviewRemarks').attr("disabled", false).css({"background-color": "#fff"});
			<?php } ?>
        </script>
    <?php } ?>
    
		<?php if($arrTasks[0]['review_status_id'] == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) { ?>
        <tr class="listContentAlternate">
            <td colspan="4" align="center" class="listContentCol bold">
              KPI's for the year <?php echo  $arrTasks[0]['review_year']; ?> has been submitted by your Line Manager.
            </td>
        </tr>
        <?php } else if($arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) { ?>
        <tr class="listContentAlternate">
            <td colspan="4" align="center" class="listContentCol bold">
                KPI assessment has been submitted for supervisor's remarks.
            </td>
        </tr>
        <?php } else if($arrTasks[0]['review_status_id'] == STATUS_KPIS_SUBMITTED_TO_HR) { ?>
        <tr class="listContentAlternate">
            <td colspan="4" align="center" class="listContentCol bold">
                KPI assessment has been submitted to HR.
            </td>
        </tr>
        <?php }
		} ?>
    </table>
</form>
</div>

<table id="tKPITemplate" style="display:none">
	<tr class="listContent">
        <td width="5%">#.</td>
        <td width="70%"><textarea rows="2" style="width: 99%" name="txt0[]" class="textArea"><?php echo $txt0[$ind]; ?></textarea></td>
        <td align="center" width="5%"><input type="radio" name="rad|" id="rad__1" value="1"></td>
        <td align="center" width="5%"><input type="radio" name="rad|" id="rad__2" value="2"></td>
        <td align="center" width="5%"><input type="radio" name="rad|" id="rad__3" value="3"></td>
        <td align="center" width="5%"><input type="radio" name="rad|" id="rad__4" value="4"></td>
        <td align="center" width="5%"><input type="radio" name="rad|" id="rad__5" value="5"></td>
    </tr>
</table>

<script>
function confirmation(val)
{
	<?php if ((int)$arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS) { ?>
		
		if(val == 'Save')
		{
			$('#selStatus').val(<?php echo STATUS_SUPERVISOR_ENTERING_KPIS; ?>);
			alert("You are going to save this KPI assessment.\nPlease note that you may add or modify the details until you submit it for employee's remarks.");
			return true;
		}
		else if(val == 'Submit')
		{
			$('#selStatus').val(<?php echo STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS; ?>);
			if (confirm("Are you sure you want to submit KPI assessment for employee's remarks?"))
				return true;
		}
	<?php } else if ((int)$arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) { ?>
		if(val == 'Save')
		{
			$('#selStatus').val(<?php echo STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS; ?>);
			alert("You are going to save this KPI assessment.\nPlease note that you may add or modify the remarks.");
			return true;
		}
		else if(val == 'Submit')
		{
			$('#selStatus').val(<?php echo STATUS_KPIS_SUBMITTED_TO_HR; ?>);
			if (confirm("Are you sure you want to submit KPI assessment to HR?"))
				return true;
		}
	<?php } ?>
	return false;
}

function setButtonName(status)
{
	if(status == 1)
		$("#btnAddTasks").val('Submit');
	else
		$("#btnAddTasks").val('Save');
}

function addKPIRow() {
	var totalRows = $('#hid0').val();
	var strTemplate = $('#tKPITemplate').html();
	strTemplate = strTemplate.replace('#', totalRows);
	var totalRowsIndex = totalRows - 1;
	strTemplate = strTemplate.split('|').join(totalRowsIndex);
	totalRows++;
	$('#table0').append(strTemplate);
	$('#hid0').val(totalRows);
}
</script>