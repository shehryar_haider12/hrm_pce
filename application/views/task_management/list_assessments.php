<?php
$empCode 	= (isset($_POST['empCode'])) 	? 	$_POST['empCode'] 		: 	$this->employeeID;
$selStatus 	= (isset($_POST['selStatus'])) 	? 	$_POST['selStatus'] 	: 	"";


$supervisorComments = isset($_POST['supervisorComments']) 		? $_POST['supervisorComments'] 	: $arrTasks[0]['assessment_remarks'];

if(isset($_POST['status']))
{ $status = $_POST['status']; }
else if(($arrMonthTasksStatus['pe_status'] > 2) && ($arrMonthTasksStatus['pe_status'] < 4))
{ $status = $arrMonthTasksStatus['pe_status']; }
else if(($arrMonthTasksStatus['pe_status'] > 5) && ($arrMonthTasksStatus['pe_status'] < 7))
{ $status = $arrMonthTasksStatus['pe_status']; }
else { $status = ''; }
?>
<?php if(count($arrEmployees) > 0) { ?>

<div class="listPageMain">
  <form name="frmSearchAssessment" id="frmSearchAssessment" method="post" action="<?php echo $frmActionURL; ?>">
    <div class="searchBoxMain">
      <div class="searchHeader">Search</div>
      <div class="searchcontentmain">
        <div class="searchCol">
          <div class="labelContainer">Employee:</div>
          <div class="textBoxContainer">
            <select name="empCode" id="empCode" class="dropDown">
              <option value="">Select Employee</option>
              <?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmployee) {
					?>
              <optgroup label="<?php echo $key; ?>">
              <?php for($i=0;$i<count($arrEmployee);$i++) { ?>
              <option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_full_name']; ?></option>
              <?php } ?>
              </optgroup>
              <?php	}
					}
					?>
            </select>
          </div>
          <div class="labelContainer">Status:</div>
          <div class="textBoxContainer">
            <select name="selStatus" id="selStatus" class="dropDown">
              <option value="">Select Status</option>
              <?php foreach($assessmentStatus as $strKey => $strStatus) { ?>
              <option value="<?php echo $strKey; ?>"><?php echo $strStatus; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="formButtonContainerWide">
          <input type="submit" class="searchButton" name="btnSearch" id="btnSearch" value="Search">
        </div>
      </div>
      <script>
	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#selStatus').val('<?php echo $selStatus; ?>');
  </script> 
    </div>
  </form>
</div>

<style>
.customBtn:hover
{
background-image:none;
background-color:#4DB3D4;
}

</style>

<div class="centerButtonContainer">
	<input class="addButton customBtn" type="button" value="New Probationary Assessment" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_assessment' ?>';" />
</div>

<?php } ?>
<div class="centerElementsContainer">
  <div class="recordCountContainer"><?php echo "Total Records Count: " . $totalRecordsCount; ?></div>
  <?php if($pageLinks) { ?>
  <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
  <?php } ?>
</div>
<div class="listContentMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr class="listHeader">
      <td class="listHeaderCol">Employee Name</td>
      <td class="listHeaderCol">Designation</td>
      <td class="listHeaderCol">Supervisor</td>
      <td class="listHeaderColLast">Action</td>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		if($arrRecords[$ind]['emp_id'] == $this->userEmpNum && $arrRecords[$ind]['assessment_status'] == STATUS_SUPERVISOR_ENTERING_DETAILS) {
			// DO NOTHING
		} else {
	?>
    <tr class="listContentAlternate">
      <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
      <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_designation']; ?></td>
      <td class="listContentCol"><?php echo getSupervisorName($arrRecords[$ind]['emp_id']); ?></td>
      <td class="listContentColLast">
      	<div class="empColButtonContainer">
          <?php if($arrRecords[$ind]['assessment_status'] == STATUS_SUBMITTED_TO_HR) { ?>
          <img title="Assessment Form" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/display.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/probation_assessment/1/' . $arrRecords[$ind]['assessment_id']; ?>';" />
          <?php } ?>
          <?php if($canWrite == YES) { ?>
          <img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/save_assessment/' . $arrRecords[$ind]['assessment_id']; ?>';">
          <?php } if($canDelete == YES) { ?>
          <img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecords[$ind]['assessment_id']; ?>');">
          <?php } ?>
        </div>
      </td>
    </tr>
    <?php
		}
	} 
	if(!$ind) {
	?>
    <tr class="listContentAlternate">
      <td colspan="4" align="center" class="listContentCol">No Record Found</td>
    </tr>
    <?php
	}
	?>
  </table>
</div>
