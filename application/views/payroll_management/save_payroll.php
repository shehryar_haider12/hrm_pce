<?php
$empID 					= (isset($_POST['empID'])) 					? $_POST['empID'] 					: $record['emp_id'];
$payrollMonth 			= (isset($_POST['payrollMonth'])) 			? $_POST['payrollMonth'] 			: $record['payroll_month'];
$payrollYear 			= (isset($_POST['payrollYear'])) 			? $_POST['payrollYear'] 			: $record['payroll_year'];
$payrollCompanyID			= (isset($_POST['payrollCompanyID'])) 			? $_POST['payrollCompanyID'] 			: $record['payroll_company_id'];
$payrollBasic 			= (isset($_POST['payrollBasic'])) 			? $_POST['payrollBasic'] 			: $record['payroll_earning_basic'];
$payrollHousing 		= (isset($_POST['payrollHousing'])) 		? $_POST['payrollHousing'] 			: $record['payroll_earning_housing'];
$payrollTransport 		= (isset($_POST['payrollTransport'])) 		? $_POST['payrollTransport'] 		: $record['payroll_earning_transport'];
$payrollUtility 		= (isset($_POST['payrollUtility'])) 		? $_POST['payrollUtility'] 			: $record['payroll_earning_utility'];
$payrollTravel 			= (isset($_POST['payrollTravel'])) 			? $_POST['payrollTravel'] 			: $record['payroll_earning_travel'];
$payrollHealth 			= (isset($_POST['payrollHealth'])) 			? $_POST['payrollHealth'] 			: $record['payroll_earning_health'];
$payrollFuel 			= (isset($_POST['payrollFuel'])) 			? $_POST['payrollFuel'] 			: $record['payroll_earning_fuel'];
$payrollMobile 			= (isset($_POST['payrollMobile'])) 			? $_POST['payrollMobile'] 			: $record['payroll_earning_mobile'];
$payrollMedical 		= (isset($_POST['payrollMedical'])) 		? $_POST['payrollMedical'] 			: $record['payroll_earning_medical_relief'];
$payrollBonus 			= (isset($_POST['payrollBonus'])) 			? $_POST['payrollBonus'] 			: $record['payroll_earning_bonus'];
$payrollLeaveEnc 		= (isset($_POST['payrollLeaveEnc'])) 		? $_POST['payrollLeaveEnc'] 		: $record['payroll_earning_annual_leave_encashment'];
$payrollClaim 			= (isset($_POST['payrollClaim'])) 			? $_POST['payrollClaim'] 			: $record['payroll_earning_claims'];
$payrollCommission 		= (isset($_POST['payrollCommission'])) 		? $_POST['payrollCommission'] 		: $record['payroll_earning_commission'];
$payrollAnnualTicket 	= (isset($_POST['payrollAnnualTicket'])) 	? $_POST['payrollAnnualTicket'] 	: $record['payroll_earning_annual_ticket'];
$payrollGratuity 		= (isset($_POST['payrollGratuity'])) 		? $_POST['payrollGratuity'] 		: $record['payroll_earning_gratuity'];
$payrollSurveyExpense 	= (isset($_POST['payrollSurveyExpense'])) 	? $_POST['payrollSurveyExpense'] 	: $record['payroll_earning_survey_expense'];
$payrollSettlement 		= (isset($_POST['payrollSettlement'])) 		? $_POST['payrollSettlement'] 		: $record['payroll_earning_settlement'];
$payrollEarningMisc 	= (isset($_POST['payrollEarningMisc'])) 	? $_POST['payrollEarningMisc'] 		: $record['payroll_earning_misc'];
$payrollTax = 0;

$basicpercent = round(($record['emp_basic_salary'] / 165) * 100);
$housepercent = round((35 / 100) * $basicpercent);
$tenPercent = round((10 / 100) * $basicpercent);
$sixPercent = round((6 / 100) * $basicpercent);
$FourPercent = round((4 / 100) * $basicpercent);
// $basicpercent = $record['emp_basic_salary'] - ($housepercent+ $tenPercent+ $tenPercent + $sixPercent + $FourPercent) ;


$payrollTax = $payrollTax / 100 * $payrollBasic;
$payrollTax 			= (isset($_POST['payrollTax'])) 			? $_POST['payrollTax'] 				: $record['payroll_deduction_tax'];
$payrollPF 				= (isset($_POST['payrollPF'])) 				? $_POST['payrollPF'] 				: $record['payroll_deduction_pf'];
$payrollLoan 			= (isset($_POST['payrollLoan'])) 			? $_POST['payrollLoan'] 			: $record['payroll_deduction_loan'];
$payrollEOBI 			= (isset($_POST['payrollEOBI'])) 			? $_POST['payrollEOBI'] 			: $record['payroll_deduction_eobi'];
$payrollDeductionTelephone = (isset($_POST['payrollDeductionTelephone'])) 	? $_POST['payrollDeductionTelephone'] 	: $record['payroll_deduction_telephone'];
$payrollDeductionMisc	= (isset($_POST['payrollDeductionMisc'])) 	? $_POST['payrollDeductionMisc'] 	: $record['payroll_deduction_misc'];
//new fields
$payrollDeductionMobile	= (isset($_POST['payrollDeductionMobile'])) 	? $_POST['payrollDeductionMobile'] 	: $record['payroll_deduction_mobile'];
$payrollDeductionJeans	= (isset($_POST['payrollDeductionJeans'])) 	? $_POST['payrollDeductionJeans'] 	: $record['payroll_deduction_jeans'];
$payrollDeductionSmoking	= (isset($_POST['payrollDeductionSmoking'])) 	? $_POST['payrollDeductionSmoking'] 	: $record['payroll_deduction_smoking'];
$payrollDeductionWrongTime	= (isset($_POST['payrollDeductionWrongTime'])) 	? $_POST['payrollDeductionWrongTime'] 	: $record['payroll_deduction_wrong_time'];
$payrollDeductionCashierChecking	= (isset($_POST['payrollDeductionCashierChecking'])) 	? $_POST['payrollDeductionCashierChecking'] 	: $record['payroll_deduction_cashier_checking'];
$payrollDeductionGuardAttention	= (isset($_POST['payrollDeductionGuardAttention'])) 	? $_POST['payrollDeductionGuardAttention'] 	: $record['payroll_deduction_guard_attention'];
$payrollDeductionDVROff	= (isset($_POST['payrollDeductionDVROff'])) 	? $_POST['payrollDeductionDVROff'] 	: $record['payroll_deduction_dvr_off'];
$payrollDeductionLaptopInBranch	= (isset($_POST['payrollDeductionLaptopInBranch'])) 	? $_POST['payrollDeductionLaptopInBranch'] 	: $record['payroll_deduction_laptop_in_branch'];
$payrollDeductionNoResponse	= (isset($_POST['payrollDeductionNoResponse'])) 	? $_POST['payrollDeductionNoResponse'] 	: $record['payroll_deduction_no_response'];
$payrollDeductionBranchIncharge	= (isset($_POST['payrollDeductionBranchIncharge'])) 	? $_POST['payrollDeductionBranchIncharge'] 	: $record['payroll_deduction_branch_incharge'];
$payrollDeductionCCFLA	= (isset($_POST['payrollDeductionCCFLA'])) 	? $_POST['payrollDeductionCCFLA'] 	: $record['payroll_deduction_CCFLA'];
$payrollDeductionDealingWithout	= (isset($_POST['payrollDeductionDealingWithout'])) 	? $_POST['payrollDeductionDealingWithout'] 	: $record['payroll_deduction_dealing_without'];
$payrollDeductionMisbehave	= (isset($_POST['payrollDeductionMisbehave'])) 	? $_POST['payrollDeductionMisbehave'] 	: $record['payroll_deduction_misbehave'];
$payrollDeductionOpenClose	= (isset($_POST['payrollDeductionOpenClose'])) 	? $_POST['payrollDeductionOpenClose'] 	: $record['payroll_deduction_open_close'];
$payrollDeductionPendingWork	= (isset($_POST['payrollDeductionPendingWork'])) 	? $_POST['payrollDeductionPendingWork'] 	: $record['payroll_deduction_pending_work'];
$payrollFoodAllowance	= (isset($_POST['payrollFoodAllowance'])) 	? $_POST['payrollFoodAllowance'] 	: $record['payroll_earning_food_allowance'];
$payrollDeductionPendingWork	= (isset($_POST['payrollDeductionMMBLoan'])) 	? $_POST['payrollDeductionMMBLoan'] 	: $record['payroll_deductsion_MMB_loan'];

// $employee_id = array();
// $att_date = array();
// $att_in = array();
// $att_out = array();
// if ($fetch_timing->num_rows() > 0) {
// 	foreach ($fetch_timing->result() as $row) {
// 		array_push($employee_id, $row->employee_id);
// 		array_push($att_date, $row->att_date);
// 		array_push($att_in, $row->att_in);
// 		array_push($att_out, $row->att_out);
// 	}
// }
// print_r($att_in);
// exit;
//$payrollBonus = 10000;
//print_r($data1);
//print_r($data1['employee_id']);
?>
<script>
$(document).ready(function () {
      $('.select-state').selectize({
          sortField: 'text'
      });
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;
	}
      .selectize-control.dropDown.single {
            width: 620px !important;
      }
	.item{
		width: 100%;
	} 
      .formTextBoxContainer div{
		height: auto !important;
	}
      .selectize-input{
            border: none;
      }
/* .selectize-input.items.has-options.full.has-items{
	height: 35px !important;
}
.item{
	width: 100%;
} */
</style>

<form name="frmSavePayroll" id="frmSavePayroll" method="post">
<div class="listPageMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="5">Payroll Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Employee:</td>
            <td class="formTextBoxContainer" colspan="4">
            	<select name="empID" id="empIDSelection" class="dropDown select-state">
                  <option value="">Select Employee</option>
                  <?php
                    if (count($arrEmployees)) {
                        foreach($arrEmployees as $key => $arrEmp) {
                    ?>
                        <optgroup label="<?php echo $key; ?>">
                            <?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
                                <option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name'];?></option>
                            <?php } ?>
                        </optgroup>
                    <?php	}
                    }
                    ?>
              </select>
            </td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Month:</td>
            <td class="formTextBoxContainer" colspan="4">
            	<select name="payrollMonth" id="payrollMonth" class="dropDown">
                  <option value="">Select Month</option>
                  <?php
				  if (count($arrMonths)) {
                              //   print_r($arrMonths);
					  foreach($arrMonths as $strKey => $strValue) {
				  ?>
				  <option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
				  <?php
					  }
				  }
				  ?>
              </select>
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Year:</td>
            <td class="formTextBoxContainer" colspan="4">
            	<select name="payrollYear" id="payrollYear" class="dropDown">
                  <option value="">Select Year</option>
                  <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
                  <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                  <?php } ?>
              </select>
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow" colspan="2" align="center">Earning</td>
            <td></td>
            <td class="formHeaderRow" colspan="2" align="center">Deduction</td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Total Salary:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" name="payrollBasic" readonly id="payrollBasic" class="textBox" value="<?php if($record['emp_basic_salary'] > 0){
                  echo $record['emp_basic_salary'];}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Tax:</td>
            <td class="formTextBoxContainer">
                <input type="text" name="payrollTax" id="payrollTax" class="textBox" value="<?php if($payrollTax > 0){
                  echo $payrollTax;}
                  else {
                  echo 0;  
                  }?>">
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Basic Salary:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollNetSalary" id="payrollNetSalary" class="textBox" value="<?php if($basicpercent > 0){
                  echo $basicpercent;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            
            <td></td>
            <td class="formLabelContainer">Departmental Claims:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollPF" id="payrollPF" class="textBox" value="<?php if($payrollPF > 0){
                  echo $payrollPF;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Housing Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollHousing" readonly id="payrollHousing" class="textBox" value="<?php if($housepercent > 0){
                  echo $housepercent;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            
            <td></td>
            <td class="formLabelContainer">Salary Advance/Loan Recovery:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollLoan" id="payrollLoan" class="textBox" value="<?php if($payrollLoan > 0){
                  echo $payrollLoan;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Conveyance Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollTransport" id="payrollTransport" class="textBox" value="<?php if($sixPercent > 0){
                  echo $sixPercent;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            
            <td></td>
            <td class="formLabelContainer">EOBI:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollEOBI" id="payrollEOBI" class="textBox" value="<?php if($payrollEOBI > 0){
                  echo $payrollEOBI;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Utility Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollUtility" id="payrollUtility" class="textBox" value="<?php if($tenPercent > 0){
                  echo $tenPercent;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            

            <td></td>
            <td class="formLabelContainer">HR Deduction:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionTelephone" id="payrollDeductionTelephone" class="textBox" value="<?php if($payrollDeductionTelephone > 0){
                  echo $payrollDeductionTelephone;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Medical Relief:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollMedical" id="payrollMedical" class="textBox" value="<?php if($tenPercent > 0){
                  echo $tenPercent;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            
            <td></td>
            <td class="formLabelContainer">Others/Misc.:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionMisc" id="payrollDeductionMisc" class="textBox" value="<?php if($payrollDeductionMisc > 0){
                  echo $payrollDeductionMisc;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Food Allowance.:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollFoodAllowance" id="payrollFoodAllowance" class="textBox" value="<?php if($FourPercent > 0){
                  echo $FourPercent;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td colspan="3"></td> -->
            <td class="formLabelContainer">MMB Loan:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionMMBLoan" id="payrollDeductionMMBLoan" class="textBox" value="<?php if($payrollDeductionMMBLoan > 0){
                  echo $payrollDeductionPendingWork;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
            <!-- <td class="formLabelContainer">Mobile Use (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionMobile" id="payrollDeductionMobile" class="textBox" value="<?php if($payrollDeductionMobile > 0){
                  echo $payrollDeductionMobile;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr>
            <td class="formLabelContainer">Bonus:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollBonus" id="payrollBonus" class="textBox" value="<?php if($payrollBonus > 0){
                  echo $payrollBonus;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Open/Close Branch Before/After Office time (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionOpenClose" id="payrollDeductionOpenClose" class="textBox" value="<?php if($payrollDeductionOpenClose > 0){
                  echo $payrollDeductionOpenClose;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr>
            <!-- <td class="formLabelContainer">Jeans/T-Shirt (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionJeans" id="payrollDeductionJeans" class="textBox" value="<?php if($payrollDeductionJeans > 0){
                  echo $payrollDeductionJeans;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Annual Ticket:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollAnnualTicket" id="payrollAnnualTicket" class="textBox" value="<?php if($payrollAnnualTicket > 0){
                  echo $payrollAnnualTicket;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Gutka/Maawa/Smoking (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionSmoking" id="payrollDeductionSmoking" class="textBox" value="<?php if($payrollDeductionSmoking > 0){
                  echo $payrollDeductionSmoking;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr>
            <td class="formLabelContainer">Health Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollHealth" id="payrollHealth" class="textBox" value="<?php if($payrollHealth > 0){
                  echo $payrollHealth;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Wrong Time IN/OUT Entry (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionWrongTime" id="payrollDeductionWrongTime" class="textBox" value="<?php if($payrollDeductionWrongTime > 0){
                  echo $payrollDeductionWrongTime;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Fuel:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollFuel" id="payrollFuel" class="textBox" value="<?php if($payrollFuel > 0){
                  echo $payrollFuel;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Cashier Checking By Guards (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionCashierChecking" id="payrollDeductionCashierChecking" class="textBox" value="<?php if($payrollDeductionCashierChecking > 0){
                  echo $payrollDeductionCashierChecking;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr>
            <td class="formLabelContainer">Mobile:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollMobile" id="payrollMobile" class="textBox" value="<?php if($payrollMobile > 0){
                  echo $payrollMobile;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Guard Attention on Duty (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionGuardAttention" id="payrollDeductionGuardAttention" class="textBox" value="<?php if($payrollDeductionGuardAttention > 0){
                  echo $payrollDeductionMiscpayrollDeductionGuardAttention;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Travel Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollTravel" id="payrollTravel" class="textBox" value="<?php if($payrollTravel > 0){
                  echo $payrollTravel;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">DVR off with no reason (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionDVROff" id="payrollDeductionDVROff" class="textBox" value="<?php if($payrollDeductionDVROff > 0){
                  echo $payrollDeductionDVROff;}
                  else {
                  echo 0;  
                  }?>">
            </td>-->
      	</tr> 
		<tr>
            <td class="formLabelContainer">Claim:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollClaim" id="payrollClaim" class="textBox" value="<?php if($payrollClaim > 0){
                  echo $payrollClaim;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Laptop in Branch (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionLaptopInBranch" id="payrollDeductionLaptopInBranch" class="textBox" value="<?php if($payrollDeductionLaptopInBranch > 0){
                  echo $payrollDeductionLaptopInBranch;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Annual Leave Encashment:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollLeaveEnc" id="payrollLeaveEnc" class="textBox" value="<?php if($payrollLeaveEnc > 0){
                  echo $payrollLeaveEnc;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">No Response to Customer (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionNoResponse" id="payrollDeductionNoResponse" class="textBox" value="<?php if($payrollDeductionNoResponse > 0){
                  echo $payrollDeductionNoResponse;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr>
            <td class="formLabelContainer">Gratuity:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollGratuity" id="payrollGratuity" class="textBox" value="<?php if($payrollGratuity > 0){
                  echo $payrollGratuity;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Branch Incharge Daily DVR Check (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionBranchIncharge" id="payrollDeductionBranchIncharge" class="textBox" value="<?php if($payrollDeductionBranchIncharge > 0){
                  echo $payrollDeductionBranchIncharge;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Final Settlement:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollSettlement" id="payrollSettlement" class="textBox" value="<?php if($payrollSettlement > 0){
                  echo $payrollSettlement;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            <!-- <td class="formLabelContainer">Company Campaiging By FLA (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionCCFLA" id="payrollDeductionCCFLA" class="textBox" value="<?php if($payrollDeductionCCFLA > 0){
                  echo $payrollDeductionCCFLA;}
                  else {
                  echo 0;  
                  }?>">
            </td> -->
            <!-- <td colspan="3"></td> -->
      	</tr>
		<tr>
            <td class="formLabelContainer">Others/Misc/Fine.:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollEarningMisc" id="payrollEarningMisc" class="textBox" value="<?php if($payrollEarningMisc > 0){
                  echo $payrollEarningMisc;}
                  else {
                  echo 0;  
                  }?>">
            </td>
            <td></td>
            
            <!-- <td class="formLabelContainer">Cash Dealing Without Computer Receipt (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionDealingWithout" id="payrollDeductionDealingWithout" class="textBox" value="<?php if($payrollDeductionDealingWithout > 0){
                  echo $payrollDeductionDealingWithout;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
            <!-- new line -->
            <tr>
                  <td class="formLabelContainer">Commission:</td>
                  <td class="formTextBoxContainer">
                        <input type="text" name="payrollCommission" id="payrollCommission" class="textBox" value="<?php if($payrollCommission > 0){
                        echo $payrollCommission;}
                        else {
                        echo 0;  
                        }?>">
                  </td>
            <td></td>
            <!-- <td class="formLabelContainer">Misbehave with Customer (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionMisbehave" id="payrollDeductionMisbehave" class="textBox" value="<?php if($payrollDeductionMisbehave > 0){
                  echo $payrollDeductionMisbehave;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
            <tr>
                  <td class="formLabelContainer">Travel/Survey Expense:</td>
                  <td class="formTextBoxContainer">
                        <input type="text" name="payrollSurveyExpense" id="payrollSurveyExpense" class="textBox" value="<?php if($payrollSurveyExpense > 0){
                        echo $payrollSurveyExpense;}
                        else {
                        echo 0;  
                        }?>">
                  </td>
            <td></td>
            <!-- <td class="formLabelContainer">Misbehave with Customer (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionMisbehave" id="payrollDeductionMisbehave" class="textBox" value="<?php if($payrollDeductionMisbehave > 0){
                  echo $payrollDeductionMisbehave;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
            <!-- new line -->
            <!-- <tr>
            <td colspan="3"></td>
            <td class="formLabelContainer">Open/Close Branch Before/After Office time (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionOpenClose" id="payrollDeductionOpenClose" class="textBox" value="<?php if($payrollDeductionOpenClose > 0){
                  echo $payrollDeductionOpenClose;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
            <!-- new line -->
            <tr>
            <td colspan="3"></td>
            <!-- <td class="formLabelContainer">Pending Work Due to Technician (CCTV):</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionPendingWork" id="payrollDeductionPendingWork" class="textBox" value="<?php if($payrollDeductionPendingWork > 0){
                  echo $payrollDeductionPendingWork;}
                  else {
                  echo 0;  
                  }?>">
            </td>
      	</tr> -->
            <tr>
            
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer" colspan="4">
      		<?php if($canWrite == YES) { ?>
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
            <?php } ?>
    		</td>
        </tr>
  </table>
  </div>
</form>
<script>
      $('#empIDSelection').on('change', function(){
            var id = $(this).val();
            // console.log('asdfasdf'+id);
            $.ajax({
                  url: "http://119.159.242.5/HRM/payroll_management/save_payroll/0/"+id,
                  method:"GET",
                  contentType: 'application/json; charset=utf-8',

                  // data: {"emp_getID": sel.value},
                  success: function(response) {
                        if(response){
                              var basic = Math.round((response / 165) * 100);
                              var housepercent = Math.round((35 / 100) * basic);
                              var tenPercent = Math.round((10 / 100) * basic);
                              var sixPercent = Math.round((6 / 100) * basic);
                              var FourPercent = Math.round((4 / 100) * basic);
                              // console.log(housepercent);
                              document.getElementById("payrollHousing").value = housepercent;
                              document.getElementById("payrollMedical").value = tenPercent;
                              document.getElementById("payrollUtility").value = tenPercent;
                              document.getElementById("payrollTransport").value = sixPercent;
                              document.getElementById("payrollFoodAllowance").value = FourPercent;
                              document.getElementById("payrollBasic").value = response;
                              document.getElementById("payrollNetSalary").value = basic;
                        }else{
                              document.getElementById("payrollBasic").value = 0;
                              document.getElementById("payrollHousing").value = 0;
                              document.getElementById("payrollMedical").value = 0;
                              document.getElementById("payrollUtility").value = 0;
                              document.getElementById("payrollTransport").value = 0;
                              document.getElementById("payrollFoodAllowance").value = 0;
                        }
                  },
                  error: function(xhr) {
                        console.log(xhr);
                  }
            });
      });
</script>
<script>
  $('#empIDSelection').val('<?php echo $empID; ?>');
  $('#payrollMonth').val('<?php echo $payrollMonth; ?>');
  $('#payrollYear').val('<?php echo $payrollYear; ?>');
  
  <?php if((int)$record['payroll_month'] <= $this->payrollCloseMonth && (int)$record['payroll_year'] <= $this->payrollCloseYear && (int)$payrollID > 0) { ?>
  $("#frmSavePayroll :input").attr("disabled", true).css({"background-color": "#ddd"});
  $(".smallButton").remove();

  <?php } ?>
</script>