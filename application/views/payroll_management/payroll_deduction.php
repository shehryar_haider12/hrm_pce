<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    @import url(http://fonts.googleapis.com/css?family=Lato:400,700);
    nav#sidebar{
        display: none;
    }
body
{
    font-family: 'Lato', 'sans-serif';
    }
.profile 
{
    min-height: 355px;
    display: inline-block;
}
.well.profile {
    width: 1000px;
}
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }
.dropdown-menu 
{
    background-color: #34495e;    
    box-shadow: none;
    -webkit-box-shadow: none;
    width: 250px;
    margin-left: -125px;
    left: 50%;
    }
.dropdown-menu .divider 
{
    background:none;    
    }
.dropdown-menu>li>a
{
    color:#f5f5f5;
    }
.dropup .dropdown-menu 
{
    margin-bottom:10px;
    }
.dropup .dropdown-menu:before 
{
    content: "";
    border-top: 10px solid #34495e;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    bottom: -10px;
    left: 50%;
    margin-left: -10px;
    z-index: 10;
    }
    .emp_info{
        width: 45% !important;
        font-size: 17px;
        padding: 14px;
    }
    .emp_info_value{
        width: 45% !important;
        font-size: 17px;
        font-weight: bold;
        padding: 14px;
        border-bottom: 1px solid #cccaca;
    }
</style>
<?php
    // print_r();
    // exit;
?>
<div class="col-md-12">
    <!-- <td width="650px">
        <table border="1" width="100%" style="line-height:22px">
        <tr>
            <td width="70%">
                <img src="<?php echo $transfer_details['LOGO_URL']?>" width="151" height="35" />
            </td>
            <td width="30%" align="right">
                Report Generated At<br />
                <?php echo $transfer_details['CREATED_DATE_TIME']?>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
            </td>
        </tr>
    </table>
    </td> -->
<div class="well profile">
    <div class="col-sm-12">
    <tr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <tr>
                    <td width="70%">
                        <img src="<?php echo $transfer_details['LOGO_URL']?>" width="151" height="150px" />
                    </td>
                    <td width="30%" align="right">
                    	Report Generated At<br />
                        <?php echo $transfer_details['CREATED_DATE_TIME']?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
        <div class="col-xs-12 col-sm-8">
            <h2><?php echo $transfer_details['EMPLOYEE_NAME']?></h2>
            <p><strong>Employee Number : </strong> <?php echo $transfer_details['EMPLOYEE_CODE']?> </p>
            <p><strong>D.O.J : </strong> <?php echo $transfer_details['JOINING_DATE']?> </p>
            <p><strong>Designation: </strong>  <?php echo $transfer_details['EMPLOYEE_DESIGNATION']?></p>
            
        </div>             
        
                
    

<table style="line-height:22px;width: 100% !important;">

    <tr>
        <tr bgcolor="#0070C0" style="border-bottom: 1px solid white;">
            <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">BENEFITS</td>
        </tr> 
        <hr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <thead>
                    <tr bgcolor="#0070C0" >
                        <th style="padding: 11px;color: white;font-weight: bold;">TITLE</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">Price</th>
                        <!-- <th style="padding: 11px;color: white;font-weight: bold;">Date of Posting</th> -->
                        <th style="padding: 11px;color: white;font-weight: bold;">Processed By</th>
                    </tr>   

                </thead>
                <tbody>
                    <?php echo $transfer_details['SYSTEMBENEFITS']; ?>
                </tbody>

            </table>
        </td>
    </tr>

    <!-- HR Deductions -->
    <?php if($transfer_details['SYSTEMDEDUCTION'] != Null){ ?>
    <tr>
        <tr bgcolor="#0070C0" style="border-bottom: 1px solid white;">
            <td valign="top" colspan="5" style="color:#FFF;padding: 16px;">DEDUCTIONS BY HR &nbsp;&nbsp; <span style=" float: right; "> <?php echo $transfer_details['TOTALHRDEDUCTION']; ?> </span> </td>
        </tr> 
        <hr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">            
                <tr>
                <td width="650px">
                    <table border="1" width="100%" style="line-height:22px">
                        <thead>
                            <tr bgcolor="#0070C0" >
                                <th style="padding: 11px;color: white;font-weight: bold;">Title</th>
                                <th style="padding: 11px;color: white;font-weight: bold;">Price</th>
                                <!-- <th style="padding: 11px;color: white;font-weight: bold;">Date of Posting</th> -->
                                <th style="padding: 11px;color: white;font-weight: bold;">Processed By</th>
                            </tr>   

                        </thead>
                        <tbody>
                            
                        <?php print_r($transfer_details['SYSTEMDEDUCTION']); ?>
                        </tbody>

                    </table>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <?php } ?>

    <!-- cctv Deduction -->
    <?php if($transfer_details['CCTVDEDUCTION'] != Null){ ?>
    <tr>
        <tr bgcolor="#0070C0" style="border-bottom: 1px solid white;">
            <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">DEDUCTIONS BY CCTV <span style=" float:right; "> <?php echo $transfer_details['TOTALCCTVDEDUCTION']; ?> </span></td>
        </tr> 
        <hr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">            
                <tr>
                <td width="650px">
                    <table border="1" width="100%" style="line-height:22px">
                        <thead>
                            <tr bgcolor="#0070C0" >
                                <th style="padding: 11px;color: white;font-weight: bold;">Title</th>
                                <th style="padding: 11px;color: white;font-weight: bold;">Price</th>
                                <!-- <th style="padding: 11px;color: white;font-weight: bold;">Date of Posting</th> -->
                                <th style="padding: 11px;color: white;font-weight: bold;">Processed By</th>
                            </tr>   

                        </thead>
                        <tbody>
                        <?php 
                            $i =1;
                            foreach ($transfer_details['CCTVDEDUCTION'] as $key => $posting) {
                            ?>
                            <tr>
                                <!-- <td style="padding: 11px;"><?php echo $i;?></td> -->
                                <td style="padding: 11px;"><?php echo $posting['name']?></td>
                                <td style="padding: 11px;"><?php echo $posting['price']?></td>
                                <!-- <td style="padding: 11px;"><?php echo $posting['created_date']?></td> -->
                                <td style="padding: 11px;"><?php echo $posting['processed_name'] > 0 ? $posting['processed_name'] : 'CCTV Manager'; ?></td>
                            </tr>
                            <?php
                            $i++;
                            }
                                ?>
                        </tbody>

                    </table>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <?php } ?>

    <!-- Compliances Deductions -->
    <?php if($transfer_details['COMPLIANCEDEDUCTION'] != Null){ ?>
        <tr>
            <tr bgcolor="#0070C0" style="border-bottom: 1px solid white;">
                <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">DEDUCTIONS BY COMPLIANCES <span style=" float: right; "> <?php echo $transfer_details['TOTALCOMPLIANCEDEDUCTION']; ?> </span></td>
            </tr> 
            <hr>
            <td width="650px">
                <table border="1" width="100%" style="line-height:22px">            
                    <tr>
                    <td width="650px">
                        <table border="1" width="100%" style="line-height:22px">
                            <thead>
                                <tr bgcolor="#0070C0" >
                                    <th style="padding: 11px;color: white;font-weight: bold;">Title</th>
                                    <th style="padding: 11px;color: white;font-weight: bold;">Price</th>
                                    <!-- <th style="padding: 11px;color: white;font-weight: bold;">Date of Posting</th> -->
                                    <th style="padding: 11px;color: white;font-weight: bold;">Processed By</th>
                                </tr>   

                            </thead>
                            <tbody>
                                <?php 
                                $i =1;
                                foreach ($transfer_details['COMPLIANCEDEDUCTION'] as $key => $posting) {
                                ?>
                                <tr>
                                    <!-- <td style="padding: 11px;"><?php echo $i;?></td> -->
                                    <td style="padding: 11px;"><?php echo $posting['title']?></td>
                                    <td style="padding: 11px;"><?php echo $posting['price']?></td>
                                    <!-- <td style="padding: 11px;"><?php echo $posting['created_date']?></td> -->
                                    <td style="padding: 11px;"><?php echo $posting['processed_name'] > 0 ? $posting['processed_name'] : 'System Generated'; ?></td>
                                </tr>
                                <?php
                                $i++;
                                }
                                    ?>
                            </tbody>

                        </table>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
    <?php } ?>
    <!-- Late Coming Deductions -->
    <?php if($transfer_details['LATECOMINGDEDUCTION'] != Null){ ?>
        <tr>
            <tr bgcolor="#0070C0" style="border-bottom: 1px solid white;">
                <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">DEDUCTIONS BY LATE COMING <span style=" float: right; "> <?php echo $transfer_details['TOTALLATECOMINGDEDUCTION']; ?> </span></td>
            </tr> 
            <hr>
            <td width="650px">
                <table border="1" width="100%" style="line-height:22px">            
                    <tr>
                    <td width="650px">
                        <table border="1" width="100%" style="line-height:22px">
                            <thead>
                                <tr bgcolor="#0070C0" >
                                    <th style="padding: 11px;color: white;font-weight: bold;">Title</th>
                                    <th style="padding: 11px;color: white;font-weight: bold;">Price</th>
                                    <!-- <th style="padding: 11px;color: white;font-weight: bold;">Date of Posting</th> -->
                                    <th style="padding: 11px;color: white;font-weight: bold;">Processed By</th>
                                    <th style="padding: 11px;color: white;font-weight: bold;">Action</th>
                                </tr>   

                            </thead>
                            <tbody>
                                <?php print_r($transfer_details['LATECOMINGDEDUCTION']); ?>
                            </tbody>

                        </table>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
    <?php } ?>

    <!-- Total Salary -->
    <tr>
        <tr bgcolor="#0070C0" style="    border-bottom: 1px solid white;">
            <td valign="top" colspan="2" style="color:#FFF;padding: 16px;">TOTAL </td>
        </tr> 
        <hr>
    	<td width="650px">
            <table border="1" width="100%" style="line-height:22px">
                <thead>
                    <tr bgcolor="#0070C0" >
                        <th style="padding: 11px;color: white;font-weight: bold;">TOTAL EARNINGS</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">TOTAL DEDUCTIONS</th>
                        <th style="padding: 11px;color: white;font-weight: bold;">NET SALARY</th>
                    </tr>   

                </thead>
                <tbody>
                    <tr>
                        <th style="padding: 11px; font-weight: bold;"><?php echo $transfer_details['TOTAL_EARNINGS']; ?></th>
                        <th style="padding: 11px; font-weight: bold;"><?php echo $transfer_details['TOTAL_DEDUCTIONS']; ?></th>
                        <th style="padding: 11px; font-weight: bold;"><?php echo $transfer_details['NET_SALARY']; ?></th>
                    </tr>
                </tbody>

            </table>
        </td>
    </tr>
  
</table>
</div>                 
</div></div>