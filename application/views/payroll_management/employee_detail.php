<div class="popupNotificationMain" style="font-size:13px">
  <div class="popupNotificationBox">
    <div class="weekendPopupHeading"> Employee Details </div>
    <div style="float:left;width:100%">
      <div style="border:5px solid #99DAEF;float:none;margin:0 auto;width:60%">
        <table border="0" align="center" cellspacing="0" cellpadding="0" class="dottedBorder" style="width:100%">
          <?php
          if(!file_exists($pictureFolder . $arrRecord['emp_photo_name']) || empty($arrRecord['emp_photo_name'])) {
			  if(empty($arrRecord['emp_gender'])) {
				  $arrRecord['emp_gender'] = 'male';
			  }
			  $arrRecord['emp_photo_name'] = 'no_image_' . strtolower($arrRecord['emp_gender']) . '.jpg';
		  }
		  ?>
          <tr>
            <td class="weekendPopupCol" colspan="2" align="center">
            	<img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrRecord['emp_photo_name']; ?>" width="125">
            </td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Name</td>
            <td class="weekendPopupColLast"><?php echo $arrRecord['emp_full_name']; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Employee Code</td>
            <td class="weekendPopupColLast"><?php echo $arrRecord['emp_code']; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Company</td>
            <td class="weekendPopupColLast"><?php echo $arrRecord['company_name']; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Designation</td>
            <td class="weekendPopupColLast"><?php echo $arrRecord['emp_designation']; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Supervisor</td>
            <td class="weekendPopupColLast"><?php echo getSupervisorName($arrRecord['emp_id']); ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Joining Date</td>
            <td class="weekendPopupColLast"><?php echo ($arrRecord['emp_joining_date'] != '0000-00-00') ? date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecord['emp_joining_date'])) : ' - '; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Probation End Date</td>
            <td class="weekendPopupColLast"><?php echo ($arrRecord['emp_probation_end_date'] != '0000-00-00') ? date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecord['emp_probation_end_date'])) : ' - '; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Confirmation Date</td>
            <td class="weekendPopupColLast"><?php echo ($arrRecord['emp_confirmation_date'] != '0000-00-00') ? date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecord['emp_confirmation_date'])) : ' - '; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Visa Issue Date</td>
            <td class="weekendPopupColLast"><?php echo ($arrRecord['emp_visa_issue_date'] != '0000-00-00') ? date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecord['emp_visa_issue_date'])) : ' - '; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Visa Expiry Date</td>
            <td class="weekendPopupColLast"><?php echo ($arrRecord['emp_visa_expiry_date'] != '0000-00-00') ? date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecord['emp_visa_expiry_date'])) : ' - '; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Sponsor</td>
            <td class="weekendPopupColLast"><?php echo getSponsorName($arrRecord['emp_sponsor_id']); ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Contract Type</td>
            <td class="weekendPopupColLast"><?php echo $arrRecord['emp_employment_type']; ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Remaining Annual Leaves</td>
            <td class="weekendPopupColLast"><?php echo round($arrRecord['emp_annual_leaves']); ?></td>
          </tr>
          <tr class="weekendPopupRowAlternate">
            <td class="weekendPopupCol">Remaining Sick Leaves</td>
            <td class="weekendPopupColLast"><?php echo round($arrRecord['emp_sick_leaves']); ?></td>
          </tr>
          <tr>
            <td class="weekendPopupColLast">&nbsp;</td>
            <td class="weekendPopupColLast">&nbsp;</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div style="text-align:center">
    <input  class="smallButton"  name="close" type="button" value="Close" onclick="window.close()">
  </div>
</div>
