<?php
$firstName 			= (set_value('firstName') != '') 		? set_value('firstName') 		: $record['first_name'];
$lastName 			= (set_value('lastName') != '') 		? set_value('lastName') 		: $record['last_name'];
$passWord 			= (set_value('passWord') != '') 		? set_value('passWord') 		: $record['password'];
$eMail 				= (set_value('eMail') != '') 			? set_value('eMail') 			: $record['email'];
$contactNo 			= (set_value('contactNo') != '') 		? set_value('contactNo') 		: $record['contact_number'];
$homePhoneNo 		= (set_value('homePhoneNo') != '') 		? set_value('homePhoneNo') 		: $record['home_contact_number'];
$candidateAddress 	= (set_value('candidateAddress') != '') ? set_value('candidateAddress') : $record['candidate_address'];
$nicNo 				= (set_value('nicNo') != '') 			? set_value('nicNo') 			: $record['nic_number'];
$fatherNICNo 		= (set_value('fatherNICNo') != '') 		? set_value('fatherNICNo') 		: $record['father_nic_number'];
$Gender 			= (set_value('Gender') != '') 			? set_value('Gender') 			: $record['gender'];
$maritalStatus 		= (set_value('maritalStatus') != '') 	? set_value('maritalStatus') 	: $record['marital_status'];
$dateOfBirth 		= (set_value('dateOfBirth') != '') 		? set_value('dateOfBirth') 		: $record['dob'];
$cityID 			= (set_value('City') != '') 			? set_value('City') 			: $record['city'];
$areaID 			= (set_value('Area') != '') 			? set_value('Area') 			: $record['area'];
$countryID 			= (set_value('Country') != '') 			? set_value('Country') 			: $record['country'];
$cvFileName 		= ($record['cv_file_name'] != '') 		? $record['cv_file_name'] 		: '';
$Comments 			= (set_value('Comments') != '') 		? set_value('Comments') 		: $record['comment'];
$Status 			= (set_value('Status') != '') 			? set_value('Status') 			: $record['status'];
$changePW 			= (isset($_POST['changePW'])) 			? $_POST['changePW'] 			: 0;
if(!$Status) {
	$Status = -1;
}
?>
<script>
$(function() {
	$( "#dateOfBirth" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( "#dateOfBirth" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateOfBirth" ).datepicker( "setDate", "<?php echo $dateOfBirth; ?>" );
});
</script>
<div class="listPageMain">
<form name="frmAddCandidate" id="frmAddCandidate" enctype="multipart/form-data" method="post">
  <div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Update Profile</td>
        </tr>
		<tr>
            <td class="formLabelContainer">First Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" name="firstName" maxlength="30" id="firstName" class="textBox" value="<?php echo $firstName; ?>">
            </td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Last Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="lastName" maxlength="30" id="lastName" class="textBox" value="<?php echo $lastName; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Email:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="eMail" maxlength="100" id="eMail" class="textBox" value="<?php echo $eMail; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Cell Phone Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="contactNo" maxlength="14" id="contactNo" class="textBox" value="<?php echo $contactNo; ?>">
            </td>
      	</tr>
		<tr></tr>
            <td class="formLabelContainer">Home Phone Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="homePhoneNo" maxlength="14" id="homePhoneNo" class="textBox" value="<?php echo $homePhoneNo; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Address:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
				<textarea rows="5" cols="30" name="candidateAddress" id="candidateAddress" class="textArea"><?php echo $candidateAddress; ?></textarea>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">NIC Number:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="nicNo" maxlength="13" id="nicNo" class="textBox" value="<?php echo $nicNo; ?>">&nbsp;<span style="font-size:11px; font-style:italic; color:#999">(Please ignore both dashes. e.g. "1234567891234")</span>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Father's NIC Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="fatherNICNo" maxlength="13" id="fatherNICNo" class="textBox" value="<?php echo $fatherNICNo; ?>">&nbsp;<span style="font-size:11px; font-style:italic; color:#999">(Please ignore both dashes. e.g. "1234567891234")</span>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Gender:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="Gender" name="Gender" class="dropDown">
                  <option value="">Select Gender</option>
                  <?php
                  if (count($Genders)) {
                      foreach($Genders as $genderVal) {
                          $selected = '';
                          if($Gender == $genderVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $genderVal; ?>" <?php echo $selected; ?>><?php echo $genderVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Marital Status:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="maritalStatus" name="maritalStatus" class="dropDown">
                  <option value="">Select Marital Status</option>
                  <?php
                  if (count($maritalStatuses)) {
                      foreach($maritalStatuses as $maritalVal) {
                          $selected = '';
                          if($maritalStatus == $maritalVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $maritalVal; ?>" <?php echo $selected; ?>><?php echo $maritalVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>  
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Date of Birth:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="date" name="dateOfBirth" maxlength="30" id="dateOfBirth" class="textBox datePicker">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Country/City:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer" id="tdlocation">
        		<select id="Country" name="Country" class="dropDown" onChange="populateLocation('tdlocation', this.value, '3');">
                  	<option value="">Select Country</option>
                    <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($countryID == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select> 
              <?php if((int)$record['city']) { ?> 
              	<select id="City" name="City" class="dropDown" style="clear:both">
                  	<option value="">Select City</option>
                    <?php 
                    if (count($Cities)) {
                        foreach($Cities as $dataCity) {
							$strSelected = '';
							if($cityID == $dataCity['location_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $dataCity['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCity['location_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
              <?php } ?>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Resume:</td>
            <td class="formTextBoxContainer">
        		<input type="file" name="Resume" id="Resume" />&nbsp;
				<?php
                if(file_exists($resumeFolder . $cvFileName)) {
				?>
                	<input type="hidden" name="cvFileName" id="cvFileName" value="<?php echo $cvFileName; ?>" />
					<a href="<?php echo base_url(). $resumeFolderDownload . $cvFileName; ?>"><?php echo $cvFileName; ?></a> 
                <?php
				}
				?>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Cover Letter:</td>
            <td class="formTextBoxContainer">
        		<textarea rows="9" cols="30" name="Comments" id="Comments" class="applyTextArea"><?php echo $Comments; ?></textarea>
      		</td>
        </tr>
		<tr>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      			<input type="submit" class="smallButton" name="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" value="Cancel" onclick="history.go(-1)">      
    		</td>
        </tr>
        <input type="hidden" name="currentPassWord" id="currentPassWord" value="<?php echo $passWord; ?>" />
  </table>
  </div>
</form>

<form name="frmPassword" id="frmPassword" enctype="multipart/form-data" method="post">
  <div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Update Password
            	<input type="hidden" name="changePW" id="changePW" value="1" />
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">New Password:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="password" name="newPassWord" maxlength="16" id="newPassWord" class="textBox" value="">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Confirm Password:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="password" name="confirmPassWord" maxlength="16" id="confirmPassWord" class="textBox" value="">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Old Password:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="password" name="oldPassWord" maxlength="16" id="oldPassWord" class="textBox" value="">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      			<input type="submit" class="smallButton" name="btnSave" value="Update">&nbsp;
        		<input type="button" class="smallButton" value="Cancel" onclick="history.go(-1)">      
    		</td>
        </tr>
        <input type="hidden" name="currentPassWord" id="currentPassWord" value="<?php echo $passWord; ?>" />
  </table>
  </div>
</form>
</div>
<script>
	$("#Status").val('<?php echo $Status; ?>');
  	<?php if((int)$countryID) { ?>
	populateLocationWithCity('tdlocation', '<?php echo $countryID; ?>', '3', <?php echo (int)$cityID; ?>, <?php echo (int)$areaID; ?>)
	<?php } ?>
	<?php if($canWrite == 0) { ?>
	$("#frmAddCandidate :input").attr("disabled", true);
	<?php } ?>
</script>