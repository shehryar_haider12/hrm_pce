<div style="background ">
<?php echo form_open('welcome'); ?>
	<div class="loginBoxMain">
    	<div class="loginBoxHeader"></div>
        <div class="loginContent">
        <h4 class="loginPageTitle">LOGIN</h4>
        	<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
				<tr>
                    <td class="loginLeftCol" align="right">User Name:</td>
                    <td class="loginRightCol"><input class="loginTextBox" type="text" placeholder="Username" name="username" value="<?php echo set_value('username'); ?>" size="50" /></td>
				</tr>
                <tr>
                    <td class="loginLeftCol" align="right">Password:</td>
                    <td class="loginRightCol"><input class="loginTextBox" type="password" placeholder="Password" name="password" value="" size="50" /></td>
				</tr>
                <tr>
                    <td colspan="2" class="loginButtonCol" align="right"><input class="smallButton" name="Login" type="submit" value="Login"></td>
                </tr>
                <tr>
                    <td colspan="2" class="loginButtonCol" align="right"><a href="<?php echo $this->baseURL . '/' . $this->currentController; ?>/forgot_password">Forgot User Name or Password?</a></td>
                </tr>
			</table>
        </div>
    </div>
<?php echo form_close(); ?>
</div>