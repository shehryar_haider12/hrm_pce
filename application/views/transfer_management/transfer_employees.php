<script>
$(document).ready(function () {
      $('select').selectize({
          sortField: 'text'
      });
  });
</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" /> -->

<style>
/* .selectize-input.items.has-options.full.has-items{
	height: 35px !important;
}
.item{
	width: 100%;
} */
</style>
<?php
// print_r($all_employees);
// exit;
$TransferType 			= (isset($_POST['TransferType'])) 						? $_POST['TransferType'] 					: $record['Transfer_type'];
$TransferCategory 		= (isset($_POST['TransferCategory'])) 					? $_POST['TransferCategory'] 				: $record['Transfer_category'];
$TransferReason 		= (isset($_POST['TransferReason'])) 					? $_POST['TransferReason'] 				: $record['Transfer_reason'];
$dateFrom 			= (isset($_POST['dateFrom'])) 						? $_POST['dateFrom'] 					: $record['Transfer_from'];
$dateTo 			= (isset($_POST['dateTo'])) 						? $_POST['dateTo'] 						: $record['Transfer_to'];
$returnDate 		= (isset($_POST['returnDate'])) 					? $_POST['returnDate'] 					: $record['Transfer_return'];
$TransferDays 			= (isset($_POST['TransferDays'])) 						? $_POST['TransferDays'] 					: $record['Transfer_days'];
$TransferDoc 		= 	($record['Transfer_doc'] != '') 	? 	$record['Transfer_doc'] 	: 	'';
$empAnnualTransfers = $chuttiyan
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "minDate", "-30" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
	$( "#returnDate" ).datepicker( "setDate", "<?php echo $returnDate; ?>" );
	
});
</script>
<?php if($canWrite == 1) { ?>
<form name="frmApplyTransfer" id="frmApplyTransfer" method="post" enctype="multipart/form-data">
<div class="listPageMain">
  <div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Transfer Employees</td>
    </tr>
    <tr>
      <td class="formLabelContainer">Select Employee:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="emp_id" id="select_page" style="width:200px;" class="operator">
        <option value="">Select Employee</option>
            <?php
              if (count($all_employees)) {
                for($ind = 0; $ind < count($all_employees); $ind++) {
                  if($all_employees[$ind]['emp_id'] != $employee_branch[0]['emp_id'])
                  {
              ?>
                <option value="<?php echo $all_employees[$ind]['emp_id']; ?>"
                <?php
                  if(isset($empTransferRequest[0]['emp_id']))
                  {
                    echo $empTransferRequest[0]['emp_id'] == $all_employees[$ind]['emp_id'] ? 'selected' : null;
                  }
                ?>
                ><?php echo $all_employees[$ind]['emp_code'].' -- '.$all_employees[$ind]['emp_full_name'].' -- '.$all_employees[$ind]['company_name']; ?></option>
              <?php
                  }
                }
              }
              ?>        
        </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Current Branch:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="TransferFrom" id="select_page" style="width:200px;" class="operator">
            <option value="">Select Branch</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            // if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            // {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($empTransferRequest[0]['company_id']))
            {
              echo $empTransferRequest[0]['company_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            // }
				  }
			  }
			  ?>
        </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Transfer To:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="TransferTo" id="select_page" style="width:200px;" class="operator">
            <option value="">Select Branch</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            // if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            // {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($empTransferRequest[0]['company_to_id']))
            {
              echo $empTransferRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            // }
				  }
			  }
			  ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Reason:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      <?php
        if(isset($empTransferRequest[0]['transfer_reason']))
        {
      ?>
      <input name="TransferReason"style="height:150px;border: 1px solid #80808094 !important;" value="<?php echo $empTransferRequest[0]['transfer_reason']?>">
      <?php 
        }else{
      ?>
      	<textarea class="textArea" name="TransferReason" id="TransferReason" style="height:150px"></textarea>
        <?php }?>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Add Supporting Document (If Any):</td>
      <td class="formTextBoxContainer">
      	<input type="file" name="TransferDoc" id="TransferDoc">
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Submit">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
  </div>
</form>
<script>
$('#TransferType').val('<?php echo $TransferType; ?>');
$('#TransferCategory').val('<?php echo $TransferCategory; ?>');
$('#TransferReason').val('<?php echo $TransferReason; ?>');
</script>
<br  />
<?php } ?>