
<?php
// print_r();
// exit;
$TransferType 			= (isset($_POST['TransferType'])) 						? $_POST['TransferType'] 					: $record['Transfer_type'];
$TransferCategory 		= (isset($_POST['TransferCategory'])) 					? $_POST['TransferCategory'] 				: $record['Transfer_category'];
$TransferReason 		= (isset($_POST['TransferReason'])) 					? $_POST['TransferReason'] 				: $record['Transfer_reason'];
$dateFrom 			= (isset($_POST['dateFrom'])) 						? $_POST['dateFrom'] 					: $record['Transfer_from'];
$dateTo 			= (isset($_POST['dateTo'])) 						? $_POST['dateTo'] 						: $record['Transfer_to'];
$returnDate 		= (isset($_POST['returnDate'])) 					? $_POST['returnDate'] 					: $record['Transfer_return'];
$TransferDays 			= (isset($_POST['TransferDays'])) 						? $_POST['TransferDays'] 					: $record['Transfer_days'];
$TransferDoc 		= 	($record['Transfer_doc'] != '') 	? 	$record['Transfer_doc'] 	: 	'';
$empAnnualTransfers = $chuttiyan
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "minDate", "-30" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
	$( "#returnDate" ).datepicker( "setDate", "<?php echo $returnDate; ?>" );
	
});
</script>
<?php if($canWrite == 1) { ?>
<form name="frmApplyTransfer" id="frmApplyTransfer" method="post" enctype="multipart/form-data">
<div class="listPageMain">
  <div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Transfer Application</td>
    </tr>
    <tr>
      <td class="formLabelContainer">Transfer From:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="TransferFrom" id="TransferFrom" class="dropDown">
            <option value="<?php echo isset($empTransferRequest[0]['company_from_id']) ? $empTransferRequest[0]['company_from_id'] : $employee_branch[0]['company_id']?>"><?php echo isset($empTransferRequest[0]['company_from']) ? $empTransferRequest[0]['company_from'] : $employee_branch[0]['company_name']?></option>
        </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Transfer To:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="TransferTo" id="TransferTo" class="dropDown">
            <option value="">Select Branch</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($empTransferRequest[0]['company_to_id']))
            {
              echo $empTransferRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Reason:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      <?php
        if(isset($empTransferRequest[0]['transfer_reason']))
        {
      ?>
      <input name="TransferReason"style="height:150px;border: 1px solid #80808094 !important;" value="<?php echo $empTransferRequest[0]['transfer_reason']?>">
      <?php 
        }else{
      ?>
      	<textarea class="textArea" name="TransferReason" id="TransferReason" style="height:150px"></textarea>
        <?php }?>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Add Supporting Document (If Any):</td>
      <td class="formTextBoxContainer">
      	<input type="file" name="TransferDoc" id="TransferDoc">
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Submit">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
  </div>
</form>
<script>
$('#TransferType').val('<?php echo $TransferType; ?>');
$('#TransferCategory').val('<?php echo $TransferCategory; ?>');
$('#TransferReason').val('<?php echo $TransferReason; ?>');
</script>
<br  />
<?php } ?>