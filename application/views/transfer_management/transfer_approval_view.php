
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;

	}
	.item{
		width: 100%;
	}
</style>

<form name="frmTranferStatus" id="frmTranferStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Transfer Approval View</div>
  </div>
</form>
		
	<div class="listContentMain">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		<tr class="listHeader">
			<td class="listHeaderCol">ID</td>
			<td class="listHeaderCol">Transfer ID</td>
			<td class="listHeaderCol">Full Name</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Employee Designation</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Working Email</td>
			<td class="listHeaderCol">Attachment</td>
			<td class="listHeaderCol">Comments</td>
			<td class="listHeaderCol">Status</td>
		</tr>
		<?php
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				if($arrRecords[$ind]['approval_status'] == 0) {
					$cssBtn = 'smallButtonUpdate';
					$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
					$strStatus = 'Pending';
				} elseif($arrRecords[$ind]['approval_status'] == 1) {
					$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
					$strStatus = 'Approved';
				} elseif($arrRecords[$ind]['approval_status'] == 2) {
					$trCSSClass = 'listContentAlternate" style="background-color:#F38374"';
					$strStatus = 'Rejected';
				}  else {
					$trCSSClass = 'listContentAlternate';
					$strStatus = '';
				}
			?>
			<tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
				<td class="listContentCol"><?php echo $arrRecords[$ind]['td_id']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['transfer_id']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['emp_designation']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords[$ind]['emp_work_email']; ?></td>
				<td class="listContentCol"><?php echo isset($arrRecords[$ind]['attachment']) ? $arrRecords[$ind]['attachment'] : "NULL" ; ?></td>
				<td class="listContentCol"><?php echo isset($arrRecords[$ind]['comments']) ? $arrRecords[$ind]['comments'] : "NULL" ; ?></td>
				<td class="listContentCol"><?php echo $strStatus; ?></td>
			</tr>
			<?php
				} 
			for($ind = 0; $ind < count($arrRecords2); $ind++) {
				if($arrRecords2[$ind]['approval_status'] == 0) {
					$cssBtn = 'smallButtonUpdate';
					$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
					$strStatus = 'Pending';
				} elseif($arrRecords2[$ind]['approval_status'] == 1) {
					$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
					$strStatus = 'Approved';
				} elseif($arrRecords2[$ind]['approval_status'] == 2) {
					$trCSSClass = 'listContentAlternate" style="background-color:#F38374"';
					$strStatus = 'Rejected';
				}  else {
					$trCSSClass = 'listContentAlternate';
					$strStatus = '';
				}
			?>
			<tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
				<td class="listContentCol"><?php echo $arrRecords2[$ind]['td_id']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords2[$ind]['transfer_id']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords2[$ind]['emp_full_name']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords2[$ind]['emp_designation']; ?></td>
				<td class="listContentCol"><?php echo $arrRecords2[$ind]['emp_work_email']; ?></td>
				<td class="listContentCol"><?php echo isset($arrRecords2[$ind]['attachment']) ? $arrRecords2[$ind]['attachment'] : "NULL" ; ?></td>
				<td class="listContentCol"><?php echo isset($arrRecords2[$ind]['comments']) ? $arrRecords2[$ind]['comments'] : "NULL" ; ?></td>
				<td class="listContentCol"><?php echo $strStatus; ?></td>
			</tr>
			<?php
				} 
			if(!$ind) {
			?>
			<tr class="listContentAlternate">
				<td colspan="8" align="center" class="listContentCol">No Record Found</td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="265px">Edit <span style="font-style:italic;color:#666">(Can only edit pending requests)</span></td>
        </tr>
    </table>
</div>
