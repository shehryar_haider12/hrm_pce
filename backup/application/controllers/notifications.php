<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller {
	
	public $arrData = array();
	public $currentController;
	private $maxLinks;
	private $limitRecords;
	
	function __construct() {
		
		parent::__construct();
		
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Karachi");
		$this->load->helper('common');
		$this->load->model('model_master', 'master', true);
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->setupConfig($this->configuration->getSettings()); # SETUP ALL SYSTEM WIDE VARIABLES REQUIRED IN CODE (TABLE hrm_configuration)
		$this->setupEmail(); # SETUP EMAIL ATTRIBUTES IF SMTP DETAILS ARE AVAILABLE
	
		$this->load->model('model_employee_management', 'employee', true);	
		$this->load->model('model_recruitment_management', 'recruitment', true);
		$this->load->model('model_user_management', 'user_management', true);
		$this->load->model('model_complain_management', 'complain', true);
		$this->load->model('model_attendance_management', 'attendance', true);
		$this->load->model('model_sbt_forum', 'forum', true);
		
		$this->arrData["baseURL"] 				= $this->config->item("base_url");
		$this->arrData["imagePath"] 			= $this->config->item("image_path");
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
	}
	
	function debugLogCron($msg, $logFilename = "dashboardcron.log", $live = "true")
	{
		if($live)
		{
			set_time_limit(0);
			ini_set("error_log", CRONLOGS_PATH.$logFilename);
			error_log($msg);
		}
		else
		{
			print("<br>[$msg]");
		}
	}	
	
	public function processEmails($statusID = 0) {
		
		$cronTime = '09:00';
		$timeStarted = time();		
		
		if(date("H:i", $timeStarted) == $cronTime) {
			
			$arrCanStatuses = $this->recruitment->getCandidateStatuses();			
			$arrRecords = $this->recruitment->getCandidateHistory(array(
																		# ONLY REJECTED
																		'status_id' => $statusID,  
																		# WHERE EMAIL NOT SENT
																		'email_sent' => 0,
																		# AND OR MINIMUM 24 HOURS EARLIER UPDATED
																		'performed_date < ' => date(DATE_TIME_FORMAT, strtotime('-1 day')) 
																	)
																);
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				
				$arrCandidate = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $arrRecords[$ind]['candidate_id']), false);
				$arrVacancy = $this->recruitment->getVacancies(array('v.vacancy_id' => $arrRecords[$ind]['vacancy_id']));
				$arrVacancy = $arrVacancy[0];
				
				# SHOOT EMAIL
				if($statusID == 6) {
					$emailTemplate = 'candidate_status.html';
				} else if($statusID == 8) {
					$emailTemplate = 'candidate_rejection.html';
				} else if($statusID == 9) {
					$emailTemplate = 'candidate_status.html';
				}
				$arrValues = array(
									'[HEADER_LOGO_LINK]' 	=> EMAIL_HEADER_LOGO,
									'[CANDIDATE_NAME]' 		=> $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
									'[VACANCY_NAME]' 		=> $arrVacancy['vacancy_name'],
									'[NEW_STATUS]' 			=> getValue($arrCanStatuses, 'status_id', $statusID, 'status_title'),
									'[COPYRIGHT_TEXT]' 		=> 'Copyright ' . date('Y') . ' PCE. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, $emailTemplate);
				$this->sendEmail(
									$arrTo = array(
													$arrCandidate['email']
													), 				# RECEIVER DETAILS
									'Application Status Changed', 	# SUBJECT
									$emailHTML						# EMAIL HTML MESSAGE
								);
								
				$arrData = array('email_sent' => 1);
				
				$arrWhere = array(
									'candidate_id' => $arrRecords[$ind]['candidate_id'],
									'vacancy_id' => $arrRecords[$ind]['vacancy_id'],
									'status_id' => $statusID,
									'email_sent' => 0
									);
									
				$this->recruitment->saveValues(TABLE_CANDIDATE_HISTORY, $arrData, $arrWhere);
			}
			echo '*** ' . $ind . ' Records Done ***';
		}
	}
	
	public function processTestEmail() {
				$emailHTML = getHTML($arrValues, 'introduction_to_HRMS.html');
				
				die($emailHTML);
				
				$this->sendEmail(
									$arrTo = array("hani9350@sbtjapan.com"),
									'Introducing SBT Employee Dashboard - Human Resource Management System',
									$emailHTML,
									SYSTEM_EMAIL_ADDRESS,
									'IT-Web Department'
								);
	}
	
	public function processEmployeeEmails() {
		
		set_time_limit(0);
		$this->db->select(' e.emp_id, e.emp_code, e.emp_status, e.emp_full_name, e.emp_last_name, e.emp_work_email, u.user_name, u.plain_password, u.user_status ');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where(array('e.emp_status' => 1, 'e.emp_id > ' => 0, 'e.emp_work_email != ' => '', 'u.user_status' => 1, 'e.modified_date >= ' => '2014-05-16', 'e.modified_by' => '2'));	// LAST SENT TILL 921
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrEmployees = $objResult->result_array();
		$objResult->free_result();
		diee($arrEmployees, true);
		
		for($ind = 0; $ind < count($arrEmployees); $ind++) {
			
			# SHOOT EMAIL
			if(!empty($arrEmployees[$ind]['emp_work_email'])) {
				
				$strEmail = explode('@', $arrEmployees[$ind]['emp_work_email']);
				$userName = trim($strEmail[0]);
				//die($arrEmployees[$ind]['emp_id'].'--');
				if($userName != $arrEmployees[$ind]['user_name']) {
					$this->db->where('employee_id', $arrEmployees[$ind]['emp_id']);
					$this->db->update(TABLE_USER, array('user_name' => $userName)); 
				} else {
					$userName = $arrEmployees[$ind]['user_name'];
				}
				
				$arrValues = array(
									'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
									'[EMPLOYEE_NAME]' 			=> $arrEmployees[$ind]['emp_full_name'],
									'[EMPLOYEE_DASHBOARD_LINK]' => $this->arrData["baseURL"],
									'[EMPLOYEE_USERNAME]' 		=> $userName,
									'[EMPLOYEE_PASSWORD]' 		=> $arrEmployees[$ind]['plain_password'],
									'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'introduction_to_HRMS.html');
				
				die($emailHTML);
				
				$this->sendEmail(
									$arrTo = array(
													$arrEmployees[$ind]['emp_work_email']
													), 															# RECEIVER DETAILS
									'Introducing SBT Employee Dashboard - Human Resource Management System', 	# SUBJECT
									$emailHTML,																	# EMAIL HTML MESSAGE
									SYSTEM_EMAIL_ADDRESS,
									'IT-Web Department'
								);
				echo 'Sent To: ' . $arrEmployees[$ind]['emp_work_email'] . '<br />';
			}
		}
		echo '*** ' . $ind . ' Records Done ***';
	}	
	
	public function processDetailsUpdateNotification() {
		
		set_time_limit(0);
		$this->db->select(' e.emp_id, e.emp_code, e.emp_status, e.emp_full_name, e.emp_work_email, u.user_name, u.plain_password, u.user_status ');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where(array('e.emp_status' => 1, 'e.emp_id > ' => 0, 'e.emp_work_email != ' => '', 'u.user_status' => 1, 'e.modified_date >= ' => '2014-05-16', 'e.modified_by' => '2'));	// LAST SENT TILL 921
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrEmployees = $objResult->result_array();
		$objResult->free_result();
		diee($arrEmployees, true);
		
		for($ind = 0; $ind < count($arrEmployees); $ind++) {
			
			# SHOOT EMAIL
			if(!empty($arrEmployees[$ind]['emp_work_email'])) {
				
				$strEmail = explode('@', $arrEmployees[$ind]['emp_work_email']);
				$userName = trim($strEmail[0]);
				//die($arrEmployees[$ind]['emp_id'].'--');
				if($userName != $arrEmployees[$ind]['user_name']) {
					$this->db->where('employee_id', $arrEmployees[$ind]['emp_id']);
					$this->db->update(TABLE_USER, array('user_name' => $userName)); 
				} else {
					$userName = $arrEmployees[$ind]['user_name'];
				}
				
				$arrValues = array(
									'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
									'[EMPLOYEE_NAME]' 			=> $arrEmployees[$ind]['emp_full_name'],
									'[EMPLOYEE_DASHBOARD_LINK]' => $this->arrData["baseURL"],
									'[EMPLOYEE_USERNAME]' 		=> $userName,
									'[EMPLOYEE_PASSWORD]' 		=> $arrEmployees[$ind]['plain_password'],
									'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'details_update_notification_and_login_details.html');
				
				die($emailHTML);
				
				$this->sendEmail(
									$arrTo = array(
													$arrEmployees[$ind]['emp_work_email']
													), 															# RECEIVER DETAILS
									'Details Update Notification - SBT HRM', 									# SUBJECT
									$emailHTML,																	# EMAIL HTML MESSAGE
									SYSTEM_EMAIL_ADDRESS,
									'IT-Web Department'
								);
				echo 'Sent To: ' . $arrEmployees[$ind]['emp_work_email'] . '<br />';
			}
		}
		echo '*** ' . $ind . ' Records Done ***';
	}
	
	public function sendPromotionalEmail() {
		
		set_time_limit(0);
		
		# SHOOT EMAIL
		$arrValues = array(
							'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'All Managers',
							'[MESSAGE_BODY]' => '
							<table width="100%">						
                            <tr><td style="color:#00769C;font-family:Arial;font-size:14px;line-height:20px;padding:10px 5px;text-align:center">
								Online Library on SBT-Dashboard is updated with another learning tool.<br /><br />
								Visit to see <b>"First Motivational Video"</b> available in <b><a href="http://hrm.sbtjapan.com/training_development/visual_learning" target="_blank">Visual Learning Center.</a></b>
							</td></tr>
              				<tr>
                				<td>&nbsp;
								</td>
							</tr>
              				<tr>
                				<td align="center"><div style="border:3px solid #89CFE6"><a href="http://hrm.sbtjapan.com/training_development/visual_learning" target="_blank"><img src="http://hrm.sbtjapan.com/images/motivational_videos_section.jpg" /></a></div>
								</td>
							</tr>
							</table>',
							'[EMPLOYEE_DASHBOARD_LINK]' => $this->arrData["baseURL"],
							'[ANNOUNCEMENT_REGARDING]' => 'Important announcement from <b>Admin Department</b> about the Parking Policy is',
							'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
							);
							
		$emailHTML = getHTML($arrValues, 'announcement_latest.html');
		die($emailHTML);
		$this->sendEmail(
							$arrTo = array(
											'sbthrm@sbtjapan.com'					# sbtpakistan@sbtjapan.com
											), 															# RECEIVER DETAILS
							'Announcement Regarding Parking Policy - SBT Dashboard', 								# SUBJECT
							$emailHTML,																	# EMAIL HTML MESSAGE
							SYSTEM_EMAIL_ADDRESS,
							'IT-Web Department'
						);
		
		echo '*** Records Done ***';
	}
	
	public function emailToManagers() {
		$arrTo = array();
		set_time_limit(0);
		$this->db->select(' e.emp_work_email ');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where("e.emp_status = 1 and e.emp_work_email != '' and u.user_status = 1 and (e.emp_job_category_id = 9 or e.emp_job_category_id = 3) and (u.user_role_id = 4 or u.user_role_id = 6)");
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e '); //echo $this->db->last_query();
		$arrEmployees = $objResult->result_array();
		$objResult->free_result();
		
		for($ind = 0; $ind < count($arrEmployees); $ind++) {
						
			if(!empty($arrEmployees[$ind]['emp_work_email'])) {
				$arrTo[] = $arrEmployees[$ind]['emp_work_email'];
			}
		}
		
		$arrTo[] = SYSTEM_EMAIL_ADDRESS;
		$arrTo[] = 'alihussain9208@sbtjapan.com';
		$arrTo[] = 'saleem9204@sbtjapan.com';
		diee($arrTo, true);
		
		# SHOOT EMAIL
		$arrValues = array(
							'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' 			=> 'All Managers',
							'[EMPLOYEE_DASHBOARD_LINK]' => $this->arrData["baseURL"],
							'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
							);
							
		$emailHTML = getHTML($arrValues, 'new_launch.html');
		die($emailHTML);
		$this->sendEmail(
							$arrTo, 															# RECEIVER DETAILS
							'Introducing Quality Control Module - SBT Dashboard', 								# SUBJECT
							$emailHTML,																	# EMAIL HTML MESSAGE
							SYSTEM_EMAIL_ADDRESS,
							'IT-Web Department'
						);
		
		echo '*** ' . $ind . ' Records Done ***';
	}	
	
	public function processEmployeePassword($empID) {
		
		# DO NOT COPY TO LIVE
		
		set_time_limit(0);
		$this->db->select(' e.emp_full_name, e.emp_work_email, u.user_name, u.plain_password ');
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->where(array('e.emp_status' => 1, 'e.emp_id' => (int)$empID));	// 502 - 142
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrEmployees = $objResult->result_array();
		$objResult->free_result();
		
		for($ind = 0; $ind < count($arrEmployees); $ind++) {
			
			# SHOOT EMAIL
			
			if(!empty($arrEmployees[$ind]['emp_work_email'])) {
				
				$arrValues = array(
									'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
									'[EMPLOYEE_NAME]' 			=> $arrEmployees[$ind]['emp_full_name'],
									'[EMPLOYEE_DASHBOARD_LINK]' => $this->arrData["baseURL"],
									'[EMPLOYEE_USERNAME]' 		=> $arrEmployees[$ind]['user_name'],
									'[EMPLOYEE_PASSWORD]' 		=> $arrEmployees[$ind]['plain_password'],
									'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'retrieve_login_details_emp.html');
				//die($emailHTML);
				$this->sendEmail(
									$arrTo = array(
													$arrEmployees[$ind]['emp_work_email']
													), 															# RECEIVER DETAILS
									'Login Details - SBT HRM', 	# SUBJECT
									$emailHTML,																	# EMAIL HTML MESSAGE
									SYSTEM_EMAIL_ADDRESS,
									'IT-Web Department'
								);
				echo 'Sent To: ' . $arrEmployees[$ind]['emp_work_email'] . '<br />';
			}
		}
		echo '*** ' . $ind . ' Records Done ***';
	}
	
	public function processCandidatePassword($candidateID) {
		
		# DO NOT COPY TO LIVE
		
		set_time_limit(0);
		$this->db->select(' c.*, u.user_name, u.plain_password ');
		$this->db->join(TABLE_USER . ' u ', 'c.candidate_id = u.candidate_id', 'left');
		$this->db->where(array('c.arrival_source' => 'Mustakbil', 'c.created_date >= ' => '2014-11-05 00:00:00'));	// 502 - 142
		
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c ');
		$arrCandidates = $objResult->result_array();
		$objResult->free_result();
		diee($arrCandidates, true);
		//die();
		for($ind = 0; $ind < count($arrCandidates); $ind++) {
			
			# SHOOT EMAIL
			if(!empty($arrEmployees[$ind]['emp_work_email'])) {
						
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[CANDIDATE_NAME]' => $arrCandidates[$ind]['first_name'] . ' ' . $arrCandidates[$ind]['last_name'],
									'[CANDIDATE_DASHBOARD_LINK]' => $this->arrData["baseURL"].'/career',
									'[CANDIDATE_USERNAME]' => $arrCandidates[$ind]['user_name'],
									'[CANDIDATE_PASSWORD]' => $arrCandidates[$ind]['plain_password'],
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'new_candidate.html');
				
				$this->sendEmail(
									$arrTo = array(
													$arrCandidates[$ind]['email']
													), 				# RECEIVER DETAILS
									'Welcome To SBT Japan', 		# SUBJECT
									$emailHTML						# EMAIL HTML MESSAGE
								);
								
				echo 'Sent To: ' . $arrCandidates[$ind]['email'] . '<br />';
			}
		}
		echo '*** ' . $ind . ' Records Done ***';
	}
	
	public function createEmpUser() {
		
		$arrEmployees = $this->employee->getEmployees();
		$jnd = 0;
		
		for($ind = 0; $ind < count($arrEmployees); $ind++) {
			
			if(!(int)$arrEmployees[$ind]['user_exists']) {
				
				$strEmail = explode('@', $arrEmployees[$ind]['emp_work_email']);
				$userName = $strEmail[0];
				$passPhrase = randomString(8);
				
				if(strlen($userName) > 6) {
					$arrValues = array(
							'user_role_id' => 2,
							'employee_id' => $arrEmployees[$ind]['emp_id'],
							'user_name' => $userName,
							'plain_password' => $passPhrase,
							'password' => md5($passPhrase),
							'user_status' => 1,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);		
							
					$this->user_management->saveValues(TABLE_USER, $arrValues);
					$jnd++;
				}				
			}			
		}
		echo $jnd . ' Users Created';
	}
	
	public function sendAbsentAlert($byPass = 0) {
		
		$cronTime = '10:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			$currDay = date('D');
			
			if($currDay == 'Mon') {
				$dateFrom 	= date('Y-m-d', strtotime(' -2 day'));
				$dateTo 	= date('Y-m-d', strtotime(' -3 day'));
			} else if($currDay == 'Tue') {
				$dateFrom 	= date('Y-m-d', strtotime(' -1 day'));
				$dateTo 	= date('Y-m-d', strtotime(' -3 day'));
			} else {
				$dateFrom 	= date('Y-m-d', strtotime(' -1 day'));
				$dateTo 	= date('Y-m-d', strtotime(' -2 day'));
			}
		
			$arrCodes1				= array();
			$arrWhere 				= array();
			$arrWhere['a.DATE'] 		= $dateFrom;
			$arrWhere['a.DAYTYPE'] 	= 'WORKDAY';
			$arrWhere['a.IN'] 		= '';
			$arrWhere['a.OUT'] 		= '';
			$arrWhere['a.WORK'] 		= '';
			$arrWhere['a.REMARK'] 	= '';
					
			$arrRecords				= $this->attendance->getAttendanceRecords($arrWhere);
			
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				if($arrRecords[$ind]['BADGENO'] > 2000) {
					$arrCodes1[] = $arrRecords[$ind]['BADGENO'];
				}
			}
			
			$arrCodes2				= array();
			$arrWhere 				= array();
			$arrWhere['a.DATE'] 		= $dateTo;
			$arrWhere['a.DAYTYPE'] 	= 'WORKDAY';
			$arrWhere['a.IN'] 		= '';
			$arrWhere['a.OUT'] 		= '';
			$arrWhere['a.WORK'] 		= '';
			$arrWhere['a.REMARK'] 	= '';
					
			$arrRecords				= $this->attendance->getAttendanceRecords($arrWhere);
			
			for($ind = 0; $ind < count($arrRecords); $ind++) {
				if($arrRecords[$ind]['BADGENO'] > 2000) {
					$arrCodes2[] = $arrRecords[$ind]['BADGENO'];
				}
			}
			
			$arrCodes = array_intersect($arrCodes1, $arrCodes2);
			$strCodes = implode(',' , $arrCodes);
			
			$arrEmployees = $this->employee->getEmployees(array('e.emp_status' => 1, 'e.emp_employment_status < ' => 6, 'e.emp_code in ' => '(' . $strCodes . ')'));
			
			
			$strEmailTxt 			= 'Following are the employees who are absent since last 2 days.<br /><br />
										<table>
											<tr>
												<td width="250px"><b>Employee Code</b></td>
												<td><b>Employee Name</b></td>
											</tr>
											<tr>
												<td width="250px"></td>
												<td></td>
											</tr>
											';
											
			for($ind = 0; $ind < count($arrEmployees); $ind++) {
				$strEmailTxt .= '<tr>
								<td>'.$arrEmployees[$ind]['emp_code'].'</td>
								<td>'.$arrEmployees[$ind]['emp_full_name'] .'</td>
								</tr>';
			}
			
			$strEmailTxt .= '</table>';
			
			# SHOOT EMAIL
			# Template: general.html
	
			$arrValues = array(
						'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
						'[DASHBOARD_LINK]' => $this->arrData["baseURL"] . '/' . $this->currentController . '/requisitions_list',
						'[EMPLOYEE_NAME]' => 'HR Team',
						'[MESSAGE_BODY]' => $strEmailTxt,
						'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
						);
						
			$emailHTML = getHTML($arrValues, 'general.html');
			die($emailHTML);
			
			$this->sendEmail(
								$arrTo = array(
												'saleem9204@sbtjapan.com',
												SYSTEM_EMAIL_ADDRESS
												), 															# RECEIVER DETAILS
								'*** Absent Alert *** - SBT Dashboard',	 									# SUBJECT
								$emailHTML,																	# EMAIL HTML MESSAGE
								SYSTEM_EMAIL_ADDRESS,
								'IT Web Department'
							);
							
			echo 'Alerts Sent';
		}
		
		exit;
	}
	
	public function sendExitNotification($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
		
			$arrWhere = array();
			$arrWhere['er.resignation_step'] = '1';
			$arrWhere['er.resignation_last_update < '] = date('Y-m-d H:i:s', strtotime("-3 day"));
					
			$arrRecords	= $this->employee->getResignations($arrWhere);
			
			
			$this->db->select(' er.*, e.emp_full_name, e.emp_authority_id, e.emp_gender, e.emp_photo_name ');				
			$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = er.resignation_emp_id', 'left');
			$this->db->where("(er.resignation_step = 1 and er.resignation_last_update < '".date('Y-m-d H:i:s', strtotime("-3 day"))."') or (er.resignation_step = 3 and er.resignation_emp_target_end_date < '".date('Y-m-d H:i:s', strtotime("-1 day"))."')");		
			$objResult = $this->db->get(TABLE_EMPLOYEE_RESIGNATION . ' er ');
			$arrRecords = $objResult->result_array();
			$objResult->free_result();
			
			$totalSent = 0;
			for($ind = 0; $ind < count($arrRecords); $ind++) {
			
				$strEmailTxt = $arrRecords[$ind]['emp_full_name'] . '\'s resignation process is waiting for your response.<br />
											You are hereby requested to provide your comments/feedback as soon as possible.<br /><br />
											Link: <a href="http://hrm.sbtjapan.com/employee_management/resignation/' . $arrRecords[$ind]['emp_id'] . '" target="_blank"></a>
											<br /><br />
											';
				
				$arrSupervisor = $this->employee->getEmployeeDetail(array('e.emp_id' => $arrRecords[$ind]['emp_authority_id'], 'e.emp_employment_status < ' => 6, 'e.emp_status' => 1), false);
				
				if($arrSupervisor['emp_work_email'] != '') {
					
					# SHOOT EMAIL
					# Template: general.html
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => $arrSupervisor['emp_full_name'],
								'[MESSAGE_BODY]' => $strEmailTxt,
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'general.html');
					//die($emailHTML);
					
					$this->sendEmail(
										$arrTo = array(
														$arrSupervisor['emp_work_email'],
														'saleem9204@sbtjapan.com',
														'jibran9244@sbtjapan.com',
														SYSTEM_EMAIL_ADDRESS
														), 															# RECEIVER DETAILS
										'Response Required on Resignation Process - SBT Dashboard',	 				# SUBJECT
										$emailHTML,																	# EMAIL HTML MESSAGE
										SYSTEM_EMAIL_ADDRESS,
										'IT Web Department'
									);
					
					$totalSent++;
				}
			}
			echo $totalSent . ' Alerts Sent';
		}
		exit;
	}
	
	public function uploadCSV() {
		
		$fileName = 'C:\wamp\www\svnprojects\ci\docs\Karachi_Areas.csv';
		if ($fileHandler = fopen($fileName, "r")) {
				
			while (!feof($fileHandler) ) {
												
				$csvRow = fgetcsv($fileHandler);
					
				$strArea = trim($csvRow[0]);
				
				if($strArea != '') {
					$arrValues = array(
										'location_name' => $strArea,
										'location_parent_id' => 290,
										'location_type_id' => 4,
										'location_status' => 1,
										'created_by' => '0', 
										'created_date' => date($this->arrData["dateTimeFormat"])
									);				
					$this->recruitment->saveValues(TABLE_LOCATION, $arrValues); echo $this->db->last_query() . ';<br />';
				}
				
			}
			fclose($fileHandler);
		}
	}
	
	public function cleanSMSFile() {	
	
		$fileName = BULK_SMS_FILE_FOLDER . 'Numbers-to-clean.csv';
			
		#print($fileName);		die();
		
		
		if ($fileHandler = fopen($fileName, "r")) {					
			while (!feof($fileHandler)) {																					
				$csvRow = fgetcsv($fileHandler);
				$mobNumber = cleanNumber($csvRow[0]);
				if(strlen($mobNumber) == 11) {
					$arrTo[] = $mobNumber;
				}
			}
			fclose($fileHandler);
		}
		
		$arrTo = array_unique($arrTo);
		
		if(count($arrTo)) {
			
			$strCSV = "";			
			for($ind = 0; $ind < count($arrTo); $ind++) {
				$strCSV .=  $arrTo[$ind] . "\n";
			}
			
			$fileName = BULK_SMS_FILE_FOLDER . 'clean/clean_file_' . date('YmdHis') . '.csv';
			$opFile = fopen($fileName, 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			echo '<a href=".' . $fileName . '">View File</a>';
		}
		exit;
	}
	
	private function setupConfig($arrConfig) {
		for($ind = 0; $ind < count($arrConfig); $ind++) {
			if(!defined($arrConfig[$ind]['config_key'])) {
				define($arrConfig[$ind]['config_key'], $arrConfig[$ind]['config_value']);
			}
		}
	}
	
	private function setupEmail() {
		
		if(SMTP_HOST != '') {
					
			$this->config->set_item('protocol', 'smtp');
			$this->config->set_item('smtp_host', SMTP_HOST);
			$this->config->set_item('smtp_user', SMTP_USER);
			$this->config->set_item('smtp_pass', SMTP_PASSWORD);
			$this->config->set_item('smtp_port', SMTP_PORT);
			
		} else {
			$this->config->set_item('protocol', 'sendmail');
		}
	}
	
	public function sendEmail($arrTo = array(), $strSubject, $strMessage, $fromEmail = SYSTEM_EMAIL_ADDRESS, $fromName = SYSTEM_SENDER_NAME, $arrAttachment = array(), $boolSend = true) {
		
		if((int)SYSTEM_SEND_NOTIFICATION_EMAILS) {
		//	SYSTEM_SEND_NOTIFICATION_EMAILS is ON LOCAL DB ONLY		-		To send email from Local Code , switch ON SYSTEM_SEND_NOTIFICATION_EMAILS from LOCAL SYSTEM CONFIGURATION
			
			require_once(APPPATH . 'libraries/PHPMailerAutoload.php');
			$objMail = new PHPMailer();
			
			if(SMTP_HOST != '') { 			
				$objMail->isSMTP();
				//$objMail->SMTPDebug = 2;
				$objMail->SMTPAuth = false;				//		REASON::	Keep false for relay.sbtjapan.com
				$objMail->Host = SMTP_HOST;
				$objMail->Port = SMTP_PORT;				
				$objMail->Username = SMTP_USER;
				$objMail->Password = SMTP_PASSWORD;							
			} else {
				$objMail->isSendmail();
			}
			
			$objMail->setFrom($fromEmail, $fromName);
			
			for($ind = 0; $ind < count($arrTo); $ind++) {
				$objMail->addAddress($arrTo[$ind]);
			}
			
			$objMail->Subject = $strSubject;
			
			$objMail->msgHTML($strMessage);
			
			if(count($arrAttachment)) {
				for($ind = 0; $ind < count($arrAttachment); $ind++) {
					$objMail->AddAttachment($arrAttachment[$ind]);
				}
			}
			
			if (!$objMail->send()) {
				echo "Mailer Error: " . $objMail->ErrorInfo; die();
				return false;
			}
		}
		return true;
	}
	
	public function sendMultiEmail($arrTo = array(), $strSubject, $strMessage, $fromEmail = SYSTEM_EMAIL_ADDRESS, $fromName = SYSTEM_SENDER_NAME, $arrAttachment = array(), $boolSend = true) {
		
		if((int)SYSTEM_SEND_NOTIFICATION_EMAILS) {
		//	SYSTEM_SEND_NOTIFICATION_EMAILS is ON LOCAL DB ONLY		-		To send email from Local Code , switch ON SYSTEM_SEND_NOTIFICATION_EMAILS from LOCAL SYSTEM CONFIGURATION
			
			require_once(APPPATH . 'libraries/PHPMailerAutoload.php');
			$objMail = new PHPMailer();
			
			if(SMTP_HOST != '') { 			
				$objMail->isSMTP();
				//$objMail->SMTPDebug = 2;
				$objMail->SMTPAuth = false;				//		REASON::	Keep false for relay.sbtjapan.com
				$objMail->Host = SMTP_HOST;
				$objMail->Port = SMTP_PORT;				
				$objMail->Username = SMTP_USER;
				$objMail->Password = SMTP_PASSWORD;							
			} else {
				$objMail->isSendmail();
			}
			
			$objMail->setFrom($fromEmail, $fromName);
			
			for($ind = 0; $ind < count($arrTo); $ind++) {
				if($arrTo[$ind]['isBCC']) {
					$objMail->addBCC($arrTo[$ind]['emailAdd']);
				} else if($arrTo[$ind]['isCC']) {
					$objMail->addCC($arrTo[$ind]['emailAdd']);
				} else {
					$objMail->addAddress($arrTo[$ind]['emailAdd']);
				}
			}
			
			$objMail->Subject = $strSubject;
			
			$objMail->msgHTML($strMessage);
			
			if(count($arrAttachment)) {
				for($ind = 0; $ind < count($arrAttachment); $ind++) {
					$objMail->AddAttachment($arrAttachment[$ind]);
				}
			}
			
			if (!$objMail->send()) {
				echo "Mailer Error: " . $objMail->ErrorInfo; die();
				return false;
			}
		}
		
		return true;
	}
}

/* End of file notifications.php */
/* Location: ./application/controllers/notifications.php */