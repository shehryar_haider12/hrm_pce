<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task_Management extends Master_Controller {
	
	private $arrData = array();
	public 	$arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_task_management', 'task', true);
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->arrData["docFolder"]				= LM_ACTIVITY_DOCS_FOLDER;
		$this->arrData["docFolderShow"]			= str_replace('./', '', LM_ACTIVITY_DOCS_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->HREmailAddress					= HR_EMAIL_ADDRESS;
		$this->arrData["GMEmpID"]				= GM_EMP_ID; 	# MR. ALI HUSSAIN
		$this->HRPMTEmailAddress				= HR_PERSON_PMT;
		$this->SystemEmailAddress				= SYSTEM_EMAIL_ADDRESS;
		$this->arrData["hrAdminEmpID"]			= $this->config->item('hr_admin_emp_id');
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		$this->arrData["kpiAssessment"]			= $this->config->item('kpi_assessment');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		$this->arrData['strHierarchy'] = $this->employee->getHierarchyWithMultipleAuthorities($this->userEmpNum);
		
		$this->arrData['skipParams'] = array(
												'save_assessment',
												'my_kpis'
											);
		
		if(!in_array($this->currentAction, $this->arrData['skipParams'])) {
			
			$this->employeeID = (int)$this->uri->segment(3);
			
			if(!$this->employeeID) {
				$this->employeeID = $this->userEmpNum;
			}
						
			$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID), false);
			$this->arrData['empJobCategoryID'] = $this->arrData['arrEmployee']['emp_job_category_id'];
			
			if($this->employeeID == $this->userEmpNum) {
				$this->arrData['ownUser'] = true;
			}
			
			$arrEmpSupervisors = getEmpSupervisors($this->employeeID);
			
			if(
				(($this->userRoleID == EMPLOYEE_ROLE_ID && !$this->arrData['ownUser']) || 
				(!$this->arrData['ownUser'] && !in_array($this->userEmpNum, $arrEmpSupervisors))) &&
				(!isAdmin($this->userRoleID) && count(array_diff($arrEmpSupervisors, explode(',', $this->arrData['strHierarchy']))) == count($arrEmpSupervisors))
				) {
					redirect($this->baseURL . '/message/access_denied');
					exit;
				} else if(!isAdmin($this->userRoleID) && !$this->arrData['ownUser']) {
									
					$this->arrData['canWrite'] = NO;
					$this->arrData['canDelete'] = NO;
					
				}
		}		
		
		if(!count($this->arrData['arrEmployee'])) {
			$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
			$this->arrData['empJobCategoryID'] = $this->arrData['arrEmployee']['emp_job_category_id'];
		}
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'task_management/index', $this->arrData);
		$this->template->render();
		
	}
	
	public function my_tasks($assessmentID)
	{
		$this->employeeID = $this->userEmpNum;
		$arrWhere = array();
		$arrWhere['emp_id'] = $this->employeeID;
				
		if(($this->input->post('month')) || ($this->input->post('year')))
		{
			$arrWhere['assessment_month'] = $this->input->post('month');
			$arrWhere['assessment_year'] = $this->input->post('year');
		}
		
		# CODE FOR DELETING RECORD
		
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if($this->task->deleteValue(TABLE_TASK_EMPLOYEE_PERFORMANCE, array('assessment_id' => (int)$this->input->post("record_id")))) {
					echo "1"; exit;
				} else {				
					# SET LOG
					echo "0"; exit;
				}		
			}
		}
		
		#################################### FORM VALIDATION START ####################################	
		
		# EXPORT ASSESSMENT REPORT AS PDF
		
		if((int)$assessmentID) {
			
			$arrAssessment = $this->task->getTasks(array('assessment_id' => $assessmentID));
			$arrAssessment = $arrAssessment[0];
			$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => $arrAssessment['emp_id']), false);
			
			if(!isAdmin($this->userRoleID)) {
				if($arrAssessment['emp_id'] != $this->userEmpNum && !in_array($this->userEmpNum, getEmpSupervisors($arrAssessment['emp_id']))) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
			
			$arrKPIs = json_decode($arrAssessment['assessment_kpi'], true);
			$strHTMLKPIs = '';
			$jnd = 1;
            for($ind = 0; $ind < count($arrKPIs); $ind++) {
				$strHTMLKPIs .= '<tr><td valign="top" width="5%" align="center">' . $jnd . '</td>
                    <td valign="top" width="47%">' . nl2br($arrKPIs[$ind]) . '</td>
					<td valign="top" width="48%">' . nl2br($arrKPIs[$ind + 1]) . '</td></tr>';
					$ind++;
					$jnd++;
			}
			
			$arrAccomplishments = json_decode($arrAssessment['assessment_accomplishments'], true);
			$strHTMLAccomplishments = '';
            for($ind = 0; $ind < count($arrAccomplishments); $ind++) {
				$strHTMLAccomplishments .= '<tr><td valign="top" width="5%" align="center">' . ($ind + 1) . '</td>
                    <td valign="top" width="95%">' . nl2br($arrAccomplishments[$ind]) . '</td></tr>';
			}
			
			$arrStrengths = json_decode($arrAssessment['assessment_strengths'], true);
			$strHTMLStrength = '';
            for($ind = 0; $ind < count($arrStrengths); $ind++) {
				$strHTMLStrength .= '<tr><td valign="top" width="5%" align="center">' . ($ind + 1) . '</td>
                    <td valign="top" width="95%">' . nl2br($arrStrengths[$ind]) . '</td></tr>';
			}
			
			$arrImprovement = json_decode($arrAssessment['assessment_improvement'], true);
			$strHTMLImprovement = '';
            for($ind = 0; $ind < count($arrImprovement); $ind++) {
				$strHTMLImprovement .= '<tr><td valign="top" width="5%" align="center">' . ($ind + 1) . '</td>
                    <td valign="top" width="95%">' . nl2br($arrImprovement[$ind]) . '</td></tr>';
			}
			
			$arrChallenges = json_decode($arrAssessment['assessment_challenges'], true);
			$strHTMLChallenges = '';
            for($ind = 0; $ind < count($arrChallenges); $ind++) {
				$strHTMLChallenges .= '<tr><td valign="top" width="5%" align="center">' . ($ind + 1) . '</td>
                    <td valign="top" width="95%">' . nl2br($arrChallenges[$ind]) . '</td></tr>';
			}
			
			$arrSkills = json_decode($arrAssessment['assessment_skills'], true);
			$strHTMLSkills = '';
            for($ind = 0; $ind < count($arrSkills); $ind++) {
				$strHTMLSkills .= '<tr><td valign="top" width="5%" align="center">' . ($ind + 1) . '</td>
                    <td valign="top" width="95%">' . nl2br($arrSkills[$ind]) . '</td></tr>';
			}
			
			$arrValues = array(
								  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
								  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),							  
								  '[EMPLOYEE_NAME]' 			=> $arrEmployee['emp_full_name'],
								  '[SELF_APPRAISAL_DATE]' 		=> date('F, Y', strtotime($arrAssessment['assessment_year'] . '-' . $arrAssessment['assessment_month'] . '-01')),
								  '[EMPLOYEE_DESIGNATION]' 		=> $arrEmployee['emp_designation'],
								  '[EMPLOYEE_DEPARTMENT]' 		=> $arrEmployee['job_category_name'],
								  '[EMPLOYEE_SUPERVISOR_NAME]' 	=> getSupervisorName($arrEmployee['emp_id']),
								  
								  '[KPIS]'						=> $strHTMLKPIs,
								  '[SIGNIFICANT_ACCOMPLISHMENTS]'	=> $strHTMLAccomplishments,
								  '[AREAS_OF_STRENGTH]'			=> $strHTMLStrength,
								  '[AREAS_OF_IMPROVEMENT]'		=> $strHTMLImprovement,
								  '[CHALLENGES_AT_WORK]'		=> $strHTMLChallenges,
								  '[SKILLS_TO_IMPROVE]'			=> $strHTMLSkills,
								  '[SUPERVISOR_COMMENTS]' 		=> nl2br($arrAssessment['assessment_remarks']),
								  '[SUPERVISOR_COMMENTS_BY]' 	=> getEmployeeName($arrAssessment['assessment_remarks_by']),
								  '[SUPERVISOR_COMMENTS_DATETIME]' => date(SHOW_DATE_TIME_FORMAT, strtotime($arrAssessment['assessment_remarks_date']))
							  );
							  
			$strHTML = getHTML($arrValues, 'self_appraisal_form.html');
			
			require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
			
			$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor(PDF_AUTHOR);
			$pdf->setCellHeightRatio(1);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetFontSize(8);				  
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);				  
			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			
			// add a page
			$pdf->AddPage();			  
			// output the HTML content
			$pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
			$pdf->lastPage();
			$pdfFileName = 'Self_Appraisal_' . $assessmentID . '.pdf';
			$pdf->Output($pdfFileName, 'D');
			
			header('Content-Type: text/doc');
			header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
			header('Cache-Control: max-age=0');
			readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
			
			exit;
		}
			
		if(($this->input->post('month')) || ($this->input->post('year')))
		{
			$this->form_validation->set_rules('month', 'Select Month', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('year', 'Select Year', 'trim|required|numeric|xss_clean');
		}
		
		if($this->input->post('selectedMonth') || $this->input->post('selectedYear'))
		{
			$this->form_validation->set_rules('selectedMonth', 'Select Month', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('selectedYear', 'Select Year', 'trim|required|numeric|xss_clean');
			
			if(!array_filter($this->input->post('txt0'))) {
				$this->form_validation->set_rules('txt0[]', 'Job Specific KPIs', 'trim|required|xss_clean');
			}
			
			if(!array_filter($this->input->post('txt1'))) {
				$this->form_validation->set_rules('txt1[]', 'Significant Accomplishments', 'trim|required|xss_clean');
			}
			
			if(!array_filter($this->input->post('txt2'))) {
				$this->form_validation->set_rules('txt2[]', 'Areas of Strength', 'trim|required|xss_clean');
			}
			
			if(!array_filter($this->input->post('txt3'))) {
				$this->form_validation->set_rules('txt3[]', 'Areas of Improvement', 'trim|required|xss_clean');
			}
			
			if(!array_filter($this->input->post('txt4'))) {
				$this->form_validation->set_rules('txt4[]', 'Constraints/Challenges you face at work', 'trim|required|xss_clean');
			}
			
			if(!array_filter($this->input->post('txt5'))) {
				$this->form_validation->set_rules('txt5[]', 'Skills to Acquire to Improve your Performance', 'trim|required|xss_clean');
			}
			
			$this->form_validation->set_rules('selStatus', 'Status', 'trim|required|numeric|xss_clean');
		}
		#################################### FORM VALIDATION END ######################################
		
		if($this->input->post('selectedMonth') || $this->input->post('selectedYear'))
		{	
			if ($this->form_validation->run() == true)
			{
				$arrWhere['emp_id'] = $this->employeeID;
								
				$arrTask = $this->task->getTasks($arrWhere);
				
				if($arrTask[0]['assessment_status_id'] > STATUS_ENTERING_TASKS)
				{
					$this->arrData['validation_error_message'] = 'Your self-assessment form has been forwarded for assessment, you can not modify the details.';					
				}
				else if(count($arrTask) > 0)
				{
					$arrValues = array(
									'emp_id' => $arrTask[0]['emp_id'],
									'assessment_month' => $arrTask[0]['assessment_month'],
									'assessment_year' => $arrTask[0]['assessment_year'],
									'assessment_kpi' => json_encode($this->input->post('txt0')),
									'assessment_accomplishments' => json_encode(array_values(array_filter($this->input->post('txt1')))),
									'assessment_challenges' => json_encode(array_values(array_filter($this->input->post('txt2')))),
									'assessment_strengths' => json_encode(array_values(array_filter($this->input->post('txt3')))),
									'assessment_improvement' => json_encode(array_values(array_filter($this->input->post('txt4')))),
									'assessment_skills' => json_encode(array_values(array_filter($this->input->post('txt5')))),
									'assessment_status_id' => (int)$this->input->post('selStatus'),
									'modified_by' => $this->employeeID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
								  );
								  
					$this->task->saveValues(TABLE_TASK_EMPLOYEE_PERFORMANCE, $arrValues, $arrWhere);
				}
				else
				{
					$arrValues = array(
									'emp_id' => $this->employeeID,
									'assessment_month' => $this->input->post('selectedMonth'),
									'assessment_year' => $this->input->post('selectedYear'),
									'assessment_kpi' => json_encode($this->input->post('txt0')),
									'assessment_accomplishments' => json_encode(array_values(array_filter($this->input->post('txt1')))),
									'assessment_challenges' => json_encode(array_values(array_filter($this->input->post('txt2')))),
									'assessment_strengths' => json_encode(array_values(array_filter($this->input->post('txt3')))),
									'assessment_improvement' => json_encode(array_values(array_filter($this->input->post('txt4')))),
									'assessment_skills' => json_encode(array_values(array_filter($this->input->post('txt5')))),
									'assessment_status_id' => (int)$this->input->post('selStatus'),
									'created_by' => $this->employeeID,
									'created_date' => date($this->arrData["dateTimeFormat"])
								  );
								  
					$this->task->saveValues(TABLE_TASK_EMPLOYEE_PERFORMANCE, $arrValues);
				}
				
				if($this->input->post("selStatus") == STATUS_SUBMIT_TASKS_FOR_REVIEW)
				{
					# SHOOT EMAIL
					# Template: tms_notificaiton.html
					
					$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID),false);			
					$empName = $arrEmployeeDetails['emp_full_name'];
					
					$arrTo = array();
					$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => $this->employeeID));
					foreach($arrSupervisors as $arrSupervisor) {
						$arrTo[] = $arrSupervisor['emp_work_email'];
					}
					
					$month = date('F', mktime(0, 0, 0, $this->input->post('selectedMonth'), 10));
					$year = $this->input->post('selectedYear');
					
					$message = $empName." has submitted self-appraisal form of ".$month." ".$year." for your Review.";
					
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => getSupervisorName($this->employeeID),
								'[MESSAGE_BODY]' => $message,
								'[TASK_DASHBOARD_LINK]' => $this->baseURL . '/task_management/my_team_tasks',
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'tms_notification.html');
					
					$this->sendEmail(
										$arrTo, 													# RECEIVER DETAILS
										'Self Appraisal' . EMAIL_SUBJECT_SUFFIX, 					# SUBJECT
										$emailHTML													# EMAIL HTML MESSAGE
									);
									
				}
						
				if($this->input->post("selStatus") == STATUS_ENTERING_TASKS) {
					$this->session->set_flashdata('success_message', 'Current self-appraisal saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/my_tasks');
					exit;
				} else if($this->input->post("selStatus") == STATUS_SUBMIT_TASKS_FOR_REVIEW) {
					$this->session->set_flashdata('success_message', 'Your self-appraisal has been submitted for review');
					redirect($this->baseURL . '/' . $this->currentController . '/my_tasks');
					exit;
				}
			} else {	
				$this->arrData['validation_error_message'] = validation_errors();				
			}
		}
		
		if(($this->input->post('month')) && ($this->input->post('year')))
		{
			if ($this->form_validation->run() == true)
			{
				$arrWhere['emp_id'] = $this->employeeID;								
				$this->arrData['arrTasks'] = $this->task->getTasks($arrWhere);
				$this->arrData['arrReviewerDetails'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->arrData['arrTasks'][0]['assessment_remarks_by']), false);
			}
		}				
		
		$this->arrData['performanceStatus'] = $this->config->item('performance_status');
		$this->arrData['months']			= $this->config->item('months');
		$this->arrData['frmActionURL'] 		= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'task_management/my_tasks', $this->arrData);
		$this->template->render();
	}
	
	public function my_team_tasks()
	{		
		$this->employeeID = $this->userEmpNum;
		$arrWhere = array();
		$arrWhere['emp_id'] = $this->input->post('empCode');
		
		if(($this->input->post('empCodePending')) || ($this->input->post('monthPending')) || ($this->input->post('yearPending')))
		{
			$arrWhere['emp_id'] = $this->input->post('empCodePending');
			$arrWhere['assessment_month'] = $this->input->post('monthPending');
			$arrWhere['assessment_year'] = $this->input->post('yearPending');
		} 
		else if(($this->input->post('month')) || ($this->input->post('year')))
		{
			$arrWhere['assessment_month'] = $this->input->post('month');
			$arrWhere['assessment_year'] = $this->input->post('year');
		} else {
			$arrWhere['assessment_month'] = "";
			$arrWhere['assessment_year'] = date('Y');
		}
						
		#################################### FORM VALIDATION START ####################################		
		if($this->input->post('selStatus') == STATUS_SUBMIT_TASKS_FOR_REVIEW || $this->input->post('selStatus') == STATUS_SUBMIT_REVIEW_TO_HR) {
			$this->form_validation->set_rules('supervisorComments', 'Supervisor\'s Comments', 'trim|required|xss_clean');
		} else if(($this->input->post('month')) || ($this->input->post('year')) || ($this->input->post('empCode')))
		{
			$this->form_validation->set_rules('empCode', 'Select Employee', 'trim|required|xss_clean');
			$this->form_validation->set_rules('month', 'Select Month', 'trim|required|xss_clean');
			$this->form_validation->set_rules('year', 'Select Year', 'trim|required|xss_clean');
		}
		#################################### FORM VALIDATION END ######################################
		
		if ($this->form_validation->run() == true)
		{
			$this->arrData['arrTasks'] = $this->task->getTasks($arrWhere);
			$this->arrData['arrEmp'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->arrData['arrTasks'][0]['emp_id']), false);
			$this->arrData['arrReviewerDetails'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->arrData['arrTasks'][0]['assessment_remarks_by']), false);
			
			if((int)$this->input->post('selStatus')) {
				
				$arrValues = array(
									'assessment_remarks' => $this->input->post('supervisorComments'),
									'assessment_remarks_date' => date($this->arrData["dateTimeFormat"]),
									'assessment_remarks_by' => $this->employeeID,
									'assessment_status_id' => (int)$this->input->post('selStatus'),
									'modified_by' => $this->employeeID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
								  );
				
				$this->task->saveValues(TABLE_TASK_EMPLOYEE_PERFORMANCE, $arrValues, $arrWhere);
			}
				
			if($this->input->post('selStatus') == STATUS_SUBMIT_REVIEW_TO_HR) {
				
				# SHOOT EMAIL
				# Template: tms_notificaiton.html
										
				$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->input->post("empID")),false);			
				$employeeName = $arrEmployeeDetails['emp_full_name'];
				$employeeEmail = $arrEmployeeDetails['emp_work_email'];
				
				$month = date('F', mktime(0, 0, 0, $this->input->post('selectedMonth'), 10));
				$year = $this->input->post('selectedYear');
				
				$arrSupervisorDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID),false);			
				$supervisorName = $arrSupervisorDetails['emp_full_name'];
				
				$message = $supervisorName." has submitted Review on your self-appraisal of ".$month." ".$year.". The evaluation has been submitted to HR.";
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => $employeeName,
							'[MESSAGE_BODY]' => $message,
							'[TASK_DASHBOARD_LINK]' => $this->baseURL . '/task_management/my_tasks',
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'tms_notification.html');
				
				$this->sendEmail(
									$arrTo = array(
													$employeeEmail
													), 												# RECEIVER DETAILS
									'Self Appraisal' . EMAIL_SUBJECT_SUFFIX, 						# SUBJECT
									$emailHTML														# EMAIL HTML MESSAGE
								);
				
				$this->session->set_flashdata('success_message', 'Review submitted successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/my_team_tasks');
				exit;
			}
		} 
		else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		
		$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		//$arrEmpWhere['e.emp_code > '] = ZERO;
		if(($this->userRoleID == HR_TEAMLEAD_ROLE_ID) || ($this->userRoleID == HR_MANAGER_ROLE_ID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE);
		} else if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name');
		} else {
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name');
		}
		
		$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrEmpWhere);		
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrWhereJobCategories);
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		$this->arrData['arrEmployees'] = $finalResult;
		
		$this->arrData['performanceStatus'] = $this->config->item('performance_status');
				
		# POPULATE UNREVIEWED RECORDS
		$this->arrData['arrUnReviewedRecords'] = $this->getUnReviewedRecords($this->userEmpNum);
		
		# FOR CHECKING AUTHORITY AND COMMENTS IF ANY
		
		if(!empty($this->arrData['message']))
		{
			$this->arrData['message'] = $this->arrData['message'];
		} else
		{
			$this->arrData['message'] = "No Record Found";
		}
		
		$this->arrData['empCode'] = $arrWhere['emp_id'];
		$this->arrData['month'] = $arrWhere['assessment_month'];
		$this->arrData['year'] = $arrWhere['assessment_year'];
		
		$this->arrData['months']	= $this->config->item('months');
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'task_management/my_team_tasks', $this->arrData);
		$this->template->render();
	}
	
	public function my_kpis($kpiID)
	{
		$this->employeeID = $this->userEmpNum;
		$arrWhere = array();
		
		#################################### FORM VALIDATION START ####################################	
		
		# EXPORT ASSESSMENT REPORT AS PDF
		
		if((int)$kpiID) {
			
			$arrAssessment = $this->task->getKPIS(array('review_id' => $kpiID));
			$arrAssessment = $arrAssessment[0];
			$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => $arrAssessment['emp_id']), false);
			
			if(!isAdmin($this->userRoleID)) {
				if($arrAssessment['emp_id'] != $this->userEmpNum && !in_array($this->userEmpNum, getEmpSupervisors($arrAssessment['emp_id']))) {
					//redirect(base_url() . 'message/access_denied');
					//exit;
				}
			}
			
			$arrKPIs = json_decode($arrAssessment['review_kpis'], true);
			
			$strHTMLKPIs = '';
			$jnd = 1;
            for($ind = 0; $ind < count($arrKPIs); $ind++) {
				$strHTMLKPIs .= '<tr><td valign="top" width="5%" align="center">' . $jnd . '.</td>
                    <td valign="top" width="70%">' . nl2br($arrKPIs[$ind][0]) . '</td>
					<td valign="top" width="25%" align="center">' . $this->arrData["kpiAssessment"][$arrKPIs[$ind][1]] . '</td></tr>';
					$jnd++;
			}
			
			$arrVSKPIs = json_decode($arrAssessment['review_vs_kpis'], true);
            $strHTMLVSKPIs = '<tr><td valign="top" width="5%" align="center">1.</td>
                    <td valign="top" width="70%">Client interaction and satisfaction</td>
					<td valign="top" width="25%" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[0]] . '</td></tr>
					
					<tr><td valign="top" align="center">2.</td>
                    <td valign="top">Team interaction and teamwork</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[1]] . '</td></tr>
					
					<tr><td valign="top" align="center">3.</td>
                    <td valign="top">Initiative for process improvement</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[2]] . '</td></tr>
					
					<tr><td valign="top" align="center">4.</td>
                    <td valign="top">On time delivery of work</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[3]] . '</td></tr>
					
					<tr><td valign="top" align="center">5.</td>
                    <td valign="top">Accuracy of work delivered</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[4]] . '</td></tr>
					
					<tr><td valign="top" align="center">6.</td>
                    <td valign="top">Punctuality</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[5]] . '</td></tr>
					
					<tr><td valign="top" align="center">7.</td>
                    <td valign="top">Dependability</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[6]] . '</td></tr>
					
					<tr><td valign="top" align="center">8.</td>
                    <td valign="top">Interaction with manager</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[7]] . '</td></tr>
					
					<tr><td valign="top" align="center">9.</td>
                    <td valign="top">General Conduct / Presentable / Professionalism</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[8]] . '</td></tr>
					
					<tr><td valign="top" align="center">10.</td>
                    <td valign="top">Adaptability to Change</td>
					<td valign="top" align="center">' . $this->arrData["kpiAssessment"][$arrVSKPIs[9]] . '</td></tr>';
			
			$arrValues = array(
								  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
								  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),							  
								  '[EMPLOYEE_NAME]' 			=> $arrEmployee['emp_full_name'],
								  '[EMPLOYEE_DESIGNATION]' 		=> $arrEmployee['emp_designation'],
								  '[EMPLOYEE_DEPARTMENT]' 		=> $arrEmployee['job_category_name'],
								  '[EMPLOYEE_SUPERVISOR_NAME]' 	=> getSupervisorName($arrEmployee['emp_id']),
								  
								  '[KPIS]'						=> $strHTMLKPIs,
								  '[VALUSTRAT_KPIS]'			=> $strHTMLVSKPIs,
								  '[KPI_SCORE]'					=> $arrAssessment['review_kpis_score'],
								  '[VS_KPI_SCORE]'				=> $arrAssessment['review_vs_kpis_score'],
								  '[TOTAL_SCORE]'				=> ($arrAssessment['review_kpis_score'] + $arrAssessment['review_vs_kpis_score']) . '%',
								  '[REVIEW_PERIOD]'				=> date('F', mktime(0, 0, 0, $arrAssessment['review_month'], 10)) . ' ' . $arrAssessment['review_year'],
								  
								  '[EMPLOYEE_COMMENTS]' 		=> nl2br($arrAssessment['review_emp_remarks']),
								  '[EMPLOYEE_COMMENTS_DATETIME]'=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrAssessment['review_emp_remarks_date'])),
								  
								  '[SUPERVISOR_COMMENTS]' 		=> nl2br($arrAssessment['review_remarks']),
								  '[SUPERVISOR_COMMENTS_BY]' 	=> getEmployeeName($arrAssessment['review_remarks_by']),
								  '[SUPERVISOR_COMMENTS_DATETIME]' => date(SHOW_DATE_TIME_FORMAT, strtotime($arrAssessment['review_remarks_date']))
							  );
							  
			$strHTML = getHTML($arrValues, 'kpi_form.html');
			
			require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
			
			$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor(PDF_AUTHOR);
			$pdf->setCellHeightRatio(1);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetFontSize(8);				  
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);				  
			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			
			// add a page
			$pdf->AddPage();			  
			// output the HTML content
			$pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
			$pdf->lastPage();
			$pdfFileName = 'KPI_' . $kpiID . '.pdf';
			$pdf->Output($pdfFileName, 'D');
			
			header('Content-Type: text/doc');
			header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
			header('Cache-Control: max-age=0');
			readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
			
			exit;
		}
		
		if($this->input->post('month') && $this->input->post('year'))
		{
			$arrWhere['emp_id'] = $this->employeeID;
			$arrWhere['review_month'] = $this->input->post('month');
			$arrWhere['review_year'] = $this->input->post('year');
			
		} else if($this->input->post('selMonth') && $this->input->post('selYear'))
		{
			$arrWhere['emp_id'] = $this->employeeID;
			$arrWhere['review_month'] = $this->input->post('selMonth');
			$arrWhere['review_year'] = $this->input->post('selYear');
			
		}
						
		#################################### FORM VALIDATION START ####################################		
		
		if($this->input->post('selMonth') && $this->input->post('selYear')) {
			$this->form_validation->set_rules('empRemarks', 'Employee\'s Comments', 'trim|required|xss_clean');
		}
		
		#################################### FORM VALIDATION END ######################################
		
		if ($this->form_validation->run() == true)
		{
			if(($this->input->post('month')) || ($this->input->post('year'))) {
				$arrWhere['emp_id'] = $this->employeeID;
				$arrWhere['review_month'] = $this->input->post('selMonth');
				$arrWhere['review_year'] = $this->input->post('selYear');				
			}
			
			$arrValues = array(
								'review_emp_remarks' => $this->input->post('empRemarks'),
								'review_emp_remarks_date' => date($this->arrData["dateTimeFormat"]),
								'review_status_id' => STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS,
								'modified_by' => $this->employeeID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
							  );
							  
			$this->task->saveValues(TABLE_TASK_EMPLOYEE_PERFORMANCE_KPIS, $arrValues, $arrWhere);
			
			# SHOOT EMAIL
			# Template: tms_notificaiton.html
			
			$month = date('F', mktime(0, 0, 0, $this->input->post('selMonth'), 10));
			$year = $this->input->post('selYear');
			
			$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID),false);			
			$empName = $arrEmployeeDetails['emp_full_name'];
			
			$arrTo = array();
			$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => $this->employeeID));
			foreach($arrSupervisors as $arrSupervisor) {
				$arrTo[] = $arrSupervisor['emp_work_email'];
			}
			
			$message = $empName." has submitted remarks on his/her KPI form of ".$month." ".$year.". You are now advised to provide your remarks.";
			
			$arrValues = array(
						'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
						'[EMPLOYEE_NAME]' => getSupervisorName($this->employeeID),
						'[MESSAGE_BODY]' => $message,
						'[TASK_DASHBOARD_LINK]' => $this->baseURL . '/task_management/my_team_kpis',
						'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
						);
						
			$emailHTML = getHTML($arrValues, 'tms_notification.html');
			die($emailHTML);
			$this->sendEmail(
								$arrTo, 														# RECEIVER DETAILS
								'KPI Assesment' . EMAIL_SUBJECT_SUFFIX, 						# SUBJECT
								$emailHTML														# EMAIL HTML MESSAGE
							);
			
			$this->session->set_flashdata('success_message', 'Remarks submitted successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/my_kpis');
			exit;
		} 
		else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		
		if(($this->input->post('month') && $this->input->post('year')) || ($this->input->post('selMonth') && $this->input->post('selYear')))
		{
			$this->arrData['arrTasks'] = $this->task->getKPIs($arrWhere);
			$this->arrData['arrReviewerDetails'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->arrData['arrTasks'][0]['review_remarks_by']), false);
		}
		
		$this->arrData['month'] = $arrWhere['review_month'];
		$this->arrData['year'] = $arrWhere['review_year'];
		
		$this->arrData['months']	= $this->config->item('months');
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'task_management/my_kpis', $this->arrData);
		$this->template->render();
	}
	
	public function my_team_kpis()
	{
		$this->employeeID = $this->input->post('empID');
		$arrWhere = array();
		$arrWhere['emp_id'] = $this->input->post('empID');
				
		if(($this->input->post('month')) && ($this->input->post('year')))
		{
			$arrWhere['review_month'] = $this->input->post('month');
			$arrWhere['review_year'] = $this->input->post('year');
		}
			
		if(($this->input->post('month')) || ($this->input->post('year')))
		{
			$this->form_validation->set_rules('month', 'Select Month', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('year', 'Select Year', 'trim|required|numeric|xss_clean');
		}
		
		if($this->input->post('selectedEmp') && $this->input->post('selectedMonth') && $this->input->post('selectedYear'))
		{			
			if(!array_filter($this->input->post('txt0'))) {
				$this->form_validation->set_rules('txt0[]', 'Job Specific KPIs', 'trim|required|xss_clean');
			}
			
			$this->form_validation->set_rules('selStatus', 'Status', 'trim|required|numeric|xss_clean');
		}
		
		if($this->input->post('selStatus') == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) {
			$this->form_validation->set_rules('reviewRemarks', 'Supervisor\'s Comments', 'trim|required|xss_clean');
		}
		
		#################################### FORM VALIDATION END ######################################
		
		if($this->input->post('selectedEmp') && $this->input->post('selectedMonth') && $this->input->post('selectedYear'))
		{	
			if ($this->form_validation->run() == true)
			{
				$arrWhere['emp_id'] = $this->input->post('selectedEmp');								
				$arrTask = $this->task->getKPIs($arrWhere);
				
				$arrJSKPIs = $this->input->post('txt0');
				$arrJSKPIsResult = array();
				$arrVSKPIsResult = array();
				for($ind = 0; $ind < count($arrJSKPIs); $ind++) {
					$arrJSKPIsResult[$ind][] = $arrJSKPIs[$ind];
					$arrJSKPIsResult[$ind][] = $this->input->post('rad' . $ind);
				}
				for($ind = 0; $ind < 10; $ind++) {
					$arrVSKPIsResult[$ind] = $this->input->post('rad_vs_' . $ind);
				}
				
				if(count($arrTask) > 0)
				{
					if($arrTask[0]['review_status_id'] < STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) {
						$arrValues = array(
										'emp_id' => $arrTask[0]['emp_id'],
										'review_month' => $arrTask[0]['review_month'],
										'review_year' => $arrTask[0]['review_year'],
										'review_kpis' => json_encode($arrJSKPIsResult),
										'review_kpis_score' => $this->input->post('jobKPIScore'),
										'review_vs_kpis' => json_encode($arrVSKPIsResult),
										'review_vs_kpis_score' => $this->input->post('vsKPIScore'),
										'review_status_id' => $this->input->post('selStatus'),
										'modified_by' => $this->userEmpNum,
										'modified_date' => date($this->arrData["dateTimeFormat"])
									  );
					} else if($arrTask[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) {
						$arrValues = array(
										'review_remarks' => $this->input->post('reviewRemarks'),
										'review_remarks_date' => date($this->arrData["dateTimeFormat"]),
										'review_remarks_by' => $this->userEmpNum,
										'review_status_id' => $this->input->post('selStatus')
										);
					}
								  
					$this->task->saveValues(TABLE_TASK_EMPLOYEE_PERFORMANCE_KPIS, $arrValues, $arrWhere);
				}
				else
				{
					$arrValues = array(
										'emp_id' => $this->input->post('selectedEmp'),
										'review_month' => $this->input->post('selectedMonth'),
										'review_year' => $this->input->post('selectedYear'),
										'review_kpis' => json_encode($arrJSKPIsResult),
										'review_kpis_score' => $this->input->post('jobKPIScore'),
										'review_vs_kpis' => json_encode($arrVSKPIsResult),
										'review_vs_kpis_score' => $this->input->post('vsKPIScore'),
										'review_status_id' => $this->input->post('selStatus'),
										'created_by' => $this->userEmpNum,
										'created_date' => date($this->arrData["dateTimeFormat"])
								  );
								  
					$this->task->saveValues(TABLE_TASK_EMPLOYEE_PERFORMANCE_KPIS, $arrValues);
				}
				
				if($this->input->post("selStatus") == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS)
				{
					# SHOOT EMAIL
					# Template: tms_notificaiton.html
					
					$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->input->post('selectedEmp')),false);			
					$empName = $arrEmployeeDetails['emp_full_name'];
					$arrTo = array($arrEmployeeDetails['emp_work_email']);
					
					$month = date('F', mktime(0, 0, 0, $this->input->post('selectedMonth'), 10));
					$year = $this->input->post('selectedYear');
					
					$message = "Your Manager/Supervisor has submitted your KPIs of " . $month . " " . $year . " for your Review.";
					
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => $empName,
								'[MESSAGE_BODY]' => $message,
								'[TASK_DASHBOARD_LINK]' => $this->baseURL . '/task_management/my_kpis',
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'tms_notification.html');
					die($emailHTML);
					$this->sendEmail(
										$arrTo, 													# RECEIVER DETAILS
										'KPI Assessment' . EMAIL_SUBJECT_SUFFIX, 					# SUBJECT
										$emailHTML													# EMAIL HTML MESSAGE
									);
									
				}
				
				if($this->input->post("selStatus") == STATUS_KPIS_SUBMITTED_TO_HR)
				{
					# SHOOT EMAIL
					# Template: tms_notificaiton.html
					
					$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->input->post('selectedEmp')),false);			
					$empName = $arrEmployeeDetails['emp_full_name'];
					$arrTo = array($arrEmployeeDetails['emp_work_email'], SYSTEM_EMAIL_ADDRESS);
					
					$month = date('F', mktime(0, 0, 0, $this->input->post('selectedMonth'), 10));
					$year = $this->input->post('selectedYear');
					
					$message = "Your Manager/Supervisor has submitted your KPI form of " . $month . " " . $year;
					
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => $empName,
								'[MESSAGE_BODY]' => $message,
								'[TASK_DASHBOARD_LINK]' => $this->baseURL . '/task_management/my_kpis',
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'tms_notification.html');
					die($emailHTML);
					$this->sendEmail(
										$arrTo, 													# RECEIVER DETAILS
										'KPI Assessment' . EMAIL_SUBJECT_SUFFIX, 					# SUBJECT
										$emailHTML													# EMAIL HTML MESSAGE
									);
									
				}
						
				if($this->input->post("selStatus") == STATUS_SUPERVISOR_ENTERING_KPIS) {
					$this->session->set_flashdata('success_message', 'KPI form saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/my_team_kpis');
					exit;
				} else if($this->input->post("selStatus") == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) {
					$this->session->set_flashdata('success_message', 'KPI form has been submitted for employee\'s remarks');
					redirect($this->baseURL . '/' . $this->currentController . '/my_team_kpis');
					exit;
				}
			} else {	
				$this->arrData['validation_error_message'] = validation_errors();				
			}
		}
		
		if($this->input->post('empID') && $this->input->post('month') && $this->input->post('year'))
		{
			if ($this->form_validation->run() == true)
			{
				$arrWhere['emp_id'] = $this->input->post('empID');								
				$this->arrData['arrTasks'] = $this->task->getKPIs($arrWhere);
				$this->arrData['arrReviewerDetails'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->arrData['arrTasks'][0]['review_remarks_by']), false);
			}
		}
		
		$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		//$arrEmpWhere['e.emp_code > '] = ZERO;
		
		//if(!in_array($this->userRoleID, $this->arrData["forcedAccessRoles"])) {
		$arrJobCategoryWhere = array(
									'job_category_status' => STATUS_ACTIVE,
									'order_by' => 'job_category_name'
									);
									
		if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
		} else if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) { 
			$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
		}		
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$arrEmpWhere['e.emp_location_id'] = $this->userLocationID;
		}
		
		$_POST['sort_field'] = 'e.emp_code';
		$_POST['sort_order'] = 'ASC';
		
		$arrEmployees = $this->employee->getEmployees($arrEmpWhere);
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrJobCategoryWhere);
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		unset($_POST['sort_field']);
		unset($_POST['sort_order']);
		
		$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->input->post('empID')), false);
		
		$this->arrData['performanceStatus'] = $this->config->item('performance_status');
		$this->arrData['months']			= $this->config->item('months');
		$this->arrData['frmActionURL'] 		= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'task_management/my_team_kpis', $this->arrData);
		$this->template->render();
	}
	
	public function probation_assessment($pageNum = 1, $assessmentID = 0) {
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}		
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if($this->task->deleteValue(TABLE_PROBATION_ASSESSMENT, null, array('assessment_id' => (int)$this->input->post("record_id")))) {
					echo "1"; exit;
				} else {				
					# SET LOG
					echo "0"; exit;
				}		
			}
		}
		
		# EXPORT ASSESSMENT REPORT AS PDF
		
		if((int)$assessmentID) {
			
			$arrWhere['pa.assessment_id'] = (int)$assessmentID;
			$this->arrData['arrRecord'] = $this->task->getProbationAssessment($arrWhere);
			$this->arrData['arrRecord'] = $this->arrData['arrRecord'][0];
			
			$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->arrData['arrRecord']['emp_id']), false);
			
			$arrAssessment 	= json_decode($this->arrData['arrRecord']['assessment_detail']);
			
			if(!isAdmin($this->userRoleID)) {
				if($this->arrData['arrRecord']['emp_id'] != $this->userEmpNum && !in_array($this->userEmpNum, getEmpSupervisors($this->arrData['arrRecord']['emp_id']))) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
			
			if($arrAssessment->rad18 == 'Confirmed') {
				$strEmpStatus = 'Confirmed';
			} else if($arrAssessment->rad18 == 'Extend') {
				$strEmpStatus = 'Probation Extended';
			}
			
			$arrValues = array(
								  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
								  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),							  
								  '[EMPLOYEE_NAME]' 			=> $arrEmployee['emp_full_name'],
								  '[EMPLOYEE_DESIGNATION]' 		=> $arrEmployee['emp_designation'],
								  '[EMPLOYEE_DOJ]' 				=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrEmployee['emp_joining_date'])),
								  '[EMPLOYEE_EOP]' 				=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrEmployee['emp_probation_end_date'])),
								  '[EMPLOYEE_SUPERVISOR_NAME]' 	=> getSupervisorName($arrEmployee['emp_id']),
								  '[REVIEW_DATE]' 				=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrAssessment->sup_date_time)),
								  
								  '[rad1]'						=> $arrAssessment->rad1,
								  '[rad2]'						=> $arrAssessment->rad2,
								  '[rad3]'						=> $arrAssessment->rad3,
								  '[rad4]'						=> $arrAssessment->rad4,
								  '[rad5]'						=> $arrAssessment->rad5,
								  '[rad6]'						=> $arrAssessment->rad6,
								  '[rad7]'						=> $arrAssessment->rad7,
								  '[rad8]'						=> $arrAssessment->rad8,
								  '[rad9]'						=> $arrAssessment->rad9,
								  '[rad10]'						=> $arrAssessment->rad10,
								  '[rad11]'						=> $arrAssessment->rad11,
								  '[rad12]'						=> $arrAssessment->rad12,
								  '[rad13]'						=> $arrAssessment->rad13,
								  '[txt14]'						=> nl2br($arrAssessment->txt14),
								  '[txt15]'						=> nl2br($arrAssessment->txt15),
								  '[txt16]'						=> nl2br($arrAssessment->txt16),
								  '[txt17]'						=> nl2br($arrAssessment->txt17),
								  '[ASSESSMENT_RESULT]'			=> $strEmpStatus
							  );
							  
			$strHTML = getHTML($arrValues, 'probationary_assessment.html');
			
			require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
			
			$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor(PDF_AUTHOR);
			$pdf->setCellHeightRatio(1);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetFontSize(8);				  
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);				  
			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			
			// add a page
			$pdf->AddPage();			  
			// output the HTML content
			$pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
			$pdf->lastPage();
			$pdfFileName = 'Probationary_Assessment_' . $assessmentID . '.pdf';
			$pdf->Output($pdfFileName, 'D');
			
			header('Content-Type: text/doc');
			header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
			header('Cache-Control: max-age=0');
			readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
			
			exit;
		}
		
		$arrWhere = array();
		$arrWhere['e.emp_id'] = $this->employeeID;
		
		if ($this->input->post()) {
			
			if($this->input->post("empCode")) {
				$arrWhere['e.emp_id'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("selStatus")) {
				$arrWhere['pa.assessment_status'] = $this->input->post("selStatus");
				$this->arrData['selStatus'] = $this->input->post("selStatus");
			}
						
		}
		
		if($this->userRoleID == EMPLOYEE_ROLE_ID) {
			$arrWhere['e.emp_id'] = $this->userEmpNum;
			$arrWhere['pa.assessment_status > '] = STATUS_SUPERVISOR_ENTERING_DETAILS;
		} else if(!isAdmin($this->userRoleID) && !isset($arrWhere['es.supervisor_emp_id in '])) {			
			$arrWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		$this->arrData['totalRecordsCount'] = $this->task->getTotalProbationAssessment($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->task->getProbationAssessment($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmSearchAssessment');
		
		# CODE FOR POPULATING PAGE CONTENT
		
		$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		
		if(($this->userRoleID == HR_TEAMLEAD_ROLE_ID) || ($this->userRoleID == HR_MANAGER_ROLE_ID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE);
		} else if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name');
		} else {
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name');
		}
		
		$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrEmpWhere);		
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrWhereJobCategories);
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData['arrEmployees'] = $finalResult;
		$this->arrData['assessmentStatus'] = $this->config->item('assessment_status');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'task_management/list_assessments', $this->arrData);
		$this->template->render();
	}
	
	public function save_assessment($assessmentID = 0) {
		
		$arrWhere = array();
		$currentStatus = 0;
		
		if((int)$assessmentID) {
			$arrWhere['pa.assessment_id'] = (int)$assessmentID;
			$this->arrData['arrRecord'] = $this->task->getProbationAssessment($arrWhere);
			$this->arrData['arrRecord'] = $this->arrData['arrRecord'][0];
			$currentStatus = (int)$this->arrData['arrRecord']['assessment_status'];
			
			if(!isAdmin($this->userRoleID)) {
				if($this->arrData['arrRecord']['emp_id'] != $this->userEmpNum && !in_array($this->userEmpNum, getEmpSupervisors($this->arrData['arrRecord']['emp_id']))) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		if((int)$assessmentID && $this->arrData['arrRecord']['emp_id'] == $this->userEmpNum && $this->arrData['arrRecord']['assessment_status'] > STATUS_SUPERVISOR_ENTERING_DETAILS) {
			$this->form_validation->set_rules('empID', 'Employee', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('txt17', 'Employee\'s Comments', 'trim|required|xss_clean');
		} else {
			$this->form_validation->set_rules('empID', 'Employee', 'trim|required|numeric|xss_clean');
			
			if($currentStatus != $this->input->post("selStatus")) {
				$this->form_validation->set_rules('rad1', 'QUALITY OF WORK PERFORMANCE Q1', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad2', 'QUALITY OF WORK PERFORMANCE Q2', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad3', 'QUALITY OF WORK PERFORMANCE Q3', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad4', 'QUALITY OF WORK PERFORMANCE Q4', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad5', 'QUALITY OF WORK PERFORMANCE Q5', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad6', 'QUALITY OF WORK PERFORMANCE Q6', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad7', 'QUALITY OF WORK PERFORMANCE Q7', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad8', 'RELATIONSHIPS Q1', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad9', 'RELATIONSHIPS Q2', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad10', 'RELATIONSHIPS Q3', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad11', 'DEPENDABILITY AND JUDGEMENT Q1', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad12', 'DEPENDABILITY AND JUDGEMENT Q2', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad13', 'DEPENDABILITY AND JUDGEMENT Q3', 'trim|required|xss_clean');
				$this->form_validation->set_rules('txt14', 'Employee\'s greatest strength', 'trim|required|xss_clean');
				$this->form_validation->set_rules('txt15', 'Employee\'s areas of improvement', 'trim|required|xss_clean');
				$this->form_validation->set_rules('txt16', 'Supervisor\'s additional comments', 'trim|required|xss_clean');
				$this->form_validation->set_rules('rad18', 'Supervisor\'s Decision', 'trim|required|xss_clean');
				
				if($this->input->post("rad18") == 'Confirmed') {
					$this->form_validation->set_rules('txt18a', 'Confirmation Date', 'trim|required|xss_clean');
				}
				
				if($this->input->post("rad18") == 'Extend') {
					$this->form_validation->set_rules('txt18b', 'Extended Probation Upto Date', 'trim|required|xss_clean');
				}
			}
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			if((int)$assessmentID && $this->arrData['arrRecord']['emp_id'] == $this->userEmpNum && $this->arrData['arrRecord']['assessment_status'] > STATUS_SUPERVISOR_ENTERING_DETAILS) {
				$arrAssessment = json_decode($this->arrData['arrRecord']['assessment_detail'], true);
				$arrAssessment['txt17'] = $this->input->post('txt17');
				$arrAssessment['emp_date_time'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrAssessment = array(
										'rad1' => $this->input->post('rad1'),
										'rad2' => $this->input->post('rad2'),
										'rad3' => $this->input->post('rad3'),
										'rad4' => $this->input->post('rad4'),
										'rad5' => $this->input->post('rad5'),
										'rad6' => $this->input->post('rad6'),
										'rad7' => $this->input->post('rad7'),
										'rad8' => $this->input->post('rad8'),
										'rad9' => $this->input->post('rad9'),
										'rad10' => $this->input->post('rad10'),
										'rad11' => $this->input->post('rad11'),
										'rad12' => $this->input->post('rad12'),
										'rad13' => $this->input->post('rad13'),
										'txt14' => $this->input->post('txt14'),
										'txt15' => $this->input->post('txt15'),
										'txt16' => $this->input->post('txt16'),
										'rad18' => $this->input->post('rad18'),
										'txt18a' => $this->input->post('txt18a'),
										'txt18b' => $this->input->post('txt18b'),
										'sup_by' => $this->userEmpNum,
										'sup_date_time' => date($this->arrData["dateTimeFormat"])
									);
			}
								
			$arrValues = array(
								'emp_id' => $this->input->post('empID'),
								'assessment_detail' => json_encode($arrAssessment),
								'assessment_status' => $this->input->post('selStatus')
								);
								
			if((int)$assessmentID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);				
				$this->task->saveValues(TABLE_PROBATION_ASSESSMENT, $arrValues, array('assessment_id' => $assessmentID));
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$this->task->saveValues(TABLE_PROBATION_ASSESSMENT, $arrValues);
			}
			
			if($currentStatus != $this->input->post("selStatus")) {
				
				# SHOOT EMAIL
				# Template: tms_notificaiton.html
										
				$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->input->post("empID")),false);			
				$employeeName = $arrEmployeeDetails['emp_full_name'];
				$employeeEmail = $arrEmployeeDetails['emp_work_email'];
				
				$supervisorName = getSupervisorName($arrEmployeeDetails['emp_id']);				
				
				$arrTo = array();
				$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => $arrEmployeeDetails['emp_id']));
				foreach($arrSupervisors as $arrSupervisor) {
					$arrTo[] = $arrSupervisor['emp_work_email'];
				}
				
				if($this->input->post("selStatus") == STATUS_EMPLOYEE_ENTERING_REMARKS) {
					
					if($this->input->post("rad18") == 'Confirmed') {
						$arrEmpStatusValues = array(
													'emp_employment_status' => STATUS_EMPLOYEE_CONFIRMED,
													'emp_confirmation_date' => $this->input->post('txt18a')
													);
						$this->employee->saveValues(TABLE_EMPLOYEE, $arrEmpStatusValues, array('emp_id' => (int)$this->input->post("empID")));
					} else if($this->input->post("rad18") == 'Extend') {
						$arrEmpStatusValues = array(
													'emp_probation_end_date' => $this->input->post('txt18b')
													);
						$this->employee->saveValues(TABLE_EMPLOYEE, $arrEmpStatusValues, array('emp_id' => (int)$this->input->post("empID")));
					}
					
					$message = $supervisorName." has submitted your probationary assessment.<br />You are now requested to provide your comments on it.";
					$arrTo[] = $employeeEmail;
					$recipientName = $employeeName;
				} else if($this->input->post("selStatus") == STATUS_SUBMITTED_TO_HR) {
					$message = $employeeName." has submitted comments on his/her probationary assessment.";
					$arrTo[] = $employeeEmail;
					$arrTo[] = $arrSupervisor['emp_work_email'];
					$arrTo[] = SYSTEM_EMAIL_ADDRESS;
					$recipientName = 'HR';
				}
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => $recipientName,
							'[MESSAGE_BODY]' => $message,
							'[TASK_DASHBOARD_LINK]' => $this->baseURL . '/task_management/probation_assessment',
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'tms_notification.html');
				
				$this->sendEmail(
									$arrTo, 														# RECEIVER DETAILS
									'Probationary Assessment' . EMAIL_SUBJECT_SUFFIX, 				# SUBJECT
									$emailHTML														# EMAIL HTML MESSAGE
								);
			}
			
			$this->session->set_flashdata('success_message', 'Probationary assessment saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/probation_assessment');
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();				
		}
		
		$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		$arrEmpWhere['e.emp_employment_status'] = STATUS_EMPLOYEE_PROBATION;
		
		if(($this->userRoleID == HR_TEAMLEAD_ROLE_ID) || ($this->userRoleID == HR_MANAGER_ROLE_ID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE);
		} else if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name');
		} else {
			$arrWhereJobCategories = array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name');
		}
		
		$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrEmpWhere);		
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrWhereJobCategories);
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData['arrEmployees'] = $finalResult;
		$this->arrData['assessmentStatus'] = $this->config->item('assessment_status');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'task_management/save_assessment', $this->arrData);
		$this->template->render();
	}
	
	private function getAuthorityID($authID = 0, $statusID = 0, $empJobCatID = 0) {
		
		if($authID == $this->arrData["GMEmpID"]) {
			if($empJobCatID == SALES_CATEGORY_ID) {
				return $this->arrData["hrAdminEmpID"];
			} else {
				if($statusID) {
					if($statusID >= 5) {
						return 0;
					} else if($statusID < 5) {
						return $authID;
					}
				} else {
					return $authID;
				}
			}
		}
		return $authID;
	}
	
	function getUnReviewedRecords($empID)
	{		
		$arrData['strHierarchy'] = $this->employee->getHierarchyWithMultipleAuthorities($empID);
		$arrEmployees = $this->task->getTeamEmployeesID($empID);	
		
		$waitingReview = 0;
		$record = array();
		
		$arrSubordinates = array();
		
		for($ind = 0; $ind < count($arrEmployees); $ind++) {
			$arrSubordinates[] = $arrEmployees[$ind]['emp_id'];
		}
		
		if(count($arrSubordinates) <= 0) {
			return 0;
		}
		
		$arrWhere = array(
			'assessment_status_id' => STATUS_SUBMIT_TASKS_FOR_REVIEW,
			'emp_id in ' => '(' . implode(',', $arrSubordinates) . ')'
		);
		
		$arrTaskStatus = $this->task->getTasks($arrWhere);
		$waitingReview = count($arrTaskStatus);
		
		for($jnd = 0; $jnd < count($arrTaskStatus); $jnd++)
		{
			$empID = $arrTaskStatus[$jnd]['emp_id'];							
			$status = $arrTaskStatus[$jnd]['assessment_status_id'];
			
			$arrEmployeeDetails = $this->employee->getEmployeeDetail(array('e.emp_id' => $empID),false);
			
			$empName = $arrEmployeeDetails['emp_full_name'];
			$empJobTitle = $this->task->getValues(TABLE_JOB_TITLE,'job_title_name',array('job_title_id' => $arrEmployeeDetails['emp_job_title_id']));
			$empJobTitle = $empJobTitle[0]['job_title_name'];
			$result[0] = $arrTaskStatus[$jnd]['emp_id'];
			$result[1] = $arrTaskStatus[$jnd]['assessment_month'];
			$result[2] = $arrTaskStatus[$jnd]['assessment_year'];
			array_push($result, $empName);
			array_push($result, $empJobTitle);
			$record[] = $result;
		}
		
		return $record;
		
	}
}

/* End of file task_management.php */
/* Location: ./application/controllers/task_management.php */