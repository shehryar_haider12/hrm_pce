<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quality_Control extends Master_Controller {
	
	private $arrData = array();
	public 	$arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_quality_control', 'qc', true);
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->arrData["docFolder"]				= LM_ACTIVITY_DOCS_FOLDER;
		$this->arrData["docFolderShow"]			= str_replace('./', '', LM_ACTIVITY_DOCS_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->HREmailAddress					= HR_EMAIL_ADDRESS;
		$this->HRPMTEmailAddress				= HR_PERSON_PMT;
		$this->SystemEmailAddress				= SYSTEM_EMAIL_ADDRESS;
		$this->arrData["hrAdminEmpID"]			= $this->config->item('hr_admin_emp_id');
		$this->arrData["qcReportsFolder"]		= QC_REPORTS_FOLDER;
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		$this->arrData['strHierarchy'] = $this->employee->getHierarchy($this->userEmpNum);		
	}
	
	public function index() {
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'quality_control/index', $this->arrData);
		$this->template->render();
	}
	
	public function import_data($pageNum = 1) 
	{		
		if((int)$pageNum < 1) $pageNum = 1;
		
		$this->load->model('model_user_management', 'user_management', true);
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == 1) {
				$reportName = $this->qc->getValues(TABLE_QC_REPORTS, 'qc_report_file_name', array('report_id' => (int)$this->input->post("record_id")));
				$reportName = $reportName[0]['qc_report_file_name'].'.csv';
				if($this->employee->deleteValue(TABLE_QC_REPORTS, array('report_id' => (int)$this->input->post("record_id")))) {
					unlink($this->arrData["qcReportsFolder"].$reportName);
					echo "1"; exit;
				} else {				
					# SET LOG
				
					echo "0"; exit;
				}								
			}
		}
				
		#################################### FORM VALIDATION START ####################################
				
		$this->form_validation->set_rules('qcReportType', 'Select Report Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('qcReportFrequency', 'Select Report Frequency', 'trim|required|xss_clean');
		$this->form_validation->set_rules('hiddenVal', 'Hidden Value', 'required');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		if (empty($_FILES['cFile']['name'])) {
			$this->form_validation->set_rules('cFile', 'CSV File', 'required');
		}		
		
		#################################### FORM VALIDATION END ####################################
		
		if((!empty($_POST['qcReportType'])) && ((empty($_POST['searchDate'])) || (empty($_POST['qcReportTypeSearch'])) || (empty($_POST['qcReportFrequencySearch']))))
		{
			if ($this->form_validation->run() == true) {
				$file = $_FILES['cFile']['name'];
				$uploadFileName = explode('.',$file);
				$uploadFileName = $uploadFileName[0];
				
				$arrWhere = array(
									'qc_report_type_id' 		=> $this->input->post('qcReportType'),
									'qc_report_frequency_id' 	=> $this->input->post('qcReportFrequency'),
									'qc_report_type_desc'		=> $this->input->post('date')
								);
								
				$alreadyExists = $this->qc->checkValues(TABLE_QC_REPORTS, $arrWhere);
				
				if($alreadyExists)
				{				
					$this->arrData['validation_error_message'] = 'Selected Report data that you are importing for the date '. date($this->arrData["showDateFormat"], strtotime($this->input->post('date'))) .', already exists !!';
				}
				else
				{
				
				$uploadConfig['upload_path'] 	= $this->arrData["qcReportsFolder"];
				$uploadConfig['allowed_types']	= 'csv';
				$uploadConfig['max_size']		= '1024';
				$uploadConfig['max_filename']	= '100';
				$uploadConfig['file_name']		= $uploadFileName;
	
				$this->load->library('upload', $uploadConfig);			
							
				if(!$this->upload->do_upload('cFile')) {
					$error = array('error' => $this->upload->display_errors());		
					$this->arrData['error_message'] = $error['error'];
					
				} else {		
					
					$dataUpload = $this->upload->data();
					$fileName = $this->arrData["qcReportsFolder"] . basename($dataUpload['file_name']);
									
					$arrValues = array(
										'qc_report_type_id' 		=> $this->input->post('qcReportType'),
										'qc_report_frequency_id' 	=> $this->input->post('qcReportFrequency'),
										'qc_report_file_name' 		=> $uploadFileName,
										'qc_report_type_desc'		=> $this->input->post('date'),
										'uploaded_by'				=> $this->userEmpNum,
										'uploaded_date'				=> date($this->arrData["dateTimeFormat"])
									);
											
					$insertedID = $this->qc->saveValues(TABLE_QC_REPORTS, $arrValues);									
										
					set_time_limit(0);
					
					if ($fileHandler = fopen($fileName, "r")) {
					
						while (!feof($fileHandler)) {
																					
							$csvRow = fgetcsv($fileHandler);
							
							if($this->input->post('qcReportType') == CDR_REPORT) // CDR Report 
							{			
								if((trim($csvRow[0]) != 'Employee Code'))
								{
									if((trim($csvRow[1]) != 'Grand Total') && (trim($csvRow[3] != 0))) // Row should not be Grand Total and No. of Calls should not be zero
									{																						
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[0])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																'emp_id'			=> $empID,
																'emp_code' 			=> trim($csvRow[0]),
																'emp_name' 			=> trim(strtoupper($csvRow[1])),
																'country' 			=> trim(strtoupper($csvRow[2])),
																'number_of_calls' 	=> trim($csvRow[3]),
																'total_call_time' 	=> trim($csvRow[4]),
																'total_ship_ok'		=> trim($csvRow[5]),
																'total_new_deposit' => trim($csvRow[6]),
																'ship_ok' 			=> trim($csvRow[7]),
																'new_deposit' 		=> trim($csvRow[8]),
																'local_manager' 	=> trim(strtoupper($csvRow[9])),
																'lm_code' 			=> trim($csvRow[10]),
																'senior_manager' 	=> trim(strtoupper($csvRow[11])),
																'sm_code' 			=> trim($csvRow[12]),
																'market' 			=> trim(strtoupper($csvRow[13])),
																'region' 			=> trim(strtoupper($csvRow[14]))
															);
																				
										$arrValuesDetails['report_id'] = $insertedID;
															
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_CDR_REPORTS, $arrValuesDetails);
										
									}
								}
							} 
							else if($this->input->post('qcReportType') == COMPREHENSIVE_CALLING_REPORT) // Comprehensive Calling Report
							{					
								if(trim($csvRow[0]) != 'Employee Code')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0))
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[0])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'emp_id'			=> $empID,
																	'emp_code' 			=> trim($csvRow[0]),
																	'emp_name' 			=> trim(strtoupper($csvRow[1])),
																	'local_manager'		=> trim(strtoupper($csvRow[2])),
																	'lm_code'			=> trim($csvRow[3]),
																	'senior_manager'	=> trim(strtoupper($csvRow[4])),
																	'sm_code'			=> trim($csvRow[5]),
																	'total_customer_approached' 		=> trim($csvRow[6]),
																	'total_dials' 		=> trim($csvRow[7]),
																	'single_dials' 		=> trim($csvRow[8]),
																	'repeated_dials'	=> trim($csvRow[9]),
																	'call_duration_less_than_one_min' 			=> trim($csvRow[10]),
																	'call_duration_between_one_and_three_min' 	=> trim($csvRow[11]),
																	'call_duration_more_than_three_min' 		=> trim($csvRow[12])
																);
																			
										$arrValuesDetails['report_id'] = $insertedID;
															
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_CALLING_REPORTS, $arrValuesDetails);
									}
								}
							}
							else if($this->input->post('qcReportType') == ADDRESS_VERIFICATION_REPORT) // Address Verification Report
							{
								if(trim($csvRow[0]) != 'Record Number')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0) || (trim($csvRow[5]) != 0)) // Record Number and Employee Code should not be zero
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[5])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'record_number'			=> trim($csvRow[0]),
																	'customer_name'			=> trim(strtoupper($csvRow[1])),
																	'customer_address'		=> trim($csvRow[2]),
																	'customer_phone'		=> trim($csvRow[3]),
																	'emp_name' 				=> trim(strtoupper($csvRow[4])),
																	'emp_code' 				=> trim($csvRow[5]),
																	'emp_id'				=> $empID,
																	'local_manager'			=> trim(strtoupper($csvRow[6])),
																	'lm_code'				=> trim($csvRow[7]),
																	'senior_manager'		=> trim(strtoupper($csvRow[8])),
																	'sm_code'				=> trim($csvRow[9]),										
																	'customer_dhl_address' 	=> trim($csvRow[10]),
																	'consignee_name' 		=> trim(strtoupper($csvRow[11])),
																	'consignee_address'		=> trim($csvRow[12]),
																	'consignee_phone'		=> trim($csvRow[13]),
																	'qc_remarks' 			=> trim($csvRow[14])
																);
																				
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_ADDRESS_VERIFICATION_REPORTS, $arrValuesDetails);
									}
								}
							}
							else if($this->input->post('qcReportType') == CONSIGNEE_VERIFICATION_REPORT) // Consignee Verification Report
							{
								if(trim($csvRow[0]) != 'Record Number')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0) || (trim($csvRow[5]) != 0)) // Record Number and Employee Code should not be zero
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[5])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'record_number'			=> trim($csvRow[0]),
																	'customer_name'			=> trim(strtoupper($csvRow[1])),
																	'customer_address'		=> trim($csvRow[2]),
																	'customer_phone'		=> trim($csvRow[3]),
																	'emp_name' 				=> trim(strtoupper($csvRow[4])),
																	'emp_code' 				=> trim($csvRow[5]),
																	'emp_id'				=> $empID,
																	'local_manager'			=> trim(strtoupper($csvRow[6])),
																	'lm_code' 				=> trim($csvRow[7]),
																	'senior_manager'		=> trim(strtoupper($csvRow[8])),										
																	'sm_code' 				=> trim($csvRow[9]),
																	'ship_country' 			=> trim(strtoupper($csvRow[10])),
																	'port_of_discharge'		=> trim(strtoupper($csvRow[11])),
																	'consignee_name' 		=> trim(strtoupper($csvRow[12])),
																	'consignee_address'		=> trim($csvRow[13]),
																	'consignee_phone'		=> trim($csvRow[14]),
																	'qc_remarks' 			=> trim($csvRow[15])
																);
																				
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_CONSIGNEE_VERIFICATION_REPORTS, $arrValuesDetails);
									}
								}
							}
							else if($this->input->post('qcReportType') == RESERVE_VERIFICATION_REPORT) // Reserve Verification Report
							{
								if(trim($csvRow[0]) != 'Record Number')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0))
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[7])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'record_number'				=> trim($csvRow[0]),
																	'customer_name'				=> trim(strtoupper($csvRow[1])),
																	'vehicle_chassis_number'	=> trim($csvRow[2]),
																	'vehicle_name'				=> trim(strtoupper($csvRow[3])),
																	'vehicle_year' 				=> trim($csvRow[4]),
																	'vehicle_month' 			=> trim($csvRow[5]),
																	'vehicle_color'				=> trim(ucwords($csvRow[6])),
																	'emp_id'					=> $empID,
																	'emp_code' 					=> trim($csvRow[7]),
																	'emp_name'					=> trim(strtoupper($csvRow[8])),
																	'local_manager'				=> trim(strtoupper($csvRow[9])),
																	'lm_code' 					=> trim($csvRow[10]),
																	'senior_manager'			=> trim(strtoupper($csvRow[11])),
																	'sm_code' 					=> trim($csvRow[12]),
																	'vehicle_confirm_date' 		=> trim($csvRow[13]),
																	'vehicle_total_sell_price'	=> trim($csvRow[14]),
																	'follow_up_status' 			=> trim($csvRow[15]),
																	'confirm_payment_date'		=> trim($csvRow[16])
																);
																				
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_RESERVE_VERIFICATION_REPORTS, $arrValuesDetails);
									}
								}
							}
							else if(($this->input->post('qcReportType') == SALES_STATS_MONTH_REPORT) || ($this->input->post('qcReportType') == SALES_STATS_REGION_REPORT) || ($this->input->post('qcReportType') == SALES_STATS_OFFICE_REPORT) || ($this->input->post('qcReportType') == SALES_STATS_COMPANY_REPORT))
							{								
								if($this->input->post('qcReportType') == SALES_STATS_MONTH_REPORT) // Sales Stats Monthly Report - Month wise
								{
									if(trim($csvRow[0]) != 'Months')
									{
										if((trim($csvRow[0]) != null) && (trim($csvRow[0]) != 'Total'))
										{
											$arrValuesDetails = array(
																		'field_name'		=> trim(ucwords($csvRow[0])),
																		'ship_ok'			=> trim($csvRow[1]),
																		'release_ok'		=> trim($csvRow[2])
																	);
																						
											$arrValuesDetails['report_id'] = $insertedID;
											
											$insertedDetailsID = $this->qc->saveValues(TABLE_QC_SALES_STATS_MONTHLY, $arrValuesDetails);
										}
									}
								} 
								else if($this->input->post('qcReportType') == SALES_STATS_REGION_REPORT) // Sales Stats Monthly Report - Region wise
								{
									if(trim($csvRow[0]) != 'Regions')
									{
										if((trim($csvRow[0]) != null) && (trim($csvRow[0]) != 'Total'))
										{
											$arrValuesDetails = array(
																		'field_name'		=> trim(ucwords($csvRow[0])),
																		'ship_ok'			=> trim($csvRow[1]),
																		'release_ok'		=> trim($csvRow[2])
																	);
																						
											$arrValuesDetails['report_id'] = $insertedID;
											
											$insertedDetailsID = $this->qc->saveValues(TABLE_QC_SALES_STATS_MONTHLY, $arrValuesDetails);
										}
									}
								}
								else if($this->input->post('qcReportType') == SALES_STATS_OFFICE_REPORT) // Sales Stats Monthly Report - Office wise
								{
									if(trim($csvRow[0]) != 'Offices')
									{
										if((trim($csvRow[0]) != null) && (trim($csvRow[0]) != 'Total'))
										{
											$arrValuesDetails = array(
																		'field_name'		=> trim(ucwords($csvRow[0])),
																		'ship_ok'			=> trim($csvRow[1]),
																		'release_ok'		=> trim($csvRow[2])
																	);
																						
											$arrValuesDetails['report_id'] = $insertedID;
											
											$insertedDetailsID = $this->qc->saveValues(TABLE_QC_SALES_STATS_MONTHLY, $arrValuesDetails);
										}
									}
								}
								else if($this->input->post('qcReportType') == SALES_STATS_COMPANY_REPORT) // Sales Stats Monthly Report - Company wise
								{
									if((trim($csvRow[0]) != null) && (trim($csvRow[0]) != 'Company & Country'))
									{
										$arrValuesDetails = array(
																	'field_name'		=> trim(ucwords($csvRow[0])),
																	'ship_ok'			=> trim($csvRow[1]),
																	'release_ok'		=> trim($csvRow[2])
																);
																					
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_SALES_STATS_MONTHLY, $arrValuesDetails);
									}										
								}							
							}
							else if(($this->input->post('qcReportType') == CALL_MONITORING_BELOW_SIX_MONTHS_REPORT) || ($this->input->post('qcReportType') == CALL_MONITORING_ABOVE_SIX_MONTHS_REPORT)) // Call Monitoring Scruitny - Below 6 Months and Call Monitoring Scruitny - Above 6 Months
							{
								if(trim($csvRow[0]) != 'Employee Code')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0))
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[0])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'emp_id'											=> $empID,
																	'emp_code'											=> trim($csvRow[0]),
																	'emp_name'											=> trim(strtoupper($csvRow[1])),
																	'enter_month'										=> trim($csvRow[2]),
																	'local_manager'										=> trim(strtoupper($csvRow[3])),
																	'lm_code'											=> trim($csvRow[4]),
																	'senior_manager'									=> trim(strtoupper($csvRow[5])),
																	'sm_code'											=> trim($csvRow[6]),
																	'qc_executive'										=> trim(strtoupper($csvRow[7])),
																	'qc_executive_code'									=> trim($csvRow[8]),
																	'call_opening_and_pitch' 							=> trim(ucwords($csvRow[9])),
																	'professional_handling_and_communication_skills' 	=> trim(ucwords($csvRow[10])),
																	'call_handling_and_selling_skills'					=> trim(ucwords($csvRow[11])),
																	'product_knowledge' 								=> trim(ucwords($csvRow[12])),
																	'cdr'												=> trim(ucwords($csvRow[13])),																	
																	'courtesy_tone' 									=> trim(ucwords($csvRow[14])),
																	'closing'											=> trim(ucwords($csvRow[15])),
																	'communication_grading' 							=> trim(ucwords($csvRow[16])),
																	'ship_ok_grading'									=> trim(ucwords($csvRow[17]))
																);
																				
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_CALL_MONITORING_REPORTS, $arrValuesDetails);
									}
								}
							}
							else if($this->input->post('qcReportType') == PMT_REPORT) // PMT Report
							{
								if(trim($csvRow[0]) != 'Employee Code')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0))
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[0])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'emp_id'						=> $empID,
																	'emp_code' 						=> trim($csvRow[0]),
																	'enter_month' 					=> trim($csvRow[1]),
																	'emp_name'						=> trim(strtoupper($csvRow[2])),																	
																	'local_manager'					=> trim(strtoupper($csvRow[3])),
																	'lm_code' 						=> trim($csvRow[4]),
																	'senior_manager'				=> trim(strtoupper($csvRow[5])),
																	'sm_code' 						=> trim($csvRow[6]),
																	'market'						=> trim(strtoupper($csvRow[7])),
																	'total_required_cdr'			=> trim($csvRow[8]),																
																	'total_required_ship_ok'		=> trim($csvRow[9]),
																	'total_required_calls'			=> trim($csvRow[10]),
																	'total_achieved_cdr'			=> trim($csvRow[11]),																
																	'total_achieved_ship_ok'		=> trim($csvRow[12]),
																	'total_achieved_new_deposit'	=> trim($csvRow[13]),
																	'total_achieved_calls'			=> trim($csvRow[14]),
																	'total_difference_cdr'			=> trim($csvRow[15]),																
																	'total_difference_ship_ok'		=> trim($csvRow[16]),
																	'total_difference_calls'		=> trim($csvRow[17]),
																	'comments'						=> trim($csvRow[18])
																);
																				
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_PMT_REPORTS, $arrValuesDetails);
									}
								}
							}
							else if($this->input->post('qcReportType') == ISMBU_PMT_REPORT) // ISMBU PMT Report
							{
								if(trim($csvRow[0]) != 'Employee Code')
								{
									if((trim($csvRow[0]) != null) || (trim($csvRow[0]) != 0))
									{
										$empID = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_id', array('emp_code' => trim($csvRow[0])));
										$empID = $empID[0]['emp_id'];
										
										$arrValuesDetails = array(
																	'emp_id'						=> $empID,
																	'emp_code' 						=> trim($csvRow[0]),
																	'emp_name'						=> trim(strtoupper($csvRow[1])),																	
																	'process_result'				=> trim($csvRow[2]),		
																	'markets'						=> trim(strtoupper($csvRow[3])),														
																	'target_call_counts'			=> trim($csvRow[4]),
																	'target_email_correspondence'	=> trim($csvRow[5]),
																	'target_reservations'			=> trim($csvRow[6]),																
																	'target_ship_oks'				=> trim($csvRow[7]),
																	'target_new_deposits'			=> trim($csvRow[8]),
																	'achieved_call_counts'			=> trim($csvRow[9]),
																	'achieved_email_correspondence'	=> trim($csvRow[10]),																
																	'achieved_reservations'			=> trim($csvRow[11]),
																	'achieved_ship_oks'				=> trim($csvRow[12]),
																	'achieved_new_deposits'			=> trim($csvRow[13]),
																	'bonus_awarded'					=> trim($csvRow[14]),
																	'qualify_disqualify'			=> trim($csvRow[15])
																);
																				
										$arrValuesDetails['report_id'] = $insertedID;
										
										$insertedDetailsID = $this->qc->saveValues(TABLE_QC_ISMBU_PMT_REPORTS, $arrValuesDetails);
									}
								}
							}
						}
						
						fclose($fileHandler);
						
						if($insertedDetailsID)
						{
							# SET LOG
							$this->session->set_flashdata('success_message', 'File Uploaded & Details Saved Successfully !!');
							redirect($this->baseURL . '/' . $this->currentController . '/import_data');
							exit;
						} else {
							$this->arrData['error_message'] = 'Data not saved, try again !!';
						}			
					}
				}
				
			}
				
			} else {
				$this->arrData['validation_error_message'] = validation_errors();
			}
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['qcReportsType'] = $this->config->item('qc_reports_type');
		$this->arrData['qcReportsFrequency'] = $this->config->item('qc_reports_frequency');
		
		$arrWhereReports = array();
		
		if($this->input->post("searchDate")) {				
			$arrWhereReports['qr.qc_report_type_desc'] = $this->input->post("searchDate");
		}
		if($this->input->post("qcReportTypeSearch")) {				
			$arrWhereReports['qr.qc_report_type_id'] = $this->input->post("qcReportTypeSearch");
		}
		if($this->input->post("qcReportFrequencySearch")) {				
			$arrWhereReports['qr.qc_report_frequency_id'] = $this->input->post("qcReportFrequencySearch");
		}
		
		$totalCount = $this->qc->getTotalReports($arrWhereReports);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->qc->getReports($arrWhereReports, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmImportData');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/import_data', $this->arrData);
		$this->template->render();
	}
	
	function cdr_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('date', 'Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empName', 'Employee Name', 'trim|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
		
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qcr.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post("date")) {				
				$arrWhere['qr.qc_report_type_desc'] = $this->input->post("date");
				$appendFileName = "-".$this->input->post("date");
			}
			if($this->input->post("empIP")) {				
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("empName")) {				
				$arrWhere['e.emp_name'] = $this->input->post("empName");
				$appendFileName = "-".ucwords($this->input->post("empName"));
			}
			if($this->input->post("empSupervisor")) {
				$arrWhere['qcr.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".$empSupervisor;
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getCDRReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('cdr_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("empName"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".ucwords($this->input->post("empName")); 
			} else if(($this->input->post("date")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("empName"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($this->input->post("empName"));
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor);
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empName"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".ucwords($this->input->post("empName"));
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(!(($this->input->post("date")) || ($this->input->post("empIP")) || ($this->input->post("empName")) || ($this->input->post("empSupervisor")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=CDR-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalCDRReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getCDRReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListCDRReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/cdr_reports', $this->arrData);
		$this->template->render();
	}
	
	function calling_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('dateFrom', 'Date From', 'trim|xss_clean');
		$this->form_validation->set_rules('dateTo', 'Date To', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empName', 'Employee Name', 'trim|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
		
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qcar.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		if ($this->form_validation->run() == true) 
		{
			if($this->input->post("dateFrom") && $this->input->post("dateTo")) {
				$arrWhere['qr.qc_report_type_desc >='] = $this->input->post("dateFrom");
				$arrWhere['qr.qc_report_type_desc <='] = $this->input->post("dateTo");
				$appendFileName = "-".$this->input->post("dateFrom")."-".$this->input->post("dateTo");
			} else if($this->input->post("dateFrom")) {				
				$arrWhere['qr.qc_report_type_desc >='] = $this->input->post("dateFrom");
				$appendFileName = "-".$this->input->post("dateFrom");
			} else if($this->input->post("dateTo")) {				
				$arrWhere['qr.qc_report_type_desc <='] = $this->input->post("dateTo");
				$appendFileName = "-".$this->input->post("dateTo");
			}
			if($this->input->post("empIP")) {				
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("empName")) {				
				$arrWhere['e.emp_name'] = $this->input->post("empName");
				$appendFileName = "-".ucwords($this->input->post("empName"));
			}
			if($this->input->post("empSupervisor")) {
				$arrWhere['qcar.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".ucwords($empSupervisor);
			}
		
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getCallingReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('calling_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("dateFrom")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("dateFrom")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("dateFrom")) && ($this->input->post("empSupervisor")) && ($this->input->post("empName"))) {
				$appendFileName = "-".$this->input->post("dateFrom")."-".ucwords($empSupervisor)."-".ucwords($this->input->post("empName")); 
			} else if(($this->input->post("dateFrom")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("dateFrom")."-".$this->input->post("empIP");
			} else if(($this->input->post("dateFrom")) && ($this->input->post("empName"))) {
				$appendFileName = "-".$this->input->post("dateFrom")."-".ucwords($this->input->post("empName"));
			} else if(($this->input->post("dateFrom")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$this->input->post("dateFrom")."-".ucwords($empSupervisor);
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empName"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".ucwords($this->input->post("empName"));
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(!(($this->input->post("dateFrom")) || ($this->input->post("empIP")) || ($this->input->post("empName")) || ($this->input->post("empSupervisor")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=Calling-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalCallingReportDetails($arrWhere);	
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getCallingReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		
		$numPages = ceil($totalCount / $this->limitRecords);		
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListCallingReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/calling_reports', $this->arrData);
		$this->template->render();
	}
	
	function address_verification_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('date', 'Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('recordNumber', 'Record Number', 'trim|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
				
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qavr.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post("date")) {				
				$arrWhere['qr.qc_report_type_desc'] = $this->input->post("date");
				$appendFileName = "-".$this->input->post("date");
			}
			if($this->input->post("empIP")) {				
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("recordNumber")) {				
				$arrWhere['qavr.record_number'] = $this->input->post("recordNumber");
				$appendFileName = "-".$this->input->post("recordNumber");
			}
			if($this->input->post("empSupervisor")) {				
				$arrWhere['qavr.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".ucwords($empSupervisor);
			}			
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getAddressVerificationReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('calling_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("recordNumber"); 
			} else if(($this->input->post("date")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("recordNumber");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor);
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("recordNumber");
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(!(($this->input->post("date")) || ($this->input->post("empIP")) || ($this->input->post("recordNumber")) || ($this->input->post("empSupervisor")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=Address-Verification-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalAddressVerificationReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getAddressVerificationReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);			
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListAddressVerificationReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
					
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/address_verification_reports', $this->arrData);
		$this->template->render();
		
	}
	
	function consignee_verification_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('date', 'Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('recordNumber', 'Record Number', 'trim|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
				
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qcvr.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		if ($this->form_validation->run() == true) 
		{
			if($this->input->post("date")) {
				$arrWhere['qr.qc_report_type_desc'] = $this->input->post("date");
				$appendFileName = "-".$this->input->post("date");
			}
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("recordNumber")) {
				$arrWhere['qcvr.record_number'] = $this->input->post("recordNumber");
				$appendFileName = "-".$this->input->post("recordNumber");
			}
			if($this->input->post("empSupervisor")) {
				$arrWhere['qcvr.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".ucwords($empSupervisor);
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getConsigneeVerificationReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('consignee_verification_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("recordNumber"); 
			} else if(($this->input->post("date")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("recordNumber");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor);
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("recordNumber");
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(!(($this->input->post("date")) || ($this->input->post("empIP")) || ($this->input->post("recordNumber")) || ($this->input->post("empSupervisor")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=Consignee-Verification-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalConsigneeVerificationReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getConsigneeVerificationReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListConsigneeVerificationReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/consignee_verification_reports', $this->arrData);
		$this->template->render();
	}
	
	function reserve_verification_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('date', 'Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('recordNumber', 'Record Number', 'trim|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
				
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qrvr.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		if ($this->form_validation->run() == true) 
		{
			if($this->input->post("date")) {
				$arrWhere['qr.qc_report_type_desc'] = $this->input->post("date");
				$appendFileName = "-".$this->input->post("date");
			}
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("recordNumber")) {
				$arrWhere['qrvr.record_number'] = $this->input->post("recordNumber");
				$appendFileName = "-".$this->input->post("recordNumber");
			}
			if($this->input->post("empSupervisor")) {
				$arrWhere['qrvr.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".ucwords($empSupervisor);
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getReserveVerificationReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('reserve_verification_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("recordNumber"); 
			} else if(($this->input->post("date")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("empIP");
			} else if(($this->input->post("date")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("recordNumber");
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor);
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("recordNumber"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("recordNumber");
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(!(($this->input->post("date")) || ($this->input->post("empIP")) || ($this->input->post("recordNumber")) || ($this->input->post("empSupervisor")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=Reserve-Verification-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalReserveVerificationReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getReserveVerificationReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);			
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListReserveVerificationReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/reserve_verification_reports', $this->arrData);
		$this->template->render();
	}
	
	function monthly_sales_stats()
	{
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('selType', 'Select Report Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('selMonth', 'Select Month', 'trim|required|xss_clean');
		$this->form_validation->set_rules('selYear', 'Select Year', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		if ($this->form_validation->run() == true) 
		{
			$arrWhere = array();
			
			if($this->input->post("selType"))
			{
				$arrWhere['qr.qc_report_type_id'] = $this->input->post("selType");				
				
				if(($this->input->post("selMonth")) && ($this->input->post("selYear"))) {
					$selMonth = sprintf("%02s", $this->input->post("selMonth"));
					$arrWhere['qr.qc_report_type_desc like '] = '%'.$this->input->post("selYear").'-'.$selMonth.'-%';				
				}
				
				$this->arrData['arrRecords'] = $this->qc->getSalesStatsReportDetails($arrWhere);
			}			
			
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrMonths']	= $this->config->item('months');
		$this->arrData['arrSalesStatsReports']	= $this->config->item('monthly_sales_stats_reports');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/monthly_sales_stats_reports', $this->arrData);
		$this->template->render();
	}
	
	function call_monitoring_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('selType', 'Select Report Type', 'trim|xss_clean');
		$this->form_validation->set_rules('selMonth', 'Select Month', 'trim|xss_clean');
		$this->form_validation->set_rules('selYear', 'Select Year', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
				
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qcmr.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			$this->arrData['arrQCExecutives'] = $this->employee->getEmployees(array('e.emp_job_category_id' => QC_CATEGORY_ID, 'e.emp_status' => STATUS_ACTIVE, 'e.emp_employment_status <' => '6'));
		}
		
		$this->arrData['arrCallMonitoringReports']	= $this->config->item('call_monitoring_reports');
		$this->arrData['arrMonths']	= $this->config->item('months');
				
		if ($this->form_validation->run() == true) 
		{
			if($this->input->post("selType")) { 
				$arrWhere['qr.qc_report_type_id'] = $this->input->post("selType");
				if($this->input->post("selType") == 10) {
					$selType = "Below 6 Months";
					$appendFileName = "-".$selType;
				} else if($this->input->post("selType") == 11) {
					$selType = "Above 6 Months";
					$appendFileName = "-".$selType;
				}
			}
								
			if(($this->input->post("selMonth")) && ($this->input->post("selYear"))) {
				$selMonth = sprintf("%02s", $this->input->post("selMonth"));
				$arrWhere['qr.qc_report_type_desc like '] = '%'.$this->input->post("selYear").'-'.$selMonth.'-%';
				$month = date('F', mktime(0, 0, 0, $selMonth, 10));
				$appendFileName = "-".$month."-".$this->input->post("selYear");			
			} else if($this->input->post("selMonth")) {
				$selMonth = sprintf("%02s", $this->input->post("selMonth"));
				$month = date('F', mktime(0, 0, 0, $selMonth, 10));
				$arrWhere['qr.qc_report_type_desc like '] = '%-'.$selMonth.'-%';	
				$appendFileName = "-".$month;
			} else if($this->input->post("selYear")) {
				$arrWhere['qr.qc_report_type_desc like '] = '%'.$this->input->post("selYear").'-%';
				$appendFileName = "-".$this->input->post("selYear");
			}
			
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("empSupervisor")) {
				$arrWhere['qcmr.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".ucwords($empSupervisor);
			}
			if($this->input->post("qcExecutive")) {
				$arrWhere['qcmr.qc_executive_code'] = $this->input->post("qcExecutive");
				$qcExecutiveName = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("qcExecutive")), FALSE);
				$qcExecutiveName = $qcExecutiveName[0]['emp_full_name'];
				$appendFileName = "-".ucwords($qcExecutiveName);
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getCallMonitoringReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('reserve_verification_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_id') { $cName = "QC Report Type"; }
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						if($cName == 'qc_report_type_id' && $cVal == 10) { $cVal = "Below 6 Months"; }
						else if($cName == 'qc_report_type_id' && $cVal == 11) { $cVal = "Above 6 Months"; }
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear")."-".ucwords($empSupervisor)."-".$this->input->post("empIP")."-".$this->input->post("qcExecutive");
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear")) && ($this->input->post("empSupervisor")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear")."-".ucwords($empSupervisor)."-".$this->input->post("qcExecutive");	
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear")."-".ucwords($empSupervisor);
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear")."-".$this->input->post("empIP");
				} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear")."-".$this->input->post("qcExecutive");
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("selYear"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("selYear");
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$selType."-".$month."-".ucwords($empSupervisor);
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("qcExecutive");
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$selType."-".$month."-".$this->input->post("empIP");
			} else if(($this->input->post("selType")) && ($this->input->post("empSupervisor")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".$selType."-".ucwords($empSupervisor)."-".$this->input->post("qcExecutive");	
			} else if(($this->input->post("selType")) && ($this->input->post("selMonth"))) {
				$appendFileName = "-".$selType."-".$month;				
			} else if(($this->input->post("selType")) && ($this->input->post("selYear"))) {
				$appendFileName = "-".$selType."-".$this->input->post("selYear");
			} else if(($this->input->post("selType")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$selType."-".ucwords($empSupervisor);
			} else if(($this->input->post("selType")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$selType."-".$this->input->post("empIP");
			} else if(($this->input->post("selType")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".$selType."-".$this->input->post("qcExecutive");
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("qcExecutive"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("qcExecutive");
			} else if(!(($this->input->post("selType")) || ($this->input->post("selMonth")) || ($this->input->post("selYear")) || ($this->input->post("empSupervisor")) || ($this->input->post("empIP")) || ($this->input->post("qcExecutive")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=Call-Monitoring-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalCallMonitoringReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getCallMonitoringReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);
		
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListCallMonitoringReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;			
					
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/call_monitoring_reports', $this->arrData);
		$this->template->render();
	}
	
	function pmt_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('date', 'Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empSupervisor', 'Select Supervisor', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
				
		if(!isAdmin($this->userRoleID)) {				
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$empCode = $this->qc->getValues(TABLE_EMPLOYEE,'emp_code',array('emp_id' => $this->userEmpNum));
			$empCode = $empCode[0]['emp_code'];
			$arrWhere['qpr.lm_code'] = $empCode;			
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
		}
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post("date")) {
				$arrWhere['qr.qc_report_type_desc'] = $this->input->post("date");
				$appendFileName = "-".$this->input->post("date");
			}
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
			if($this->input->post("empSupervisor")) {
				$arrWhere['qpr.lm_code'] = $this->input->post("empSupervisor");
				
				$empSupervisor = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_code' => $this->input->post("empSupervisor")),FALSE);
				$empSupervisor = $empSupervisor[0]['emp_full_name'];
				$appendFileName = "-".ucwords($empSupervisor);
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getPmtReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('pmt_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
			
						
			if(($this->input->post("date")) && ($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor)."-".$this->input->post("empIP");			
			} else if(($this->input->post("date")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("empIP");			
			} else if(($this->input->post("date")) && ($this->input->post("empSupervisor"))) {
				$appendFileName = "-".$this->input->post("date")."-".ucwords($empSupervisor);			
			} else if(($this->input->post("empSupervisor")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".ucwords($empSupervisor)."-".$this->input->post("empIP");
			} else if(!(($this->input->post("date")) || ($this->input->post("empIP")) || ($this->input->post("empSupervisor")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=PMT-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalPmtReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getPmtReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);			
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListPmtReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/pmt_reports', $this->arrData);
		$this->template->render();
	}
	
	function ismbu_pmt_reports($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		#################################### FORM VALIDATION START ##################################
				
		$this->form_validation->set_rules('date', 'Date', 'trim|xss_clean');
		$this->form_validation->set_rules('empIP', 'Employee IP', 'trim|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		$arrWhere = array();
		
		if(!isAdmin($this->userRoleID)) {			
			$arrWhere['e.emp_authority_id'] = $this->userEmpNum;			
		}
				
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post("date")) {
				$arrWhere['qr.qc_report_type_desc'] = $this->input->post("date");
				$appendFileName = "-".$this->input->post("date");
			}
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$appendFileName = "-".$this->input->post("empIP");
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR RECORD EXPORT IN A FILE
		
		if((int)$this->input->post("txtExport") && (isAdmin($this->userRoleID))) {
			
			$arrExportRecords = $this->qc->getIsmbuPmtReportDetails($arrWhere);
			$strCSV = "";
			$discardColumns = array('ismbu_pmt_report_id','report_id'); 
			
			for($ind = 0; $ind < count($arrExportRecords); $ind++) {
				
				$arrExportRecord = array();
				if(!$ind) {
					foreach($arrExportRecords[$ind] as $cName => $cVal) {
						if(!in_array($cName, $discardColumns)) {
							if($cName == 'qc_report_type_desc') { $cName = "Date"; }
							$strCSV .=  ucwords(replace_underscore_with_space($cName)) . ",";
						}
					}
					$strCSV .=  "\n";
				}
				
				foreach($arrExportRecords[$ind] as $cName => $cVal) {
					if(!in_array($cName, $discardColumns)) {
						$arrExportRecord[] =  str_replace(',', ' ', $cVal);
					}
				}
				
				$strCSV .=  trim(preg_replace( "/[\\x00-\\x20]+/" , " ", implode(',', $arrExportRecord)) , "\\x00-\\x20")  . "\n";
			}
									
			if(($this->input->post("date")) && ($this->input->post("empIP"))) {
				$appendFileName = "-".$this->input->post("date")."-".$this->input->post("empIP");
			} else if(!(($this->input->post("date")) || ($this->input->post("empIP")))) {
				$appendFileName = "";
			}
			
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=ISMBU-PMT-Report'.$appendFileName.'.csv');
			$opFile = fopen('php://output', 'w');
			fwrite($opFile, $strCSV);
			fclose($opFile);
			exit;
		}
		
		$totalCount = $this->qc->getTotalIsmbuPmtReportDetails($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		if($totalCount) {
			$this->arrData['arrRecords'] = $this->qc->getIsmbuPmtReportDetails($arrWhere, $this->limitRecords, $offSet);
		}
		$numPages = ceil($totalCount / $this->limitRecords);			
		
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListIsmbuPmtReport');
		
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/ismbu_pmt_reports', $this->arrData);
		$this->template->render();
	}
	
	function submit_offense($offenseID) {
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$offenseID = (int)$offenseID;
		
		#################################### FORM VALIDATION START ##################################		
		
		$this->form_validation->set_rules('employee', 'Employee', 'trim|required|xss_clean');
		$this->form_validation->set_rules('employeeDBID', 'Employee DB ID', 'trim|required|xss_clean|max_length[4]');
		$this->form_validation->set_rules('concernedPersons[]', 'Concerned Person(s)', 'trim|required|xss_clean');
		$this->form_validation->set_rules('offenseType', 'Offense Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('offenseDetails', 'Offense Details', 'trim|required|xss_clean');
		$this->form_validation->set_rules('offenseBriefing', 'Offense Briefing', 'trim|required|xss_clean');
		$this->form_validation->set_rules('offensePenalty', 'Offense Penalty', 'trim|required|xss_clean');
		$this->form_validation->set_rules('offenseStatus', 'Offense Status', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{
			$arrValues = array(
								'offense_type_id' 			=> $this->input->post("offenseType"),
								'emp_id'					=> $this->input->post("employee"),
								'emp_db_id' 				=> $this->input->post("employeeDBID"),
								'offense_details' 			=> $this->input->post("offenseDetails"),
								'offense_briefing' 			=> $this->input->post("offenseBriefing"),
								'offense_penalty_id' 		=> $this->input->post("offensePenalty"),
								'offense_status'	 		=> $this->input->post("offenseStatus")								
								);
								
			if($offenseID) {
				$arrValues['modified_by'] 		= $this->userEmpNum;
				$arrValues['modified_date'] 	= date($this->arrData["dateTimeFormat"]);
				
				$arrWhere = array('offense_id' => $offenseID);
				
				$offenseUpdatedID = $this->qc->saveValues(TABLE_QC_OFFENSES, $arrValues, $arrWhere);
				
				# FIRST DELETE ALL ASSOCIATED PERSONS WITH OFFENSE
				$this->db->delete(TABLE_QC_OFFENSES_PERSON, array('offense_id' => $offenseID));
				
				$offenseConcernedPersons = array_unique($this->input->post("concernedPersons"));
				for($ind = 0; $ind < count($offenseConcernedPersons); $ind++) {						
					if((int)$offenseConcernedPersons[$ind]) {
						$arrValuesOffensePersons = array(
											'offense_id' 	=> $offenseID,
											'emp_id' 		=> $offenseConcernedPersons[$ind]
											);
														
						$offensePersonSaved = $this->qc->saveValues(TABLE_QC_OFFENSES_PERSON, $arrValuesOffensePersons);
					}
				}
				
				if($offenseUpdatedID) {
					$this->session->set_flashdata('success_message', 'Offense updated successfully !!');
					redirect($this->baseURL . '/' . $this->currentController . '/list_offenses');
					exit;
				} else {
					$this->arrData['error_message'] = 'Offense not updated, try again !!';
				}				
							
			} else {
				
				$arrValues['created_by'] 		= $this->userEmpNum;
				$arrValues['created_date'] 		= date($this->arrData["dateTimeFormat"]);
				
				$offenseSaved = $this->qc->saveValues(TABLE_QC_OFFENSES, $arrValues);
				
				$insertedOffenseID = $this->db->insert_id();
				
				$empDetails = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $this->input->post("employee")));
				$empName = $empDetails[0]['emp_full_name'];
				$empEmail = $empDetails[0]['emp_work_email'];
				$arrTo[] = $empEmail;
				
				$offenseConcernedPersons = array_unique($this->input->post("concernedPersons"));
				for($ind = 0; $ind < count($offenseConcernedPersons); $ind++) {						
					if((int)$offenseConcernedPersons[$ind]) {
						$arrValuesOffensePersons = array(
											'offense_id' 	=> $insertedOffenseID,
											'emp_id' 		=> $offenseConcernedPersons[$ind]
											);
														
						$offensePersonSaved = $this->qc->saveValues(TABLE_QC_OFFENSES_PERSON, $arrValuesOffensePersons);
						
						$concernedPersonDetails = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $offenseConcernedPersons[$ind]));
						$concernedPersonName = $concernedPersonDetails[0]['emp_full_name'];
						$concernedPersonEmail = $concernedPersonDetails[0]['emp_work_email'];
						$arrTo[] = $concernedPersonEmail;
					}
				}
				
				# SHOOT EMAIL
				# Template: offense_notificaiton.html
				
				$arrTo[] = SYSTEM_EMAIL_ADDRESS;
				
				$arrValuesEmail = array(
									'[HEADER_LOGO_LINK]' 	=> EMAIL_HEADER_LOGO,
									'[EMPLOYEE_NAME]' 		=> $empName,
									'[DASHBOARD_LINK]' 		=> $this->baseURL . '/quality_control/list_offenses',
									'[COPYRIGHT_TEXT]' 		=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValuesEmail, 'offense_notification.html');
								
				$boolSent = $this->sendEmail(
									$arrTo, 											# RECEIVER DETAILS
									'[SBT QC Department] Offense Notification', 		# SUBJECT
									$emailHTML											# EMAIL HTML MESSAGE
								);
								
				if($boolSent) {
					$arrValuesEmailSent = array('email_sent' => YES);
				} else {
					$arrValuesEmailSent = array('email_sent' => NO);
				}
				
				$arrWhereEmailSent = array('offense_id' => $insertedOffenseID);
				
				$offenseEmailSent = $this->qc->saveValues(TABLE_QC_OFFENSES, $arrValuesEmailSent, $arrWhereEmailSent);
				
				if($offenseSaved && $boolSent) {
					$this->session->set_flashdata('success_message', 'Offense added and email sent successfully !!');
					redirect($this->baseURL . '/' . $this->currentController . '/list_offenses');
					exit;
				} else if($offenseSaved) {
					$this->session->set_flashdata('success_message', 'Offense added successfully !!');
					redirect($this->baseURL . '/' . $this->currentController . '/list_offenses');
					exit;
				} else {
					$this->arrData['error_message'] = 'Offense not added, try again !!';
				}
			}
			
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		
		# CODE FOR PAGE CONTENT		
		if($offenseID) {
			$this->arrData['record'] = $this->qc->getOffenses(array('offense_id' => $offenseID));
			$this->arrData['record'] = $this->arrData['record'][0];
			
			$this->arrData['arrConcernedPersons'] = $this->qc->getValues(TABLE_QC_OFFENSES_PERSON, '*', array('offense_id' => $offenseID));
		} else {
			$concernedPersons = array_unique($this->input->post("concernedPersons"));
			for($ind = 0; $ind < count($concernedPersons); $ind++) {
				$this->arrData['arrConcernedPersons'][]['emp_id'] = $concernedPersons[$ind];
			}
		}
		
		$this->arrData['arrSalesEmployees'] = $this->employee->getEmployees(array('e.emp_code >' => ZERO, 'emp_status' => STATUS_ACTIVE, 'emp_job_category_id' => SALES_JOB_CATEGORY_ID));
		
		$arrOffenseEmployees = $this->employee->getEmployees(array('e.emp_code >' => ZERO, 'e.emp_status' => STATUS_ACTIVE, 'u.user_role_id in (' => OFFENSE_USER_ROLE_IDS . ')'));
				
		$arrJobCategories = $this->qc->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrOffenseEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrReportToEmployees"] = $finalResult;
		
		$this->arrData['offenseTypes'] = $this->config->item('offenses');
		$this->arrData['offensePenalties'] = $this->config->item('offense_penalties');
		$this->arrData['offenseStatuses'] = $this->config->item('offense_statuses');
		
		if(count($this->arrData['arrConcernedPersons'])) {
			$this->arrData['totalPersons'] = count($this->arrData['arrConcernedPersons']);
		} else {
			$this->arrData['totalPersons'] = ((int)$this->input->post("totalPersons") <= 0) ? 1 : (int)$this->input->post("totalPersons");
		}
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/offenses/submit_offense', $this->arrData);
		$this->template->render();
	}
	
	function list_offenses($pageNum = 1) {
		
		if((int)$pageNum < 1) $pageNum = 1;
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if($this->employee->deleteValue(TABLE_QC_OFFENSES, array('offense_id' => (int)$this->input->post("record_id")))) {
					echo "1"; exit;
				} else {				
					# SET LOG
				
					echo "0"; exit;
				}								
			}
		}
		
		$arrWhere = array();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		if ($this->input->post()) {
			if($this->input->post("offenseType")) {				
				$arrWhere['qo.offense_type_id'] = $this->input->post("offenseType");
				$this->arrData['offenseType'] = $this->input->post("offenseType");
			}
			if($this->input->post("offensePenalty")) {
				$arrWhere['qo.offense_penalty_id'] = $this->input->post("offensePenalty");
				$this->arrData['offensePenalty'] = $this->input->post("offensePenalty");
			}
			if($this->input->post("offenseStatus")) {
				$arrWhere['qo.offense_status'] = $this->input->post("offenseStatus");
				$this->arrData['offenseStatus'] = $this->input->post("offenseStatus");
			}
		}
		
		if(!isAdmin($this->userRoleID) && !array_search($this->userEmpNum,$offensePermissions)) {
			$arrWhere['qo.emp_id'] = $this->userEmpNum;
			$arrCheck['qop.emp_id'] = $this->userEmpNum;
		}
			
		$totalCount = $this->qc->getTotalOffenses($arrWhere, $arrCheck);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->qc->getOffenses($arrWhere, $arrCheck, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListOffenses');
		
		for($ind=0; $ind < count($this->arrData['arrRecords']); $ind++) {
			$this->arrData['arrRecords'][$ind]['concernedPersons'] = $this->qc->getOffensePersonDetails(array('offense_id' => $this->arrData['arrRecords'][$ind]['offense_id']));
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['arrSalesEmployees'] = $this->employee->getEmployees(array('e.emp_code >' => ZERO, 'emp_status' => STATUS_ACTIVE, 'emp_job_category_id' => SALES_JOB_CATEGORY_ID));
		
		$this->arrData['offenseTypes'] = $this->config->item('offenses');
		$this->arrData['offensePenalties'] = $this->config->item('offense_penalties');
		$this->arrData['offenseStatuses'] = $this->config->item('offense_statuses');
		$this->arrData['offensePermissions'] = $this->config->item('qc_offense_permissions');		
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/offenses/list_offenses', $this->arrData);
		$this->template->render();
	}
	
	function offense_details($offenseID = 0) {
		$offenseID = (int)$offenseID;
		
		$employeeID = $this->userEmpNum;
				
		if(!$employeeID || !$offenseID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		if($this->input->post()) {
		
			#################################### FORM VALIDATION START ##################################
			if(isAdmin($this->userRoleID) || array_search($this->userEmpNum,$offensePermissions)) {
				$this->form_validation->set_rules('offenseStatus', 'Offense Status', 'trim|required|xss_clean');
			}
			
			$this->form_validation->set_rules('offenseResponse', 'Offense Response', 'trim|required|xss_clean');
			#################################### FORM VALIDATION END ####################################
			
			if ($this->form_validation->run() == true) 
			{
				if(isAdmin($this->userRoleID) || array_search($this->userEmpNum,$offensePermissions)) {
				
					$arrValuesUpdate = array('offense_status' 		=> $this->input->post("offenseStatus"),
												'modified_by' 		=> $this->userEmpNum,
												'modified_date' 	=> date($this->arrData["dateTimeFormat"])
											);
					
					$arrWhereUpdate = array('offense_id' => $offenseID);
					
					$offenseUpdated = $this->qc->saveValues(TABLE_QC_OFFENSES, $arrValuesUpdate, $arrWhereUpdate);
				}		
				
				$arrValues = array(
									'offense_id' 				=> $offenseID,
									'emp_id' 					=> $this->userEmpNum,
									'offense_details' 			=> $this->input->post("offenseResponse"),
									'created_date' 				=> date($this->arrData["dateTimeFormat"])
									);
									
				$offenseDetailSaved = $this->qc->saveValues(TABLE_QC_OFFENSES_DETAILS, $arrValues);
				$insertedOffenseDetailID = $this->db->insert_id();			
				
				$offenseConcernedPersons = $this->qc->getValues(TABLE_QC_OFFENSES_PERSON, '*', array('offense_id' => $offenseID));
				for($ind = 0; $ind < count($offenseConcernedPersons); $ind++) {						
					if((int)$offenseConcernedPersons[$ind]) {				
						$concernedPersonDetails = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $offenseConcernedPersons[$ind]['emp_id']));
						$concernedPersonName = $concernedPersonDetails[0]['emp_full_name'];
						$concernedPersonEmail = $concernedPersonDetails[0]['emp_work_email'];
						$arrTo[] = $concernedPersonEmail;
					}
				}
				
				# SHOOT EMAIL
				# Template: offense_response_notificaiton.html
				
				$arrTo[] = SYSTEM_EMAIL_ADDRESS;
				
				$offenseEmp = $this->qc->getValues(TABLE_QC_OFFENSES, 'emp_id', array('offense_id' => $offenseID));
				$offenseEmpID = $offenseEmp[0]['emp_id'];
				
				$offenseEmpDetail = $this->qc->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_id' => $offenseEmpID));
				$offenseEmpName = $offenseEmpDetail[0]['emp_full_name'];
				
				$arrValuesEmail = array(
									'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
									'[RESPONSE_PERSON_NAME]' 	=> $this->userWelcomeName,
									'[COMMITTED_PERSON_NAME]' 	=> $offenseEmpName,
									'[DASHBOARD_LINK]' 			=> $this->baseURL . '/quality_control/offense_details/'.$offenseID,
									'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValuesEmail, 'offense_response_notification.html');
								
				$boolSent = $this->sendEmail(
									$arrTo, 												# RECEIVER DETAILS
									'[SBT QC Department] '.$offenseEmpName.' Offense Response Notification', 	# SUBJECT
									$emailHTML												# EMAIL HTML MESSAGE
								);
								
				if($boolSent) {
					$arrValuesEmailSent = array('email_sent' => YES);
				} else {
					$arrValuesEmailSent = array('email_sent' => NO);
				}
				
				$arrWhereEmailSent = array('offense_detail_id' => $insertedOffenseDetailID);
				
				$offenseEmailSent = $this->qc->saveValues(TABLE_QC_OFFENSES_DETAILS, $arrValuesEmailSent, $arrWhereEmailSent);
				
				if($offenseDetailSaved && $boolSent) {
					$this->session->set_flashdata('success_message', 'Offense response added and email sent successfully !!');
					redirect($this->baseURL . '/' . $this->currentController . '/list_offenses');
					exit;
				} else if($offenseDetailSaved) {
					$this->session->set_flashdata('success_message', 'Offense response added successfully !!');
					redirect($this->baseURL . '/' . $this->currentController . '/list_offenses');
					exit;
				} else {
					$this->arrData['error_message'] = 'Offense response not added, try again !!';
				}
				
			} else {	
				$this->arrData['validation_error_message'] = validation_errors();
			}
			
		}
		
		
		$arrWhere = array('qo.offense_id' => $offenseID);
		$arrCheck = array();
		
		if(!isAdmin($this->userRoleID) && !array_search($this->userEmpNum,$offensePermissions)) {	
			$arrWhere['qo.emp_id'] = $this->userEmpNum;
			$arrCheck['qop.emp_id'] = $this->userEmpNum;
		}
		
		$this->arrData['arrParentRecord'] = $this->qc->getOffenses($arrWhere, $arrCheck);
		$this->arrData['arrParentRecord'] = $this->arrData['arrParentRecord'][0];
				
		if(empty($this->arrData['arrParentRecord'])) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		$this->arrData['arrParentRecord']['concernedPersons'] = $this->qc->getOffensePersonDetails(array('offense_id' => $this->arrData['arrParentRecord']['offense_id']));		
		
		$this->arrData['arrRecords'] = $this->qc->getOffenseDetails(array('qod.offense_id' => $offenseID));
		
		
		# CODE FOR PAGE CONTENT
		$this->arrData['offenseTypes'] = $this->config->item('offenses');
		$this->arrData['offensePenalties'] = $this->config->item('offense_penalties');
		$this->arrData['offenseStatuses'] = $this->config->item('offense_statuses');
		$this->arrData['offensePermissions'] = $this->config->item('qc_offense_permissions');	
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'quality_control/offenses/offense_details', $this->arrData);
		$this->template->render();
		
	}
	
}
/* End of file qc_reports.php */
/* Location: ./application/controllers/qc_reports.php */