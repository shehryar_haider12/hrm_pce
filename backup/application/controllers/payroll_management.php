<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payroll_Management extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	public $payrollCloseMonth;
	public $payrollCloseYear;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_payroll_management', 'payroll', true);
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID, ACCOUNT_MANAGER_ROLE_ID, ACCOUNT_EMPLOYEE_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$payrollCloseDate = explode('-', PAYROLL_CLOSE_DATE);
		$this->payrollCloseMonth = (int)$payrollCloseDate[1];
		$this->payrollCloseYear = (int)$payrollCloseDate[0];
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];		
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'payroll_management/index', $this->arrData);
		$this->template->render();
		
	}
	
	public function save_account($employeeID = 0) {
		
		$employeeID = (int)$employeeID;
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		$arrWhere = array();
		
		if($employeeID) {			
			$arrWhere = array(
							'emp_id' => $employeeID
							);
							
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhere['emp_company_id'] = $this->userCompanyID;
			}
		} else {
			if(!isAdmin($this->userRoleID)) {				
				# SET LOG				
				redirect($this->baseURL . '/message/access_denied');
				exit;
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('empPayMode', 'Pay Mode', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empBank', 'Bank', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empBranch', 'Branch Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empAccountNum', 'Account Number', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
				
			$arrValues = array(
								'emp_salary_bank_id' => $this->input->post("empBank"),
								'emp_salary_bank_branch' => $this->input->post("empBranch"),
								'emp_salary_bank_account_number' => $this->input->post("empAccountNum"),
								'emp_pay_mode' => $this->input->post("empPayMode"),
								'emp_payroll_execution' => $this->input->post("empPayrollExe"),
								'modified_by' => $this->userEmpNum,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
											
			$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
						
			# SET LOG
			debugLog("Banking Details Updated: [EmpID: ".$employeeID."]");
						
			$this->session->set_flashdata('success_message', 'Details Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_accounts');
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$arrEmpWhere = array();
			$arrEmpWhere['e.emp_id'] = $employeeID;			
							
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['record'] = $this->employee->getEmployeeDetail($arrEmpWhere, false);
		}
		
		if(!count($this->arrData['record'])) {
			redirect(base_url() . 'message/access_denied');
			exit;
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['employeeID'] = $employeeID;
		$this->arrData['arrBanks'] = $this->configuration->getBanks();		
		$this->arrData["arrPayModes"] = $this->config->item('payment_modes');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/save_account', $this->arrData);
		$this->template->render();
	}
	
	public function list_accounts($pageNum = 1) {
				
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("empCode")) {
				$arrWhere['e.emp_code'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$this->arrData['empIP'] = $this->input->post("empIP");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['e.emp_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empDepartment")) {
				$arrWhere['e.emp_job_category_id'] = $this->input->post("empDepartment");
				$this->arrData['empDepartment'] = $this->input->post("empDepartment");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("empBank")) {
				$arrWhere['e.emp_salary_bank_id'] = $this->input->post("empBank");
				$this->arrData['empBank'] = $this->input->post("empBank");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
			
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$arrWhere['e.emp_company_id'] = $this->userCompanyID;
			$this->arrData['empCompany'] = $this->userCompanyID;
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		$this->arrData['totalRecordsCount'] = $this->employee->getTotalEmployees($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->employee->getEmployees($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListEmployees');
		
		# CODE FOR PAGE CONTENT
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies(array('company_id' => $this->userCompanyID));
		} else {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		}
		
		if(!isAdmin($this->userRoleID)) {
			$empJobType = $this->configuration->getValues(TABLE_GRADES, 'job_type', array('grade_id' => $this->arrData['arrEmployee']['emp_grade_id']));
			$empJobType = $empJobType[0]['job_type'];
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE, 'job_type' => $empJobType));
		} else {
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE));
		}
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		
		if(!isAdmin($this->userRoleID)) {		
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE, 'e.emp_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			}
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] = $finalResultSupervisors;
		
		if($this->userRoleID == WEB_ADMIN_ROLE_ID) {
			foreach($this->arrData['arrRecords'][0] as $cName => $cVal) {
				$this->arrData['arrTblColumns'][] =  $cName;
			}
		}
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['arrBanks'] = $this->configuration->getBanks();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/list_accounts', $this->arrData);
		$this->template->render();
	}
	
	public function save_payroll($payrollID = 0) {
				
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('empID', 'Employee', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollMonth', 'Month', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollYear', 'Year', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollBasic', 'Basic Salary', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			if($this->input->post("payrollMonth") <= $this->payrollCloseMonth && $this->input->post("payrollYear") <= $this->payrollCloseYear) {
				$this->session->set_flashdata('error_message', 'Payroll For ' . date('F', mktime(0, 0, 0, $this->input->post("payrollMonth"), 10)) . ' ' . $this->input->post("payrollYear") . ' Has Already Been Closed For Changes');
				redirect($this->baseURL . '/' . $this->currentController . '/list_payroll');
			}
			
			$arrWhere = array(
							  'ep.emp_id' => $this->input->post("empID"),
							  'ep.payroll_month' => $this->input->post("payrollMonth"),
							  'ep.payroll_year' => $this->input->post("payrollYear")
							  );
							  
			$arrRecords = $this->payroll->getPayrolls($arrWhere);
			
			$arrValues = array(
								'emp_id' => $this->input->post("empID"),
								'payroll_month' => $this->input->post("payrollMonth"),
								'payroll_year' => $this->input->post("payrollYear"),
								'payroll_earning_basic' => $this->input->post("payrollBasic"),
								'payroll_earning_housing' => $this->input->post("payrollHousing"),
								'payroll_earning_transport' => $this->input->post("payrollTransport"),
								'payroll_earning_utility' => $this->input->post("payrollUtility"),
								'payroll_earning_travel' => $this->input->post("payrollTravel"),
								'payroll_earning_health' => $this->input->post("payrollHealth"),
								'payroll_earning_fuel' => $this->input->post("payrollFuel"),
								'payroll_earning_mobile' => $this->input->post("payrollMobile"),
								'payroll_earning_medical_relief' => $this->input->post("payrollMedical"),
								
								'payroll_earning_bonus' => $this->input->post("payrollBonus"),
								'payroll_earning_annual_leave_encashment' => $this->input->post("payrollLeaveEnc"),
								'payroll_earning_claims' => $this->input->post("payrollClaim"),
								'payroll_earning_commission' => $this->input->post("payrollCommission"),
								'payroll_earning_annual_ticket' => $this->input->post("payrollAnnualTicket"),
								'payroll_earning_gratuity' => $this->input->post("payrollGratuity"),
								'payroll_earning_survey_expense' => $this->input->post("payrollSurveyExpense"),
								'payroll_earning_settlement' => $this->input->post("payrollSettlement"),
								
								'payroll_earning_misc' => $this->input->post("payrollEarningMisc"),
								'payroll_deduction_tax' => $this->input->post("payrollTax"),
								'payroll_deduction_pf' => $this->input->post("payrollPF"),
								'payroll_deduction_loan' => $this->input->post("payrollLoan"),
								'payroll_deduction_eobi' => $this->input->post("payrollEOBI"),
								'payroll_deduction_telephone' => $this->input->post("payrollDeductionTelephone"),
								'payroll_deduction_misc' => $this->input->post("payrollDeductionMisc"),
								'modified_by' => $this->userEmpNum,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
			
			if(count($arrRecords)) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				$this->payroll->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues, $arrWhere);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$this->payroll->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues);
			}
						
			$this->session->set_flashdata('success_message', 'Payroll Details Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_payroll');
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($payrollID) {
			$this->arrData['record'] = $this->payroll->getPayrolls(array('ep.payroll_id' => $payrollID));
			$this->arrData['record'] = $this->arrData['record'][0];
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrEmployee = $this->employee->getEmployees(
															  array(
																	  'e.emp_id' => $this->arrData['record']['emp_id'],
																	  'e.emp_company_id' => $this->userCompanyID
																  )
															  );
				if(!count($arrEmployee)) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
		}
		
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		# CODE FOR PAGE CONTENT
		$this->arrData['arrMonths'] = $this->config->item('months');
		$this->arrData['payrollID'] = $payrollID;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/save_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function list_payroll($pageNum = 1) {
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {	
				
				$payrollRecord = $this->payroll->getPayrolls(array('ep.payroll_id' => (int)$this->input->post("record_id")));
				if($payrollRecord[0]['payroll_month'] <= $this->payrollCloseMonth && $payrollRecord[0]['payroll_year'] <= $this->payrollCloseYear) {
					echo NO; exit;
				}
				
				if(!$this->payroll->deleteValue(TABLE_EMPLOYEE_PAYROLL, null, array('payroll_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					echo YES; exit;
				}								
			}
		}
		##########################
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("empID")) {
				$arrWhere['e.emp_id'] = $this->input->post("empID");
				$this->arrData['empID'] = $this->input->post("empID");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['e.emp_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("selMonth")) {
				$arrWhere['ep.payroll_month'] = $this->input->post("selMonth");
				$this->arrData['selMonth'] = $this->input->post("selMonth");
			}
			
			if($this->input->post("selYear")) {
				$arrWhere['ep.payroll_year'] = $this->input->post("selYear");
				$this->arrData['selYear'] = $this->input->post("selYear");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
			
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$arrWhere['e.emp_company_id'] = $this->userCompanyID;
			$this->arrData['empCompany'] = $this->userCompanyID;
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		if($this->input->post("empCompany") && $this->input->post("selMonth") && $this->input->post("selYear")) {
			
			$arrAllRecords = $this->payroll->getPayrolls($arrWhere);
			$totalEarning = 0;
			$totalDeduction = 0;
			$totalEarnings = 0;
			$totalDeductions = 0;
			$totalNetSalary = 0;
			
			for($ind = 0; $ind < count($arrAllRecords); $ind++) {
				$totalEarning = (int)$arrAllRecords[$ind]['payroll_earning_basic'] + 
								(int)$arrAllRecords[$ind]['payroll_earning_housing'] +
								(int)$arrAllRecords[$ind]['payroll_earning_transport'] +
								(int)$arrAllRecords[$ind]['payroll_earning_utility'] +
								(int)$arrAllRecords[$ind]['payroll_earning_travel'] +
								(int)$arrAllRecords[$ind]['payroll_earning_health'] +
								(int)$arrAllRecords[$ind]['payroll_earning_fuel'] +
								(int)$arrAllRecords[$ind]['payroll_earning_mobile'] +
								(int)$arrAllRecords[$ind]['payroll_earning_medical_relief'] +
								(int)$arrAllRecords[$ind]['payroll_earning_bonus'] +
								(int)$arrAllRecords[$ind]['payroll_earning_annual_leave_encashment'] +
								(int)$arrAllRecords[$ind]['payroll_earning_claims'] +
								(int)$arrAllRecords[$ind]['payroll_earning_commission'] +
								(int)$arrAllRecords[$ind]['payroll_earning_annual_ticket'] +
								(int)$arrAllRecords[$ind]['payroll_earning_gratuity'] +
								(int)$arrAllRecords[$ind]['payroll_earning_survey_expense'] +
								(int)$arrAllRecords[$ind]['payroll_earning_settlement'] +
								(int)$arrAllRecords[$ind]['payroll_earning_misc'];
								
				$totalDeduction = (int)$arrAllRecords[$ind]['payroll_deduction_tax'] + 
								  (int)$arrAllRecords[$ind]['payroll_deduction_pf'] + 
								  (int)$arrAllRecords[$ind]['payroll_deduction_loan'] + 
								  (int)$arrAllRecords[$ind]['payroll_deduction_eobi'] + 
								  (int)$arrAllRecords[$ind]['payroll_deduction_telephone'] + 
								  (int)$arrAllRecords[$ind]['payroll_deduction_misc'];
				
				$totalEarnings += (int)$totalEarning;						
				$totalDeductions += (int)$totalDeduction;						
				$totalNetSalary += (int)($totalEarning - $totalDeduction);
			}
			
			$this->arrData['totalEarnings'] = $totalEarnings;
			$this->arrData['totalDeductions'] = $totalDeductions;
			$this->arrData['totalNetSalary'] = $totalNetSalary;
		}
		
		$this->arrData['totalRecordsCount'] = $this->payroll->getTotalPayrolls($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->payroll->getPayrolls($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListPayroll');
		
		# CODE FOR PAGE CONTENT
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies(array('company_id' => $this->userCompanyID));
		} else {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		}
		
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] = $finalResultSupervisors;
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['arrBanks'] = $this->configuration->getBanks();
		$this->arrData['arrMonths'] = $this->config->item('months');
		$this->arrData['arrLocations'] = $this->configuration->getLocations();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/list_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function manage_payroll() {
		
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('payrollMonth', 'Month', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollYear', 'Year', 'trim|required|numeric|xss_clean');
		
		if($this->input->post("frmType") == 1) {
			$this->form_validation->set_rules('companyID', 'Company', 'trim|required|numeric|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$arrWhere = array(
							  'ep.emp_id' => $this->input->post("empID"),
							  'ep.payroll_month' => $this->input->post("payrollMonth"),
							  'ep.payroll_year' => $this->input->post("payrollYear")
							  );
							  
			if($this->input->post("frmType") == 1) {
				
				$arrCompanyWhere = array();
				$arrCompanyWhere['company_id'] = $this->input->post("companyID");				
				$arrCompanies = $this->configuration->getCompanies($arrCompanyWhere);
				
				$strCSV = "";
				
				for($ind = 0; $ind < count($arrCompanies); $ind++) {
					
					$totalBasic = 0;
					$totalHousing = 0;
					$totalTransport = 0;
					$totalUtility = 0;
					$totalTravel = 0;
					$totalHealth = 0;
					$totalFuel = 0;
					$totalMobile = 0;
					$totalMedical = 0;
					$totalBonus = 0;
					$totalAnnualLeaveEnc = 0;
					$totalClaims = 0;
					$totalAnnualTicket = 0;
					$totalCommission = 0;
					$totalSurveyExpense = 0;
					$totalGratuity = 0;
					$totalSettlement = 0;
					$totalEarningMisc = 0;
					$totalEarnings = 0;
					
					$totalTax = 0;
					$totalPF = 0;
					$totalLoan = 0;
					$totalEOBI = 0;
					$totalTelephone = 0;
					$totalDeductionMisc = 0;
					$totalDeductions = 0;
					
					$totalNetSalary = 0;
					
					$strCSV .= $arrCompanies[$ind]['company_name'] . "\n";
					$strCSV .= date('F', mktime(0, 0, 0, $this->input->post("payrollMonth"), 10)) . ' ' . $this->input->post("payrollYear") . "\n";
					
					$arrEmpWhere = array(
										'e.emp_id > ' => STATUS_ACTIVE,
										//'e.emp_status' => STATUS_ACTIVE,
										//'e.emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED,
										'e.emp_company_id' => $arrCompanies[$ind]['company_id']
										);
										
					$arrEmployees = $this->employee->getEmployees($arrEmpWhere);
					
					for($jnd = 0; $jnd < count($arrEmployees); $jnd++) {
						
						$arrPayroll = $this->payroll->getPayrolls(array(
																		'ep.emp_id' => $arrEmployees[$jnd]['emp_id'],
																		'ep.payroll_month' => $this->input->post("payrollMonth"),
																		'ep.payroll_year' => $this->input->post("payrollYear")
																		));
						
						if(count($arrPayroll)) {
							$totalBasic += (int)$arrPayroll[0]['payroll_earning_basic'];
							$totalHousing += (int)$arrPayroll[0]['payroll_earning_housing'];
							$totalTransport += (int)$arrPayroll[0]['payroll_earning_transport'];
							$totalClaims += (int)$arrPayroll[0]['payroll_earning_claims'];
							$totalAnnualTicket += (int)$arrPayroll[0]['payroll_earning_annual_ticket'];
							$totalCommission += (int)$arrPayroll[0]['payroll_earning_commission'];
							$totalSurveyExpense += (int)$arrPayroll[0]['payroll_earning_survey_expense'];						
							$totalUtility += (int)$arrPayroll[0]['payroll_earning_utility'];
							$totalTravel += (int)$arrPayroll[0]['payroll_earning_travel'];
							$totalHealth += (int)$arrPayroll[0]['payroll_earning_health'];
							$totalFuel += (int)$arrPayroll[0]['payroll_earning_fuel'];
							$totalMobile += (int)$arrPayroll[0]['payroll_earning_mobile'];
							$totalMedical += (int)$arrPayroll[0]['payroll_earning_medical_relief'];
							$totalBonus += (int)$arrPayroll[0]['payroll_earning_bonus'];
							$totalAnnualLeaveEnc += (int)$arrPayroll[0]['payroll_earning_annual_leave_encashment'];
							$totalGratuity+= (int)$arrPayroll[0]['payroll_earning_gratuity'];
							$totalSettlement += (int)$arrPayroll[0]['payroll_earning_settlement'];
							$totalEarningMisc += (int)$arrPayroll[0]['payroll_earning_misc'];
													
							$totalTax += (int)$arrPayroll[0]['payroll_deduction_tax'];
							$totalPF += (int)$arrPayroll[0]['payroll_deduction_pf'];
							$totalLoan += (int)$arrPayroll[0]['payroll_deduction_loan'];
							$totalEOBI += (int)$arrPayroll[0]['payroll_deduction_eobi'];
							$totalTelephone += (int)$arrPayroll[0]['payroll_deduction_telephone'];
							$totalDeductionMisc += (int)$arrPayroll[0]['payroll_deduction_misc'];
						}
					}
					
					$arrHeaderColumns = array(
												'ID',
												'Employee',
												'Department',
												'PayMode'
											);
											
					if($totalBasic) $arrHeaderColumns[] = 'Basic';
					if($totalHousing) $arrHeaderColumns[] = 'Housing';
					if($totalTransport) $arrHeaderColumns[] = 'Transport';
					if($totalClaims) $arrHeaderColumns[] = 'Claims';
					if($totalAnnualTicket) $arrHeaderColumns[] = 'Annual Ticket Allowance';
					if($totalCommission) $arrHeaderColumns[] = 'Commission';
					if($totalSurveyExpense) $arrHeaderColumns[] = 'Travel/Survey Expense';
					if($totalUtility) $arrHeaderColumns[] = 'Utility';
					if($totalTravel) $arrHeaderColumns[] = 'Travel';
					if($totalHealth) $arrHeaderColumns[] = 'Health';
					if($totalFuel) $arrHeaderColumns[] = 'Fuel';
					if($totalMobile) $arrHeaderColumns[] = 'Mobile';
					if($totalMedical) $arrHeaderColumns[] = 'Medical';
					if($totalBonus) $arrHeaderColumns[] = 'Bonus';
					if($totalAnnualLeaveEnc) $arrHeaderColumns[] = 'Annual Leave Encashment';
					if($totalGratuity) $arrHeaderColumns[] = 'Gratuity';
					if($totalSettlement) $arrHeaderColumns[] = 'Settlement';
					if($totalEarningMisc) $arrHeaderColumns[] = 'Other Earning';
					
					$arrHeaderColumns[] = 'Total Earning';
					
					if($totalTax) $arrHeaderColumns[] = 'Tax';
					if($totalPF) $arrHeaderColumns[] = 'Provident Fund';
					if($totalLoan) $arrHeaderColumns[] = 'Loan';
					if($totalEOBI) $arrHeaderColumns[] = 'EOBI';
					if($totalTelephone) $arrHeaderColumns[] = 'Telephone Expense';
					if($totalDeductionMisc) $arrHeaderColumns[] = 'Other Deduction';
					
					$arrHeaderColumns[] = 'Total Deduction';
					$arrHeaderColumns[] = 'Net Salary';
					
					$strCSV .= implode(',', $arrHeaderColumns) . "\n";
					
					for($jnd = 0; $jnd < count($arrEmployees); $jnd++) {
						
						$arrPayroll = $this->payroll->getPayrolls(array(
																		'ep.emp_id' => $arrEmployees[$jnd]['emp_id'],
																		'ep.payroll_month' => $this->input->post("payrollMonth"),
																		'ep.payroll_year' => $this->input->post("payrollYear")
																		));
						if(count($arrPayroll)) {
							$totalEarning = (int)$arrPayroll[0]['payroll_earning_basic'] + 
											(int)$arrPayroll[0]['payroll_earning_housing'] +
											(int)$arrPayroll[0]['payroll_earning_transport'] +
											(int)$arrPayroll[0]['payroll_earning_utility'] +
											(int)$arrPayroll[0]['payroll_earning_travel'] +
											(int)$arrPayroll[0]['payroll_earning_health'] +
											(int)$arrPayroll[0]['payroll_earning_fuel'] +
											(int)$arrPayroll[0]['payroll_earning_mobile'] +
											(int)$arrPayroll[0]['payroll_earning_medical_relief'] +
											(int)$arrPayroll[0]['payroll_earning_bonus'] +
											(int)$arrPayroll[0]['payroll_earning_annual_leave_encashment'] +
											(int)$arrPayroll[0]['payroll_earning_claims'] +
											(int)$arrPayroll[0]['payroll_earning_commission'] +
											(int)$arrPayroll[0]['payroll_earning_annual_ticket'] +
											(int)$arrPayroll[0]['payroll_earning_gratuity'] +
											(int)$arrPayroll[0]['payroll_earning_survey_expense'] +
											(int)$arrPayroll[0]['payroll_earning_settlement'] +
											(int)$arrPayroll[0]['payroll_earning_misc'];
											
							$totalDeduction = (int)$arrPayroll[0]['payroll_deduction_tax'] + 
											  (int)$arrPayroll[0]['payroll_deduction_pf'] + 
											  (int)$arrPayroll[0]['payroll_deduction_loan'] + 
											  (int)$arrPayroll[0]['payroll_deduction_eobi'] + 
											  (int)$arrPayroll[0]['payroll_deduction_telephone'] + 
											  (int)$arrPayroll[0]['payroll_deduction_misc'];
							
							$totalEarnings += (int)$totalEarning;						
							$totalDeductions += (int)$totalDeduction;						
							$totalNetSalary += (int)($totalEarning - $totalDeduction);
							
							$arrEmpPay = array(
												$arrEmployees[$jnd]['emp_code'],
												$arrEmployees[$jnd]['emp_full_name'],
												str_replace(',', ' ', $arrEmployees[$jnd]['job_category_name']),
												$arrEmployees[$jnd]['emp_pay_mode']
											);
							
							if(in_array('Basic', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_basic'];
							if(in_array('Housing', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_housing'];
							if(in_array('Transport', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_transport'];
							if(in_array('Claims', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_claims'];
							if(in_array('Annual Ticket Allowance', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_annual_ticket'];
							if(in_array('Commission', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_commission'];
							if(in_array('Travel/Survey Expense', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_survey_expense'];
							if(in_array('Utility', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_utility'];
							if(in_array('Travel', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_travel'];
							if(in_array('Health', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_health'];
							if(in_array('Fuel', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_fuel'];
							if(in_array('Mobile', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_mobile'];
							if(in_array('Medical', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_medical_relief'];
							if(in_array('Bonus', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_bonus'];
							if(in_array('Annual Leave Encashment', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_annual_leave_encashment'];
							if(in_array('Gratuity', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_gratuity'];
							if(in_array('Settlement', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_settlement'];
							if(in_array('Other Earning', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_misc'];
							
							$arrEmpPay[] = $totalEarning;
							
							if(in_array('Tax', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_tax'];
							if(in_array('Provident Fund', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_pf'];
							if(in_array('Loan', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_loan'];
							if(in_array('EOBI', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_eobi'];
							if(in_array('Telephone Expense', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_telephone'];
							if(in_array('Other Deduction', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_misc'];
							
							$arrEmpPay[] = $totalDeduction;
							$arrEmpPay[] = (int)($totalEarning - $totalDeduction);
							
							$strCSV .=  implode(',', $arrEmpPay) . "\n";
						}
					}
					
					$arrGrandTotal = array(
											'Grand Total',
											'',
											'',
											''
										);
					
					if(in_array('Basic', $arrHeaderColumns)) $arrGrandTotal[] = $totalBasic;
					if(in_array('Housing', $arrHeaderColumns)) $arrGrandTotal[] = $totalHousing;
					if(in_array('Transport', $arrHeaderColumns)) $arrGrandTotal[] = $totalTransport;
					if(in_array('Claims', $arrHeaderColumns)) $arrGrandTotal[] = $totalClaims;
					if(in_array('Annual Ticket Allowance', $arrHeaderColumns)) $arrGrandTotal[] = $totalAnnualTicket;
					if(in_array('Commission', $arrHeaderColumns)) $arrGrandTotal[] = $totalCommission;
					if(in_array('Travel/Survey Expense', $arrHeaderColumns)) $arrGrandTotal[] = $totalSurveyExpense;
					if(in_array('Utility', $arrHeaderColumns)) $arrGrandTotal[] = $totalUtility;
					if(in_array('Travel', $arrHeaderColumns)) $arrGrandTotal[] = $totalTravel;
					if(in_array('Health', $arrHeaderColumns)) $arrGrandTotal[] = $totalHealth;
					if(in_array('Fuel', $arrHeaderColumns)) $arrGrandTotal[] = $totalFuel;
					if(in_array('Mobile', $arrHeaderColumns)) $arrGrandTotal[] = $totalMobile;
					if(in_array('Medical', $arrHeaderColumns)) $arrGrandTotal[] = $totalMedical;
					if(in_array('Bonus', $arrHeaderColumns)) $arrGrandTotal[] = $totalBonus;
					if(in_array('Annual Leave Encashment', $arrHeaderColumns)) $arrGrandTotal[] = $totalAnnualLeaveEnc;
					if(in_array('Gratuity', $arrHeaderColumns)) $arrGrandTotal[] = $totalGratuity;
					if(in_array('Settlement', $arrHeaderColumns)) $arrGrandTotal[] = $totalSettlement;
					if(in_array('Other Earning', $arrHeaderColumns)) $arrGrandTotal[] = $totalEarningMisc;
					
					$arrGrandTotal[] = $totalEarnings;
					
					if(in_array('Tax', $arrHeaderColumns)) $arrGrandTotal[] = $totalTax;
					if(in_array('Provident Fund', $arrHeaderColumns)) $arrGrandTotal[] = $totalPF;
					if(in_array('Loan', $arrHeaderColumns)) $arrGrandTotal[] = $totalLoan;
					if(in_array('EOBI', $arrHeaderColumns)) $arrGrandTotal[] = $totalEOBI;
					if(in_array('Telephone Expense', $arrHeaderColumns)) $arrGrandTotal[] = $totalTelephone;
					if(in_array('Other Deduction', $arrHeaderColumns)) $arrGrandTotal[] = $totalDeductionMisc;
					
					$arrGrandTotal[] = $totalDeductions;
					$arrGrandTotal[] = (int)($totalEarnings - $totalDeductions);
					
					$strCSV .=  implode(',', $arrGrandTotal) . "\n";
					$strCSV .= "\n\n";
				}
				
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=salary_register_' . strtolower(date('F', mktime(0, 0, 0, $this->input->post("payrollMonth"), 10))) . '_' . $this->input->post("payrollYear") . '.csv');
				$opFile = fopen('php://output', 'w');
				fwrite($opFile, $strCSV);
				fclose($opFile);
				exit;
				
			} else if($this->input->post("frmType") == 2) {
				
				$strDate = $this->input->post("payrollYear") . '-' . str_pad($this->input->post("payrollMonth"), 2, '0', STR_PAD_LEFT) . '-01';
				$this->configuration->saveValues(TABLE_CONFIGURATION, array('config_value' => date("Y-m-t", strtotime($strDate))), array('config_key' => 'PAYROLL_CLOSE_DATE'));
				$this->session->set_flashdata('success_message', 'Payroll Closed Successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/manage_payroll');
				
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		$this->arrData['arrMonths'] = $this->config->item('months');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/manage_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function employee_detail($empID = 0) {
		
		$this->arrData['arrRecord'] = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$empID), false);
		
		$this->template->write_view('content', 'payroll_management/employee_detail', $this->arrData);
		$this->template->render();
	}
	
	public function payslip($payrollID = 0, $empID = 0) {
		
		$arrWhere['ep.payroll_id'] = (int)$payrollID;
		$arrWhere['ep.emp_id'] = (int)$empID;
			
		$this->arrData['record'] = $this->payroll->getPayrolls($arrWhere);
		$this->arrData['record'] = $this->arrData['record'][0];
			
		$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$empID), false);
		$arrCompany = $this->configuration->getCompanies(array('company_id' => $this->arrData['arrEmployee']['emp_company_id']));
		$arrJobCategory = $this->configuration->getJobCategories(array('job_category_id' => $this->arrData['arrEmployee']['emp_job_category_id']));
		$arrLocation = $this->configuration->getLocations(array('location_id' => $this->arrData['arrEmployee']['emp_currency_id']));
		$arrBank = $this->configuration->getBanks(array('bank_id' => $this->arrData['arrEmployee']['emp_salary_bank_id']));
		
		$totalEarning = (int)$this->arrData['record']['payroll_earning_basic'] + 
						(int)$this->arrData['record']['payroll_earning_housing'] +
						(int)$this->arrData['record']['payroll_earning_transport'] +
						(int)$this->arrData['record']['payroll_earning_utility'] +
						(int)$this->arrData['record']['payroll_earning_travel'] +
						(int)$this->arrData['record']['payroll_earning_health'] +
						(int)$this->arrData['record']['payroll_earning_fuel'] +
						(int)$this->arrData['record']['payroll_earning_mobile'] +
						(int)$this->arrData['record']['payroll_earning_medical_relief'] +
						(int)$this->arrData['record']['payroll_earning_bonus'] +
						(int)$this->arrData['record']['payroll_earning_annual_leave_encashment'] +
						(int)$this->arrData['record']['payroll_earning_claims'] +
						(int)$this->arrData['record']['payroll_earning_commission'] +
						(int)$this->arrData['record']['payroll_earning_annual_ticket'] +
						(int)$this->arrData['record']['payroll_earning_gratuity'] +
						(int)$this->arrData['record']['payroll_earning_survey_expense'] +
						(int)$this->arrData['record']['payroll_earning_settlement'] +
						(int)$this->arrData['record']['payroll_earning_misc'];
						
		$totalDeduction = (int)$this->arrData['record']['payroll_deduction_tax'] + 
						  (int)$this->arrData['record']['payroll_deduction_pf'] + 
						  (int)$this->arrData['record']['payroll_deduction_loan'] + 
						  (int)$this->arrData['record']['payroll_deduction_eobi'] + 
						  (int)$this->arrData['record']['payroll_deduction_telephone'] + 
						  (int)$this->arrData['record']['payroll_deduction_misc'];
						  
		$objFormatter = new NumberFormatter("en", NumberFormatter::SPELLOUT);
		
		$strEarningsHTML = '';
		
		if((int)$this->arrData['record']['payroll_earning_basic']) {
			$strEarningsHTML .= '<tr>
			<td>Basic</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_basic'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_housing']) {
			$strEarningsHTML .= '<tr>
			<td>Housing Allowance</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_housing'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_transport']) {
			$strEarningsHTML .= '<tr>
			<td>Transport Allowance</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_transport'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_utility']) {
			$strEarningsHTML .= '<tr>
			<td>Utility Allowance</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_utility'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_travel']) {
			$strEarningsHTML .= '<tr>
			<td>Travel Expenses</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_travel'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_survey_expense']) {
			$strEarningsHTML .= '<tr>
			<td>Travel/Survey Expense</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_survey_expense'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_commission']) {
			$strEarningsHTML .= '<tr>
			<td>Commission</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_commission'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_health']) {
			$strEarningsHTML .= '<tr>
			<td>Health Allowance</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_health'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_fuel']) {
			$strEarningsHTML .= '<tr>
			<td>Fuel</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_fuel'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_mobile']) {
			$strEarningsHTML .= '<tr>
			<td>Mobile/Telephone</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_mobile'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_medical_relief']) {
			$strEarningsHTML .= '<tr>
			<td>Medical Relief</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_medical_relief'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_bonus']) {
			$strEarningsHTML .= '<tr>
			<td>Bonus</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_bonus'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_annual_ticket']) {
			$strEarningsHTML .= '<tr>
			<td>Annual Ticket</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_annual_ticket'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_claims']) {
			$strEarningsHTML .= '<tr>
			<td>Claims</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_claims'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_annual_leave_encashment']) {
			$strEarningsHTML .= '<tr>
			<td>Annual Leave Encashment</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_annual_leave_encashment'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_gratuity']) {
			$strEarningsHTML .= '<tr>
			<td>Gratuity</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_gratuity'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_settlement']) {
			$strEarningsHTML .= '<tr>
			<td>Final Settlement</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_settlement'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_misc']) {
			$strEarningsHTML .= '<tr>
			<td>Others</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_misc'], 2) . '</td>
		</tr>';
		}
		
		$strDeductionsHTML = '';
		
		if((int)$this->arrData['record']['payroll_deduction_tax']) {
			$strDeductionsHTML .= '<tr>
			<td>Income Tax</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_tax'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_pf']) {
			$strDeductionsHTML .= '<tr>
			<td>Provident Fund</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_pf'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_loan']) {
			$strDeductionsHTML .= '<tr>
			<td>Salary Advance/Loan Recovery</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_loan'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_eobi']) {
			$strDeductionsHTML .= '<tr>
			<td>EOBI</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_eobi'], 2) . '</td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_telephone']) {
			$strDeductionsHTML .= '<tr>
			<td>Telephone Expense</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_telephone'], 2) . '</td>
		</tr>';
		}
		
		$strDeductionsHTML .= '<tr>
			<td>Others</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_deduction_misc'], 2) . '</td>
		</tr>';
				  
		$arrValues = array(
							  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
							  '[OFFICE_ADDRESS]' 			=> $arrCompany[0]['company_name'] . '<br />&nbsp;&nbsp;' . $arrCompany[0]['company_address'],
							  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),
							  '[MONTH]' 					=> date('F', mktime(0, 0, 0, (int)$this->arrData['record']['payroll_month'], 10)),
							  '[YEAR]' 						=> $this->arrData['record']['payroll_year'],
							  '[EMPLOYEE_CODE]' 			=> $this->arrData['arrEmployee']['emp_code'],
							  '[EMPLOYEE_DESIGNATION]' 		=> $this->arrData['arrEmployee']['emp_designation'],
							  '[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
							  '[EMPLOYEE_JOINING_DATE]' 	=> date(SHOW_DATE_TIME_FORMAT, strtotime($this->arrData['arrEmployee']['emp_joining_date'])),
							  '[EMPLOYEE_DEPARTMENT]' 		=> $arrJobCategory[0]['job_category_name'],
							  '[EMPLOYEE_CURRENCY]' 		=> $arrLocation[0]['location_currency_code'],
							  '[EMPLOYEE_BANK]' 			=> $arrBank[0]['bank_name'],
							  '[EMPLOYEE_BRANCH]' 			=> $this->arrData['arrEmployee']['emp_salary_bank_branch'],
							  '[EMPLOYEE_ACCOUNT_NUMBER]' 	=> $this->arrData['arrEmployee']['emp_salary_bank_account_number'],
							  '[BASIC_SALARY]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_basic'], 2),
							  '[HOUSING]' 					=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_housing'], 2),
							  '[TRANSPORT]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_transport'], 2),
							  '[GROSS_EARNING]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalEarning, 2),
							  '[EARNINGS]' 					=> $strEarningsHTML,
							  '[TOTAL_EARNINGS]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalEarning, 2),
							  '[DEDUCTIONS]' 				=> $strDeductionsHTML,
							  '[TOTAL_DEDUCTIONS]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalDeduction, 2),
							  '[NET_SALARY]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format((int)($totalEarning - $totalDeduction), 2),
							  '[AMOUNT_IN_WORDS]' 			=> strtoupper($objFormatter->format((int)($totalEarning - $totalDeduction)))
							  );
							  
		  $strHTML = getHTML($arrValues, 'payslip.html');
		  
		  require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

		  $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		  // set document information
		  $pdf->SetCreator(PDF_CREATOR);
		  $pdf->SetAuthor(PDF_AUTHOR);
		  $pdf->setCellHeightRatio(1);
		  $pdf->setPrintHeader(false);
		  $pdf->setPrintFooter(false);
		  $pdf->SetFontSize(8);				  
		  // set margins
		  $pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);				  
		  // set auto page breaks
		  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
		  // set image scale factor
		  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
		  // set some language-dependent strings (optional)
		  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			  require_once(dirname(__FILE__).'/lang/eng.php');
			  $pdf->setLanguageArray($l);
		  }
		  
		  // add a page
		  $pdf->AddPage();			  
		  // output the HTML content
		  $pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
		  $pdf->lastPage();
		  $pdfFileName = 'Salary_Slip_' . str_replace(' ', '_', $this->arrData['arrEmployee']['emp_full_name']) . '_' . date('F', mktime(0, 0, 0, (int)$this->arrData['record']['payroll_month'], 10)) . '_' . $this->arrData['record']['payroll_year'] . '.pdf';
		  $pdf->Output($pdfFileName, 'D');
		  
		  header('Content-Type: text/doc');
		  header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
		  header('Cache-Control: max-age=0');
		  readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
		  
		  exit;
	}
}

/* End of file employee_management.php */
/* Location: ./application/controllers/employee_management.php */