<?php
class Model_Payroll_Management extends Model_Master {
	
	public $strHierarchy;
	private $tblNoDeletion	= array(
									TABLE_EMPLOYEE_PAYROLL
									);
	
	function __construct() {
		 parent::__construct();	
	}
	
	function getPayrolls($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->distinct();
		$this->db->select(' ep.*, e.* ');				
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('ep.payroll_year', 'DESC');
		$this->db->order_by('ep.payroll_month', 'DESC');
		$this->db->order_by('e.emp_full_name', 'ASC');
		
		if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', STATUS_ACTIVE);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_PAYROLL . ' ep ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalPayrolls($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' ep.payroll_id ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_PAYROLL . ' ep ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		$arrResult = count($arrResult);
		
		return $arrResult;
	}
}
?>