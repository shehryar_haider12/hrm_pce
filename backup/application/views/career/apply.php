<?php
$firstName 				= 	$_POST['firstName'];
$lastName 				= 	$_POST['lastName'];
$eMail 					= 	$_POST['eMail'];
$contactNo 				= 	$_POST['contactNo'];
$homePhoneNo 			= 	$_POST['homePhoneNo'];
$candidateAddress 		= 	$_POST['candidateAddress'];
$nicNo 					= 	$_POST['nicNo'];
$fatherNICNo 			= 	$_POST['fatherNICNo'];
$Gender 				= 	$_POST['Gender'];
$maritalStatus 			= 	$_POST['maritalStatus'];
$dateOfBirth 			= 	$_POST['dateOfBirth'];
$Country 				= 	$_POST['Country'];
$City 					= 	$_POST['City'];
$Area 					= 	$_POST['Area'];
$Comments 				= 	$_POST['Comments'];
$arrivalSource			= 	$_POST['arrivalSource'];
$arrivalSourceOther		= 	$_POST['arrivalSourceOther'];

if(!(int)$Country) {
	$Country = 98;
}
?>
<script>
function chkVal(strVal) {
	
	$('#divOther').hide();
	if(strVal == '<?php echo CONSTANT_REFERRED_BY; ?>' || strVal == '<?php echo CONSTANT_LAST_ARRIVAL_SOURCE; ?>') {
		$('#divOther').show();
		$('#spanInfo').hide();
	} else if(strVal == '<?php echo CONSTANT_REFERRED_BY_SBT_EMPLOYEE; ?>') {
		$('#divOther').show();
		$('#spanInfo').show();
	}
}
</script>
<script>
var base_url = '<?php echo $this->baseURL; ?>';

$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
									
	
	
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateOfBirth" ).datepicker( "setDate", "<?php echo $dateOfBirth; ?>" );
	$( "#dateOfBirth" ).datepicker( "option", "maxDate", '<?php echo date('Y-m-d', strtotime('18 years ago')); ?>' );
});

$('body').on('focus', ".datePickerWork", function(){	
    $(this).removeClass('hasDatepicker').datepicker({
						changeMonth: true,
						changeYear: true
						});
	$(this).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$(this).datepicker( "option", "maxDate", 0 );
});

function chkExp(strBool) {
	if(strBool) {
		$('#tblWorking').hide();
	} else {
		$('#tblWorking').show();
	}
}
</script>

<form name="frmAddCandidate" id="frmAddCandidate" enctype="multipart/form-data" method="post">
<div class="listPageMain">
	<div class="formMainWide">
	<table border="0" cellspacing="0" cellpadding="0" style="width:100%" class="listTableMain">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
				<tr>
					<td class="formHeaderRow" colspan="3">Apply for <?php echo $vacancy['vacancy_name']; ?></td>
				</tr>
				<tr>
					<td>
						<div class="formMain">
						<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
						<tr>
							<td class="formLabelContainer" align="left">First Name:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" align="left" colspan="2"><input type="text" name="firstName" maxlength="30" id="firstName" class="textBox" value="<?php echo $firstName; ?>"></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Last Name:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><input type="text" name="lastName" maxlength="30" id="lastName" class="textBox" value="<?php echo $lastName; ?>"></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Email:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><input type="text" name="eMail" maxlength="100" id="eMail" class="textBox" value="<?php echo $eMail; ?>"></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Cell Phone Number:</td>
							<td class="formTextBoxContainer" colspan="2"><input type="text" name="contactNo" maxlength="14" id="contactNo" class="textBox" value="<?php echo $contactNo; ?>"></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Home Phone Number:</td>
							<td class="formTextBoxContainer" colspan="2"><input type="text" name="homePhoneNo" maxlength="14" id="homePhoneNo" class="textBox" value="<?php echo $homePhoneNo; ?>"></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Address:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><textarea rows="5" cols="30" name="candidateAddress" id="candidateAddress" class="textArea"><?php echo $candidateAddress; ?></textarea></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">NIC Number:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer"><input type="text" name="nicNo" maxlength="13" id="nicNo" class="textBox" value="<?php echo $nicNo; ?>"></td>
							<td class="formTextBoxContainer" style="width:275px; font-size:11px; font-style:italic; color:#999">(Please ignore both hyphens e.g. "1234567891234")</td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Father's NIC Number:</td>
							<td class="formTextBoxContainer"><input type="text" name="fatherNICNo" maxlength="13" id="fatherNICNo" class="textBox" value="<?php echo $fatherNICNo; ?>"></td>
                            <td class="formTextBoxContainer" style="width:275px; font-size:11px; font-style:italic; color:#999">(Please ignore both hyphens e.g. "1234567891234")</td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Gender:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><select id="Gender" name="Gender" class="dropDown">
							<option value="">Select Gender</option>
							<?php
													  if (count($Genders)) {
														  foreach($Genders as $genderVal) {
															  $selected = '';
															  if($Gender == $genderVal) {
																  $selected = 'selected="selected"';
															  }
													  ?>
							<option value="<?php echo $genderVal; ?>" <?php echo $selected; ?>><?php echo $genderVal; ?></option>
							<?php
														  }
													  }
													  ?>
							</select></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Marital Status:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><select id="maritalStatus" name="maritalStatus" class="dropDown">
							<option value="">Select Marital Status</option>
							<?php
													  if (count($maritalStatuses)) {
														  foreach($maritalStatuses as $maritalVal) {
															  $selected = '';
															  if($maritalStatus == $maritalVal) {
																  $selected = 'selected="selected"';
															  }
													  ?>
							<option value="<?php echo $maritalVal; ?>" <?php echo $selected; ?>><?php echo $maritalVal; ?></option>
							<?php
														  }
													  }
													  ?>
							</select></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Date of Birth:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><input type="text" name="dateOfBirth" maxlength="30" id="dateOfBirth" class="textBox datePicker"></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Country / City:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" id="tdlocation" width="170px"><select id="Country" name="Country" class="dropDown" onChange="populateLocation('tdlocation', this.value, '3');">
							<option value="">Select Country</option>
							<?php 
							if (count($Countries)) {
							foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($countryID == $dataCountry['location_id']) {
							$strSelected = 'selected="selected"';
							}
							?>
							<option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
							<?php
							}
							}
							?>
							</select></td>
							<td style="width:275px">&nbsp;</td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Resume:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2"><input type="file" name="Resume" id="Resume" /></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">Cover Letter</td>
							<td class="formTextBoxContainer" colspan="2"><textarea name="Comments" id="Comments" class="textArea" style="margin: 0px; height: 119px; width: 459px;"><?php echo $Comments; ?></textarea></td>
						</tr>
						<tr>
							<td class="formLabelContainer" align="left">How Did You Reach Us:<span class="mandatoryStar"> *</span></td>
							<td class="formTextBoxContainer" colspan="2">
							<select id="arrivalSource" name="arrivalSource" class="dropDown" onchange="chkVal(this.value)">
							<option value="">Select Source</option>
							<?php
							if (count($arrArrivalSource)) {
							foreach($arrArrivalSource as $sourceVal) {
							$selected = '';
							if($arrivalSource == $sourceVal) {
							$selected = 'selected="selected"';
							}
							?>
							<option value="<?php echo $sourceVal; ?>" <?php echo $selected; ?>><?php echo $sourceVal; ?></option>
							<?php
							}
							}
							?>
							</select>
                            <div style="clear:both"></div>
							<div id="divOther" <?php if($arrivalSource != CONSTANT_LAST_ARRIVAL_SOURCE && $arrivalSource != CONSTANT_REFERRED_BY && $arrivalSource != CONSTANT_REFERRED_BY_SBT_EMPLOYEE) { ?>style="display:none; margin-top:5px"<?php } ?>>
							<input type="text" name="arrivalSourceOther" id="arrivalSourceOther" class="textBox" maxlength="100" value="<?php echo $arrivalSourceOther; ?>" />
                            <div style="clear:both"></div>
                            <span id="spanInfo" style="font-size:12px; font-style:italic; color:red; font-weight:normal">In case of <?php echo CONSTANT_REFERRED_BY_SBT_EMPLOYEE; ?>. Type 4 digit numeric employee code</span>                            
							</div>
							</td>
						</tr>
						</table>
						</div>
					</td>
				</tr>
				
				<!--	EDUCATION	-->
				<tr>
					<td>
						<div class="formMainWideSub">
						<div class="subHeading">Education</div>
						<div class="subTableMainDiv">
						<table class="subTableMain" border="0" cellspacing="0" cellpadding="0" style="width:1095px" id="tblEducation">
						<tr class="subHeader">
							<td style="width:165px;" class="subHeaderCol">Qualification Level<span class="mandatoryStar"> *</span></td>
							<td style="width:165px;" class="subHeaderCol">Educational Institute<span class="mandatoryStar"> *</span></td>
							<td style="width:190px;" class="subHeaderCol">Majors<span class="mandatoryStar"> *</span></td>
							<td style="width:165px;" class="subHeaderCol">Passing Year<span class="mandatoryStar"> *</span></td>
							<td style="width:165px;" class="subHeaderCol">GPA/Grade</td>
							<!--<td style="width:165px;" class="subHeaderCol">Status</td>-->
							<td style="width:80px;" class="subHeaderColLast"></td>
						</tr>
						<!-- ###############	   HIDDEN EDUCATIONAL FIELDS		################### -->
						<tr class="subContent" style="display:none">
							<td class="subContentCol" colspan="7">
								<table>
									<tr class="subContent" id="trEdu">
										<td class="subContentCol">
										<select name="eduLevel[" class="dropDownSub">
										<option value="">Select Degree</option>
										<?php
										for($ind = 0; $ind < count($eduLevels); $ind++) {
											$selected = '';
											if($eduLevels[$ind]['edu_level_id'] == $eduLevel) {
												$selected = 'selected="selected"';
											}
										?>
										<option value="<?php echo $eduLevels[$ind]['edu_level_id']; ?>" <?php echo $selected; ?>><?php echo $eduLevels[$ind]['edu_level_name']; ?></option>
										<?php
										}
										?>
										</select>
										</td>
										
										<td class="subContentCol"><input type="text" name="eduInstitute[" maxlength="100" class="textBoxSub" value=""></td>
										
										<td class="subContentCol">
										<select name="eduMajors[" class="dropDownSub" style="width:175px">
										<option value="">Select Major</option>
										<?php
										for($ind = 0; $ind < count($eduMajors); $ind++) {
											$selected = '';
											if($eduMajors[$ind]['edu_major_id'] == $eduMajor) {
												$selected = 'selected="selected"';
											}
										?>
										<option value="<?php echo $eduMajors[$ind]['edu_major_id']; ?>" <?php echo $selected; ?>><?php echo $eduMajors[$ind]['edu_major_name']; ?></option>
										<?php
										}
										?>
										</select>
										</td>
										<td class="subContentCol">
                                        	<select name="eduEnded[" class="dropDownSub">
                                            	<option value="">Select Year</option>
                                                <option value="In Progress">In Progress</option>
												<?php for($ind = date('Y'); $ind > 1969; $ind--) { ?>
                                                <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>		
										
										<td class="subContentCol"><input type="text" name="eduGPA[" maxlength="10" class="textBoxSub" value=""></td>								
										<!--<td class="subContentCol">
										<select name="eduStatus[" class="dropDownSub">
                                            <option value="Passed">Passed</option>
                                            <option value="In Progress">In Progress</option>
										</select>
										</td>-->
									</tr>
								</table>
							</td>
						</tr>
						<!-- ############################################################### -->
						<?php for($ind = 0; $ind < $totalEducations; $ind++) { ?>
						<tr class="subContent">
							<td class="subContentCol">
								<select name="eduLevel[]" id="eduLevel<?php echo $ind; ?>" class="dropDownSub">
								<option value="">Select Degree</option>
								<?php
								for($jnd = 0; $jnd < count($eduLevels); $jnd++) {
								?>
								<option value="<?php echo $eduLevels[$jnd]['edu_level_id']; ?>"><?php echo $eduLevels[$jnd]['edu_level_name']; ?></option>
								<?php
								}
								?>
								</select>
								<script>$('#eduLevel<?php echo $ind; ?>').val(<?php echo set_value('eduLevel[]'); ?>)</script>
							</td>
							<td class="subContentCol"><input type="text" name="eduInstitute[]" maxlength="100" class="textBoxSub" value="<?php echo set_value('eduInstitute[]'); ?>"></td>
							<td class="subContentCol">
								<select name="eduMajors[]" id="eduMajors<?php echo $ind; ?>" class="dropDownSub" style="width:175px">
								<option value="">Select Major</option>
								<?php
								for($jnd = 0; $jnd < count($eduMajors); $jnd++) {
								?>
								<option value="<?php echo $eduMajors[$jnd]['edu_major_id']; ?>"><?php echo $eduMajors[$jnd]['edu_major_name']; ?></option>
								<?php
								}
								?>
								</select>
								<script>$('#eduMajors<?php echo $ind; ?>').val(<?php echo set_value('eduMajors[]'); ?>)</script>
							</td>
							<td class="subContentCol">                            	
                                <select name="eduEnded[]" id="eduEnded<?php echo $ind; ?>" class="dropDownSub">
                                    <option value="">Select Year</option>
                                    <option value="In Progress">In Progress</option>
                                    <?php for($jnd = date('Y'); $jnd > 1969; $jnd--) { ?>
                                    <option value="<?php echo $jnd; ?>"><?php echo $jnd; ?></option>
                                    <?php } ?>
                                </select>
								<script>$('#eduEnded<?php echo $ind; ?>').val('<?php echo set_value('eduEnded[]'); ?>')</script>
                            </td>
							<td class="subContentCol"><input type="text" name="eduGPA[]" id="eduGPA<?php echo $ind; ?>" maxlength="10" class="textBoxSub" value="<?php echo set_value('eduGPA[]'); ?>"></td>
							<!--<td class="subContentCol">	
								<select name="eduStatus[]" id="eduStatus<?php echo $ind; ?>" class="dropDownSub">
								<option value="Passed">Passed</option>
								<option value="In Progress">In Progress</option>
								</select>
								<script>$('#eduStatus<?php echo $ind; ?>').val('<?php echo set_value('eduStatus[]'); ?>')</script>
							</td>-->
							<td class="subContentColLast">                	
								<?php if(!$ind) { ?>
								<span class=""><a href="#" onclick="addEducation('tblEducation'); return false;">Add More</a></span>
								<?php } else { ?>
								<span class=""><a href="#" onclick="removeEducation('tblEducation'); return false;">Remove</a></span>
								<?php } ?>
							</td>
						</tr>
						<?php } ?>
						<tr class="">
							<td class="" colspan="6">
								<input type="hidden" name="totalEducations" id="totalEducations" value="<?php echo $totalEducations; ?>" />
							</td>
						</tr>
						</table>
						</div>
						</div>
					</td>
				</tr>
				
				<!--	EXPERIENCE	-->		
				<tr>
					<td>
						<div class="formMainWideSub">
						<div class="subHeading">
							Experience<br>
							<div class="subHeadingLabel"><input type="checkbox" name="noExp" id="noExp" value="1" <?php if((int)$this->input->post("noExp") == 1 ) { echo 'checked="checked"'; } ?> onclick="chkExp(this.checked)" /><label for="noExp"> Fresh Candidate having no experience </label></div>
						</div>
						<div class="subTableMainDiv">
						<table class="subTableMain" border="0" cellspacing="0" cellpadding="0" style="width:1095px; <?php if((int)$this->input->post("noExp") == 1 ) { ?> display:none<?php } ?>" id="tblWorking">
						<tr class="subHeader">
							<td style="width:165px;" class="subHeaderCol">Company Name<span class="mandatoryStar"> *</span></td>
							<td style="width:165px;" class="subHeaderCol">Job Title<span class="mandatoryStar"> *</span></td>
							<td style="width:165px;" class="subHeaderCol">Starting Date<span class="mandatoryStar"> *</span></td>
							<td style="width:165px;" class="subHeaderCol">Ending Date</td>
							<td style="width:355px;" class="subHeaderCol">Job Description</td>
							<td style="width:80px;" class="subHeaderColLast"></td>
						</tr>
						<!-- ###############	   HIDDEN EDUCATIONAL FIELDS		################### -->
						<tr class="subContent" style="display:none">
							<td class="subContentCol"colspan="6">
							<table>
								<tr class="subContent" id="trWork">
									<td class="subContentCol"><input type="text" name="workCompany[" maxlength="100" class="textBoxSub" value=""></td>
									<td class="subContentCol"><input type="text" name="workJobTitle[" maxlength="100" class="textBoxSub" value=""></td>
									<td class="subContentCol"><input type="text" name="workFrom[" maxlength="30" class="textBoxSub datePickerWork" value=""></td>
									<td class="subContentCol"><input type="text" name="workTo[" maxlength="30" class="textBoxSub datePickerWork" value=""></td>
									<td class="subContentCol"><textarea name="workDescription[" class="textAreaSub" maxlength="1000"></textarea></td>
								</tr>
							</table>
							</td>
						</tr>
						<!-- ############################################################### -->
						<?php for($ind = 0; $ind < $totalWorkings; $ind++) { ?>
						<tr class="subContent">
							<td class="subContentCol"><input type="text" name="workCompany[]" maxlength="100" class="textBoxSub" value="<?php echo set_value('workCompany[]'); ?>"></td>
							<td class="subContentCol"><input type="text" name="workJobTitle[]" maxlength="100" class="textBoxSub" value="<?php echo set_value('workJobTitle[]'); ?>"></td>
							<td class="subContentCol"><input type="text" name="workFrom[]" maxlength="30" class="textBoxSub datePickerWork" value="<?php echo set_value('workFrom[]'); ?>"></td>
							<td class="subContentCol"><input type="text" name="workTo[]" maxlength="30" class="textBoxSub datePickerWork" value="<?php echo set_value('workTo[]'); ?>"></td>
							<td class="subContentCol"><textarea name="workDescription[]" class="textAreaSub" maxlength="1000"><?php echo set_value('workDescription[]'); ?></textarea></td>
							<td class="subContentColLast">                	
							<?php if(!$ind) { ?>
							<span class=""><a href="#" onclick="addWorking('tblWorking'); return false;">Add More</a></span>
							<?php } else { ?>
							<span class=""><a href="#" onclick="removeWorking('tblWorking'); return false;">Remove</a></span>
							<?php } ?>
							</td>
						</tr>
						<?php } ?>
						<tr class="">
							<td class="" colspan="6">
							<input type="hidden" name="totalWorkings" id="totalWorkings" value="<?php echo $totalWorkings; ?>" />
							</td>
						</tr>
						</table>
						</div>
						</div>
					</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Apply"></td>
				</tr>
				</table>
				<!--	Main Apply Table End	-->
			</td>
		</tr>
	</table>
	</div>
</div>
<?php if((int)$Country) {	?>
<script>
  $('#Country').val(<?php echo (int)$Country; ?>);
  populateLocationWithCity('tdlocation', '<?php echo $Country; ?>', '3', <?php echo (int)$City; ?>, <?php echo (int)$Area; ?>);
</script>
<?php } ?>
</form>
