<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: 'u.last_login_date';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: 'DESC';
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});
</script>
<form name="frmUserReporting" id="frmUserReporting" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee Code:</div>
        <div class="textBoxContainer">
          <input type="text" id="empCode" name="empCode" maxlength="4" class="textBox" value="<?php echo $empCode; ?>">
        </div>
        
		<div class="labelContainer">Department:</div>
        <div class="textBoxContainer">
          <select id="empDepartment" name="empDepartment" class="dropDown">
            <option value="">All</option>
            <?php
			  for($ind = 0; $ind < count($empDepartments); $ind++) {
				  $selected = '';
				  if($empDepartments[$ind]['job_category_id'] == $empDepartment) {
					  $selected = 'selected="selected"';
				  }
            ?>
             <option value="<?php echo $empDepartments[$ind]['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $empDepartments[$ind]['job_category_name']; ?></option>
            <?php
              }
            ?>
          </select>
        </div>
        <div class="labelContainer">Grade & Designation:</div>
        <div class="textBoxContainer">
        	<select class="dropDown" name="empDesignation" id="empDesignation">
				<option value="">All</option>
				<?php
                if (count($empDesignations)) {
                    foreach($empDesignations as $empDesig) {
						if($empDesig['job_type'] == 1)
						  { $jobType = 'Sales'; }
						  else if($empDesig['job_type'] == 2)
						  { $jobType = 'General'; }
                ?>
                <option value="<?php echo $empDesig['grade_id']; ?>"><?php echo $jobType.' : '.$empDesig['grade_code'].' - '.$empDesig['grade_title']; ?></option>
                <?php
                    }
                }
                ?>
          	</select>
        </div>
      </div>
      
	  <div class="searchCol">
        <div class="labelContainer">IP Extension:</div>
        <div class="textBoxContainer">
          <input type="text" id="empIP" name="empIP" maxlength="4" class="textBox" value="<?php echo $empIP; ?>">
        </div>
        
        <div class="labelContainer">Date From:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateFrom" name="dateFrom" />
        </div>
        <div class="labelContainer">Supervisor:</div>
        <div class="textBoxContainer">
        	<select name="empSupervisor" id="empSupervisor" class="dropDown">
            	<option value="">All</option>
				<?php
                if (count($arrSupervisors)) {
                    foreach($arrSupervisors as $arrSupervisor) {
                ?>
                <option value="<?php echo $arrSupervisor['emp_id']; ?>"><?php echo $arrSupervisor['emp_full_name']; ?></option>
                <?php
                    }
                }
                ?>
            </select>
        </div>
		
      </div>
      
	  <div class="searchCol">
		<div class="labelContainer">Employee SBT Email:</div>
        <div class="textBoxContainer">
          <input type="text" id="empSBTEmail" name="empSBTEmail" class="textBox" value="<?php echo $empSBTEmail; ?>" />
        </div>
        <div class="labelContainer">Date To:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateTo" name="dateTo" />
        </div>
      </div>
      
	  <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
		<input class="searchButton" type="reset" value="Reset">
      </div>
    </div>
  </div>
  <script>
  	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#empIP').val('<?php echo $empIP; ?>');
  	$('#empDesignation').val('<?php echo $empDesignation; ?>');
	$('#empDepartment').val('<?php echo $empDepartment; ?>');
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
	$('#empSBTEmail').val('<?php echo $empSBTEmail; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Legends:</b>&nbsp;&nbsp;&nbsp;<span style="background-color:#B9D1B8;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Recently Modified</span>
		&nbsp;&nbsp;&nbsp;<span style="text-decoration:underline">abc</span><span class="mandatoryStar"> Sortable Columns</span>
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol"></td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_first_name', 'frmUserReporting')">Employee Name</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_code', 'frmUserReporting')">Code</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_ip_num', 'frmUserReporting')">IP No.</td>
    <td class="listHeaderColClickable" onclick="setSort('g.grade_title', 'frmUserReporting')">Designation</td>
    <td class="listHeaderCol">Last Log In</td>
    <td class="listHeaderCol">Last Log Out</td>
    <td class="listHeaderCol">Time Spend</td>
    <!--	<td class="listHeaderCol">Status</td>	-->
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	$trCSSClass = 'listContentAlternate';
	/*
	if(!empty($arrRecords[$ind]['last_login_date']) || $arrRecords[$ind]['last_login_date'] != '')
		($arrRecords[$ind]['last_login_date'] <= date("Y-m-d h:i:s") && $arrRecords[$ind]['last_logout_date'] <= date("Y-m-d h:i:s"))	?	$statusImage = 'normal' : $statusImage = 'urgent';
	*/
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol" style="background-color:#FFF; padding:0px; width:60px">
    <?php
    if(!file_exists($pictureFolder . $arrRecords[$ind]['emp_photo_name']) || empty($arrRecords[$ind]['emp_photo_name'])) {
		$arrRecords[$ind]['emp_photo_name'] = 'no_image_' . strtolower($arrRecords[$ind]['emp_gender']) . '.jpg';
	}
	?>
    	<img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrRecords[$ind]['emp_photo_name']; ?>" width="60" height="60" />
    </td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_code']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_ip_num']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['grade_title']; ?></td>
    <td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['last_login_date'])) ? date($showDateFormat . ' h:i:s', strtotime($arrRecords[$ind]['last_login_date'])) : ' - ' ; ?></td>
    <td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['last_logout_date'])) ? date($showDateFormat . ' h:i:s', strtotime($arrRecords[$ind]['last_logout_date'])) : ' - ' ; ?></td>
    <td class="listContentCol"><?php echo round($arrRecords[$ind]['total_time_spent']); ?> Minutes</td>
    <!--	<td class="listContentCol"><img src="< ?php echo $this->imagePath . '/'.$statusImage.'.png' ?>" align="top" /></td>	-->
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
