<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: 'l.created_date';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: 'DESC';
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});
</script>
<form name="frmLogs" id="frmLogs" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee:</div>
        <div class="textBoxContainer">
            <select id="empID" name="empID" class="dropDown">
                <option value="">All</option>
                <?php
				if (count($arrEmployees)) {
					foreach($arrEmployees as $key => $arrEmployee) {
				?>
                	<optgroup label="<?php echo $key; ?>">
						<?php for($i=0;$i<count($arrEmployee);$i++) { ?>					
                            <option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_code']." - ".$arrEmployee[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
				<?php	}
				}
				?>
            </select>
        </div>
        <div class="labelContainer">Module:</div>
        <div class="textBoxContainer">
            <select class="dropDown" id="moduleID" name="moduleID">
                <option value="">All</option>
                <?php
                if (count($arrModules)) {
                    foreach($arrModules as $arrModule) {
                ?>
                <option value="<?php echo $arrModule['module_id']; ?>"><?php echo $arrModule['display_name']; ?></option>
                <?php
                    }
                }
                ?>
            </select>
        </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Date From:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateFrom" name="dateFrom" />
        </div>
        <div class="labelContainer">Date To:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateTo" name="dateTo" />
        </div>
      </div>
      <div class="searchCol">
		<div class="labelContainer">Activity:</div>
        <div class="textBoxContainer">
        	<input type="text" id="activity" name="activity" class="textBox" value="<?php echo $activity; ?>" />
        </div>
	   </div>	
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val(0)">
        <?php 	/*	Export Excel Option for Training Section Logs	*/
		if($moduleID == 77) { ?>
        <input class="searchButton" name="btnExport" id="btnExport" type="submit" value="Export Records" onclick="$('#txtExport').val(1)">
        <input type="hidden" name="txtExport" id="txtExport" value="0" />
        <?php } ?>
		<input class="searchButton" type="reset" value="Reset">
      </div>
    </div>
  </div>
  <script>
  	$('#empID').val('<?php echo $empID; ?>');
	$('#moduleID').val('<?php echo $moduleID; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
    </div>
	
	<?php
		if($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php
    	}
	?>
</div>

<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
    <tr>
        <td class="formHeaderRow" colspan="6" style="padding-left: 8px">Activities</td>
    </tr>
	<tr class="listHeader">
    	<td class="listHeaderCol" width="200px">Employee</td>
    	<td class="listHeaderCol" width="80px">Code/IP Ext.</td>
    	<td class="listHeaderCol" width="180px">Module</td>
    	<td class="listHeaderCol" width="180px">When</td>
    	<td class="listHeaderCol" style="min-width:300px; max-width:400px">What</td>
    	<!--<td class="listHeaderCol" width="100px">IP</td>-->
    </tr>
    <?php
    for($ind = 0; $ind < count($arrActivities); $ind++) {
		if($arrActivities[$ind]['log_activity_txt'] == "Log-In")
			$class = "listContent green";
		else if(strpos($arrActivities[$ind]['log_activity_txt'],'Updated') !== false) {
			$class = "listContent red";
			if($this->userRoleID != WEB_ADMIN_ROLE_ID && $this->userEmpNum != GM_EMP_ID) {
				$arrActivities[$ind]['log_activity_txt'] = substr($arrActivities[$ind]['log_activity_txt'], 0, strpos($arrActivities[$ind]['log_activity_txt'], 'Updated') + 7);
			}
		}
		else if(strpos($arrActivities[$ind]['log_activity_txt'],'Entered in') !== false)
			$class = "listContent yellow";
		else
			$class = "listContent";
	?>
    <tr class="<?php echo $class; ?>">
    	<td class="listContentCol"><?php echo $arrActivities[$ind]['emp_full_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrActivities[$ind]['emp_code'] . '/' . $arrActivities[$ind]['emp_ip_num']; ?></td>
    	<td class="listContentCol"><?php echo $arrActivities[$ind]['display_name']; ?></td>
    	<td class="listContentCol"><?php echo date('j-F-Y g:i A', strtotime($arrActivities[$ind]['created_date'])); ?></td>
		<td class="listContentCol paddingTopBottom"><?php echo $arrActivities[$ind]['log_activity_txt']; ?></td>
		<!--<td class="listContentCol paddingTopBottom"><?php //echo $arrActivities[$ind]['log_ip']; ?></td>-->
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="6" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>