<?php
$userRole	=	(isset($_POST['userRole']))	?	$_POST['userRole']	:	$record['user_role_name'];
$isAdmin	=	(isset($_POST['isAdmin']))	?	$_POST['isAdmin']	:	$record['is_admin'];
if ($record['user_role_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['user_role_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['user_role_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 	=	(isset($_POST['status'])) 	?	$_POST['status']	: 	$recordStatus;
$userRoleID = $record['user_role_id'];
?>

<form name="frmAddUserRole" id="frmAddUserRole" method="post">
<div class="listPageMain">
	<div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
			<tr>
            	<?php if($record['user_role_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update User Role</td>
                <?php } else { ?>
	                <td class="formHeaderRow" colspan="2">Add New User Role</td>
                <?php } ?>
			</tr>
			<tr>
				<td class="formLabelContainer">User Role Name:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<input class="textBox" type="text" id="userRole" name="userRole" value="<?php echo set_value('userRole'); ?>" size="50" />
				</td>
			</tr>
			<tr class="formAlternateRow">
				<td class="formLabelContainer">Is Admin:</td>
				<td class="formTextBoxContainer">
					<input type="checkbox" id="isAdmin" name="isAdmin" value="1" <?php if($isAdmin == 1) { echo ' checked=true';} ?> />
				</td>
			</tr>
			<tr>
				<td class="formLabelContainer">Status:</td>
				<td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
				</td>
			</tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addUserRole" type="submit" value="Save">
                    <?php if(strpos($_SERVER["REQUEST_URI"],$userRoleID) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_user_role' ?>';">
                    <?php } ?>
                </td>
            </tr>
		</table>	
	</div>
</div>

</form>

<script>
	$('#userRole').val('<?php echo $userRole; ?>');
	$('#status').val('<?php echo $status; ?>');
	<?php if($canWrite == 0) { ?>
	$("#frmAddUser :input").attr("disabled", true);
	<?php } ?>
</script>
