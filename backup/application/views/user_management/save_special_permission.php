<?php
$empID					= (isset($_POST['empID']))				?	$_POST['empID'] 				:	$arrRecord['emp_id'];
$moduleName 			= (isset($_POST['moduleName'])) 	   	?	$_POST['moduleName'] 			:	$arrRecord['module_name'];
$screenName 			= (isset($_POST['screenName'])) 		?	$_POST['screenName']			:	$arrRecord['sub_module_name'];
$userRole				= (isset($_POST['userRole'])) 			? 	$_POST['userRole']				:	$arrRecord['assigned_role_id'];
?>

<form name="frmAddModule" id="frmAddModule" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($arrRecord['emp_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Permissions</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Special Permissions</td>
                <?php } ?>
			</tr>
            <tr>
                <td class="formLabelContainer">Employee:</td>
                <td class="formTextBoxContainer">
                    <select id="empID" name="empID" class="dropDown">
                        <option value="">Select Employee</option>
                        <?php
						if (count($arrEmployees)) {
							foreach($arrEmployees as $key => $arrEmployee) {
						?>
							<optgroup label="<?php echo $key; ?>">
								<?php for($i=0;$i<count($arrEmployee);$i++) { ?>					
									<option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_code']." - ".$arrEmployee[$i]['emp_full_name']; ?></option>
								<?php } ?>
							</optgroup>
						<?php	}
						}
						?>
                    </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Departments:</td>
                <td class="formTextBoxContainer">
				  <select id="empDepartment" name="empDepartment" class="dropDown">
					<option value="">All</option>
					<?php
					  for($ind = 0; $ind < count($empDepartments); $ind++) {
						  $selected = '';
						  if($empDepartments[$ind]['job_category_id'] == $empDepartment) {
							  $selected = 'selected="selected"';
						  }
					?>
					 <option value="<?php echo $empDepartments[$ind]['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $empDepartments[$ind]['job_category_name']; ?></option>
					<?php
					  }
					?>
				  </select>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">User Role:</td>
                <td class="formTextBoxContainer">
					<select class="dropDown" id="userRoleID" name="userRoleID">
						<option value="">All</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>"><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Module:</td>
                <td class="formTextBoxContainer">
                    <select class="dropDown" id="moduleName" name="moduleName">
                        <option value="">Select Module</option>
                        <?php
                        if (count($arrModules)) {
                            foreach($arrModules as $arrModule) {
                        ?>
                        <option value="<?php echo $arrModule['module_name']; ?>"><?php echo $arrModule['display_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Sub Module Name:</td>
                <td class="formTextBoxContainer">
                    <input class="textBox" type="text" id="screenName" name="screenName" value="<?php echo $screenName; ?>" maxlength="100" />
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Assigned Role ID:</td>
                <td class="formTextBoxContainer">
                    <select class="dropDown" id="userRole" name="userRole">
						<option value="">Select User Role</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>"><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addModule" type="submit" value="Save">
                    <input class="smallButton" type="button" name="btnBack" id="btnBack" value="Back" onClick="history.go(-1)">
                </td>
        	</tr>
		</table>
	</div>      
</div>
</form>

<script>
	$('#empID').val('<?php echo $empID; ?>');
	$('#moduleName').val('<?php echo $moduleName; ?>');
	$('#userRole').val('<?php echo $userRole; ?>');
</script>