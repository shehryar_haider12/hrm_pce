<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: '';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: '';
?>
<form name="frmListPayroll" id="frmListPayroll" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee:</div>
        <div class="textBoxContainer">
			<select name="empID" id="empID" class="dropDown">
              <option value="">All</option>
              <?php
				if (count($arrEmployees)) {
					foreach($arrEmployees as $key => $arrEmp) {
				?>
					<optgroup label="<?php echo $key; ?>">
						<?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
							<option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
						<?php } ?>
					</optgroup>
				<?php	}
				}
				?>
          </select>
        </div>
        <div class="labelContainer">Supervisor:</div>
        <div class="textBoxContainer">
        	<select name="empSupervisor" id="empSupervisor" class="dropDown">
            	<option value="">All</option>
                <?php
                if (count($arrSupervisors)) {
                    foreach($arrSupervisors as $key => $arrSupervisor) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($i=0;$i<count($arrSupervisor);$i++) { ?>					
                            <option value="<?php echo $arrSupervisor[$i]['emp_id']; ?>"><?php echo $arrSupervisor[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
                <?php	}
                }
                ?>
            </select>
        </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Company:</div>
        <div class="textBoxContainer">
        	<select id="empCompany" name="empCompany" class="dropDown">
                <option value="">All</option>
                <?php
                  for($ind = 0; $ind < count($arrCompanies); $ind++) {
                      $selected = '';
                      if($arrCompanies[$ind]['company_id'] == $empCompany) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $arrCompanies[$ind]['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompanies[$ind]['company_name']; ?></option>
                <?php
                  }
                ?>
              </select>
        </div>
        <div class="labelContainer">Bank:</div>
        <div class="textBoxContainer">
        	<select id="empBank" name="empBank" class="dropDown">
                <option value="">All</option>
                <?php
                  for($ind = 0; $ind < count($arrBanks); $ind++) {
                      $selected = '';
                      if($arrBanks[$ind]['bank_id'] == $empBank) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $arrBanks[$ind]['bank_id']; ?>" <?php echo $selected; ?>><?php echo $arrBanks[$ind]['bank_name']; ?></option>
                <?php
                  }
                ?>
              </select>
        </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Month:</div>
        <div class="textBoxContainer">
        	<select name="selMonth" id="selMonth" class="dropDown">
            	<option value="">All</option>
                <?php
				if (count($arrMonths)) {
					foreach($arrMonths as $strKey => $strValue) {
				?>
				<option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
				<?php
					}
				}
				?>
            </select>
        </div>          
        <div class="labelContainer">Year:</div>
        <div class="textBoxContainer">
        	<select name="selYear" id="selYear" class="dropDown">
            	<option value="">All</option>
                <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
                <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                <?php } ?>
            </select>
        </div>
      </div>
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
      </div>
    </div>
  </div> 
  <script>
  	$('#empID').val('<?php echo $empID; ?>');
  	$('#empIP').val('<?php echo $empIP; ?>');
  	$('#empDesignation').val('<?php echo $empDesignation; ?>');
	$('#empDepartment').val('<?php echo $empDepartment; ?>');
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  	$('#empCompany').val('<?php echo $empCompany; ?>');
  	$('#selMonth').val('<?php echo $selMonth; ?>');
  	$('#selYear').val('<?php echo $selYear; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Employee Name</td>
    <td class="listHeaderCol">Month</td>
    <td class="listHeaderCol">Year</td>
    <td class="listHeaderCol">Earning</td>
    <td class="listHeaderCol">Deduction</td>
    <td class="listHeaderCol">Net</td>
    <td class="listHeaderCol">Late Coming Deduction</td>
    <td class="listHeaderColLast">Action</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		$trCSSClass = ' class="listContent"';
		if($ind % 2 == 0) {
			$trCSSClass = ' class="listContentAlternate"';
		}
		$currencyCode = getValue($arrLocations, 'location_id', $arrRecords[$ind]['emp_currency_id'], 'location_currency_code') . ' ';
		$totalEarning = (int)$arrRecords[$ind]['payroll_earning_basic'] + 
						(int)$arrRecords[$ind]['payroll_earning_housing'] +
						(int)$arrRecords[$ind]['payroll_earning_transport'] +
						(int)$arrRecords[$ind]['payroll_earning_utility'] +
						(int)$arrRecords[$ind]['payroll_earning_travel'] +
						(int)$arrRecords[$ind]['payroll_earning_health'] +
						(int)$arrRecords[$ind]['payroll_earning_fuel'] +
						(int)$arrRecords[$ind]['payroll_earning_mobile'] +
						(int)$arrRecords[$ind]['payroll_earning_medical_relief'] +
						(int)$arrRecords[$ind]['payroll_earning_bonus'] +
						(int)$arrRecords[$ind]['payroll_earning_annual_leave_encashment'] +
						(int)$arrRecords[$ind]['payroll_earning_claims'] +
						(int)$arrRecords[$ind]['payroll_earning_commission'] +
						(int)$arrRecords[$ind]['payroll_earning_annual_ticket'] +
						(int)$arrRecords[$ind]['payroll_earning_gratuity'] +
						(int)$arrRecords[$ind]['payroll_earning_survey_expense'] +
						(int)$arrRecords[$ind]['payroll_earning_settlement'] +
						(int)$arrRecords[$ind]['payroll_earning_misc'];
						
		// $totalDeduction = (int)$arrRecords[$ind]['payroll_deduction_tax'] + 
		// 				  (int)$arrRecords[$ind]['payroll_deduction_pf'] + 
		// 				  (int)$arrRecords[$ind]['payroll_deduction_loan'] + 
		// 				  (int)$arrRecords[$ind]['payroll_deduction_eobi'] + 
		// 				  (int)$arrRecords[$ind]['payroll_deduction_telephone'] + 
		// 				  (int)$arrRecords[$ind]['payroll_deduction_misc'];
		$totalDeduction = (int)$arrRecords[$ind]['payroll_deduction_tax'] + 
						  (int)$arrRecords[$ind]['payroll_deduction_pf'] + 
						  (int)$arrRecords[$ind]['payroll_deduction_loan'] + 
						  (int)$arrRecords[$ind]['payroll_deduction_eobi'] + 
						  (int)$arrRecords[$ind]['payroll_deduction_telephone'] + 
						  (int)$arrRecords[$ind]['payroll_deduction_misc'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_mobile'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_jeans'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_smoking'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_wrong_time'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_cashier_checking'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_guard_attention'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_dvr_off'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_laptop_in_branch'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_no_response'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_branch_incharge'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_CCFLA'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_dealing_without'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_misbehave'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_open_close'] +
              (int)$arrAllRecords[$ind]['payroll_deduction_late_coming'] + 
              (int)$arrAllRecords[$ind]['payroll_deduction_pending_work'];
	?>
  <tr<?php echo $trCSSClass; ?> id="tr<?php echo $ind; ?>">
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo date('F', mktime(0, 0, 0, (int)$arrRecords[$ind]['payroll_month'], 10)); ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['payroll_year']; ?></td>
    <td class="listContentCol"><?php echo  $currencyCode . number_format($totalEarning); ?></td>
    <td class="listContentCol"><?php echo $currencyCode . number_format($totalDeduction); ?></td>
    <td class="listContentCol"><?php echo $currencyCode . number_format($totalEarning - $totalDeduction); ?></td>
    <td class="listContentCol"><?php echo 'sdfasdf'.$currencyCode . number_format($totalDeduction); ?></td>
    <td class="listContentColLast">
      <div class="empColButtonContainer">
        <?php if($canWrite == YES) { ?>
      	<img title="PaySlip" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/display.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/payslip/' . $arrRecords[$ind]['payroll_id'] . '/' . $arrRecords[$ind]['emp_id']; ?>';" />
      	<img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/save_payroll/' . $arrRecords[$ind]['payroll_id']; ?>';">
      	<?php } ?>
        <?php if($canDelete == YES && (int)$arrRecords[$ind]['payroll_month'] > $this->payrollCloseMonth && (int)$arrRecords[$ind]['payroll_year'] >= $this->payrollCloseYear) { ?>
      	<img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecords[$ind]['payroll_id']; ?>');">
      	<?php } ?>
	  </div>
	  </td>
  </tr>
  <?php
	}
	if((int)$totalNetSalary) {
  ?>
  <tr<?php echo $trCSSClass; ?>>
    <td class="listContentCol"><b>Grand Total</b></td>
    <td class="listContentCol"></td>
    <td class="listContentCol"></td>
    <td class="listContentCol"><b><?php echo  $currencyCode . number_format($totalEarnings); ?></b></td>
    <td class="listContentCol"><b><?php echo $currencyCode . number_format($totalDeductions); ?></b></td>
    <td class="listContentCol"><b><?php echo $currencyCode . number_format($totalNetSalary); ?></b></td>
    <td class="listContentColLast"></td>
  </tr>
  <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="7" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>