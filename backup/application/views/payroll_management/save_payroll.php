<?php
$empID 					= (isset($_POST['empID'])) 					? $_POST['empID'] 					: $record['emp_id'];
$payrollMonth 			= (isset($_POST['payrollMonth'])) 			? $_POST['payrollMonth'] 			: $record['payroll_month'];
$payrollYear 			= (isset($_POST['payrollYear'])) 			? $_POST['payrollYear'] 			: $record['payroll_year'];

$payrollBasic 			= (isset($_POST['payrollBasic'])) 			? $_POST['payrollBasic'] 			: $record['payroll_earning_basic'];
$payrollHousing 		= (isset($_POST['payrollHousing'])) 		? $_POST['payrollHousing'] 			: $record['payroll_earning_housing'];
$payrollTransport 		= (isset($_POST['payrollTransport'])) 		? $_POST['payrollTransport'] 		: $record['payroll_earning_transport'];
$payrollUtility 		= (isset($_POST['payrollUtility'])) 		? $_POST['payrollUtility'] 			: $record['payroll_earning_utility'];
$payrollTravel 			= (isset($_POST['payrollTravel'])) 			? $_POST['payrollTravel'] 			: $record['payroll_earning_travel'];
$payrollHealth 			= (isset($_POST['payrollHealth'])) 			? $_POST['payrollHealth'] 			: $record['payroll_earning_health'];
$payrollFuel 			= (isset($_POST['payrollFuel'])) 			? $_POST['payrollFuel'] 			: $record['payroll_earning_fuel'];
$payrollMobile 			= (isset($_POST['payrollMobile'])) 			? $_POST['payrollMobile'] 			: $record['payroll_earning_mobile'];
$payrollMedical 		= (isset($_POST['payrollMedical'])) 		? $_POST['payrollMedical'] 			: $record['payroll_earning_medical_relief'];
$payrollBonus 			= (isset($_POST['payrollBonus'])) 			? $_POST['payrollBonus'] 			: $record['payroll_earning_bonus'];
$payrollLeaveEnc 		= (isset($_POST['payrollLeaveEnc'])) 		? $_POST['payrollLeaveEnc'] 		: $record['payroll_earning_annual_leave_encashment'];
$payrollClaim 			= (isset($_POST['payrollClaim'])) 			? $_POST['payrollClaim'] 			: $record['payroll_earning_claims'];
$payrollCommission 		= (isset($_POST['payrollCommission'])) 		? $_POST['payrollCommission'] 		: $record['payroll_earning_commission'];
$payrollAnnualTicket 	= (isset($_POST['payrollAnnualTicket'])) 	? $_POST['payrollAnnualTicket'] 	: $record['payroll_earning_annual_ticket'];
$payrollGratuity 		= (isset($_POST['payrollGratuity'])) 		? $_POST['payrollGratuity'] 		: $record['payroll_earning_gratuity'];
$payrollSurveyExpense 	= (isset($_POST['payrollSurveyExpense'])) 	? $_POST['payrollSurveyExpense'] 	: $record['payroll_earning_survey_expense'];
$payrollSettlement 		= (isset($_POST['payrollSettlement'])) 		? $_POST['payrollSettlement'] 		: $record['payroll_earning_settlement'];
$payrollEarningMisc 	= (isset($_POST['payrollEarningMisc'])) 	? $_POST['payrollEarningMisc'] 		: $record['payroll_earning_misc'];

$payrollTax 			= (isset($_POST['payrollTax'])) 			? $_POST['payrollTax'] 				: $record['payroll_deduction_tax'];
$payrollPF 				= (isset($_POST['payrollPF'])) 				? $_POST['payrollPF'] 				: $record['payroll_deduction_pf'];
$payrollLoan 			= (isset($_POST['payrollLoan'])) 			? $_POST['payrollLoan'] 			: $record['payroll_deduction_loan'];
$payrollEOBI 			= (isset($_POST['payrollEOBI'])) 			? $_POST['payrollEOBI'] 			: $record['payroll_deduction_eobi'];
$payrollDeductionTelephone = (isset($_POST['payrollDeductionTelephone'])) 	? $_POST['payrollDeductionTelephone'] 	: $record['payroll_deduction_telephone'];
$payrollDeductionMisc	= (isset($_POST['payrollDeductionMisc'])) 	? $_POST['payrollDeductionMisc'] 	: $record['payroll_deduction_misc'];
?>
<form name="frmSavePayroll" id="frmSavePayroll" method="post">
<div class="listPageMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="5">Payroll Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Employee:</td>
            <td class="formTextBoxContainer" colspan="4">
            	<select name="empID" id="empID" class="dropDown">
                  <option value="">Select Employee</option>
                  <?php
                    if (count($arrEmployees)) {
                        foreach($arrEmployees as $key => $arrEmp) {
                    ?>
                        <optgroup label="<?php echo $key; ?>">
                            <?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
                                <option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php	}
                    }
                    ?>
              </select>
            </td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Month:</td>
            <td class="formTextBoxContainer" colspan="4">
            	<select name="payrollMonth" id="payrollMonth" class="dropDown">
                  <option value="">Select Month</option>
                  <?php
				  if (count($arrMonths)) {
					  foreach($arrMonths as $strKey => $strValue) {
				  ?>
				  <option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
				  <?php
					  }
				  }
				  ?>
              </select>
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Year:</td>
            <td class="formTextBoxContainer" colspan="4">
            	<select name="payrollYear" id="payrollYear" class="dropDown">
                  <option value="">Select Year</option>
                  <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
                  <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                  <?php } ?>
              </select>
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow" colspan="2" align="center">Earning</td>
            <td></td>
            <td class="formHeaderRow" colspan="2" align="center">Deduction</td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Basic:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" name="payrollBasic" id="payrollBasic" class="textBox" value="<?php echo $payrollBasic; ?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Tax:</td>
            <td class="formTextBoxContainer">
                <input type="text" name="payrollTax" id="payrollTax" class="textBox" value="<?php echo $payrollTax; ?>">
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Housing Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollHousing" id="payrollHousing" class="textBox" value="<?php echo $payrollHousing; ?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Provident Fund:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollPF" id="payrollPF" class="textBox" value="<?php echo $payrollPF; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Transport Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollTransport" id="payrollTransport" class="textBox" value="<?php echo $payrollTransport; ?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Salary Advance/Loan Recovery:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollLoan" id="payrollLoan" class="textBox" value="<?php echo $payrollLoan; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Utility Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollUtility" id="payrollUtility" class="textBox" value="<?php echo $payrollUtility; ?>">
            </td>
            <td></td>
            <td class="formLabelContainer">EOBI:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollEOBI" id="payrollEOBI" class="textBox" value="<?php echo $payrollEOBI; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Travel Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollTravel" id="payrollTravel" class="textBox" value="<?php echo $payrollTravel; ?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Telephone Expense:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionTelephone" id="payrollDeductionTelephone" class="textBox" value="<?php echo $payrollDeductionTelephone; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Travel/Survey Expense:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollSurveyExpense" id="payrollSurveyExpense" class="textBox" value="<?php echo $payrollSurveyExpense; ?>">
            </td>
            <td></td>
            <td class="formLabelContainer">Others/Misc.:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollDeductionMisc" id="payrollDeductionMisc" class="textBox" value="<?php echo $payrollDeductionMisc; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Commission:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollCommission" id="payrollCommission" class="textBox" value="<?php echo $payrollCommission; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Bonus:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollBonus" id="payrollBonus" class="textBox" value="<?php echo $payrollBonus; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Annual Ticket:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollAnnualTicket" id="payrollAnnualTicket" class="textBox" value="<?php echo $payrollAnnualTicket; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Health Allowance:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollHealth" id="payrollHealth" class="textBox" value="<?php echo $payrollHealth; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Fuel:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollFuel" id="payrollFuel" class="textBox" value="<?php echo $payrollFuel; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Mobile:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollMobile" id="payrollMobile" class="textBox" value="<?php echo $payrollMobile; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Medical Relief:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollMedical" id="payrollMedical" class="textBox" value="<?php echo $payrollMedical; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Claim:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollClaim" id="payrollClaim" class="textBox" value="<?php echo $payrollClaim; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Annual Leave Encashment:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollLeaveEnc" id="payrollLeaveEnc" class="textBox" value="<?php echo $payrollLeaveEnc; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Gratuity:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollGratuity" id="payrollGratuity" class="textBox" value="<?php echo $payrollGratuity; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Final Settlement:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollSettlement" id="payrollSettlement" class="textBox" value="<?php echo $payrollSettlement; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Others/Misc.:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="payrollEarningMisc" id="payrollEarningMisc" class="textBox" value="<?php echo $payrollEarningMisc; ?>">
            </td>
            <td colspan="3"></td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer" colspan="4">
      		<?php if($canWrite == YES) { ?>
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
            <?php } ?>
    		</td>
        </tr>
  </table>
  </div>
</form>
<script>
  $('#empID').val('<?php echo $empID; ?>');
  $('#payrollMonth').val('<?php echo $payrollMonth; ?>');
  $('#payrollYear').val('<?php echo $payrollYear; ?>');
  
  <?php if((int)$record['payroll_month'] <= $this->payrollCloseMonth && (int)$record['payroll_year'] <= $this->payrollCloseYear && (int)$payrollID > 0) { ?>
  $("#frmSavePayroll :input").attr("disabled", true).css({"background-color": "#ddd"});
  $(".smallButton").remove();
  <?php } ?>
</script>