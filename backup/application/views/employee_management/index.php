<div class="midSection">
    <div class="dashboardTitle">
        Welcome To Employee Management System
    </div>
    
    <div class="notificationMain">
    	<div class="notificationBox">
        	<h1>Notice Board</h1>
            <div class="notificationContent">
                <div class="boldBlueHeading" style="font-family:arial; line-height:22px">                
                	<div class="internalMessageBox"> Only HR department is authorised to make any change in Employement Management System. If any modification is required in your details, please request HR Department through <a href="<?php echo $this->baseURL . '/complain_management/submit_complain'; ?>" target="_blank">Request Management Module</a>.</div>
                </div>
            </div>
        </div>
    </div>

	<div class="dashboardBoxesMain">
	<?php
	foreach($allowedSubModulesList as $modules => $moduleValue)
	{
	?>
		<div class="boxMain">			
				<div class="boxImageMain">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
                		<img src="<?php echo $this->imagePath . '/modules/' . $moduleValue['module_name']; ?>.png" alt="">
                    </a>
                </div>
				<!--<div class="boxTitle">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
						<?php echo $moduleValue['display_name']; ?>
                    </a>
                </div>-->
		</div>
	<?php
	}
	?>
	</div>
</div>