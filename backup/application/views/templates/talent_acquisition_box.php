<div class="employeeDetailLeftCol">    
    <div class="employeeDetailLinksMain">
        <ul>
        	<?php if($showOptions == false) { ?>
           		<li id="manage_contest"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/manage_contest'; ?>">Contest Management</a></li>
            <?php } else if($showOptions == true) { ?>
            	<li id="manage_contest"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/manage_contest/' . $contestID; ?>">Contest Management</a></li>
                <li id="contest_categories"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/contest_categories/' . $contestID; ?>">Contest Categories</a></li>
                <li id="contest_result"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/contest_result/' . $contestID; ?>">Contest Result</a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<script>$('#<?php echo $this->currentAction; ?>').addClass('selected');</script>
