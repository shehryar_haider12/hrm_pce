<?php $userRoleID = $this->session->userdata('user_role_id');
$headerTitle = "ValuStrat Employee Dashboard - HRMS";
?>
<div class="headerTitle">
    <a href="<?php echo base_url();?>home">
        <img src="<?php echo $this->imagePath; ?>/logo.png" alt="ValuStrat HRM" title="ValuStrat HRM">
    </a>
</div>

<?php if($this->currentController != 'message') {
		if($this->userID != 0) { ?>
	<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || (int)$this->session->userdata('user_is_webadmin')) { ?>
<div class="logoutMain" style="padding-top:22px">
	<div style="float:right">    	
    	<form name="frmChangeUser" id="frmChangeUser" method="post">
            <div class="logoutText">Use As:&nbsp;
            <select name="changeUser" id="changeUser" onchange="this.form.submit()" class="dropDown" style="float:right">
                <option value="">Select Employee</option>
                <?php for($ind = 0; $ind < count($this->allEmployees); $ind++) { ?>
                <option value="<?php echo $this->allEmployees[$ind]['emp_id']; ?>"><?php echo $this->allEmployees[$ind]['emp_full_name']; ?> - <?php echo $this->allEmployees[$ind]['user_role_name']; ?></option>
                <?php } ?>
            </select></div>
            <script>$('#changeUser').val('<?php echo (int)$this->userEmpNum; ?>');</script>
        </form>
    </div>
    <?php } else { ?>
    <div class="logoutMain">
    <?php } ?>
	<div class="logoutText">
		Welcome - <?php echo $this->userWelcomeName; ?>
	</div>
	
	<?php echo form_open(); ?>
	<div class="logoutButton">
		<input class="smallButton" name="logoffBtn" type="submit" value="Logout">
		<input type="hidden" name="logoff" value="1">
	</div>
	<?php echo form_close(); ?>
</div>
<?php }
} ?>