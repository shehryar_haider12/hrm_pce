<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$selStatus = ($selStatus != '') ? $selStatus : $this->input->post('selStatus');
?>

<form name="frmListCompanies" id="frmListCompanies" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
    	<div class="searchHeader">Search Criteria</div>
        
        <div class="searchcontentmain">
            <div class="searchCol">
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<?php echo statusCombo('selStatus',$this->userRoleID, 'All', 'dropDown'); ?>
				</div>
			</div>
			
			<div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchCompanies" id="btnSearchCompanies" value="Search">
			</div>
		</div>
	</div>
      
  <script>
  	$('#selStatus').val('<?php echo $selStatus; ?>');
  </script>
</form>

	<?php if($canWrite == 1) { ?>
	<div class="centerButtonContainer">
		<input class="addButton" type="button" value="Add New Company" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_company' ?>';" />
	</div>
	<?php }	?>
    
	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php 	}	?>
	</div>

	<div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
            <td class="listHeaderCol">Name</th>
            <td class="listHeaderCol">Address</th>
            <td class="listHeaderCol">Head</th>
            <td class="listHeaderCol">Country</th>
            <td class="listHeaderCol">Tax</th>
            <td class="listHeaderCol">Visas</th>
            <td class="listHeaderCol">Status</th>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['company_status'] != STATUS_DELETED))) { ?>
            <td class="listHeaderColLast" style="width:100px">Action</th>
            <?php } ?>
        </tr>
        <?php
        for($ind = 0; $ind < count($arrRecords); $ind++) 
		{
			($arrRecords[$ind]['company_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
        ?>
        <tr class="<?php echo $classListingData; ?>">
            <td class="listContentCol"><?php echo $arrRecords[$ind]['company_name']; ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['company_address']; ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['company_head_name']; ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['company_country_name']; ?></td>
            <td class="listContentCol"><?php if((int)$arrRecords[$ind]['company_tax_applicable']) echo 'Yes'; else echo 'No'; ?></td>
            <td class="listContentCol"><?php echo '<b>Total:</b> ' . (int)$arrRecords[$ind]['company_visas'] . '<br /><b>Granted:</b> ' . (int)$arrRecords[$ind]['company_visas_granted']; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['company_status'] == STATUS_ACTIVE) echo "Active"; else if ($arrRecords[$ind]['company_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; else if ($arrRecords[$ind]['company_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['company_status'] != STATUS_DELETED))) { ?>
			<td class="listContentColLast">
            	<div class="colButtonContainer" style="width:150px">
                <?php if($canWrite == 1) { ?>
                    <input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_company/' . $arrRecords[$ind]['company_id']; ?>';" />
                <?php } ?>
                <?php if(($canDelete == 1) && ($arrRecords[$ind]['company_status'] != STATUS_DELETED)){ ?>
                    <input class="smallButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['company_id']; ?>');" />
                <?php } ?>
                </div>
            </td>
			<?php }	?>
        </tr>
        <?php
       }
       ?>
    </table>
    </div>
</div>