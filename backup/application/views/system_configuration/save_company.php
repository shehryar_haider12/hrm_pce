<?php
$comName	 		= (isset($_POST['comName']))			?	$_POST['comName']			:	$record['company_name'];
$comAddress 		= (isset($_POST['comAddress']))			?	$_POST['comAddress']		:	$record['company_address'];
$comCountryID 		= (isset($_POST['comCountryID']))		?	$_POST['comCountryID']		:	$record['company_country_id'];
$comCurrencyID 		= (isset($_POST['comCurrencyID']))		?	$_POST['comCurrencyID']		:	$record['company_currency_id'];
$comHeadID 			= (isset($_POST['comHeadID']))			?	$_POST['comHeadID']			:	$record['company_head_id'];
$comAnnualLeaves 	= (isset($_POST['comAnnualLeaves']))	?	$_POST['comAnnualLeaves']	:	$record['company_annual_leaves'];
$comSickLeaves 		= (isset($_POST['comSickLeaves']))		?	$_POST['comSickLeaves']		:	$record['company_sick_leaves'];
$comTaxApplicable 	= (isset($_POST['comTaxApplicable']))	?	$_POST['comTaxApplicable']	:	$record['company_tax_applicable'];
$comWeeklyOff1	 	= (isset($_POST['comWeeklyOff1']))		?	$_POST['comWeeklyOff1']		:	$record['company_weekly_off_1'];
$comWeeklyOff2	 	= (isset($_POST['comWeeklyOff2']))		?	$_POST['comWeeklyOff2']		:	$record['company_weekly_off_2'];
$comVisas 			= (isset($_POST['comVisas']))			?	$_POST['comVisas']			:	$record['company_visas'];

//Code Added By Zeeshan  Qasim
$tradeLicenseIssue	=	(isset($_POST['tradeLicenseIssue']))     ?	$_POST['tradeLicenseIssue']			:	$record['trade_license_issue_date'];
$tradeLicenseExpiry	=	(isset($_POST['tradeLicenseExpiry']))     ?	$_POST['tradeLicenseExpiry']		:	$record['trade_license_expiry_date'];
$establishmentCardIssue	=(isset($_POST['establishmentCardIssue']))   ? $_POST['establishmentCardIssue'] :	$record['establishment_card_issue_date'];
$establishmentCardExpiry	=(isset($_POST['establishmentCardExpiry']))   ? $_POST['establishmentCardExpiry'] :	$record['establishment_card_expiry_date'];

$dubaiChamberIssue	=(isset($_POST['dubaiChamberIssue']))   ? $_POST['dubaiChamberIssue'] :	$record['dubaichamber_issue_date'];

$dubaiChamberExpiry	=(isset($_POST['dubaiChamberExpiry']))   ? $_POST['dubaiChamberExpiry'] :	$record['dubaichamber_expiry_date'];

$piCoverIssue	=(isset($_POST['piCoverIssue']))   ? $_POST['piCoverIssue'] :	$record['picover_issue_date'];

$piCoverExpiry	=(isset($_POST['piCoverExpiry']))   ? $_POST['piCoverExpiry'] :	$record['picover_expiry_date'];

$tenancyContractIssue	=(isset($_POST['tenancyContractIssue']))   ? $_POST['tenancyContractIssue'] :	$record['tenancycontract_issue_date'];

$tenancyContractExpiry	=(isset($_POST['tenancyContractExpiry']))   ? $_POST['tenancyContractExpiry'] :	$record['tenancycontract_expiry_date'];

$maintenanceContractIssue	=(isset($_POST['maintenanceContractIssue']))   ? $_POST['maintenanceContractIssue'] :	$record['maintenance_contract_issue_date'];

$maintenanceContractExpiry	=(isset($_POST['maintenanceContractExpiry']))   ? $_POST['maintenanceContractExpiry'] :	$record['maintenance_contract_expiry_date'];

$dcdFireIssue	=(isset($_POST['dcdFireIssue']))   ? $_POST['dcdFireIssue'] :	$record['dcdfire_issue_date'];
$dcdFireExpiry	=(isset($_POST['dcdFireExpiry']))   ? $_POST['dcdFireExpiry'] :	$record['dcdfire_expiry_date'];


$poboxIssue	=(isset($_POST['poboxIssue']))   ? $_POST['poboxIssue'] :	$record['pobox_renewal_issue_date'];

$poboxExpiry	=(isset($_POST['poboxExpiry']))   ? $_POST['poboxExpiry'] :	$record['pobox_renewal_expiry_date'];

$businessoperationIssue	=(isset($_POST['businessoperationIssue']))   ? $_POST['businessoperationIssue'] :	$record['business_operation_issue_date'];

$businessoperationExpiry	=(isset($_POST['businessoperationExpiry']))   ? $_POST['businessoperationExpiry'] :	$record['business_operation_expiry_date'];


$reraIssue	=(isset($_POST['reraIssue']))   ? $_POST['reraIssue'] :	$record['rera_issue_date'];

$reraExpiry	=(isset($_POST['reraExpiry']))   ? $_POST['reraExpiry'] :	$record['rera_expiry_date'];


// Code Added End By Zeeshan Qasim 


if ($record['company_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['company_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['company_status'] == STATUS_DELETED) {$recordStatus = 2;}
$comStatus 			= (isset($_POST['comStatus'])) 			?	$_POST['comStatus']			:	$recordStatus;
?>


<!--Code Added By Zeeshan Qasim !-->
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#empDOB" ).datepicker( "setDate", "<?php echo $empDOB; ?>" );
	$( "#empDOB" ).datepicker( "option", "maxDate", '<?php echo date('Y-m-d', strtotime('18 years ago')); ?>' );
	
	<?php if($tradeLicenseIssue != '0000-00-00') { ?>
		$( "#tradeLicenseIssue" ).datepicker( "setDate", "<?php echo $tradeLicenseIssue; ?>" );
		$( "#tradeLicenseIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($tradeLicenseExpiry != '0000-00-00') { ?>
		$( "#tradeLicenseExpiry" ).datepicker( "setDate", "<?php echo $tradeLicenseExpiry; ?>" );
		$( "#tradeLicenseExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($establishmentCardIssue != '0000-00-00') { ?>
		$( "#establishmentCardIssue" ).datepicker( "setDate", "<?php echo $establishmentCardIssue; ?>" );
		$( "#establishmentCardIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
<?php if($establishmentCardExpiry != '0000-00-00') { ?>
		$( "#establishmentCardExpiry" ).datepicker( "setDate", "<?php echo $establishmentCardExpiry; ?>" );
		$( "#establishmentCardExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($dubaiChamberIssue != '0000-00-00') { ?>
		$( "#dubaiChamberIssue" ).datepicker( "setDate", "<?php echo $dubaiChamberIssue; ?>" );
		$( "#dubaiChamberIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($dubaiChamberExpiry != '0000-00-00') { ?>
		$( "#dubaiChamberExpiry" ).datepicker( "setDate", "<?php echo $dubaiChamberExpiry; ?>" );
		$( "#dubaiChamberExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($piCoverIssue != '0000-00-00') { ?>
		$( "#piCoverIssue" ).datepicker( "setDate", "<?php echo $piCoverIssue; ?>" );
		$( "#piCoverIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($piCoverExpiry != '0000-00-00') { ?>
		$( "#piCoverExpiry" ).datepicker( "setDate", "<?php echo $piCoverExpiry; ?>" );
		$( "#piCoverExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($tenancyContractIssue != '0000-00-00') { ?>
		$( "#tenancyContractIssue" ).datepicker( "setDate", "<?php echo $tenancyContractIssue; ?>" );
		$( "#tenancyContractIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($tenancyContractExpiry != '0000-00-00') { ?>
		$( "#tenancyContractExpiry" ).datepicker( "setDate", "<?php echo $tenancyContractExpiry; ?>" );
		$( "#tenancyContractExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($maintenanceContractIssue != '0000-00-00') { ?>
		$( "#maintenanceContractIssue" ).datepicker( "setDate", "<?php echo $maintenanceContractIssue; ?>" );
		$( "#maintenanceContractIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($maintenanceContractExpiry != '0000-00-00') { ?>
		$( "#maintenanceContractExpiry" ).datepicker( "setDate", "<?php echo $maintenanceContractExpiry; ?>" );
		$( "#maintenanceContractExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($dcdFireIssue != '0000-00-00') { ?>
		$( "#dcdFireIssue" ).datepicker( "setDate", "<?php echo $dcdFireIssue; ?>" );
		$( "#dcdFireIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($dcdFireExpiry != '0000-00-00') { ?>
		$( "#dcdFireExpiry" ).datepicker( "setDate", "<?php echo $dcdFireExpiry; ?>" );
		$( "#dcdFireExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	
	<?php if($poboxIssue != '0000-00-00') { ?>
		$( "#poboxIssue" ).datepicker( "setDate", "<?php echo $poboxIssue; ?>" );
		$( "#poboxIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($poboxExpiry != '0000-00-00') { ?>
		$( "#poboxExpiry" ).datepicker( "setDate", "<?php echo $poboxExpiry; ?>" );
		$( "#poboxExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($businessoperationIssue != '0000-00-00') { ?>
		$( "#businessoperationIssue" ).datepicker( "setDate", "<?php echo $businessoperationIssue; ?>" );
		$( "#businessoperationIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($businessoperationExpiry != '0000-00-00') { ?>
		$( "#businessoperationExpiry" ).datepicker( "setDate", "<?php echo $businessoperationExpiry; ?>" );
		$( "#businessoperationExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($reraIssue != '0000-00-00') { ?>
		$( "#reraIssue" ).datepicker( "setDate", "<?php echo $reraIssue; ?>" );
		$( "#reraIssue" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	<?php if($reraExpiry != '0000-00-00') { ?>
		$( "#reraExpiry" ).datepicker( "setDate", "<?php echo $reraExpiry; ?>" );
		$( "#reraExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
	
	
	
});
</script>

<!--Code Added End By Zeeshan Qasim !-->

<form name="frmAddCompany" id="frmAddCompany" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['company_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Company</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add Company</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Company Name:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                	<input type="text" id="comName" name="comName" class="textBox" value="<?php echo $comName; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Address:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                	<textarea id="comAddress" name="comAddress" class="textArea" rows="8"><?php echo $comAddress; ?></textarea>
                </td>
            </tr>
            <tr>
            	<td class="formLabelContainer">Country:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                	<select id="comCountryID" name="comCountryID" class="dropDown">
                    	<option value="">Select Country</option>
						<?php
                        if (count($Countries)) {
                            foreach($Countries as $arrCountry) {
								$strSelected = '';
								if($comCountryID == $dataCountry['location_id']) {
									$strSelected = 'selected="selected"';
								}
                        ?>
                            <option value="<?php echo $arrCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $arrCountry['location_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Currency:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                	<select id="comCurrencyID" name="comCurrencyID" class="dropDown">
                    	<option value="">Select Currency</option>
						<?php
                        if (count($Countries)) {
                            foreach($Countries as $arrCountry) {
								$strSelected = '';
								if($comCurrencyID == $dataCountry['location_id']) {
									$strSelected = 'selected="selected"';
								}
                        ?>
                            <option value="<?php echo $arrCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $arrCountry['location_name'] . ' - ' . $arrCountry['location_currency']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Company Head:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                    <select id="comHeadID" name="comHeadID" class="dropDown">
                      <option value="">Select Head</option>
                      <?php
                      if (count($empSupervisors)) {
                          foreach($empSupervisors as $supervisorVal) {
                              $selected = '';
                              if($comHeadID == $supervisorVal['emp_id']) {
                                  $selected = 'selected="selected"';
                              }
                      ?>
                          <option value="<?php echo $supervisorVal['emp_id']; ?>" <?php echo $selected; ?>><?php echo $supervisorVal['emp_full_name']; ?></option>
                      <?php
                          }
                      }
                      ?>
                  </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Annual Leaves:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                	<input type="text" id="comAnnualLeaves" name="comAnnualLeaves" maxlength="3" class="textBox" value="<?php echo $comAnnualLeaves; ?>">
                </td>
            </tr>
            <tr>
            	<td class="formLabelContainer">Sick Leaves:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
                	<input type="text" id="comSickLeaves" name="comSickLeaves" maxlength="100" class="textBox" value="<?php echo $comSickLeaves; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Tax Applicable:</td>
                <td class="formTextBoxContainer" align="left">
                	<input type="checkbox" id="comTaxApplicable" name="comTaxApplicable" value="1" class="textBox">
                </td>
            </tr>
             <tr>
            	<td class="formLabelContainer">Visa Quantity:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="comVisas" name="comVisas" maxlength="8" class="textBox" value="<?php echo $comVisas; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
            	<td class="formLabelContainer">Weekly Off 1:</td>
                <td class="formTextBoxContainer" align="left">                	
                    <select id="comWeeklyOff1" name="comWeeklyOff1" class="dropDown">
                      <option value="">N/A</option>
                      <?php
                      foreach($arrDays as $strKey => $strVal) {
                          $selected = '';
                          if($comWeeklyOff1 == $strKey) {
                              $selected = 'selected="selected"';
                          }
                      ?>
                          <option value="<?php echo $strKey; ?>" <?php echo $selected; ?>><?php echo $strVal; ?></option>
                      <?php
                      }
                      ?>
                  </select>
                </td>
            </tr>
             <tr>
            	<td class="formLabelContainer">Weekly Off 2:</td>
                <td class="formTextBoxContainer">               	
                    <select id="comWeeklyOff2" name="comWeeklyOff2" class="dropDown">
                      <option value="">N/A</option>
                      <?php
                      foreach($arrDays as $strKey => $strVal) {
                          $selected = '';
                          if($comWeeklyOff2 == $strKey) {
                              $selected = 'selected="selected"';
                          }
                      ?>
                          <option value="<?php echo $strKey; ?>" <?php echo $selected; ?>><?php echo $strVal; ?></option>
                      <?php
                      }
                      ?>
                  </select>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Status:<span class="mandatoryStar"> *</span></td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('comStatus', $this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
           
           
           <!-- Code Added By Zeeshan  Qasim!-->
            <tr>
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Trade Licence Issue Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="tradeLicenseIssue" id="tradeLicenseIssue" class="textBox datePicker" value="<?php echo $tradeLicenseIssue; ?>" tabindex="8">
            </td>
            </tr>
          
          
            <tr>
             <tr class="formAlternateRow">
            <td class="formLabelContainer">Trade Licence Expiry Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="tradeLicenseExpiry" id="tradeLicenseExpiry" class="textBox datePicker" value="<?php echo $tradeLicenseExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr>
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Establishment Card Issue Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="establishmentCardIssue" id="establishmentCardIssue" class="textBox datePicker" value="<?php echo $establishmentCardIssue; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr>
             <tr class="formAlternateRow">
            <td class="formLabelContainer">Establishment Card Expiry Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="establishmentCardExpiry" id="establishmentCardExpiry" class="textBox datePicker" value="<?php echo $establishmentCardExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr>
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Dubai Chamber Issue Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="dubaiChamberIssue" id="dubaiChamberIssue" class="textBox datePicker" value="<?php echo $dubaiChamberIssue; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr>
             <tr class="formAlternateRow">
            <td class="formLabelContainer">Dubai Chamber Expiry Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="dubaiChamberExpiry" id="dubaiChamberExpiry" class="textBox datePicker" value="<?php echo $dubaiChamberExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
             <tr>
             <tr class="formLabelContainer">
            <td class="formLabelContainer">PI Cover Issue Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="piCoverIssue" id="piCoverIssue" class="textBox datePicker" value="<?php echo $piCoverIssue; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr>
             <tr class="formAlternateRow">
            <td class="formLabelContainer">PI Cover Expiry Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="piCoverExpiry" id="piCoverExpiry" class="textBox datePicker" value="<?php echo $piCoverExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
             <tr>
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Tenancy Contract Issue Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="tenancyContractIssue" id="tenancyContractIssue" class="textBox datePicker" value="<?php echo $tenancyContractIssue; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr class="formAlternateRow">
            <td class="formLabelContainer">Tenancy Contract Expiry Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="tenancyContractExpiry" id="tenancyContractExpiry" class="textBox datePicker" value="<?php echo $tenancyContractExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Maintenance  Contract Issue Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="maintenanceContractIssue" id="maintenanceContractIssue" class="textBox datePicker" value="<?php echo $maintenanceContractIssue; ?>" tabindex="8">
            </td>
            </tr>
            
              <tr class="formAlternateRow">
            <td class="formLabelContainer">Maintenance  Contract Expiry Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="maintenanceContractExpiry" id="maintenanceContractExpiry" class="textBox datePicker" value="<?php echo $maintenanceContractExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Dcd Fire Issue Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="dcdFireIssue" id="dcdFireIssue" class="textBox datePicker" value="<?php echo $dcdFireIssue; ?>" tabindex="8">
            </td>
            </tr>
          
           <tr class="formAlternateRow">
            <td class="formLabelContainer">Dcd Fire Expiry Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="dcdFireExpiry" id="dcdFireExpiry" class="textBox datePicker" value="<?php echo $dcdFireExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
            
             <tr class="formLabelContainer">
            <td class="formLabelContainer">Po Box Renewal Issue Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="poboxIssue" id="poboxIssue" class="textBox datePicker" value="<?php echo $poboxIssue; ?>" tabindex="8">
            </td>
            </tr>
            
              <tr class="formAlternateRow">
            <td class="formLabelContainer">Po Box Renewal Expiry Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="poboxExpiry" id="poboxExpiry" class="textBox datePicker" value="<?php echo $poboxExpiry; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr class="formLabelContainer">
            <td class="formLabelContainer">BusinessOp Permit Issue Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="businessoperationIssue" id="businessoperationIssue" class="textBox datePicker" value="<?php echo $businessoperationIssue; ?>" tabindex="8">
            </td>
            </tr>
            
            <tr class="formAlternateRow">
            <td class="formLabelContainer">BusinessOp Permit Expiry Date:</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="businessoperationExpiry" id="businessoperationExpiry" class="textBox datePicker" value="<?php echo $businessoperationExpiry; ?>" tabindex="8">
            </td>
            </tr>
          
          <tr class="formLabelContainer">
            <td class="formLabelContainer">RERA Issue Date :</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="reraIssue" id="reraIssue" class="textBox datePicker" value="<?php echo $reraIssue; ?>" tabindex="8">
            </td>
            </tr>
            
              <tr class="formAlternateRow">
            <td class="formLabelContainer">RERA Expiry Date :</td>
            <td class="formTextBoxContainer">
        	<input type="text" name="reraExpiry" id="reraExpiry" class="textBox datePicker" value="<?php echo $reraExpiry; ?>" tabindex="8">
            </td>
            </tr>
          
          
          
          
            
          
          
          
          
          <!-- Code Added End By Zeeshan  Qasim!-->
          
           
           
           
           
           
           
           
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addGrade" type="submit" value="Save">
                    <?php if(strpos($_SERVER["REQUEST_URI"],$record['company_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_companies' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
$('#comCountryID').val('<?php echo $comCountryID; ?>');
$('#comCurrencyID').val('<?php echo $comCurrencyID; ?>');
$('#comHeadID').val('<?php echo $comHeadID; ?>');
$('#comStatus').val('<?php echo $comStatus; ?>');
if(<?php echo (int)$comTaxApplicable; ?> == 1) {
	$('#comTaxApplicable').attr('checked', true);
}
</script>