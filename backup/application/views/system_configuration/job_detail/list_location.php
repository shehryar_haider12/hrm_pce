<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$region = ($region != '') ? $region : $this->input->post('region');
$country = ($country != '') ? $country : $this->input->post('country');
$city = ($city != '') ? $city : $this->input->post('city');
$status = ($status != '') ? $status : $this->input->post('status');
?>

<form name="frmListLocations" id="frmListLocations" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
        <div class="searchHeader">Search Criteria</div>
            
            <div class="searchcontentmain">
                <div class="searchCol">
                	<div class="labelContainer">Region:</div>
                    <div class="textBoxContainer">					
                        <select id="region" name="region" class="dropDown">
                            <option value="">Select Region</option>
                            <?php
                            if (count($regions)) {
                                foreach($regions as $arrRegion) {
                            ?>
                                <option value="<?php echo $arrRegion['location_id']; ?>"><?php echo $arrRegion['location_name']; ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="labelContainer">Country:</div>
                    <div class="textBoxContainer">					
                        <select id="country" name="country" class="dropDown">
                            <option value="">Select Country</option>
                            <?php
                            if (count($countries)) {
                                foreach($countries as $arrCountry) {
                            ?>
                                <option value="<?php echo $arrCountry['location_id']; ?>"><?php echo $arrCountry['location_name']; ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
               </div>
               
               <div class="searchCol">                                     
                    <div class="labelContainer">Status:</div>
                    <div class="textBoxContainer">
                        <?php echo statusCombo('status',$this->userRoleID, 'All', 'dropDown'); ?>
                    </div>
              </div>
              
                <div class="formButtonContainerWide">
                    <input type="submit" class="searchButton" name="btnSearchLocation" id="btnSearchLocation" value="Search">
                </div>
            </div>  
	</div>
    
  <script>
  	$('#region').val('<?php echo $region; ?>');
	$('#country').val('<?php echo $country; ?>');
	$('#city').val('<?php echo $city; ?>');
	$('#status').val('<?php echo $status; ?>');
  </script>
</form>

	<?php if($canWrite == 1) { ?>
	<div class="centerButtonContainer">
		<input class="addButton" type="button" value="Add Location" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_location/' ?>';" />
	</div>
	<?php }	?>
	
	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php 	}	?>
	</div>
    
    <div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
            <td class="listHeaderCol" style="width:150px">Location</td>
            <td class="listHeaderCol" style="width:150px">Location Type</td>
            <td class="listHeaderCol" style="width:150px">Region</td>
            <td class="listHeaderCol" style="width:150px">Country</td>
            <td class="listHeaderCol" style="width:150px">Status</td>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['market_status'] != STATUS_DELETED))) { ?>
            <td class="listHeaderColLast">Action</td>
            <?php } ?>
        </tr>
        <?php
        for($ind = 0; $ind < count($arrRecords); $ind++) 
		{
			($arrRecords[$ind]['location_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
        ?>
        <tr class="<?php echo $classListingData; ?>">
            <td class="listContentCol"><?php echo $arrRecords[$ind]['location_name']; ?></td>
            <td class="listContentCol"><?php if($arrRecords[$ind]['location_type_id'] == 1) echo "Region"; else if($arrRecords[$ind]['location_type_id'] == 2) echo "Country"; else if($arrRecords[$ind]['location_type_id'] == 3) echo "City";  ?></td>
            <td class="listContentCol"><?php if($arrRecords[$ind]['region_name']) echo $arrRecords[$ind]['region_name']; else if($arrRecords[$ind]['country_region_name']) echo $arrRecords[$ind]['country_region_name']; else echo "-"; ?></td>
            <td class="listContentCol"><?php if($arrRecords[$ind]['country_name']) echo $arrRecords[$ind]['country_name']; else echo "-"; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['location_status'] == STATUS_ACTIVE) echo "Active"; else if ($arrRecords[$ind]['location_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; else if ($arrRecords[$ind]['location_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['market_status'] != STATUS_DELETED))) { ?>
			<td class="listContentColLast">
            	<div class="colButtonContainer">
                <?php if($canWrite == 1) { ?>
                    <input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_location/' . $arrRecords[$ind]['location_id']; ?>';" />
                <?php } ?>
                <?php if(($canDelete == 1) && ($arrRecords[$ind]['location_status'] != STATUS_DELETED)){ ?>
                    <input class="smallButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['location_id']; ?>');" />
                <?php } ?>
                </div>
            </td>
			<?php } ?>
        </tr>
        <?php
       }
       ?>
    </table>
    </div>
</div>