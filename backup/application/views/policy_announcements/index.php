<script>
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<div class="midSection">
    <div class="dashboardTitle">
        Policies & Announcements Section
    </div>
	
	<div class="notificationWideMain">
    	<div class="notificationWideBox">
            <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
				<tr>
					<td valign="top" width="48%">
                    	<table width="100%">
                        	<tr>
                            	<td colspan="2"><h1>Policies</h1></td>
                            </tr>
                            <?php
							$lastCompName = '-';
                            for($ind = 0; $ind < count($arrPolicies); $ind++) {
								if($arrPolicies[$ind]['company_name'] == '') {
									$arrPolicies[$ind]['company_name'] = 'General Policies';
								}
								if($arrPolicies[$ind]['company_name'] != $lastCompName) {
									$lastCompName = $arrPolicies[$ind]['company_name'];
							?>
                        	<tr>
                            	<td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                            	<td colspan="2">
                                	<h1 style="font-size:20px"><?php echo $arrPolicies[$ind]['company_name']; ?></h1>
                                </td>
                            </tr>
                        	<tr>
                            	<td colspan="2"><hr /></td>
                            </tr>
                            <?php
								}
								if($ind % 2 == 0) {
									$cssClass = ' class="listContentAlternate"';
								}
							?>
                        	<tr<?php echo $cssClass; ?>>
                            	<td class="listContentCol">
									<?php echo $arrPolicies[$ind]['ann_title']; ?>
                                    <?php if($arrPolicies[$ind]['ann_is_new']) { ?>
                                    &nbsp;&nbsp;<span class="fileIcon"><img src="<?php echo $this->imagePath . '/new_notification.gif'; ?>"></span>
                                    <?php } ?>
                                </td>
                                <td class="listContentCol"><a href="<?php echo base_url(); ?>js/ViewerJS/#<?php echo $arrPolicies[$ind]['ann_file']; ?>" target="_blank">View</a></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </td>
                    <td>&nbsp;</td>
					<td valign="top" width="48%">
                    	<table width="100%">
                        	<tr>
                            	<td colspan="2"><h1>Announcements</h1></td>
                            </tr>
                            <?php
							$lastCompName = '-';
                            for($ind = 0; $ind < count($arrAnnouncements); $ind++) {
								if($arrAnnouncements[$ind]['company_name'] == '') {
									$arrAnnouncements[$ind]['company_name'] = 'General Announcements';
								}
								if($arrAnnouncements[$ind]['company_name'] != $lastCompName) {
									$lastCompName = $arrAnnouncements[$ind]['company_name'];
							?>
                        	<tr>
                            	<td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                            	<td colspan="2">
                                	<h1 style="font-size:20px"><?php echo $arrAnnouncements[$ind]['company_name']; ?></h1>
                                </td>
                            </tr>
                        	<tr>
                            	<td colspan="2"><hr /></td>
                            </tr>
                            <?php
								}
								if($ind % 2 == 0) {
									$cssClass = ' class="listContentAlternate"';
								}
							?>
                        	<tr<?php echo $cssClass; ?>>
                            	<td class="listContentCol">
									<?php echo $arrAnnouncements[$ind]['ann_title']; ?>
                                    <?php if($arrAnnouncements[$ind]['ann_is_new']) { ?>
                                    &nbsp;&nbsp;<span class="fileIcon"><img src="<?php echo $this->imagePath . '/new_notification.gif'; ?>"></span>
                                    <?php } ?>
                                </td>
                                <td class="listContentCol"><a href="<?php echo base_url(); ?>js/ViewerJS/#<?php echo $arrAnnouncements[$ind]['ann_file']; ?>" target="_blank">View</a></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>