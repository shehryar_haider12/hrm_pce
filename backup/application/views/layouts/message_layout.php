<?php
$css_path = $this->config->item("css_path");
$style_css_path = $this->config->item("style_css_path");
$script_path = $this->config->item("script_path");

?>
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ValuStrat - HRMS</title>

    <link type="text/css" rel="stylesheet" href="<?php echo $style_css_path;?>">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path;?>reset.css">
	<script type="text/javascript" src="<?php echo $script_path; ?>/common.js"></script>
	<?php echo $font; ?>
  </head>
  <body>
	<div class="mainBody">
  		<div class="mainDiv">
			
			<!----- Header Start ----->
			<div class="headerMain">
				<?php echo $header; ?>
			</div>
			<!----- Header End ----->
    		
			<!----- Mid Section Start  ----->
			<div class="midMain">
    			<?php echo $content; ?>
			</div>
			<!----- Mid Section End ----->
			
			<!----- Footer Start ----->
			<div class="footerMain">
				<?php echo $footer; ?>
			</div>
			<!----- Footer End ----->
			
		</div>
	</div>
  </body>
</html>