<?php
$monthSelection = ($this->input->post('monthSelection')) ? ($this->input->post('monthSelection')) : '';
$year 	= ($this->input->post('year')) ? ($this->input->post('year')) : (int)date('Y');
$department 	= ($this->input->post('department')) ? ($this->input->post('department')) : '';

if(count($arrRecords) > 0)
{
	$arrResult = array();
	$arrayCount = 1;
	array_push($arrResult,$arrRecords);
	
	for($ind = 0; $ind < count($arrRecords); $ind++) 
	{
		if(count($arrResult[$ind]) > 0)
		{ 
?>
		<?php if(($this->input->post('monthSelection')) && ($this->input->post('year'))) { ?>     
            
            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
			<script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
            
            var data = google.visualization.arrayToDataTable([
			
				<?php				
				$result = '';
				for($znd = 0; $znd < count($empDetails); $znd++)
				{
					$totalRecords = count($empDetails);					
					$result .= $empDetails[$znd]['name']."";
					if(!($znd == ($totalRecords - 1)))
					{ $result .= "','"; } else  {$result .= ""; }
				}			
				?>
				['Month', '<?php echo $result; ?>', { role: 'style' } ],
				
				<?php					 
				$arrYear = array();
				for($month = 0; $month < 12; $month++)
				{
					for($jnd = 0; $jnd < count($arrResult[$ind]); $jnd++) {
					
						for($znd = 0; $znd < count($empDetails); $znd++) {
							
							if($arrResult[$ind][$jnd]['month'] == ($month + 1)) {
								if($empDetails[$znd]['id'] == $arrResult[$ind][$jnd]['emp_id']) {
									if($arrYear[$month][$empDetails[$znd]['id']] == '') {
									
										$arrYear[$month][$empDetails[$znd]['id']] = $arrResult[$ind][$jnd]['total_weighted_score'];
															
									}
								} else {
									if($arrYear[$month][$empDetails[$znd]['id']] == '') {
										$arrYear[$month][$empDetails[$znd]['id']] = 0;
									}
								}
							}
						}
					}
				}
				
				?>
				['<?php echo date("F", mktime(0, 0, 0, $monthSelection, 1)); ?>',
				<?php				
					for($znd = 0; $znd < count($empDetails); $znd++) {
						$totalRecords = count($empDetails);
						echo (float)$arrYear[$monthSelection-1][$empDetails[$znd]['id']];
						if(!($znd == ($totalRecords - 1))) echo ",";
					}
				?>,
				''            
				],
            	  
            ]);
            
            var view = new google.visualization.DataView(data);
			
			view.setColumns([
			0, 
			<?php for($znd = 0; $znd < count($empDetails); $znd++) { $totalRecords = count($empDetails); 
			if($arrYear[$monthSelection-1][$empDetails[$znd]['id']] > 0) { ?>
			{
				label: '<?php echo $empDetails[$znd]['name']; ?>',
				type: 'number',
				calc: function (dt, row) {
					var ann = dt.getValue(row, <?php echo $znd + 1; ?>);
					return ann;
				}
			}, {
				role: 'annotation',
				type: 'string',
				calc: function (dt, row) {
					var ann = dt.getValue(row, <?php echo $znd + 1; ?>);
					var ann = ann + "%";
					return ann;
				}
			}
			<?php
			if(!($znd == ($totalRecords - 1)))
					{ echo ","; }
			}
			} ?>			
			]);
            
            var options = {
                    title: "Year <?php echo $arrRecords[$ind]['year']; ?> Performance Report",
    				hAxis: {title: "Month(s)"},
                    legend: { position: 'right', maxLines: 800 },
                    <?php if(count($arrResult[$ind]) < 6) { ?> bar: { groupWidth: '50%' }, <?php } else { ?> bar: { groupWidth: '85%' }, <?php } ?>
					vAxis: {title: "Performance", viewWindow: {min: 0,max: 101}},
					
                  };
            
            
            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            
            chart.draw(view, options);
            
            }
            </script>			
<?php 
			}
		}
	}

}
?>

<div class="listPageMain">
  <form name="frmSearchTasks" id="frmSearchTasks" method="post" action="<?php echo $frmActionURL; ?>">
    <div class="searchBoxMain">
      <div class="searchHeader">DEPARTMENT REPORTS</div>
      <div class="searchcontentmain">
        <div class="searchCol">
        	<div class="labelContainer">Department:</div>
            <div class="textBoxContainer">
              <select id="department" name="department" class="dropDown">
                <option value="">Select Department</option>
                <?php
                  for($ind = 0; $ind < count($empDepartments); $ind++) {
                      $selected = '';
                      if($empDepartments[$ind]['job_category_id'] == $empDepartment) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $empDepartments[$ind]['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $empDepartments[$ind]['job_category_name']; ?></option>
                <?php
                  }
                ?>
              </select>
            </div>			
        </div>
        <div class="searchCol">
        	<div class="labelContainer">Month:</div>
              <div class="textBoxContainer">
                <select class="dropDown" id="monthSelection" name="monthSelection" style="width:85px; margin-left:5px">
                  <option value="">Month</option>
                  <?php
                            foreach($months as $key => $value) {
                        ?>
                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                  <?php
                            }
                        ?>
                </select>
              </div>
        	<div class="labelContainer">Year:</div>
          	<div class="textBoxContainer">
                <select id="year" name="year" class="dropDown" style="width:85px; margin-left:5px">
                  <option value="">Year</option>
                  <?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
                  <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                  <?php } ?>
                </select>
             </div>
        </div>
        
        <div class="formButtonContainerWide">
          <input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
          <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
        </div>
      </div>
      <script>
  	$('#department').val('<?php echo $department; ?>');
	$('#monthSelection').val('<?php echo $monthSelection; ?>');
  	$('#year').val('<?php echo $year; ?>');
  </script> 
    </div>
  </form>
</div>
<div class="listPageMain">
	<?php if(count($arrRecords) > 0) { ?>
		<div id="chart_div" style="float:left; width:100%; height:800px;"></div>
    <?php } else if($message) { ?>
		<div class="listContentAlternate bold" align="center">
        	<?php echo $message; ?>
        </div>
	<?php } else { ?>
    	<div class="listContentAlternate bold" align="center">
        	<?php echo "Select Criteria and Year field and then click Search"; ?>
        </div>
    <?php } ?>
</div>