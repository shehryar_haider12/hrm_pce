<script>
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<style>
.formLabelContainer { padding: 0 10px 0 0 !important; width:50px !important; }
</style>
<?php
$path 				= '/'.$this->currentController.'/'.$this->currentAction;
$currentMonth		= 6;
$currentYear		= date('Y');
$month 				= ($month) 		? $month 	: ($this->input->post('month') 	? $this->input->post('month') 	: (($this->input->post('selectedMonth')) 	? $this->input->post('selectedMonth') 	: 6));
$year 				= ($year) 		? $year		: ($this->input->post('year') 	? $this->input->post('year') 	: (($this->input->post('selectedYear')) 	? $this->input->post('selectedYear') 	: (int)date('Y')));
$empCode 			= ($empCode) 	? $empCode 	: ($this->input->post('empCode')? $this->input->post('empCode') : (($this->input->post('empID')) 			? $this->input->post('empID') 			: $this->employeeID));

$txt0				= json_decode($arrTasks[0]['assessment_kpi']);
$txt1				= json_decode($arrTasks[0]['assessment_accomplishments']);
$txt2				= json_decode($arrTasks[0]['assessment_challenges']);
$txt3				= json_decode($arrTasks[0]['assessment_strengths']);
$txt4				= json_decode($arrTasks[0]['assessment_improvement']);
$txt5				= json_decode($arrTasks[0]['assessment_skills']);

$supervisorComments = isset($_POST['supervisorComments']) 		? $_POST['supervisorComments'] 	: $arrTasks[0]['assessment_remarks'];

if(isset($_POST['status']))
{ $status = $_POST['status']; }
else if(($arrMonthTasksStatus['pe_status'] > 2) && ($arrMonthTasksStatus['pe_status'] < 4))
{ $status = $arrMonthTasksStatus['pe_status']; }
else if(($arrMonthTasksStatus['pe_status'] > 5) && ($arrMonthTasksStatus['pe_status'] < 7))
{ $status = $arrMonthTasksStatus['pe_status']; }
else { $status = ''; }
?>

<div class="listPageMain">
<?php if(is_array($arrUnReviewedRecords) && count($arrUnReviewedRecords) > 0) { ?>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<a href="#pendingReviews" class="fancybox normalLink" title="Pending Reviews">Click here</a><b> to view Employee(s) whom review is Pending</b>
    </div>
</div>

<table border="0" cellpadding="0" cellspacing="0" id="pendingReviews" style="display:none;" width="650px">
<form name="frmResponse" id="frmResponse" method="post" action="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>">
	<tr class="listHeader" >
        <td class="listHeaderCol" style="text-align:center" width="30%">Team Member Name</td>
        <td class="listHeaderCol" style="text-align:center" width="10%">Month</td>
        <td class="listHeaderCol" style="text-align:center" width="10%">Year</td>
        <td class="listHeaderColLast" style="text-align:center" width="20%">Action</td>
    </tr>
    <input type="hidden" name="empCodePending" id="empCodePending" />
    <input type="hidden" id="monthPending" name="monthPending" />
    <input type="hidden" id="yearPending" name="yearPending" />
    
	<?php for($ind = 0; $ind < count($arrUnReviewedRecords); $ind++) { ?>
    <tr class="listContent">
    	<td class="listContentCol" align="left"><?php echo $arrUnReviewedRecords[$ind][3]; ?></td>
        <td class="listContentCol" align="center"><?php echo date("F", mktime(0, 0, 0, $arrUnReviewedRecords[$ind][1], 1)); ?></td>
        <td class="listContentCol" align="center"><?php echo $arrUnReviewedRecords[$ind][2]; ?></td>
    	<td class="listContentCol" align="center">
            <input type="button" class="smallButton" name="btnViewPendingReviews" id="btnViewPendingReviews" value="Review" onclick="fillValues('<?php echo $arrUnReviewedRecords[$ind][0]; ?>','<?php echo $arrUnReviewedRecords[$ind][1]; ?>','<?php echo $arrUnReviewedRecords[$ind][2]; ?>')">
        </td>
	</tr>
    <?php } ?>
    
    <script>
		function fillValues(empCode,month,year)
		{
			$('#empCodePending').val(empCode);
			$('#monthPending').val(month);
			$('#yearPending').val(year);			
			$('#frmResponse').submit();
		}
	</script>
</form>
</table>
<?php } ?>

<form name="frmSearchTasks" id="frmSearchTasks" method="post" action="<?php echo $frmActionURL; ?>">
<div class="searchBoxMain">
    <div class="searchHeader">MY TEAM'S TASKS - <?php echo date('F', mktime(0, 0, 0, $month, 10)) . ' ' .$year; ?></div>
    <div class="searchcontentmain">
        <div class="searchCol">
        	<?php
			if(count($arrEmployees) > 0) {
			?>
			<div class="labelContainer">Employee:</div>
			<div class="textBoxContainer">
			  <select name="empCode" id="empCode" class="dropDown">
				  <option value="">Select Employee</option>
                  <?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmployee) {
					?>
						<optgroup label="<?php echo $key; ?>">
							<?php for($i = 0; $i < count($arrEmployee); $i++) { ?>					
								<option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_full_name']; ?></option>
							<?php } ?>
						</optgroup>
					<?php	}
					}
					?>
			  </select>
			</div>
			<?php
			}
			?>
        </div>
        <div class="searchCol">
        	<div class="labelContainer">Month:</div>
            <div class="textBoxContainer">
                <select class="dropDown" id="month" name="month" style="width:85px; margin-left:5px">
                    <option value="">Month</option>
                    <option value="6">June</option>
					<option value="12">December</option>
                </select>
            </div>
            <div class="labelContainer">Year:</div>
            <div class="textBoxContainer">
                <select id="year" name="year" class="dropDown" style="width:85px; margin-left:5px">
                    <option value="">Year</option>
                    <?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
                    <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="formButtonContainerWide">
            <input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
        </div>
    </div>
    <script>
	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#month').val('<?php echo $month; ?>');
  	$('#year').val('<?php echo $year; ?>');
  </script>
</div>
</form>
</div>

<?php if(count($arrTasks)) { ?>
<div class="listContentMain" style="height:auto">
<form name="frmAddTasks" id="frmAddTasks" method="post" action="<?php echo $frmActionURL; ?>">
	<input type="hidden" id="month" name="month" value="<?php echo $month; ?>" />
    <input type="hidden" id="year" name="year" value="<?php echo $year; ?>" />
    <input type="hidden" id="empCode" name="empCode" value="<?php echo $empCode; ?>" />
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px 170px;">
        
        <!-- POST RECORDS START -->
        <tr>
        	<td>
                <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
                    <tr class="listHeader">
                        <td class="listHeaderCol center" colspan="6">
                            SELF APPRAISAL FORM
                        </td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol" width="25%">Employee</td>
                        <td class="listContentCol" width="25%"><?php echo $arrEmp['emp_full_name']; ?></td>
                        <td class="listContentCol" width="25%">Self-Appraisal Date</td>
                        <td class="listContentCol" width="25%"><?php echo date(SHOW_DATE_TIME_FORMAT, strtotime($arrTasks[0]['assessment_remarks_date'])); ?></td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol">Job Title</td>
                        <td class="listContentCol"><?php echo $arrEmp['emp_designation']; ?></td>
                        <td class="listContentCol">Department</td>
                        <td class="listContentCol"><?php echo $arrEmp['job_category_name']; ?></td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol">Name of Supervisor</td>
                        <td class="listContentCol" colspan="3"><?php echo getSupervisorName($arrEmp['emp_id']); ?></td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol" colspan="4">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<table border="1" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr class="formHeaderRow">
                        <td>S. No.</td>
                        <td align="center" style="font-style:italic; font-size:12px">Job Specific KPIs<br />
                                    <span>Note: You are required to revert to the KPIs set<br />between you and your Manager at the start of the year</span>
                        </td>
                        <td align="center" style="font-style:italic; font-size:12px">Employee Comments about their performance against the KPIs</td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                <?php
                $totalRecords = count($txt0);
				if(!$totalRecords) {
					$totalRecords = 8;
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                	<tr>
                        <td width="5%"><?php echo (($ind + 2) / 2); ?>.</td>
                        <td width="47%"><?php echo nl2br($txt0[$ind]); ?></td>
                        <td width="48%"><?php echo nl2br($txt0[$ind + 1]); ?></td>
                    </tr>
                <?php
					$ind++;
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow">
                Significant Accomplishments
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                <?php
                $totalRecords = count($txt1);
				if(!$totalRecords) {
					$totalRecords = 4; 
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                    <tr>
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="95%"><?php echo nl2br($txt1[$ind]); ?></td>
                    </tr>
                <?php
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow">
                Areas of Strength
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                <?php
                $totalRecords = count($txt2);
				if(!$totalRecords) {
					$totalRecords = 4; 
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                    <tr>
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="95%"><?php echo nl2br($txt2[$ind]); ?></td>
                    </tr>
                <?php
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow">
                Areas of Improvement
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                <?php
                $totalRecords = count($txt3);
				if(!$totalRecords) {
					$totalRecords = 4; 
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                    <tr>
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="95%"><?php echo nl2br($txt3[$ind]); ?></td>
                    </tr>
                <?php
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow">
                Constraints/Challenges you face at work
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                <?php
                $totalRecords = count($txt4);
				if(!$totalRecords) {
					$totalRecords = 4; 
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                    <tr>
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="95%"><?php echo nl2br($txt4[$ind]); ?></td>
                    </tr>
                <?php
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow">
                Skills to Acquire to Improve your Performance
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                <?php
                $totalRecords = count($txt5);
				if(!$totalRecords) {
					$totalRecords = 4; 
				}
				

				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                    <tr>
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="95%"><?php echo nl2br($txt5[$ind]); ?></td>
                    </tr>
                <?php
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <!-- POST RECORDS END -->

	<?php if($arrTasks[0]['assessment_status_id'] == STATUS_SUBMIT_REVIEW_TO_HR) { ?>
        <tr>
            <td>
        <div class="intervieweeMain">
            <div class="interviewee">
                <?php echo nl2br("<b>Supervisor's Comments: </b><br />".$arrTasks[0]['assessment_remarks']); ?>
            </div>
            <div class="intervieweeIcon">
                <div class="intervieweeIconImage">
                <?php
                if(!file_exists($pictureFolder . $arrReviewerDetails['emp_photo_name']) || empty($arrReviewerDetails['emp_photo_name']))
                    {
                        if(empty($arrReviewerDetails['emp_gender'])) {
                            $arrReviewerDetails['emp_gender'] = 'male';
                        }
                        $arrReviewerDetails['emp_photo_name'] = 'no_image_' . strtolower($arrReviewerDetails['emp_gender']) . '.jpg';
                    }
                ?>
                <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrReviewerDetails['emp_photo_name']; ?>" alt="" width="60" />
                </div>
                <div class="intervieweeIconName">
                    <b><?php echo $arrReviewerDetails['emp_full_name']; ?></b>
                    <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrTasks[0]['assessment_remarks_date'])); ?>
                </div>
            </div>
        </div>
        </td>
        </tr>
        <tr class="listContentAlternate">
            <td align="center" class="listContentCol bold">
                Self-Appraisal has been submitted to HR after supervisor's review.
            </td>
        </tr>
    <?php }
    	else if($arrTasks[0]['assessment_status_id'] == STATUS_SUBMIT_TASKS_FOR_REVIEW) {
	?>
        
        <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
     	<tr class="headerRow">
            <td width="80%" align="right">
            	<select class="dropDown" id="selStatus" name="selStatus" style="display:none;">
                	<option value="">Select Status</option>
                    <?php for($ind = STATUS_SUBMIT_TASKS_FOR_REVIEW; $ind <= STATUS_SUBMIT_REVIEW_TO_HR; $ind++) { ?>
                        <option value="<?php echo $ind; ?>"><?php echo $performanceStatus[$ind]; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr class="headerRow">
			<td width="80%" align="right">
            	<textarea rows="4" cols="20" name="supervisorComments" placeholder="Provide Comments ..." class="textArea" style="width:442px;float:none;"><?php echo $supervisorComments; ?></textarea>
            </td>
        </tr>
        <tr class="headerRow">			
            <td  width="80%" align="right">
                <input type="submit" class="smallButton" name="btnAddReviews" id="btnAddReviews" value="Save" onclick="return confirmation('Save')">&nbsp;
                <input type="submit" class="smallButton" name="btnSaveReviews" id="btnSaveReviews" value="Submit" onclick="return confirmation('Submit')">
            </td>
        </tr>        
        <tr class="listContent">
        	<td colspan="2">&nbsp;</td>
        </tr>
     </table>
    <?php } ?>
    </table>
</form>
</div>
<?php } else { ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listContentAlternate">
		<td align="center" class="listContentCol">No Record Found</td>
	</tr>
</table>
<?php } ?>

<script>
function confirmation(val)
{
	if(val == 'Save')
	{
		$('#selStatus').val(<?php echo STATUS_SUBMIT_TASKS_FOR_REVIEW; ?>);
		alert("You are going to save your remarks on this self-appraisal.\nPlease note that you may add or modify the remarks until you submit it to HR.");
		return true;
	}
	else if(val == 'Submit')
	{
		$('#selStatus').val(<?php echo STATUS_SUBMIT_REVIEW_TO_HR; ?>);
		if (confirm("Are you sure you want to submit self-appraisal remarks to HR?"))
			return true;
	}
	return false;
}
</script>