<?php
$empID 		= isset($_POST['empID']) 		? $_POST['empID'] 		: $empID;
$selMonth 	= isset($_POST['selMonth']) 	? $_POST['selMonth'] 	: $selMonth;
$selYear 	= isset($_POST['selYear']) 		? $_POST['selYear'] 	: $selYear;
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "minDate", '<?php echo date('Y-m-d', strtotime('6 months ago')); ?>' );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});

function saveNotes(txtArea, empID, strDate) {
	var strNotes = $('#' + txtArea).val();
	$.ajax({
			url: base_url + "/ajax/saveAttNotes/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_notes=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert('Note Saved!');
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
}
</script>
<form name="frmAttDetail" id="frmAttDetail" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
	<?php
	if(count($arrEmployees) > 0) {
	?>
      <div class="searchCol">
        <div class="labelContainer">Employee:</div>
        <div class="textBoxContainer">
          <select name="empID" id="empID" class="dropDown">
              <option value="">Select Employee</option>
              <?php
				if (count($arrEmployees)) {
					foreach($arrEmployees as $key => $arrEmp) {
				?>
					<optgroup label="<?php echo $key; ?>">
						<?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
							<option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
						<?php } ?>
					</optgroup>
				<?php	}
				}
				?>
          </select>
        </div>
	  </div>
	<?php
	}
	?>
    <div class="searchCol">
        <div class="labelContainer">Month:</div>
        <div class="textBoxContainer">
        	<select id="selMonth" name="selMonth" class="dropDown">
            	<option value="">Month</option>
            	<option value="01">Jan</option>
            	<option value="02">Feb</option>
            	<option value="03">Mar</option>
            	<option value="04">Apr</option>
            	<option value="05">May</option>
            	<option value="06">Jun</option>
            	<option value="07">Jul</option>
            	<option value="08">Aug</option>
            	<option value="09">Sep</option>
            	<option value="10">Oct</option>
            	<option value="11">Nov</option>
            	<option value="12">Dec</option>
          	</select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Year:</div>
        <div class="textBoxContainer">
        	<select id="selYear" name="selYear" class="dropDown">
            	<option value="">Year</option>
            	<?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
            	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
				<?php } ?>
          	</select>
        </div>
      </div>
	  <!--<div class="searchCol">
        <div class="labelContainer">Date From:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" name="dateFrom" id="dateFrom" />
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Date To:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" name="dateTo" id="dateTo" />
        </div>
      </div>-->
      <div class="buttonContainer">
      	<input type="hidden" name="txtExport" id="txtExport" value="0" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val('0')">
        <input class="searchButton" name="btnExport" id="btnExport" type="submit" value="Export PDF" onclick="$('#txtExport').val('1')">
      </div>
    </div>
  </div>
  <script>
  	$('#empID').val('<?php echo $empID; ?>');
	$('#selMonth').val('<?php echo $selMonth; ?>');
	$('#selYear').val('<?php echo $selYear; ?>');
  </script>
</form>
<div class="centerElementsContainer">
    <div class="recordCountContainer">
            <b>Legends:</b>&nbsp;&nbsp;&nbsp;<span style="background-color:#8FB05C;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Off Day</span>
    </div>
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>
<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Date</td>
    <td class="listHeaderCol">Day</td>
    <td class="listHeaderCol">In</td>
    <td class="listHeaderCol">Out</td>
    <td class="listHeaderCol">Working Hrs</td>
    <td class="listHeaderCol">Leave Applied</td>
    <td class="listHeaderCol">Notes</td>
    <?php if(false) { ?>
    <td class="listHeaderCol">Clocking</td>
    <?php } ?>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrAllDates); $ind++) {
		$showClocking = true;
		$dateIndex = array_search($arrAllDates[$ind], array_column($arrRecords, 'DATE'));
		if($dateIndex !== false) {
			$jnd = $dateIndex;
		} else {
			$jnd = -1;
			$showClocking = false;
		}
		
		$rowBGColor = '';
		$publicHoliday = '';
		$holidayIndex = array_search($arrAllDates[$ind], array_column($arrHolidays, 'holiday_date'));
		
		if(date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_1'] || date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_2'] || $holidayIndex !== false) {
			$rowBGColor = ' bgcolor="#8FB05C" style="color:#FFF"';
			$showClocking = false;
			if($holidayIndex !== false) {
				$publicHoliday = $arrHolidays[$holidayIndex]['holiday_name'];
				$showClocking = false;
			}
		}
		
		// TO HIDE TODAY'S OUT TIME TO AVOID CONFUSION
		
		if($arrAllDates[$ind] == date('Y-m-d')) {
			$arrRecords[$jnd]['OUT'] = '';
		}
	?>
  <tr<?php echo $rowBGColor; ?> height="30px">
    <td class="listContentCol"><?php echo readableDate($arrAllDates[$ind], 'jS M, Y'); ?></td>
    <td class="listContentCol"><?php echo date('l', strtotime($arrAllDates[$ind])); ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$jnd]['IN'] != '') ? date('g:i A', strtotime($arrRecords[$jnd]['IN'])) : '-'; ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$jnd]['OUT'] != '') ? date('g:i A', strtotime($arrRecords[$jnd]['OUT'])) : '-'; ?></td>
    <td class="listContentCol">
	<?php
	if($publicHoliday != '') {
		echo $publicHoliday;
	} else {
		if($arrRecords[$jnd]['IN'] != '' && $arrRecords[$jnd]['OUT'] != '') { 
			$strTime1 = new DateTime($arrRecords[$jnd]['IN']);
			$strTime2 = new DateTime($arrRecords[$jnd]['OUT']);
			$intInterval = $strTime1->diff($strTime2);
			if($intInterval->format('%h') > 0 || $intInterval->format('%i') > 0) {
				echo $intInterval->format('%h')." hr ".$intInterval->format('%i')." min";
			}
		} else {
			echo '-';
		}
	}
	?>
    </td>
    <td class="listContentCol" width="100px">
    <?php
	$strLeave = getIfLeaveApplied($empID, $arrAllDates[$ind]);	
    echo ($strLeave != '') ? $strLeave : '-';
	?>
    </td>
    <td class="listContentCol" width="300px">
    <?php
	$strNotes = getAttendanceNotes($empID, $arrAllDates[$ind]);
	
    if($canWrite == YES) {
	?>
    <input type="text" name="txtNote<?php echo $arrAllDates[$ind]; ?>" id="txtNote<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $strNotes; ?>">&nbsp;
    <input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveNotes('txtNote<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>')">
    <?php
	} else {
		echo ($strNotes != '') ? $strNotes : '-';
	}
	?>
    </td>
    <?php if(false) { ?>
    <td class="listContentCol" align="center"><?php if($showClocking) { ?><img src="<?php echo $this->imagePath; ?>/clocking-icon.png" alt="Clocking" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/clocking_detail/' . $empID . '/' . $arrAllDates[$ind]; ?>', 650, 500)" style="cursor:pointer" /><?php } ?></td>
    <?php } ?>
  </tr>
  <?php
	}
  ?>
  <?php
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="7" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
  	?>
</table>
</div>