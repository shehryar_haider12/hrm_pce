<?php
$eduLevel 		= (isset($_POST['eduLevel'])) 			? $_POST['eduLevel'] 			: $record['edu_level_id'];
$eduInstitute 	= (isset($_POST['eduInstitute'])) 		? $_POST['eduInstitute'] 		: $record['edu_institute'];
$eduMajor 		= (isset($_POST['eduMajors'])) 			? $_POST['eduMajors'] 			: $record['edu_major_id'];
$eduGPA	 		= (isset($_POST['eduGPA'])) 			? $_POST['eduGPA'] 				: $record['edu_gpa'];
$eduEnded 		= (isset($_POST['eduEnded'])) 			? $_POST['eduEnded'] 			: $record['edu_ended'];
#$eduStatus		= (isset($_POST['eduStatus'])) 			? $_POST['eduStatus'] 			: $record['edu_status'];
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker();
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#eduFrom" ).datepicker( "setDate", "<?php echo $eduFrom; ?>" );
	$( "#eduTo" ).datepicker( "setDate", "<?php echo $eduTo; ?>" );
});
</script>
<form name="frmEduHistory" id="frmEduHistory" method="post">
<div class="listPageMain">
  <div class="formMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  	<tr>
        <td class="formHeaderRow" colspan="2">Add/Edit Eductaional History</td>
    </tr>
    <tr>
      <td class="formLabelContainer">Candidate Name:</td>
      <td class="formTextBoxContainer"><div><?php echo $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name']; ?></div></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Qualification Level:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          <select id="eduLevel" name="eduLevel" class="dropDown">
          	  <option value="">Select Degree</option>
              <?php
                  for($ind = 0; $ind < count($eduLevels); $ind++) {
					  $selected = '';
					  if($eduLevels[$ind]['edu_level_id'] == $eduLevel) {
						  $selected = 'selected="selected"';
					  }
              ?>
                  <option value="<?php echo $eduLevels[$ind]['edu_level_id']; ?>" <?php echo $selected; ?>><?php echo $eduLevels[$ind]['edu_level_name']; ?></option>
              <?php
                  }
              ?>
          </select>      
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Educational Institute:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="eduInstitute" maxlength="100" id="eduInstitute" class="textBox" value="<?php echo $eduInstitute; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Majors<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">      
          <select id="eduMajors" name="eduMajors" class="dropDown">
          	  <option value="">Select Major</option>
              <?php
                  for($ind = 0; $ind < count($eduMajors); $ind++) {
					  $selected = '';
					  if($eduMajors[$ind]['edu_major_id'] == $eduMajor) {
						  $selected = 'selected="selected"';
					  }
              ?>
                  <option value="<?php echo $eduMajors[$ind]['edu_major_id']; ?>" <?php echo $selected; ?>><?php echo $eduMajors[$ind]['edu_major_name']; ?></option>
              <?php
                  }
              ?>
          </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Passing Year:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="eduEnded" id="eduEnded" class="dropDown" onchange="chkEduEnded('', this.value)">
            <option value="">Select Year</option>
            <option value="In Progress">In Progress</option>
            <?php for($jnd = date('Y'); $jnd > 1969; $jnd--) { ?>
            <option value="<?php echo $jnd; ?>"><?php echo $jnd; ?></option>
            <?php } ?>
        </select>
        <script>$('#eduEnded').val('<?php echo $eduEnded; ?>')</script>
    </tr>
    <tr>
      <td class="formLabelContainer">GPA/Grade:</td>
      <td class="formTextBoxContainer"><input type="text" name="eduGPA" maxlength="12" id="eduGPA" class="textBox" value="<?php echo $eduGPA; ?>" <?php echo ($eduEnded == 'In Progress') ? 'disabled="disabled"' : ''; ?>></td>
    </tr>
    <!--<tr>
      <td class="formLabelContainer">Status</td>
      <td class="formTextBoxContainer">
      	<select name="eduStatus" id="eduStatus" class="dropDown">
            <option value="">Select Status</option>
            <option value="Passed">Passed</option>
            <option value="In Progress">In Progress</option>
        </select>
        <script>$('#eduStatus').val('<?php echo $eduStatus; ?>');</script>
    </tr>-->
    <tr class="formAlternateRow">
      <td class="formLabelContainer"><input type="hidden" name="eduCandidateID" id="eduCandidateID" value="<?php echo $eduCandidateID; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
      </td>
    </tr>
    <tr>
    	<td colspan="2" height="25px">&nbsp;</td>
    </tr>
  </table>
  </div>
  
  <div class="internalMessageBox"> You can enter multiple records in your educational list.</div>
  
</div>
</form>

<br  />
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listHeader">
    	<td class="listHeaderCol">Qualification</th>
    	<td class="listHeaderCol">Institute</th>
    	<td class="listHeaderCol">Majors</th>
    	<td class="listHeaderCol">Passing Year</th>
    	<td class="listHeaderCol">GPA/Grade</th>
    	<!--<td class="listHeaderCol">Status</th>-->
    	<td class="listHeaderColLast">Action</th>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_level_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_institute']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_major_name']; ?></td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['edu_ended'])) ? $arrRecords[$ind]['edu_ended'] : ' - '; ?></td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['edu_gpa'])) ? $arrRecords[$ind]['edu_gpa'] : ' - '; ?></td>
    	<!--<td class="listContentCol"><?php echo $arrRecords[$ind]['edu_status']; ?></td>-->
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == 1) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/' . $this->currentAction . '/' . $arrRecords[$ind]['edu_id']; ?>';" />
            <?php } if($canDelete == 1) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrCandidate['candidate_id']; ?>', '<?php echo $arrRecords[$ind]['edu_id']; ?>');" />
            <?php } ?>
            </div>
		</td>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="6" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<?php //echo buildGrid($arrRecords, array('edu_id', 'edu_candidate_id'), $currentController . '/education_history/', array('edu_candidate_id', 'edu_id')); ?>
<br />
<?php if($canWrite == 0) { ?>
<script>$("#frmEduHistory :input").attr("disabled", true);</script>
<?php } ?>