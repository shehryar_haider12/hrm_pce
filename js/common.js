function deleteRecord(path, record_id) {
	if (confirm("Delete Selected Record?")) {
		$.ajax({
			url: base_url + path,
			data: "record_id=" + record_id,
			dataType: "text",
			type: "POST",
			beforeSend: function () {
				$("#snake").show()
			},
			success: function (msg) {
				$("#snake").hide();
				if(msg == 1) {
					alert("Record Deleted Successfully");
					window.location.href = window.location.href
				} else if(msg == 2) {
					alert("Can not be deleted as it is used for an Active Record");
				}
				else {
					alert("Error in Deleting Record");
				}
			}
		})
	}
	return false
}

function populateModuleParticularScreen(role_id,module_id,sub_module_id)
{	
	if((role_id)&&(module_id > 0))
	{
		$.ajax({
			url: base_url + "/ajax/populateModuleParticularScreen/",
			data: "role_id=" + role_id + "&module_id=" + module_id + "&sub_module_id=" + sub_module_id,
			dataType: "text",
			type: "POST",
			beforeSend: function () {
				//$("#snake").show()
			},
			success: function (msg) {
			   // $("#snake").hide();
				$("#screens").show();
				$("#screens").html(msg)
			}
		})
	}
}

function populateModuleScreens(role_id,module_id)
{	
	if(role_id >0)
	{
		if(module_id > 0)
		{
			$.ajax({
				url: base_url + "/ajax/populateModuleScreens/",
				data: "role_id=" + role_id + "&module_id=" + module_id,
				dataType: "text",
				type: "POST",
				beforeSend: function () {
					//$("#snake").show()
				},
				success: function (msg) {
				   // $("#snake").hide();
				   if (msg.toLowerCase().indexOf("privileges") >= 0)
				   { $("#savePermission").hide(); } else {
					   $("#savePermission").show();
				   }
					$("#screens").show();
					$("#screens").html(msg);
					
				}
			})
		}
	} 
	else { alert('Select User Role'); $('#module').val(''); }
		
}

function populateRoleModuleScreens(role_id,module_id,role_name,screen_id)
{
	if(role_id >0)
	{
		$.ajax({
			url: base_url + "/ajax/populateRoleModuleScreens/",
			data: "role_id=" + role_id + "&module_id=" + module_id + "&role_name=" + role_name + "&screen_id=" + screen_id,
			dataType: "text",
			type: "POST",
			beforeSend: function () {
				//$("#snake").show()
			},
			success: function (msg) {
			   // $("#snake").hide();
			   if (screen_id == 1 & msg.toLowerCase().indexOf("no modules") >= 0) {
			   		$("#copyPermission").hide(); 
			   } else if(screen_id == 1) {
				   $("#copyPermission").show(); 
			   }
			   if(screen_id == 1){
				$("#screens").show();
				$("#screens").html(msg);				
			   } else if(screen_id == 2){
				$("#screensTo").show();
				$("#copyTo").show();
				$("#screensTo").html(msg);
			   }
			}
		})
	} 
	else { 
		if(screen_id == 1) { alert('Select User Role'); } 
	}
		
}

function checkRecord(elmID, strCol, strVal, strID) {	
	if(strVal != '') {
		$.ajax({
			url: base_url + "/ajax/checkCandidate/",
			data: "strCol=" + strCol + "&strVal=" + strVal + "&strID=" + strID,
			type: "POST",
			success: function (msg) {
				$("#spanMsg").remove();
				$("#" + elmID).append(msg)
			}
		})
	}
}
var date = moment($('input[name="empDOB"]').val(),'YYYY/MM/DD').format('YYYY-MM-DD');
$("input").on("change", function() {
	this.setAttribute(
		"data-date",
		moment(this.value, "YYYY-MM-DD")
		.format( this.getAttribute("data-date-format") )
	)
}).trigger("change")
function checkRecordEmp(elmID, strCol, strVal, strID) {
	if(strVal != '') {
		$.ajax({
			url: base_url + "/ajax/checkEmployee/",
			data: "strCol=" + strCol + "&strVal=" + strVal + "&strID=" + strID,
			type: "POST",
			success: function (msg) {
				$("#spanMsg").remove();
				$("#" + elmID).append(msg)
			}
		})
	}
}
function getEmpDetails(empID) {
	if(empID != '') {
		$.ajax({
			url: base_url + "/ajax/getEmployee/",
			data: "empID=" + empID,
			type: "POST",
			success: function (msg) {
				var objJSON = jQuery.parseJSON(msg);
				$('#empDoJ').html($.datepicker.formatDate('dd M yy', new Date(objJSON[0].emp_joining_date)));
				$('#empJT').html(objJSON[0].emp_designation);
				$('#empEoP').html($.datepicker.formatDate('dd M yy', new Date(objJSON[0].emp_probation_end_date)));
			}
		})
	}
}

function getEmpSupervisor(empID) {
	if(empID != '') {
		$.ajax({
			url: base_url + "/ajax/getEmployeeSupervisors/",
			data: "empID=" + empID,
			type: "POST",
			success: function (msg) {
				$('#empNoS').html(msg);
			}
		})
	}
}


function addInterviewer(elmID) {
	var totalInt = $('#totalInterviewers').val();
	var strHTML = '<div id="div'+parseInt(totalInt)+'"><select name="interviewerName[]" class="dropDown" id="interviewerName'+(parseInt(totalInt) + 1)+'">' + $('#' + elmID + 'Hidden').html() + '</select>';
	$('#' + elmID).append(strHTML + '<span class="spanAnchor"><a href="#" onclick="removeInterviewer(\'interviewerName'+(parseInt(totalInt) + 1)+'\'); $(\'#div'+parseInt(totalInt)+'\').remove(); return false;">Remove</a></span></div>');
	$('#totalInterviewers').val(parseInt(totalInt) + 1);	
}

function removeInterviewer(elmID) {
	var totalInt = $('#totalInterviewers').val();
	totalInt = parseInt(totalInt) - 1;
	$('#' + elmID).remove();
	$('#totalInterviewers').val(parseInt(totalInt));	
}

function addEducation(elmID) {
	var totalInt = $('#totalEducations').val();
	var strHTML = '<tr id="trEdu'+(parseInt(totalInt) + 1)+'">' + $('#trEdu').html().replace(/\[/g, '[]');
	$('#' + elmID).append(strHTML + '<td class="subContentColLast"><span><a href="#" onclick="removeEducation(\'trEdu'+(parseInt(totalInt) + 1)+'\'); return false;">Remove</a><span></td></tr>');
	$('#totalEducations').val(parseInt(totalInt) + 1);	
}

function removeEducation(elmID) {
	var totalInt = $('#totalEducations').val();
	totalInt = parseInt(totalInt) - 1;
	$('#' + elmID).remove();
	$('#totalEducations').val(parseInt(totalInt));	
}

function addWorking(elmID) {
	var totalInt = $('#totalWorkings').val();
	var strHTML = '<tr id="trWork'+(parseInt(totalInt) + 1)+'">' + $('#trWork').html().replace(/\[/g, '[]');
	$('#' + elmID).append(strHTML + '<td class="subContentColLast"><span><a href="#" onclick="removeWorking(\'trWork'+(parseInt(totalInt) + 1)+'\'); return false;">Remove</a><span></td></tr>');
	$('#totalWorkings').val(parseInt(totalInt) + 1);	
}

function removeWorking(elmID) {
	var totalInt = $('#totalWorkings').val();
	totalInt = parseInt(totalInt) - 1;
	$('#' + elmID).remove();
	$('#totalWorkings').val(parseInt(totalInt));	
}

function setSort(colName, frmName) {
	var currColName = $('#sort_field').val();
	var sortOrder = $('#sort_order').val();
	
	if(colName == currColName) {
		if(sortOrder == 'DESC') {
			sortOrder = 'ASC';
		} else {
			sortOrder = 'DESC';
		}
	} else {
		sortOrder = 'ASC';
	}
	
	$('#sort_field').val(colName);
	$('#sort_order').val(sortOrder);
	$('#' + frmName).submit();
}

function populateLocationWithCity(elmID, locID, locType, citySelect, areaSelect) {	
	
	var strPath = window.location.pathname;
	var regMatches = strPath.match(/\/career\/(.*)$/);
	if (regMatches) {
    	strPath = 'career';
	} else {
   		strPath = 'ajax';
	}
	
	if(locID) {
		$.ajax({
			url: base_url + "/" + strPath + "/populateLocations/" + elmID + "/" + locID + "/" + locType,
			dataType: "text",
			type: "GET",
			success: function (msg) {
				if(locType == 2) {
					$('#Country').remove();
				}
				$('#City').remove();
				$('#Area').remove();
				$('#' + elmID).append(msg);
				if(parseInt(citySelect) > 0) {
					$('#City').val(citySelect);
				}
				if(typeof areaSelect !== "undefined") {
					
					$.ajax({
						url: base_url + "/" + strPath + "/populateAreas/" + elmID + "/" + citySelect + "/4",
						dataType: "text",
						type: "GET",
						success: function (msg) {
							$('#' + elmID).append(msg);
							if(parseInt(areaSelect) > 0) {
								$('#Area').val(areaSelect);
							}
						}
					})
				}
			}
		})
	} else {
		$('#City').remove();
		$('#Area').remove();
	}
}

function populateLocation(elmID, locID, locType) {
	
	var strPath = window.location.pathname;
	var regMatches = strPath.match(/\/career\/(.*)$/);
	if (regMatches) {
    	strPath = 'career';
	} else {
   		strPath = 'ajax';
	}
	
	if(locID) {
		$.ajax({
			url: base_url + "/" + strPath + "/populateLocations/" + elmID + "/" + locID + "/" + locType,
			dataType: "text",
			type: "GET",
			success: function (msg) {
				if(locType == 2) {
					$('#Country').remove();
				}
				$('#City').remove();
				$('#Area').remove();
				$('#' + elmID).append(msg);
			}
		})
	} else {
		$('#City').remove();
		$('#Area').remove();
	}
}

function populateAreas(elmID, locID, locType) {
	
	var strPath = window.location.pathname;
	var regMatches = strPath.match(/\/career\/(.*)$/);
	if (regMatches) {
    	strPath = 'career';
	} else {
   		strPath = 'ajax';
	}
	
	if(locID) {
		$.ajax({
			url: base_url + "/" + strPath + "/populateAreas/" + elmID + "/" + locID + "/" + locType,
			dataType: "text",
			type: "GET",
			success: function (msg) {
				$('#Area').remove();
				$('#' + elmID).append(msg);
			}
		})
	} else {
		$('#Area').remove();
	}
}

function populateLocationsSystem(elmID, locID, locType, onChange) {
	$.ajax({
		url: base_url + "/ajax/populateLocationsSystem/" + elmID + "/" + locID + "/" + locType + "/" + onChange,
		dataType: "text",
		type: "GET",
		success: function (msg) {
			if(locType == 1) {
				$('#Region').remove();
			}
			if(locType == 2) {
				$('#Country').remove();
			}
			if(locType == 3) {
				$('#City').remove();
			}
			$('#' + elmID).append(msg);
		}
	})
}

function populateRegion(field,locTypeID,onChange,locType,selectedID)
{
	$.ajax({
		url: base_url + "/ajax/populateRegion/" + field + "/" + locTypeID + "/" + onChange + "/" + selectedID,
		dataType: "text",
		type: "GET",
		success: function (msg) {
			$('#Region').remove();
			if(locType == 2) {
				if(isNaN($('#Country').val())) {} else {
				$('#Country').remove();
				
				$('#tdCountry').html('<input type="text" name="Country" id="Country" class="textBox" />');
				}
			}
			$('#' + field).html(msg);
		}
	})
	return true;
}

function populateCountry(field,region_id,selectedID)
{
	$.ajax({
		url: base_url + "/ajax/populateCountry/" + field + "/" + region_id + "/" + selectedID,
		dataType: "text",
		type: "GET",
		success: function (msg) {
			$('#Country').remove();
			$('#trCountry').show();
			$('#trCity').show();
			$('#' + field).html(msg);
		}
	})
}

function chkEduEnded(strID, strVal) {
	if(strVal == 'In Progress') {
		$('#eduGPA' + strID).val('').attr("disabled", true);
	} else {
		$('#eduGPA' + strID).val('').attr("disabled", false);
	}
}

function callCandidate(candID, numCall) {
	$('#msgContent').show();
	$('#btnSubmit').css('background-color', '#B8BEC0');
	$('#btnSubmit').val('Processing ....');
	$('#btnSubmit').prop('disabled',true);
		
	$.ajax({
		url: base_url + "/ajax/callCandidate/" + candID + "/" + numCall,
		dataType: "text",
		type: "GET",
		success: function (msg) {
			if(msg > 0) {
				$('#callID').val(msg);	
				$('#btnSubmit').css('background-color', '');
				$('#btnSubmit').val('Submit');
				$('#btnSubmit').prop('disabled', false);	
			} else {
				$('#msgContent').html('<p><b style="color:red">Error !!</b><br />Close this window and try again ...</p>');
			}
		}
	})
}

function callEmployee(empID, numCall) {
	$('#msgContent').show();
	$('#btnSubmit').css('background-color', '#B8BEC0');
	$('#btnSubmit').val('Processing ....');
	$('#btnSubmit').prop('disabled',true);
		
	$.ajax({
		url: base_url + "/ajax/callEmployee/" + empID + "/" + numCall,
		dataType: "text",
		type: "GET",
		success: function (msg) {
			if(msg > 0) {
				$('#callID').val(msg);	
				$('#btnSubmit').css('background-color', '');
				$('#btnSubmit').val('Submit');
				$('#btnSubmit').prop('disabled', false);	
			} else {
				$('#msgContent').html('<p><b style="color:red">Error !!</b><br />Close this window and try again ...</p>');
			}
		}
	})
}

function showPopup(strURL, pWidth, pHeight, pParams) {
	popupWindow = window.open(strURL, 'popUpWindow', 'height=' + pHeight + ',width=' + pWidth + ',left=100,top=100,screenX=0,screenY=100,resizable=0,scrollbar=1,toolbar=0,menubar=0,location=0,directories=0,status=1' + pParams);
}

$( document ).ready(function() {
	$('form').submit(function() {
		
		var btnClicked = $("input[type=submit][clicked=true]");		
		var btnVal = btnClicked.val();
		
	  	btnClicked.css('background-color', '#B8BEC0');
	  	btnClicked.val('Processing ....');
	  	btnClicked.prop('disabled',true);
		
		setTimeout(function(){
	  		btnClicked.css('background-color', '');
	  		btnClicked.val(btnVal);
			btnClicked.prop('disabled', false);
		}, 5000);
		
	});
	
	$("form input[type=submit]").click(function() {
		$("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
		$(this).attr("clicked", "true");
	});

});


function addPerson(elmID) {
	var totalInt = $('#totalPersons').val();
	var strHTML = '<div id="div'+parseInt(totalInt)+'"><select name="concernedPersons[]" class="dropDown" id="concernedPersons'+(parseInt(totalInt) + 1)+'">' + $('#' + elmID + 'Hidden').html() + '</select>';
	$('#' + elmID).append(strHTML + '<span class="spanAnchor"><a href="#" onclick="removePerson(\'concernedPersons'+(parseInt(totalInt) + 1)+'\'); $(\'#div'+parseInt(totalInt)+'\').remove(); return false;">Remove</a></span></div>');
	$('#totalPersons').val(parseInt(totalInt) + 1);	
}

function removePerson(elmID) {
	var totalInt = $('#totalPersons').val();
	totalInt = parseInt(totalInt) - 1;
	$('#' + elmID).remove();
	$('#totalPersons').val(parseInt(totalInt));	
}