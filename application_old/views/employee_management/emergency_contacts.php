<?php
$emergencyContactName 			= (isset($_POST['emergencyContactName'])) 			? $_POST['emergencyContactName'] 			: $record['emc_name'];
$emergencyContactRelationship 	= (isset($_POST['emergencyContactRelationship'])) 	? $_POST['emergencyContactRelationship'] 	: $record['emc_relationship'];
$emergencyContactNumber 		= (isset($_POST['emergencyContactNumber'])) 		? $_POST['emergencyContactNumber'] 			: $record['emc_contact_number'];
?>

<?php if($canWrite == YES) { ?>
<form name="frmEmergencyContacts" id="frmEmergencyContacts" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Emergency Contacts</td>
    </tr>
    <tr>
      <td class="formLabelContainer" width="20%">Contact Name:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer" align="left"><input type="text" name="emergencyContactName" maxlength="100" id="emergencyContactName" class="textBox" value="<?php echo $emergencyContactName; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Contact Relationship:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
        <select id="emergencyContactRelationship" name="emergencyContactRelationship" class="dropDown">
            <option value="">Select Relationship</option>
            <?php
            if (count($arrRelationships)) {
                foreach($arrRelationships as $arrRelationship) {
            ?>
                <option value="<?php echo $arrRelationship; ?>"><?php echo $arrRelationship; ?></option>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Contact Number:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="emergencyContactNumber" id="emergencyContactNumber" class="textBox" value="<?php echo $emergencyContactNumber; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"><input type="hidden" name="employeeID" id="employeeID" value="<?php echo $arrEmployee['emp_id']; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } ?>

<script>
	$('#emergencyContactRelationship').val('<?php echo $emergencyContactRelationship; ?>');
</script>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Contact Name</td>
    	<td class="listHeaderCol">Contact Relationship</td>
    	<td class="listHeaderCol">Contact Number</td>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</td>
		<?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['emc_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['emc_relationship']; ?></td>
        <td class="listContentCol"><?php echo $arrRecords[$ind]['emc_contact_number']; ?></td>
        <?php if(($canWrite == YES) || ($canDelete == YES)) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == YES) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $arrEmployee['emp_id'] . '/' . $arrRecords[$ind]['emc_id']; ?>';" />
            <?php } if($canDelete == YES) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['emc_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="5" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<?php if($canWrite == NO) { ?>
<script>$("#frmEmergencyContacts :input").attr("disabled", true);</script>
<?php } ?>