<?php
$docType 		= $_POST['docType'];
$docTitle 		= $_POST['docTitle'];
?>
<?php if($canWrite == YES) { ?>
<form name="frmAddDocument" id="frmAddDocument" method="post" enctype="multipart/form-data">
  <div class="employeeFormMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  	<tr>
        <td class="formHeaderRow" colspan="2">Add Document</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Document Type:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          <select id="docType" name="docType" class="dropDown">
              <option value="">Select Document Type</option>
              <?php
              if (count($arrDocTypes)) {
                  foreach($arrDocTypes as $arrDocType) {
                      $selected = '';
                      if($docType == $arrDocType['doc_type_id']) {
                          $selected = 'selected="selected"';
                      }
              ?>
                  <option value="<?php echo $arrDocType['doc_type_id']; ?>" <?php echo $selected; ?>><?php echo $arrDocType['doc_type']; ?></option>
              <?php
                  }
              }
              ?>
          </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Title:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          <input type="text" name="docTitle" maxlength="100" id="docTitle" class="textBox" value="<?php echo $docTitle; ?>">
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Document:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	  <input type="file" name="docFile" id="docFile" class="textBox">
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
      </td>
    </tr>
    <tr>
    	<td colspan="2" height="25px">&nbsp;</td>
    </tr>
  </table>
  </div>
</form>
<script>
$("#docType").val('<?php echo $docType; ?>');
</script>
<br  />
<?php } ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol" width="50%">Title</th>
    	<td class="listHeaderCol" width="20%">Document Type</th>
    	<td class="listHeaderCol" width="20%">Document</th>
        <?php if($canDelete == YES) { ?>
    	<td class="listHeaderColLast">Action</th>
        <?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['doc_title']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['doc_type']; ?></td>
    	<td class="listContentCol"><?php echo ($arrRecords[$ind]['doc_file'] != '') ? '<a href="' . $this->baseURL . '/' . $empDocFolderShow . $arrRecords[$ind]['doc_file'] . '" style="color:#f00" target="_blank">View Document</a>' : ' - '; ?></td>
        <?php if($canDelete == YES) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrRecords[$ind]['emp_id']; ?>', '<?php echo $arrRecords[$ind]['doc_id']; ?>');" />
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="4" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>