<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "setDate", "<?php echo $selectedDate; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", "0" );
});
</script>
<form name="frmAttReport" id="frmAttReport" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Attendance Report - Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Show Me Records Of:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="selectedDate" name="selectedDate" />
        </div>
        <div class="labelContainer">Employees Who:</div>
        <div class="textBoxContainer">
        	<select name="showCriteria" id="showCriteria" class="dropDown">
            	<option value="">Select Criteria</option>
            	<option value="4">Are Present</option>
            	<option value="1">Are On Time</option>
            	<option value="2">Came Late Or Halfday</option>
            	<option value="3">Are Absent</option>
            </select>
        </div>
      </div>
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController; ?>';">
      </div>
    </div>
  </div>
  <script>
  	$('#showCriteria').val('<?php echo $showCriteria; ?>');
  	$('#empDepartment').val('<?php echo $empDepartment; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . (int)$totalRecordsCount; ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol"></td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_first_name', 'frmListEmployees')">Employee Name</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_code', 'frmListEmployees')">Code</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_ip_num', 'frmListEmployees')">IP No.</td>
    <td class="listHeaderCol">Email</td>
    <td class="listHeaderColClickable" onclick="setSort('g.grade_title', 'frmListEmployees')">Designation</td>
    <td class="listHeaderColClickable" onclick="setSort('e.emp_gender', 'frmListEmployees')">Gender</td>
    <td class="listHeaderColLast">Action</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		
		$cssWrkBtn = 'normal';
		$cssEduBtn = 'normal';
		
		if(strtotime($arrRecords[$ind]['modified_date']) >= strtotime('-3 days')) {
			$cssBtn = 'smallButtonUpdate';
			$trCSSClass = 'listContentAlternate" style="background-color:#B9D1B8"';
		}  else {
			$trCSSClass = 'listContentAlternate';
		}
		
		if(strtotime($arrRecords[$ind]['work_created']) >= strtotime('-3 days') || strtotime($arrRecords[$ind]['work_modified']) >= strtotime('-3 days')) {
			$cssWrkBtn = 'update';
		}		
		if(strtotime($arrRecords[$ind]['education_created']) >= strtotime('-3 days') || strtotime($arrRecords[$ind]['education_modified']) >= strtotime('-3 days')) {
			$cssEduBtn = 'update';
		}
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol" style="background-color:#FFF; padding:0px; width:60px">
    <?php
    if(!file_exists($pictureFolder . $arrRecords[$ind]['emp_photo_name']) || empty($arrRecords[$ind]['emp_photo_name'])) {
		$strGender = $arrRecords[$ind]['emp_gender'];
		if($strGender == '') {
			$strGender = 'Male';
		}
		$arrRecords[$ind]['emp_photo_name'] = 'no_image_' . strtolower($strGender) . '.jpg';
	}
	?>
    	<img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrRecords[$ind]['emp_photo_name']; ?>" width="60" height="60" />
    </td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_code']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_ip_num']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_work_email']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['grade_title']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_gender']; ?></td>
    <td class="listContentColLast">
      <div class="empColButtonContainer">
      <form name="frmAtt" method="post" target="_blank" action="<?php echo $this->baseURL . '/' . $this->currentController . '/att_detail'; ?>">
      	<input type="image" title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>">
        <input type="hidden" name="empCode" value="<?php echo $arrRecords[$ind]['emp_code']; ?>" />
      </form>
	  </div>
	  </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>