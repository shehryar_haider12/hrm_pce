<?php
$language	=	(isset($_POST['language']))		?	$_POST['language']	:	$record['language_name'];
if ($record['language_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['language_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['language_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 	= (isset($_POST['status'])) 		? 	$_POST['status']	:	$recordStatus;
?>

<form name="frmAddJobCategory" id="frmAddJobCategory" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['language_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Language</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add Language</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Language:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="language" name="language" maxlength="100" class="textBox" value="<?php echo $language; ?>">
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addLanguage" type="submit" value="Save">
                    <?php if(strpos($_SERVER["REQUEST_URI"],$record['language_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_language' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#status').val('<?php echo $status; ?>');
</script>