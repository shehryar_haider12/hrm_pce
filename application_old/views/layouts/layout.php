<?php
$css_path = $this->config->item("css_path");
$style_css_path = $this->config->item("style_css_path");
$script_path = $this->config->item("script_path");
$url = $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>PCE - HRMS</title>

    <link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>jquery-ui.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $style_css_path; ?>">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>reset.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>tipsy.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>xmodern.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>dashmix.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>dashmix.min.css">
    <script>var base_url = '<?php echo $this->baseURL; ?>';</script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/common.js"></script>
    <script type="text/javascript" src="<?php echo $script_path; ?>/jquery.tipsy.js"></script>
    <script type="text/javascript" src="<?php echo $script_path; ?>/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/popper.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/dashmix.core.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/dashmix.app.min.js"></script>

	<?php echo $font; ?>
  </head>
  <body>
	<div class="<?php if($url == '/hrm/' || $url == '/hrm/welcome') { echo 'loginBody'; }?> mainBody">
  		<div class="<?php if($url == '/hrm/' || $url == '/hrm/welcome') { echo 'transparentMainDiv'; }?>mainDiv">
			<!----- Header Start ----->
			<div class="headerMain">
				<?php echo $header; ?>
			</div>
			<!-----	Header End ----->
                        
       		<!----- Breadcrumbs Start ----->
			<!--<div class="Breadcrumbs">
				< ?php echo $breadcrumbs; ?>
			</div>-->
			<!-----	Breadcrumbs End -----> 
			
			<!----- Message Box Start [It is required to move in elements later] ----->
			<?php 
			if(trim($this->session->flashdata('success_message')) != '')
				$success_message = trim($this->session->flashdata('success_message'));
			if(trim($this->session->flashdata('error_message')) != '')
				$error_message = trim($this->session->flashdata('error_message'));
			if(trim($this->session->flashdata('validation_error_message')) != '')
				$validation_error_message = trim($this->session->flashdata('validation_error_message'));
			if ($success_message != '' || $error_message != '' || $validation_error_message != '') { ?>
				<div class="messageBox">
					<?php if($success_message != '') {?>
					<div class="successMessage">
						<?php echo $success_message; ?>
					</div>
					<?php } ?>

					<?php if($error_message != '') {?>
					<div class="errorMessage">
						<?php echo $error_message; ?>
					</div>
					<?php } ?>
					
					<?php if($validation_error_message != '') { ?>
					<div class="errorMessage">
						<?php echo $validation_error_message; ?>
					</div>
					<?php } ?>
					
				</div>
			<?php } ?>
			<!----- Message Box End  ----->
			
			<!----- Mid Section Start  ----->
			<div class="midMain">
    			<?php echo $content; ?>
			</div>
			<!----- Mid Section End ----->
			<!----- Footer Start ----->
			<div class="footerMain">
				<?php echo $footer; ?>
			</div>
			<!----- Footer End ----->
		</div>
	</div>
  </body>
</html>