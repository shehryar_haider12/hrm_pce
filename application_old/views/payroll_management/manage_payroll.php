<?php
$companyID 				= (isset($_POST['companyID'])) 				? $_POST['companyID'] 				: '';
$payrollMonth 			= (isset($_POST['payrollMonth'])) 			? $_POST['payrollMonth'] 			: date('n', strtotime('-1 month'));
$payrollYear 			= (isset($_POST['payrollYear'])) 			? $_POST['payrollYear'] 			: date('Y');
?>

<div class="listPageMain">
  <form name="frmManagePayroll" id="frmManagePayroll" method="post">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
      <tr>
        <td class="formHeaderRow" colspan="5">Generate Payroll Register</td>
      </tr>
      <tr>
        <td class="formLabelContainer">Company:</td>
        <td class="formTextBoxContainer"><select name="companyID" id="companyID" class="dropDown">
            <option value="">Select Company</option>
            <?php
                  for($ind = 0; $ind < count($arrCompanies); $ind++) {
                      $selected = '';
                      if($arrCompanies[$ind]['company_id'] == $companyID) {
                          $selected = 'selected="selected"';
                      }
                ?>
            <option value="<?php echo $arrCompanies[$ind]['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompanies[$ind]['company_name']; ?></option>
            <?php
                  }
                ?>
          </select></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">Month:</td>
        <td class="formTextBoxContainer"><select name="payrollMonth" id="payrollMonth" class="dropDown payrollMonth">
            <option value="">Select Month</option>
            <?php
				  if (count($arrMonths)) {
					  foreach($arrMonths as $strKey => $strValue) {
				  ?>
            <option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
            <?php
					  }
				  }
				  ?>
          </select></td>
      </tr>
      <tr>
        <td class="formLabelContainer">Year:</td>
        <td class="formTextBoxContainer"><select name="payrollYear" id="payrollYear" class="dropDown payrollYear">
            <option value="">Select Year</option>
            <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
            <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer"></td>
        <td class="formTextBoxContainer"><?php if($canWrite == YES) { ?>
          <input type="hidden" name="frmType" value="1" />
          <input type="submit" class="smallButton" name="btnGenerate" id="btnGenerate" value="Generate Register">
          <?php } ?></td>
      </tr>
    </table>
  </form>
  <?php if($this->userRoleID != ACCOUNT_EMPLOYEE_ROLE_ID) { ?>
  <div style="clear:both" id="payrollclose">&nbsp;</div>
  <form name="frmManagePayroll" id="frmManagePayroll" method="post">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
      <tr>
        <td class="formHeaderRow" colspan="5">Close Payroll Period</td>
      </tr>
      <tr>
        <td class="formLabelContainer">Month:</td>
        <td class="formTextBoxContainer"><select name="payrollMonth" id="payrollMonth" class="dropDown payrollMonth">
            <option value="">Select Month</option>
            <?php
				  if (count($arrMonths)) {
					  foreach($arrMonths as $strKey => $strValue) {
				  ?>
            <option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
            <?php
					  }
				  }
				  ?>
          </select></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">Year:</td>
        <td class="formTextBoxContainer"><select name="payrollYear" id="payrollYear" class="dropDown payrollYear">
            <option value="">Select Year</option>
            <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
            <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr>
        <td class="formLabelContainer"></td>
        <td class="formTextBoxContainer"><?php if($canWrite == YES) { ?>
          <input type="hidden" name="frmType" value="2" />
          <input type="submit" class="smallButton" name="btnClose" id="deletButton" value="Close Payroll">
          <?php } ?></td>
      </tr>
    </table>
  </form>
  <?php } ?>
</div>
<script>
  $('#companyID').val('<?php echo $companyID; ?>');
  $('.payrollMonth').val('<?php echo $payrollMonth; ?>');
  $('.payrollYear').val('<?php echo $payrollYear; ?>');
</script>