<?php
$annTitle 		= (isset($_POST['annTitle'])) 			? $_POST['annTitle'] 			: $record['ann_title'];
$annIsNew 		= (isset($_POST['annIsNew'])) 			? $_POST['annIsNew'] 			: $record['ann_is_new'];
$annFile 		= (isset($_POST['annFile'])) 			? $_POST['annFile'] 			: $record['ann_file'];
$annHTML 		= (isset($_POST['annHTML'])) 			? $_POST['annHTML'] 			: $record['ann_html'];
$annCompany		= (isset($_POST['annCompany'])) 		? $_POST['annCompany'] 			: $record['ann_company_id'];
$annIsPolicy	= (isset($_POST['annIsPolicy'])) 		? $_POST['annIsPolicy'] 		: $record['ann_is_policy'];
$annSortOrder 	= (isset($_POST['annSortOrder'])) 		? $_POST['annSortOrder'] 		: $record['ann_sort_order'];
$annStatus 		= (isset($_POST['annStatus'])) 			? $_POST['annStatus'] 			: $record['ann_status'];

if($annStatus == 0 && $annStatus != '') {
	$annStatus = -1;
}
?>
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
	width : 600,
 plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
 });
</script>

<div class="listPageMain">
  <div class="listContentMain">
    <form name="frmAnnouncement" id="frmAnnouncement" method="post" enctype="multipart/form-data">
      <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
          <td class="formHeaderRow" colspan="2">Add/Edit Policy/Announcement</td>
        </tr>
        <tr>
          <td class="formLabelContainer">Title:<span class="mandatoryStar"> *</span></td>
          <td class="formTextBoxContainer"><input type="text" id="annTitle" name="annTitle" class="textBox" value="<?php echo $annTitle; ?>"></td>
        </tr>
        <tr class="formAlternateRow">
          <td class="formLabelContainer">Show New Flag:</td>
          <td class="formTextBoxContainer"><input type="checkbox" name="annIsNew" id="annIsNew" value="1"></td>
        </tr>
        <tr>
          <td class="formLabelContainer">File:</td>
          <td class="formTextBoxContainer"><input type="file" name="annFile" id="annFile">
            <?php if(!empty($annFile)) { ?>
            <input type="hidden" id="pdfFileName" name="pdfFileName" class="textBox" value="<?php echo $annFile; ?>">
            <a href="<?php echo $this->imagePath . '/announcements/' . $annFile; ?>" class="normalLink" target="_blank">View File</a>
            <?php } ?>
            <br />
            Only PDFs Allowed
          </td>
        </tr>
        <!--<tr class="formAlternateRow">
          <td class="formLabelContainer">Announcement HTML:</td>
          <td class="formTextBoxContainer"><textarea rows="9" cols="30" name="annHTML" id="annHTML" class="textArea"><?php echo $annHTML; ?></textarea></td>
        </tr>
        <tr>
            <td class="formLabelContainer">Announcement Sort Order:</td>
            <td class="formTextBoxContainer">
                <input type="text" id="annSortOrder" name="annSortOrder" class="textBox" value="<?php //echo $annSortOrder; ?>">
            </td>
        </tr>-->
        <tr class="formAlternateRow">
            <td class="formLabelContainer">For Company:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="annCompany" name="annCompany" class="dropDown">
                  <option value="0">All</option>
                  <?php
                  if (count($arrCompanies)) {
                      foreach($arrCompanies as $arrCompany) {
                          $selected = '';
                          if($annCompany == $arrCompany['company_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrCompany['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompany['company_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
        <tr>
          <td class="formLabelContainer">Status</td>
          <td class="formTextBoxContainer"><select class="dropDown" name="annStatus" id="annStatus">
              <option value="">Select Status</option>
              <option value="1">Active</option>
              <option value="-1">InActive</option>
            </select></td>
        </tr>
        <tr class="formAlternateRow">
          <td class="formLabelContainer">Is Policy:</td>
          <td class="formTextBoxContainer"><input type="checkbox" name="annIsPolicy" id="annIsPolicy" value="1"></td>
        </tr>
        <tr>
          <td class="formLabelContainer"></td>
          <td class="formTextBoxContainer"><input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">
            &nbsp;
            <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)"></td>
        </tr>
      </table>
    </form>
  </div>
</div>
<script>
	<?php if((int)$annIsNew) { ?>
	$('#annIsNew').prop('checked', true);
	<?php } ?>
	<?php if((int)$annIsPolicy) { ?>
	$('#annIsPolicy').prop('checked', true);
	<?php } ?>
	$("#annStatus").val('<?php echo $annStatus; ?>');
	<?php if($canWrite == 0) { ?>
	$("#frmAnnouncement :input").attr("disabled", true);
	<?php } ?>
  </script>
<div class="clear">&nbsp;</div>
<div class="listContentMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr class="listHeader">
      <td class="listHeaderCol">Title</td>
      <td class="listHeaderCol">Company</td>
      <td class="listHeaderCol">Is New</td>
      <td class="listHeaderCol">Is Policy</td>
      <td class="listHeaderCol">Status</td>
      <td class="listHeaderColLast">Action</td>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrAnnouncements); $ind++) {
		if($ind % 2 == 0) {				
			$trCSSClass = 'listContent';
		} else {
			$trCSSClass = 'listContentAlternate';
		}
	?>
    <tr class="<?php echo $trCSSClass; ?>">
      <td class="listContentCol"><?php echo $arrAnnouncements[$ind]['ann_title']; ?></td>
      <td class="listContentCol"><?php echo ($arrAnnouncements[$ind]['company_name'] == '') ? 'All' : $arrAnnouncements[$ind]['company_name']; ?></td>
      <td class="listContentCol"><?php echo ($arrAnnouncements[$ind]['ann_is_new'] == 1) ? 'Yes' : 'No'; ?></td>
      <td class="listContentCol"><?php echo ($arrAnnouncements[$ind]['ann_is_policy'] == 1) ? 'Yes' : 'No'; ?></td>
      <td class="listContentCol"><?php echo ($arrAnnouncements[$ind]['ann_status'] == 1) ? 'Active' : 'InActive'; ?></td>
      <td class="listContentColLast"><div class="empColButtonContainer">
          <?php if($canWrite == 1) { ?>
          <input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $arrAnnouncements[$ind]['ann_id']; ?>';" />
          <?php } if($canDelete == 1) { ?>
          <input type="button" class="smallButton" id="deletButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrAnnouncements[$ind]['ann_id']; ?>');" />
          <?php } ?>
        </div></td>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
    <tr class="listContentAlternate">
      <td colspan="6" align="center" class="listContentCol">No Record Found</td>
    </tr>
    <?php
	}
	?>
  </table>
</div>
