<div class="midSection">	
	<div class="notificationWideMain" style="min-height:0px">
    	<div class="notificationWideBox">
        	<h1>PMS Reporting</h1>
            <div style="clear:both">&nbsp;</div>
            <div style="font-size:12px; font-family:Arial, Helvetica, sans-serif; float:left; width:300px">
				<ul>
					<li class="boldBlueHeading" style="padding-right:20px; list-style:circle; margin-left:20px"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/individual'; ?>" class="normalLink">Individual Reports</a></li>
					<li class="boldBlueHeading" style="padding-right:20px; list-style:circle; margin-left:20px"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/team'; ?>" class="normalLink">Team Reports</a></li>
				</ul>
            </div>
		</div>
	</div>
</div>