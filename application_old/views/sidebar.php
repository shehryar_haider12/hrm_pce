<div  id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed main-content-narrow">
		<nav class="cl" style="background-image: url('http:/.websoloutions.com/assets/img/sidebar-bg/01.jpg')" id="sidebar" aria-label="Main Navigation">
                <!-- Side Header (mini Sidebar mode) -->
                <div class="smini-visible-block">
                    <div class="content-header">
                        <!-- Logo -->
                        <a class="font-w600 text-white tracking-wide" href="index.html">
                            D<span class="opacity-75">x</span>
                        </a>
                        <!-- END Logo -->
                    </div>
                </div>
                <!-- END Side Header (mini Sidebar mode) -->

                <!-- Side Header (normal Sidebar mode) -->
                <div class="smini-hidden">
                    <div class="content-header justify-content-lg-center">
                        <!-- Logo -->
                        <a class="font-w600 text-white tracking-wide" href="index.html">    
						<!-- Dash<span class="opacity-75">mix</span>
                        <span class="font-w400">Modern</span> -->
						<img src="http:/.websoloutions.com/assets/img/logos/Pakistan.png" alt="Pakistan Currency Exchange Logo" width="150" heigh="auto">
                        </a>
						
                        <!-- END Logo -->

                        <!-- Options -->
                        <div class="d-lg-none">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <a class="text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                                <i class="fa fa-times-circle"></i>
                            </a>
                            <!-- END Close Sidebar -->
                        </div>
                        <!-- END Options -->
                    </div>
                </div>
                <!-- END Side Header (normal Sidebar mode) -->

                <!-- Sidebar Scrolling -->
                <div class="js-sidebar-scroll">
                    <!-- Side Actions -->
                    <div class="content-side content-side-full bg-black-10 text-center">
                        <div class="smini-hide">
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-user-circle"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-pencil-alt"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-file-alt"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-envelope"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary">
                                <i class="fa fa-fw fa-cog"></i>
                            </button>
                        </div>
                    </div>
                    <!-- END Side Actions -->

                    <!-- Side Navigation -->
                    <div class="content-side">
                        <ul class="nav-main">
                            <li class="nav-main-item">
                                <a class="nav-main-link active" href="/home">
                                    <i class="nav-main-link-icon fa fa-chart-bar"></i>
                                    <span class="nav-main-link-name">Dashboard</span>
                                    <span class="nav-main-link-badge badge badge-pill badge-primary">3</span>
                                </a>
                            </li>
                            <li class="nav-main-heading">Manage</li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/system_configuration">
                                    <i class="nav-main-link-icon fa fa-briefcase"></i>
                                    <span class="nav-main-link-name">System Configuration</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration">
                                            <i class="nav-main-link-icon fa fa-plus"></i>
                                            <span class="nav-main-link-name">Open System Configuration</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/system_settings">
                                            <i class="nav-main-link-icon fa fa-plus"></i>
                                            <span class="nav-main-link-name">System Settings</span>
                                        </a>
                                    </li>

                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/list_module">
                                            <i class="nav-main-link-icon fa fa-star"></i>
                                            <span class="nav-main-link-name">Modules</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/list_companies">
                                            <i class="nav-main-link-icon fa fa-star"></i>
                                            <span class="nav-main-link-name">Companies</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/docList_Real">
                                            <i class="nav-main-link-icon fa fa-star"></i>
                                            <span class="nav-main-link-name">Documents</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/docList_Real">
                                            <i class="nav-main-link-icon fa fa-star"></i>
                                            <span class="nav-main-link-name">Documents</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/list_job_category">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Departments</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/list_edu_level">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Education Levels</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/system_configuration/list_edu_major">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Education Majors</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/user_management">
                                    <i class="nav-main-link-icon fa fa-user"></i>
                                    <span class="nav-main-link-name">User Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management">
                                            <i class="nav-main-link-icon fa fa-plus"></i>
                                            <span class="nav-main-link-name">User Management</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/list_user">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage Users</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/save_user_role">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add User Role</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/list_user_role">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage User Roles</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/save_user_permission">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add User Permission</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/list_user_permission">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage User Permission</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/special_permissions">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Special Permissions</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/user_management/user_logs">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">User Logs</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
							<li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/user_management">
                                    <i class="nav-main-link-icon fa fa-user"></i>
                                    <span class="nav-main-link-name">Employee Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management">
                                            <i class="nav-main-link-icon fa fa-plus"></i>
                                            <span class="nav-main-link-name">Employee Management</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/add_employee">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Add Employee</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/save_employee">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Profile</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/employment_details">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Employment Details</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/emp_education_history">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Educational History</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/emp_work_history">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Work Experience</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/dependents">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Dependents</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/emergency_contacts">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Emergency Contacts</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/employee_management/emergency_contacts">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Emergency Contacts</span>
                                        </a>
                                    </li>
								</ul>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/policy_announcements">
                                    <i class="nav-main-link-icon fa fa-file-alt"></i>
                                    <span class="nav-main-link-name">Policies & Announcements</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/policy_announcements">
                                            <i class="nav-main-link-icon fa fa-plus"></i>
                                            <span class="nav-main-link-name">Policies & Announcements</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/complain_management">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">Add Edit Policies Announcement</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/complain_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Request Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/complain_management">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">Complain Management</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/complain_management/submit_complain">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Submit Request</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/complain_management/list_complains">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">List Requests</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/attendance_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Attendance Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/attendance_management">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">Attendance Management</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/complain_management/submit_complain">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Submit Request</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/complain_management/list_complains">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">List Requests</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
							<li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/task_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Performance Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/task_management">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">Performance Managment</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/task_management/my_tasks">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Self Appraisal</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/task_management/my_team_tasks">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Team's Self Appraisal</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/task_management/my_kpis">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">KPIs</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/task_management/my_team_kpis">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Team's KPIs</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/task_management/probation_assessment">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Probationary Assessment</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
							<li  class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/payroll_management">
                                    <i class="nav-main-link-icon fa fa-check"></i>
                                    <span class="nav-main-link-name">Payroll Management</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/payroll_management">
                                            <i class="nav-main-link-icon fa fa-hourglass"></i>
                                            <span class="nav-main-link-name">Payroll Management</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/payroll_management/save_payroll">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Submit Monthly Salary</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/payroll_management/list_payroll">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Payroll Details</span>
                                        </a>
                                    </li>
									<li class="nav-main-item">
                                        <a class="nav-main-link" href="/payroll_management/manage_payroll">
                                            <i class="nav-main-link-icon fa fa-pencil-alt"></i>
                                            <span class="nav-main-link-name">Manage Payroll</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="/forum">
                                    <i class="nav-main-link-icon fa fa-glasses"></i>
                                    <span class="nav-main-link-name">Forum</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/forum">
                                            <i class="nav-main-link-icon fa fa-book-open"></i>
                                            <span class="nav-main-link-name">Forum</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/forum/birthday_calendar">
                                            <i class="nav-main-link-icon fa fa-globe"></i>
                                            <span class="nav-main-link-name">Birthday Calender</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/forum/list_events">
                                            <i class="nav-main-link-icon fa fa-directions"></i>
                                            <span class="nav-main-link-name">Events</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-main-heading">Dashboards</li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="../">
                                    <i class="nav-main-link-icon fa fa-arrow-left"></i>
                                    <span class="nav-main-link-name">Go Back</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- END Sidebar Scrolling -->
            </nav>
	</div>
