<script>
function doSlide(trID, trDescID) {
	// set effect from select menu value
	$('#' + trDescID).toggle('slow');
	setTimeout(function() { if($('#' + trDescID).is(":visible") == true) {
									$('#' + trID).attr('class', 'listContentAlternate');
								} else {
									$('#' + trID).attr('class', 'listContent');
								}
							}
			, 600);
}
</script>
<style>
/*
.recordDiv{border:1px solid #59A7DF;border-collapse:collapse;float:left;width:1256px}
.vacanciesTable{font-family:arial;font-size:11pt;text-align:left;width:1256px}
.vacanciesHeader{background:url('http://www.sbtjapan.com/images/table_header_bg.jpg') repeat-x scroll 0 0 rgba(0, 0, 0, 0);border-color:#000000;border-style:dotted;border-width:0 1px 1px 0;font-size:11pt;height:32px;padding:0 0 0 4px}
.vacanciesCol{font-family:arial;font-size:13px;line-height:15px;padding:5px;vertical-align:middle}
.vacanciesColBorderRight{border-right:1px dotted #9FA1A6}
.vacanciesColBorderBottom{border-bottom:1px dotted #9FA1A6}
.jobDetails{float:left;font-family:arial;padding:10px;width:100%}
.jobDetails h1{color:#3f6195;font-size:20px}
.jobDetails ul{color:#000;line-height:20px;list-style:square inside none;margin:0 0 0 2px}
.smallButton {background-color:#4C6A97;border:1px solid #01546E;color:#FFFFFF;font-size:15px;height:26px;padding:3px 10px;text-align:center}
.smallButton:hover{background-color:#73A1D7;box-shadow:0px 3px 5px #999;color:#000}
.vacanciesAlternateRow{background-color:#DBF3FB}
*/

</style>
<div id="recordDiv" class="listContentCareer">
  <table class="listTableMain" cellspacing="0" border="0" style="border:solid 1px #e1e1e1; border-collapse:collapse;">
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		if(!$ind) {
	?>
	<tr class="listHeader">
      <td class="listHeaderCol" width="300px">Job Title</td>
      <td class="listHeaderCol" width="200px">Career Level</td>
      <td class="listHeaderCol" width="150px">Work Shift</td>
      <td class="listHeaderCol" width="80px;">Positions</td>
      <td class="listHeaderCol" width="80px;">Location</td>
      <td class="listHeaderCol" width="200px">End Date</td>
      <td class="listHeaderCol" width="200px">Actions</td>
    </tr>
    <?php
		}
	?>
    <tr class="listContent" id="tr_<?php echo $ind; ?>">
      <!--	<td class="vacanciesCol vacanciesColBorderRight vacanciesColBorderBottom">< ?php echo $arrRecords[$ind]['vacancy_name']; ?></td>	-->
      <td class="listContentCol"><?php echo $arrRecords[$ind]['vacancy_name']; ?></td>
      <td class="listContentCol"><?php echo $arrRecords[$ind]['vacancy_level']; ?></td>
      <td class="listContentCol"><?php echo (trim($arrRecords[$ind]['shift_name']) != '') ? $arrRecords[$ind]['shift_name'] : '-'; ?></td>
      <td class="listContentCol"><?php echo $arrRecords[$ind]['no_of_positions']; ?></td>
      <td class="listContentCol"><?php echo $arrRecords[$ind]['location_name']; ?></td>
      <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['vacancy_to'], $showDateFormat); ?></td>
      <td class="listContentColLast" align="right"><div style="float:right">
          <input class="smallButton" type="button" value="View Details" name="btnDetails" id="btnDetails<?php echo $ind; ?>" onclick="$('#btnDetails<?php echo $ind; ?>').toggleClass('smallButtonSelected');doSlide('tr_<?php echo $ind; ?>', 'tr<?php echo $ind; ?>')">
          &nbsp;
          <input class="smallButton" id="deletButton" type="button" value="Apply" name="btnApply" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/apply/' . $arrRecords[$ind]['vacancy_id']; ?>';">
          &nbsp; </div></td>
    </tr>
    <tr class="listContent" id="tr<?php echo $ind; ?>" style="display:none" >
		<td class="" colspan="7">
			<div class="interviewHistoryMain">
			<table class="historyTableMain" cellspacing="5">
				<tr class="historyHeader">
					<td class="historyHeaderCol" style="">Job Description:</td>
				</tr>
				<tr class="historyContent">
					<td class="historyContentCol" style=""><?php echo $arrRecords[$ind]['description']; ?></td>
				</tr>
			</table>
			</div>
		</td>
    </tr>
<!--
    <tr id="tr< ?php echo $ind; ?>" style="display:none" >
      <td colspan="7" class="vacanciesCol vacanciesColBorderBottom"><div class="jobDetails">
          <h1>Job Description:</h1>
          <div style="clear:both"></div>
          < ?php echo $arrRecords[$ind]['description']; ?> </div></td>
    </tr>
-->
    <?php
      }
      if(!$ind) {
      ?>
    <tr class="listContentAlternate">
      <td align="center" class="listContentCol">Sorry, Currently We Are Not Hiring !!</td>
    </tr>
    <?php
      }
    ?>
  </table>
</div>
<br />
<div align="center"><?php echo $pageLinks; ?></div>
