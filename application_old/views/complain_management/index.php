<div class="midSection">
    <div class="dashboardTitle">
        Welcome To Request/Complain Management System
    </div>
	
	<div class="dashboardBoxesMain">
	<?php
	foreach($allowedSubModulesList as $modules => $moduleValue)
	{	
		$tipsyID = '';
		$complain = false;
		$task = false;
		
		if(strpos(strtolower($moduleValue['module_name']), 'list_complains') !== false) {
			$arrWhere = array(
							'emp_id' => $this->userEmpNum
							);
							
			$issueUnChecked = getIssuesUnChecked($arrWhere);
			if((int)$issueUnChecked){
				$tipsyID = ' tipsyID';
				$complain = true;
			}			
		}
		
		if(strpos(strtolower($moduleValue['module_name']), 'list_tasks') !== false) {
			$arrWhere = array(
							'emp_id' => $this->userEmpNum
							);
							
			$tasksUnChecked = getTasksUnChecked($arrWhere);
			if((int)$tasksUnChecked){
				$tipsyID = ' tipsyID';
				$task = true;
			}
		}
	?>
		<div class="boxMain<?php echo $tipsyID; ?>" original-title="<?php if($complain) { ?><?php echo $issueUnChecked; ?> issue(s) awaiting response<?php } else if($task) { ?><?php echo $tasksUnChecked; ?> task(s) awaiting response<?php } ?>">			
				<div class="boxImageMain">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
                		<img src="<?php echo $this->imagePath . '/modules/' . $moduleValue['module_name']; ?>.png" alt="">
                    </a>
                </div>
				<!--<div class="boxTitle">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
						<?php echo $moduleValue['display_name']; ?>
                    </a>
                </div>-->
		</div>
	<?php
	}
	?>
	</div>
</div>
<script>
	$(".tipsyID").each(function() {
		$( this ).tipsy({gravity: "s", title: "original-title", trigger: "manual"});
		$( this ).tipsy("show");
	});
</script>