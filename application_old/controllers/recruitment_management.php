<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recruitment_Management extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $sendMail;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_recruitment_management', 'recruitment', true);
		
		$this->arrRoleIDs 						= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_INTERNEE_ROLE_ID, HR_RECRUITER_ROLE_ID);
 		$this->arrIntRoleIDs      				= explode(',', INTERVIEWERS_ROLE_IDS);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["resumeFolder"]			= RESUME_FOLDER;
		$this->arrData["resumeFolderDownload"]	= str_replace('./', '', RESUME_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->arrData["GMEmpID"]				= GM_EMP_ID; 	# MR. ALI HUSSAIN
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$this->arrData["arrArrivalSource"]		= explode(',', ARRIVAL_SOURCES);
		$this->arrData["arrArrivalSource"][]	= CONSTANT_REFERRED_BY;
		$this->arrData["arrArrivalSource"][]	= CONSTANT_REFERRED_BY_SBT_EMPLOYEE;
		$this->arrData["arrArrivalSource"][]	= CONSTANT_LAST_ARRIVAL_SOURCE;
		
		# SETUP CANDIDATE VACANCY STATUS CONSTANTS
		$this->setupStatuses();
		
		if($this->userRoleID == HR_ADMIN_ROLE_ID) {
			$this->sendMail	= false;
		} else {
			$this->sendMail = true;
		}
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu');
		$this->template->write_view('content', 'recruitment_management/index', $this->arrData);
		$this->template->render();
	}
	
	public function save_candidate($candidateID = 0) {
		
		$tblName = TABLE_CANDIDATE;
		$tblNameUsers = TABLE_USER;	
		$tblNameLocations = TABLE_LOCATION;		
		
		$this->load->model('model_user_management', 'user_management', true);
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$candidateID = (int)$candidateID;
		if($candidateID) {			
			$arrWhere = array(
							'c.candidate_id' => $candidateID
							);
		}
		
		#################################### FORM VALIDATION START ####################################
		
		$this->form_validation->set_rules('firstName', 'First Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('eMail', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('contactNo', 'Cell Phone Number', 'trim|xss_clean|callback_strValidate[contactNo]');
		$this->form_validation->set_rules('homePhoneNo', 'Home Phone Number', 'trim|xss_clean|callback_strValidate[homePhoneNo]');
		$this->form_validation->set_rules('candidateAddress', 'Address', 'trim|xss_clean');
		$this->form_validation->set_rules('nicNo', 'NIC Number', 'trim|numeric|min_length[13]|max_length[13]|xss_clean');
		$this->form_validation->set_rules('fatherNICNo', 'Father\'s NIC Number', 'trim|numeric|min_length[13]|max_length[13]|xss_clean');
		$this->form_validation->set_rules('Gender', 'Gender', 'trim|required|xss_clean');
		$this->form_validation->set_rules('maritalStatus', 'Marital Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dateOfBirth', 'Date of Birth', 'trim|required|xss_clean');
		$this->form_validation->set_rules('Country', 'Country', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('City', 'City', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('Comments', 'About Candidate', 'trim|xss_clean');
		
		if(isset($_POST['Area'])){	
			$this->form_validation->set_rules('Area', 'Area', 'trim|required|numeric|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			if(
				$this->recruitment->notExistingCandidate('email', $this->input->post("eMail"), $candidateID) 
				&& ($this->recruitment->notExistingCandidate('nic_number', $this->input->post("nicNo"), $candidateID) || $this->input->post("nicNo") == '')
			) {
				$uploadConfig['upload_path'] = $this->arrData["resumeFolder"];
				$uploadConfig['allowed_types'] = 'rtf|doc|docx|pdf';
				$uploadConfig['max_size']	= '1024';
				$uploadConfig['max_filename']	= '100';
				$uploadConfig['file_name']	= strtolower($this->input->post("firstName") . '_' . $this->input->post("lastName")) . '_' . date('dmYHis');
	
				$this->load->library('upload', $uploadConfig);
				
				if(!$this->upload->do_upload('Resume')) {
					$error = array('error' => $this->upload->display_errors());		
					$this->arrData['error_message'] = $error['error'];
				} else {		
					$dataUpload = $this->upload->data();
					$fileName = basename($dataUpload['file_name']);
					if($fileName != $this->input->post("cvFileName")) {
						unlink($this->arrData["resumeFolder"] . $this->input->post("cvFileName")); # DELETE PREVIOUS RESUME FILE
					}
				}
				
				$statusID = (int)$this->input->post("Status"); 
				if($statusID == STATUS_INACTIVE) {
					$statusID = STATUS_INACTIVE_VIEW;
				}
				$arrValues = array(
									'first_name' => $this->input->post("firstName"),
									'last_name' => $this->input->post("lastName"),
									'contact_number' => $this->input->post("contactNo"),
									'home_contact_number' => $this->input->post("homePhoneNo"),
									'candidate_address' => $this->input->post("candidateAddress"),
									'email' => $this->input->post("eMail"),
									'nic_number' => $this->input->post("nicNo"),
									'father_nic_number' => $this->input->post("fatherNICNo"),
									'gender' => $this->input->post("Gender"),
									'marital_status' => $this->input->post("maritalStatus"),
									'dob' => $this->input->post("dateOfBirth"),
									'city' => $this->input->post("City"),
									'area' => $this->input->post("Area"),
									'country' => $this->input->post("Country"),
									'comment' => $this->input->post("Comments")
									);
				
				if($candidateID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				} else {
					$arrValues['arrival_source'] = 'System';
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				}
				
				if($statusID == STATUS_DELETED) {
					$arrValues['deleted_by'] = $this->userEmpNum;
					$arrValues['deleted_date'] = date($this->arrData["dateTimeFormat"]);
				}
									
				if(trim($fileName) != '') {
					$arrValues['cv_file_name'] = $fileName;
				}
				
				$candID = $this->recruitment->saveValues($tblName, $arrValues, $arrWhere);
				
				if(count($arrWhere) <= 0) { # IF NEW USER
					
					$userName = str_replace(' ', '', strtolower(substr($this->input->post("firstName"), 0, 1) . $this->input->post("lastName") . $candID));
					$passPhrase = randomString(8);
					
					$arrValues = array(
							'user_role_id' => 1,
							'candidate_id' => $candID,
							'user_name' => $userName,
							'plain_password' => $passPhrase,
							'password' => md5($passPhrase),
							'user_status' => $statusID,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
							
									
					if($this->user_management->saveValues($tblNameUsers, $arrValues)) {
						
						/*
						# SHOOT EMAIL
						# Template: new_candidate.html
						
						$arrValues = array(
											'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
											'[CANDIDATE_NAME]' => $this->input->post("firstName") . ' ' . $this->input->post("lastName"),
											'[CANDIDATE_DASHBOARD_LINK]' => $this->baseURL . '/career',
											'[CANDIDATE_USERNAME]' => $userName,
											'[CANDIDATE_PASSWORD]' => $passPhrase,
											'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'new_candidate.html');
						
						$this->sendEmail(
											$arrTo = array(
															$this->input->post("eMail")
															), 				# RECEIVER DETAILS
											'Welcome To SBT Japan', 		# SUBJECT
											$emailHTML						# EMAIL HTML MESSAGE
										);
						*/
						
					}
				} else {
					
					$this->user_management->saveValues(	$tblNameUsers, 
														array('user_status' => $statusID), 		# WHAT TO UPDATE
														array('candidate_id' => $candidateID)	# WHERE
														);
				}
							
				# SET LOG
				
				$this->session->set_flashdata('success_message', 'Candidate saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/list_candidate');
				exit;
			} else {
				$this->arrData['validation_error_message'] = '<p>E-Mail/NIC is already registered</p>';
			}				
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT CANDIDATE RECORD
		if($candidateID) {
			$this->arrData['record'] = $this->recruitment->getCandidateDetail($arrWhere);
			
			if($this->arrData['record']['arrival_source'] == CONSTANT_REFERRED_BY_SBT_EMPLOYEE && strlen($this->arrData['record']['reference']) == 4) {
				$this->arrData['isEmpReference'] = true;
				$this->arrData['arrEmpReference'] = $this->employee->getEmployeeDetail(array('e.emp_code' => $this->arrData['record']['reference']));
			}
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['candidateID'] = $candidateID;
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles(array('job_title_status' => STATUS_ACTIVE));
		$this->arrData['Countries'] = $this->configuration->getLocations(array('location_type_id' => '2'));
		if((int)$this->arrData['record']['city']) {
			$countryID = $this->configuration->getLocationParent(array('location_id' => (int)$this->arrData['record']['city']));
			$this->arrData['countryID'] = (int)$countryID['location_parent_id'];
			$this->arrData['Cities'] = $this->configuration->getLocations(array('location_parent_id' => $this->arrData['countryID']));			
		}
		$this->arrData['Genders'] = $this->config->item('genders');
		$this->arrData['maritalStatuses'] = $this->config->item('marital_status');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/save_candidate', $this->arrData);
		$this->template->render();
	}
	
	public function complete_profile($candidateID = 0) {
		$tblName = TABLE_CANDIDATE;
		$tblNameUsers = TABLE_USER;	
		$tblNameLocations = TABLE_LOCATION;		
		
		$this->load->model('model_user_management', 'user_management', true);
		$this->load->model('model_profile', 'my_profile', true);
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$candidateID = (int)$candidateID;
		if($candidateID) {			
			$arrWhere = array(
							'c.candidate_id' => $candidateID
							);
		}
		
		# CODE FOR CURRENT CANDIDATE RECORD
		if($candidateID) {
			$this->arrData['record'] = $this->recruitment->getCandidateDetail($arrWhere);
			
			# Miscellaneous
			$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => STATUS_ACTIVE));
			$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => STATUS_ACTIVE));
			$this->arrData['arrCanStatuses'] = $this->recruitment->getCandidateStatuses();		
			$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $candidateID));
			
			# Education Details
			$this->arrData['eduCandidateID'] = $candidateID;
			$this->arrData['arrRecords'] = $this->recruitment->getEducationalHistory(array('e.edu_candidate_id' => $candidateID));
			
			# Experience Details
			$this->arrData['workCandidateID'] = $candidateID;
			$this->arrData['arrRecordsWork'] = $this->recruitment->getWorkHistory(array('work_candidate_id' => $candidateID));
			
			# Applied Vacancies Details
			$this->arrData['arrRecordsVacancies'] = $this->recruitment->getCandidateVacancies(array('candidate_id' => $candidateID));
			
			# Application Form Details
			$this->arrData['applicationRecord'] = $this->my_profile->getCandidateApplicationDetail(array('a.candidate_id' => $candidateID));
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles(array('job_title_status' => STATUS_ACTIVE));
		$this->arrData['Countries'] = $this->configuration->getLocations(array('location_type_id' => '2'));
		
		$countryID = $this->configuration->getLocationParent(array('location_id' => (int)$this->arrData['record']['city']));
		$this->arrData['countryID'] = (int)$countryID['location_parent_id'];
		$this->arrData['Cities'] = $this->configuration->getLocations(array('location_parent_id' => $this->arrData['countryID']));
		$this->arrData['Areas'] = $this->configuration->getLocations(array('location_parent_id' => $this->arrData['record']['city']));			
		
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/complete_profile', $this->arrData);
		$this->template->render();
	}
	
	public function list_candidate($pageNum = 1) {
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$notApplied = false;
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->recruitment->deleteValue(TABLE_CANDIDATE, array('candidate_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					echo YES; exit;
				}								
			}
		}
		##########################
		
		$arrWhere = array();
		if ($this->input->post()) {
			
			if($this->input->post("notApplied")) {
				$notApplied = (int)$this->input->post("notApplied");
				$this->arrData['notApplied'] = $notApplied;
			} else {
				if($this->input->post("jobTitleCode")) {
					$arrWhere['jv.job_title_code'] = $this->input->post("jobTitleCode");
					$this->arrData['jobTitleCode'] = $this->input->post("jobTitleCode");
				}
				if($this->input->post("vacancyName")) {
					$arrWhere['jv.vacancy_id'] = $this->input->post("vacancyName");
					$this->arrData['vacancyName'] = $this->input->post("vacancyName");
				}
				if($this->input->post("Status")) {
					$arrWhere['cv.status'] = $this->input->post("Status");
					$this->arrData['Status'] = $this->input->post("Status");
				}
				if($this->input->post("hiringManagerID")) {
					$arrWhere['jv.hiring_manager_id'] = $this->input->post("hiringManagerID");
					$this->arrData['hiringManagerID'] = $this->input->post("hiringManagerID");
				}
				if($this->input->post("dateInterview")) {
					$arrWhere['ji.interview_date'] = $this->input->post("dateInterview");
					$this->arrData['dateInterview'] = $this->input->post("dateInterview");
				}
				if($this->input->post("Interviewer")) {
					$arrWhere['jii.interviewer_id'] = $this->input->post("Interviewer");
					$this->arrData['Interviewer'] = $this->input->post("Interviewer");
				}
			}
			if($this->input->post("Gender")) {
				$arrWhere['c.gender'] = $this->input->post("Gender");
				$this->arrData['Gender'] = $this->input->post("Gender");
			}
			if($this->input->post("eduDegree")) {
				$arrWhere['e.edu_level_id'] = $this->input->post("eduDegree");
				$this->arrData['eduDegree'] = $this->input->post("eduDegree");
			}
			if($this->input->post("eduMajor")) {
				$arrWhere['e.edu_major_id'] = $this->input->post("eduMajor");
				$this->arrData['eduMajor'] = $this->input->post("eduMajor");
			}
			if($this->input->post("checkedBy")) {
				$arrWhere['c.resume_downloaded_by'] = $this->input->post("checkedBy");
				$this->arrData['checkedBy'] = $this->input->post("checkedBy");
			}
			if($this->input->post("dateFrom")) {
				$arrWhere['c.created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['c.created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			if($this->input->post("candEmail")) {
				$arrWhere['c.email'] = $this->input->post("candEmail");
				$this->arrData['candEmail'] = $this->input->post("candEmail");
			}
			if($this->input->post("City")) {
				$arrWhere['c.city'] = $this->input->post("City");
				$this->arrData['City'] = $this->input->post("City");
			}
			if($this->input->post("cityArea")) {
				$arrWhere['c.area'] = $this->input->post("cityArea");
				$this->arrData['cityArea'] = $this->input->post("cityArea");
			}
			if($this->input->post("mobileNumber")) {
				$arrWhere['c.contact_number'] = $this->input->post("mobileNumber");
				$this->arrData['mobileNumber'] = $this->input->post("mobileNumber");
			}
			if($this->input->post("fatherNICNo")) {
				$arrWhere['c.father_nic_number'] = $this->input->post("fatherNICNo");
				$this->arrData['fatherNICNo'] = $this->input->post("fatherNICNo");
			}
			if($this->input->post("arrivalSource")) {
				if($this->input->post("arrivalSource") == CONSTANT_LAST_ARRIVAL_SOURCE) {
					$arrWhere['c.arrival_source not in '] = "('" . implode("','", $this->arrData["arrArrivalSource"]) . "','Mustakbil','System')";
				} else if ($this->input->post("arrivalSource") == 1) {
					$arrWhere['c.arrival_source != '] = 'System';
				} else {
					$arrWhere['c.arrival_source'] = $this->input->post("arrivalSource");
				}
				$this->arrData['arrivalSource'] = $this->input->post("arrivalSource");
			}
		}
		
		$candStatus = $this->input->post("candStatus");
		if($candStatus != '') {
			if($candStatus == STATUS_INACTIVE) {
				$candStatus = STATUS_INACTIVE_VIEW;
			}
			$arrWhere['u.user_status'] = $candStatus;
			$this->arrData['candStatus'] = $this->input->post("candStatus");
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['u.user_status < '] = STATUS_DELETED;
			}
		}
		
		# CODE FOR PAGING
		$this->arrData['totalRecordsCount'] = $this->recruitment->getTotalCandidates($arrWhere, $notApplied);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->recruitment->getCandidates($arrWhere, $this->limitRecords, $offSet, $notApplied);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListCandidates');
		
		# CODE FOR PAGE CONTENT		
		if(isAdmin($this->userRoleID)) {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE);
			$arrWhereVacancyTitles = array();
		} else {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE, 'jt.job_category_id' => $this->userCategoryID);
			$arrWhereVacancyTitles = array('jt.job_category_id' => $this->userCategoryID);
		}
		
		$this->arrData['Genders'] = $this->config->item('genders');
		$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => STATUS_ACTIVE));
		$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => STATUS_ACTIVE));
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles($arrWhereJobTitles);	
		$this->arrData['vacancyTitles'] = $this->configuration->getVacancyTitles($arrWhereVacancyTitles);
		$this->arrData['arrEmpHR'] = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE, 'e.emp_job_category_id' => HR_JOB_CATEGORY_ID), '', '', false);
		$this->arrData['arrLocations'] = $this->configuration->getLocations();
		$this->arrData['arrInterviewers'] = $this->employee->getSupervisors();		
		$this->arrData['arrCanStatuses'] = $this->recruitment->getCandidateStatuses();		
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/list_candidate', $this->arrData);
		$this->template->render();
	}
	
	public function download_resume($candidateID = 0, $fileName = '') {
		
		if((int)$candidateID && !empty($fileName)) {
			
			$tblName = TABLE_CANDIDATE;
			
			$arrValues = array(
								'resume_downloaded' => YES,
								'resume_downloaded_by' => $this->userEmpNum,
								'resume_downloaded_date' => date(DATE_TIME_FORMAT)
								);
			
			$arrWhere = array(
								'candidate_id' => (int)$candidateID, 
								'resume_downloaded' => NO
								);
								
			$this->recruitment->saveValues($tblName, $arrValues, $arrWhere);
			
			# SET LOG
				
			header('Content-Type: text/doc');
			header('Content-Disposition: attachment;filename="'.$fileName.'"');
			header('Cache-Control: max-age=0');
			readfile($this->arrData["resumeFolder"] . $fileName);
			exit;
		
		} else {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
	}
	
	public function apply_vacancy($candidateID = 0) {
				
		$tblName = TABLE_CANDIDATE_VACANCY;
		$tblNameHistory = TABLE_CANDIDATE_HISTORY;
		
		if(!$candidateID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('candidateID', 'Candidate', 'trim|required|numeric');
		$this->form_validation->set_rules('vacancyID', 'Vacancy', 'trim|required|numeric');
		$this->form_validation->set_rules('appliedDate', 'Applied Date', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'candidate_id' => $this->input->post("candidateID"),
								'vacancy_id' => $this->input->post("vacancyID"),
								'status' => STATUS_ACTIVE,
								'applied_date' => $this->input->post("appliedDate")
								);
								
			$this->recruitment->saveValues($tblName, $arrValues);			
			
			$this->recruitment->deleteValue($tblNameHistory, array(
																	'candidate_id' => $this->input->post("candidateID"), 
																	'vacancy_id' => $this->input->post("vacancyID")
																	)
											);
																
			$arrValuesHistory = array(
								'candidate_id' => $this->input->post("candidateID"),
								'vacancy_id' => $this->input->post("vacancyID"),
								'performed_by' => $this->userEmpNum,
								'performed_date' => date($this->arrData["dateTimeFormat"]),
								'status_id' => STATUS_ACTIVE,
								'note' => 'Applied for vacancy on candidate\'s behalf'
								);								
			
			$this->recruitment->saveValues($tblNameHistory, $arrValuesHistory);
			
			# SET LOG
				
			$this->session->set_flashdata('success_message', 'Vacancy applied successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/candidate_vacancy/' . $candidateID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['candidateID'] = $candidateID;
		$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $candidateID));
		$this->arrData['arrVacancies'] = $this->recruitment->getVacancies();
		$arrCandidateVacancies = $this->recruitment->getCandidateVacancies(array('candidate_id' => $candidateID));
		$arrCanVac = array();
		foreach($arrCandidateVacancies as $arrCV) {
			$arrCanVac[] = $arrCV['vacancy_id'];
		}
		$this->arrData['arrCandidateVacancies'] = $arrCanVac;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/apply_vacancy', $this->arrData);
		$this->template->render();
	}
	
	public function education_history($eduCandidateID = 0, $eduID = 0) {
		
		$eduCandidateID = (int)$eduCandidateID;
		
		if(!$eduCandidateID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->recruitment->deleteValue(TABLE_CANDIDATE_EDUCATION, array('edu_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('eduCandidateID', 'Candidate', 'trim|required|numeric');
		$this->form_validation->set_rules('eduLevel', 'Qualification Level', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('eduInstitute', 'Educational Institute', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('eduMajors', 'Majors', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('eduGPA', 'GPA', 'trim|xss_clean');
		$this->form_validation->set_rules('eduEnded', 'Passing Year', 'trim|required|min_length[4]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$eduID = (int)$eduID;
		$arrWhere['e.edu_candidate_id'] = $eduCandidateID;
		$this->arrData['record'] = array();
		
		if($eduID) {			
			$arrWhere['e.edu_id'] = $eduID;
		}
		
		if ($this->form_validation->run() == true) {
			
			$arrValues = array(
								'edu_candidate_id' => $this->input->post("eduCandidateID"),
								'edu_level_id' => $this->input->post("eduLevel"),
								'edu_institute' => $this->input->post("eduInstitute"),
								'edu_major_id' => $this->input->post("eduMajors"),
								'edu_gpa' => ($this->input->post("eduGPA") != FALSE) ? $this->input->post("eduGPA") : NULL,
								'edu_ended' => $this->input->post("eduEnded")
								);
								
			if($eduID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
				
			if($eduID) {
				$this->recruitment->saveValues(TABLE_CANDIDATE_EDUCATION, $arrValues, $arrWhere);
			} else {
				$this->recruitment->saveValues(TABLE_CANDIDATE_EDUCATION, $arrValues);
			}					
			
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);			
			$this->recruitment->saveValues(TABLE_CANDIDATE, $arrValues, array('candidate_id' => $eduCandidateID));
			
			$this->session->set_flashdata('success_message', 'Education history saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $eduCandidateID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EDUCATION RECORD
		if($eduID) {
			$this->arrData['record'] = $this->recruitment->getEducationalHistory($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['eduCandidateID'] = $eduCandidateID;
		$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => STATUS_ACTIVE));
		$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => STATUS_ACTIVE));
		$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $eduCandidateID));
		$this->arrData['arrRecords'] = $this->recruitment->getEducationalHistory(array('e.edu_candidate_id' => $eduCandidateID));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/education_history', $this->arrData);
		$this->template->render();
	}
	
	public function work_history($workCandidateID = 0, $workID = 0) {
		
		$workCandidateID = (int)$workCandidateID;
		
		if(!$workCandidateID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->recruitment->deleteValue(TABLE_CANDIDATE_WORK_EXPERIENCE, array('work_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					
					# SET LOG
				
					echo YES; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('workCandidateID', 'Candidate', 'trim|required|numeric');
		$this->form_validation->set_rules('workCompany', 'Company Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('workJobTitle', 'Job Title', 'trim|required|alpha_dash_space|min_length[5]|xss_clean');
		$this->form_validation->set_rules('workFrom', 'Starting Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('workTo', 'Work Ending Date', 'trim|xss_clean');
		$this->form_validation->set_rules('workDescription', 'Work Description', 'trim|max_length[1000]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$workID = (int)$workID;
		$arrWhere['work_candidate_id'] = $workCandidateID;
		$this->arrData['record'] = array();
		
		if($workID) {			
			$arrWhere['work_id'] = $workID;
		}
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'work_candidate_id' => $this->input->post("workCandidateID"),
								'work_company' => $this->input->post("workCompany"),
								'work_job_title' => $this->input->post("workJobTitle"),
								'work_from' => $this->input->post("workFrom"),
								'work_to' => $this->input->post("workTo"),
								'work_description' => $this->input->post("workDescription")
								);	
								
			if($workID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}							
								
			if($workID) {
				$this->recruitment->saveValues(TABLE_CANDIDATE_WORK_EXPERIENCE, $arrValues, $arrWhere);
			} else {
				$this->recruitment->saveValues(TABLE_CANDIDATE_WORK_EXPERIENCE, $arrValues);
			}						
				
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);			
			$this->recruitment->saveValues(TABLE_CANDIDATE, $arrValues, array('candidate_id' => $workCandidateID));
				
			$this->session->set_flashdata('success_message', 'Work history saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $workCandidateID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT WORK RECORD
		if($workID) {
			$this->arrData['record'] = $this->recruitment->getWorkHistory($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['workCandidateID'] = $workCandidateID;
		$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $workCandidateID));
		$this->arrData['arrRecords'] = $this->recruitment->getWorkHistory(array('work_candidate_id' => $workCandidateID));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/work_history', $this->arrData);
		$this->template->render();
	}
	
	public function candidate_vacancy($vacCandidateID = 0, $recordID = 0) {
		
		$tblName = TABLE_CANDIDATE_VACANCY;
		$tblNameCandidate = TABLE_CANDIDATE;
		$tblNameHistory = TABLE_CANDIDATE_HISTORY;
		$tblNameEmp = TABLE_EMPLOYEE;
		$tblNameEmpEdu = TABLE_EMPLOYEE_EDUCATION;
		$tblNameEmpWork = TABLE_EMPLOYEE_WORK_EXPERIENCE;
		$tblNameUser = TABLE_USER;
		
		$vacCandidateID = (int)$vacCandidateID;
		
		if(!$vacCandidateID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->recruitment->deleteValue($tblName, array('candidate_vacancy_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					$this->recruitment->deleteValue(TABLE_CANDIDATE_INTERVIEW, array('candidate_vacancy_id' => (int)$this->input->post("record_id")));
					echo YES; exit;
				}								
			}
		}		
		
		$recordID = (int)$recordID;
		$arrWhere['candidate_id'] = $vacCandidateID;
		$this->arrData['record'] = array();
		
		if($recordID) {			
			$arrWhere['candidate_vacancy_id'] = $recordID;
			$this->arrData['showForm'] = true;
		}
		
		# CODE FOR CURRENT RECORD
		if($recordID) {
			$this->arrData['record'] = $this->recruitment->getCandidateVacancies($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
			$curStatusID = $this->arrData['record']['status'];			
			$this->arrData['arrStatuses'] = $this->recruitment->getSubStatuses($curStatusID);
			
			if($curStatusID == STATUS_APPLICATION_INITIATED) {
				$arrCanVacancies = $this->recruitment->getCandidateVacancies(array('candidate_id' => $vacCandidateID));
				for($ind = 0; $ind < count($arrCanVacancies); $ind++) {
					if($arrCanVacancies[$ind]['status'] == STATUS_MOVED_TO_ANOTHER_VACANCY) {
						$this->arrData['arrStatuses'][] = array('status_id' => STATUS_JOB_OFFERED, 'status_title' => 'Job Offered');
						break;
					}
				}
			}
		}
		
		# CODE FOR PAGE LOADING	
		$this->arrData['invalidInterviewer'] = false;	
		$isInterviewer = false;
		
		if($this->arrData['showForm']) {			
			
			if(	$curStatusID != STATUS_SCHEDULE_INTERVIEW && 
				$curStatusID != STATUS_INTERVIEW_RESPONSE && 
				!in_array($this->userRoleID, $this->arrRoleIDs)
				) { # IF NOT INTERVIEW SCHEDULED OR RESPONSE EXPECTED
				
				redirect($this->baseURL . '/message/access_denied');
				exit;
				
			}
			
			$isInterviewer = true;
			$interviewID = $this->recruitment->getCandidateLastInterviewID($vacCandidateID, $this->arrData['record']['vacancy_id']);
			$interviewInterviewers = $this->recruitment->getInterviewers(array('interview_id' => $interviewID)); # INTERVIEWERS IDs
			$totalResponseReceived = $this->recruitment->getResponses(array( # TOTAL RESPONSES RECEIVED
																			'candidate_id' 	=> $vacCandidateID,
																			'vacancy_id' 	=> $this->arrData['record']['vacancy_id'],
																			'interview_id' 	=> $interviewID,
																			'status_id > ' 	=> STATUS_ON_HOLD,
																			'status_id < ' 	=> STATUS_INTERVIEW_CANCELED
																			));
			
			$invalidInterviewer = true;
			
			for($ind = 0; $ind < count($interviewInterviewers); $ind++){
				if($this->userEmpNum == $interviewInterviewers[$ind]['interviewer_id']) {
					$invalidInterviewer = false;
				}
			}			
			
			$this->arrData['invalidInterviewer'] = $invalidInterviewer;
			
			if(	$invalidInterviewer && 
				!in_array($this->userRoleID, $this->arrRoleIDs)
				) {  # IF NOT A VALID INTERVIEWER	
				
				redirect($this->baseURL . '/message/access_denied');
				exit;
				
			}
			
			$alreadyResponded = false;
			for($ind = 0; $ind < count($totalResponseReceived); $ind++){ # IF CURRENT INTERVIEWER HAS ALREADY SUBMITTED RESPONSE
				if($this->userEmpNum == $totalResponseReceived[$ind]['performed_by']) {
					$this->arrData['alreadyResponded'] = true;
					$this->arrData['invalidInterviewer'] = true;
				}
			}
			
			if(count($interviewInterviewers) == count($totalResponseReceived)) { # IF ALL RESPONSES RECEIVED
				$this->arrData['invalidInterviewer'] = false;				
			}
			
			if(isAdmin($this->userRoleID) && $this->arrData['invalidInterviewer']) {			
				$this->arrData['invalidInterviewer'] = false;
				$this->arrData['hideStatus'] = STATUS_INTERVIEW_RESPONSE; # HIDE RESPONSE OPTION
			}
			
			if(in_array($this->userRoleID, $this->arrData["forcedAccessRoles"])) {
				$this->arrData['invalidInterviewer'] = false;
				$this->arrData['hideStatus'] = '';
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('candidateID', 'Candidate ID', 'trim|required|numeric');
		$this->form_validation->set_rules('vacancyID', 'Vacancy ID', 'trim|required|numeric');
		$this->form_validation->set_rules('Status', 'Candidate Vacancy Status', 'trim|required|numeric');
		$this->form_validation->set_rules('Comments', 'Comments', 'trim|xss_clean');
		
		if($this->input->post("Status") == STATUS_INTERVIEW_RESPONSE) {		
			$this->form_validation->set_rules('Rating', 'Rating', 'trim|required|numeric');
			$this->form_validation->set_rules('Comments', 'Comments', 'trim|required|min_length[25]|xss_clean');
		}
		
		if((int)$this->input->post("generateLetter")) {	
			$this->form_validation->set_rules('joiningDate', 'Joining Date', 'trim|required|xss_clean');
			if($this->input->post("empContract") != 1) {
				$this->form_validation->set_rules('basicSalary', 'Basic Salary', 'trim|required|xss_clean');
			} else {
				$this->form_validation->set_rules('empDepartment', 'Department Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('contractMonths', 'Contact Months', 'trim|required|xss_clean');
			}
			$this->form_validation->set_rules('grossSalary', 'Gross Salary', 'trim|required|xss_clean');
			$this->form_validation->set_rules('signingAuthority', 'Signing Authority', 'trim|required|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
			
		if ($this->form_validation->run() == true) {
			
			$interviewRating = 0;
			$statusID = $this->input->post("Status");
			if($statusID == STATUS_INTERVIEW_RESPONSE) {
				$interviewRating = $this->input->post("Rating");
			}
			
			$allResponsesReceived = false;
			if(
				(count($interviewInterviewers) - 1) == count($totalResponseReceived) && 
				count($interviewInterviewers) && 
				$statusID == STATUS_INTERVIEW_RESPONSE) {
				$allResponsesReceived = true;
				$statusID = STATUS_RESPONSE_SUBMITTED;
			}
			
			$vacancyName = $this->arrData['record']['vacancy_name'];
			$vacancyID = $this->input->post("vacancyID");
			
			$pdfFileName = '';
			
			if((int)$this->input->post("generateLetter")) { # IF JOB OFFERED, PREPARE PDF FILE
			
				require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
				$isContactual = ($this->input->post("empContract") == 1);

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor(PDF_AUTHOR);
				$pdf->setCellHeightRatio(0.8);
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				
				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set auto page breaks
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				
				// set image scale factor
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				
				// set some language-dependent strings (optional)
				if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
					require_once(dirname(__FILE__).'/lang/eng.php');
					$pdf->setLanguageArray($l);
				}
				
				// ---------------------------------------------------------
								
				// add a page
				$pdf->AddPage();
				
				if(!$this->input->post("printerFriendly")) {
					// get the current page break margin
					$bMargin = $pdf->getBreakMargin();
					// disable auto-page-break
					$pdf->SetAutoPageBreak(false, 0);
					// set bacground imagev
					$img_file = $this->imagePath.'/offer-letter-bg-full.jpg';
					$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
					// restore auto-page-break status
					$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
					// set the starting point for the page content
					$pdf->setPageMark();
				}				
				
				$arrCandidate = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $vacCandidateID));
				// create some HTML content
				$html = '<div style="float:left; height:auto;width:100%">
	<table border="0" align="center" cellspacing="0" cellpadding="0" style="width:650px">
        <tr>
        	<td align="left" valign="top" style="color:#232323;font-family:Arial;font-size:12px;line-height:18px">
            	<table border="0" cellspacing="0" cellpadding="0" style="width:100%; font-size:13px">
                	<tr>
                        <td style="padding:0 0 15px">' . date('F d, Y', strtotime('now')) . '</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;padding:0 0 15px">' . ucwords($arrCandidate['first_name'] . ' ' . $arrCandidate['last_name']) . '<br />';
								
				if(!empty($arrCandidate['candidate_address'])) {			
                	$html .= $arrCandidate['candidate_address'] . ',<br />';
				}
				
				if((int)$arrCandidate['city']) {
					$strCityName = $this->configuration->getLocations(array('location_id' => $arrCandidate['city']));				
                	$html .= $strCityName[0]['location_name'];
				}
				
				$html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0 0 15px">
                            <br /><br />Subject: <span style="font-weight:bold;text-decoration:underline">';
				
				if($isContactual) {			
					$html .= 'Letter of Contract for the Position of "' . $vacancyName . '"';
				} else {
					$html .= 'Letter of Intent for the Position of "' . $vacancyName . '"';
				}
							
				$html .= '</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-weight:bold;padding:0 0 15px 0"><br />Dear ' . ucwords($arrCandidate['first_name']) . ',</span><br /><br />';
							if($isContactual) {		
							
								$html .= 'We are pleased to offer you employment on contract in our <span style="font-weight:bold">' . $this->input->post("empDepartment") . ' Department</span>, as "<span style="font-weight:bold;text-decoration:underline">' . $vacancyName . '</span>" effective from <span style="font-weight:bold">' . date('F d, Y', strtotime($this->input->post("joiningDate"))) . '</span>.<br /><br />Your contract tenure will be of ' . $this->input->post("contractMonths") . ' months. You will be entitled of monthly salary of <span style="font-weight:bold">Rs. ' . $this->numberFormatted($this->input->post("grossSalary"), 0) . '/-</span>. Deduction will be made as per company\'s policy and applicable income tax rules and provisions if there are any. However the company can extend the contract tenure based on your performance.<br />';

							} else {
								
								$html .= 'We are pleased to offer you the position of "<span style="font-weight:bold;text-decoration:underline">' . $vacancyName . '</span>" as discussed, we expect 
								you to join us from <span style="font-weight:bold">' . date('F d, Y', strtotime($this->input->post("joiningDate"))) . '.</span><br />
								Your total gross salary will be <span style="font-weight:bold">Rs. ' . $this->numberFormatted($this->input->post("grossSalary"), 0) . '/-</span> per month to subject deduction as per company\'s and applicable Income tax rules and provision.<br />';
								
							}
							
							
                $html .= '</td>
                    </tr>';
					
				if(!$isContactual) {
					
					$html .= '<tr>
							<td style="padding:0 0 20px">
								<table border="0" cellspacing="0" cellpadding="0" style=" font-weight:bold;width:600px;font-size:13px">';
								
									$html .= '<tr>
										<td style="width:400px">Basic Salary</td>
										<td style="width:50px">PKR.</td>
										<td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("basicSalary")) . '</td>
									</tr>';
								
					if($this->input->post("houseAllowance") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">House Rent Allowance</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("houseAllowance")) . '</td>
  								</tr>';
					}
					if($this->input->post("utilityAllowance") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">Utility Allowance</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("utilityAllowance")) . '</td>
  								</tr>';
					}
					if($this->input->post("fixedSalary") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">Fixed Salary</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("fixedSalary")) . '</td>
  								</tr>';
					}
					if($this->input->post("performanceSalary") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">*Performance Based Salary</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("performanceSalary")) . '</td>
  								</tr>';
					}
					if($this->input->post("languageAllowance") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">Language/Market Allowance</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("languageAllowance")) . '</td>
  								</tr>';
					}
					if($this->input->post("lhdAllowance") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">LHD Allowance</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("lhdAllowance")) . '</td>
  								</tr>';
					}
					if($this->input->post("nightAllowance") != '') {
                    	$html .= '<tr>
                                    <td style="width:400px">Night Shift Allowance</td>
                                    <td style="width:50px">PKR.</td>
                                    <td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("nightAllowance")) . '</td>
  								</tr>';
					}
                    
					$html .= '<tr>
                                    <td style="width:400px">&nbsp;</td>
                                    <td style="width:50px">&nbsp;</td>
                                    <td style="width:70px">&nbsp;</td>
  								</tr>';
								
					if(!$isContactual) {
						$html .= '<tr>
							<td style="width:400px">Gross Salary (payable monthly)</td>
							<td style="width:50px">PKR.</td>
							<td style="width:70px" align="right">' . $this->numberFormatted($this->input->post("grossSalary")) . '</td>
						</tr>';
					}
								
					$html .= '</table>
                        </td>
                    </tr>';
				}
				
				if($isContactual) {
						
					$html .= '<tr>
							<td style="padding:0 0 15px">
<<<<<<< .mine
								<br /><u>You will be required to have an account in Standard Chartered Bank (SCB) otherwise in case of not being an account holder at SCB, your salary will not be paid in Cash / Cheque.</u><br /><br />
								We welcome you to SBT Japan and hope it would be the beginning of a long and mutually beneficial association. Kindly acknowledge the duplicate copy of this letter as an acceptance of this offer.<br /><br />
								<span style="font-weight:bold">Please Note:</span> This offer is valid for one month from the date of issuance.
=======
								<br /><br />You will be entitled to any our benefits including leave policy. However the company can extend the contract tenure based on your performance.<br /><br />
								We welcome you to SBT Japan and hope it would be the beginning of a long and mutually beneficial association.
>>>>>>> .r1002
							</td>
						</tr>';
					
				} else {
						
					$html .= '<tr>
							<td style="padding:0 0 15px">
								<br /><br />The Terms and conditions of your employment will be specified in your letter of appointment, which will be issued to you on your acceptance of this offer and subsequent joining.<br /><br />
<<<<<<< .mine
								<u>You will be required to have an account in Standard Chartered Bank (SCB) otherwise in case of not being an account holder at SCB, your salary will not be paid in Cash / Cheque.</u><br /><br />
								We welcome you to SBT Japan and hope it would be the beginning of a long and mutually beneficial association. Kindly acknowledge the duplicate copy of this letter as an acceptance of this offer.<br /><br />
=======
								<br />
								We welcome you to SBT Japan and hope it would be the beginning of a long and mutually beneficial association.<br /><br />
								Kindly acknowledge the duplicate copy of this letter as an acceptance of this offer.<br />
>>>>>>> .r1002
								<span style="font-weight:bold">Please Note:</span> This offer is valid for one month from the date of issuance.
							</td>
						</tr>';
					
				}
					
                $html .= '<tr>
                        <td style="font-weight:bold;padding:20px 0"><br /><br />Sincerely,</td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-weight:bold">
                                <br /><br /><br />' . str_replace('#', '<br />', $this->input->post("signingAuthority")) . '
                            </span><br />
                            SBT Japan - Pakistan Office
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
	</table>
</div>';
				// output the HTML content
				$pdf->writeHTML(utf8_encode($html), true, 0, true, 0);
				$pdf->lastPage();
				$pdfFileName = 'offerletter_' . (int)$this->input->post("candidateID") . '_' . date('YmdHis') . '.pdf';
				$pdf->Output('./' . PDF_FILES_FOLDER . $pdfFileName, 'F');
			}
			
			$arrValues = array(
								'candidate_id' => $this->input->post("candidateID"),
								'vacancy_id' => $vacancyID,
								'status' => $statusID
								);
								
			$arrValuesHistory = array(
								'candidate_id' => $this->input->post("candidateID"),
								'vacancy_id' => $vacancyID,
								'status_id' => $statusID,
								'candidate_rating' => $interviewRating,
								'performed_by' => $this->userEmpNum,
								'performed_date' => date($this->arrData["dateTimeFormat"]),
								'note' => $this->input->post("Comments"), 
								'pdf_file_name' => $pdfFileName
								);
								
			if(($statusID == STATUS_INTERVIEW_RESPONSE or $statusID == STATUS_RESPONSE_SUBMITTED) and (int)$interviewID > 0) { # IF RESPONSE SUBMITTED
				$arrValuesHistory['interview_id'] = (int)$interviewID;
			}
			
			if(in_array($statusID, explode(',', ON_STATUS_SHOOT_EMAIL))) {
				$arrValuesHistory['email_sent'] = YES;
			}
			
			$this->recruitment->saveValues($tblName, $arrValues, $arrWhere);
			$this->recruitment->saveValues($tblNameHistory, $arrValuesHistory);
							
			$arrRecord = $this->recruitment->getCandidateVacancies($arrWhere);
			$arrRecord = $arrRecord[0];
				
			$arrCandidate = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $vacCandidateID));	
			
			$arrVacancy = $this->recruitment->getVacancies(array('v.vacancy_id' => $arrRecord['vacancy_id']));
			$arrVacancy = $arrVacancy[0];
			
			$arrCanStatuses = $this->recruitment->getCandidateStatuses();
						
			if($statusID == STATUS_HIRED) {
				
				# CREATE CANDIDATE AN EMPLOYEE				
				
				$arrValues = array(
									'modified_by' => $this->userEmpNum, 
									'modified_date' => date($this->arrData["dateTimeFormat"]), 									
									);
				$this->load->model('model_user_management', 'user_management', true);
				
				$this->recruitment->saveValues($tblNameCandidate, 
												$arrValues, 
												array('candidate_id' => $vacCandidateID));
				
				$arrCanEdu = $this->recruitment->getEducationalHistory(array('e.edu_candidate_id' => $vacCandidateID));
				$arrCanWork = $this->recruitment->getWorkHistory(array('work_candidate_id' => $vacCandidateID));				

				$arrValues = array(
									'emp_first_name' 			=> $arrCandidate['first_name'],
									'emp_last_name'			 	=> $arrCandidate['last_name'],
									'emp_email'			 		=> $arrCandidate['email'],
									'emp_full_name'			 	=> $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
									'emp_dob'				 	=> $arrCandidate['dob'],
									'emp_gender'	 			=> $arrCandidate['gender'],
									'emp_marital_status' 		=> $arrCandidate['marital_status'],
									'emp_nic_num' 				=> $arrCandidate['nic_number'],
									'emp_father_nic_num'		=> $arrCandidate['father_nic_number'],
									'emp_status' 				=> STATUS_ACTIVE,
									'emp_employment_status' 	=> STATUS_EMPLOYEE_PROBATION,
									'emp_job_title_id'			=> $arrVacancy['job_title_code'],
									'emp_city_id' 				=> $arrCandidate['city'],
									'emp_area_id' 				=> $arrCandidate['area'],
									'emp_country_id' 			=> $arrCandidate['country'],
									'emp_mobile' 				=> $arrCandidate['contact_number'],
									'emp_home_telephone' 		=> $arrCandidate['home_contact_number'],
									'emp_address' 				=> $arrCandidate['candidate_address'],
									'emp_cv_file_name' 			=> $arrCandidate['cv_file_name'],
									'created_by' 				=> $this->userEmpNum,
									'created_date' 				=> date($this->arrData["dateTimeFormat"])
									);	
									
				$empID = $this->employee->saveValues($tblNameEmp, $arrValues);	
				
				$this->user_management->saveValues(	$tblNameUser,
													array(	# UPDATE
															'user_role_id' => 2,
															'employee_id' => $empID, 
															'modified_by' => $this->userEmpNum, 
															'modified_date' => date($this->arrData["dateTimeFormat"])
														 ), 
													array(	# WHERE
															'candidate_id' => $arrCandidate['candidate_id']
														 )
												  );
				
				for($ind = 0; $ind < count($arrCanEdu); $ind++) {
					
					$arrValues = array(
									'emp_id' 					=> $empID,
									'edu_level_id'			 	=> $arrCanEdu[$ind]['edu_level_id'],
									'edu_institute'				=> $arrCanEdu[$ind]['edu_institute'],
									'edu_major_id'	 			=> $arrCanEdu[$ind]['edu_major_id'],
									'edu_gpa' 					=> ($arrCanEdu[$ind]['edu_gpa'] != FALSE) ? $arrCanEdu[$ind]['edu_gpa'] : NULL,
									'edu_ended'					=> $arrCanEdu[$ind]['edu_ended']
									);	
									
					$this->employee->saveValues($tblNameEmpEdu, $arrValues);	
				}
				
				for($ind = 0; $ind < count($arrCanWork); $ind++) {
					
					$arrValues = array(
									'emp_id' 					=> $empID,
									'work_company'			 	=> $arrCanWork[$ind]['work_company'],
									'work_job_title'			=> $arrCanWork[$ind]['work_job_title'],
									'work_from' 				=> $arrCanWork[$ind]['work_from'],
									'work_to' 					=> $arrCanWork[$ind]['work_to'],
									'work_description' 			=> $arrCanWork[$ind]['work_description']
									);	
									
					$this->employee->saveValues($tblNameEmpWork, $arrValues);	
				}
			}
			
			# SHOOT EMAIL
			# Template: candidate_status.html
			
			if(in_array($statusID, explode(',', ON_STATUS_SHOOT_EMAIL))) {
				
				$arrAttachment = array();
				if(
					(int)$this->input->post("generateLetter") && 
					(int)$this->input->post("sendLetter") && 
					file_exists('./' . PDF_FILES_FOLDER . $pdfFileName)
				) {
					$arrAttachment[] = './' . PDF_FILES_FOLDER . $pdfFileName;
				}
				
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[CURRENT_DATE]' => date('F d, Y', strtotime('now')),
									'[CANDIDATE_NAME]' => $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
									'[VACANCY_NAME]' => $arrVacancy['vacancy_name'],
									'[NEW_STATUS]' => getValue($arrCanStatuses, 'status_id', $statusID, 'status_title'),
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
				
				$emailTemplate = 'candidate_status.html';
				if($statusID == STATUS_OFFER_ACCEPTED) { # OFFER ACCEPTED
					$emailTemplate = 'candidate_job_offered.html';
				}
				
				$emailHTML = getHTML($arrValues, $emailTemplate);
				$this->sendEmail(
									$arrTo = array(
													$arrCandidate['email']
													), 				# RECEIVER DETAILS
									'Application Status Changed', 	# SUBJECT
									$emailHTML,						# EMAIL HTML MESSAGE
									HR_EMAIL_ADDRESS,
									HR_SENDER_NAME,
									$arrAttachment,					# ATTACHMENTS
									$this->sendMail					# DON'T SEND EMAIL
								);
			}
			
			if($statusID == STATUS_INTERVIEW_CANCELED) {
				if($arrCandidate['contact_number'] != '' && strlen($arrCandidate['contact_number']) >= 11) {
				
					# CALL SMS API
					
					$strNum = cleanNumberForSMS($arrCandidate['contact_number']);
					$apiResponse = $this->sendSMS(array($strNum), SMS_CANDIDATE_INTERVIEW_CANCELED, CONSTANT_RMS_SMS_MASK_NAME);
					if($apiResponse !== false) {
						$totalRejected = 0;
				
						$arrValues = array(
											'sender_id' => 0,
											'sms_sent_to' => $arrCandidate['candidate_id'],
											'sms_mask' => CONSTANT_RMS_SMS_MASK_NAME,
											'sms_message' => SMS_CANDIDATE_INTERVIEW_CANCELED,
											'total_sms_sent' => (1 - $totalRejected),
											'total_sms_rejected' => $totalRejected,
											'created_date' => date($this->arrData["dateTimeFormat"])
											);
						$this->recruitment->saveValues(TABLE_SENT_SMS_MESSAGES, $arrValues);
					}
				}
			}
			
			# SET LOG
				
			$this->session->set_flashdata('success_message', 'Vacancy history saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $vacCandidateID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['vacCandidateID'] = $vacCandidateID;
		$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $vacCandidateID));
		$this->arrData['arrCandidateHistory'] = $this->recruitment->getCandidateHistory(array('candidate_id' => $vacCandidateID));
		$this->arrData['arrCandidateInterviewHistory'] = $this->recruitment->getCandidateInterviewDetails($vacCandidateID);
		$this->arrData['arrCanStatuses'] = $this->recruitment->getCandidateStatuses();	
		$this->arrData['arrRecords'] = $this->recruitment->getCandidateVacancies(array('candidate_id' => $vacCandidateID));
		$this->arrData['arrEmpHR'] = $this->employee->getInterviewerEmployees($this->arrIntRoleIDs);
		$this->arrData['arrRatings'] = $this->config->item('interview_ratings');
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE));
		
		$arrWhere = array(
							'vacancy_from <= ' => date(DATE_TIME_FORMAT), 
							'vacancy_to >= ' => date(DATE_TIME_FORMAT), 
							'vacancy_status' => STATUS_ACTIVE
						);
		$this->arrData['arrVacancies'] = $this->recruitment->getAllVacancies($arrWhere);
		
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/candidate_vacancy', $this->arrData);
		$this->template->render();
	}
	
	public function schedule_interview($candidateID = 0, $candidateVacancyID = 0, $vacancyID = 0, $statusID = 0) {
		
		$tblNameCandidateVacancy = TABLE_CANDIDATE_VACANCY;
		$tblNameHistory = TABLE_CANDIDATE_HISTORY;
		$tblNameInterview = TABLE_CANDIDATE_INTERVIEW;
		$tblNameInterviewer = TABLE_CANDIDATE_INTERVIEW_INTERVIEWER;
		$candidateID = (int)$candidateID;
		
		if(!$candidateID or !$candidateVacancyID or !$vacancyID or !$statusID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		} else {
			$arrWhere['candidate_id'] = (int)$candidateID;
			$arrWhere['vacancy_id'] = (int)$vacancyID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('candidateID', 'Candidate', 'trim|required|numeric');
		$this->form_validation->set_rules('vacancyID', 'Vacancy', 'trim|required|numeric');
		$this->form_validation->set_rules('interviewName', 'Interview Title', 'trim|required|alpha_dash_space|min_length[5]|xss_clean');
		$this->form_validation->set_rules('interviewerName[]', 'Interviewer Name', 'trim|required|numeric');
		$this->form_validation->set_rules('interviewDate', 'Interview Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('interviewTime', 'Interview Time', 'trim|required|xss_clean');
		$this->form_validation->set_rules('Comments', 'Comments', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'candidate_vacancy_id' => $candidateVacancyID,
								'candidate_id' => $this->input->post("candidateID"),
								'interview_name' => $this->input->post("interviewName"),
								'interview_date' => $this->input->post("interviewDate"),
								'interview_time' => $this->input->post("interviewTime"),
								'note' => $this->input->post("Comments")
								);
			
			$interviewID = $this->recruitment->saveValues($tblNameInterview, $arrValues);
								
			$arrValues = array(
								'candidate_id' => $this->input->post("candidateID"),
								'vacancy_id' => $this->input->post("vacancyID"),
								'performed_by' => $this->userEmpNum,
								'performed_date' => date($this->arrData["dateTimeFormat"]),
								'interview_id' => $interviewID,
								'status_id' => $statusID,
								'note' => $this->input->post("Comments"),
								'email_sent' => YES
								);								
			
			$this->recruitment->saveValues($tblNameHistory, $arrValues);	
			
			$interviewerIDs = array_unique($this->input->post("interviewerName"));
			
			$arrCandidate = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $candidateID));
			$arrVacancy = $this->recruitment->getVacancies(array('v.vacancy_id' => $vacancyID));
			$arrVacancy = $arrVacancy[0];
			
			$arrTo = array();
			
			for($ind = 0; $ind < count($interviewerIDs); $ind++) {
								
				$arrValues = array(
									'interview_id' => $interviewID,
									'interviewer_id' => $interviewerIDs[$ind]
									);
												
				$this->recruitment->saveValues($tblNameInterviewer, $arrValues);
				
														
				$arrEmployee = $this->employee->getEmployees(array('emp_id' => $interviewerIDs[$ind]));
				$arrEmployee = $arrEmployee[0];
				$arrTo[] = ($arrEmployee['emp_work_email'] != '') ? $arrEmployee['emp_work_email'] : '';
				
			}		
			
			# SHOOT EMAIL
			# Template: interviewer_notification.html
			
			if(in_array($statusID, explode(',', ON_STATUS_SHOOT_EMAIL))) {
				
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[CANDIDATE_NAME]' => $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
									'[VACANCY_NAME]' => $arrVacancy['vacancy_name'],
									'[INTERVIEW_DATE]' => readableDate($this->input->post("interviewDate"), $this->arrData["showDateFormat"]),
									'[INTERVIEW_TIME]' => $this->input->post("interviewTime"),
									'[INTERVIEW_VENUE]' => PAKISTAN_OFFICE_ADDRESS,
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'interviewer_notification.html');
				if(count($arrTo)) {
					$this->sendEmail(
										$arrTo, 						# RECEIVER DETAILS
										'Interview Scheduled', 			# SUBJECT
										$emailHTML,						# EMAIL HTML MESSAGE
										HR_EMAIL_ADDRESS,
										HR_SENDER_NAME,
										array(),						# ATTACHMENTS
										$this->sendMail					# DON'T SEND EMAIL
									);
				}
			}
			
			$arrValues = array(
								'status' => $statusID
								);								
			
			$this->recruitment->saveValues($tblNameCandidateVacancy, $arrValues, $arrWhere);
			
			# SHOOT EMAIL
			# Template: candidate_interview.html
			
			if(in_array($statusID, explode(',', ON_STATUS_SHOOT_EMAIL))) {
									
				$currentUser = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum));
				$jobTitle = $this->employee->getValues(TABLE_JOB_TITLE, "job_title_name", array('job_title_id' => $currentUser['emp_job_title_id']));
					
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[CANDIDATE_NAME]' => $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
									'[VACANCY_NAME]' => $arrVacancy['vacancy_name'],
									'[INTERVIEW_DATE]' => readableDate($this->input->post("interviewDate"), $this->arrData["showDateFormat"]),
									'[INTERVIEW_TIME]' => $this->input->post("interviewTime"),
									'[INTERVIEW_VENUE]' => PAKISTAN_OFFICE_ADDRESS,
									'[ALTIJARAH_BUILDING_IMAGE_LINK]' => ALTIJARAH_BUILDING_IMAGE_LINK,
									'[ALTIJARAH_MAP_IMAGE_LINK]' => ALTIJARAH_MAP_IMAGE_LINK,
									'[HR_EMPLOYEE_NAME]' => $currentUser['emp_full_name'],
									'[HR_EMPLOYEE_DESIGNATION]' => $jobTitle[0]['job_title_name'],
									'[HR_EMPLOYEE_EMAIL]' => $currentUser['emp_work_email'],
									'[OFFICE_TELEPHONE_NUMBER]' => OFFICE_TELEPHONE_NUMBER,
									'[HR_EMPLOYEE_EXTENSION]' => $currentUser['emp_ip_num'],
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
				
				$emailHTML = getHTML($arrValues, 'candidate_interview.html');
				$arrRTDetails = explode(',', HR_RECRUITMENT_REPLY_TO);
					
				$this->sendEmail(
									$arrTo = array(
													$arrCandidate['email']
													), 				# RECEIVER DETAILS
									'Interview Notification', 		# SUBJECT
									$emailHTML,						# EMAIL HTML MESSAGE
									HR_EMAIL_ADDRESS,
									HR_SENDER_NAME,
									array(),						# ATTACHMENTS
									$this->sendMail,				# DON'T SEND EMAIL
									array($arrRTDetails[1] /* EMAIL ADDRESS */, $arrRTDetails[0] /* NAME */)
								);
								
				if($arrCandidate['contact_number'] != '' && strlen($arrCandidate['contact_number']) >= 11) {
					
					# CALL SMS API
					
					$strMessage = SMS_CANDIDATE_INTERVIEW;
					$strMessage = str_replace('[INTERVIEW_DATE_TIME]', $this->input->post("interviewTime") . ' on ' . readableDate($this->input->post("interviewDate"), $this->arrData["showDateFormat"]), $strMessage);
					
					$strNum = cleanNumberForSMS($arrCandidate['contact_number']);
					$apiResponse = $this->sendSMS(array($strNum), $strMessage, CONSTANT_RMS_SMS_MASK_NAME);
					if($apiResponse !== false) {
						$totalRejected = 0;
				
						$arrValues = array(
											'sender_id' => 0,
											'sms_sent_to' => $arrCandidate['candidate_id'],
											'sms_mask' => CONSTANT_RMS_SMS_MASK_NAME,
											'sms_message' => $strMessage,
											'total_sms_sent' => (1 - $totalRejected),
											'total_sms_rejected' => $totalRejected,
											'created_date' => date($this->arrData["dateTimeFormat"])
											);
						$this->recruitment->saveValues(TABLE_SENT_SMS_MESSAGES, $arrValues);
					}
				}
			}
					
			# SET LOG
								
			$this->session->set_flashdata('success_message', 'Interview Scheduled successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/candidate_vacancy/' . $candidateID);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT WORK RECORD
		if($candidateID and $vacancyID) {
			$this->arrData['record'] = $this->recruitment->getCandidateVacancies($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['totalInterviewers'] = ((int)$this->input->post("totalInterviewers") <= 0)? 1 : (int)$this->input->post("totalInterviewers");
		
		$this->arrData['candidateID'] = $candidateID;
		$this->arrData['arrIntTimings'] = $this->config->item('shift_timings');
		$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $candidateID));
		$this->arrData['arrStatuses'] = $this->recruitment->getCandidateStatuses();	
		$this->arrData['arrVacancies'] = $this->recruitment->getVacancies();
		$this->arrData['arrEmpInterviewers'] = $this->employee->getInterviewerEmployees($this->arrIntRoleIDs);		
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/schedule_interview', $this->arrData);
		$this->template->render();
	
	}
	
	public function save_vacancy($vacancyID) {
		
		$tblName = TABLE_VACANCY;
		
		#################################### FORM VALIDATION START ####################################	
			
		$this->form_validation->set_rules('jobTitleCode', 'Job Title', 'trim|required');
		$this->form_validation->set_rules('vacancyName', 'Vacancy Name', 'trim|required|alpha_dash_space|min_length[5]|xss_clean');
		$this->form_validation->set_rules('hiringManagerID', 'Hiring Manager', 'trim|required');
		$this->form_validation->set_rules('noOfPositions', 'Number of Positions', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('Country', 'Country', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('City', 'City', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('vacancyLevel', 'Experience Level', 'trim|required|xss_clean');
		$this->form_validation->set_rules('Description', 'Vacancy Description', 'trim|required|min_length[50]|xss_clean');
		$this->form_validation->set_rules('vacancyFrom', 'Vacancy Opening Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('vacancyTo', 'Vacancy Closing Date', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################		
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$vacancyID = (int)$vacancyID;		
		if($vacancyID) {			
			$arrWhere = array(
							'v.vacancy_id' => $vacancyID
							);
			$arrWhereSave = array(
							'vacancy_id' => $vacancyID
							);
		}
		
		if ($this->form_validation->run() == true) {
			
			$statusID = (int)$this->input->post("Status");
			if($statusID == STATUS_INACTIVE) {
				$statusID = STATUS_INACTIVE_VIEW;
			}
			$arrValues = array(
								'job_title_code' => $this->input->post("jobTitleCode"),
								'vacancy_name' => ucwords($this->input->post("vacancyName")),
								'hiring_manager_id' => $this->input->post("hiringManagerID"),
								'no_of_positions' => $this->input->post("noOfPositions"),
								'work_shift' => $this->input->post("workShift"),
								'vacancy_location' => $this->input->post("City"),
								'description' => $this->input->post("Description"),
								'vacancy_level' => $this->input->post("vacancyLevel"),
								'vacancy_status' => $statusID, 
								'vacancy_from' => $this->input->post("vacancyFrom"), 
								'vacancy_to' => $this->input->post("vacancyTo")
								);
			
			if($vacancyID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
			
			if($statusID == 2) {
				$arrValues['deleted_by'] = $this->userEmpNum;
				$arrValues['deleted_date'] = date($this->arrData["dateTimeFormat"]);
			}
								
			$this->recruitment->saveValues($tblName, $arrValues, $arrWhereSave);
			
			# SET LOG
				
			$this->session->set_flashdata('success_message', 'Vacancy saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_vacancy');
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT VACANCY RECORD
		if($vacancyID) {
			$this->arrData['record'] = $this->recruitment->getVacancies($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles(array('job_title_status' => STATUS_ACTIVE));
		$this->arrData['arrEmpHR'] = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE, 'e.emp_job_category_id' => HR_JOB_CATEGORY_ID), '', '', false);
		
		$this->arrData['Countries'] = $this->configuration->getLocations(array('location_type_id' => '2'));
		if((int)$this->arrData['record']['vacancy_location']) {
			$countryID = $this->configuration->getLocationParent(array('location_id' => (int)$this->arrData['record']['vacancy_location']));
			$this->arrData['countryID'] = (int)$countryID['location_parent_id'];
			$this->arrData['Cities'] = $this->configuration->getLocations(array('location_parent_id' => $this->arrData['countryID']));			
		}
		
		$this->arrData['arrWorkShifts'] = $this->configuration->getWorkShifts();
		$this->arrData['arrVacLevels'] = explode(',', EXPERIENCE_LEVELS);
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/save_vacancy', $this->arrData);
		$this->template->render();
	}
	
	public function list_vacancy($pageNum = 1) {
				
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$tblName = TABLE_VACANCY;
		$this->recruitment->saveValues($tblName, array('vacancy_status' => STATUS_EXPIRED), array('vacancy_to < ' => date(DATE_TIME_FORMAT)));
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->recruitment->deleteValue($tblName, array('vacancy_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					echo YES; exit;
				}								
			}
		}
		
		$arrWhere = array();
		if ($this->input->post()) {
			if($this->input->post("jobTitleCode")) {
				$arrWhere['v.job_title_code'] = $this->input->post("jobTitleCode");
				$this->arrData['jobTitleCode'] = $this->input->post("jobTitleCode");
			}
			if($this->input->post("hiringManagerID")) {
				$arrWhere['v.hiring_manager_id'] = $this->input->post("hiringManagerID");
				$this->arrData['hiringManagerID'] = $this->input->post("hiringManagerID");
			}
			if($this->input->post("dateFrom")) {
				$arrWhere['v.vacancy_from >= '] = $this->input->post("dateFrom");
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['v.vacancy_to <= '] = $this->input->post("dateTo");
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			if($this->input->post("Status")) {
				$status = $this->input->post("Status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['v.vacancy_status'] = $status;
				$this->arrData['Status'] = $this->input->post("Status");
			}
		}		
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['v.vacancy_status < '] = STATUS_DELETED;
		}
		
		# CODE FOR PAGING
		$totalCount = $this->recruitment->getVacancies($arrWhere);
		$totalCount = count($totalCount);		
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->recruitment->getVacancies($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);				
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListJobVacancy');		
		
		# CODE FOR PAGE LOADING
		if(isAdmin($this->userRoleID)) {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE);
		} else {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE, 'jt.job_category_id' => $this->userCategoryID);
		}
		
		$this->arrData['arrRoleIDs'] = $this->arrRoleIDs;
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles($arrWhereJobTitles);
		$this->arrData['arrEmpHR'] = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE, 'e.emp_job_category_id' => HR_JOB_CATEGORY_ID), '', '', false);
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/list_vacancy', $this->arrData);
		$this->template->render();
	
	}
	
	public function employee_requisition($requestID = 0) {
				
		if(isAdmin($this->userRoleID)) {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE);
		} else {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE, 'jt.job_category_id' => $this->userCategoryID);
		}
		
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles($arrWhereJobTitles);
		
		#################################### FORM VALIDATION START ####################################	
			
		$this->form_validation->set_rules('jobTitleCode', 'Job Title', 'trim|required');
		$this->form_validation->set_rules('startDate', 'Starting Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('eduRequired', 'Required Education', 'trim|required|xss_clean');
		$this->form_validation->set_rules('expRequired', 'Required Experience', 'trim|required|xss_clean');
		$this->form_validation->set_rules('jobDesc', 'Job Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('positionType', 'Type Of Position', 'trim|required|xss_clean');
		
		if($this->input->post('positionType') == 'No') {
			
			$this->form_validation->set_rules('empReplace', 'Person To Replace', 'trim|required|xss_clean');
			$this->form_validation->set_rules('replaceReason', 'Replacement Reason', 'trim|required|xss_clean');
			
		} else if($this->input->post('positionType') == 'Yes') {		
		
			$this->form_validation->set_rules('noOfPositions', 'Number Of Positions', 'trim|required|xss_clean');
			$this->form_validation->set_rules('empJustification', 'New Employee Justification', 'trim|required|xss_clean');
			
		}
		
		#################################### FORM VALIDATION END ####################################		
		if((int)$requestID) {
			$arrWhere = array(	'request_id' => $requestID, 
								'emp_id' => $this->userEmpNum, 
								'depthead_signed' => 0
							);
		}
		
		if ($this->form_validation->run() == true) {
			
			$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
			$arrValues = array(
								'emp_id' => $this->userEmpNum,
								'job_title' => $this->input->post("jobTitleCode"),
								'start_date' => $this->input->post("startDate"),
								'edu_required' => $this->input->post("eduRequired"),
								'exp_required' => $this->input->post("expRequired"),
								'job_description' => $this->input->post("jobDesc"),
								'position_type' => $this->input->post("positionType"),
								'emp_replace' => ($this->input->post("positionType") == 'No') ? $this->input->post("empReplace") : '',
								'replace_reason' => ($this->input->post("positionType") == 'No') ? $this->input->post("replaceReason") : '', 
								'no_of_positions' => ($this->input->post("positionType") == 'Yes') ? $this->input->post("noOfPositions") : '', 
								'request_justification' => ($this->input->post("positionType") == 'Yes') ? $this->input->post("empJustification") : '',
								'pdf_file' => $pdfFileName,
								'depthead_id' => $arrEmployee['emp_authority_id'],
								'gm_id' => $this->arrData["GMEmpID"],
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
			
			if((int)$requestID) {
				unset($arrValues['created_date']);
				$this->recruitment->saveValues(TABLE_EMPLOYEE_REQUISITION, $arrValues, $arrWhere);
			} else {
								
				$this->recruitment->saveValues(TABLE_EMPLOYEE_REQUISITION, $arrValues);
				
				# SHOOT EMAIL
				# Template: employee_request.html
				
				$requestedPosition = getValue($this->arrData['jobTitles'], 'job_title_id', $this->input->post("jobTitleCode"), 'job_title_name');
				$arrDHDetails = $this->employee->getEmployees(array('e.emp_id' => $arrEmployee['emp_authority_id']));
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[EMPLOYEE_NAME]' => $arrDHDetails[0]['emp_full_name'],
									'[REQUESTER_NAME]' => $arrEmployee['emp_full_name'],
									'[REQUESTED_POSITION]' => $requestedPosition,
									'[DASHBOARD_LINK]' => $this->baseURL . '/' . $this->currentController . '/requisitions_list',
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'employee_request.html');
				
				$this->sendEmail(
									$arrTo = array(
													$arrDHDetails[0]['emp_work_email'],
													SYSTEM_EMAIL_ADDRESS
													), 				# RECEIVER DETAILS
									'New ' . $requestedPosition . ' Request', 		# SUBJECT
									$emailHTML,						# EMAIL HTML MESSAGE
									SYSTEM_EMAIL_ADDRESS,
									SYSTEM_SENDER_NAME
									//$arrAttachment				# ATTACHMENTS
								);
			}
			
			# SET LOG
				
			$this->session->set_flashdata('success_message', 'Requisition saved successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/requisitions_list');			
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE LOADING		
		$strHierarchy = $this->employee->getHierarchy($this->userEmpNum);
		
		if((int)$requestID) {
			$this->arrData['arrRequest'] = $this->recruitment->getRequisitions($arrWhere);
			$this->arrData['arrRequest'] = $this->arrData['arrRequest'][0];
			if(!count($this->arrData['arrRequest'])) {
				redirect(base_url() . 'message/access_denied');
				exit;
			}
		}
		
		$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE, 'e.emp_authority_id in ' => '(' . $strHierarchy . ')'), '', '', false);
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/employee_requisition_form', $this->arrData);
		$this->template->render();
	
	}
	
	public function requisitions_list($pageNum = 1, $reqID = 0) {
				
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		if(isAdmin($this->userRoleID)) {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE);
		} else {
			$arrWhereJobTitles = array('jt.job_title_status' => STATUS_ACTIVE, 'jt.job_category_id' => $this->userCategoryID);
		}
		
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles($arrWhereJobTitles);
		
		if((int)$reqID) {
			
			$intType = $this->input->post('approvalVal' . $reqID);
			$strBy 	 = $this->input->post('userType' . $reqID);
			$strType = '';
			
			if($intType == 1) {
				$strType = 'Approved';
			} else if($intType == 2) {
				$strType = 'Rejected';
			}
			
			$arrRequest = $this->recruitment->getRequisitions(array('r.request_id' => $reqID));
			$arrRequest = $arrRequest[0];
			
			if(!isAdmin($this->userRoleID)) {
								
				if($this->userEmpNum != $arrRequest['depthead_id']) {
					redirect($this->baseURL . '/message/access_denied');
					exit;
				}
			}
			
			if($strBy == 'hr') {
				
				$pdfFileName = 'emp_request_letter_' . (int)$reqID . '.pdf';
					
					
				$arrValues = array(
									'pdf_file' 				=> $pdfFileName,
									'hr_no_of_applicants' 	=> $this->input->post("hr_no_of_applicants" . $reqID),
									'hr_no_of_interviews' 	=> $this->input->post("hr_no_of_interviews" . $reqID),
									'hr_no_of_hirings' 		=> $this->input->post("hr_no_of_hirings" . $reqID),
									'hrm_emp_id' 			=> $this->userEmpNum,
									'hr_signed_date' 		=> date($this->arrData["dateTimeFormat"])
								  );
				
				$this->recruitment->saveValues(TABLE_EMPLOYEE_REQUISITION, $arrValues, array('request_id' => $reqID)); 
				
				$arrRequest = $this->recruitment->getRequisitions(array('r.request_id' => $reqID));
				$arrRequest = $arrRequest[0];
			
				require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
	
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor(PDF_AUTHOR);
				$pdf->setCellHeightRatio(0.8);
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetFontSize(9);
				
				// set margins
				$pdf->SetMargins(5, 5, 5); // 	LEFT, TOP, RIGHT
				
				// set image scale factor
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				
				// set some language-dependent strings (optional)
				if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
					require_once(dirname(__FILE__).'/lang/eng.php');
					$pdf->setLanguageArray($l);
				}
				
				// ---------------------------------------------------------
								
				// add a page
				$pdf->AddPage();
				
				// get the current page break margin
				$bMargin = $pdf->getBreakMargin();
				// disable auto-page-break
				$pdf->SetAutoPageBreak(false, 0);
				// restore auto-page-break status
				$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
				// set the starting point for the page content
				$pdf->setPageMark();
				
				$arrEmployee = $this->employee->getEmployees(array('e.emp_id' => $arrRequest['emp_id']));
				$arrEmployee = $arrEmployee[0];
				$arrJobTitle = $this->configuration->getJobTitles(array('job_title_status' => 1));
				$requesterJT = getValue($arrJobTitle, 'job_title_id', $arrEmployee['emp_job_title_id'], 'job_title_name');
				$jobTitle = getValue($arrJobTitle, 'job_title_id', $arrRequest['job_title'], 'job_title_name');
				$jobDepartment = getValue($arrJobTitle, 'job_title_id', $arrRequest['job_title'], 'job_category_name');
				$requestedDate = ($arrRequest['created_date'] != '') ? readableDate($arrRequest['created_date'], 'F j, Y') : '';
				$startDate = ($arrRequest['start_date'] != '') ? readableDate($arrRequest['start_date'], 'F j, Y') : '';
				$dateDH = ($arrRequest['depthead_signed_date'] != '') ? readableDate($arrRequest['depthead_signed_date'], 'F j, Y') : '';
				$dateGM = ($arrRequest['gm_signed_date'] != '') ? readableDate($arrRequest['gm_signed_date'], 'F j, Y') : '';
				$dateHR = ($arrRequest['hr_signed_date'] != '') ? readableDate($arrRequest['hr_signed_date'], 'F j, Y') : '';
					
					$html = '
				<style type="text/css">
				<!--
				body {
					margin-left: 0px;
					margin-top: 0px;
					margin-right: 0px;
					margin-bottom: 0px;
					background-color:#E3E3E3;
				}
				-->
				</style>
				
				<body>
				
				<div style="float:left; height:auto;width:100%">
					<table border="0" align="center" cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;width:800px">
				<tr>
					<td align="left" valign="top" style="color:#232323;font-family:Arial;font-size:12px;line-height:22px; margin:30px 0 0;padding:50px">
						<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
										<tr>
											<td style="padding:0 0 20px;width:230px"><img src="'.EMAIL_HEADER_LOGO.'" alt="SBT Japan" /></td>
											<td style="font-weight:bold;font-size:16px;text-align:center;text-decoration:underline;width:250px;line-height:75px">EMPLOYEE REQUISITION FORM
											</td>
											<td style="width:200px">
												<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
													<tr>
														<td style="font-size:11px; line-height:15px; text-align:right; height:15px;width:200px">
															Ref #: <span style="color:#296CA6">SBTPK/ERF/1011/01</span>
														</td>
													</tr>
													<tr>
														<td style="font-size:11px; line-height:15px; text-align:right; height:15px">Version No: <span style="color:#296CA6">01</span></td>
													</tr>
													<tr>
														<td style="font-size:11px; line-height:15px; text-align:right; height:15px">Revision Date:<span style="color:#296CA6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
													</tr>
													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="padding:0 0 20px">
									<table border="1" cellspacing="0" cellpadding="0" style=" border:1px solid #999999;width:100%">
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; width:200px; font-size:12px">&nbsp;Date of Request:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; width:480px; font-size:12px">&nbsp;'.$requestedDate.'</td>
										</tr>
									   
										<tr>
											<td style="border-width:0 1px 0 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Requested By:</td>
											<td style="border-width:0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrEmployee['emp_full_name'] . ', ' . $requesterJT  .'</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="font-weight:bold;font-size:15px;padding:0 0 5px">&nbsp;Job Information:</td>
							</tr>
							<tr>
								<td style="padding:0 0 20px">
									<table border="1" cellspacing="0" cellpadding="0" style=" border:1px solid #999999;width:100%">
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; width:200px; font-size:12px">&nbsp;Job Title:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; width:480px; font-size:12px">&nbsp;' . $jobTitle .'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Department</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;' . $jobDepartment .'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Start Date:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$startDate.'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Education Requirement:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrRequest['edu_required'].'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Experience Required:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrRequest['exp_required'].'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Job Duties Description:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrRequest['job_description'].'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;New position:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrRequest['position_type'].'</td>
										</tr>
									</table>
								</td>
							</tr>';
							
							if($arrRequest['position_type'] == 'No') {
								
								$html .= '
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td style="font-weight:bold;font-size:15px;padding:0 0 5px">&nbsp;Replacement:</td>
								</tr>
								<tr>
									<td style="padding:20px 0">
										<table border="1" cellspacing="0" cellpadding="0" style=" border:1px solid #999999;width:100%">
											<tr>
												<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; width:200px; font-size:12px">&nbsp;Name of Person Being Replaced</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; width:480px; font-size:12px">&nbsp;'.$arrRequest['emp_replace'].'</td>
											</tr>
											<tr>
												<td style="border-width:0 1px 0 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Reason for Replacement</td>
												<td style="border-width:0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrRequest['replace_reason'].'</td>
											</tr>
										</table>
										<table width="100%">							
											<tr>
												<td>&nbsp;</td>
											</tr>
										</table>
										<table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0 0; width:100%">
											<tr>
												<td style="width:105px; font-size:12px">Department Head</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:150px">&nbsp;Signed</td>
												<td style="width:45px"></td>
												<td style="width:35px; font-size:12px">Sign:</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:150px">&nbsp;Signed</td>
												<td style="width:45px"></td>
												<td style="width:35px; font-size:12px">Date:</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:90px">&nbsp;'.$dateDH.'</td>
											</tr>
										</table>
									</td>
								</tr>';
								
							}
							
							if($arrRequest['position_type'] == 'Yes') {
							
								$html .= '
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td style="font-weight:bold;font-size:15px;padding:0 0 5px">&nbsp;New Position:</td>
								</tr>
								<tr>
								<td style="padding:20px 0">
									<table border="1" cellspacing="0" cellpadding="0" style=" border:1px solid #999999;width:100%">
											<tr>
												<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; width:200px; font-size:12px">&nbsp;Number of positions:</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; width:480px; font-size:12px">&nbsp;'.$arrRequest['no_of_positions'].'</td>
											</tr>
											<tr>
												<td style="border-width:0 1px 0 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Please Justify:</td>
												<td style="border-width:0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.$arrRequest['request_justification'].'</td>
											</tr>
										</table>
										<table width="100%">							
											<tr>
												<td>&nbsp;</td>
											</tr>
										</table>
										<table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0 0; width:100%">
											<tr>
												<td style="width:105px; font-size:12px">Department Head</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:150px">&nbsp;Signed</td>
												<td style="width:45px"></td>
												<td style="width:35px; font-size:12px">Sign:</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:150px">&nbsp;Signed</td>
												<td style="width:45px"></td>
												<td style="width:35px; font-size:12px">Date:</td>
												<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:90px">&nbsp;'.$dateDH.'</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>';
								
							}
							
							$html .= '
							<tr>
								<td>						
									<table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0 0; width:100%">
										<tr>
											<td style="font-size:12px">Approved By:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666">&nbsp;Signed</td>
											<td></td>
											<td>&nbsp;</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666"></td>
											<td></td>
											<td>&nbsp;</td>
											<td></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td style="padding:0 0 0 10px; font-size:12px">General Manager</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td style="padding:0 0 0 10px; font-size:12px">Director</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="font-weight:bold;font-size:14px;padding:4px 0;text-align:left">&nbsp;To be filled by HR</td>
							</tr>
							<tr>
								<td style="padding:20px 0">
									<table border="1" cellspacing="0" cellpadding="0" style=" border:1px solid #999999;width:100%">
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; width:200px; font-size:12px">&nbsp;Number of applicants:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; width:480px; font-size:12px">&nbsp;'.(int)$arrRequest['hr_no_of_applicants'].'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Applicants interviewed:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.(int)$arrRequest['hr_no_of_interviews'].'</td>
										</tr>
										<tr>
											<td style="border-width:0 1px 1px 0; border-style:dotted; border-color:#999999; padding:0 5px; font-size:12px">&nbsp;Applicants Hired:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#999999; color:#296CA6; padding:0 5px; font-size:12px">&nbsp;'.(int)$arrRequest['hr_no_of_hirings'].'</td>
										</tr>
									</table>
									<table width="100%">							
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>
									<table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0 0; width:100%">
										<tr>
											<td style="width:45px">&nbsp;</td>
											<td style="width:255px">&nbsp;</td>
											<td style="width:35px; font-size:12px">Sign:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:150px">&nbsp;Signed</td>
											<td style="width:45px">&nbsp;</td>
											<td style="width:35px; font-size:12px">Date:</td>
											<td style="border-width:0 0 1px 0; border-style:dotted; border-color:#666666;width:90px">&nbsp;'.$dateHR.'</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			</body>';
			//die($html);	
								
				// output the HTML content
				$pdf->writeHTML(utf8_encode($html), true, 0, true, 0);
				$pdf->lastPage();
				$pdf->Output('./' . PDF_FILES_FOLDER . $pdfFileName, 'F');
				
			} else {
				
				$arrValues = array(
									$strBy . '_signed' 		=> $intType,
									$strBy . '_signed_date' => date($this->arrData["dateTimeFormat"])
								  );
				$strEmail = '';
				$strReceiverName = '';
				$requestedPosition = getValue($this->arrData['jobTitles'], 'job_title_id', $arrRequest['job_title'], 'job_title_name');
				
				if($strBy == 'depthead' && $arrRequest['depthead_id'] == $arrRequest['gm_id']) {
				
					$arrValues = array(
										'depthead_signed' 		=> $intType,
										'depthead_signed_date' 	=> date($this->arrData["dateTimeFormat"]),
										'gm_signed' 			=> $intType,
										'gm_signed_date' 		=> date($this->arrData["dateTimeFormat"])
									  );
					$strEmail = HR_EMAIL_ADDRESS;
					$strReceiverName = 'HR';
					
				} else if($strBy == 'gm') {
					$strEmail = HR_EMAIL_ADDRESS;
					$strReceiverName = 'HR';
				} else if($strBy == 'depthead') {
					$arrGMDetails = $this->employee->getEmployees(array('e.emp_id' => $arrRequest['gm_id']));
					$strEmail = $arrGMDetails[0]['emp_work_email'];
					$strReceiverName = $arrGMDetails[0]['emp_full_name'];
				}
				
				$this->recruitment->saveValues(TABLE_EMPLOYEE_REQUISITION, $arrValues, array('request_id' => $reqID)); 
		
				if(!empty($strEmail)) {
					
					# SHOOT EMAIL
					# Template: employee_request.html
					
					$arrRTDetails = explode(',', HR_RECRUITMENT_REPLY_TO);
					$arrValues = array(
										'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
										'[EMPLOYEE_NAME]' => $strReceiverName,
										'[REQUESTER_NAME]' => getEmployeeName($arrRequest['emp_id']),
										'[REQUESTED_POSITION]' => $requestedPosition,
										'[DASHBOARD_LINK]' => $this->baseURL . '/' . $this->currentController . '/requisitions_list',
										'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'employee_request.html');
					
					$this->sendEmail(
										$arrTo = array(
														$strEmail,
														SYSTEM_EMAIL_ADDRESS,
														$arrRTDetails[1]
														), 				# RECEIVER DETAILS
										'New Employee Request', 		# SUBJECT
										$emailHTML,						# EMAIL HTML MESSAGE
										SYSTEM_EMAIL_ADDRESS,
										SYSTEM_SENDER_NAME
									);
				}
			}
							
			$this->session->set_flashdata('success_message', 'Request ' . $strType . ' Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->recruitment->deleteValue(TABLE_EMPLOYEE_REQUISITION, array('request_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					echo YES; exit;
				}								
			}
		}
		
		$arrWhere = array();
		if ($this->input->post()) {
			if($this->input->post("jobTitleCode")) {
				$arrWhere['r.job_title_code'] = $this->input->post("jobTitleCode");
				$this->arrData['jobTitleCode'] = $this->input->post("jobTitleCode");
			}
			if($this->input->post("empCode")) {
				$arrWhere['r.emp_id'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			if($this->input->post("positionType")) {
				$arrWhere['r.position_type'] = $this->input->post("positionType");
				$this->arrData['positionType'] = $this->input->post("positionType");
			}
			if($this->input->post("dateFrom")) {
				$arrWhere['r.created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['r.created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
		}		
		
		if(!isAdmin($this->userRoleID)) {
			$strHierarchy = $this->employee->getHierarchy($this->userEmpNum);
			$arrWhere['r.emp_id in '] = '(' . $strHierarchy . ')';
		}
		
		# CODE FOR PAGING
		$this->arrData['totalRecordsCount'] = $this->recruitment->getTotalRequisitions($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->recruitment->getRequisitions($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);				
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListRequisitions');		
		
		# CODE FOR PAGE LOADING
		
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		if(isAdmin($this->userRoleID)) {
			$this->arrData['arrEmployees'] = $this->employee->getSupervisors();
		} else {
			
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_id' => $this->userEmpNum));
			$this->arrData['arrEmployees'] = array_merge($this->arrData['arrEmployees'], $this->employee->getEmployees(array('e.emp_authority_id in ' => '(' . $strHierarchy . ')', 'u.user_role_id in ' => '('.INTERVIEWERS_ROLE_IDS.')'), '', '', false));
			
		}
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/employee_requisitions_list', $this->arrData);
		$this->template->render();
	
	}
	
	public function call_history($candidateID = 0) {
		
		if((int)$candidateID) {
			
			$arrWhere['cc.called_candidate_id'] = $candidateID;								
			$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $candidateID));
			$this->arrData['arrRecords'] = $this->recruitment->getCandidateCallsStats($arrWhere);	
		
			# TEMPLATE LOADING
			$this->template->write_view('content', 'recruitment_management/call_history', $this->arrData);
			$this->template->render();	
		
		} else {
			redirect(base_url() . 'message/access_denied');
			exit;
		}
	}
	
	public function call_candidate($candidateID = 0) {
		
		if((int)$candidateID) {
			
			$this->arrData['arrCandidate'] = $this->recruitment->getCandidateDetail(array('c.candidate_id' => $candidateID));
		}	
			
		#################################### FORM VALIDATION START ####################################
		$this->form_validation->set_rules('callID', 'Call ID', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('callRemarks', 'Call Remarks', 'trim|required|min_length[10]|xss_clean');
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$arrValues = array(
								'caller_remarks' => $this->input->post('callRemarks')
								);
			$arrWhere = array(
								'call_id' => $this->input->post('callID')
								);
								
			$this->recruitment->saveValues(TABLE_CANDIDATE_CALLS, $arrValues, $arrWhere);
							
			$this->session->set_flashdata('success_message', 'Remarks saved successfully<br />You may close the window');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $candidateID);
			
		} else {
			if($_POST) {
				$this->arrData['showRemarksBox'] = true;
			}
			$this->arrData['validation_error_message'] = validation_errors();
		}	
		
		$this->arrData['callNumber'] = $this->input->post('callNumber');
		$this->arrData['callID'] = $this->input->post('callID');
		$this->arrData['callRemarks'] = $this->input->post('callRemarks');
	
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/call_candidate_popup', $this->arrData);
		$this->template->render();	
		
	}
	
	public function import_candidates() {
		
		$this->load->model('model_user_management', 'user_management', true);
				
		#################################### FORM VALIDATION START ####################################
				
		$this->form_validation->set_rules('hiddenVal', 'Hidden Value', 'required');
		if (empty($_FILES['cFile']['name'])) {
			$this->form_validation->set_rules('cFile', 'CSV File', 'required');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$uploadConfig['upload_path'] = $this->arrData["resumeFolder"];
			$uploadConfig['allowed_types'] = 'csv';
			$uploadConfig['max_size']	= '1024';
			$uploadConfig['max_filename']	= '100';

			$this->load->library('upload', $uploadConfig);
			
			if(!$this->upload->do_upload('cFile')) {
				
				$error = array('error' => $this->upload->display_errors());		
				$this->arrData['error_message'] = $error['error'];
				
			} else {		
				
				$totalRecords = 0;
				$dataUpload = $this->upload->data();
				$fileName = $this->arrData["resumeFolder"] . basename($dataUpload['file_name']);
				
				set_time_limit(0);
				
				if ($fileHandler = fopen($fileName, "r")) {
				
					while (!feof($fileHandler) ) {
														
						$csvRow = fgetcsv($fileHandler);
						if(trim($csvRow[3]) != 'Email' && trim($csvRow[3]) != '') {
							
							$csvRow[3] = trim($csvRow[3]);
							$chkAlready = $this->recruitment->getTotalCandidates(array('c.email' => $csvRow[3]));
							if(!(int)$chkAlready) {
								
								$locDetails = $this->configuration->getLocationsSystem(array('l.location_name' => addslashes($csvRow[8]), 'l.location_type_id' => '3'));
								$locDetails = $locDetails[0];
								if((int)$locDetails['location_id']) {
									$cityID = $locDetails['location_id'];
									$countryID = $locDetails['location_parent_id'];
								} else {
									$cityID = 290;
									$countryID = 98;
								}
				
								$fullName = explode(' ', $csvRow[0], 2);
								$firstName = addslashes($fullName[0]);
								$lastName = addslashes($fullName[1]);								
								$nicNum = str_replace('-', '', addslashes($csvRow[1]));								
								$doBirth = explode('/', addslashes($csvRow[2]));
								$doBirth = $doBirth[2] . '-' . $doBirth[0] . '-' . $doBirth[1];
								
								
								$arrValues = array(
													'first_name' => $firstName,
													'last_name' => $lastName,
													'contact_number' => addslashes($csvRow[6]),
													'home_contact_number' => addslashes($csvRow[5]),
													'candidate_address' => addslashes($csvRow[7]),
													'email' => addslashes($csvRow[3]),
													'nic_number' => $nicNum,
													'dob' => $doBirth,
													'city' => $cityID,
													'country' => $countryID,
													'comment' => addslashes("* Academic\n" . $csvRow[9] . "\n\n* Worked As\n" . $csvRow[10] . "\n\n* Skills\n" . $csvRow[11] . "\n\n* Experience\n" . $csvRow[12]),
													'cv_file_name' => '', 
													'created_by' => '2', 
													'created_date' => date($this->arrData["dateTimeFormat"]), 
													'arrival_source' => 'Mustakbil'				
												);
												
								$candidateID = $this->recruitment->saveValues(TABLE_CANDIDATE, $arrValues);
								
								$userName = str_replace(' ', '', strtolower(substr($firstName . $lastName, 0, 6) . $candidateID));
								$passPhrase = randomString(8);
							
								$arrValues = array(
													'user_role_id' => 1,
													'candidate_id' => $candidateID,
													'user_name' => $userName,
													'plain_password' => $passPhrase,
													'password' => md5($passPhrase),
													'user_status' => STATUS_ACTIVE,
													'created_date' => date($this->arrData["dateTimeFormat"])
												);
									
											
								if($this->user_management->saveValues(TABLE_USER, $arrValues)) {
								
									if((int)$this->input->post('sendEmail')) {
										
										# SHOOT EMAIL
										# Template: new_candidate.html
										
										$arrValues = array(
															'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
															'[CANDIDATE_NAME]' => $firstName . ' ' . $lastName,
															'[CANDIDATE_DASHBOARD_LINK]' => $this->baseURL.'/career',
															'[CANDIDATE_USERNAME]' => $userName,
															'[CANDIDATE_PASSWORD]' => $passPhrase,
															'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
															);
															
										$emailHTML = getHTML($arrValues, 'new_candidate.html');									
										
										$this->sendEmail(
															$arrTo = array(
																			addslashes($csvRow[3])
																			), 				# RECEIVER DETAILS
															'Welcome To SBT Japan', 		# SUBJECT
															$emailHTML						# EMAIL HTML MESSAGE
														);
									}
													
									$totalRecords++;
									
								}
							}
						}
					}
					
					fclose($fileHandler);
				}
				
				# SET LOG
				
				$this->session->set_flashdata('success_message', $totalRecords . ' Candidates Saved Successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/list_candidate');
				exit;
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/import_candidates', $this->arrData);
		$this->template->render();
	}
	
	public function reporting($strMode = '', $pageNum = 1) {
		
		$this->arrData['arrFullReportAccess'] = array('', 'status');
		if(!in_array($this->userRoleID, $this->arrData["forcedAccessRoles"]) && !in_array($strMode, $this->arrData['arrFullReportAccess'])) {
			redirect(base_url() . 'message/access_denied');
			exit;
		}
		
		$arrWhere = array();
		if ($this->input->post()) {
			
			if($this->input->post("notApplied")) {
				$notApplied = (int)$this->input->post("notApplied");
				$this->arrData['notApplied'] = $notApplied;
			} else {
				if($this->input->post("jobTitleCode")) {
					$arrWhere['jv.job_title_code'] = $this->input->post("jobTitleCode");
					$this->arrData['jobTitleCode'] = $this->input->post("jobTitleCode");
				}
				if($this->input->post("vacancyName")) {
					$arrWhere['jv.vacancy_id'] = $this->input->post("vacancyName");
					$this->arrData['vacancyName'] = $this->input->post("vacancyName");
				}
				if($this->input->post("Status")) {
					$arrWhere['cv.status'] = $this->input->post("Status");
					$this->arrData['Status'] = $this->input->post("Status");
				}
				if($this->input->post("hiringManagerID")) {
					$arrWhere['jv.hiring_manager_id'] = $this->input->post("hiringManagerID");
					$this->arrData['hiringManagerID'] = $this->input->post("hiringManagerID");
				}
				if($this->input->post("dateInterview")) {
					$arrWhere['ji.interview_date'] = $this->input->post("dateInterview");
					$this->arrData['dateInterview'] = $this->input->post("dateInterview");
				}
				if($this->input->post("Interviewer")) {
					$arrWhere['jii.interviewer_id'] = $this->input->post("Interviewer");
					$this->arrData['Interviewer'] = $this->input->post("Interviewer");
				}
			}
			if($this->input->post("Gender")) {
				$arrWhere['c.gender'] = $this->input->post("Gender");
				$this->arrData['Gender'] = $this->input->post("Gender");
			}
			if($this->input->post("eduDegree")) {
				$arrWhere['e.edu_level_id'] = $this->input->post("eduDegree");
				$this->arrData['eduDegree'] = $this->input->post("eduDegree");
			}
			if($this->input->post("eduMajor")) {
				$arrWhere['e.edu_major_id'] = $this->input->post("eduMajor");
				$this->arrData['eduMajor'] = $this->input->post("eduMajor");
			}
			if($this->input->post("checkedBy")) {
				$arrWhere['c.resume_downloaded_by'] = $this->input->post("checkedBy");
				$this->arrData['checkedBy'] = $this->input->post("checkedBy");
			}
			if($this->input->post("dateFrom")) {
				$arrWhere['c.created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['c.created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			if($this->input->post("candEmail")) {
				$arrWhere['c.email'] = $this->input->post("candEmail");
				$this->arrData['candEmail'] = $this->input->post("candEmail");
			}
			if($this->input->post("Country")) {
				$arrWhere['c.country'] = $this->input->post("Country");
				$this->arrData['Country'] = $this->input->post("Country");
			}
			if($this->input->post("arrivalSource")) {
				$arrWhere['c.arrival_source'] = $this->input->post("arrivalSource");
				$this->arrData['arrivalSource'] = $this->input->post("arrivalSource");
			}
		}
		
		$candStatus = $this->input->post("candStatus");
		if($candStatus != '') {
			if($candStatus == STATUS_INACTIVE) {
				$candStatus = STATUS_INACTIVE_VIEW;
			}
			$arrWhere['u.user_status'] = $candStatus;
			$this->arrData['candStatus'] = $this->input->post("candStatus");
		}
		
		$loadPageContent = true;
				
		if($strMode == '') {
			
			$fileTemplate = 'reporting';
			$this->arrData['showCharts'] = false;
			
			if($this->arrData['showCharts']) {
				$this->arrData['arrEmpHR'] = $this->employee->getSupervisors();
				$this->arrData['vacancyTitles'] = $this->configuration->getVacancyTitles();
				
				$this->arrData['appliedCandidates'] = $this->recruitment->getAppliedCandidatesDateStat(10); # NUMBER OF DAYS			
				$this->arrData['appliedCandidates'] = array_reverse($this->arrData['appliedCandidates']);
				$this->arrData['sourceCandidates'] = $this->recruitment->getCandidatesSourceStat();
				$this->arrData['vacancyCandidates'] = $this->recruitment->getVacancyStat();
				$this->arrData['recruiterCandidates'] = $this->recruitment->getRecruitersStat(array('resume_downloaded_by > ' => 0));
				
				$totalRecords = 0;
				for($ind = 0; $ind < count($this->arrData['sourceCandidates']); $ind++) {
					$totalRecords += $this->arrData['sourceCandidates'][$ind]['total'];
				}
				
				$this->arrData['totalRecordsCount'] = $totalRecords;
			}
			
			$loadPageContent = false;
			
		} else if($strMode == 'daily') {
			
			$fileTemplate = 'day_happenings';
			$arrWhere = array();
			
			if($this->input->post("dateDay")) {
				$arrWhere['ch.performed_date >= '] = $this->input->post("dateDay") . ' 00:00:00';
				$arrWhere['ch.performed_date <= '] = $this->input->post("dateDay") . ' 23:59:59';
				$this->arrData['dateDay'] = $this->input->post("dateDay");
			} else {
				$arrWhere['ch.performed_date >= '] = date('Y-m-d') . ' 00:00:00';
				$arrWhere['ch.performed_date <= '] = date('Y-m-d') . ' 23:59:59';
				$this->arrData['dateDay'] = date('Y-m-d');
			}
			
			if($this->input->post("checkedBy")) {
				$arrWhere['ch.performed_by'] = $this->input->post("checkedBy");
				$this->arrData['checkedBy'] = $this->input->post("checkedBy");
			}
			
			$this->arrData['arrEmpHR'] = $this->employee->getSupervisors();
			$arrUsers = $this->recruitment->getDayUsers($arrWhere); # NUMBER OF DAYS
			$arrRecords = array();
			
			for($ind = 0; $ind < count($arrUsers); $ind++) {
				
				$arrWhere['ch.performed_by'] = $arrUsers[$ind]['performed_by'];
				$strKey = trim(getValue($this->arrData['arrEmpHR'], 'emp_id', $arrUsers[$ind]['performed_by'], array('emp_full_name')));
				if(empty($strKey)) {
					$strKey = 'Candidate';
				}
				
				$arrRecords[$strKey] = $this->recruitment->getDayHistory($arrWhere); # NUMBER OF DAYS
			}
			
			$this->arrData['arrRecords'] = $arrRecords;
			$loadPageContent = false;	
			
		} else if($strMode == 'manual') {
			
			$fileTemplate = 'manual_candidates';
			$arrWhere['c.arrival_source'] = 'System';
			
		} else if($strMode == 'online') {
			
			$fileTemplate = 'online_candidates';
			$arrWhere['c.arrival_source != '] = 'System';
						
		} else if($strMode == 'applicants') {
			
			$fileTemplate = 'applicants';
			$arrWhere = array();
			
			if($this->input->post("dateFrom")) {
				$arrWhere['created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			if(count($arrWhere) > 0) {
				$this->arrData['appliedCandidates'] = $this->recruitment->getAppliedCandidatesDateStat(0, $arrWhere); # NUMBER OF DAYS			
				$this->arrData['appliedCandidates'] = array_reverse($this->arrData['appliedCandidates']);
				
				$totalRecords = 0;
				for($ind = 0; $ind < count($this->arrData['appliedCandidates']); $ind++) {
					$totalRecords += $this->arrData['appliedCandidates'][$ind]['manual'] + $this->arrData['appliedCandidates'][$ind]['online'];
				}
				
				$this->arrData['totalRecordsCount'] = $totalRecords;
			}
			$loadPageContent = false;
						
		} else if($strMode == 'source') {
			
			$fileTemplate = 'source';
			$arrWhere = array();			
			
			if($this->input->post("dateFrom")) {
				$arrWhere['created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			if(count($arrWhere) > 0) {
				$this->arrData['sourceCandidates'] = $this->recruitment->getCandidatesSourceStat($arrWhere); # NUMBER OF DAYS	
				
				$totalRecords = 0;
				for($ind = 0; $ind < count($this->arrData['sourceCandidates']); $ind++) {
					$totalRecords += $this->arrData['sourceCandidates'][$ind]['total'];
				}
				
				$this->arrData['totalRecordsCount'] = $totalRecords;
			}
			$loadPageContent = false;
						
		} else if($strMode == 'status') {
			
			$fileTemplate = 'status';
			$arrWhere = array();			
			
			if($this->input->post("dateFrom")) {
				$arrWhere['performed_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['performed_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			if(count($arrWhere) > 0) {
				$this->arrData['statusCandidates'] = $this->recruitment->getCandidatesStatusStat($arrWhere); # NUMBER OF DAYS	
				
				$totalRecords = 0;
				for($ind = 0; $ind < count($this->arrData['statusCandidates']); $ind++) {
					$totalRecords += $this->arrData['statusCandidates'][$ind]['total'];
				}
				
				$this->arrData['totalRecordsCount'] = $totalRecords;
			}
			$loadPageContent = false;
						
		} else if($strMode == 'recruiter') {
			
			$fileTemplate = 'recruiter';
			$arrWhere = array();			
			
			if($this->input->post("dateFrom")) {
				$arrWhere['performed_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			if($this->input->post("dateTo")) {
				$arrWhere['performed_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			if($this->input->post("Status")) {
				$arrWhere['status_id'] = $this->input->post("Status");
				$this->arrData['Status'] = $this->input->post("Status");
			} else {
				$Status = STATUS_APPLICATION_INITIATED;
				$arrWhere['status_id'] = $Status;
				$this->arrData['Status'] = $Status;
			}
			$arrWhere['performed_by > '] = 0;
			
			$this->arrData['arrEmpHR'] = $this->employee->getEmployees(array('e.emp_job_category_id' => HR_JOB_CATEGORY_ID), '', '', false); # GET ALL OLD HR EMPLOYEES ALSO $this->employee->getSupervisors();
			$this->arrData['arrRecords'] = $this->recruitment->getStatusStats($arrWhere);
			$loadPageContent = false;	
			
		} else if($strMode == 'calls') {
			
			$fileTemplate = 'candidate_calls';
			$arrWhere = array();
			
			if($this->input->post("empID")) {
				$arrWhere['cc.caller_emp_id'] = $this->input->post("empID");
				$this->arrData['empID'] = $this->input->post("empID");
			}
			
			if($this->input->post("calledTo")) {
				$arrWhere['cc.called_candidate_id'] = 0;
				$this->arrData['calledTo'] = $this->input->post("calledTo");
			}
			
			if($this->input->post("dateFrom")) {
				$arrWhere['cc.call_datetime >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			
			if($this->input->post("dateTo")) {
				$arrWhere['cc.call_datetime <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			
			$this->arrData['totalRecordsCount'] = $this->recruitment->getCandidateCallsStatsTotal($arrWhere);
			$offSet = ($pageNum - 1) * $this->limitRecords;
			$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
			$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/' . $strMode . '/#/', 'frmListCandidates');
			
			$this->arrData['arrRecords'] = $this->recruitment->getCandidateCallsStats($arrWhere, $this->limitRecords, $offSet);
			$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $strMode;
			
			$loadPageContent = false;	
			
		} else if($strMode == 'employee_calls') {
			
			$fileTemplate = 'employee_calls';
			$arrWhere = array();
			
			if($this->input->post("empID")) {
				$arrWhere['ec.caller_emp_id'] = $this->input->post("empID");
				$this->arrData['empID'] = $this->input->post("empID");
			}
			
			if($this->input->post("calledTo")) {
				$arrWhere['ec.called_candidate_id'] = 0;
				$this->arrData['calledTo'] = $this->input->post("calledTo");
			}
			
			if($this->input->post("dateFrom")) {
				$arrWhere['ec.call_datetime >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			
			if($this->input->post("dateTo")) {
				$arrWhere['ec.call_datetime <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();			
			$this->arrData['totalRecordsCount'] = $this->employee->getEmployeeCallsStatsTotal($arrWhere);
			$offSet = ($pageNum - 1) * $this->limitRecords;
			$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
			$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/' . $strMode . '/#/', 'frmListEmployeeCalls');
			
			$this->arrData['arrRecords'] = $this->employee->getEmployeeCallsStats($arrWhere, $this->limitRecords, $offSet);
			$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $strMode;
			
			$loadPageContent = false;	
			
		} else if($strMode == 'referrer') {
			
			$fileTemplate = 'sbt_emp_referrer';
			$arrWhere = array();
			
			$arrWhere['c.arrival_source'] = CONSTANT_REFERRED_BY_SBT_EMPLOYEE;
			$arrWhere['e.emp_code != '] = '';
			
			if($this->input->post("empCode")) {
				$arrWhere['c.reference'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("dateFrom")) {
				$arrWhere['created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			
			if($this->input->post("dateTo")) {
				$arrWhere['created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
			
			
			$this->arrData['totalRecordsCount'] = count($this->recruitment->getReferredCandidates($arrWhere));
			$offSet = ($pageNum - 1) * $this->limitRecords;
			$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
			$this->arrData['arrRecords'] = $this->recruitment->getReferredCandidates($arrWhere, $this->limitRecords, $offSet);
			$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/' . $strMode . '/#/', 'frmListReferrer');
			
			for($ind = 0; $ind < count($this->arrData['arrRecords']); $ind++) {
				
				$canHired = 0;
				$canConfirmed = 0;
				$arrHiredConfirmed = $this->recruitment->getHiredConfirmedCandidates(array('c.reference' => $this->arrData['arrRecords'][$ind]['emp_code']));
				
				for($jnd = 0; $jnd < count($arrHiredConfirmed); $jnd++) {
					
					if((int)$arrHiredConfirmed[$jnd]['employee_id']) {
						$canHired++;						
					}
					if((int)$arrHiredConfirmed[$jnd]['emp_employment_status'] == STATUS_EMPLOYEE_CONFIRMED) {
						$canConfirmed++;						
					}
				}
				
				$this->arrData['arrRecords'][$ind]['total_hired'] = $canHired;
				$this->arrData['arrRecords'][$ind]['total_confirmed'] = $canConfirmed;
			}
			
			$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $strMode;
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE));
			
			$loadPageContent = false;	
			
		} else {
			redirect($this->baseURL . '/message/access_denied');
			exit;
		}
		
		if($loadPageContent) {
			# CODE FOR PAGING
			$this->arrData['totalRecordsCount'] = $this->recruitment->getTotalCandidates($arrWhere, $notApplied);
			$offSet = ($pageNum - 1) * $this->limitRecords;
			$this->arrData['arrRecords'] = $this->recruitment->getCandidates($arrWhere, $this->limitRecords, $offSet, $notApplied);
			$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
			$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/' . $strMode . '/#/', 'frmListCandidates');
			
			# CODE FOR PAGE CONTENT
			$this->arrData['Genders'] = $this->config->item('genders');
			$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => 1));
			$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => 1));
			$this->arrData['jobTitles'] = $this->configuration->getJobTitles(array('job_title_status' => STATUS_ACTIVE));		
			$this->arrData['vacancyTitles'] = $this->configuration->getVacancyTitles(array('jv.vacancy_status' => STATUS_ACTIVE));
			$this->arrData['arrEmpHR'] = $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE, 'e.emp_job_category_id' => HR_JOB_CATEGORY_ID), '', '', false);
			$this->arrData['arrLocations'] = $this->configuration->getLocations();
			$this->arrData['arrInterviewers'] = $this->employee->getSupervisors();	
			$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $strMode;
		}
				
		$this->arrData['arrCanStatuses'] = $this->recruitment->getCandidateStatuses();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/reports/' . $fileTemplate, $this->arrData);
		$this->template->render();
	}
	
	public function candidates_referred($empCode = 0, $strMode = 0) {
		
		if((int)$empCode) {
			
			$arrWhere['c.arrival_source'] 				= CONSTANT_REFERRED_BY_SBT_EMPLOYEE;
			$arrWhere['c.reference']	 				= $empCode;
			
			if((int)$strMode == 1) { # HIRED
				$arrWhere['u.employee_id > '] 			= 0;
			}
			if((int)$strMode == 2) { # CONFIRMED
				$arrWhere['e.emp_employment_status'] 	= STATUS_EMPLOYEE_CONFIRMED;
			}
			
			$this->arrData['arrCandidate'] 	= $this->recruitment->getReferredCandidates($arrWhere, null, null, false);
				
			# TEMPLATE LOADING
			$this->template->write_view('content', 'recruitment_management/reports/candidates_referred', $this->arrData);
			$this->template->render();	
		
		} else {
			redirect(base_url() . 'message/access_denied');
			exit;
		}
	}
	
	public function stats($strMode = '') {		
		
		if($strMode == '') {
			
			$fileTemplate = 'stats';
			
		} else if($strMode == 'source') {
			
			$fileTemplate = 'source';
			$arrWhere = array();
			
			#################################### FORM VALIDATION START ####################################
				
			$this->form_validation->set_rules('arrivalSource', 'Arrival Source', 'required');
			$this->form_validation->set_rules('dateFrom', 'Date From', 'required');
			$this->form_validation->set_rules('dateTo', 'Date To', 'required');
			
			#################################### FORM VALIDATION END ####################################
		
			if ($this->form_validation->run() == true) {
			
				if($this->input->post("dateFrom")) {
					$arrWhere['created_date >= '] = $this->input->post("dateFrom") . ' 00:00:00';
					$this->arrData['dateFrom'] = $this->input->post("dateFrom");
				}
				if($this->input->post("dateTo")) {
					$arrWhere['created_date <= '] = $this->input->post("dateTo") . ' 23:59:59';
					$this->arrData['dateTo'] = $this->input->post("dateTo");
				}
				if($this->input->post("arrivalSource")) {				
					$arrWhere['arrival_source'] = $this->input->post("arrivalSource");				
					$this->arrData['arrivalSource'] = $this->input->post("arrivalSource");
				}
				
				if(count($arrWhere) > 0) {
					$this->arrData['sourceCandidates'] = $this->recruitment->getCandidatesSourceStatsWebMarketing($arrWhere); # NUMBER OF DAYS	
					$totalRecords = 0;
					for($ind = 0; $ind < count($this->arrData['sourceCandidates']); $ind++) {
						if($this->arrData['sourceCandidates'][$ind]['arrival_source_counts'] > 0) {
							$totalRecords += $this->arrData['sourceCandidates'][$ind]['arrival_source_counts'];
						}
					}
					
					$this->arrData['totalRecordsCount'] = $totalRecords;
				}
			
			} else {	
				$this->arrData['validation_error_message'] = validation_errors();
			}
			
			$this->arrData["arrArrivalSource"]		= $this->config->item('arrival_source_web_marketing');
						
		}
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'recruitment_management/stats/' . $fileTemplate, $this->arrData);
		$this->template->render();
	}
	
	public function strValidate($strValue, $strField) {
		
		if($strField == 'contactNo') { 
			$this->form_validation->set_message('strValidate', 'Cell Phone Number field must only contain numbers.');
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'homePhoneNo') { 
			$this->form_validation->set_message('strValidate', 'Home Phone Number field must only contain numbers.');
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'nic') {
			$this->form_validation->set_message('strValidate', 'NIC Number field must only contain numbers.');
			return (preg_match("/^[0-9]*$/u", $strValue)) ? true : false;
		}
		return false;
		
 	}
	
	private function numberFormatted($intNum = 0, $intDecimals = 2) {
		
		$intNum = str_replace(',', '', $intNum);
		return number_format($intNum, $intDecimals);
		
	}
	
	private function setupStatuses() {
		
		$arrStatuses = $this->recruitment->getCandidateStatuses();
		for($ind = 0; $ind < count($arrStatuses); $ind++) {
			if(!defined($arrStatuses[$ind]['status_key'])) {
				define($arrStatuses[$ind]['status_key'], $arrStatuses[$ind]['status_id']);
			}
		}
	}
}

/* End of file recruitment_management.php */
/* Location: ./application/controllers/recruitment_management.php */