<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller {
	 
	public $baseURL = '';
	public $arrData = array();
	public $currentController;
	private $maxLinks;
	private $limitRecords;
	
	function __construct() {
		
		parent::__construct();
		
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Karachi");
		
		$this->baseURL = $this->config->item("base_url");
		$this->load->model('model_master', 'master', true);
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->setupConfig($this->configuration->getSettings()); # SETUP ALL SYSTEM WIDE VARIABLES REQUIRED IN CODE (TABLE hrm_configuration)
		$this->setupEmail(); # SETUP EMAIL ATTRIBUTES IF SMTP DETAILS ARE AVAILABLE
		
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_recruitment_management', 'recruitment', true);
		$this->load->model('model_user_management', 'user_management', true);
		
		$this->load->library(array('session', 'form_validation'));	
		$this->load->helper(array('url', 'common', 'html'));
		
		$userID = (int)$this->session->userdata('user_id');		
		
		if($userID) {
			redirect($this->baseURL . '/welcome');
			exit;
		}
		
		$this->arrData["baseURL"] 				= $this->config->item("base_url");
		$this->arrData["imagePath"] 			= $this->config->item("image_path");
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["resumeFolder"]			= './' . RESUME_FOLDER . '/';
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->currentController 				= 'career';
		
		$this->arrData["arrArrivalSource"]		= explode(',', ARRIVAL_SOURCES);
		$this->arrData["arrArrivalSource"][]	= CONSTANT_REFERRED_BY;
		$this->arrData["arrArrivalSource"][]	= CONSTANT_REFERRED_BY_SBT_EMPLOYEE;
		$this->arrData["arrArrivalSource"][]	= CONSTANT_LAST_ARRIVAL_SOURCE;
		
		$this->template->set_template("career");
		$this->template->write_view('header', 'templates/header_career');
		$this->template->write_view('footer', 'templates/footer');
	}
	
	public function index($pageNum = 1)	{
				
		$tblName = TABLE_VACANCY;
		
		$arrWhere = array(
							'vacancy_from <= ' => date(DATE_TIME_FORMAT), 
							'vacancy_to >= ' => date(DATE_TIME_FORMAT), 
							'vacancy_status' => 1
						);
		
		# CODE FOR PAGING
		$totalCount = $this->recruitment->getAllVacancies($arrWhere);
		$totalCount = count($totalCount);		
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->recruitment->getAllVacancies($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);				
		$this->arrData['pageLinks'] = displayLinks($numPages, $this->maxLinks, $pageNum, $this->currentController . '/index/#');		
		
		# CODE FOR PAGE LOADING
		$this->arrData['arrLocations'] = $this->configuration->getLocations();
		
		# Redirected from Welcome Controller - On Candidate Login Failure
		if($_GET["fail"] == 1)
		{
			$this->arrData['error_message'] = $this->session->userdata('error_message');
			$this->arrData['validation_error_message'] = $this->session->userdata('validation_error_message');
		}
		
		# TEMPLATE LOADING		
		$this->template->write_view('content', 'career/vacancies', $this->arrData);
		$this->template->render();
	}
	
	public function getVacancies()	{
		
		if((int)$_SERVER['HTTP_IS_FROM_HRMS'] == 1) {
			
			$arrWhere = array(
								'vacancy_from <= ' => date(DATE_TIME_FORMAT), 
								'vacancy_to >= ' => date(DATE_TIME_FORMAT), 
								'vacancy_status' => 1
							);
			
			echo json_encode($this->recruitment->getAllVacancies($arrWhere));
			
		} else {
			die('NO ACCESS ALLOWED');
		}
		
		exit;
	}
	
	public function apply($jobID) {
		
		if((int)$jobID) {
			$this->arrData['vacancy'] = $this->recruitment->getAllVacancies(array('v.vacancy_id' => (int)$jobID, 'v.vacancy_status' => 1));
			$this->arrData['vacancy'] = $this->arrData['vacancy'][0];
			
			# IF INACTIVE VACANCY OPENED THROUGH URL
			if(!count($this->arrData['vacancy'])) {
				redirect($this->baseURL . '/' . $this->currentController);
				exit;
			}
		} else {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		#################################### FORM VALIDATION START ####################################
		
		$this->form_validation->set_rules('firstName', 'First Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('eMail', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('contactNo', 'Cell Phone Number', 'trim|xss_clean|callback_strValidate[contactNo]');
		$this->form_validation->set_rules('homePhoneNo', 'Home Phone Number', 'trim|xss_clean|callback_strValidate[homePhoneNo]');
		$this->form_validation->set_rules('candidateAddress', 'Address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('nicNo', 'NIC Number', 'trim|required|numeric|min_length[13]|max_length[15]|xss_clean');
		$this->form_validation->set_rules('fatherNICNo', 'Father\'s NIC Number', 'trim|numeric|exact_length[13]|xss_clean');
		$this->form_validation->set_rules('Gender', 'Gender', 'trim|required|xss_clean');
		$this->form_validation->set_rules('maritalStatus', 'Marital Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dateOfBirth', 'Date of Birth', 'trim|required|xss_clean');
		$this->form_validation->set_rules('Country', 'Country', 'trim|required|xss_clean');
		$this->form_validation->set_rules('City', 'City', 'trim|required|xss_clean');
		
		if(isset($_POST['Area'])){	
			$this->form_validation->set_rules('Area', 'Area', 'trim|required|numeric|xss_clean');
		}
		
		$errorFree = true;
		if($_FILES['Resume']['error'] > 0) {
			$this->form_validation->set_rules('Resume', 'Resume', 'required');
			$errorFree = false;
		}
		
		$this->form_validation->set_rules('Comments', 'Cover Letter', 'trim|xss_clean');
		$this->form_validation->set_rules('arrivalSource', 'How Did You Reach Us', 'trim|required|xss_clean');
		
		if($this->input->post("arrivalSource") == CONSTANT_LAST_ARRIVAL_SOURCE || $this->input->post("arrivalSource") == CONSTANT_REFERRED_BY) {
			$this->form_validation->set_rules('arrivalSourceOther', 'How Did You Reach Us', 'trim|required|min_length[3]|xss_clean');
		} else if($this->input->post("arrivalSource") == CONSTANT_REFERRED_BY_SBT_EMPLOYEE) {
			$this->form_validation->set_rules('arrivalSourceOther', 'How Did You Reach Us - ' . CONSTANT_REFERRED_BY_SBT_EMPLOYEE, 'trim|required|numeric|exact_length[4]|xss_clean');
		}		
		
		$this->form_validation->set_rules('eduLevel[]', 'Qualification Level', 'trim|required|xss_clean');
		$this->form_validation->set_rules('eduInstitute[]', 'Educational Institute', 'trim|required|xss_clean');
		$this->form_validation->set_rules('eduMajors[]', 'Majors', 'trim|required|xss_clean');
		$this->form_validation->set_rules('eduGPA[]', 'GPA/Grade', 'trim|xss_clean');
		$this->form_validation->set_rules('eduEnded[]', 'Passing Year', 'trim|required|min_length[4]|xss_clean');
		#$this->form_validation->set_rules('eduStatus[]', 'Education Status', 'trim|required|xss_clean');
		#$this->form_validation->set_rules('eduFrom[]', 'Education Starting Date', 'trim|xss_clean');
		
		if((int)$this->input->post("noExp") <= 0 ) {
			$this->form_validation->set_rules('workCompany[]', 'Company Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('workJobTitle[]', 'Job Title', 'trim|required|alpha_dash_space|xss_clean');
			$this->form_validation->set_rules('workFrom[]', 'Starting Date', 'trim|required|xss_clean');
			$this->form_validation->set_rules('workTo[]', 'Ending Date', 'trim|xss_clean');
			$this->form_validation->set_rules('workDescription[]', 'Description', 'trim|max_length[1000]|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true && $errorFree) {
			
			# CODE FOR RESUME UPLOAD
			$chkEmail 	= $this->recruitment->notExistingCandidate('email', $this->input->post("eMail"));
			$chkNIC 	= $this->recruitment->notExistingCandidate('nic_number', $this->input->post("nicNo"));
			$chkFamily 	= (int)count($this->employee->getEmployees(array('emp_status' => STATUS_ACTIVE, 
															  'emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED, 
															  'emp_father_nic_num' => $this->input->post("fatherNICNo"))));
			
			if($chkEmail && $chkNIC && !$chkFamily) {
				
				$uploadConfig['upload_path'] = $this->arrData["resumeFolder"];
				$uploadConfig['allowed_types'] = 'rtf|doc|docx|pdf';
				$uploadConfig['max_size']	= '1024';
				$uploadConfig['max_filename']	= '100';
				$uploadConfig['file_name']	= strtolower($this->input->post("firstName") . '_' . $this->input->post("lastName")) . '_' . date('dmYHis');
	
				$this->load->library('upload', $uploadConfig);
				
				if(!$this->upload->do_upload('Resume')) {
					$error = array('error' => $this->upload->display_errors());		
					$this->arrData['error_message'] = $error['error'];
				} else {		
					$dataUpload = $this->upload->data();
					$fileName = basename($dataUpload['file_name']);
				
					# CODE FOR INSERT CANDIDATE
					
					$arrivalSourceReference = '';
					$arrivalSource = $this->input->post("arrivalSource");
					if($arrivalSource == CONSTANT_LAST_ARRIVAL_SOURCE) {
						$arrivalSource = $this->input->post("arrivalSourceOther");
					}
					if($arrivalSource == CONSTANT_REFERRED_BY || $arrivalSource == CONSTANT_REFERRED_BY_SBT_EMPLOYEE) {
						$arrivalSourceReference = $this->input->post("arrivalSourceOther");
					}
					
					$arrValues = array(
										'first_name' => $this->input->post("firstName"),
										'last_name' => $this->input->post("lastName"),
										'contact_number' => $this->input->post("contactNo"),
										'home_contact_number' => $this->input->post("homePhoneNo"),
										'candidate_address' => $this->input->post("candidateAddress"),
										'email' => $this->input->post("eMail"),
										'nic_number' => $this->input->post("nicNo"),
										'father_nic_number' => $this->input->post("fatherNICNo"),
										'gender' => $this->input->post("Gender"),
										'marital_status' => $this->input->post("maritalStatus"),
										'dob' => $this->input->post("dateOfBirth"),
										'country' => $this->input->post("Country"),
										'city' => $this->input->post("City"),
										'area' => $this->input->post("Area"),
										'cv_file_name' => $fileName, 
										'created_date' => date(DATE_TIME_FORMAT),
										'modified_date' => date(DATE_TIME_FORMAT),
										'comment' => $this->input->post("Comments"),
										'arrival_source' => $arrivalSource,
										'reference' => $arrivalSourceReference,
										#'status' => 1					
										);
										
					$candidateID = $this->recruitment->saveValues(TABLE_CANDIDATE, $arrValues);	
						
					# CODE FOR APPLY VACANCY
					$arrVacancyValues = array(
												'candidate_id' => $candidateID, 
												'vacancy_id' => $jobID, 
												'status' => 1, 
												'applied_date' => date(DATE_TIME_FORMAT)
											);
											
					$this->recruitment->saveValues(TABLE_CANDIDATE_VACANCY, $arrVacancyValues);
					
					# SET LOG
					
					# CODE FOR CANDIDATE HISTORY
					$arrValues = array(
										'candidate_id' => $candidateID,
										'vacancy_id' => $jobID,
										'performed_date' => date($this->arrData["dateTimeFormat"]),
										'status_id' => 1,
										'note' => 'Applied for vacancy by candidate',
										'email_sent' => 1
										);								
					
					$this->recruitment->saveValues(TABLE_CANDIDATE_HISTORY, $arrValues);
					
					# CODE FOR QUALIFICATION INSERT
					$eduLevel 		= $this->input->post("eduLevel");
					$eduInstitute 	= $this->input->post("eduInstitute");
					$eduMajors 		= $this->input->post("eduMajors");
					$eduGPA 		= $this->input->post("eduGPA");
					$eduEnded 		= $this->input->post("eduEnded");
					#$eduStatus 		= $this->input->post("eduStatus");
					
					for($ind = 0; $ind < count($eduLevel); $ind++) {
						if(!empty($eduLevel[$ind]) and 
							!empty($eduInstitute[$ind]) and
							!empty($eduEnded[$ind])) {
								
								$arrEduValues = array(
										'edu_candidate_id' => $candidateID,
										'edu_level_id' => $eduLevel[$ind],
										'edu_institute' => $eduInstitute[$ind],
										'edu_major_id' => $eduMajors[$ind],
										'edu_gpa' => ($eduGPA[$ind] != FALSE) ? $eduGPA[$ind] : NULL,
										'edu_ended' => $eduEnded[$ind],
										#'edu_status' => $eduStatus[$ind],
										'created_date' => date($this->arrData["dateTimeFormat"]),
										);
										
								$this->recruitment->saveValues(TABLE_CANDIDATE_EDUCATION, $arrEduValues);
							}
					}
					
							
					# CODE FOR WORK EXPERIENCE INSERT
					
					if((int)$this->input->post("noExp") <= 0 ) {
						$workCompany 		= $this->input->post("workCompany");
						$workJobTitle 		= $this->input->post("workJobTitle");
						$workFrom 			= $this->input->post("workFrom");
						$workTo 			= $this->input->post("workTo");
						$workDescription 	= $this->input->post("workDescription");
						
						for($ind = 0; $ind < count($workCompany); $ind++) {
							if(!empty($workCompany) and
								!empty($workJobTitle) and
								!empty($workFrom) and
								!empty($workTo)) {
									
									$arrWorkValues = array(
											'work_candidate_id' => $candidateID,
											'work_company' => $workCompany[$ind],
											'work_job_title' => $workJobTitle[$ind],
											'work_from' => $workFrom[$ind],
											'work_to' => $workTo[$ind],
											'work_description' => $workDescription[$ind],
											'created_date' => date($this->arrData["dateTimeFormat"])
											);
											
									$this->recruitment->saveValues(TABLE_CANDIDATE_WORK_EXPERIENCE, $arrWorkValues);
								}
						}
					}
					
					$userName = str_replace(' ', '', strtolower(substr($this->input->post("firstName"), 0, 1) . $this->input->post("lastName") . $candidateID));
					$passPhrase = randomString(8);
					
					$arrValues = array(
							'user_role_id' => 1,
							'candidate_id' => $candidateID,
							'user_name' => $userName,
							'plain_password' => $passPhrase,
							'password' => md5($passPhrase),
							'user_status' => 1,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					
					$candUserID = $this->user_management->saveValues(TABLE_USER, $arrValues);
					
					if($candidateID) {
						
						# CODE FOR AUTO LOGIN
						$this->session->set_userdata('user_name', $userName);
						$this->session->set_userdata('display_name', $this->input->post("firstName"));
						$this->session->set_userdata('user_id', $candUserID);
						$this->session->set_userdata('employee_id', 0);
						$this->session->set_userdata('user_role_id', 1);
						$this->session->set_userdata('isFromApply', 1);
						
						# SET LOG
					
						# SHOOT EMAIL
						# Template: new_candidate.html
						
						$arrValues = array(
											'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
											'[CANDIDATE_NAME]' => $this->input->post("firstName") . ' ' . $this->input->post("lastName"),
											'[CANDIDATE_DASHBOARD_LINK]' => $this->baseURL.'/career',
											'[CANDIDATE_USERNAME]' => $userName,
											'[CANDIDATE_PASSWORD]' => $passPhrase,
											'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' PCE. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'new_candidate.html');
						
						$this->sendEmail(
											$arrTo = array(
															$this->input->post("eMail")
															), 				# RECEIVER DETAILS
											'Welcome To SBT Japan', 		# SUBJECT
											$emailHTML						# EMAIL HTML MESSAGE
										);
							
						# SHOOT EMAIL
						# Template: apply_vacancy.html
						
						$arrVacancy = $this->recruitment->getVacancies(array('v.vacancy_id' => $jobID));
						$arrVacancy = $arrVacancy[0];
					
						$arrValues = array(
											'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
											'[CANDIDATE_NAME]' => $this->input->post("firstName") . ' ' . $this->input->post("lastName"),
											'[VACANCY_NAME]' => $arrVacancy['vacancy_name'],
											'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'apply_vacancy.html');
				
						$this->sendEmail(
											$arrTo = array(
															$this->input->post("eMail")
															), 				# RECEIVER DETAILS
											'Application Received', 		# SUBJECT
											$emailHTML						# EMAIL HTML MESSAGE
										);
						
						if($this->input->post("contactNo") != '' && strlen($this->input->post("contactNo")) >= 11) {
							
							# CALL SMS API
							$strNum = cleanNumberForSMS($this->input->post("contactNo"));
							$apiResponse = $this->sendSMS(array($strNum), SMS_CANDIDATE_REGISTERED, CONSTANT_RMS_SMS_MASK_NAME);
							if($apiResponse !== false) {
								$totalRejected = 0;
						
								$arrValues = array(
													'sender_id' => 0,
													'sms_sent_to' => $candidateID,
													'sms_mask' => CONSTANT_RMS_SMS_MASK_NAME,
													'sms_message' => SMS_CANDIDATE_REGISTERED,
													'total_sms_sent' => (1 - $totalRejected),
													'total_sms_rejected' => $totalRejected,
													'created_date' => date($this->arrData["dateTimeFormat"])
													);
								$this->recruitment->saveValues(TABLE_SENT_SMS_MESSAGES, $arrValues);
							}
						}
					}
						
					$this->session->set_flashdata('success_message', 'You Have Successfully Applied To Vacancy "' . $this->arrData['vacancy']['vacancy_name'] . '"');
					redirect($this->baseURL);
					exit;
				}
			} else {
				
				if(!$chkNIC) {
					$arrCandidate = $this->recruitment->getCandidateDetail(array('c.nic_number' => $this->input->post("nicNo")), false);
					$strEmail = urlencode($arrCandidate['email']);
					$this->arrData['validation_error_message'] = '<p>You are already registered with provided NIC number, Please login using your account details or <a href="'.$this->baseURL . '/' . $this->currentController . '/send_details/' . $strEmail . '">click here</a> for further assistance.</p>';
				} else if($chkFamily) {
					$this->arrData['validation_error_message'] = '<p>Your relative is already working in SBT Japan, therefore you are not eligible to work in SBT Japan.</p>';
				} else {
					$strEmail = urlencode($this->input->post("eMail"));
					$this->arrData['validation_error_message'] = '<p>You are already registered with provided email address, Please login using your account details or <a href="'.$this->baseURL . '/' . $this->currentController . '/send_details/' . $strEmail . '">click here</a> for further assistance.</p>';
				}
			}
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['totalEducations'] = ((int)$this->input->post("totalEducations") <= 0)? 1 : (int)$this->input->post("totalEducations");
		$this->arrData['totalWorkings'] = ((int)$this->input->post("totalWorkings") <= 0)? 1 : (int)$this->input->post("totalWorkings");
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles(array('job_title_status' => 1));
		$this->arrData['Countries'] = $this->configuration->getLocations(array('location_type_id' => '2'));
		$this->arrData['Genders'] = $this->config->item('genders');
		$this->arrData['maritalStatuses'] = $this->config->item('marital_status');
		$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => 1));
		$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => 1));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'career/apply', $this->arrData);
		$this->template->render();
	}
	
	public function send_details($strEmail = '')	{
		
		$strEmail = urldecode($strEmail);
		if(trim($strEmail) == '') {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		$this->arrData['strEmail'] = $strEmail;
		$arrWhere = array(
							'email' => $strEmail
						);
		
		
		
		#################################### FORM VALIDATION START ####################################
		
		$this->form_validation->set_rules('candEmail', 'EMail', 'trim|required|valid_email|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$arrCandidate = $this->recruitment->getCandidateDetail(array('c.email' => $strEmail), false);
			if(count($arrCandidate)) {
				$arrUser = $this->user_management->getUsers(array('u.candidate_id' => $arrCandidate['candidate_id']));
				$arrUser = $arrUser[0];
				
				# SHOOT EMAIL
				# Template: retrieve_login_details.html
			
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[CANDIDATE_NAME]' => $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
									'[CANDIDATE_DASHBOARD_LINK]' => $this->baseURL.'/career',
									'[CANDIDATE_USERNAME]' => $arrUser['user_name'],
									'[CANDIDATE_PASSWORD]' => $arrUser['plain_password'],
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'retrieve_login_details.html');
		
				$this->sendEmail(
									$arrTo = array(
													$arrCandidate['email']
													), 				# RECEIVER DETAILS
									'Account Details', 				# SUBJECT
									$emailHTML						# EMAIL HTML MESSAGE
								);
								
				$this->session->set_flashdata('success_message', 'You account details are sent to ' . $arrCandidate['email'] . '.');
				redirect($this->baseURL . '/' . $this->currentController);
			} else {
				$this->session->set_flashdata('error_message', 'Wrong details provided.');
				redirect($this->baseURL . '/' . $this->currentController . '/send_details/' . urlencode($strEmail));
			}
			
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# TEMPLATE LOADING		
		$this->template->write_view('content', 'career/send_details', $this->arrData);
		$this->template->render();
	}
	
	private function setupConfig($arrConfig) {
		for($ind = 0; $ind < count($arrConfig); $ind++) {
			if(!defined($arrConfig[$ind]['config_key'])) {
				define($arrConfig[$ind]['config_key'], $arrConfig[$ind]['config_value']);
			}
		}
	}
	
	private function setupEmail() {
		
		if(SMTP_HOST != '') {
					
			$this->config->set_item('protocol', 'smtp');
			$this->config->set_item('smtp_host', SMTP_HOST);
			$this->config->set_item('smtp_user', SMTP_USER);
			$this->config->set_item('smtp_pass', SMTP_PASSWORD);
			$this->config->set_item('smtp_port', SMTP_PORT);
			
		} else {
			$this->config->set_item('protocol', 'sendmail');
		}
	}
	
	function populateLocations($elmID, $locID, $locType) {
		
		$locID = (int)$locID;
		$locType = (int)$locType;
		
		if($locType == 1) {
			$strType = 'Region';
		} else if($locType == 2) {
			$strType = 'Country';
		} else if($locType == 3) {
			$strType = 'City';
		}
		
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_parent_id' => $locID, 'location_type_id' => $locType));
		if($locType < 3) {
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" onchange="populateLocation(\''.$elmID.'\', this.value, \''.$locType.'\')" style="clear:both">';
		} else {
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" onchange="populateAreas(\''.$elmID.'\', this.value, \''.($locType + 1).'\')" style="clear:both">';
		}
		$strSelect .= '<option value="">Select '.$strType.'</option>';
		
		for($ind = 0; $ind < count($arrLocations); $ind++) {
			$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'">'.$arrLocations[$ind]['location_name'].'</option>';
		}
		$strSelect .= '</select>';
		
		echo $strSelect;
		exit;
	}
	
	function populateAreas($elmID, $locID, $locType) {
		
		$locID = (int)$locID;
		$locType = (int)$locType;
		$strType = 'Area';
		$strSelect = '';
		
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_parent_id' => $locID, 'location_type_id' => $locType));
		
		if(count($arrLocations)) {
			
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" style="clear:both">';
			$strSelect .= '<option value="">Select '.$strType.'</option>';
			
			for($ind = 0; $ind < count($arrLocations); $ind++) {
				$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'">'.$arrLocations[$ind]['location_name'].'</option>';
			}
			$strSelect .= '</select>';
			
		}
		echo $strSelect;
		exit;
	}
	
	public function strValidate($strValue, $strField) {
		
		if($strField == 'contactNo') { 
			$this->form_validation->set_message('strValidate', 'Cell Phone Number field must only contain numbers.');
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'homePhoneNo') { 
			$this->form_validation->set_message('strValidate', 'Home Phone Number field must only contain numbers.');
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'nic') {
			$this->form_validation->set_message('strValidate', 'NIC Number field must only contain numbers.');
			return (preg_match("/^[0-9]*$/u", $strValue)) ? true : false;
		}
		return false;
		
 	}
	
	public function sendEmail($arrTo = array(), $strSubject, $strMessage, $fromEmail = HR_EMAIL_ADDRESS, $fromName = HR_SENDER_NAME) {
		
		require_once APPPATH . 'libraries/PHPMailerAutoload.php';
		$objMail = new PHPMailer();
		
		if(SMTP_HOST != '') { 			
			$objMail->isSMTP();
			//$objMail->SMTPDebug = 2;
			$objMail->SMTPAuth = false;				//		REASON::	Keep false for relay.sbtjapan.com
			$objMail->Host = SMTP_HOST;
			$objMail->Port = SMTP_PORT;
			$objMail->Username = SMTP_USER;
			$objMail->Password = SMTP_PASSWORD;
		} else {
			$objMail->isSendmail();
		}
		
		$objMail->setFrom($fromEmail, $fromName);
		
		for($ind = 0; $ind < count($arrTo); $ind++) {
			$objMail->addAddress($arrTo[$ind]);
		}
		
		$objMail->Subject = $strSubject;
		
		$objMail->msgHTML($strMessage);
		
		if (!$objMail->send()) {
			echo "Mailer Error: " . $objMail->ErrorInfo; die();
			return false;
		}
		return true;
		
	}
	
	public function sendSMS($arrTo = array(), $strMessage, $maskName, $boolSend = true) {
		
		if((int)SYSTEM_SEND_SMS && $boolSend && count($arrTo) && !empty($strMessage) && !empty($maskName)) {
			
			$strRecipients = implode(',', $arrTo);
			
			$arrSMSAPI = $this->config->item('sms_api');
			
			$strURL  = SMS_API_URL . '?action=sendmessage&username=' . $arrSMSAPI[$maskName]['id'];
		  	$strURL .= '&password=' . $arrSMSAPI[$maskName]['key'];
		  	$strURL .= '&recipient=' . urlencode($strRecipients);
		  	$strURL .= '&originator=' . urlencode($maskName);
		  	$strURL .= '&messagedata=' . urlencode($strMessage);
	
		  	# CALL API TO SEND SMS
		  	
			try {
				
				$objCURL = curl_init();
				
				curl_setopt_array($objCURL, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $strURL,
					CURLOPT_CONNECTTIMEOUT => 30
				));
				
				$strResponse = curl_exec($objCURL);
				curl_close($objCURL);
				$strResponse = str_replace(array("<",">"), array("&lt;","&gt;"), $strResponse);
	
		  		return $strResponse;
				
			} catch(Exception $objException) {
				return false;
			}
		}
		
		return false;
		
	}
}

/* End of file career.php */
/* Location: ./application/controllers/career.php */