<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance_Management extends Master_Controller {
		
	private $arrData 			= array();
	public $arrRoleIDs			= array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter 			= '-';
	private $employeeID 		= 0;
	private $employeeCode 		= 0;
	private $companyID 			= 0;
	
	public $dateFrom 			= 0;
	public $dateTo 				= 0;	
	public $maxLateAllowed 		= 5;
	public $halfOnLates 		= 7;
	public $fullOnLates 		= 8;
	public $fullHalfDayHours 	= 4.45;
	public $halfHalfDayHours 	= 4.15;
	public $fullHours 			= 8.45;
	public $halfdayHours 		= 8.15;
	public $secLate 			= 0;	# 0 MINUTES
	public $secHalfday 			= 1800;	# 30 MINUTES
	public $lateMinSeconds		= 900;	# 15 MINUTES
	public $lateMaxTime			= 0.25;
	
	public $startRamadan;
	public $endRamadan;	
	public $dateTimeChange;
	//public $query;

	
	
	function __construct() {
		
		parent::__construct();
		
		/*if(!(int)ATTENDANCE_MODULE_STATUS && $this->userRoleID != WEB_ADMIN_ROLE_ID) {
			redirect(base_url() . 'message/down_for_maintenance');
			exit;
		}*/
		
		$this->dateFrom 					= date('Y-m-d', mktime(0, 0, 0, date("m"), 01, date("Y")));
		$this->dateTo 						= date('Y-m-d', mktime(0, 0, 0, date("m"), date("t"), date("Y")));
		
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID, COMPANY_ADMIN_ROLE_ID);
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["docFolder"]				= ATTENDANCE_LEAVES_DOCS_FOLDER;
		$this->arrData["docFolderShow"]			= str_replace('./', '', ATTENDANCE_LEAVES_DOCS_FOLDER);
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		$this->arrData['strHierarchy'] = $this->employee->getHierarchyWithMultipleAuthorities($this->userEmpNum);
		
		$this->arrData['skipParams'] = array(
												'leave_requisition',
												'leave_requests',
												'leaves_details',
												'leave_status'
											);
		
		if(!in_array($this->currentAction, $this->arrData['skipParams'])) {
			
			$this->employeeID = (int)$this->input->post("empID");
			if(!(int)$this->employeeID) {
				$this->employeeID = (int)$this->uri->segment(3);
			}
			
			if(!(int)$this->employeeID) {
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
				$this->employeeID = $this->arrData['arrEmployee']['emp_id'];
				$this->employeeCode = $this->arrData['arrEmployee']['emp_code'];
				$this->companyID = $this->arrData['arrEmployee']['emp_company_id'];
			} else {
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->employeeID), false);
				$this->employeeID = $this->arrData['arrEmployee']['emp_id'];
				$this->employeeCode = $this->arrData['arrEmployee']['emp_code'];
				$this->companyID = $this->arrData['arrEmployee']['emp_company_id'];
			}
			
			if($this->employeeID == $this->userEmpNum) {
				$this->arrData['ownUser'] = true;
			}
			
			if($this->userRoleID == COMPANY_ADMIN_ROLE_ID && $this->arrData['arrEmployee']['emp_location_id'] != $this->userLocationID) {
				redirect($this->baseURL . '/message/access_denied');
			}
			
			$arrEmpSupervisors = getEmpSupervisors($this->employeeID);
			
			if(
				(($this->userRoleID == 2 && !$this->arrData['ownUser']) || 
				(!$this->arrData['ownUser'] && !in_array($this->userEmpNum, $arrEmpSupervisors))) &&
				(!isAdmin($this->userRoleID) && count(array_diff($arrEmpSupervisors, explode(',', $this->arrData['strHierarchy']))) == count($arrEmpSupervisors))
				) {
					redirect($this->baseURL . '/message/access_denied');
					exit;
				}
			
			$this->arrData['employeeCode'] = $this->employeeCode;
		}
		// //NEW CONTROLLER FUNCTION STARTS
		// $this->load->model('model_attendance_management');
		// //$this->load->model('model_attendance_management', 'attendance', true);
		// $data["fetch_timing"] = $this->model_attendance_management->fetch_timing();
		// // $this->load->view('attendance_detail', $data);
		// $this->load->view("attendance_management/attendance_detail", $data);
		// //ENDS HERE
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');		
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'attendance_management/index', $this->arrData);
		$this->template->render();
	}
	
	public function leave_requisition($leaveID = 0) {
		$this->load->model('model_attendance_management');
		// $this->load->model('model_employee_management');
		// print_r("asdasdddsas");
		// exit;
		#################################### FORM VALIDATION START ####################################		
				
		$this->form_validation->set_rules('leaveType', 'Leave Type', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('leaveCategory', 'Leave Category', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('dateFrom', 'Starting Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dateTo', 'Ending Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('returnDate', 'Return Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leaveDays', 'Total Days', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('leaveReason', 'Reason', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			#	SUPPORTING DOCUMENT (IF ANY)
			$uploadPicConfig['upload_path'] 	= $this->arrData["docFolder"];
			$uploadPicConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp|doc|docs|pdf';
			$uploadPicConfig['max_size']		= '2048';
			$uploadPicConfig['max_filename']	= '100';
			$uploadPicConfig['encrypt_name']	= true;

			$this->load->library('upload');
			$this->upload->initialize($uploadPicConfig);
			
			$docFileName = '';
			
			if(!$this->upload->do_upload('leaveDoc')) {
				if(!empty($_FILES['leaveDoc']['name'])) {
					$this->arrData['error_message'] = $error['error'];
					$error = array('error' => $this->upload->display_errors());	
					
						//echo $error['error'] ;
				}					
			} else {				
				$dataUpload = $this->upload->data();
				$docFileName = basename($dataUpload['file_name']);
				//echo $docFileName;
			}
										
			$arrValues = array(
								'emp_id' => $this->userEmpNum,
								'leave_type' => $this->input->post("leaveType"),
								'leave_category' => $this->input->post("leaveCategory"),
								'leave_from' => $this->input->post("dateFrom"),
								'leave_to' => $this->input->post("dateTo"),
								'leave_return' => $this->input->post("returnDate"),
								'leave_days' => $this->input->post("leaveDays"),
								'leave_reason' => $this->input->post("leaveReason"),
								'leave_status' => 0,
								'created_by' => $this->userEmpNum
							);
			
			if(!empty($docFileName)) {
				$arrValues['leave_doc'] = $docFileName;
				
			}
			
			
			
			if((int)$leaveID) {
								
				$this->model_attendance_management->saveValues(TABLE_ATTENDANCE_LEAVES, $arrValues, array('l.leave_id' => (int)$leaveID, 'l.emp_id' => $this->userEmpNum, 'l.leave_status' => 0));
				
				
				//var_dump($arrValues);
			
				
				$this->session->set_flashdata('success_message', 'Application updated successfully');
				
			} else {
				
				$arrValues['created_date'] 	= date($this->arrData["dateTimeFormat"]);
								
				$this->model_attendance_management->saveValues(TABLE_ATTENDANCE_LEAVES, $arrValues);
			
				#	SHOOT EMAIL
				
				$arrTo = array();
				$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
				$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => $this->arrData['arrEmployee']['emp_id']));
				foreach($arrSupervisors as $arrSupervisor) {
					$arrTo[] = $arrSupervisor['emp_work_email'];
				}
				
				if(count($arrTo)) {
					
					$arrValues = array(
										'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
										'[EMPLOYEE_NAME]' 			=> getSupervisorName($this->arrData['arrEmployee']['emp_id']),
										'[APPLICANT_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
										'[DASHBOARD_LINK]' 			=> $this->baseURL . '/' . $this->currentController . '/leave_requests',
										'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'leave_request_notification.html');
					
					$this->sendEmail(
										$arrTo, 																	# RECEIVER DETAILS
										'Leave Request Notification' . EMAIL_SUBJECT_SUFFIX,						# SUBJECT
										$emailHTML																	# EMAIL HTML MESSAGE
									);
				}
				
				$this->session->set_flashdata('success_message', 'Application submitted successfully');
				
			}
		redirect($this->baseURL . '/' . $this->currentController . '/leave_status');
			
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($leaveID) {
			$this->arrData['record'] = $this->model_attendance_management->getValues(TABLE_ATTENDANCE_LEAVES, $colSelect = '*', array('leave_id' => (int)$leaveID, 'emp_id' => $this->userEmpNum, 'leave_status' => 0));
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		$this->arrData['arrCategories'] = $this->configuration->getValues(TABLE_ATTENDANCE_LEAVE_CATEGORIES, '*');
		$this->arrData['arrTypes'] = $this->config->item('leave_types');
		
		$this->template->write_view('content', 'attendance_management/leave_requisition', $this->arrData);
		$this->template->render();
	}
	
	public function leave_status($pageNum = 1, $leaveID) {
		$this->load->model('model_attendance_management');
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}		
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == 1) {				
				if(!$this->model_attendance_management->deleteValue(TABLE_ATTENDANCE_LEAVES, null, array('leave_id' => (int)$this->input->post("record_id")))) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}								
			}
		}
		##########################
		
		# EXPORT LEAVE FORM AS PDF
		
		// print_r($leaveID);
		// exit;
		if((int)$leaveID) {
			
			$arrLeaveDetail = $this->model_attendance_management->getLeaveDetails($leaveID);
			if(!isAdmin($this->userRoleID)) {
				if($arrLeaveDetail['emp_id'] != $this->userEmpNum && !in_array($this->userEmpNum, getEmpSupervisors($arrLeaveDetail['emp_id']))) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
			
			$strLeaveStatus = 'Pending';
			if((int)$arrLeaveDetail['leave_status'] == 1) {
				$strLeaveStatus = 'Approved';
			} else if((int)$arrLeaveDetail['leave_status'] == 2) {
				$strLeaveStatus = 'Rejected';
			}
			
			$arrValues = array(
							  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
							  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),							  
							  '[EMPLOYEE_NAME]' 			=> $arrLeaveDetail['emp_full_name'],
							  '[EMPLOYEE_DESIGNATION]' 		=> $arrLeaveDetail['emp_designation'],
							  
							  '[LEAVE_TYPE]'				=> $arrLeaveDetail['leave_category'],
							  
							  '[LEAVE_START_DATE]'			=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrLeaveDetail['leave_from'])),
							  '[LEAVE_END_DATE]'			=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrLeaveDetail['leave_to'])),
							  '[RETURN_DATE]'				=> date(SHOW_DATE_TIME_FORMAT, strtotime($arrLeaveDetail['leave_return'])),
							  '[TOTAL_NUMBER_OF_DAYS]'		=> $arrLeaveDetail['leave_days'],
							  
							  '[ANNUAL_LEAVE_BALANCE]'		=> $arrLeaveDetail['emp_annual_leaves'],
							  '[SICK_LEAVE_BALANCE]'		=> $arrLeaveDetail['emp_sick_leaves'],
							  '[FLEXI_LEAVE_BALANCE]'		=> $arrLeaveDetail['emp_flexi_leaves'],
							  '[EDU_LEAVE_BALANCE]'			=> $arrLeaveDetail['emp_edu_leaves'],
							  '[MATERNITY_LEAVE_BALANCE]'	=> $arrLeaveDetail['emp_maternity_leaves'],
							  
							  '[LEAVE_STATUS]'				=> $strLeaveStatus,
							  
							  '[PROCESSED_BY]'				=> $arrLeaveDetail['processed_by_name'],
							  '[PROCESSED_DATE]'			=> ($arrLeaveDetail['processed_date'] != '') ? date(SHOW_DATE_TIME_FORMAT, strtotime($arrLeaveDetail['processed_date'])) : '-',
							  '[MANAGER_COMMENTS]'			=> $arrLeaveDetail['leave_comments']							  
							  );
							  
			$strHTML = getHTML($arrValues, 'leave_form.html');
			
			require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
			
			$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor(PDF_AUTHOR);
			$pdf->setCellHeightRatio(1);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetFontSize(8);				  
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);				  
			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			
			// add a page
			$pdf->AddPage();			  
			// output the HTML content
			$pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
			$pdf->lastPage();
			$pdfFileName = 'Leave_Form_' . $leaveID . '.pdf';
			$pdf->Output($pdfFileName, 'D');
			
			header('Content-Type: text/doc');
			header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
			header('Cache-Control: max-age=0');
			readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
			
			exit;
		}
		
		$arrWhere = array();
		
		$arrWhere['emp_id'] 				= $this->userEmpNum;
		
		if($this->input->post("leaveCategory")) {
			$arrWhere['leave_category'] 	= $this->input->post("leaveCategory");
		}		
		if($this->input->post("leaveStatus")) {
			$arrWhere['leave_status'] 		= $this->input->post("leaveStatus");
			if($arrWhere['leave_status'] == -1) {
				$arrWhere['leave_status'] = 0;
			}
		}		
		if($this->input->post("leaveYear") && $this->input->post("leaveMonth")) {
			$strDateFrom 					= $this->input->post("leaveYear") . '-' . $this->input->post("leaveMonth") . '-';
			$arrWhere['custom_string'] 		= " (leave_from like '" . $strDateFrom . "%' OR leave_to like '" . $strDateFrom . "%') ";
		}
		
		$this->arrData['totalRecordsCount']	= $this->model_attendance_management->getTotalLeaves($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_attendance_management->getLeaves($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmLeaveStatus');
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
				
		$this->arrData['arrCategories'] 	= $this->configuration->getValues(TABLE_ATTENDANCE_LEAVE_CATEGORIES, '*');		
		$this->arrData['arrSupervisors'] 	= $this->employee->getSupervisors();
		$this->arrData['arrTypes'] 			= $this->config->item('leave_types');
		// print_r($this->arrData);
		// exit;
		$this->template->write_view('content', 'attendance_management/leave_status', $this->arrData);
		$this->template->render();
	}
	// End Method



	public function leave_requests($pageNum = 1, $intType = 0, $intID = 0, $strComments = '') {
		
		$this->load->model('model_attendance_management');
		// $this->load->model('model_employee_management');
		// print_r("asdasdddsas");
		// exit;


		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == 1) {
				
				$arrLeave = $this->model_attendance_management->getLeaves(array('leave_id' => (int)$this->input->post("record_id")));
				$arrEmployee = $this->employee->getEmployees(array('e.emp_id' => $arrLeave[0]['emp_id']));
				
				if(!$this->model_attendance_management->deleteValue(TABLE_ATTENDANCE_LEAVES, null, array('leave_id' => (int)$this->input->post("record_id")))) {
					
					echo "0"; exit;
					
				} else {
					
					if($arrLeave[0]['leave_category'] == 1) {
						# ANNUAL
						$empAnnualLeaves = (int)($arrEmployee[0]['emp_annual_leaves'] + $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_annual_leaves' => $empAnnualLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else if($arrLeave[0]['leave_category'] == 2) {
						# SICK
						$empSickLeaves = (int)($arrEmployee[0]['emp_sick_leaves'] + $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_sick_leaves' => $empSickLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else if($arrLeave[0]['leave_category'] == 3) {
						# MATERNITY/PATERNITY
						$empMaternityLeaves = (int)($arrEmployee[0]['emp_maternity_leaves'] + $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_maternity_leaves' => $empMaternityLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else if($arrLeave[0]['leave_category'] == 4) {
						# FLEXI
						$empFlexiLeaves = (int)($arrEmployee[0]['emp_flexi_leaves'] + $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_flexi_leaves' => $empFlexiLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else if($arrLeave[0]['leave_category'] == 5) {
						# EDUCATIONAL/TRAINING
						$empEduLeaves = (int)($arrEmployee[0]['emp_edu_leaves'] + $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_edu_leaves' => $empEduLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					}
				
					echo "1"; exit;
				}
			}
		}
		##########################
		
		if($intType && $intID) {
			
			$strType = '';
			if($intType == 1) {
				$strType = 'Approved';
			} else if($intType == 2) {
				$strType = 'Rejected';
			}
			
			$arrLeave 		= $this->model_attendance_management->getLeaves(array('leave_id' => $intID));
			$arrEmployee 	= $this->employee->getEmployees(array('e.emp_id' => $arrLeave[0]['emp_id']));
			
			if(!isAdmin($this->userRoleID)) {								
				if(!in_array($this->userEmpNum, getEmpSupervisors($arrEmployee[0]['emp_id']))) {
					redirect($this->baseURL . '/message/access_denied');
					exit;
				}
			}
			
			$boolApprove = true;
			
			if($intType == 1) {
				# IF APPROVED THEN DEDUCT FROM LEAVE QUOTA
				if($arrLeave[0]['leave_category'] == 1) {
					# ANNUAL
					if($arrEmployee[0]['emp_annual_leaves'] >= $arrLeave[0]['leave_days']) {
						$empAnnualLeaves = (int)($arrEmployee[0]['emp_annual_leaves'] - $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_annual_leaves' => $empAnnualLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else {
						$boolApprove = false;
					}
				} else if($arrLeave[0]['leave_category'] == 2) {
					# SICK
					if($arrEmployee[0]['emp_sick_leaves'] >= $arrLeave[0]['leave_days']) {
						$empSickLeaves = (int)($arrEmployee[0]['emp_sick_leaves'] - $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_sick_leaves' => $empSickLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else {
						$boolApprove = false;
					}
				} else if($arrLeave[0]['leave_category'] == 3) {
					# MATERNITY/PATERNITY
					if($arrEmployee[0]['emp_maternity_leaves'] >= $arrLeave[0]['leave_days']) {
						$empMaternityLeaves = (int)($arrEmployee[0]['emp_maternity_leaves'] - $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_maternity_leaves' => $empMaternityLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else {
						$boolApprove = false;
					}
				} else if($arrLeave[0]['leave_category'] == 4) {
					# FLEXI
					if($arrEmployee[0]['emp_flexi_leaves'] >= $arrLeave[0]['leave_days']) {
						$empFlexiLeaves = (int)($arrEmployee[0]['emp_flexi_leaves'] - $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_flexi_leaves' => $empFlexiLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else {
						$boolApprove = false;
					}
				} else if($arrLeave[0]['leave_category'] == 5) {
					# EDUCATIONAL/TRAINING
					if($arrEmployee[0]['emp_edu_leaves'] >= $arrLeave[0]['leave_days']) {
						$empEduLeaves = (int)($arrEmployee[0]['emp_edu_leaves'] - $arrLeave[0]['leave_days']);
						$this->employee->saveValues(TABLE_EMPLOYEE, array('emp_edu_leaves' => $empEduLeaves), array('emp_id' => $arrEmployee[0]['emp_id']));
					} else {
						$boolApprove = false;
					}
				}
			}
			
			if($boolApprove) {
				$arrValues = array(
								   'leave_status' 	=> $intType,
								   'leave_comments' => urldecode($strComments),
								   'processed_by' 	=> $this->userEmpNum,
								   'processed_date'	=> date($this->arrData["dateTimeFormat"])
								  );
				
				$this->model_attendance_management->saveValues(TABLE_ATTENDANCE_LEAVES, $arrValues, array('leave_id' => $intID));
			} else {
				$this->session->set_flashdata('success_message', 'There is not enough quota available to approve this leave request');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);
				exit;
			}
			
			$this->arrData['arrCategories'] 	= $this->configuration->getValues(TABLE_ATTENDANCE_LEAVE_CATEGORIES, '*', array('leave_category_id' => $arrLeave[0]['leave_category']));
			$this->arrData['arrTypes'] 			= $this->config->item('leave_types');
			
			
			#	SHOOT EMAIL
			
			$arrValues = array(
								'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' 			=> $arrEmployee[0]['emp_full_name'],
								'[APPLICATION_STATUS]'		=> $strType,
								'[EMPLOYEE_CODE]'			=> $arrEmployee[0]['emp_code'],
								'[LEAVE_TYPE]'				=> $this->arrData['arrTypes'][$arrLeave[0]['leave_type']],
								'[LEAVE_CATEGORY]'			=> $this->arrData['arrCategories'][0]['leave_category'],
								'[DATE_FROM]'				=> readableDate($arrLeave[0]['leave_from'], $this->arrData["showDateFormat"]),
								'[DATE_TO]'					=> readableDate($arrLeave[0]['leave_to'], $this->arrData["showDateFormat"]),
								'[RETURN_DATE]'				=> readableDate($arrLeave[0]['leave_return'], $this->arrData["showDateFormat"]),
								'[NO_OF_DAYS]'				=> (int)$arrLeave[0]['leave_days'],
								'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
								);
								
			$arrTo = array($arrEmployee[0]['emp_work_email']);
			
			if($intType == 1) {
				$arrLeaveNotifEmailRecipients = explode(',', LEAVE_STATUS_NOTIFICATION_RECIPIENTS);
				for($ind = 0; $ind < count($arrLeaveNotifEmailRecipients); $ind++) {
					if(trim($arrLeaveNotifEmailRecipients[$ind]) != '') {
						$arrTo[] = $arrLeaveNotifEmailRecipients[$ind];
					}
				}
			}
			
			$emailHTML = getHTML($arrValues, 'leave_status_notification.html');
			$this->sendEmail(
								$arrTo, 																	# RECEIVER DETAILS
								'Leave Status Notification' . EMAIL_SUBJECT_SUFFIX,							# SUBJECT
								$emailHTML																	# EMAIL HTML MESSAGE
							);
							
			$this->session->set_flashdata('success_message', 'Leave ' . $strType . ' Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);
			exit;
		}
		
		//$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		//$arrEmpWhere['e.emp_code > '] = ZERO;
		
		$arrJobCategoryWhere = array(
									'job_category_status' => STATUS_ACTIVE,
									'order_by' => 'job_category_name'
									);
									
		if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			//$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
		} else if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) { 
			$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
		}
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$arrEmpWhere['e.emp_location_id'] = $this->userLocationID;
		}
		
		$_POST['sort_field'] = 'e.emp_code';
		$_POST['sort_order'] = 'ASC';
		
		$arrEmployees = $this->employee->getEmployees(array('e.emp_id' => $this->userEmpNum));
		$arrEmployees = array_merge($arrEmployees, $this->employee->getEmployees($arrEmpWhere));
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrJobCategoryWhere);
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		unset($_POST['sort_field']);
		unset($_POST['sort_order']);
		
		$arrWhere = array();
		
		$arrWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
		$this->arrData['empSupervisor']	= $this->userEmpNum;
		
		if(isAdmin($this->userRoleID) && $this->input->post("empSupervisor")) {
			if((int)$this->input->post("empSupervisor") > 0) {
				unset($arrWhere['es.supervisor_emp_id in ']);
				$arrWhere['es.supervisor_emp_id'] = $this->input->post("empSupervisor");
				$this->arrData['empSupervisor']	= $this->input->post("empSupervisor");
			} else {
				unset($arrWhere['es.supervisor_emp_id']);
				unset($arrWhere['es.supervisor_emp_id in ']);
				$this->arrData['empSupervisor']	= '';
			}
		} else if(isAdmin($this->userRoleID)) {
			unset($arrWhere['es.supervisor_emp_id']);
			unset($arrWhere['es.supervisor_emp_id in ']);
			$this->arrData['empSupervisor']	= '';
		}
		
		if($this->input->post("empID")) {
			$arrWhere['l.emp_id'] 	= $this->input->post("empID");
			$this->arrData['empID']	= $this->input->post("empID");
		}
		
		if($this->input->post("leaveCategory")) {
			$arrWhere['l.leave_category'] 	= $this->input->post("leaveCategory");
			$this->arrData['leaveCategory']	= $this->input->post("leaveCategory");
		}
			
		if($this->input->post("leaveStatus")) {
			$arrWhere['l.leave_status'] 	= $this->input->post("leaveStatus");
			$this->arrData['leaveStatus']	= $this->input->post("leaveStatus");
			if($arrWhere['l.leave_status'] == -1) {
				$arrWhere['l.leave_status'] = 0;
			}
		}
		
		if($this->input->post("dateFrom") && $this->input->post("dateTo")) {
			$arrWhere['custom_string'] 		= "(leave_from between '" . $this->input->post("dateFrom") . "' AND '" . $this->input->post("dateTo") . "' OR leave_to between '" . $this->input->post("dateFrom") . "' AND '" . $this->input->post("dateTo") . "')";
			$this->arrData['dateFrom']		= $this->input->post("dateFrom");
			$this->arrData['dateTo']		= $this->input->post("dateTo");
		}
		
		$arrWhere['e.emp_status'] 			= STATUS_ACTIVE;		
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$arrWhere['e.emp_location_id'] 	= $this->userLocationID;
		}
		
		$this->arrData['totalRecordsCount']	= $this->model_attendance_management->getTotalLeavesForApproval($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_attendance_management->getLeavesForApproval($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmLeaveRequests');
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
				
		$this->arrData['arrCategories'] 	= $this->configuration->getValues(TABLE_ATTENDANCE_LEAVE_CATEGORIES, '*');
		$arrWhereSupervisor = array('e.emp_status' => STATUS_ACTIVE); //, 'e.emp_code >' => ZERO
		$this->arrData['arrSupervisors'] 	= $this->employee->getSupervisors($arrWhereSupervisor);
		$arrJobCategories 					= $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$finalResult 						= array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData['arrSupervisors'] 	= $finalResult;
		$this->arrData['arrTypes'] 			= $this->config->item('leave_types');
		
		$this->template->write_view('content', 'attendance_management/leave_requests', $this->arrData);
		$this->template->render();
	}
	
	public function att_detail() {
		//New Controller CRUD
		// $this->load->model('model_attendance_management', 'attendance', true);
		// $data["fetch_timing"] = $this->main_model->fetch_timing();
		// // $this->load->view('attendance_detail', $data);
		// $this->load->view("attendance_detail", $data);
		// //Ends Here
		//NEW CONTROLLER FUNCTION STARTS
		$this->load->model('model_attendance_management');
		$this->load->model('model_employee_management');
		//$this->load->model('model_attendance_management', 'attendance', true);
		$data["fetch_timing"] = $this->model_attendance_management->fetch_timing();
		$this->load->view("attendance_management/attendance_detail", $data);
		//ENDS HERE

		$this->arrRoleIDs[] = ACCOUNT_MANAGER_ROLE_ID;
		$this->arrRoleIDs[] = ACCOUNT_EMPLOYEE_ROLE_ID;
		
		$this->arrData['displayClocking'] = false;
		
		$arrWhere['USERID'] = (int)$this->employeeCode;
		$this->arrData['empCode'] = (int)$this->employeeCode;
		$this->arrData['empID'] = (int)$this->employeeID;
		
		/*if($this->input->post("dateFrom")) {
			$arrWhere['DATE >= '] = $this->input->post("dateFrom");
		} else {
			$arrWhere['DATE >= '] = $this->dateFrom;
		}
		$this->arrData['dateFrom'] = $arrWhere['DATE >= '];
		
		if($this->input->post("dateTo")) {
			$arrWhere['DATE <= '] = $this->input->post("dateTo");
		} else {
			$arrWhere['DATE <= '] = $this->dateTo;
		}
		$this->arrData['dateTo'] = $arrWhere['DATE <= '];*/
		
		if((int)$this->input->post("selMonth") && (int)$this->input->post("selYear")) {
			$arrWhere['DATE >= '] = $this->input->post("selYear") . '-' . $this->input->post("selMonth") . '-01';
			$arrWhere['DATE <= '] = $this->input->post("selYear") . '-' . $this->input->post("selMonth") . '-' . date('t', strtotime($arrWhere['DATE >= ']));
		} else {
			$arrWhere['DATE >= '] = $this->dateFrom;
			$arrWhere['DATE <= '] = $this->dateTo;
		}
		
		$this->arrData['selMonth'] = date('m', strtotime($arrWhere['DATE >= ']));
		$this->arrData['selYear'] = date('Y', strtotime($arrWhere['DATE >= ']));
		
		$this->arrData['dateFrom'] = $arrWhere['DATE >= '];
		$this->arrData['dateTo'] = $arrWhere['DATE <= '];
		
		$arrEmpWhere['e.emp_status'] = STATUS_ACTIVE;
		//$arrEmpWhere['e.emp_code > '] = ZERO;
		
		//if(!in_array($this->userRoleID, $this->arrData["forcedAccessRoles"])) {
		$arrJobCategoryWhere = array(
									'job_category_status' => STATUS_ACTIVE,
									'order_by' => 'job_category_name'
									);
									
		if(!isAdmin($this->userRoleID)) {
			$arrEmpWhere['es.supervisor_emp_id in '] = '(' . $this->arrData['strHierarchy'] . ')';
			$arrEmpWhere['e.emp_employment_status < '] = STATUS_EMPLOYEE_SEPARATED;
		} else if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) { 
			$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
		}		
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$arrEmpWhere['e.emp_location_id'] = $this->userLocationID;
		}
		
		$_POST['sort_field'] = 'e.emp_code';
		$_POST['sort_order'] = 'ASC';
		
		$arrEmployees = $this->model_employee_management->getEmployees(array('e.emp_id' => $this->userEmpNum));
		$arrEmployees = array_merge($arrEmployees, $this->model_employee_management->getEmployees($arrEmpWhere));
		$arrJobCategories = $this->model_employee_management->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', $arrJobCategoryWhere);
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($arrEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		
		$this->arrData["arrEmployees"] = $finalResult;
		unset($_POST['sort_field']);
		unset($_POST['sort_order']);
		
		if($this->arrData['empID']) {
			
			$objAllDates = new DatePeriod(
				 new DateTime($this->arrData['dateFrom']),
				 new DateInterval('P1D'),
				 new DateTime(date('Y-m-d', strtotime($this->arrData['dateTo'] . ' +1 day')))
			);
			
			$this->arrData['arrAllDates'] = array();
			foreach ($objAllDates as $strKey => $strValue) {
				$this->arrData['arrAllDates'][] = $strValue->format('Y-m-d');
			}
			
			$arrCompany = $this->configuration->getCompanies(array('company_id' => $this->arrData['arrEmployee']['emp_company_id']));
			$locationID = $arrCompany[0]['company_country_id'];
			$this->arrData['arrHolidays'] = $this->configuration->getValues(TABLE_PUBLIC_HOLIDAYS, "*", array('holiday_country_id' => $locationID, 'holiday_date >= ' => $arrWhere['DATE >= '], 'holiday_date <= ' => $arrWhere['DATE <= ']));
			
			$arrWhere['DATE >= '] = $arrWhere['DATE >= '] . ' 00:00:00';
			$arrWhere['DATE <= '] = $arrWhere['DATE <= '] . ' 23:59:59';
			
			// if($this->companyID == 1) {
			// 	$this->arrData['arrRecords'] = $this->model_attendance_management->getAttendanceRecordsPK($arrWhere);
			// } else {
			// 	$this->arrData['arrRecords'] = $this->model_attendance_management->getAttendanceRecordsDxb($arrWhere);
			// }
			
			$this->arrData['totalRecordsCount'] = count($this->arrData['arrRecords']);
			
			if($this->input->post("txtExport") == 1) {
				
				$arrJobCategory = $this->configuration->getJobCategories(array('job_category_id' => $this->arrData['arrEmployee']['emp_job_category_id']));
				
				$strHTML = '';
				
				for($ind = 0; $ind < count($this->arrData['arrAllDates']); $ind++) {
					
					$dateIndex = array_search($this->arrData['arrAllDates'][$ind], array_column($this->arrData['arrRecords'], 'DATE'));
					
					if($dateIndex !== false) {
						$jnd = $dateIndex;
					} else {
						$jnd = -1;
					}
					
					$rowBGColor = '';
					$publicHoliday = '';
					$holidayIndex = array_search($this->arrData['arrAllDates'][$ind], array_column($this->arrData['arrHolidays'], 'holiday_date'));
					
					if(date('N', strtotime($this->arrData['arrAllDates'][$ind])) == $this->arrData['arrEmployee']['company_weekly_off_1'] || date('N', strtotime($this->arrData['arrAllDates'][$ind])) == $this->arrData['arrEmployee']['company_weekly_off_2'] || $holidayIndex !== false) {
						$rowBGColor = ' bgcolor="#8FB05C" style="color:#FFF"';
						if($holidayIndex !== false) {
							$publicHoliday = $this->arrData['arrHolidays'][$holidayIndex]['holiday_name'];
						}
					}
					
					// TO HIDE TODAY'S OUT TIME TO AVOID CONFUSION
		
					if($this->arrData['arrAllDates'][$ind] == date('Y-m-d')) {
						$this->arrData['arrRecords'][$jnd]['OUT'] = '';
					}
					
				  
				  $strDate = readableDate($this->arrData['arrAllDates'][$ind], 'jS M, Y');
				  $strDay = date('l', strtotime($this->arrData['arrAllDates'][$ind]));
				  $strIN = ($this->arrData['arrRecords'][$jnd]['IN'] != '') ? date('g:i A', strtotime($this->arrData['arrRecords'][$jnd]['IN'])) : '-';
				  $strOUT = ($this->arrData['arrRecords'][$jnd]['OUT'] != '') ? date('g:i A', strtotime($this->arrData['arrRecords'][$jnd]['OUT'])) : '-';
				  if($publicHoliday != '') {
					  $strHrs = $publicHoliday;
				  } else {
					  if($this->arrData['arrRecords'][$jnd]['IN'] != '' && $this->arrData['arrRecords'][$jnd]['OUT'] != '') { 
						  $strTime1 = new DateTime($this->arrData['arrRecords'][$jnd]['IN']);
						  $strTime2 = new DateTime($this->arrData['arrRecords'][$jnd]['OUT']);
						  $intInterval = $strTime1->diff($strTime2);
						  if($intInterval->format('%h') > 0 || $intInterval->format('%i') > 0) {
							  $strHrs = $intInterval->format('%h')." hr ".$intInterval->format('%i')." min";
						  }
					  } else {
						  $strHrs = '-';
					  }
				  }
				  
				  $strNotes = getAttendanceNotes($this->arrData['empID'], $this->arrData['arrAllDates'][$ind]);				  
				  if(trim($strNotes) == '') {
				  	$strNotes = '-';
				  }
				  
				  $strLeave = getIfLeaveApplied($this->arrData['empID'], $this->arrData['arrAllDates'][$ind]);
				  if(trim($strLeave) == '') {
				  	$strLeave = '-';
				  }
				  
				  $strHTML .= '<tr ' . $rowBGColor . ' height="30px">
					<td class="listContentCol">' . $strDate . '</td>
					<td class="listContentCol">' . $strDay . '</td>
					<td class="listContentCol">' . $strIN . '</td>
					<td class="listContentCol">' . $strOUT . '</td>
					<td class="listContentCol">' . $strHrs . '</td>
					<td class="listContentCol">' . $strLeave . '</td>
					<td class="listContentCol">' . $strNotes . '</td>
				  </tr>';
				}
				
				$arrValues = array(
									  '[LOGO_URL]' 					=> EMAIL_HEADER_LOGO,
									  '[OFFICE_ADDRESS]' 			=> $arrCompany[0]['company_name'] . '<br />&nbsp;&nbsp;' . $arrCompany[0]['company_address'],
									  '[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),
									  '[MONTH]' 					=> strtoupper(date('F', mktime(0, 0, 0, (int)$this->input->post("selMonth"), 10))),
									  '[YEAR]' 						=> $this->input->post("selYear"),
									  '[EMPLOYEE_CODE]' 			=> $this->arrData['arrEmployee']['emp_code'],
									  '[EMPLOYEE_DESIGNATION]' 		=> $this->arrData['arrEmployee']['emp_designation'],
									  '[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
									  '[EMPLOYEE_JOINING_DATE]' 	=> date(SHOW_DATE_TIME_FORMAT, strtotime($this->arrData['arrEmployee']['emp_joining_date'])),
									  '[EMPLOYEE_DEPARTMENT]' 		=> $arrJobCategory[0]['job_category_name'],
									  '[EMPLOYEE_SUPERVISOR]' 		=> getSupervisorName($this->arrData['arrEmployee']['emp_id']),
									  '[ATTENDANCE_DETAIL]'			=> $strHTML
									  );
									  
				  $strHTML = getHTML($arrValues, 'attendance.html');
				  
				  require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');

				  $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
				  // set document information
				  $pdf->SetCreator(PDF_CREATOR);
				  $pdf->SetAuthor(PDF_AUTHOR);
				  $pdf->setCellHeightRatio(1);
				  $pdf->setPrintHeader(false);
				  $pdf->setPrintFooter(false);
				  $pdf->SetFontSize(8);				  
				  // set margins
				  $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);				  
				  // set auto page breaks
				  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
				  // set image scale factor
				  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
				  // set some language-dependent strings (optional)
				  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
					  require_once(dirname(__FILE__).'/lang/eng.php');
					  $pdf->setLanguageArray($l);
				  }
				  
				  // add a page
				  $pdf->AddPage();			  
				  // output the HTML content
				  $pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
				  $pdf->lastPage();
				  $pdfFileName = 'Attendance_' . str_replace(' ', '_', $this->arrData['arrEmployee']['emp_full_name']) . '_' . $this->input->post("selMonth") . '_' . $this->input->post("selYear") . '.pdf';
				  $pdf->Output($pdfFileName, 'D');
				  
				  /*header('Content-Type: text/doc');
				  header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
				  header('Cache-Control: max-age=0');
				  readfile('./' . PDF_FILES_FOLDER . $pdfFileName);*/
				  
				  exit;
			}
		}
		
		$this->arrData['arrEmploymentStatuses'] = $this->model_employee_management->populateEmploymentStatus();
		// $this->arrData['csvinfo'] = 'sdfsdfsdfdaf';
		$this->template->write_view('content', 'attendance_management/attendance_detail', $this->arrData);
		$this->template->render();
	}
	
	public function clocking_detail($empID = 0, $strDate = '') {
		
		$arrWhere = array();
		
		if(trim($strDate) == '') {
			$strDate = date(strtotime('now'), 'Y-m-d');
		}
		
		$arrWhere['DATE >= '] = $strDate . ' 00:00:00';
		$arrWhere['DATE <= '] = $strDate . ' 23:59:59';
		$arrWhere['USERID'] = $this->arrData['arrEmployee']['emp_code'];
		
		if($this->companyID == 1) {
			$this->arrData['arrRecords'] = $this->model_attendance_management->getClockingRecordsPK($arrWhere);
		} else {
			$this->arrData['arrRecords'] = $this->model_attendance_management->getClockingRecordsDxb($arrWhere);
		}
		$this->arrData['strDate'] = $strDate;
		
		$this->template->write_view('content', 'attendance_management/clocking_detail', $this->arrData);
		$this->template->render();
	}
	//Employment detail function picked from employee management
	public function employment_details($employeeID = 0) {
		
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->arrData['record'] = array();
		
		$employeeID = (int)$employeeID;
		
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		
		$arrWhere = array(
						'emp_id' => $employeeID
						);
		
		if(!isAdmin($this->userRoleID)) {
			
			# SET LOG
			
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
			$this->arrData['canWrite'] = NO;
			$this->arrData['canDelete'] = NO;
		}
		
		#################################### FORM VALIDATION START ####################################	
		
		$this->form_validation->set_rules('empCompany', 'Company', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empID', 'Employee Id', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empDepartment', 'Department', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empDesignation', 'Designation', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empWorkEmail', 'Work EMail', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('empJobLocation', 'Job Location', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empEmploymentStatus', 'Employment Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empJoiningDate', 'Joining Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empProbationEndDate', 'Probation End Date', 'trim|required|xss_clean');
		
		if($this->input->post('empEmploymentStatus') == STATUS_EMPLOYEE_CONFIRMED) {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|required|xss_clean');
		} else {
			$this->form_validation->set_rules('empConfirmationDate', 'Confirmation Date', 'trim|xss_clean');
		}
		
		$this->form_validation->set_rules('empAnnualLeaves', 'Annual Leaves', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empSickLeaves', 'Sick Leaves', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empFlexiLeaves', 'Flexi Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empEduLeaves', 'Educational/Training Leaves', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('empMaternityLeaves', 'Maternity/Paternity Leaves', 'trim|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ######################################
		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post() && (int)$this->arrData['canWrite'])
			{
				$arrValues = array(
								'emp_code' => $this->input->post("empID"),
								'emp_company_id' => $this->input->post("empCompany"),
								'emp_job_category_id' => $this->input->post("empDepartment"),
								'emp_designation' => $this->input->post("empDesignation"),
								'emp_location_id' => $this->input->post("empJobLocation"),
								'emp_ot_eligibility' => $this->input->post("empOT"),
								'emp_ip_num' => $this->input->post("empIPNum"),
								'emp_work_email' => $this->input->post("empWorkEmail"),
								//'emp_authority_id' => (int)$this->input->post("empSupervisor"),
								'emp_currency_id' => $this->input->post("empCurrency"),
								'emp_employment_type' => $this->input->post("empEmploymentType"),
								'emp_employment_status' => $this->input->post("empEmploymentStatus"),
								'emp_joining_date' => $this->input->post("empJoiningDate"),
								'emp_probation_end_date' => $this->input->post("empProbationEndDate"),
								'emp_confirmation_date' => $this->input->post("empConfirmationDate"),
								'emp_visa_company_id' => $this->input->post("empVisaCompany"),
								'emp_visa_issue_date' => $this->input->post("empVisaIssueDate"),
								'emp_visa_expiry_date' => $this->input->post("empVisaExpiryDate"),
								'emp_nic_issue_date' => $this->input->post("empNICIssueDate"),
								'emp_nic_expiry_date' => $this->input->post("empNICExpiryDate"),
								'emp_labour_card_issue_date' => $this->input->post("empLabourIssueDate"),
								'emp_labour_card_expiry_date' => $this->input->post("empLabourExpiryDate"),
								'emp_sponsor_id' => $this->input->post("empSponsor"),
								'emp_annual_leaves' => $this->input->post("empAnnualLeaves"),
								'emp_sick_leaves' => $this->input->post("empSickLeaves"),
								'emp_flexi_leaves' => $this->input->post("empFlexiLeaves"),
								'emp_edu_leaves' => $this->input->post("empEduLeaves"),
								'emp_maternity_leaves' => $this->input->post("empMaternityLeaves")
								);
										
				if($employeeID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				} else {
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				}
								
				if($employeeID) {
					$result = $this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
					
					if((int)$this->input->post("empEmploymentStatus") < STATUS_EMPLOYEE_SEPARATED) {
						$userStatusID = STATUS_ACTIVE;
					} else {
						$userStatusID = STATUS_INACTIVE_VIEW;
					}
					
					$this->employee->saveValues(	TABLE_USER, 
													array('user_status' => $userStatusID), 	# WHAT TO UPDATE
													array('employee_id' => $employeeID)		# WHERE
												);
				}
				
				$arrSupervisors = $this->input->post("empSupervisor");
					
				if(count($arrSupervisors)) {
					
					$this->master->deleteValue(TABLE_EMPLOYEE_SUPERVISORS, null, array('emp_id' => $employeeID));
					
					foreach($arrSupervisors as $strKey => $strValue) {
						$arrValues = array(					
									'emp_id' => $employeeID,
									'supervisor_emp_id' => (int)$strValue,
									'created_by' => $this->userEmpNum,
									'created_date' => date($this->arrData["dateTimeFormat"])		
									);
									
						$this->employee->saveValues(TABLE_EMPLOYEE_SUPERVISORS, $arrValues);
					}
				}
								
				if($result)
				{
					# SHOOT EMAIL
					
					$strEmail = $this->input->post("empWorkEmail");
					if(!empty($strEmail)) {
						
						$arrUserDetails = $this->configuration->getValues(TABLE_USER, ' user_name, plain_password ', array('employee_id' => $employeeID));
						$arrUserDetails = $arrUserDetails[0];
						
						/*$arrUserName = explode('@', $strEmail);
						$userName = trim($arrUserName[0]);
						
						$userName = trim($arrUserDetails['user_name']);
						
						if($userName != $arrUserDetails['user_name']) {
							$userResult = $this->configuration->saveValues(TABLE_USER, array('user_name' => $userName), array('employee_id' => $employeeID));
						}*/
				
						$arrValues = array(
											'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
											'[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
											'[EMPLOYEE_DASHBOARD_LINK]' => $this->baseURL,
											'[EMPLOYEE_USERNAME]' 		=> $arrUserDetails['user_name'],
											'[EMPLOYEE_PASSWORD]' 		=> $arrUserDetails['plain_password'],
											'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
											);
											
						$emailHTML = getHTML($arrValues, 'details_update_notification_and_login_details.html');
						
						$this->sendEmail(
											$arrTo = array(
															$strEmail
															), 															# RECEIVER DETAILS
											'Employment Details Update Notification' . EMAIL_SUBJECT_SUFFIX,			# SUBJECT
											$emailHTML																	# EMAIL HTML MESSAGE
										);
					}
					
					# SET LOG
					debugLog("Employment Details Updated: [EmpID/Code: ".$employeeID."/".$this->input->post("empID")."]");
					
					$this->session->set_flashdata('success_message', 'Information saved successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $employeeID);
					exit;
				} else {
					$this->arrData['error_message'] = 'Data not saved, try again';
				}
				
				
			}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$this->arrData['record'] = $this->arrData['arrEmployee'];
		}
		
		# CODE FOR PAGE CONTENT
		//$this->arrData['empGrades'] = $this->employee->populateGrades();
		$this->arrData['empWorkShifts'] = $this->employee->populateWorkShifts();
		$this->arrData['empSupervisors'] = $this->employee->getSupervisors(array('e.emp_id != ' => $employeeID));
		$this->arrData['empEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['empDepartments'] = $this->employee->populateDepartments();
		$this->arrData['empCompanies'] = $this->configuration->getCompanies();
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['empSponsors'] = $this->configuration->getSponsors();
		$this->arrData['employmentTypes'] = $this->config->item('employment_types');
		
		# TEMPLATE LOADING
		$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'employee_management/employment_detail', $this->arrData);
		$this->template->render();
	}

	// function load_data()
	// {
	// 	$result = $this->csv_import_model->select();
	// 	$output = '
	// 	 <h3 align="center">Imported User Details from CSV File</h3>
    //     <div class="table-responsive">
    //     	<table class="table table-bordered table-striped">
    //     		<tr>
    //     			<th>Sr. No</th>
    //     			<th>First Name</th>
    //     			<th>Last Name</th>
    //     			<th>Phone</th>
    //     			<th>Email Address</th>
    //     		</tr>
	// 	';
	// 	$count = 0;
	// 	if($result->num_rows() > 0)
	// 	{
	// 		foreach($result->result() as $row)
	// 		{
	// 			$count = $count + 1;
	// 			$output .= '
	// 			<tr>
	// 				<td>'.$count.'</td>
	// 				<td>'.$row->first_name.'</td>
	// 				<td>'.$row->last_name.'</td>
	// 				<td>'.$row->phone.'</td>
	// 				<td>'.$row->email.'</td>
	// 			</tr>
	// 			';
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$output .= '
	// 		<tr>
	//     		<td colspan="5" align="center">Data not Available</td>
	//     	</tr>
	// 		';
	// 	}
	// 	$output .= '</table></div>';
	// 	echo $output;
	// }

	public function importcsv()
	{
		$this->load->library('csvimport');
		$this->load->model('model_attendance_management');
		// print_r($_FILES["csv_file"]["tmp_name"]);
		$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
		// print_r($file_data);
		// exit;
		foreach($file_data as $row)
		{
			$data[] = array(
				'employee_id'	=>	$row["Employee_ID"],
        		'att_date'		=>	$row["Date"],
        		'att_in'		=>	$row["In"],
        		'att_out'		=>	$row["Out"],
			);
		}
		// print_r($data);
		// exit;
		$this->model_attendance_management->insert($data);
	}
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */