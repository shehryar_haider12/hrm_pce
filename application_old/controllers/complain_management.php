<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Complain_Management extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_complain_management', 'complain', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->arrData["docFolder"]				= COMPLAIN_SUPPORTING_DOCS_FOLDER;
		$this->arrData["docFolderTasks"]		= TASKS_SUPPORTING_DOCS_FOLDER;
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');		
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'complain_management/index', $this->arrData);
		$this->template->render();
		
	}
		
	public function submit_complain()
	{	
					
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('complainType', 'Complain Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('complainReportTo', 'Complain Report To', 'trim|required|xss_clean');
		$this->form_validation->set_rules('complain', 'Complain', 'trim|required|xss_clean');
		$this->form_validation->set_rules('complainPriority', 'Complain Priority', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{
			$boolFileUp = true;
			$strFileName = null;
			if(!empty($_FILES['supportingDoc']['name'][0])) {
				## Document Upload
				$this->load->library('upload');
				
				$uploadDocConfig['upload_path'] 	= $this->arrData["docFolder"];
				$uploadDocConfig['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|jpg|png|bmp|jpeg';
				$uploadDocConfig['max_size']		= '1024';
				$uploadDocConfig['max_filename']	= '100';
				$uploadDocConfig['encrypt_name']	= true;
					
				$this->upload->initialize($uploadDocConfig);
				
				foreach($_FILES['supportingDoc'] as $key=>$val)
				{
					$i = 1;
					foreach($val as $v)
					{
						$field_name = "file_".$i;
						$_FILES[$field_name][$key] = $v;
						$i++;   
					}
				}
				
				unset($_FILES['supportingDoc']);
				
				$strFileName = '';
				foreach($_FILES as $field_name => $file) {				   	
					if(!$this->upload->do_upload($field_name)) {
						$error = array('error' => $this->upload->display_errors());	
						$this->arrData['error_message'] = $error['error'];	
						$boolFileUp = false;			
					} else {			
						$dataUpload = $this->upload->data();
						if(empty($strFileName)) {
							$strFileName = basename($dataUpload['file_name']);
						} else {
							$strFileName .= ",".basename($dataUpload['file_name']);
						}
					}
				}
			}
			
			$complainType = $this->input->post("complainType");
			
			if($complainType == TYPE_REQUEST) {
				$isAnonymous = 0;
			} else {
				$isAnonymous = (int)$this->input->post("isAnonymous");
			}
			
			if($boolFileUp)
			{
				$arrValues = array(
									'complain_type_id' 			=> $this->input->post("complainType"),
									'complain_priority'			=> $this->input->post("complainPriority"),
									'emp_id' 					=> $this->userEmpNum,
									'complain_report_to_id'		=> $this->input->post("complainReportTo"),
									'complain_text' 			=> $this->input->post("complain"),
									'complain_status' 			=> OPEN,
									'complain_last_replied_by' 	=> $this->userEmpNum,
									'is_anonymous' 				=> $isAnonymous,
									'created_date' 				=> date($this->arrData["dateTimeFormat"]),
									'complain_replied_date' 	=> date($this->arrData["dateTimeFormat"]),
									'complain_supporting_doc'	=> $strFileName
									);
				
				$complainID = $this->employee->saveValues(TABLE_EMPLOYEE_COMPLAINS, $arrValues);
				
				# SET LOG
				debugLog("Complain Submitted: [UserID/UserName/EmpID/ComplainID: ".$this->userID.'/'.$this->userName."/".$this->userEmpNum."/".$complainID."]");
				
				if($complainID)
				{
					# SHOOT EMAIL
					# Template: complain_notificaiton.html
					
					if((int)$this->input->post("isAnonymous")) {
						$reportedByName = 'Anonymous';
					} else {
						$reportedByName = $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_id' => $this->userEmpNum));
						$reportedByName = $reportedByName[0]['emp_full_name'];
					}
								
					$reportedToDetails 	= $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $this->input->post("complainReportTo")));
					$reportedToName 	= $reportedToDetails[0]['emp_full_name'];
					$reportedToEmail	= $reportedToDetails[0]['emp_work_email'];
					
					$arrValues = array(
										'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
										'[REPORTED_BY_NAME]' => $reportedByName,
										'[EMPLOYEE_NAME]' => $reportedToName,
										'[COMPLAIN_DASHBOARD_LINK]' => $this->baseURL . '/complain_management/list_complains',
										'[COMPLAIN_TEXT]' => $this->input->post("complain"),
										'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'complain_notification.html');
					
					$this->sendEmail(
										$arrTo = array(
														$reportedToEmail
														), 												# RECEIVER DETAILS
										'Request/Complain Notification' . EMAIL_SUBJECT_SUFFIX, 		# SUBJECT
										$emailHTML														# EMAIL HTML MESSAGE
									);
					
					
					$this->session->set_flashdata('success_message', 'Request/Complain reported successfully');
					redirect($this->baseURL . '/' . $this->currentController . '/list_complains');
					exit;
				}
				else {
					$this->arrData['error_message'] = 'Complain not reported, try again';
				}
			}
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
				
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['complainPriorities']	= $this->config->item('complain_priorities');
		$this->arrData['complainTypes']			= $this->config->item('complain_types');
		
		$arrWhere = array();		
		$reportToEmployees = $this->complain->getReportToEmployees($arrWhere);
		$arrJobCategories = $this->complain->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'job_category_id in ' => '(' . REQUEST_TO_DEPARTMENT_CATEGORY_IDS . ')', 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($reportToEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrReportToEmployees"] = $finalResult;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'complain_management/submit_complain', $this->arrData);
		$this->template->render();
	}
	
	public function list_complains($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
				
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if($this->employee->deleteValue(TABLE_EMPLOYEE_COMPLAINS, array('complain_id' => (int)$this->input->post("record_id")))) {
					echo "1"; exit;
				} else {				
					# SET LOG
				
					echo "0"; exit;
				}								
			}
		}
		
		$arrWhere = array();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		if ($this->input->post()) {
			if($this->input->post("complainType")) {				
				$arrWhere['ei.complain_type_id'] = $this->input->post("complainType");
				$this->arrData['complainType'] = $this->input->post("complainType");
			}
			if($this->input->post("complainPriority")) {
				$arrWhere['ei.complain_priority'] = $this->input->post("complainPriority");
				$this->arrData['complainPriority'] = $this->input->post("complainPriority");
			}
			if($this->input->post("complainReportTo")) {
				$arrWhere['ei.complain_report_to_id'] = $this->input->post("complainReportTo");
				$this->arrData['complainReportTo'] = $this->input->post("complainReportTo");
			}
			if($this->input->post("complainReportedBy")) {
				$arrWhere['ei.emp_id_custom'] = $this->input->post("complainReportedBy");
				$this->arrData['complainReportedBy'] = $this->input->post("complainReportedBy");
			}
			if($this->input->post("dateReported")) {
				$arrWhere['ei.created_date >= '] = $this->input->post("dateReported") . ' 00:00:00';
				$arrWhere['ei.created_date <= '] = $this->input->post("dateReported") . ' 23:59:59';
				$this->arrData['dateReported'] = $this->input->post("dateReported");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				$arrWhere['ei.complain_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
						
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhere['ei.emp_id'] = $this->userEmpNum;
			
			$totalCount = $this->complain->getTotalIssues($arrWhere);
			$totalCount = $totalCount[0]['total'];
			$this->arrData['totalRecordsCount'] = $totalCount;
			$offSet = ($pageNum - 1) * $this->limitRecords;
			$this->arrData['arrRecords'] = $this->complain->getIssues($arrWhere, $this->limitRecords, $offSet);
			$numPages = ceil($totalCount / $this->limitRecords);
			$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListIssues');
			} else {			
			$totalCount = $this->complain->getTotalIssues($arrWhere);
			$totalCount = $totalCount[0]['total'];
			$this->arrData['totalRecordsCount'] = $totalCount;
			$offSet = ($pageNum - 1) * $this->limitRecords;
			$this->arrData['arrRecords'] = $this->complain->getIssues($arrWhere, $this->limitRecords, $offSet);
			$numPages = ceil($totalCount / $this->limitRecords);
			$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListIssues');
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['complainPriorities']	= $this->config->item('complain_priorities');
		$this->arrData['complainStatuses']		= $this->config->item('complain_statuses');
		$this->arrData['complainTypes']			= $this->config->item('complain_types');
		$this->arrData['arrEmployees'] 			= $this->employee->getEmployees(array('e.emp_status' => STATUS_ACTIVE));
		
		$reportToEmployees = $this->complain->getReportToEmployees($arrWhereReportTo);
		$arrJobCategories = $this->complain->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'job_category_id in ' => '(' . REQUEST_TO_DEPARTMENT_CATEGORY_IDS . ')', 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($reportToEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}		
		$this->arrData["arrReportToEmployees"] = $finalResult;
		
		$arrEmployeesSorted = array();
		$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$arrEmployeesSorted[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		$this->arrData["arrEmployees"] = $arrEmployeesSorted;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'complain_management/list_complains', $this->arrData);
		$this->template->render();
		
	}
	
	public function complain_details($complainID = 0)
	{
		$complainID = (int)$complainID;
		
		$employeeID = $this->userEmpNum;
				
		if(!$employeeID) {
			redirect($this->baseURL . '/' . $this->currentController);
			exit;
		}
		
		$this->arrData['arrParentRecord'] 		= $this->complain->getIssues(array('complain_id' => $complainID));
		$strToEmailEmpID						= $this->arrData['arrParentRecord'][0]['complain_last_replied_by'];
		$parentByEmpID							= $this->arrData['arrParentRecord'][0]['emp_id'];
		$parentToEmpID							= $this->arrData['arrParentRecord'][0]['complain_report_to_id'];
		
		$reportToEmployees = $this->complain->getReportToEmployees($arrWhereReportTo);
		$arrJobCategories = $this->complain->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'job_category_id in ' => '(' . REQUEST_TO_DEPARTMENT_CATEGORY_IDS . ')', 'order_by' => 'job_category_name'));
		$finalResult = array();
		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($reportToEmployees, 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
		}
		
		$this->arrData["arrReportToEmployees"] = $finalResult;
		
		#################################### FORM VALIDATION START ####################################
		if(isset($_POST['complainStatus'])){
			$this->form_validation->set_rules('complainStatus', 'Complain Status', 'trim|required|xss_clean');
		}
		if(isset($_POST['complainResponse'])){
			$this->form_validation->set_rules('complainResponse', 'Complain Response', 'trim|required|xss_clean');
		}
		if((isset($_POST['isTransfer'])) && (isset($_POST['complainTransferTo']))){
			$this->form_validation->set_rules('complainTransferTo', 'Complain Transfer To', 'trim|required|xss_clean');
		}
		#################################### FORM VALIDATION END ####################################
			
		if ($this->form_validation->run() == true) 
		{	
			## Document Upload
			$boolFileUp = true;
			$strFileName = null;
			if(!empty($_FILES['supportingDoc']['name'][0])) {
				$this->load->library('upload');
				
				$uploadDocConfig['upload_path'] 	= $this->arrData["docFolder"];
				$uploadDocConfig['allowed_types'] 	= 'doc|docx|xls|xlsx|pdf|jpg|png|bmp|jpeg';
				$uploadDocConfig['max_size']		= '1024';
				$uploadDocConfig['max_filename']	= '100';
				$uploadDocConfig['encrypt_name']	= true;
					
				$this->upload->initialize($uploadDocConfig);			
				
				foreach($_FILES['supportingDoc'] as $key=>$val)
				{
					$i = 1;
					foreach($val as $v)
					{
						$field_name = "file_".$i;
						$_FILES[$field_name][$key] = $v;
						$i++;   
					}
				}
				
				unset($_FILES['supportingDoc']);
				
				$strFileName = '';
				foreach($_FILES as $field_name => $file) {				   	
					if(!$this->upload->do_upload($field_name)) {
						$error = array('error' => $this->upload->display_errors());	
						$this->arrData['error_message'] = $error['error'];	
						$boolFileUp = false;			
					} else {			
						$dataUpload = $this->upload->data();
						if(empty($strFileName)) {
							$strFileName = basename($dataUpload['file_name']);
						} else {
							$strFileName .= ",".basename($dataUpload['file_name']);
						}
					}
				}
			}
			
			if($boolFileUp)
			{
				# CODE FOR RECORD INSERTION
				
				if(isset($_POST['complainStatus'])){
					$complainStatus = (int)$_POST['complainStatus'];
				} else {
					$complainStatus = IN_PROGRESS;
				}
				
				if((isset($_POST['isTransfer'])) && (isset($_POST['complainTransferTo'])))
				{
					$transferText = 'Complain/Suggestion is being transferred from '.$this->arrData['arrParentRecord'][0]['reported_to'].' to '.getValue($reportToEmployees, 'emp_id', $_POST['complainTransferTo'], 'emp_full_name').'. <br><br> '.$_POST['complainResponse'];
					
					$arrValues = array(
										'complain_id' 				=> $complainID,
										'emp_id' 					=> $this->userEmpNum,
										'complain_history_text' 	=> $transferText,
										'complain_supporting_doc'	=> $strFileName,
										'created_date' 				=> date($this->arrData["dateTimeFormat"])
										);
				}
				else
				{	
					$arrValues = array(
										'complain_id' 				=> $complainID,
										'emp_id' 					=> $this->userEmpNum,
										'complain_history_text' 	=> $this->input->post("complainResponse"),
										'complain_supporting_doc'	=> $strFileName,
										'created_date' 				=> date($this->arrData["dateTimeFormat"])
										);
				}
				
				$complainHistoryId = $this->employee->saveValues(TABLE_EMPLOYEE_COMPLAINS_HISTORY, $arrValues);
				
				if($complainHistoryId)
				{
					if((isset($_POST['isTransfer'])) && (isset($_POST['complainTransferTo'])) && (!empty($_POST['complainTransferTo'])))
					{
						$arrValues = array(
										'complain_report_to_id'		=> $this->input->post("complainTransferTo"),
										'is_transfer'				=> YES,
										'complain_transfer_by_id' 	=> $this->userEmpNum,
										'complain_transfer_date' 	=> date($this->arrData["dateTimeFormat"]),
										'complain_replied_date' 	=> date($this->arrData["dateTimeFormat"])
										);
					
						$arrWhere = array(
									'complain_id' 				=> $complainID
									);
									
						$result = $this->db->update(TABLE_EMPLOYEE_COMPLAINS, $arrValues, $arrWhere);
					}
					else //if(!(int)$_POST['webAdminResponse']) # Check applied for webAdminResponse.
					{
						$arrValues = array(
										'complain_last_replied_by'	=> $this->userEmpNum,
										'complain_status' 			=> $complainStatus,
										'is_checked'				=> YES,
										'complain_replied_date' 	=> date($this->arrData["dateTimeFormat"])
										);
					
						$arrWhere = array(
									'complain_id' 				=> $complainID
									);
									
						$result = $this->db->update(TABLE_EMPLOYEE_COMPLAINS, $arrValues, $arrWhere);
					}
					
					# SHOOT EMAIL
					# Template: complain_notificaiton.html				
					
					$arrComplainDetail = $this->complain->getValues(TABLE_EMPLOYEE_COMPLAINS, 'emp_id,is_anonymous', array('complain_id' => $complainID));
					$arrComplainDetail = $arrComplainDetail[0];
					
					if((int)$arrComplainDetail['is_anonymous'] && $arrComplainDetail['emp_id'] == $this->userEmpNum) {
						$reportedByName = 'Anonymous';
					} else {				
						$reportedByName = $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name', array('emp_id' => $this->userEmpNum));
						$reportedByName = $reportedByName[0]['emp_full_name'];
					}
					
					if((isset($_POST['isTransfer'])) && (isset($_POST['complainTransferTo'])) && (!empty($_POST['complainTransferTo'])))
					{
						$reportedTransferToDetails 	= $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $this->input->post("complainTransferTo")));	
						$reportedToDetails 			= $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $strToEmailEmpID));	
						$complainText = $transferText;
						$arrTo = array($reportedTransferToDetails[0]['emp_work_email'], $reportedToDetails[0]['emp_work_email']);
					}
					else if(isset($_POST['webAdminResponse']) && ($_POST['webAdminResponse'] == YES)) {
						$complainText = $this->input->post("complainResponse");
						$parentByEmpDetails = $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $parentByEmpID));
						$parentToEmpDetails = $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $parentToEmpID));
						$arrTo = array($parentByEmpDetails[0]['emp_work_email'], $parentToEmpDetails[0]['emp_work_email']);					
					} else {
						$reportedToDetails 	= $this->complain->getValues(TABLE_EMPLOYEE, 'emp_full_name,emp_work_email', array('emp_id' => $strToEmailEmpID));
						$complainText = $this->input->post("complainResponse");
						$arrTo = array($reportedToDetails[0]['emp_work_email']);
					}
					$reportedToName 	= $reportedToDetails[0]['emp_full_name'];
									
					$arrValues = array(
										'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
										'[REPORTED_BY_NAME]' => $reportedByName,
										'[EMPLOYEE_NAME]' => $reportedToName,
										'[COMPLAIN_DASHBOARD_LINK]' => $this->baseURL . '/complain_management/list_complains',
										'[COMPLAIN_TEXT]' => $complainText,
										'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' ValuStrat. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'complain_notification.html');
					
					$this->sendEmail(
										$arrTo, 														# RECEIVER DETAILS
										'Request/Complain Notification' . EMAIL_SUBJECT_SUFFIX, 		# SUBJECT
										$emailHTML														# EMAIL HTML MESSAGE
									);
					
				}
							
				if($complainHistoryId)	{
					$this->session->set_flashdata('success_message', 'Response submitted successfully');
					if((isset($_POST['isTransfer']))) {
						redirect($this->baseURL . '/' . $this->currentController . '/list_complains');
					}else {
						redirect($this->baseURL . '/' . $this->currentController . '/complain_details/'.$complainID);
					}
					exit;
				} else {
					$this->arrData['error_message'] = 'Response not submitted, try again';
				}
				
			}
					
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
			
		# CODE FOR POPULATING PAGE CONTENT
		if(!isAdmin($this->userRoleID)) {
			$arrWhere = array(
				'ei.emp_id'		 => $this->userEmpNum,
				'ei.complain_id' => $complainID
				);
		} else {
			$arrWhere = array(
			'ei.complain_id' => $complainID
			);
		}
		
		$this->arrData['arrParentRecord'] 		= $this->complain->getIssues($arrWhere);
		$this->arrData['arrParentRecord'] 		= $this->arrData['arrParentRecord'][0];
		if(count($this->arrData['arrParentRecord']) <= 0)
		{
			redirect($this->baseURL . '/' . $this->currentController . '/list_complains');
			exit;
		}
		$this->arrData['arrRecords'] 			= $this->complain->getIssueDetails($employeeID,$complainID);
		$this->arrData['complainStatuses']		= $this->config->item('complain_statuses');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'complain_management/complain_details', $this->arrData);
		$this->template->render();
	}
}