<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_Configuration extends Master_Controller {

	private $arrData = array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter = '-';
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_system_configuration', 'model_system', true);
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrData["baseURL"] 				= $this->baseURL;
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;

//code add by zeeshan qasim
		$this->arrData["companyDocFolder"]			= COMPANY_DOCS_FOLDER;
		$this->arrData["companyDocFolderShow"]		= str_replace('./', '', COMPANY_DOCS_FOLDER);
		//code end by zeeshan qasim
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
	}
	
	public function index()
	{		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'system_configuration/index', $this->arrData);
		$this->template->render();
	}
	
	public function save_module($moduleID)
	{		
		$tblName = TABLE_MODULE;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$moduleID = (int)$moduleID;
		if($moduleID) {			
			$arrWherePopulate = array(
				'm.module_id' => $moduleID
				);
				
			$arrWhere = array(
				'module_id' => $moduleID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('displayModuleName', 'Module Display Name', 'trim|required|min_length[3]|xss_clean|callback_validate_english[md_english]');
		$this->form_validation->set_rules('moduleName', 'Module Name', 'trim|required|min_length[3]|xss_clean|callback_validate_english[m_english]');
		$this->form_validation->set_rules('moduleParent', 'Module Parent', 'trim|callback_numcheck[moduleParent]');
		$this->form_validation->set_rules('layout', 'Layout', 'trim|callback_numcheck[layout]');
		$this->form_validation->set_rules('moduleSortOrder', 'Module Sort Order', 'trim|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{				
				$table = TABLE_MODULE;
				$column = 'module_id';
				$moduleParentID = (int)$this->input->post("moduleParent");
				$Where = array(
							'module_name' => $this->input->post("moduleName")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $moduleID, $moduleParentID);
				if($checkResult == YES)
				{			
					if($this->input->post("moduleSortOrder") != '')
					{
						$moduleSortOrder = $this->input->post("moduleSortOrder");
					} else {
						$moduleSortOrder = 99;
					}
										
					if($moduleID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Module in Active User Permissions
							$tableName = TABLE_USER_PERMISSIONS;
							
							$Where = array(
								'sub_module_id' => $moduleID,
								'status' => STATUS_ACTIVE
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Module Status can not be changed as it is used for an Active User Permission.';
							}
							else
							{
								$arrValues = array(
									'display_name' => ucwords($this->input->post("displayModuleName")),
									'module_name' => $this->input->post("moduleName"),
									'module_parent_id' => $this->input->post("moduleParent"),
									'layout' => $this->input->post("layout"),
									'module_sort_order' => $moduleSortOrder,
									'module_status' => $status,
									'module_skip_display' => (int)$this->input->post("moduleSkipListing"),
									'modified_by' => $this->userID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
							}
						}
						else
						{
							$arrValues = array(
									'display_name' => ucwords($this->input->post("displayModuleName")),
									'module_name' => $this->input->post("moduleName"),
									'module_parent_id' => $this->input->post("moduleParent"),
									'layout' => $this->input->post("layout"),
									'module_sort_order' => $moduleSortOrder,
									'module_status' => $status,
									'module_skip_display' => (int)$this->input->post("moduleSkipListing"),
									'modified_by' => $this->userID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
								'display_name' => ucwords($this->input->post("displayModuleName")),
								'module_name' => $this->input->post("moduleName"),
								'module_parent_id' => $this->input->post("moduleParent"),
								'layout' => $this->input->post("layout"),
								'module_sort_order' => $moduleSortOrder,
								'module_status' => $status,
								'module_skip_display' => (int)$this->input->post("moduleSkipListing"),
								'created_by' => $this->userID,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Module you entering already exists.';
				}
			}
			
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Module saved successfully');
						redirect(base_url() . $this->currentController . '/list_module');
						exit;
					}
			}
			
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED MODULE
		if($moduleID) {
			$this->arrData['record'] = $this->model_system->getModules($arrWherePopulate);
			$this->arrData['record'] = $this->arrData['record'][0];
		}		
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["parentModules"] = $this->model_system->populateParentModules();
		$this->arrData["layouts"] = $this->model_system->populateLayouts();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/module_configuration/save_module', $this->arrData);
		$this->template->render();
	}

	public function list_module($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{
				# Check this Module in Active User Permissions
				$tableName = TABLE_USER_PERMISSIONS;
				
				$Where = array(
					'sub_module_id' => $this->input->post("record_id"),
					'status' => STATUS_ACTIVE
					);
				
				$resultModule = $this->model_system->checkValues($tableName, $Where);
				if($resultModule == YES)
				{
					$tblName = TABLE_MODULE;
					
					$arrDeleteWhere = array(
						'module_id' => $this->input->post("record_id")
						);
						
					$arrDeleteValues = array(
						'module_status' => STATUS_DELETED,
						'deleted_by' => $this->userID,
						'deleted_date' => date($this->arrData["dateTimeFormat"])
						);
					
					if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
						echo "0"; exit;
					} else {
						echo "1"; exit;
					}
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("module")) {				
				$arrWhere['m.module_parent_id'] = $this->input->post("module");
				$this->arrData['module'] = $this->input->post("module");
			}
			if($this->input->post("layout")) {
				$arrWhere['m.layout'] = $this->input->post("layout");
				$this->arrData['layout'] = $this->input->post("layout");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['m.module_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
			if($this->input->post("moduleSkipListing")) {
				$arrWhere['m.module_skip_display'] = $this->input->post("moduleSkipListing");
				$this->arrData['moduleSkipListing'] = $this->input->post("moduleSkipListing");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalModules($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getModules($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListModules');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["modules"] = $this->model_system->populateParentModules();
		$this->arrData["layouts"] = $this->model_system->populateLayouts();
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/module_configuration/list_module', $this->arrData);
		$this->template->render();
	}
	
	public function save_designation($designationID)
	{		
		
		$tblName = TABLE_DESIGNATIONS;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$designationID = (int)$designationID;
		if($designationID) {				
			$arrWhere = array(
				'designation_id' => $designationID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('designationName', 'Designation Name', 'trim|required|min_length[5]|xss_clean|callback_validate_english[des_english]');
		$this->form_validation->set_rules('jobType', 'Designation Type', 'trim|required|callback_numcheck[jobType]');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{				
				$table = TABLE_DESIGNATIONS;
				$column = 'designation_id';
				$Where = array(
							'designation_name' => $this->input->post("designationName")
							);
				if($designationID)
				{
					$checkResult = $this->model_system->checkValues($table, $Where, $column, $designationID);
				} else {
					$checkResult = $this->model_system->checkValues($table, $Where);
				}			
				
				if($checkResult == YES)
				{	
					if($designationID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
							'designation_name' => ucwords($this->input->post("designationName")),
							'designation_status' => (int)$status,
							'modified_by' => (int)$this->userID,
							'modified_date' => date($this->arrData["dateTimeFormat"])
							);
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
								'designation_name' => ucwords($this->input->post("designationName")),
								'designation_status' => (int)$status,
								'created_by' => (int)$this->userID,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
					
					if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Designation saved successfully');
						redirect(base_url() . $this->currentController . '/list_designation');
						exit;
					}
					
				}
				else
				{
					$this->arrData['error_message'] = 'Designation that you have entered already exists.';
				}
			}
			
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED DESIGNATION
		if($designationID) {
			$this->arrData['record'] = $this->model_system->getDesignations($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}		
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_designations', $this->arrData);
		$this->template->render();		
	}
	
	public function list_designation($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				$tblName = TABLE_DESIGNATIONS;
				
				$arrDeleteWhere = array(
					'designation_id' => $this->input->post("record_id")
					);
					
				$arrDeleteValues = array(
					'designation_status' => STATUS_DELETED,
					'deleted_by' => $this->userID,
					'deleted_date' => date($this->arrData["dateTimeFormat"])
					);
				
				if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post())
		{
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['designation_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
			if($this->input->post("jobType")) {
				$this->arrData['jobType'] = $this->input->post("jobType");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalDesignations($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getDesignations($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListDesignations');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_designations', $this->arrData);
		$this->template->render();
	}
	
	public function save_location($locationID)
	{		
		$tblName = TABLE_LOCATION;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$locationID = (int)$locationID;
		if($locationID) {				
			$arrWherePopulate = array(
				'l.location_id' => $locationID
				);
			
			$arrWhere = array(
				'location_id' => $locationID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('locationType', 'Location Type', 'trim|callback_numcheck[locationType]');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{
				$table = TABLE_LOCATION;
				$column = 'location_id';
				
				if($this->input->post("locationType") == LOCATION_TYPE_REGION)
				{
					$Where = array(
								'location_name' => $this->input->post("Region")
								);
				} else if($this->input->post("locationType") == LOCATION_TYPE_COUNTRY) {
					$Where = array(
								'location_name' => $this->input->post("Country")
								);
				} else if($this->input->post("locationType") == LOCATION_TYPE_CITY) {
					$Where = array(
								'location_name' => $this->input->post("City")
								);
				}
				
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $locationID);
				if($checkResult == YES)
				{					
					if($locationID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							$resultLocationType = $this->model_system->getValues(TABLE_LOCATION, "location_type_id", array('location_id' => $locationID));
							$resultLocationType = $resultLocationType[0]['location_type_id'];
							
							# Check this Location in Active Employee / Job Candidate
							$tableName = TABLE_EMPLOYEE;
							
							if($resultLocationType == LOCATION_TYPE_REGION)
							{
								$countLocations = $this->model_system->getValues(TABLE_LOCATION, "count(*) as total", array('location_parent_id' => $locationID,'location_status' => STATUS_ACTIVE));
								$countLocations = $countLocations[0]['total'];
								if($countLocations > ZERO)
								{
									$this->arrData['error_message'] = 'Location Status can not be changed as it is used for an Active Employee / Job Candidate.';
								}							
							}
							else if($resultLocationType == LOCATION_TYPE_COUNTRY)
							{
								$Where = array(
									'emp_country_id' => $locationID,
									'emp_status' => STATUS_ACTIVE
									);
							} 
							else if($resultLocationType == LOCATION_TYPE_CITY)
							{
								$Where = array(
									'emp_city_id' => $locationID,
									'emp_status' => STATUS_ACTIVE
									);
							}
							
							if(!empty($Where))
							{
								if(!$this->model_system->checkValues($tableName, $Where))
								{
									$this->arrData['error_message'] = 'Location Status can not be changed as it is used for an Active Employee / Job Candidate.';
								}
								else
								{					
									if($this->input->post("locationType") == LOCATION_TYPE_REGION)
									{
										$arrValues = array(
												'location_type_id' => LOCATION_TYPE_REGION,
												'location_name' => $this->input->post("Region"),
												'location_parent_id' => ZERO,
												'location_status' => (int)$status,
												'modified_by' => (int)$this->userID,
												'modified_date' => date($this->arrData["dateTimeFormat"])
												);			
									}
									else if($this->input->post("locationType") == LOCATION_TYPE_COUNTRY)
									{
										$arrValues = array(
												'location_type_id' => LOCATION_TYPE_COUNTRY,
												'location_name' => $this->input->post("Country"),
												'location_parent_id' => $this->input->post("Region"),
												'location_status' => (int)$status,
												'modified_by' => (int)$this->userID,
												'modified_date' => date($this->arrData["dateTimeFormat"])
												);
									}
									else if($this->input->post("locationType") == LOCATION_TYPE_CITY)
									{
										$arrValues = array(
												'location_type_id' => LOCATION_TYPE_CITY,
												'location_name' => $this->input->post("City"),
												'location_parent_id' => $this->input->post("Country"),
												'location_status' => (int)$status,
												'modified_by' => (int)$this->userID,
												'modified_date' => date($this->arrData["dateTimeFormat"])
												);
									}
								}
							}
						}
						else
						{
							if($this->input->post("locationType") == LOCATION_TYPE_REGION)
							{
								$arrValues = array(
										'location_type_id' => LOCATION_TYPE_REGION,
										'location_name' => $this->input->post("Region"),
										'location_parent_id' => ZERO,
										'location_status' => (int)$status,
										'modified_by' => (int)$this->userID,
										'modified_date' => date($this->arrData["dateTimeFormat"])
										);			
							}
							else if($this->input->post("locationType") == LOCATION_TYPE_COUNTRY)
							{
								$arrValues = array(
										'location_type_id' => LOCATION_TYPE_COUNTRY,
										'location_name' => $this->input->post("Country"),
										'location_parent_id' => $this->input->post("Region"),
										'location_status' => (int)$status,
										'modified_by' => (int)$this->userID,
										'modified_date' => date($this->arrData["dateTimeFormat"])
										);
							}
							else if($this->input->post("locationType") == LOCATION_TYPE_CITY)
							{
								$arrValues = array(
										'location_type_id' => LOCATION_TYPE_CITY,
										'location_name' => $this->input->post("City"),
										'location_parent_id' => $this->input->post("Country"),
										'location_status' => (int)$status,
										'modified_by' => (int)$this->userID,
										'modified_date' => date($this->arrData["dateTimeFormat"])
										);
							}
						}					
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = 0;
						}
						
						if($this->input->post("locationType") == LOCATION_TYPE_REGION)
						{
							$arrValues = array(
									'location_type_id' => $this->input->post("locationType"),
									'location_name' => $this->input->post("Region"),
									'location_parent_id' => ZERO,
									'location_status' => (int)$status,
									'created_by' => (int)$this->userID,
									'created_date' => date($this->arrData["dateTimeFormat"])
									);			
						}
						else if($this->input->post("locationType") == LOCATION_TYPE_COUNTRY)
						{
							$arrValues = array(
									'location_type_id' => $this->input->post("locationType"),
									'location_name' => $this->input->post("Country"),
									'location_parent_id' => $this->input->post("Region"),
									'location_status' => (int)$status,
									'created_by' => (int)$this->userID,
									'created_date' => date($this->arrData["dateTimeFormat"])
									);
						}
						else if($this->input->post("locationType") == LOCATION_TYPE_CITY)
						{
							$arrValues = array(
									'location_type_id' => $this->input->post("locationType"),
									'location_name' => $this->input->post("City"),
									'location_parent_id' => $this->input->post("Country"),
									'location_status' => (int)$status,
									'created_by' => (int)$this->userID,
									'created_date' => date($this->arrData["dateTimeFormat"])
									);
						}
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Location you entering already exists.';	
				}
			}
			
			if(!empty($arrValues))
			{		
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
					$this->arrData['error_message'] = 'Data not saved, try again';
				} else {
					$this->session->set_flashdata('success_message', 'Location saved successfully.');
					redirect(base_url() . $this->currentController . '/list_location');
					exit;
				}
			}
			
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED LOCATION
		if($locationID) {
			$this->arrData['record'] = $this->model_system->getLocationsSystem($arrWherePopulate);
			$this->arrData['record'] = $this->arrData['record'][0];
		}	
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_location', $this->arrData);
		$this->template->render();
	}
	
	public function list_location($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{					
				$resultLocationType = $this->model_system->getValues(TABLE_LOCATION, "location_type_id", array('location_id' => $this->input->post("record_id")));
				$resultLocationType = $resultLocationType[0]['location_type_id'];
				
				# Check this Location in Active Employee / Job Candidate
				$tableName = TABLE_EMPLOYEE;
				
				if($resultLocationType == LOCATION_TYPE_COUNTRY)
				{
					$Where = array(
						'emp_country_id' => $this->input->post("record_id"),
						'emp_status' => STATUS_ACTIVE
						);
				} 
				else if($resultLocationType == LOCATION_TYPE_CITY)
				{
					$Where = array(
						'emp_city_id' => $this->input->post("record_id"),
						'emp_status' => STATUS_ACTIVE
						);
				}
				
				if(!empty($Where))
				{
					$resultModule = $this->model_system->checkValues($tableName, $Where);
					if($resultModule == YES)
					{
						$tblName = TABLE_LOCATION;
						
						$arrDeleteWhere = array(
							'location_id' => $this->input->post("record_id")
							);
							
						$arrDeleteValues = array(
							'location_status' => STATUS_DELETED,
							'deleted_by' => $this->userID,
							'deleted_date' => date($this->arrData["dateTimeFormat"])
							);
						
						if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
							echo "0"; exit;
						} else {
							echo "1"; exit;
						}
					}
					else { echo "2"; exit; }
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("country")) {
				$arrWhere['l.location_parent_id'] = $this->input->post("country");
				$this->arrData['country'] = $this->input->post("country");
			}
			else if($this->input->post("region")) {
				$arrWhere['l.location_parent_id'] = $this->input->post("region");
				$this->arrData['region'] = $this->input->post("region");
			}
			
			
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['l.location_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
				
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalLocations($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getLocationsSystem($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListLocations');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData["regions"] = $this->model_system->getLocations(array('location_type_id' => LOCATION_TYPE_REGION));
		$this->arrData["countries"] = $this->model_system->getLocations(array('location_type_id' => LOCATION_TYPE_COUNTRY));
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_location', $this->arrData);
		$this->template->render();		
	}
	
	public function save_job_title($titleID)
	{		
		$tblName = TABLE_JOB_TITLE;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$titleID = (int)$titleID;
		if($titleID) {				
			$arrWhere = array(
				'job_title_id' => $titleID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('jobCategory', 'Job Category', 'trim|required');
		$this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('jobDescription', 'Job Description', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{				
				$table = TABLE_JOB_TITLE;
				$column = 'job_title_id';
				$Where = array(
							'job_title_name' => $this->input->post("jobTitle")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $titleID);
				if($checkResult == 1)
				{
					if($titleID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
											
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Job Title in Job Vacancy
							$tableName = TABLE_VACANCY;
							
							$Where = array(
								'job_title_code' => $titleID,
								'vacancy_status' => STATUS_ACTIVE
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Job Title Status can not be changed as it is used for an Active Vacancy.';
							}
							else{
								# Check this Job Title in Employee
								$tableName = TABLE_EMPLOYEE;
								
								$Where = array(
									'emp_job_title_id' => $titleID,
									'emp_status' => STATUS_ACTIVE
									);
								
								if(!$this->model_system->checkValues($tableName, $Where))
								{
									$this->arrData['error_message'] = 'Job Title Status can not be changed as it is used for an Active Employee.';
								}
								else
								{
									$arrValues = array(
									'job_category_id' => $this->input->post("jobCategory"),
									'job_title_name' => $this->input->post("jobTitle"),
									'job_description' => $this->input->post("jobDescription"),
									'job_title_status' => (int)$status,
									'modified_by' => (int)$this->userID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
								}
							}
							
						}
						else
						{
							$arrValues = array(
							'job_category_id' => $this->input->post("jobCategory"),
							'job_title_name' => $this->input->post("jobTitle"),
							'job_description' => $this->input->post("jobDescription"),
							'job_title_status' => (int)$status,
							'modified_by' => (int)$this->userID,
							'modified_date' => date($this->arrData["dateTimeFormat"])
							);
						}
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
								'job_category_id' => $this->input->post("jobCategory"),
								'job_title_name' => $this->input->post("jobTitle"),
								'job_description' => $this->input->post("jobDescription"),
								'job_title_status' => (int)$status,
								'created_by' => (int)$this->userID,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Job Title you entering already exists.';
				}			
			}
						
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Job Title saved successfully');
						redirect(base_url() . $this->currentController . '/list_job_title');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($titleID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_JOB_TITLE, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['jobCategories'] = $this->model_system->getValues(TABLE_JOB_CATEGORY, "*", array('job_category_status' => 1, 'order_by' => 'job_category_name'));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_job_title', $this->arrData);
		$this->template->render();
	}
		
	public function list_job_title($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Job Title in Job Vacancy
				$tableName = TABLE_VACANCY;
				
				$Where = array(
					'job_title_code' => $this->input->post("record_id"),
					'vacancy_status' => STATUS_ACTIVE
					);
					
				$resultJobTitle1 = $this->model_system->checkValues($tableName, $Where);
				if($resultJobTitle1 == YES)
				{				
					# Check this Job Title in Employee
					$tableName = TABLE_EMPLOYEE;
					
					$Where = array(
						'emp_job_title_id' => $this->input->post("record_id"),
						'emp_status' => STATUS_ACTIVE
						);
					
					$resultJobTitle2 = $this->model_system->checkValues($tableName, $Where);
					if($resultJobTitle2 == YES)
					{						
						$tblName = TABLE_JOB_TITLE;
						
						$arrDeleteWhere = array(
							'job_title_id' => $this->input->post("record_id")
							);
							
						$arrDeleteValues = array(
							'job_title_status' => STATUS_DELETED,
							'deleted_by' => $this->userID,
							'deleted_date' => date($this->arrData["dateTimeFormat"])
							);
						
						if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
							echo "0"; exit;
						} else {
							echo "1"; exit;
						}
					}
					else { echo "2"; exit; }
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("jobCategory")) {
				$arrWhere['jt.job_category_id'] = $this->input->post("jobCategory");
				$this->arrData['jobCategory'] = $this->input->post("jobCategory");
			}			
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['jt.job_title_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalJobTitles($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getJobTitles($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListJobTitles');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['jobCategories'] = $this->model_system->getValues(TABLE_JOB_CATEGORY, "*", array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_job_title', $this->arrData);
		$this->template->render();		
	}	
	
	public function save_job_category($categoryID)
	{		
		$tblName = TABLE_JOB_CATEGORY;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$categoryID = (int)$categoryID;
		if($categoryID) {				
			$arrWhere = array(
				'job_category_id' => $categoryID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('jobCategory', 'Job Category', 'trim|required|min_length[5]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{				
				$table = TABLE_JOB_CATEGORY;
				$column = 'job_category_id';
				$Where = array(
							'job_category_name' => $this->input->post("jobCategory")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $categoryID);
				if($checkResult == YES)
				{
					if($categoryID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Job Category in Job Title
							$tableName = TABLE_JOB_TITLE;
							
							$Where = array(
								'job_category_id' => $categoryID,
								'job_title_status' => STATUS_ACTIVE
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Job Category Status can not be changed as it is used for an Active Job Title.';
							}
							else
							{
								$arrValues = array(
									'job_category_name' => $this->input->post("jobCategory"),
									'job_category_status' => (int)$status,
									'modified_by' => (int)$this->userID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
							}
						}
						else
						{
							$arrValues = array(
								'job_category_name' => $this->input->post("jobCategory"),
								'job_category_status' => (int)$status,
								'modified_by' => (int)$this->userID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
								'job_category_name' => $this->input->post("jobCategory"),
								'job_category_status' => (int)$status,
								'created_by' => (int)$this->userID,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Job Category you entering already exists.';
				}	
			}
						
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Job Category saved successfully');
						redirect(base_url() . $this->currentController . '/list_job_category');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($categoryID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_JOB_CATEGORY, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['jobCategories'] = $this->model_system->getValues(TABLE_JOB_CATEGORY, "*", array('job_category_status' => 1, 'order_by' => 'job_category_name'));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_job_category', $this->arrData);
		$this->template->render();		
	}
	
	public function list_job_category($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Job Category in Job Title
				$tableName = TABLE_JOB_TITLE;
				
				$Where = array(
					'job_category_id' => $this->input->post("record_id"),
					'job_title_status' => STATUS_ACTIVE
					);
				
				$resultCategory = $this->model_system->checkValues($tableName, $Where);
				if($resultCategory == YES)
				{
					$tblName = TABLE_JOB_CATEGORY;
					
					$arrDeleteWhere = array(
						'job_category_id' => $this->input->post("record_id")
						);
						
					$arrDeleteValues = array(
						'job_category_status' => STATUS_DELETED,
						'deleted_by' => $this->userID,
						'deleted_date' => date($this->arrData["dateTimeFormat"])
						);
					
					if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
						echo "0"; exit;
					} else {
						echo "1"; exit;
					}
				}
				else
				{
					echo "2"; exit;
				}
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['job_category_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalJobCategories($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getJobCategories($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListJobCategories');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['jobCategories'] = $this->model_system->getValues(TABLE_JOB_CATEGORY, "*", array('job_category_status' => 1, 'order_by' => 'job_category_name'));
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_job_category', $this->arrData);
		$this->template->render();		
	}	
		
	public function save_work_shift($shiftID)
	{		
		$tblName = TABLE_WORK_SHIFT;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$shiftID = (int)$shiftID;
		if($shiftID) {				
			$arrWhere = array(
				'shift_id' => $shiftID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('workShift', 'Work Shift', 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('workShiftFrom', 'Work Shift From', 'trim|required|xss_clean');
		$this->form_validation->set_rules('workShiftTo', 'Work Shift To', 'trim|required|xss_clean');
		$this->form_validation->set_rules('workShiftHours', 'Work Shift Hours', 'trim|required|min_length[1]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{	
				$table = TABLE_WORK_SHIFT;
				$column = 'shift_id';
				$Where = array(
							'shift_name' => $this->input->post("workShift")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $shiftID);
				if($checkResult == YES)
				{		
					if($shiftID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Work Shift in Job Vacancy
							$tableName = TABLE_VACANCY;
							
							$Where = array(
								'work_shift' => $shiftID,
								'vacancy_status' => STATUS_ACTIVE
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Work Shift Status can not be changed as it is used for an Active Job Vacancy.';
							}
							else{
								# Check this Work Shift in Employee
								$tableName = TABLE_EMPLOYEE;
								
								$Where = array(
									'emp_work_shift_id' => $shiftID,
									'emp_status' => STATUS_ACTIVE
									);
								
								if(!$this->model_system->checkValues($tableName, $Where))
								{
									$this->arrData['error_message'] = 'Work Shift Status can not be changed as it is used for an Active Employee.';
								}
								else
								{
									$arrValues = array(
										'shift_name' => $this->input->post("workShift"),
										'shift_hours_from' => $this->input->post("workShiftFrom"),
										'shift_hours_to' => $this->input->post("workShiftTo"),
										'shift_duration' => $this->input->post("workShiftHours"),
										'shift_status' => (int)$status,
										'modified_by' => (int)$this->userID,
										'modified_date' => date($this->arrData["dateTimeFormat"])
									);
								}
							}
						}
						else
						{
							$arrValues = array(
								'shift_name' => $this->input->post("workShift"),
								'shift_hours_from' => $this->input->post("workShiftFrom"),
								'shift_hours_to' => $this->input->post("workShiftTo"),
								'shift_duration' => $this->input->post("workShiftHours"),
								'shift_status' => (int)$status,
								'modified_by' => (int)$this->userID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
							'shift_name' => $this->input->post("workShift"),
							'shift_hours_from' => $this->input->post("workShiftFrom"),
							'shift_hours_to' => $this->input->post("workShiftTo"),
							'shift_duration' => $this->input->post("workShiftHours"),
							'shift_status' => (int)$status,
							'created_by' => (int)$this->userID,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Work Shift you entering already exists.';
				}
			}
			
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Work Shift saved successfully');
						redirect(base_url() . $this->currentController . '/list_work_shift');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($shiftID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_WORK_SHIFT, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['shift_timings'] = $this->config->item("shift_timings");
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_work_shift', $this->arrData);
		$this->template->render();		
	}
		
	public function list_work_shift($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Work Shift in Job Vacancy
				$tableName = TABLE_VACANCY;
				
				$Where = array(
					'work_shift' => $this->input->post("record_id"),
					'vacancy_status' => STATUS_ACTIVE
					);
								
				$resultWorkShift1 = $this->model_system->checkValues($tableName, $Where);
				if($resultWorkShift1 == YES)
				{				
					# Check this Work Shift in Employee
					$tableName = TABLE_EMPLOYEE;
					
					$Where = array(
						'emp_work_shift_id' => $this->input->post("record_id"),
						'emp_status' => STATUS_ACTIVE
						);
						
					$resultWorkShift2 = $this->model_system->checkValues($tableName, $Where);	
					if($resultWorkShift2 == YES)
					{
						$tblName = TABLE_WORK_SHIFT;
						
						$arrDeleteWhere = array(
							'shift_id' => $this->input->post("record_id")
							);
							
						$arrDeleteValues = array(
							'shift_status' => STATUS_DELETED,
							'deleted_by' => $this->userID,
							'deleted_date' => date($this->arrData["dateTimeFormat"])
							);
						
						if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
							echo "0"; exit;
						} else {
							echo "1"; exit;
						}
					}
					else { echo "2"; exit; }
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['shift_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalWorkShifts($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getWorkShifts($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListWorkShift');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_work_shift', $this->arrData);
		$this->template->render();		
	}
		
	public function save_language($languageID)
	{		
		$tblName = TABLE_LANGUAGE;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$languageID = (int)$languageID;
		if($languageID) {				
			$arrWhere = array(
				'language_id' => $languageID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('language', 'Language', 'trim|required|min_length[3]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{				
				$table = TABLE_LANGUAGE;
				$column = 'language_id';
				$Where = array(
							'language_name' => $this->input->post("language")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $languageID);
				if($checkResult == YES)
				{
					if($languageID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Language in Employee
							$tableName = TABLE_EMPLOYEE_LANGUAGE;
							
							$Where = array(
								'language_id' => $languageID
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Language Status can not be changed as it is used for an Active Employee.';
							}
							else
							{
								$arrValues = array(
									'language_name' => $this->input->post("language"),
									'language_status' => (int)$status,
									'modified_by' => (int)$this->userID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
							}
						}
						else
						{
							$arrValues = array(
								'language_name' => $this->input->post("language"),
								'language_status' => (int)$status,
								'modified_by' => (int)$this->userID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
								'language_name' => $this->input->post("language"),
								'language_status' => (int)$status,
								'created_by' => (int)$this->userID,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Language you entering already exists.';
				}
			}
						
			if(!empty($arrValues))
			{			
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Language saved successfully');
						redirect(base_url() . $this->currentController . '/list_language');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($languageID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_LANGUAGE, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_language', $this->arrData);
		$this->template->render();		
	}
	
	public function list_language($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Language in Employee
				$tableName = TABLE_EMPLOYEE_LANGUAGE;
				
				$Where = array(
					'language_id' => $this->input->post("record_id")
					);
				
				$resultModule = $this->model_system->checkValues($tableName, $Where);
				if($resultModule == YES)
				{				
					$tblName = TABLE_LANGUAGE;
					
					$arrDeleteWhere = array(
						'language_id' => $this->input->post("record_id")
						);
						
					$arrDeleteValues = array(
						'language_status' => STATUS_DELETED,
						'deleted_by' => $this->userID,
						'deleted_date' => date($this->arrData["dateTimeFormat"])
						);
					
					if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
						echo "0"; exit;
					} else {
						echo "1"; exit;
					}
				}
				else { echo "2"; exit;}
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['language_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalLanguages($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getLanguages($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListLanguages');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_language', $this->arrData);
		$this->template->render();		
	}
	
	public function save_edu_level($eduLevelID)
	{		
		$tblName = TABLE_EDUCATION_LEVELS;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$eduLevelID = (int)$eduLevelID;
		if($eduLevelID) {				
			$arrWhere = array(
				'edu_level_id' => $eduLevelID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('educationLevel', 'Education Level', 'trim|required|min_length[5]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{	
				$table = TABLE_EDUCATION_LEVELS;
				$column = 'edu_level_id';
				$Where = array(
							'edu_level_name' => $this->input->post("educationLevel")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $eduLevelID);
				if($checkResult == YES)
				{		
					if($eduLevelID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Education Level in Candidate Education
							$tableName = TABLE_CANDIDATE_EDUCATION;
					
							$Where = array(
								'edu_level_id' => $eduLevelID
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Education Level Status can not be changed as it is used for an Active Candidate Education.';
							}
							else{
								# Check this Education Level in Employee Education
								$tableName = TABLE_EMPLOYEE_EDUCATION;
								
								$Where = array(
									'edu_level_id' => $eduLevelID
									);
								
								if(!$this->model_system->checkValues($tableName, $Where))
								{
									$this->arrData['error_message'] = 'Education Level Status can not be changed as it is used for an Active Employee Education.';
								}
								else
								{
									$arrValues = array(
										'edu_level_name' => $this->input->post("educationLevel"),
										'edu_level_status' => (int)$status,
										'modified_by' => (int)$this->userID,
										'modified_date' => date($this->arrData["dateTimeFormat"])
									);
								}
							}
						}
						else
						{
							$arrValues = array(
								'edu_level_name' => $this->input->post("educationLevel"),
								'edu_level_status' => (int)$status,
								'modified_by' => (int)$this->userID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
							'edu_level_name' => $this->input->post("educationLevel"),
							'edu_level_status' => (int)$status,
							'created_by' => (int)$this->userID,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Education Level you entering already exists.';
				}
			}
			
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Education Level saved successfully');
						redirect(base_url() . $this->currentController . '/list_edu_level');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($eduLevelID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_EDUCATION_LEVELS, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
						
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_edu_level', $this->arrData);
		$this->template->render();		
	}
	
	public function list_edu_level($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Education Level in Candidate Education
				$tableName = TABLE_CANDIDATE_EDUCATION;
				
				$Where = array(
					'edu_level_id' => $this->input->post("record_id")
					);
								
				$resultEduLevel1 = $this->model_system->checkValues($tableName, $Where);
				if($resultEduLevel1 == YES)
				{				
					# Check this Education Level in Employee Education
					$tableName = TABLE_EMPLOYEE_EDUCATION;
					
					$Where = array(
						'edu_level_id' => $this->input->post("record_id")
						);
						
					$resultEduLevel2 = $this->model_system->checkValues($tableName, $Where);	
					if($resultEduLevel2 == YES)
					{
						$tblName = TABLE_EDUCATION_LEVELS;
						
						$arrDeleteWhere = array(
							'edu_level_id' => $this->input->post("record_id")
							);
							
						$arrDeleteValues = array(
							'edu_level_status' => STATUS_DELETED,
							'deleted_by' => $this->userID,
							'deleted_date' => date($this->arrData["dateTimeFormat"])
							);
						
						if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
							echo "0"; exit;
						} else {
							echo "1"; exit;
						}
					}
					else { echo "2"; exit; }
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['edu_level_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalEduLevels($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getEduLevels($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListEduLevels');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_edu_level', $this->arrData);
		$this->template->render();		
	}
	
	public function save_edu_major($eduMajorID)
	{		
		$tblName = TABLE_EDUCATION_MAJORS;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$eduMajorID = (int)$eduMajorID;
		if($eduMajorID) {				
			$arrWhere = array(
				'edu_major_id' => $eduMajorID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('educationMajor', 'Education Major', 'trim|required|min_length[5]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{	
				$table = TABLE_EDUCATION_MAJORS;
				$column = 'edu_major_id';
				$Where = array(
							'edu_major_name' => $this->input->post("educationMajor")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $eduMajorID);
				if($checkResult == YES)
				{		
					if($eduMajorID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Education Major in Candidate Education
							$tableName = TABLE_CANDIDATE_EDUCATION;
					
							$Where = array(
								'edu_major_id' => $eduMajorID
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Education Major Status can not be changed as it is used for an Active Candidate Education.';
							}
							else{
								# Check this Education Major in Employee Education
								$tableName = TABLE_EMPLOYEE_EDUCATION;
								
								$Where = array(
									'edu_major_id' => $eduMajorID
									);
								
								if(!$this->model_system->checkValues($tableName, $Where))
								{
									$this->arrData['error_message'] = 'Education Major Status can not be changed as it is used for an Active Employee Education.';
								}
								else
								{
									$arrValues = array(
										'edu_major_name' => $this->input->post("educationMajor"),
										'edu_major_status' => (int)$status,
										'modified_by' => (int)$this->userID,
										'modified_date' => date($this->arrData["dateTimeFormat"])
									);
								}
							}
						}
						else
						{
							$arrValues = array(
								'edu_major_name' => $this->input->post("educationMajor"),
								'edu_major_status' => (int)$status,
								'modified_by' => (int)$this->userID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
							'edu_major_name' => $this->input->post("educationMajor"),
							'edu_major_status' => (int)$status,
							'created_by' => (int)$this->userID,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Education Major you entering already exists.';
				}
			}
			
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Education Major saved successfully');
						redirect(base_url() . $this->currentController . '/list_edu_major');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($eduMajorID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_EDUCATION_MAJORS, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
						
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_edu_major', $this->arrData);
		$this->template->render();		
	}
	
	public function list_edu_major($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Education Major in Candidate Education
				$tableName = TABLE_CANDIDATE_EDUCATION;
				
				$Where = array(
					'edu_major_id' => $this->input->post("record_id")
					);
								
				$resultEduMajor1 = $this->model_system->checkValues($tableName, $Where);
				if($resultEduMajor1 == YES)
				{				
					# Check this Education Major in Employee Education
					$tableName = TABLE_EMPLOYEE_EDUCATION;
					
					$Where = array(
						'edu_major_id' => $this->input->post("record_id")
						);
						
					$resultEduMajor2 = $this->model_system->checkValues($tableName, $Where);	
					if($resultEduMajor2 == YES)
					{
						$tblName = TABLE_EDUCATION_MAJORS;
						
						$arrDeleteWhere = array(
							'edu_major_id' => $this->input->post("record_id")
							);
							
						$arrDeleteValues = array(
							'edu_major_status' => STATUS_DELETED,
							'deleted_by' => $this->userID,
							'deleted_date' => date($this->arrData["dateTimeFormat"])
							);
						
						if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
							echo "0"; exit;
						} else {
							echo "1"; exit;
						}
					}
					else { echo "2"; exit; }
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['edu_major_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalEduMajors($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getEduMajors($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListEduMajors');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_edu_major', $this->arrData);
		$this->template->render();		
	}
	
	public function save_grades($gradeID)
	{		
		$tblName = TABLE_GRADES;
		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$gradeID = (int)$gradeID;
		if($gradeID) {				
			$arrWhere = array(
				'grade_id' => $gradeID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('gradeCode', 'Grade Code', 'trim|required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('gradeTitle', 'Grade Title', 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('designation', 'Designation', 'trim|required|xss_clean');
		$this->form_validation->set_rules('salaryRangeStart', 'Salary Range Start', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('salaryRangeEnd', 'Salary Range End', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('gradeCriteria', 'Grade Criteria', 'trim|xss_clean');
		$this->form_validation->set_rules('gradeBenefits', 'Grade Benefits', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{	
				$table = TABLE_GRADES;
				$column = 'grade_id';
				$Where = array(
							'grade_code' => $this->input->post("gradeCode"),
							'grade_title' => $this->input->post("gradeTitle")
							);
				$checkResult = $this->model_system->checkValues($table, $Where, $column, $gradeID);
				if($checkResult == YES)
				{		
					if($gradeID)
					{
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						if($status == STATUS_INACTIVE_VIEW || $status == STATUS_DELETED)
						{
							# Check this Grade in Employment Details
							$tableName = TABLE_EMPLOYEE;
					
							$Where = array(
								'emp_grade_id' => $gradeID
								);
							
							if(!$this->model_system->checkValues($tableName, $Where))
							{
								$this->arrData['error_message'] = 'Grade Status can not be changed as it is used for an Active Employee Record.';
							}
							else
							{
								$arrValues = array(
									'grade_code' => $this->input->post("gradeCode"),
									'grade_title' => $this->input->post("gradeTitle"),
									'job_type' => $this->input->post("designation"),
									'salary_range_start' => $this->input->post("salaryRangeStart"),
									'salary_range_end' => $this->input->post("salaryRangeEnd"),
									'grade_criteria' => $this->input->post("gradeCriteria"),
									'grade_benefits' => $this->input->post("gradeBenefits"),
									'grade_status' => (int)$status,
									'modified_by' => (int)$this->userID,
									'modified_date' => date($this->arrData["dateTimeFormat"])
								);
							}
						}
						else
						{
							$arrValues = array(
								'grade_code' => $this->input->post("gradeCode"),
								'grade_title' => $this->input->post("gradeTitle"),
								'job_type' => $this->input->post("designation"),
								'salary_range_start' => $this->input->post("salaryRangeStart"),
								'salary_range_end' => $this->input->post("salaryRangeEnd"),
								'grade_criteria' => $this->input->post("gradeCriteria"),
								'grade_benefits' => $this->input->post("gradeBenefits"),
								'grade_status' => (int)$status,
								'modified_by' => (int)$this->userID,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
						}
					}
					else 
					{					
						$status = $this->input->post("status");
						if($status == STATUS_INACTIVE) {
							$status = STATUS_INACTIVE_VIEW;
						}
						
						$arrValues = array(
							'grade_code' => $this->input->post("gradeCode"),
							'grade_title' => $this->input->post("gradeTitle"),
							'job_type' => $this->input->post("designation"),
							'salary_range_start' => $this->input->post("salaryRangeStart"),
							'salary_range_end' => $this->input->post("salaryRangeEnd"),
							'grade_criteria' => $this->input->post("gradeCriteria"),
							'grade_benefits' => $this->input->post("gradeBenefits"),
							'grade_status' => (int)$status,
							'created_by' => (int)$this->userID,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Band & Grade details you entering already exists.';
				}
			}
			
			if(!empty($arrValues))
			{
				if(!$this->model_system->saveValues($tblName, $arrValues, $arrWhere)) {
						$this->arrData['error_message'] = 'Data not saved, try again';
					} else {
						$this->session->set_flashdata('success_message', 'Band & Grade saved successfully');
						redirect(base_url() . $this->currentController . '/list_grades');
						exit;
					}
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		$this->arrData['Designations'] = $this->model_system->getValues(TABLE_DESIGNATIONS, "*", array('designation_status' => 1));
		if($gradeID) {
			$this->arrData['record'] = $this->model_system->getValues(TABLE_GRADES, "*", $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
						
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/save_grades', $this->arrData);
		$this->template->render();		
	}
	
	public function list_grades($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				# Check this Grade in Employment Details
				$tableName = TABLE_EMPLOYEE;
				
				$Where = array(
					'emp_grade_id' => $this->input->post("record_id")
					);
								
				$resultGrade = $this->model_system->checkValues($tableName, $Where);
				if($resultGrade == YES)
				{	
					$tblName = TABLE_GRADES;
					
					$arrDeleteWhere = array(
						'grade_id' => $this->input->post("record_id")
						);
						
					$arrDeleteValues = array(
						'grade_status' => STATUS_DELETED,
						'deleted_by' => $this->userID,
						'deleted_date' => date($this->arrData["dateTimeFormat"])
						);
					
					if(!$this->model_system->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
						echo "0"; exit;
					} else {
						echo "1"; exit;
					}
				}
				else { echo "2"; exit; }
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == STATUS_INACTIVE) {
					$status = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['grade_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalGrades($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getGrades($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListGrades');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/job_detail/list_grades', $this->arrData);
		$this->template->render();		
	}
	
	public function save_religion($relID)
	{		
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$relID = (int)$relID;
		if($relID) {				
			$arrWhere = array(
				'religion_id' => $relID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('religionName', 'Religion Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('selStatus', 'Status', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{
				$selStatus = $this->input->post("selStatus");
				if($selStatus == STATUS_INACTIVE) {
					$selStatus = STATUS_INACTIVE_VIEW;
				}
						
				$arrValues = array(
								'religion_name' => $this->input->post("religionName"),
								'religion_status' => $selStatus
							);
								
				if(!$relID)
				{
					$arrValues['created_by'] = $this->userID;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
					$result = $this->model_system->saveValues(TABLE_RELIGIONS, $arrValues);
				}
				else
				{
					$arrValues['modified_by'] = $this->userID;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
					$result = $this->model_system->saveValues(TABLE_RELIGIONS, $arrValues, $arrWhere);
				}
				
				$this->session->set_flashdata('success_message', 'Religion saved successfully');
				redirect(base_url() . $this->currentController . '/list_religions');
				exit;
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($relID) {
			$this->arrData['record'] = $this->model_system->getReligions($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
						
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/save_religion', $this->arrData);
		$this->template->render();		
	}
	
	public function list_religions($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				$arrWhere = array(
					'religion_id' => $this->input->post("record_id")
					);
					
				if(!$this->model_system->deleteValue(TABLE_RELIGIONS, null, $arrWhere)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("selStatus")) {
				$selStatus = $this->input->post("selStatus");
				if($selStatus == STATUS_INACTIVE) {
					$selStatus = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['religion_status'] = $selStatus;
				$this->arrData['selStatus'] = $this->input->post("selStatus");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalReligions($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getReligions($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListReligions');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/list_religions', $this->arrData);
		$this->template->render();		
	}
	
	public function save_company($compID)
	{
		$arrWhere = array();
		$this->arrData['record'] = array();
				
		$compID = (int)$compID;
		if($compID) {				
			$arrWhere = array(
				'company_id' => $compID
				);
		}
		
		#################################### FORM VALIDATION START ##################################
		
		$this->form_validation->set_rules('comName', 'Company Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('comAddress', 'Company Address', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('comCountryID', 'Country', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('comCurrencyID', 'Currency', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('comHeadID', 'Company Head', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('comAnnualLeaves', 'Annual Leaves', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('comSickLeaves', 'Sick Leaves', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('comVisas', 'Visas', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('comStatus', 'Status', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) 
		{			
			if($this->input->post())
			{
				$comStatus = $this->input->post("comStatus");
				if($comStatus == STATUS_INACTIVE) {
					$comStatus = STATUS_INACTIVE_VIEW;
				}
						
				$arrValues = array(
								'company_name' => $this->input->post("comName"),
								'company_address' => $this->input->post("comAddress"),
								'company_country_id' => $this->input->post("comCountryID"),
								'company_currency_id' => $this->input->post("comCurrencyID"),
								'company_head_id' => $this->input->post("comHeadID"),
								'company_annual_leaves' => $this->input->post("comAnnualLeaves"),
								'company_sick_leaves' => $this->input->post("comSickLeaves"),
								'company_tax_applicable' => $this->input->post("comTaxApplicable"),
								'company_visas' => $this->input->post("comVisas"),
								'company_weekly_off_1' => $this->input->post("comWeeklyOff1"),
								'company_weekly_off_2' => $this->input->post("comWeeklyOff2"),
								'company_status' => $comStatus,
						
									//Code Added By Zeeshan Qasim
								'trade_license_issue_date' => $this->input->post("tradeLicenseIssue"),
								
								'trade_license_expiry_date' => $this->input->post("tradeLicenseExpiry"),
								
								'establishment_card_issue_date' => $this->input->post("establishmentCardIssue"),
								
								'establishment_card_expiry_date' => $this->input->post("establishmentCardExpiry"),
								
								'dubaichamber_issue_date' => $this->input->post("dubaiChamberIssue"),
								
								'dubaichamber_expiry_date' => $this->input->post("dubaiChamberExpiry"),
								
								'picover_issue_date' => $this->input->post("piCoverIssue"),
								
								
									'picover_expiry_date' => $this->input->post("piCoverExpiry"),
									
									'tenancycontract_issue_date' => $this->input->post("tenancyContractIssue"),
									
									'tenancycontract_expiry_date' => $this->input->post("tenancyContractExpiry"),
									'maintenance_contract_issue_date' => $this->input->post("maintenanceContractIssue"),
									'maintenance_contract_expiry_date' => $this->input->post("maintenanceContractExpiry"),
									'dcdfire_issue_date' => $this->input->post("dcdFireIssue"),
									'dcdfire_expiry_date' => $this->input->post("dcdFireExpiry"),
									'pobox_renewal_issue_date' => $this->input->post("poboxIssue"),
									'pobox_renewal_expiry_date' => $this->input->post("poboxExpiry"),
									'business_operation_issue_date' => $this->input->post("businessoperationIssue"),
									'business_operation_expiry_date' => $this->input->post("businessoperationExpiry"),
									'rera_issue_date' => $this->input->post("reraIssue"),
									'rera_expiry_date' => $this->input->post("reraExpiry")
									
									
									
								
								//Code Ended By Zeeshan Qasim
								
								
								
								
								
								
								
								
							);
								
				if(!$compID)
				{
					$arrValues['created_by'] = $this->userID;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
					$result = $this->model_system->saveValues(TABLE_COMPANIES, $arrValues);
				}
				else
				{
					$arrValues['modified_by'] = $this->userID;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
					$result = $this->model_system->saveValues(TABLE_COMPANIES, $arrValues, $arrWhere);
				}
				
				$this->session->set_flashdata('success_message', 'Company saved successfully');
				redirect(base_url() . $this->currentController . '/list_companies');
				exit;
			}
		}
		else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED JOB TITLE
		if($compID) {
			$this->arrData['record'] = $this->model_system->getCompanies($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}		
		
		$this->arrData['empSupervisors'] = $this->employee->getSupervisors();
		$this->arrData['Countries'] = $this->configuration->getLocations();
		$this->arrData['arrDays'] = $this->config->item('days');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/save_company', $this->arrData);
		$this->template->render();		
	}

//add code by zeeshan qasim
	public function docList_Real($companyID = 1)
	{
	 //echo  "test";
	 $getCompanies = $this->model_system->getCompanies();
		
		$companyID = (int)$companyID;
		
		if(!$companyID) {
			$companyID = $this->companyID;
		}
		//die($this->companyID);
		
		//$arrWhere['ed.company_id'] = $companyID;		
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {
				$arrRecord = $this->model_system->getCompaniesDocumentList(array('ed.doc_id' => (int)$this->input->post("record_id"), 'ed.company_id' => $companyID));
				if(!$this->model_system->deleteValue(TABLE_COMPANIES_DOCUMENTS, array('doc_id' => (int)$this->input->post("record_id"), 'company_id' => $companyID))) {
					echo NO; exit;
				} else {
					unlink($this->arrData["companyDocFolder"] . $arrRecord[0]['doc_file']);
					echo YES; exit;
				}								
			}
		}
		
		
		
		$arrWhere = array();
		
		#################################### FORM VALIDATION START ####################################		
		
		//$this->form_validation->set_rules('docType', 'Document Type', 'trim|required|numeric|xss_clean');
		//$this->form_validation->set_rules('companies', 'Company Name', 'trim|required|xss_clean');
		//if (empty($_FILES['docFile']['name'])) {
			//$this->form_validation->set_rules('docFile', 'Document', 'required');
		//}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->input->post()) {
			if($this->input->post("companies")) {
				$companyName = $this->input->post("companies");
				
				$arrWhere['company_id'] = $companyName;
				
			//	$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList(array('ed.company_id' => $companyID));
				
				///(array('ed.doc_id' => (int)$this->input->post("record_id"), 'ed.company_id' => $companyID));
				
				//$this->arrData['company_name'] = $this->input->post("companies");
				
				$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList(array('ed.company_id' => $companyName));
			
				//var_dump($this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList(array('ed.company_name' => $companyName)));
				
				
			//	die(companyID);
			}
		}
	
	//	if (is_null($companyName))
		//{
		
		//$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList();
		//echo "yes";
	//	}
		
		//var_dump($arrWhere);
		
		
	
//	var_dump($this->arrData['arrRecords']);
	
	
	
	//	$totalCount = $totalCount[0]['total'];
	//	$this->arrData['totalRecordsCount'] = $totalCount;
		//$offSet = ($pageNum - 1) * $this->limitRecords;
		//$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentListReal($arrWhere, $this->limitRecords, $offSet);
		//$numPages = ceil($totalCount / $this->limitRecords);
		//$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListCompanies');
		//$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		//$arrWhere['company_name'] = $companyName;
		
	
		//$arrWhere['ed.company_id'] = $companyID;
		//var_dump($this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList(array('ed.company_name' => $companyName)));
	//	$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList();
	//$this->arrData['arrDocTypes'] = $this->model_system->getValues(TABLE_COMPANIES_DOCUMENT_TYPES);
		
		//var_dump($this->arrData['arrRecords']);
		
		
		                         	
			
			
			
			if (is_null($companyName))
		{
		
		
		$totalCount = $this->model_system->getTotalCompaniesDocuments($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		//$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListCompanies');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
		$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList();
		//echo "yes";
		}
		
		$this->arrData['getCompanies'] = $getCompanies;
		
		
		# TEMPLATE LOADING
		//$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'system_configuration/docList_Real', $this->arrData);
		$this->template->render();
	
	
	
	}
	
	
	public function docList_Companies($docID)
	{
		//echo "testing";
		
		$getCompanies = $this->model_system->getCompanies();
		
		$docID = (int)$docID;
		//die($this->companyID);
		
		$arrWhere['ed.doc_id'] = $docID;
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('docType', 'Document Type', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('companyID', 'Company', 'trim|required|xss_clean');
		if (empty($_FILES['docFile']['name'])) {
			$this->form_validation->set_rules('docFile', 'Document', 'required');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		# CODE FOR POPULATING PAGE CONTENT
		if($this->form_validation->run() == true && $this->arrData['canWrite']) {
			
			#	DOCUMENT UPLOADING
			$uploadConfig['upload_path'] 	= $this->arrData["companyDocFolder"];
			$uploadConfig['allowed_types'] 	= '*'; //'jpg|jpeg|png|bmp|doc|docs|pdf|xls|xlsx';
			$uploadConfig['max_size']		= '12240';
			$uploadConfig['max_filename']	= '100';
			$uploadConfig['encrypt_name']	= true;

			$this->load->library('upload');
			$this->upload->initialize($uploadConfig);
			
			$docFileName = '';
			
			if(!$this->upload->do_upload('docFile')) {
				if(!empty($_FILES['docFile']['name'])) {
					$error = array('error' => $this->upload->display_errors());	
					$this->arrData['error_message'] = $error['error'];
					
				}					
			} else {				
				$dataUpload = $this->upload->data();
				$docFileName = basename($dataUpload['file_name']);
			}
			
			$arrValues = array(
								'doc_type_id' => $this->input->post("docType"),
								'company_id' => $this->input->post("companyID"),
								'doc_file' => $docFileName
							);
							
			if((int)$docID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				$resultID = $this->model_system->saveValues(TABLE_COMPANIES_DOCUMENTS, $arrValues, array('doc_id' => $docID));
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$resultID = $this->model_system->saveValues(TABLE_COMPANIES_DOCUMENTS, $arrValues);
			}
			
//die($arrValues);
      				
				if($resultID) {		

//die($resultID);
				
					$this->session->set_flashdata('success_message', 'Document Uploaded Successfully');
					//redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $companyID);
					redirect(base_url() . $this->currentController . '/docList_Real');
					exit;
				}
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		
		
		
		$this->arrData['arrRecords'] = $this->model_system->getCompaniesDocumentList(array('ed.doc_id' => $docID));
		$this->arrData['arrRecords'] = $this->arrData['arrRecords'][0];
		$this->arrData['arrDocTypes'] = $this->model_system->getValues(TABLE_COMPANIES_DOCUMENT_TYPES);
		
		//var_dump($this->arrData['arrRecords']);
		
		$this->arrData['getCompanies'] = $getCompanies;
		
		
		# TEMPLATE LOADING
		//$this->template->write_view('employee_box', 'templates/employee_box', $this->arrData);
		$this->template->write_view('content', 'system_configuration/document_checklist', $this->arrData);
		$this->template->render();
	
	//var_dump($this->arrData['arrRecords']);

		
		
		
	}
	
	
	//end code by zeeshan qasim
	
	public function list_companies($pageNum = 1)
	{
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == YES)
			{				
				$arrWhere = array(
					'company_id' => $this->input->post("record_id")
					);
					
				if(!$this->model_system->deleteValue(TABLE_COMPANIES, null, $arrWhere)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("selStatus")) {
				$selStatus = $this->input->post("selStatus");
				if($selStatus == STATUS_INACTIVE) {
					$selStatus = STATUS_INACTIVE_VIEW;
				}
				$arrWhere['company_status'] = $selStatus;
				$this->arrData['selStatus'] = $this->input->post("selStatus");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_system->getTotalCompanies($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_system->getCompanies($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListCompanies');
		$this->arrData['frmActionURL'] = base_url() . $this->currentController . '/' . $this->currentAction;
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'system_configuration/list_companies', $this->arrData);
		$this->template->render();		
	}
	
	public function system_settings($configID = 0)
	{		
		$tblName = TABLE_CONFIGURATION;
		
		#################################### FORM VALIDATION START ####################################
		
		$this->form_validation->set_rules('update_' . $configID, 'Value', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$strVal = $this->input->post('update_' . $configID);
			
			$arrValues = array(
								'config_value' => $strVal
							);
			$arrWhere = array(
								'config_id' => $configID
							);
							
			$this->model_system->saveValues($tblName, $arrValues, $arrWhere);
			$this->session->set_flashdata('success_message', 'Value Updated Successfully');
			redirect(base_url() . $this->currentController . '/' . $this->currentAction);	
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		$this->arrData['configID'] = $configID;
		$this->arrData['arrConfigs'] = $this->model_system->getSettings();
		$this->template->write_view('content', 'system_configuration/email_settings/save_mail_configuration', $this->arrData);
		$this->template->render();
	}
	
	public function numcheck($in, $value) {
		if (intval($in) <= 0) {
			if($value == 'moduleParent')
				$this->form_validation->set_message('numcheck', 'Select Parent Module.');
			else if ($value == 'layout')
				$this->form_validation->set_message('numcheck', 'Select Layout.');
			else if ($value == 'marketParent')
				$this->form_validation->set_message('numcheck', 'Select Parent Market.');
			else if ($value == 'locationType')
				$this->form_validation->set_message('numcheck', 'Select Location Type.');
			else if ($value == 'jobCategory')
				$this->form_validation->set_message('numcheck', 'Select Job Category.');
			else if ($value == 'jobType')
				$this->form_validation->set_message('numcheck', 'Select Designation Type.');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function validate_english($string,$value) 
	{
		if(preg_match("/((select|delete).+from|update.+set|(alter|truncate|drop).+table|<[a-z])/i", $string)==false)	//condition for sql injection
		{	
			if($value == 'md_english') 
			{ 
				$this->form_validation->set_message('validate_english', 'Module Display Name field must only contain letters.');
				return (preg_match("/^[a-zA-Z\/\s]*$/u", $string))?true:false;				
			}
			else if($value == 'm_english') 
			{ 
				$this->form_validation->set_message('validate_english', 'Module Name field must only contain letters.');
				return (preg_match("/^[a-zA-Z_\/\s]*$/u", $string))?true:false;				
			}
			else if($value == 'des_english') 
			{ 
				$this->form_validation->set_message('validate_english', 'Designation Name field must only contain letters.');
				return (preg_match("/^[a-zA-Z\/\s]*$/u", $string))?true:false;				
			}
			else if($value == 'mar_english') 
			{ 
				$this->form_validation->set_message('validate_english', 'Market Name field must only contain letters.');
				return (preg_match("/^[a-zA-Z\/\s]*$/u", $string))?true:false;				
			}
			else if($value == 'both') 
			{
				$this->form_validation->set_message('validate_english', 'User Name field must only contain letters and numbers.');
				return (preg_match("/^[a-zA-Z0-9.]*$/u", $string))?true:false;
			}
		} else return false;
	}
}

/* End of file system_configuration.php */
/* Location: ./application/controllers/system_configuration.php */