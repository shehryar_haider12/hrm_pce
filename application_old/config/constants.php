<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); 	// truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

# GENERAL CONSTANTS
define("STATUS_ACTIVE", 						"1");	//	Active Records
define("STATUS_INACTIVE", 						"-1");	//	Inactive Records
define("STATUS_INACTIVE_VIEW", 					"0");	//	Inactive Records
define("STATUS_DELETED", 						"2");	//	Deleted Records
define("STATUS_EXPIRED", 						"3");	//	Expired Records (RMS)
define("STATUS_BLACKLISTED", 					"4");	//	Blacklisted Records (RMS)
define("YES", "1");
define("NO", "0");
define("ZERO", "0");
define("OFFICE_TELEPHONE_NUMBER", 				"");

define("LOCATION_TYPE_REGION",					"1");
define("LOCATION_TYPE_COUNTRY",					"2");
define("LOCATION_TYPE_CITY",					"3");

define("STATUS_EMPLOYEE_INTERN",	 			1);
define("STATUS_EMPLOYEE_CONTRACT", 				2);
define("STATUS_EMPLOYEE_PROBATION", 			3);
define("STATUS_EMPLOYEE_CONFIRMED", 			4);
define("STATUS_EMPLOYEE_ONNOTICEPERIOD", 		5);
define("STATUS_EMPLOYEE_SEPARATED", 			6);
define("STATUS_EMPLOYEE_TERMINATED", 			7);


define("EXIT_MODULE_STEP_INITIATED", 			0);
define("EXIT_MODULE_STEP_MANAGER1", 			1);
define("EXIT_MODULE_STEP_HR1", 					2);
define("EXIT_MODULE_STEP_MANAGER2", 			3);
define("EXIT_MODULE_STEP_ITWEB", 				4);
define("EXIT_MODULE_STEP_NETWORK", 				5);
define("EXIT_MODULE_STEP_ADMIN", 				6);
define("EXIT_MODULE_STEP_HR2", 					7);
define("EXIT_MODULE_STEP_FINANCE", 				8);
define("EXIT_MODULE_STEP_COMPLETED", 			9);
define("EXIT_MODULE_STEP_CANCELED_RETAINED",	100);
define("EXIT_MODULE_STEP_RETAINED_BY_MANAGER",	101);
define("EXIT_MODULE_STEP_RETAINED_BY_HR",		102);

define('EMPLOYEE_ROLE_ID',						'1');
define('HR_ADMIN_ROLE_ID', 						'2');
define('SUPER_ADMIN_ROLE_ID', 					'12');
define('WEB_ADMIN_ROLE_ID', 					'10');
define('HR_EMPLOYEE_ROLE_ID', 					'13');
define('HR_MANAGER_ROLE_ID', 					'14');
define('ACCOUNT_MANAGER_ROLE_ID', 				'15');
define('ACCOUNT_EMPLOYEE_ROLE_ID', 				'16');
define('DOCUMENT_MANAGER_ROLE_ID', 				'17');
define('COMPANY_ADMIN_ROLE_ID', 				'18');
define('REQUEST_TO_DEPARTMENT_CATEGORY_IDS',	'18,19');
define('SUPERVISOR_ROLE_ID',	'20');

define('WEB_ADMIN_EMP_ID',						'1');
define('WEB_ADMINISTRATOR',						'Web Administrator');

# FOLDERS & PATHS
//ATTENDANCE FOLDER CHANGED FROM ATTENDANCE_DB_HOST_PK 192.168.3.206 TO LOCALHOST ON LINE 96
define('localhost', 			            	'localhost');
define('ATTENDANCE_DB_HOST_DXB', 				'192.168.2.230');
define('EVENTS_IMAGES_FOLDER', 					'./images/events/');
define('EMAIL_TEMPLATE_FOLDER', 				'./email_templates/');
define('RESUME_FOLDER', 						'./resume/');
define('ANNOUNCEMENT_FILES_FOLDER', 			'./pdfs/announcements/');
define('ATTENDANCE_LEAVES_DOCS_FOLDER', 		'./docs/leaves/');
//Change Code By Zeeshan Qasim
define('COMPANY_DOCS_FOLDER', 				    './docs/companies/');
//Change Code End By Zeeshan Qasim
define('EMP_DOCS_FOLDER', 						'./docs/employment/');
define('COMPLAIN_SUPPORTING_DOCS_FOLDER', 		'./docs/complain/');
define('TASKS_SUPPORTING_DOCS_FOLDER', 			'./docs/tasks/');
define('GST_CERTIFICATE_FOLDER', 				'./docs/inventory/gst');
define('LM_ACTIVITY_DOCS_FOLDER', 				'./docs/emp_activities/');
define('PDF_FILES_FOLDER', 						'pdfs/');
define('PERFORMANCE_PDF_FILES_FOLDER', 			'pdfs/performance/');
define('POLL_HEADERS_FILE_FOLDER', 				'./images/modules/forum/poll_headers/');
define('PROFILE_PICTURE_FOLDER', 				'./emp_pictures/');
define('QC_REPORTS_FOLDER', 					'./docs/qc_reports/');
define('BULK_SMS_FILE_FOLDER', 					'./docs/sms_files/');
define('TRAINING_DOCS_PATH', 					'/docs/training');
define('EMAIL_SUBJECT_SUFFIX', 					' - PCE HRM');

# RECRUITMENT MANAGEMENT
define('CONSTANT_REFERRED_BY',					'Referred By');
define('CONSTANT_REFERRED_BY_SBT_EMPLOYEE',		'Referred By Employee');
define('CONSTANT_LAST_ARRIVAL_SOURCE',			'Other');

# COMPLAIN CONSTANTS
define("OPEN", "1");
define("IN_PROGRESS", "2");
define("RESOLVED", "3");
define("UNRESOLVED", "4");
define("COMPLETED", "3");
define("INCOMPLETED", "4");

define("TYPE_SUGGESTION", "1");
define("TYPE_COMPLAIN", "2");
define("TYPE_REQUEST", "3");

define("URGENT_PRIORITY", "1");
define("IMPORTANT_PRIORITY", "2");
define("NORMAL_PRIORITY", "3");

# PERFORMANCE MANAGEMENT CONSTANTS
define("STATUS_ENTERING_TASKS", "0");
define("STATUS_SUBMIT_TASKS_FOR_REVIEW", "1");
define("STATUS_SUBMIT_REVIEW_TO_HR", "2");

# KPI ASSESSMENT CONSTANTS
define("STATUS_SUPERVISOR_ENTERING_KPIS", "0");
define("STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS", "1");
define("STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS", "2");
define("STATUS_KPIS_SUBMITTED_TO_HR", "3");

# PROBATION ASSESSMENT CONSTANTS
define("STATUS_SUPERVISOR_ENTERING_DETAILS", "0");
define("STATUS_EMPLOYEE_ENTERING_REMARKS", "1");
define("STATUS_SUBMITTED_TO_HR", "2");

# FORUM CONSTANTS
define("VISIBILITY_STATUS_ACTIVE", "1");
define("VISIBILITY_STATUS_INACTIVE", "2");
define("STATUS_INPROGRESS", "1");
define("STATUS_APPROVAL_AND_PUBLISH", "2");
define("STATUS_APPROVE_AND_PUBLISH", "3");
define("STATUS_DISAPPROVE", "4");
define("STATUS_CLOSE", "5");
define("RESULT_SHOW", "1");
define("RESULT_HIDE", "0");


/* End of file constants.php */
/* Location: ./application/config/constants.php */