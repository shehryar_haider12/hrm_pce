<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

# LOCAL

//$db['default']['hostname'] = 'localhost';
//$db['default']['username'] = 'root';
//$db['default']['password'] = '3<8mjMcf&b(+Vd~?';
//$db['default']['database'] = 'hrm';
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'pce';
$db['default']['password'] = 'Pce#123.456';
$db['default']['database'] = 'hrm13';
//$db['default']['dsn'] = '';

$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

define('TABLE_PREFIX', 'hrm_');

#	SYSTEM CONFIGURATION

define('TABLE_MODULE', TABLE_PREFIX . 'module');
define('TABLE_JOB_TITLE', TABLE_PREFIX . 'job_title');
define('TABLE_JOB_CATEGORY', TABLE_PREFIX . 'job_category');
define('TABLE_LAYOUT', TABLE_PREFIX . 'layout');
define('TABLE_LOCATION', TABLE_PREFIX . 'location');
define('TABLE_LANGUAGE', TABLE_PREFIX . 'language');
define('TABLE_CONFIGURATION', TABLE_PREFIX . 'configuration');
define('TABLE_DEPARTMENT', TABLE_PREFIX . 'department');
define('TABLE_EDUCATION_LEVELS', TABLE_PREFIX . 'edu_levels');
define('TABLE_EDUCATION_MAJORS', TABLE_PREFIX . 'edu_majors');
define('TABLE_WORK_SHIFT', TABLE_PREFIX . 'work_shift');
define('TABLE_GRADES', TABLE_PREFIX . 'grades');
define('TABLE_DESIGNATIONS', TABLE_PREFIX . 'designations');
define('TABLE_RELIGIONS', TABLE_PREFIX . 'religions');
define('TABLE_COMPANIES', TABLE_PREFIX . 'companies');
//Change Code By Zeeshan Qasim
define('TABLE_COMPANIES_DOCUMENTS', TABLE_PREFIX . 'companies_documents');
define('TABLE_COMPANIES_DOCUMENT_TYPES', TABLE_PREFIX . 'companies_document_types');
//Change Code End By Zeeshan Qasim
define('TABLE_BANKS', TABLE_PREFIX . 'banks');

# 	USER MANAGEMENT

define('TABLE_USER', TABLE_PREFIX . 'user');
define('TABLE_USER_PERMISSIONS', TABLE_PREFIX . 'user_permissions');
define('TABLE_USER_SPECIAL_PERMISSIONS', TABLE_PREFIX . 'user_special_permissions');
define('TABLE_USER_ROLE', TABLE_PREFIX . 'user_role');
define('TABLE_USER_LOGS', TABLE_PREFIX . 'user_logs');

# 	RECRUITMENT MANAGEMENT

define('TABLE_VACANCY', TABLE_PREFIX . 'job_vacancy');
define('TABLE_CANDIDATE', TABLE_PREFIX . 'job_candidate');
define('TABLE_CANDIDATE_CALLS', TABLE_PREFIX . 'job_candidate_calls');
define('TABLE_CANDIDATE_WORK_EXPERIENCE', TABLE_PREFIX . 'job_candidate_work_experience');
define('TABLE_CANDIDATE_EDUCATION', TABLE_PREFIX . 'job_candidate_education');
define('TABLE_CANDIDATE_HISTORY', TABLE_PREFIX . 'job_candidate_history');
define('TABLE_CANDIDATE_STATUS', TABLE_PREFIX . 'job_candidate_status');
define('TABLE_CANDIDATE_STATUS_ORDER', TABLE_PREFIX . 'job_candidate_status_order');
define('TABLE_CANDIDATE_VACANCY', TABLE_PREFIX . 'job_candidate_vacancy');
define('TABLE_CANDIDATE_INTERVIEW', TABLE_PREFIX . 'job_candidate_interview');
define('TABLE_CANDIDATE_INTERVIEW_INTERVIEWER', TABLE_PREFIX . 'job_candidate_interview_interviewer');
define('TABLE_CANDIDATE_APPLICATION', TABLE_PREFIX . 'job_candidate_application');
define('TABLE_EMPLOYEE_REQUISITION', TABLE_PREFIX . 'job_emp_requisition');
define('TABLE_EMPLOYMENT_STATUS', TABLE_PREFIX . 'employment_status');

#	EMPLOYEE MANAGEMENT

define('TABLE_EMPLOYEE', TABLE_PREFIX . 'employee');
define('TABLE_EMPLOYEE_EDUCATION', TABLE_PREFIX . 'employee_education');
define('TABLE_EMPLOYEE_WORK_EXPERIENCE', TABLE_PREFIX . 'employee_work_experience');
define('TABLE_EMPLOYEE_LANGUAGE', TABLE_PREFIX . 'employee_language');
define('TABLE_EMPLOYEE_DEPENDENTS', TABLE_PREFIX . 'employee_dependents');
define('TABLE_EMPLOYEE_EMERGENCY_CONTACTS', TABLE_PREFIX . 'employee_emergency_contacts');
define('TABLE_EMPLOYEE_DOCUMENTS', TABLE_PREFIX . 'employee_documents');
define('TABLE_EMPLOYEE_DOCUMENT_TYPES', TABLE_PREFIX . 'employee_document_types');
define('TABLE_EMPLOYEE_BENEFITS', TABLE_PREFIX . 'employee_benefits');
define('TABLE_EMPLOYEE_SPONSOR', TABLE_PREFIX . 'employee_sponsor');
define('TABLE_EMPLOYEE_SUPERVISORS', TABLE_PREFIX . 'employee_supervisors');
define('TABLE_EMPLOYEE_PAYROLL', TABLE_PREFIX . 'employee_payroll');

#	COMPLAIN MANAGEMENT

define('TABLE_EMPLOYEE_COMPLAINS', TABLE_PREFIX . 'employee_complains');
define('TABLE_EMPLOYEE_COMPLAINS_HISTORY', TABLE_PREFIX . 'employee_complains_history');
define('TABLE_EMPLOYEE_TASKS', TABLE_PREFIX . 'employee_tasks');
define('TABLE_EMPLOYEE_TASKS_HISTORY', TABLE_PREFIX . 'employee_tasks_history');

#	ATTENDANCE MANAGEMENT

define('TABLE_ATTENDANCE_DXB', 'Mx_DATDTrn');
define('TABLE_ATTENDANCE_CLOCKING_DXB', 'Mx_ATDEventTrn');
define('TABLE_ATTENDANCE_PK', 'CHECKINOUT');
define('TABLE_ATTENDANCE', 'attendance');
define('TABLE_ATTENDANCE_NOTES', TABLE_PREFIX . 'attendance_notes');
define('TABLE_ATTENDANCE_CLOCKING', TABLE_PREFIX . 'clocking');
define('TABLE_ATTENDANCE_SCHEDULE', TABLE_PREFIX . 'attendance_schedule');
define('TABLE_ATTENDANCE_SUMMARY', TABLE_PREFIX . 'attendance_summary');
define('TABLE_ATTENDANCE_WEEKEND_SCHEDULE', TABLE_PREFIX . 'emp_weekend_schedule');
define('TABLE_ATTENDANCE_LEAVES', TABLE_PREFIX . 'employee_leaves');
define('TABLE_ATTENDANCE_LEAVE_CATEGORIES', TABLE_PREFIX . 'leave_categories');
define('TABLE_ATTENDANCE_TIMING', TABLE_PREFIX . 'attendance');


#	INVENTORY MANAGEMENT

define('TABLE_INVENTORY_PRODUCTS', TABLE_PREFIX . 'inv_products');
define('TABLE_INVENTORY_VENDORS', TABLE_PREFIX . 'inv_vendors');
define('TABLE_INVENTORY_VENDORS_PRODUCTS', TABLE_PREFIX . 'inv_vendors_products');
define('TABLE_INVENTORY_PRODUCT_TYPES', TABLE_PREFIX . 'inv_product_types');
define('TABLE_INVENTORY_PRODUCT_FREQUENCY', TABLE_PREFIX . 'inv_product_frequency');
define('TABLE_INVENTORY_PRODUCT_METRICS', TABLE_PREFIX . 'inv_product_metrics');

#	POLICY ANNOUCEMENT

define('TABLE_ANNOUNCEMENTS', TABLE_PREFIX . 'announcements');
define('TABLE_PUBLIC_HOLIDAYS', TABLE_PREFIX . 'public_holidays');

#	TASK MANAGEMENT

define('TABLE_TASK_EMPLOYEE_PERFORMANCE', TABLE_PREFIX . 'employee_performance');
define('TABLE_TASK_EMPLOYEE_PERFORMANCE_KPIS', TABLE_PREFIX . 'employee_performance_kpis');
define('TABLE_PROBATION_ASSESSMENT', TABLE_PREFIX . 'employee_probation_assessment');

#	VALUSTRAT FORUM

define('TABLE_FORUM_EVENTS', TABLE_PREFIX . 'forum_events');
define('TABLE_FORUM_EVENTS_IMAGES', TABLE_PREFIX . 'forum_events_images');


/* End of file database.php */
/* Location: ./application/config/database.php */
