<?php
class Model_Sbt_Forum extends Model_Master {
	
	function __construct() {
		 parent::__construct();	
	}
		
	function getBirthdayDetails($arrWhere = array())
	{
		$this->db->select(' e.emp_full_name,e.emp_code,e.emp_gender,e.emp_photo_name,emp_dob,jc.job_category_name ');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');	
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$results = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getEvents($arrWhere = array(),  $rowsLimit = '', $rowsOffset = '')
	{		
		$this->db->select(' e.*, c.company_name ');
		$this->db->join(TABLE_COMPANIES . ' c ', 'c.company_id = e.company_id', 'left');
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('e.event_date', 'DESC');
		
		$results = $this->db->get(TABLE_FORUM_EVENTS . ' e ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getEventDetails($eventID)
	{		
		$this->db->select(' e.*, c.company_name ');
		$this->db->join(TABLE_COMPANIES . ' c ', 'c.company_id = e.company_id', 'left');
				
		$this->db->where(array('e.event_id' => (int)$eventID));
		
		$results = $this->db->get(TABLE_FORUM_EVENTS . ' e ');
		$arrResult = $results->result_array();
		$results->free_result();
		$arrFinalResult[0] = $arrResult;
		
		$this->db->where(array('event_id' => (int)$eventID));
		$this->db->order_by('created_date', 'ASC');
		
		$results = $this->db->get(TABLE_FORUM_EVENTS_IMAGES);
		$arrResult = $results->result_array();
		$results->free_result();
		$arrFinalResult[1] = $arrResult;
		
		return $arrFinalResult;
	}
	
	function getTotalEvents($arrWhere = array())
	{		
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_COMPANIES . ' c ', 'c.company_id = e.company_id', 'left');
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$results = $this->db->get(TABLE_FORUM_EVENTS . ' e ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult[0]['total'];	
	}	
}
?>