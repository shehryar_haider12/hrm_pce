<?php
class Model_Profile extends Model_Master {
	
	private $tblNoDeletion				= array(
												TABLE_CANDIDATE,
												TABLE_VACANCY
												);
	
	function __construct() {
		 parent::__construct();	
	}
	
	function deleteValue($tblName, $arrDeleteValues = array())
	{
		if(count($arrDeleteValues)) {
			
			$this->db->where($arrDeleteValues);
			if(!in_array($this->tblNoDeletion, $tblName)) {
				$this->db->delete($tblName);
			} else {
				$arrValues = array(
									'status' => 2,
									'deleted_by' => $this->userEmpNum,
									'deleted_date' => date('Y-m-d')
									);
				$this->db->update($tblName, $arrValues, $arrDeleteValues);
			}
			return ($this->db->affected_rows() > 0);
			
		}
	}
	
	function getCandidateDetail($arrWhere = array()) {
		
		$this->db->select(' c.*, u.plain_password, u.password ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_USER . ' u ', 'u.candidate_id = c.candidate_id', 'left');
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		if(count($arrResult) == 1) {
			$arrResult = $arrResult[0];
		}
		
		return $arrResult;
	}
	
	function getCandidateApplicationDetail($arrWhere = array()) {
		
		$this->db->select(' a.*, u.plain_password, u.password ');
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_USER . ' u ', 'u.candidate_id = a.candidate_id', 'left');
		$objResult = $this->db->get(TABLE_CANDIDATE_APPLICATION . ' a');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		if(count($arrResult) == 1) {
			$arrResult = $arrResult[0];
		}
		
		return $arrResult;
	}
	
	function getCandidateID($arrWhere = array()) {
		
		$this->db->select(' c.candidate_id ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_USER . ' u ', 'u.candidate_id = c.candidate_id', 'left');
			
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		$arrResult = $arrResult[0];
		
		return (int)$arrResult['candidate_id'];
	}
	
	function getCandidateHistory($arrWhere = array()) {
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('candidate_history_id', 'ASC');
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateStatuses() {
		
		$objResult = $this->db->get(TABLE_CANDIDATE_STATUS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateVacancies($arrWhere = array()) {
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
			
		$objResult = $this->db->get(TABLE_CANDIDATE_VACANCY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getVacancies($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->select(' v.* ');
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('v.vacancy_id', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$this->db->order_by('v.vacancy_id', 'DESC');
		$objResult = $this->db->get(TABLE_VACANCY . ' v');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getOpenVacancies($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {		
		
		$this->db->distinct();
		$this->db->select(' v.*, jt.job_title_name, l.location_name, w.shift_name ');
		
		$this->db->join(TABLE_CANDIDATE_VACANCY . ' cv ', 'cv.vacancy_id = v.vacancy_id', 'left');
		$this->db->join(TABLE_JOB_TITLE . ' jt ', 'jt.job_title_id = v.job_title_code', 'left');
		$this->db->join(TABLE_WORK_SHIFT . ' w ', 'w.shift_id = v.work_shift', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = v.vacancy_location', 'left');
		
		//if(count($arrWhere)) {
		//	$this->db->where($arrWhere);
		//}		
		
		$this->db->where('v.vacancy_id not in ', '(select distinct vacancy_id from ' . TABLE_CANDIDATE_VACANCY . ' where candidate_id = '.$arrWhere['cv.candidate_id'].') ', false);
		$this->db->where('v.vacancy_status', '1');
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('v.vacancy_id', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_VACANCY . ' v');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getAllVacancies($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {	
		
		$this->db->select(' v.*, l.location_name, w.shift_name ');
		$this->db->join(TABLE_WORK_SHIFT . ' w ', 'w.shift_id = v.work_shift', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = v.vacancy_location', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('v.vacancy_id', 'DESC');
		$objResult = $this->db->get(TABLE_VACANCY . ' v');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function notExistingCandidate($strField = 'candidate_id', $strValue = 0, $strID = 0) {	
		$this->db->select(' count(*) as count ');
		$this->db->where($strField, $strValue);
		
		if($strID) {
			$this->db->where('candidate_id not in ', '(' . $strID . ')', false);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		if($arrResult[0]['count'] >= 1) {
			return false;
		}
		return true;
	}
	
}
?>